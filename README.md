#Divide et Impera

Jest to wersja dostosowana do grudniowego patcha 5.2. 

Powiedzmy, że jest to pierwsza oficjalna wersja moda pod dodatek Divine Wind, choć zapewne nie jest jeszcze doskonała i części ze zgłoszonych wcześniej crashów nie udało mi się zlokalizować, co może oznaczać że się powtórzą. Niemniej jest to pierwsza wersja, która oprócz usunięcia oczywistych bugów wprowadza pewne zupełnie nowe rozwiązania do rozgrywki. Dotyczą one systemu kolonizacji. Zaznaczam, że nie miałem czasu go dłużej testować pod względem balansu, a jedynie sprawdziłem techniczną poprawność poszczególnych rozwiązań. Stąd moja prośba o przetestowania nowego systemu zarówno od strony gracza jak i sprawdzenia jak sobie z nim radzi AI.
