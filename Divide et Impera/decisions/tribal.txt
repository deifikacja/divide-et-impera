country_decisions = {
	
	tribal_despotism_reform = {
		potential = {
			government = tribal_despotism
			or = {
				NOT = { centralization_decentralization = 2 }
				NOT = { innovative_narrowminded = 2 }
			}
			OR = {
				OR = {
						technology_group = western
						technology_group = eastern
						technology_group = ottoman
						technology_group = muslim
				}
				any_neighbor_country = {
					OR = {
						technology_group = western
						technology_group = eastern
						technology_group = ottoman
						technology_group = muslim
					}
				}
			}
		}
		allow = {
			ADM = 7
			legitimacy = 0.75
			government_tech = 2
			stability = 2
			war = no
			officials = 3
		}
		effect = {
			government = eastern_despotism
			stability = -5
			officials = -3
		}
		ai_will_do = {
			factor = 1
		}
	}
	tribal_federation_reform = {
		potential = {
			government = tribal_federation
			or = {
				NOT = { centralization_decentralization = 2 }
				NOT = { innovative_narrowminded = 2 }
			}
			OR = {
				OR = {
						technology_group = western
						technology_group = eastern
						technology_group = ottoman
						technology_group = muslim
				}
				any_neighbor_country = {
					OR = {
						technology_group = western
						technology_group = eastern
						technology_group = ottoman
						technology_group = muslim
					}
				}
			}
		}
		allow = {
			ADM = 7
			government_tech = 4
			stability = 2
			war = no
			officials = 3
		}
		effect = {
			government = eastern_despotism
			stability = -4
			officials = -3
		}
		ai_will_do = {
			factor = 1
		}
	}
	tribal_democracy_reform = {
		potential = {
			government = tribal_democracy
			or = {
				NOT = { centralization_decentralization = 2 }
				NOT = { innovative_narrowminded = 2 }
			}
			OR = {
				OR = {
						technology_group = western
						technology_group = eastern
						technology_group = ottoman
						technology_group = muslim
				}
				any_neighbor_country = {
					OR = {
						technology_group = western
						technology_group = eastern
						technology_group = ottoman
						technology_group = muslim
					}
				}
			}
		}
		allow = {
			ADM = 7
			government_tech = 6
			stability = 2
			war = no
			officials = 3
		}
		effect = {
			government = oligarchical_monarchy
			stability = -3
			officials = -3
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	steppe_horde_reform = {
		potential = {
			government = steppe_horde
		}
		allow = {
		NOT = { centralization_decentralization = 2 }
			NOT = { innovative_narrowminded = 1 }
			OR = {
				OR = {
						technology_group = western
						technology_group = eastern
						technology_group = ottoman
						technology_group = muslim
				}
				any_neighbor_country = {
					OR = {
						technology_group = western
						technology_group = eastern
						technology_group = ottoman
						technology_group = muslim
					}
				}
			}
			ADM = 7
			legitimacy = 0.75
			government_tech = 10
			stability = 2
		}
		effect = {
			government = eastern_despotism
			stability = -5
		}
		ai_will_do = {
			factor = 1
		}
	}
	
}
