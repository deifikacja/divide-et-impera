country_decisions = {

	the_polish_commonwealth = {
		potential = {
			NOT = { exists = RZP }
			OR = { 
				tag = POL
				tag = HUP
			}
			NOT = { ai = yes }
		}
		
		allow = {
			OR = {
				senior_union_with = LIT
				LIT = { vassal_of = POL }
				LIT = { vassal_of = HUP }
			}
			relation = { who = LIT value = 190 }
			LIT = { religion = THIS }
			DIP = 6	
			OR = { idea = humanist_tolerance government = noble_republic }
			war = no
		}
	
		effect = {		
			POL = {
				add_core = 37
				add_core = 38
				add_core = 269
				add_core = 270
				add_core = 271
				add_core = 272
				add_core = 273
				add_core = 275
				add_core = 276
				add_core = 277
				add_core = 278
				add_core = 279
				add_core = 280
				add_core = 289
				add_core = 290
				add_core = 292
				add_core = 293
				add_core = 1320
				add_core = 2165
				add_core = 2167
				add_core = 2168
				add_core = 2169
				add_core = 2175
			}
			inherit = LIT
			capital_scope = { base_tax = 1 multiply_citysize = 1.15 }
			prestige = 0.1
			change_tag = RZP
			badboy = -10
		}

		ai_will_do = {
			factor = 0
		}
	}


	the_lithuanian_commonwealth = {
		potential = {
			NOT = { exists = RZP }
			tag = LIT
		}
		
		allow = {
			OR = {
				senior_union_with = POL
				senior_union_with = HUP
				POL = { vassal_of = LIT }
				HUP = { vassal_of = LIT }
			}
			OR = { 
				relation = { who = POL value = 190 }
				relation = { who = HUP value = 190 }
			}
			DIP = 6	
			OR = { idea = humanist_tolerance government = noble_republic }
			war = no
		}
	
		effect = {		
			LIT = {
				add_core = 42
				add_core = 43
				add_core = 254
				add_core = 255
				add_core = 256
				add_core = 257
				add_core = 258
				add_core = 259
				add_core = 260
				add_core = 261
				add_core = 262
				add_core = 1252
				add_core = 1253
				add_core = 1944
				add_core = 2172
				add_core = 2173
				add_core = 2174
			}
			inherit = POL
			capital_scope = { base_tax = 1 multiply_citysize = 1.15 }
			prestige = 0.1
			change_tag = RZP
			badboy = -10
		}

		ai_will_do = {
			factor = 0
		}
	}

	the_polish_commonwealth_ai = {
		potential = {
			NOT = { exists = RZP }
			OR = { 
				tag = POL
				tag = HUP
			}
			ai = yes
		}
		
		allow = {
			alliance_with =  LIT
			relation = { who = LIT value = 100 }
			DIP = 6	
			OR = { idea = humanist_tolerance government = noble_republic }
			year = 1505
			war = no
		}
	
		effect = {		
			POL = {
				add_core = 37
				add_core = 38
				add_core = 269
				add_core = 270
				add_core = 271
				add_core = 272
				add_core = 273
				add_core = 275
				add_core = 276
				add_core = 277
				add_core = 278
				add_core = 279
				add_core = 280
				add_core = 289
				add_core = 290
				add_core = 292
				add_core = 293
				add_core = 1320
				add_core = 2165
				add_core = 2167
				add_core = 2168
				add_core = 2169
				add_core = 2175
			}
			inherit = LIT
			capital_scope = { base_tax = 1 multiply_citysize = 1.15 }
			prestige = 0.1
			change_tag = RZP
			badboy = -10
		}

		ai_will_do = {
			factor = 1
		}
	}

}