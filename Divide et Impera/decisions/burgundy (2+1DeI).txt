country_decisions = {
	establish_golden_fleece = {
	
		potential = {
			tag = BUR
			NOT = { has_country_modifier = golden_fleece }
		}
		allow = {
			religion = catholic
			NOT = {  aristocracy_plutocracy = -2 }
			prestige = 0.40
			government = feudal_monarchy
		}
		effect = {
			add_country_modifier = {
				name = "golden_fleece"
				duration = -1
			}
			aristocracy_plutocracy = -1
			centralization_decentralization = -1
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	create_the_compagnies_ordonnance = {
	
		potential = {
			tag = BUR
			NOT = { has_country_modifier = compagnies_ordonnance }
		}
		allow = {
			MIL = 7
		}
		effect = {
			add_country_modifier = {
				name = "compagnies_ordonnance"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
		}
	}

	the_king_of_the_golden_hall = {
	
		potential = {
			tag = BUR
			NOT = { has_country_flag = the_king_of_the_golden_hall_flag }
		}
		allow = {
			prestige = 0.5
			num_of_cities = 20
			treasury = 101
			DIP = 6
		}
		effect = {
			treasury = -100
			prestige = 0.2
			capital_scope = { base_tax = 1 multiply_citysize = 1.15 }
			random_owned = { base_tax = 1 }
			relation = { who = FRA value = -150 }
			add_country_modifier = {
				name = "the_king_of_the_golden_hall"
				duration = 3650
			}
			set_country_flag = the_king_of_the_golden_hall_flag
		}
		ai_will_do = {
			factor = 1
		}
	}		
}