country_decisions = {

	rise_of_the_chinese_dynasty = {
		potential = {
			culture_group = sinnic
			num_of_cities = 5
			NOT = { has_country_flag = new_chinese_dynasty }
			NOT = { has_global_flag = dei_rise_of_the_new_power }
		}
		allow = {
			prestige = 0.05
		}
		effect = {
			ming_china = { add_core = THIS }
			treasury = 250
			add_country_modifier = {
				name = "rise_of_the_new_dynasty"
				duration = 7300
			}
			government = eastern_bureaucracy
			set_country_flag = new_chinese_dynasty
			set_global_flag = dei_rise_of_the_new_power
		}
		ai_will_do = {
			factor = 1
		}
	}

	rise_of_the_chinese_empire = {
		potential = {
			culture_group = sinnic
			has_country_flag = new_chinese_dynasty
			NOT = { has_country_flag = old_chinese_dynasty }
		}
		allow = {
			NOT = { is_subject = yes }
			prestige = 0.10
			controls = 695			# Beijing
		}
		effect = {
			ming_china = { secede_province = THIS }
			government = eastern_bureaucratic_empire
			set_country_flag = old_chinese_dynasty
			clr_global_flag = dei_rise_of_the_new_power
		}
		ai_will_do = {
			factor = 1
		}
	}
}
