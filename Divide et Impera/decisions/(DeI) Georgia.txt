country_decisions = {

	georgian_nation = {
		potential = {
			NOT = { exists = GEO }
			primary_culture = georgian
		}
		allow = {
			owns = 423	# Kartli
			owns = 1314 	# Imereti
			owns = 1315	# Kakheti

			is_core = 423	# Kartli
			is_core = 1314 	# Imereti
			is_core = 1315	# Kakheti

			war = no
		}
	
		effect = {
			GEO = {
			add_core = 422	# Abkhazia
			add_core = 423	# Kartli
			add_core = 1314	# Imereti
			add_core = 1315	# Kakheti
			}
			centralization_decentralization = -2
			merchants = 1
			random_owned = {
				base_tax = 1
			}
			prestige = 0.05
			change_tag = GEO
		}

		ai_will_do = {
			factor = 1
		}
	}
}