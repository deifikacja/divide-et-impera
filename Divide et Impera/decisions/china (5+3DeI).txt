country_decisions = {
	great_chinese_fleet = {
	
		potential = {
			culture_group = sinnic
			NOT = { has_country_flag = great_chinese_fleet_start }
		}
		allow = {
			num_of_ports = 10
			OR = { 
				MIL = 7
				advisor = naval_reformer
				advisor = naval_organiser
			}
		}
		effect = {
			set_country_flag = great_chinese_fleet_start
			add_idea = quest_for_the_new_world
			land_naval = 1
			innovative_narrowminded = -1
			add_country_modifier = {
				name = "great_chinese_fleet"
				duration = 4380
			}			
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				NOT = { stability = 2 }
			}
			modifier = {
				factor = 0
				war = yes
			}
		}
	}

	great_chinese_fleet_2 = {
	
		potential = {
			culture_group = sinnic
			has_country_flag = great_chinese_fleet_start
			NOT = { has_country_modifier = great_chinese_fleet }
			NOT = { has_country_modifier = end_of_naval_illusions }
		}
		allow = {
			num_of_ports = 10
			treasury = 150
		}
		effect = {
			treasury = -150
			land_tech = -500
			add_country_modifier = {
				name = "great_chinese_fleet"
				duration = 4380
			}			
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				NOT = { land_naval = 0 }
			}

		}
	}
	end_of_great_chinese_fleet = {
	
		potential = {
			culture_group = sinnic
			has_country_flag = great_chinese_fleet_start
			NOT = { has_country_modifier = great_chinese_fleet }
			NOT = { has_country_modifier = end_of_naval_illusions }
		}
		allow = {
			NOT = { land_naval = 0 }
		}
		effect = {
			remove_idea = quest_for_the_new_world
			land_naval = -1
			innovative_narrowminded = 1
			add_country_modifier = {
				name = "end_of_naval_illusions"
				duration = -1
			}			
		}
		ai_will_do = {
			factor = 1
		}
	}
}

cultural_decisions = {
	support_temple_faction = {
		potential = {
			NOT = { faction_in_power = temples }
			OR = {
				faction_in_power = bureaucrats
				faction_in_power = enuchs
			}			
		}
		allow = {
			officials = 3
		}
		effect = {
			faction_influence = {
				faction = temples
				influence = 5
			}
			officials = -3
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	support_enuchs_faction = {
		potential = {
			NOT = { faction_in_power = enuchs }
			OR = {
				faction_in_power = temples
				faction_in_power = bureaucrats
			}
		}
		allow = {
			officials = 3
		}
		effect = {
			faction_influence = {
				faction = enuchs
				influence = 5
			}
			officials = -3
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	support_crats_faction = {
		potential = {
			NOT = { faction_in_power = bureaucrats }
			OR = {
				faction_in_power = temples
				faction_in_power = enuchs
			}
		}
		allow = {
			officials = 3
		}
		effect = {
			faction_influence = {
				faction = bureaucrats
				influence = 5
			}
			officials = -3
		}
		ai_will_do = {
			factor = 0
		}
	}		
}

province_decisions = {
	reparations_of_the_great_wall = {
		potential = {
			not = { has_province_modifier = great_wall_reparations }
			owner = {
				or = {
					tag = MCH
					tag = MNG
				}
				owns = 693
				owns = 695
				owns = 696
				owns = 697
				owns = 699
				owns = 700
				owns = 707
				owns = 709
			}
			or = {
				province_id = 693
				province_id = 695
				province_id = 696
				province_id = 697
				province_id = 699
				province_id = 700
				province_id = 707
				province_id = 709
			}
		}
		allow = {
			owner = {
				or = {
					advisor = fortification_expert
					advisor = army_reformer
					mil = 7
				}
				treasury  = 100
				officials = 1
			}
		}
		effect = {
			owner = { treasury = -50 officials = -1 }
			add_province_modifier = {
				name = "great_wall_reparations"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				NOT = { owner = { treasury  = 500 } }
			}
		}
	}

	build_the_forbidden_city = {
		potential = {
			OR = {
				province_id = 1816
				province_id = 1821
			}
			is_capital = yes
			owner = {
				OR = {
					tag = MCH
					tag = MNG
				}
				has_country_flag = no_forbidden_city
			}
			NOT = { has_province_modifier = construct_the_forbidden_city }
			NOT = { has_province_flag = the_forbidden_city }
		}
		allow = {
			owner = {
				war = no
				OR = {
					statesman = 4
					adm = 7
				}
				OR = {
					faction_in_power = bureaucrats
					has_factions = no
					}
				stability = 1
				treasury  = 300
				officials = 3
			}
			NOT = {
				any_neighbor_province = {
					NOT = { owned_by = THIS }
					NOT = { controlled_by = THIS }
				}
			}
		}
		effect = {
			owner = { 
				treasury = -300 
				officials = -3
			}
			set_province_flag = construct_the_forbidden_city
			add_province_modifier = {
				name = "construct_the_forbidden_city"
				duration = 1825
			}
			owner = { clr_country_flag = no_forbidden_city }
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				NOT = { owner = { treasury  = 600 } }
			}
		}
	}
}
