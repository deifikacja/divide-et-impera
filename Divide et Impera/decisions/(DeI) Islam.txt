province_decisions = {

	muslim_schools = {
		potential = {
			owner = { religion_group = muslim }
			has_missionary = yes
			NOT = { religion_group = christian }
			NOT = { religion_group = muslim }
		}
		
		allow = {
			owner = { 
				secularism_theocracy = 3
				treasury = 25 
				officials = 1 
			}
			NOT = { has_province_modifier = muslim_schools }
		}
		
		effect = {
			add_province_modifier = {
				name = "muslim_schools"
				duration = 7300
			}
		}
		
		ai_will_do = {
			factor = 1
		}
	}
}