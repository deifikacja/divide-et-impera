country_decisions = {

	amber_monopoly = {
		potential = {
			production_tech = 5
			government_tech = 5
			owns = 41
			41 = { NOT = { has_province_modifier = amber_monopoly } }
		}
		allow = {
			ADM = 6
			NOT = { centralization_decentralization = 1 }
		}
		effect = {
			41 = { 
				add_province_modifier = {
					name = "amber_monopoly"
					duration = 9125
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	enact_prussian_military_reforms = {
		potential = {
			tag = PRU
			NOT = { has_country_modifier = prussian_military_reforms }
		}
		allow = {
			MIL = 7
			land_tech = 30
			army_tradition = 0.5
			capital = 50
		}
		effect = {
			add_country_modifier = {
				name = "prussian_military_reforms"
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	form_kingdom_of_prussia = {
		potential = {
			tag = BRA
			NOT = { exists = PRU }
		}
		allow = {
			is_religion_enabled = protestant
			owns = 41			# Ostpreussen
			OR = {
				owns = 43		#danzig
				owns = 48		#hinterpommern
			}
			DIP = 7
			prestige = 0.2
			war = no
		}
		effect = {
			centralization_decentralization = -1
			prestige = 0.1
			primary_culture = east_german
			change_tag = PRU
			PRU = {
				add_core = 41			# Ostpreussen
				add_core = 42			# Warmia
				add_core = 43			# Danzig
				add_core = 40			# Memel
				add_core = 1252 		# Danzig Pomerania
				add_core = 2145 		# Masuria
				add_core = 2173  		# Chelminskie
			}
			capital_scope = {
				culture = east_german
				base_tax = 2
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
}
