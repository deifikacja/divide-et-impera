province_decisions = {

build_armory = {
		potential = {
			owner = { ai = yes }
			NOT = { has_building = armory }
			OR = { 
				AND = {
					has_owner_religion = yes
					culture_group = THIS
					}
				AND = {
					owner = { 
						treasury = 150 
						officials = 3
						}					
					culture_group = THIS
					}
				AND = {
					owner = { 
						treasury = 250 
						officials = 4
						}				
					has_owner_religion = yes
					}

				}
			owner = { land_tech = 4 }
		}
		allow = {
			controlled_by = owner
			owner = { 
					treasury = 100  
					officials = 1
					}
		}
		effect = {
			owner = {
				treasury = -40
				officials = -1
			}
            add_building = armory
		}

	}
build_regimental = {
		potential = {
			owner = { ai = yes}
			has_building = barracks
			NOT = { has_building = regimental_camp }
			OR = { 
				manpower = 8
				AND = {
					manpower = 6
					owner = { treasury = 350 }
					}
				AND = {
					manpower = 4
					owner = { treasury = 500 }
					}
				AND = {
					manpower = 2
					owner = { treasury = 500 
							officials = 2 }
					}
				}
			owner = { land_tech = 19 }
		}
		allow = {
			controlled_by = owner
			owner = { 
					treasury = 200  
					officials = 1
					}
		}
		effect = {
			owner = {
				treasury = -120
				officials = -1
			}
            add_building = regimental_camp
		}

	}
	
build_barracks = {
		potential = {
			owner = { ai = yes}
			has_building = training_fields
			NOT = { has_building = barracks }
			OR = { 
				AND = {
					has_owner_religion = yes
					culture_group = THIS
					}
				AND = {
					owner = { 
						treasury = 250 
						officials = 2
						}					
					culture_group = THIS
					}
				AND = {
					owner = { 
						treasury = 400 
						officials = 3
						}				
					has_owner_religion = yes
					}

				}
			owner = { land_tech = 12 }
		}
		allow = {
			controlled_by = owner
			owner = { 
					treasury = 200  
					officials = 1
					}
		}
		effect = {
			owner = {
				treasury = -85
				officials = -1
			}
            add_building = barracks
		}

	}

build_training_fields = {
		potential = {
			owner = { ai = yes}
			has_building = armory 
			NOT = { has_building = training_fields }
			OR = { 
				AND = {
					has_owner_religion = yes
					culture_group = THIS
					}
				AND = {
					owner = { 
						treasury = 350 
						officials = 2
						}					
					culture_group = THIS
					}
				AND = {
					owner = { 
						treasury = 500 
						officials = 3
						}				
					has_owner_religion = yes
					}

			}
			owner = { land_tech = 8 }
		}
		allow = {
			controlled_by = owner
			owner = { 
					treasury = 200  
					officials = 1
					}
		}
		effect = {
			owner = {
				treasury = -65
				officials = -1
			}
            add_building = training_fields
		}

	}
	

	
build_constable = {
		potential = {
			owner = { ai = yes}
			NOT = { has_building = constable }
			OR = { 
				AND = {
					base_tax = 2 
					has_owner_religion = yes
					has_owner_culture = yes
					}
				AND = {
					base_tax = 3
					has_owner_religion = yes
					culture_group = THIS
					}
				AND = {
					base_tax = 4 
					has_owner_religion = no
					culture_group = THIS
					}
				AND = {
					base_tax = 4 
					has_owner_religion = no
					has_owner_culture = yes
					}
				AND = {
					base_tax = 5 
					has_owner_religion = yes
					}
				base_tax = 6
				}
			owner = { production_tech = 4 }
		}
		allow = {
			owner = { 
					treasury = 70  
					officials = 1
					}
			controlled_by = owner
		}
		effect = {
			owner = {
				treasury = -40
				officials = -1
			}
            add_building = constable
		}

	}

build_dock = {
		potential = {
			owner = { ai = yes}
			NOT = { has_building = dock }
			owner = { naval_tech = 4 }
			port = yes
		}
		allow = {
			owner = { 
					treasury = 80  
					officials = 1
					}
			controlled_by = owner
		}
		effect = {
			owner = {
				treasury = -40
				officials = -1
			}
            add_building = dock
		}

	}
	
build_town = {
		potential = {
			owner = { ai = yes}
			has_building = spy_agency
			NOT = { has_building = town_hall }
			owner = { government_tech = 19 }
			NOT = { is_capital = yes }
			OR = {
				base_tax = 6
				AND = {
					base_tax = 4
					treasury = 1000
					officials = 4
				}
			}
		}
		allow = {
			owner = { 
					treasury = 250  
					officials = 1
					}
			controlled_by = owner
		}
		effect = {
			owner = {
				treasury = -125
				officials = -1
			}
            add_building = town_hall
		}

	}
	
	
build_treasury = {
		potential = {
			owner = { ai = yes}
			has_building = counting_house
			NOT = { has_building = treasury_office }
			OR = { 
				AND = {
					base_tax = 2 
					has_owner_religion = yes
					has_owner_culture = yes
					}
				AND = {
					base_tax = 3
					has_owner_religion = yes
					culture_group = THIS
					}
				AND = {
					base_tax = 4 
					has_owner_religion = no
					culture_group = THIS
					}
				AND = {
					base_tax = 4 
					has_owner_religion = no
					has_owner_culture = yes
					}
				AND = {
					base_tax = 5 
					has_owner_religion = yes
					}
				base_tax = 6
				}
			owner = { production_tech = 19 }
		}
		allow = {
			owner = { 
					treasury = 200  
					officials = 1
					}
			controlled_by = owner
		}
		effect = {
			owner = {
				treasury = -120
				officials = -1
			}
            add_building = treasury_office
		}

	}		
	
build_counting = {
		potential = {
			owner = { ai = yes}
			has_building = workshop
			NOT = { has_building = counting_house }
			OR = { 
				AND = {
					NOT = { trade_goods = gold }
					OR = {
						trade_goods = grain
						trade_goods = wool
					}
					owner = { 
						treasury = 600 
						officials = 4
					}
				}
				AND = {
					OR = {
						trade_goods = naval_supplies
						trade_goods = fish
					}
					owner = { 
						treasury = 400 
						officials = 3
					}
				}
				AND = {
					NOT = {
						OR = {
							trade_goods = grain
							trade_goods = wool
							trade_goods = naval_supplies
							trade_goods = fish
						}
					}
				}
				base_tax = 6
				}
			owner = { production_tech = 13 }
		}
		allow = {
			owner = { 
					treasury = 150  
					officials = 1
					}
			controlled_by = owner
		}
		effect = {
			owner = {
				treasury = -85
				officials = -1
			}
            add_building = counting_house
		}

	}	

build_marketplace = {
		potential = {
			owner = { ai = yes}
			NOT = { has_building = marketplace }
			OR = { 
				AND = {
					OR = {
						trade_goods = grain
						trade_goods = wool
					}
					owner = { 
						treasury = 300 
						officials = 4
					}
				}
				AND = {
					OR = {
						trade_goods = naval_supplies
						trade_goods = fish
					}
					owner = { 
						treasury = 200 
						officials = 3
					}
				}
				AND = {
					NOT = {
						OR = {
							trade_goods = grain
							trade_goods = wool
							trade_goods = naval_supplies
							trade_goods = fish
						}
					}
				}
			}
			owner = { trade_tech = 4 }
		}
		allow = {
			owner = { 
					treasury = 100  
					officials = 1
					}
			controlled_by = owner
		}
		effect = {
			owner = {
				treasury = -40
				officials = -1
			}
            add_building = marketplace
		}

	}

build_workshop = {
		potential = {
			owner = { ai = yes}
			NOT = { has_building = workshop }
			has_building = constable
			OR = { 
				AND = {
					has_owner_religion = yes
					culture_group = THIS
				}
				AND = {
					owner = { 
						treasury = 200 
						officials = 3
						}					
					culture_group = THIS
				}
				AND = {
					owner = { 
						treasury = 300 
						officials = 4
						}				
					has_owner_religion = yes
				}
			}
			owner = { production_tech = 8 }
		}
		allow = {
			controlled_by = owner
			owner = { 
				treasury = 100  
				officials = 1
			}
		}
		effect = {
			owner = {
				treasury = -65
				officials = -1
			}
            add_building = workshop
		}

	}

build_canal = {
		potential = {
			owner = { ai = yes}
			has_building = trade_depot
			NOT = { has_building = canal }
			OR = { 
				AND = {
					OR = {
						trade_goods = grain
						trade_goods = wool
						}
					owner = { 
						treasury = 600 
						officials = 4
						}
					}
				AND = {
					OR = {
						trade_goods = naval_supplies
						trade_goods = fish
						}
					owner = { 
						treasury = 400 
						officials = 3
						}
					}
				AND = {
					NOT = {
						OR = {
							trade_goods = grain
							trade_goods = wool
							trade_goods = naval_supplies
							trade_goods = fish
						}
					}
				}
			}
			owner = { trade_tech = 12 }
		}
		allow = {
			owner = { 
					treasury = 200  
					officials = 1
					}
			controlled_by = owner
		}
		effect = {
			owner = {
				treasury = -85
				officials = -1
			}
            add_building = canal
		}

	}
	
build_depot = {
		potential = {
			owner = { ai = yes}
			has_building = marketplace
			NOT = { has_building = trade_depot }
			OR = { 
				AND = {
					OR = {
						trade_goods = grain
						trade_goods = wool
						}
					owner = { 
						treasury = 600 
						officials = 4
						}
					}
				AND = {
					OR = {
						trade_goods = naval_supplies
						trade_goods = fish
						}
					owner = { 
						treasury = 400 
						officials = 3
						}
					}
				AND = {
					NOT = {
						OR = {
							trade_goods = grain
							trade_goods = wool
							trade_goods = naval_supplies
							trade_goods = fish
						}
					}
				}
			}
			owner = { trade_tech = 8 }
		}
		allow = {
			owner = { 
					treasury = 250  
					officials = 1
					}
			controlled_by = owner
		}
		effect = {
			owner = {
				treasury = -65
				officials = -1
			}
            add_building = trade_depot
		}

	}	


build_court = {
		potential = {
			owner = { ai = yes}
			has_building = temple
			NOT = { has_building = courthouse }
			owner = { government_tech = 8 }
			OR = {
				base_tax = 6
				AND = {
					base_tax = 4
					treasury = 500
					officials = 3
				}
				AND = {
					base_tax = 2
					treasury = 1000
					officials = 4
				}
			}
		}
		allow = {
			owner = { 
				treasury = 200  
				officials = 1
			}
			controlled_by = owner
		}
		effect = {
			owner = {
				treasury = -65
				officials = -1
			}
            add_building = courthouse
		}

	}
	
build_spy = {
		potential = {
			owner = { ai = yes}
			has_building = courthouse
			NOT = { has_building = spy_agency }
			owner = { government_tech = 12 }
			NOT = { is_capital = yes }
			OR = {
				base_tax = 6
				AND = {
					base_tax = 4
					treasury = 1000
					officials = 3
				}
				AND = {
					base_tax = 2
					treasury = 2000
					officials = 4
				}
			}
		}
		allow = {
			owner = { 
				treasury = 250  
				officials = 1
			}
			controlled_by = owner
		}
		effect = {
			owner = {
				treasury = -85
				officials = -1
			}
            add_building = spy_agency
		}

	}
	
build_fine_arts_academy = {
		potential = {
			owner = { ai = yes}
			NOT = { has_building = refinery }
			NOT = { has_building = wharf }
			NOT = { has_building = weapons }
			NOT = { has_building = textile }
			NOT = { has_building = fine_arts_academy }
			NOT = { has_building = university }
			NOT = {
				OR = {
					trade_goods = wine
					trade_goods = sugar
					trade_goods = wool
					trade_goods = cloth
					trade_goods = copper
					trade_goods = iron
					trade_goods = naval_supplies
				}
			}
			OR = { 
				AND = {
					owner = { 
						treasury = 6000 
						officials = 1
					}
				}
				AND = {
					owner = { 
						treasury = 3500 
						officials = 4
						}
					}
				}
			owner = { government_tech = 21 }
		}
		allow = {
			owner = { 
				treasury = 1500  
				officials = 1
			}
			controlled_by = owner
		}
		effect = {
			owner = {
				treasury = -950
				officials = -1
			}
            add_building = fine_arts_academy
		}

	}

build_refinery = {
		potential = {
			owner = { ai = yes}
			NOT = { has_building = refinery }
			NOT = { has_building = wharf }
			NOT = { has_building = weapons }
			NOT = { has_building = textile }
			NOT = { has_building = fine_arts_academy }
			NOT = { has_building = university }
			NOT = {
				OR = {
					trade_goods = wool
					trade_goods = cloth
					trade_goods = copper
					trade_goods = iron
					trade_goods = naval_supplies
				}
			}
			OR = { 
				AND = {
					OR = {
						trade_goods = wine
						trade_goods = sugar
					}
					owner = { 
						treasury = 5000 
						officials = 1
					}
				}
				AND = {
					OR = {
						trade_goods = wine
						trade_goods = sugar
					}
					owner = { 
						treasury = 2500 
						officials = 2
					}
				}
				AND = {
					owner = { 
						treasury = 10000 
						officials = 4
					}
				}
			}
			owner = { trade_tech = 14 }
		}
		allow = {
			owner = { 
				treasury = 1500  
				officials = 1
			}
			controlled_by = owner
		}
		effect = {
			owner = {
				treasury = -950
				officials = -1
			}
            add_building = refinery
		}

	}
	
build_textile = {
		potential = {
			owner = { ai = yes}
			NOT = { has_building = refinery }
			NOT = { has_building = wharf }
			NOT = { has_building = weapons }
			NOT = { has_building = textile }
			NOT = { has_building = fine_arts_academy }
			NOT = { has_building = university }
			NOT = {
				OR = {
					trade_goods = wine
					trade_goods = sugar
					trade_goods = copper
					trade_goods = iron
					trade_goods = naval_supplies
				}
			}
			OR = { 
				AND = {
					OR = {
						trade_goods = wool
						trade_goods = cloth
						}
					owner = { 
						treasury = 5000 
						officials = 1
						}
					}
				AND = {
					OR = {
						trade_goods = wool
						trade_goods = cloth
						}
					owner = { 
						treasury = 2500 
						officials = 2
						}
					}
				AND = {
					owner = { 
						treasury = 10000 
						officials = 4
						}
					}

				}
			owner = { production_tech = 14 }
		}
		allow = {
			owner = { 
				treasury = 1500  
				officials = 1
			}
			controlled_by = owner
		}
		effect = {
			owner = {
				treasury = -950
				officials = -1
			}
            add_building = textile
		}

	}
	
build_weapons = {
		potential = {
			owner = { ai = yes}
			NOT = { has_building = refinery }
			NOT = { has_building = wharf }
			NOT = { has_building = weapons }
			NOT = { has_building = textile }
			NOT = { has_building = fine_arts_academy }
			NOT = { has_building = university }
			NOT = {
				OR = {
					trade_goods = wine
					trade_goods = sugar
					trade_goods = wool
					trade_goods = cloth
					trade_goods = naval_supplies
				}
			}
			OR = { 
				AND = {
					OR = {
						trade_goods = copper
						trade_goods = iron
						}
					owner = { 
						treasury = 5000 
						officials = 1
						}
					}
				AND = {
					OR = {
						trade_goods = copper
						trade_goods = iron
						}
					owner = { 
						treasury = 2500 
						officials = 2
						}
					}
				AND = {
					owner = { 
						treasury = 10000 
						officials = 4
						}
					}

				}
			owner = { land_tech = 15 }
		}
		allow = {
			owner = { 
					treasury = 1500  
					officials = 1
					}
			controlled_by = owner
		}
		effect = {
			owner = {
				treasury = -950
				officials = -1
			}
            add_building = weapons
		}

	}
	
build_wharf = {
		potential = {
			owner = { ai = yes}
			NOT = { has_building = refinery }
			NOT = { has_building = wharf }
			NOT = { has_building = weapons }
			NOT = { has_building = textile }
			NOT = { has_building = fine_arts_academy }
			NOT = { has_building = university }
			NOT = {
					OR = {
						trade_goods = wine
						trade_goods = sugar
						trade_goods = wool
						trade_goods = cloth
						trade_goods = copper
						trade_goods = iron
						}
			}
			OR = { 
				AND = {
					OR = {
						trade_goods = naval_supplies
						}
					owner = { 
						treasury = 5000 
						officials = 1
						}
					}
				AND = {
					OR = {
						trade_goods = naval_supplies
						}
					owner = { 
						treasury = 2500 
						officials = 2
						}
					}
				AND = {
					owner = { 
						treasury = 10000 
						officials = 4
						}
					}

				}
			owner = { naval_tech = 15 }
		}
		allow = {
			owner = { 
					treasury = 1500  
					officials = 1
					}
			controlled_by = owner
		}
		effect = {
			owner = {
				treasury = -950
				officials = -1
			}
            add_building = wharf
		}

	}
	
build_university = {
		potential = {
			owner = { ai = yes}
			NOT = { has_building = refinery }
			NOT = { has_building = wharf }
			NOT = { has_building = weapons }
			NOT = { has_building = textile }
			NOT = { has_building = fine_arts_academy }
			NOT = { has_building = university }
			NOT = {
					OR = {
						trade_goods = wine
						trade_goods = sugar
						trade_goods = wool
						trade_goods = cloth
						trade_goods = copper
						trade_goods = iron
						trade_goods = naval_supplies
						}
			}
			OR = { 
				AND = {
					owner = { 
						treasury = 6000 
						officials = 1
						}
					}
				AND = {
					owner = { 
						treasury = 3500 
						officials = 4
						}
					}
				AND = {
					owner = { 
						treasury = 10000 
						officials = 4
						}
					}

				}
			owner = { government_tech = 16 }
		}
		allow = {
			owner = { 
					treasury = 1500  
					officials = 1
					}
			controlled_by = owner
		}
		effect = {
			owner = {
				treasury = -950
				officials = -1
			}
            add_building = university
		}

	}
	
}