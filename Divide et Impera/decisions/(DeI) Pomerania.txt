country_decisions = {

	pommeranian_nation = {
		potential = {
			NOT = { tag = GER }
			NOT = { tag = HRE }
			NOT = { tag = HSA }
			NOT = { exists = POM }
			primary_culture = pommeranian
		}

		allow = {
			owns = 47	# Vorpommern
			owns = 48	# Hinterpommern
			owns = 2156	# Slupsk
			is_core = 2156	# Slupsk
			is_core = 47	# Vorpommern
			is_core = 48	# Hinterpommern
			war = no
		}
	
		effect = {
			western_pommerania = { add_core = POM }
			centralization_decentralization = -1
			random_owned = { base_tax = 1 }
		        prestige = 0.05
			change_tag = POM
		}

		ai_will_do = {
			factor = 1
		}
	}
	
}