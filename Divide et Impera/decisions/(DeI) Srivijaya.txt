country_decisions = {

	srivijayan_empire = {
		potential = {
			NOT = { exists = SRV }
			primary_culture = malayan
		}

		allow = {
			owns = 1375
			owns = 1377
			owns = 1378
			owns = 1379
			owns = 1380
			owns = 617

			is_core = 1375
			is_core = 1377
			is_core = 1378
			is_core = 1379
			is_core = 1380
			is_core = 617

			war = no
		}
	
		effect = {
			SRV = { 
				add_core = 1375
				add_core = 1377
				add_core = 1378
				add_core = 1379
				add_core = 1380
				add_core = 617
				add_core = 619
				add_core = 621
				add_core = 622
				add_core = 623
				add_core = 620
				add_core = 618
				add_core = 1376
				add_core = 1400
			}
			prestige = 0.1
			merchants = 2
			treasury = 100
			change_tag = SRV
		}

		ai_will_do = {
			factor = 1
		}
	}
	
}