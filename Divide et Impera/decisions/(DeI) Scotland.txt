country_decisions = {

	make_edinburgh_capital = {
		potential = {
			tag = SCO
			production_tech = 4
                        government_tech = 4
                        trade_tech = 4
			owns = 248
			NOT = { capital = 248 }
			NOT = { has_country_flag = relocated_capital_edinburgh }
		}
		allow = {
			war = no
			ADM = 7

		}
		effect = {
			set_country_flag = relocated_capital_edinburgh
			stability = -2
			capital = 248
            innovative_narrowminded = -1
            serfdom_freesubjects = -1
            aristocracy_plutocracy = -1
            mercantilism_freetrade = 1
			248 = {
			 	base_tax = 1 
			 	change_manpower = 1
				citysize = 2000
			 }
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				NOT = { stability = 1 }
			}
		}
	}
}