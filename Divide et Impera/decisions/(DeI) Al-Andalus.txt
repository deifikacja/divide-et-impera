country_decisions = {

	al_andalus = {
		potential = {
			NOT = { exists = CDB }
			religion_group = muslim
			primary_culture = andalucian
		}
		allow = {
			owns = 219	#Toledo
			owns = 220	#Valencia
			owns = 223	#Granada
			owns = 224	#Andalucia
			owns = 225	#Cordoba

			is_core = 219	#Toledo
			is_core = 220	#Valencia
			is_core = 223	#Granada
			is_core = 224	#Andalucia
			is_core = 225	#Cordoba

			war = no
		}

		effect = {
			CDB = {
				add_core = 217
				add_core = 218
				add_core = 219
				add_core = 220
				add_core = 221
				add_core = 222
				add_core = 223
				add_core = 224
				add_core = 225
				add_core = 226
				add_core = 1259
				add_core = 229
				add_core = 230
				add_core = 1920
			}
			capital = 225	#Cordoba
			225 = { base_tax = 1 multiply_citysize = 1.10 }
			prestige = 0.1
			change_tag = CDB
			badboy = 2
		}
		ai_will_do = {
			factor = 1
		}
	}
}
