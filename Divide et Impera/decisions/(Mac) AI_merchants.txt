country_decisions = {


send_merchants1 = {
		potential = {
			ai = yes
			merchants = 5
			mercantilism_freetrade = 3
			NOT = { badboy = 0.4 } 
		}
		allow = {
			OR = {
				AND = {
					NOT = { num_of_cities = 5 }
					treasury = 50
					}
				AND = {
					num_of_cities = 5 
					NOT = { num_of_cities = 10 }
					treasury = 100
					}
				AND = {
					num_of_cities = 10 
					treasury = 150
					}
				}			
		}
		effect = {
			random_center_of_trade = {
				limit = {
				cot_value = 1500
						NOT = { placed_merchants = 5 }
				has_discovered = THIS
				owner = {  
					NOT = { trade_embargo_by = THIS }
					open_market = THIS
					}
				}
				send_merchant = THIS
				merchants = -1
				treasury = -4
			}
			random_center_of_trade = {
				limit = {
				cot_value = 1000
						NOT = { placed_merchants = 5 }
				has_discovered = THIS
				owner = {  
					NOT = { trade_embargo_by = THIS }
					open_market = THIS
					}
				}
				send_merchant = THIS
				merchants = -1
				treasury = -4
			}
			random_center_of_trade = {
				limit = {
				cot_value = 700
						NOT = { placed_merchants = 5 }
				has_discovered = THIS
				owner = {  
					NOT = { trade_embargo_by = THIS }
					open_market = THIS
					}
				}
				send_merchant = THIS
				merchants = -1
				treasury = -4
			}
			random_center_of_trade = {
				limit = {
				cot_value = 400
						NOT = { placed_merchants = 5 }
				has_discovered = THIS
				owner = {  
					NOT = { trade_embargo_by = THIS }
					open_market = THIS
					}
				}
				send_merchant = THIS
				merchants = -1
				treasury = -4
			}
			random_center_of_trade = {
				limit = {
				cot_value = 500
				owner = { trade_tech = 15 }
				owned_by = THIS
				}
				send_merchant = THIS
				merchants = -1
				treasury = -5
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
send_merchants2 = {
		potential = {
			ai = yes
			merchants = 5
			NOT = { mercantilism_freetrade = 3 }
			mercantilism_freetrade = -2
		}
		allow = {
			OR = {
				AND = {
					NOT = { num_of_cities = 5 }
					treasury = 50
					}
				AND = {
					num_of_cities = 5 
					NOT = { num_of_cities = 10 }
					treasury = 100
					}
				AND = {
					num_of_cities = 10 
					treasury = 150
					}
				}			
		}
		effect = {
			random_center_of_trade = {
				limit = {
				cot_value = 800
						NOT = { placed_merchants = 5 }
				owned_by = THIS
				}
				send_merchant = THIS
				merchants = -1
				treasury = -6
			}
			random_center_of_trade = {
				limit = {
					cot_value = 500
					NOT = { placed_merchants = 5 }
					owner = { NOT = { trade_tech = 15 } }
					owned_by = THIS
				}
				send_merchant = THIS
				merchants = -1
				treasury = -6
			}
			random_center_of_trade = {
				limit = {
				OR = { 
					NOT = { placed_merchants = 6 }
					owner = {trade_tech = 15}
					}
				owned_by = THIS
				}
				send_merchant = THIS
				merchants = -1
				treasury = -6
			}
			random_center_of_trade = {
				limit = {
				THIS = { NOT = { badboy = 0.2 } }
				cot_value = 1000
				NOT = { placed_merchants = 5 }
				has_discovered = THIS
				owner = {  
					NOT = { trade_embargo_by = THIS }
					open_market = THIS
					}
				}
				send_merchant = THIS
				merchants = -1
				treasury = -10
			}
			random_center_of_trade = {
				limit = {
				THIS = { NOT = { badboy = 0.2 } }
				cot_value = 700
				NOT = { placed_merchants = 5 }
				has_discovered = THIS
				owner = {  
					NOT = { trade_embargo_by = THIS }
					open_market = THIS
					}
				}
				send_merchant = THIS
				merchants = -1
				treasury = -10
			}
			random_center_of_trade = {
				limit = {
				THIS = { NOT = { badboy = 0.2 } }
				cot_value = 300
				NOT = { placed_merchants = 5 }
				has_discovered = THIS
				owner = {  
					NOT = { trade_embargo_by = THIS }								open_market = THIS
					}
				}
				send_merchant = THIS
				merchants = -1
				treasury = -10
			}

		}
		ai_will_do = {
			factor = 1
		}
	}
send_merchants3 = {
		potential = {
			ai = yes
			merchants = 5
			trade_tech = 15 
			NOT = { mercantilism_freetrade = -2 }
		}
		allow = {
			OR = {
				AND = {
					NOT = { num_of_cities = 5 }
					treasury = 100
					}
				AND = {
					num_of_cities = 5 
					NOT = { num_of_cities = 10 }
					treasury = 150
					}
				AND = {
					num_of_cities = 10 
					treasury = 200
					}
				}			
		}
		effect = {
			random_center_of_trade = {
				limit = {
				NOT = { placed_merchants = 6 }
				owned_by = THIS
				}
				send_merchant = THIS
				merchants = -1
				treasury = -8
			}
			random_center_of_trade = {
				limit = {
				NOT = { placed_merchants = 6 }
				owned_by = THIS
				}
				send_merchant = THIS
				merchants = -1
				treasury = -8
			}
			random_center_of_trade = {
				limit = {
				owned_by = THIS
				}
				send_merchant = THIS
				merchants = -1
				treasury = -8
			}
			random_center_of_trade = {
				limit = {
				owned_by = THIS
				}
				send_merchant = THIS
				merchants = -1
				treasury = -8
			}
			random_center_of_trade = {
				limit = {
				owned_by = THIS
				}
				send_merchant = THIS
				merchants = -1
				treasury = -8
			}

		}
		ai_will_do = {
			factor = 1
		}
	}


}

