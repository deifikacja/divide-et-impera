country_decisions = {

	aztec_nation = {
		potential = {
			NOT = { exists = AZT }
			ADM = 5
			DIP = 5
			MIL = 5
			OR = {
				culture_group = uto-aztecan
				culture_group = central_american
				culture_group = purepechan
			}
		}
		allow = {
			owns = 852			
			owns = 2077			
			owns = 2078
			diplomats = 2
			merchants = 2
            OR = {
				ADM = 6
				DIP = 6
				MIL = 6 
			}
			war = no
		}
	  effect = {
			AZT = {
			add_core = 852			
			add_core = 2077			
			add_core = 2078
			add_core = 1942
			add_core = 2080
			}		
			centralization_decentralization = -1
			offensive_defensive = -1
			capital_scope = {
				multiply_citysize = 1.15
				base_tax = 1
				change_manpower = 1
			}
			prestige = 0.05
			change_tag = AZT
			define_advisor = { type = statesman skill = 5 }
			diplomats = -2
			merchants = -2
			badboy = 2 
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	master_of_mexico = {
		potential = {
			tag = AZT
			MIL = 8
            NOT = { has_country_flag = master_of_mexico }
		}
		allow = {
			stability = 2
			treasury = 101
			owns = 852			
			owns = 2077			
			owns = 2078
			owns = 1942
			owns = 2080
		}
		effect = {
			set_country_flag = master_of_mexico
			add_country_modifier = {
				name = "morale_boost"
				duration = 2500
			}
			add_country_modifier = {
				name = "commandant_boost"
				duration = 2500
			}
			add_core = 2073
			add_core = 853
			add_core = 858
			add_core = 2081
			add_core = 2078
			add_core = 848	
			quality_quantity = -1
			offensive_defensive = -1
			capital_scope = { add_building = regimental_camp }
			land_tech = +0.25
			define_advisor = { type = army_organiser skill = 6 }
			badboy = 2
			treasury = -100
			stability = -1
		}
		ai_will_do = {
			factor = 1
		}
	}
	
}