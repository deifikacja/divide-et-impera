province_decisions = {

	colonization_focus_1 = {
		potential = {
			is_colony = yes
		}
		
		allow = {
			NOT = { has_province_modifier = colonization_focus_1 }
			NOT = { has_province_modifier = colonization_focus_2 }
			NOT = { has_province_modifier = colonization_focus_3 }
			NOT = { has_province_modifier = colonization_trade_point }
			owner = {
				diplomats = 1
				colonists = 1
				treasury = 60
			}
		}
		
		effect = {
			add_province_modifier = {
				name = "colonization_focus_1"
				duration = -1
			}
			owner = {
				treasury = -50
				diplomats = -1
				colonists = -1
			}
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	
	colonization_focus_2 = {
		potential = {
			is_colony = yes
		}
		
		allow = {
			has_province_modifier = colonization_focus_1
			NOT = { has_province_modifier = colonization_focus_2 }
			any_neighbor_province = { owned_by = THIS }
			owner = {
				diplomats = 1
				colonists = 1
				treasury = 80
			}
		}
		
		effect = {
			remove_province_modifier = colonization_focus_1
			add_province_modifier = {
				name = "colonization_focus_2"
				duration = -1
			}
			owner = {
				treasury = -50
				diplomats = -1
				colonists = -1
			}
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	
	colonization_focus_3 = {
		potential = {
			is_colony = yes
		}
		
		allow = {
			has_province_modifier = colonization_focus_2
			any_neighbor_province = { 
				owned_by = THIS
				is_colony = no
			}
			owner = {
				diplomats = 1
				colonists = 1
				treasury = 100
			}
		}
		
		effect = {
			remove_province_modifier = colonization_focus_2
			add_province_modifier = {
				name = "colonization_focus_3"
				duration = -1
			}
			owner = {
				treasury = -50
				diplomats = -1
				colonists = -1
			}
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	
	colonization_trade_point = {
		potential = {
			is_colony = yes
			NOT = { has_province_modifier = colonization_focus_1 }
			NOT = { has_province_modifier = colonization_focus_2 }
			NOT = { has_province_modifier = colonization_focus_3 }
		}
		
		allow = {
			NOT = { has_province_modifier = colonization_trade_point }
			NOT = { citysize = 200 }
			owner = {
				diplomats = 1
				treasury = 20
			}
		}
		
		effect = {
			add_province_modifier = {
				name = "colonization_trade_point"
				duration = -1
			}
			owner = {
				treasury = -10
				diplomats = -1
			}
		}
		
		ai_will_do = {
			factor = 1
			
			modifier= {
				NOT = { continent = africa }
				NOT = { region = amazonas }
				NOT = { region = gujana_region }
				factor = 0 
			}
			
			modifier= {
				continent = africa
				region = cape_colony
				factor = 0 
			}
		}
	}
	
	send_people_to_colony = {
		potential = {
			owner = {
				any_owned_province = {
					is_colony = yes
				}
			}			
			is_overseas = no
			citysize = 10000
		}
		
		allow = {
			citysize = 15000
			owner = {
				treasury = 25
				colonists = 1
				NOT = { has_country_modifier = dei_colonial_policy }
			}
		}
		
		effect = {
			citysize = -200
			owner = {
				treasury = -25
				add_country_modifier = {
					name = "dei_colonial_policy"
					duration = -1
				}
				colonists = -1
			}
		}
		
		ai_will_do = {
			factor = 1
			
			modifier = {
				NOT  = {
					owner = {
						any_owned_province = {
							has_province_modifier = colonization_focus_3
						}
					}
				}
				factor = 0.0
			}
		}
	}

	
	colonization_focus_1_cancel = {
		potential = {
			is_colony = yes
			has_province_modifier = colonization_focus_1
		}
		
		allow = {
			owner = {
				diplomats = 1
			}
		}
		
		effect = {
			remove_province_modifier = colonization_focus_1
			owner = {
				diplomats = -1
			}
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	
	colonization_focus_2_cancel = {
		potential = {
			is_colony = yes
			has_province_modifier = colonization_focus_2
		}
		
		allow = {
			owner = {
				diplomats = 1
			}
		}
		
		effect = {
			remove_province_modifier = colonization_focus_2
			owner = {
				diplomats = -1
			}
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	
	colonization_focus_3_cancel = {
		potential = {
			is_colony = yes
			has_province_modifier = colonization_focus_3
		}
		
		allow = {
			owner = {
				diplomats = 1
			}

		}
		
		effect = {
			remove_province_modifier = colonization_focus_3
			owner = {
				diplomats = -1
			}
		}
		
		ai_will_do = {
			factor = 1
		}
	}
	
	colonization_trade_point_cancel = {
		potential = {
			is_colony = yes
			has_province_modifier = colonization_trade_point
		}
		
		allow = {
			owner = {
				diplomats = -1
			}
		}
		
		effect = {
			remove_province_modifier = colonization_trade_point
			owner = {
				diplomats = -1
			}
		}
		
		ai_will_do = {
			factor = 1
		}
	}
}