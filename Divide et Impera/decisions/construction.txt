province_decisions = {

	rebuild_royal_palace = {
		potential = {
			owner = {
				government = monarchy
				government_tech = 30
			}
			has_building = royal_palace
			not = { has_province_modifier = great_royal_palace }
			is_capital = yes
		}
		allow = {
			OR = { 
				owner = { advisor = architect }
				owner = { advisor = artist }
			}
			owner = { treasury = 150 officials = 2 }
		}
		effect = {
			owner = {
				treasury = -150
				prestige = 0.05
				officials = -2
			}
			add_province_modifier = {
				name = "great_royal_palace"
				duration = 36500
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				NOT = { owner = { treasury = 250 } }
			}
		}
	}
}
