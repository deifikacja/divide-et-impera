country_decisions = {


	medium_taxes_from_low = {
		potential = {
			OR = { 
				has_country_modifier = low_taxes_tribal_group
				has_country_modifier = low_taxes_eastern_group
				has_country_modifier = low_taxes_feudal_group
				has_country_modifier = low_taxes_renaissance_group
				has_country_modifier = low_taxes_enlightenment_group
			}
		}
		allow = {
			OR = { 
				has_country_modifier = low_taxes_tribal_group
				has_country_modifier = low_taxes_eastern_group
				has_country_modifier = low_taxes_feudal_group
				has_country_modifier = low_taxes_renaissance_group
				has_country_modifier = low_taxes_enlightenment_group
			}
			NOT = { has_country_modifier = dei_upper_taxes }
			NOT = { has_country_modifier = dei_lower_taxes }
		}
		effect = {
			remove_country_modifier = low_taxes_tribal_group
			remove_country_modifier = low_taxes_eastern_group
			remove_country_modifier = low_taxes_feudal_group
			remove_country_modifier = low_taxes_renaissance_group
			remove_country_modifier = low_taxes_enlightenment_group
			add_country_modifier = {
				name = "dei_upper_taxes"
				duration = 365
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	medium_taxes_from_high = {
		potential = {
			OR = { 
				has_country_modifier = high_taxes_tribal_group
				has_country_modifier = high_taxes_eastern_group
				has_country_modifier = high_taxes_feudal_group
				has_country_modifier = high_taxes_renaissance_group
				has_country_modifier = high_taxes_enlightenment_group
			}
		}
		allow = {
			OR = { 
				has_country_modifier = high_taxes_tribal_group
				has_country_modifier = high_taxes_eastern_group
				has_country_modifier = high_taxes_feudal_group
				has_country_modifier = high_taxes_renaissance_group
				has_country_modifier = high_taxes_enlightenment_group
			}
			NOT = { has_country_modifier = dei_upper_taxes }
			NOT = { has_country_modifier = dei_lower_taxes }
		}
		effect = {
			remove_country_modifier = high_taxes_tribal_group
			remove_country_modifier = high_taxes_eastern_group
			remove_country_modifier = high_taxes_feudal_group
			remove_country_modifier = high_taxes_renaissance_group
			remove_country_modifier = high_taxes_enlightenment_group
			add_country_modifier = {
				name = "dei_lower_taxes"
				duration = 365
			}
		}
		ai_will_do = {
			factor = 0
		}
	}


	low_taxes_tribal_group = {
		potential = {
			OR = {
				government = tribal_despotism
				government = tribal_federation
				government = tribal_democracy
				government = eastern_despotism
				government = federation
			}
			NOT = { 
				OR = { 
					has_country_modifier = high_taxes_tribal_group
					has_country_modifier = high_taxes_eastern_group
					has_country_modifier = high_taxes_feudal_group
					has_country_modifier = high_taxes_renaissance_group
					has_country_modifier = high_taxes_enlightenment_group
				}
			}
			NOT = { 
				OR = {
					has_country_modifier = low_taxes_tribal_group
					has_country_modifier = low_taxes_eastern_group
					has_country_modifier = low_taxes_feudal_group
					has_country_modifier = low_taxes_renaissance_group
					has_country_modifier = low_taxes_enlightenment_group
				}
			}
		}
		allow = {
			NOT = { has_country_modifier = dei_upper_taxes }
			NOT = { has_country_modifier = dei_lower_taxes }
		}
		effect = {
			add_country_modifier = {
				name = "low_taxes_tribal_group"
				duration = -1
			}
			add_country_modifier = {
				name = "dei_lower_taxes"
				duration = 365
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	high_taxes_tribal_group = {
		potential = {
			OR = {
				government = tribal_despotism
				government = tribal_federation
				government = tribal_democracy
				government = eastern_despotism
				government = federation
			}
			NOT = { 
				OR = { 
					has_country_modifier = high_taxes_tribal_group
					has_country_modifier = high_taxes_eastern_group
					has_country_modifier = high_taxes_feudal_group
					has_country_modifier = high_taxes_renaissance_group
					has_country_modifier = high_taxes_enlightenment_group
				}
			}
			NOT = { 
				OR = {
					has_country_modifier = low_taxes_tribal_group
					has_country_modifier = low_taxes_eastern_group
					has_country_modifier = low_taxes_feudal_group
					has_country_modifier = low_taxes_renaissance_group
					has_country_modifier = low_taxes_enlightenment_group
				}
			}
		}
		allow = {
			NOT = { has_country_modifier = dei_upper_taxes }
			NOT = { has_country_modifier = dei_lower_taxes }
		}
		effect = {
			add_country_modifier = {
				name = "high_taxes_tribal_group"
				duration = -1
			}
			add_country_modifier = {
				name = "dei_upper_taxes"
				duration = 365
			}
		}
		ai_will_do = {
			factor = 0
		}
	}


	low_taxes_eastern_group = {
		potential = {
			OR = {
				government = caste_monarchy
				government = oligarchical_monarchy
				government = confessional_state
				government = military_despotism
				government = religion_federation
				government = military_federation
				government = daimyo
				government = despotic_monarchy
			}
			NOT = { 
				OR = { 
					has_country_modifier = high_taxes_tribal_group
					has_country_modifier = high_taxes_eastern_group
					has_country_modifier = high_taxes_feudal_group
					has_country_modifier = high_taxes_renaissance_group
					has_country_modifier = high_taxes_enlightenment_group
				}
			}
			NOT = { 
				OR = {
					has_country_modifier = low_taxes_tribal_group
					has_country_modifier = low_taxes_eastern_group
					has_country_modifier = low_taxes_feudal_group
					has_country_modifier = low_taxes_renaissance_group
					has_country_modifier = low_taxes_enlightenment_group
				}
			}
		}
		allow = {
			NOT = { has_country_modifier = dei_upper_taxes }
			NOT = { has_country_modifier = dei_lower_taxes }
		}
		effect = {
			add_country_modifier = {
				name = "low_taxes_eastern_group"
				duration = -1
			}
			add_country_modifier = {
				name = "dei_lower_taxes"
				duration = 365
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	high_taxes_eastern_group = {
		potential = {
			OR = {
				government = caste_monarchy
				government = oligarchical_monarchy
				government = confessional_state
				government = military_despotism
				government = religion_federation
				government = military_federation
				government = daimyo
				government = despotic_monarchy
			}
			NOT = { 
				OR = { 
					has_country_modifier = high_taxes_tribal_group
					has_country_modifier = high_taxes_eastern_group
					has_country_modifier = high_taxes_feudal_group
					has_country_modifier = high_taxes_renaissance_group
					has_country_modifier = high_taxes_enlightenment_group
				}
			}
			NOT = { 
				OR = {
					has_country_modifier = low_taxes_tribal_group
					has_country_modifier = low_taxes_eastern_group
					has_country_modifier = low_taxes_feudal_group
					has_country_modifier = low_taxes_renaissance_group
					has_country_modifier = low_taxes_enlightenment_group
				}
			}
		}
		allow = {
			NOT = { has_country_modifier = dei_upper_taxes }
			NOT = { has_country_modifier = dei_lower_taxes }
		}
		effect = {
			add_country_modifier = {
				name = "high_taxes_eastern_group"
				duration = -1
			}
			add_country_modifier = {
				name = "dei_upper_taxes"
				duration = 365
			}
		}
		ai_will_do = {
			factor = 0
		}
	}


	low_taxes_feudal_group = {
		potential = {
			OR = {
				government = feudal_monarchy
				government = merchant_republic
				government = major_merchant_republic
				government = imperial_government
				government = noble_republic
				government = military_order
				government = theocratic_government
				government = papal_government
				government = eastern_bureaucracy
				government = shogunate
			}
			NOT = { 
				OR = { 
					has_country_modifier = high_taxes_tribal_group
					has_country_modifier = high_taxes_eastern_group
					has_country_modifier = high_taxes_feudal_group
					has_country_modifier = high_taxes_renaissance_group
					has_country_modifier = high_taxes_enlightenment_group
				}
			}
			NOT = { 
				OR = {
					has_country_modifier = low_taxes_tribal_group
					has_country_modifier = low_taxes_eastern_group
					has_country_modifier = low_taxes_feudal_group
					has_country_modifier = low_taxes_renaissance_group
					has_country_modifier = low_taxes_enlightenment_group
				}
			}
		}
		allow = {
			NOT = { has_country_modifier = dei_upper_taxes }
			NOT = { has_country_modifier = dei_lower_taxes }
		}
		effect = {
			add_country_modifier = {
				name = "low_taxes_feudal_group"
				duration = -1
			}
			add_country_modifier = {
				name = "dei_lower_taxes"
				duration = 365
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	high_taxes_feudal_group = {
		potential = {
			OR = {
				government = feudal_monarchy
				government = merchant_republic
				government = major_merchant_republic
				government = imperial_government
				government = noble_republic
				government = military_order
				government = theocratic_government
				government = papal_government
				government = eastern_bureaucracy
				government = shogunate
			}
			NOT = { 
				OR = { 
					has_country_modifier = high_taxes_tribal_group
					has_country_modifier = high_taxes_eastern_group
					has_country_modifier = high_taxes_feudal_group
					has_country_modifier = high_taxes_renaissance_group
					has_country_modifier = high_taxes_enlightenment_group
				}
			}
			NOT = { 
				OR = {
					has_country_modifier = low_taxes_tribal_group
					has_country_modifier = low_taxes_eastern_group
					has_country_modifier = low_taxes_feudal_group
					has_country_modifier = low_taxes_renaissance_group
					has_country_modifier = low_taxes_enlightenment_group
				}
			}
		}
		allow = {
			NOT = { has_country_modifier = dei_upper_taxes }
			NOT = { has_country_modifier = dei_lower_taxes }
		}
		effect = {
			add_country_modifier = {
				name = "high_taxes_feudal_group"
				duration = -1
			}
			add_country_modifier = {
				name = "dei_upper_taxes"
				duration = 365
			}
		}
		ai_will_do = {
			factor = 0
		}
	}


	low_taxes_renaissance_group = {
		potential = {
			OR = {
				government = administrative_monarchy
				government = administrative_republic
				government = absolute_monarchy
				government = republican_dictatorship
				government = eastern_bureaucratic_empire
			}
			NOT = { 
				OR = { 
					has_country_modifier = high_taxes_tribal_group
					has_country_modifier = high_taxes_eastern_group
					has_country_modifier = high_taxes_feudal_group
					has_country_modifier = high_taxes_renaissance_group
					has_country_modifier = high_taxes_enlightenment_group
				}
			}
			NOT = { 
				OR = {
					has_country_modifier = low_taxes_tribal_group
					has_country_modifier = low_taxes_eastern_group
					has_country_modifier = low_taxes_feudal_group
					has_country_modifier = low_taxes_renaissance_group
					has_country_modifier = low_taxes_enlightenment_group
				}
			}
		}
		allow = {
			NOT = { has_country_modifier = dei_upper_taxes }
			NOT = { has_country_modifier = dei_lower_taxes }
		}
		effect = {
			add_country_modifier = {
				name = "low_taxes_renaissance_group"
				duration = -1
			}
			add_country_modifier = {
				name = "dei_lower_taxes"
				duration = 365
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	high_taxes_renaissance_group = {
		potential = {
			OR = {
				government = administrative_monarchy
				government = administrative_republic
				government = absolute_monarchy
				government = republican_dictatorship
				government = eastern_bureaucratic_empire
			}
			NOT = { 
				OR = { 
					has_country_modifier = high_taxes_tribal_group
					has_country_modifier = high_taxes_eastern_group
					has_country_modifier = high_taxes_feudal_group
					has_country_modifier = high_taxes_renaissance_group
					has_country_modifier = high_taxes_enlightenment_group
				}
			}
			NOT = { 
				OR = {
					has_country_modifier = low_taxes_tribal_group
					has_country_modifier = low_taxes_eastern_group
					has_country_modifier = low_taxes_feudal_group
					has_country_modifier = low_taxes_renaissance_group
					has_country_modifier = low_taxes_enlightenment_group
				}
			}
		}
		allow = {
			NOT = { has_country_modifier = dei_upper_taxes }
			NOT = { has_country_modifier = dei_lower_taxes }
		}
		effect = {
			add_country_modifier = {
				name = "high_taxes_renaissance_group"
				duration = -1
			}
			add_country_modifier = {
				name = "dei_upper_taxes"
				duration = 365
			}
		}
		ai_will_do = {
			factor = 0
		}
	}


	low_taxes_enlightenment_group = {
		potential = {
			OR = {
				government = constitutional_monarchy
				government = constitutional_republic
				government = enlightened_despotism
				government = bureaucratic_despotism
				government = revolutionary_republic
				government = revolutionary_empire
			}
			NOT = { 
				OR = { 
					has_country_modifier = high_taxes_tribal_group
					has_country_modifier = high_taxes_eastern_group
					has_country_modifier = high_taxes_feudal_group
					has_country_modifier = high_taxes_renaissance_group
					has_country_modifier = high_taxes_enlightenment_group
				}
			}
			NOT = { 
				OR = {
					has_country_modifier = low_taxes_tribal_group
					has_country_modifier = low_taxes_eastern_group
					has_country_modifier = low_taxes_feudal_group
					has_country_modifier = low_taxes_renaissance_group
					has_country_modifier = low_taxes_enlightenment_group
				}
			}
		}
		allow = {
			NOT = { has_country_modifier = dei_upper_taxes }
			NOT = { has_country_modifier = dei_lower_taxes }
		}
		effect = {
			add_country_modifier = {
				name = "low_taxes_enlightenment_group"
				duration = -1
			}
			add_country_modifier = {
				name = "dei_lower_taxes"
				duration = 365
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

	high_taxes_enlightenment_group = {
		potential = {
			OR = {
				government = constitutional_monarchy
				government = constitutional_republic
				government = enlightened_despotism
				government = bureaucratic_despotism
				government = revolutionary_republic
				government = revolutionary_empire
			}
			NOT = { 
				OR = { 
					has_country_modifier = high_taxes_tribal_group
					has_country_modifier = high_taxes_eastern_group
					has_country_modifier = high_taxes_feudal_group
					has_country_modifier = high_taxes_renaissance_group
					has_country_modifier = high_taxes_enlightenment_group
				}
			}
			NOT = { 
				OR = {
					has_country_modifier = low_taxes_tribal_group
					has_country_modifier = low_taxes_eastern_group
					has_country_modifier = low_taxes_feudal_group
					has_country_modifier = low_taxes_renaissance_group
					has_country_modifier = low_taxes_enlightenment_group
				}
			}
		}
		allow = {
			NOT = { has_country_modifier = dei_upper_taxes }
			NOT = { has_country_modifier = dei_lower_taxes }
		}
		effect = {
			add_country_modifier = {
				name = "high_taxes_enlightenment_group"
				duration = -1
			}
			add_country_modifier = {
				name = "dei_upper_taxes"
				duration = 365
			}
		}
		ai_will_do = {
			factor = 0
		}
	}

}