country_decisions = {

	plan_of_partition = {
		potential = {
			num_of_cities = 4
			any_neighbor_country = {
				num_of_cities = 4
				NOT = { truce_with = THIS }
				any_neighbor_country = {
					num_of_cities = 4
					alliance_with = THIS
					relation = { who = THIS value = 100 }
				}
			}
		}
		
		allow = {
			DIP = 6
			diplomats = 3
			prestige = 0.1
			war = no
		}
		
		effect = {
			diplomats = -3
			add_country_modifier = {
				name = "partition_offered"
				duration = 5
			}
			random_country = {
				limit = {
					num_of_cities = 4
					NOT = { truce_with = THIS }
					any_neighbor_country = { has_country_modifier = partition_offered }
					any_neighbor_country = { 
						alliance_with = THIS
						num_of_cities = 4
					}
				}
				add_country_modifier = {
					name = "partitioned_country"
					duration = 5
				}
			}
			any_country = {
				limit = {
					any_neighbor_country = { has_country_modifier = partitioned_country }
					num_of_cities = 4
					alliance_with = THIS
					relation = { who = THIS value = 100 }
				}
				country_event = 707001
			}
		}
		
		ai_will_do = {
			factor = 1
			modifier =  {
				factor = 0.0
				NOT = { stability = 2 }
			}
			modifier =  {
				factor = 0.0
				NOT = { MIL = 6 }
			}
			modifier =  {
				factor = 0.0
				war_exhaustion = 3
			}
			modifier =  {
				factor = 0.0
				badboy = 0.6
			}
			modifier =  {
				factor = 0.0
				NOT = { num_of_allies = 2 }
			}
			modifier =  {
				factor = 0.0
				any_known_country = {
					alliance_with = THIS
					ai = no
					num_of_cities = 25
				}
			}
			modifier =  {
				factor = 0.0
				is_subject = yes
			}
		}
	}
}

province_decisions = {
	
	offer_buying = {
		potential = {
			any_neighbor_province = {
				NOT = { owned_by = THIS }
				has_province_modifier = negotiations_up_our_heads
			}
			NOT = { owner = { has_country_flag = special_client } }
		}
		
		allow = { 
			owner = { diplomats = 1	}
		}
		
		effect = {
			owner = { 
				diplomats = -1 
				set_country_flag = special_client
			}
			random_neighbor_province = {
				limit = { 
					NOT = { owned_by = THIS }
					has_province_modifier = negotiations_up_our_heads
				}
				owner = { country_event = 707005 }
			}
		}
		
		ai_will_do = {
			factor = 0
		}
	}
}