religion_decisions = {

	reform_paganism = {
		potential = {
			OR = {
				religion = animism
				religion = shamanism
				religion = inti
				religion = teotl
			}
			any_known_country = {
				NOT = { religion_group = pagan }
			}
		}
		
		allow = {
			any_neighbor_country = {
				NOT = { religion_group = pagan }
			}
			NOT = { secularism_theocracy = 2 }
			NOT = { innovative_narrowminded = 1 }
			ADM = 6
			DIP = 6
			prestige = 0.1
		}
	
		effect = {
			religion = reformed_pagan
			capital_scope = { 
				religion = reformed_pagan 
				clr_province_flag = religion_animism
				clr_province_flag = religion_shamanism
				clr_province_flag = religion_inti
				clr_province_flag = religion_teotl
				set_province_flag = religion_reformed_pagan 
			}
			random_owned = {
				limit = { is_capital = no }
				religion = reformed_pagan
				clr_province_flag = religion_animism
				clr_province_flag = religion_shamanism
				clr_province_flag = religion_inti
				clr_province_flag = religion_teotl
				set_province_flag = religion_reformed_pagan
			}
			missionaries = 2
			stability = -5
			#random_owned = {
			#	heretic_rebels = 1
			#}
		}

		ai_will_do = {
			factor = 1
		}
	}
	
	establish_teotl = {
		potential = {
			religion = animism
			tag = AZT
		}
		
		allow = {
			secularism_theocracy = 0
			ADM = 6
			OR = { 
				advisor = theologian
				advisor = inquisitor
			}
		}
	
		effect = {
			religion = teotl
			capital_scope = { 
				religion = teotl 
				clr_province_flag = religion_animism
				set_province_flag = religion_reformed_pagan 
			}
			random_owned = {
				limit = { is_capital = no }
				religion = teotl
				clr_province_flag = religion_animism
				set_province_flag = religion_reformed_pagan 
			}
			random_owned = {
				limit = { is_capital = no }
				religion = teotl
				clr_province_flag = religion_animism
				set_province_flag = religion_reformed_pagan 
			}
			stability = -2
			centralization_decentralization = -1
			add_country_modifier = {
				name = "morale_boost"
				duration = 7300
			}
			create_general = 90
		}

		ai_will_do = {
			factor = 1
		}
	}
	
}