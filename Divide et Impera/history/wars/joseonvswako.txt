name = "Joseon-So war"
casus_belli = cb_conquest

1419.6.9 = {
	add_attacker = KOR
	add_defender = SOC
}

1419.6.20 = {
	battle = {
		name = "Nukadake"
		location = 1362
		attacker = {
			commander = "Yi Jong-mu"
			infantry = 2000
			cavalry = 1000
			losses = 65	# percent
			country = KOR
		}
		defender = {
			commander = "So Sadamori"
			infantry = 1000
			cavalry = 500
			losses = 20	# percent
			country = SOC
		}		
		result = loss
	}
}

# Treaty signed
1419.9.29 = {
	rem_attacker = KOR
	rem_defender = SOC
}
