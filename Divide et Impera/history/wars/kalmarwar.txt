name = "Kalmar War"
casus_belli = cb_trade_war

1611.4.4 = {
	add_attacker = DAN
	add_defender = SWE
}

1612.8.26 = {
	battle = {
		name = "Kringen"
		location = 22
		attacker = {
			commander = "Lars Gram"	# Lars Gram
			infantry = 500
			losses = 3	# percent
			country = NOR
		}
		defender = {
			commander = "Alexander Ramsay"	# Alexander Ramsay
			infantry = 300
			losses = 92	# percent
			country = SWE
		}
		result = win
	}
}

# Treaty of Kn�red
1613.1.20 = {
	rem_attacker = DAN
	rem_defender = SWE
}
