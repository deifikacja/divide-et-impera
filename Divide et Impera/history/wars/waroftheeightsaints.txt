name = "War of the Eight Saints"
casus_belli = cb_sphere_of_influence

1375.1.1 = {
	add_attacker = PAP
	add_defender = SIE
	add_defender = TUS
	add_defender = MLO
}

#Peace and end of war
1378.7.1 = {
	rem_attacker = PAP
	rem_defender = SIE
	rem_defender = TUS
	rem_defender = MLO
}
