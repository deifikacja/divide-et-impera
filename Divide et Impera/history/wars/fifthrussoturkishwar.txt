name = "Fifth Russo-Turkish War"
casus_belli = cb_sphere_of_influence

1806.1.1 = {
	add_attacker = RUS
	add_defender = TUR
	add_defender = WAL
	add_defender = MOL
}

# Treaty of Bucharest
1812.5.28 = {
	rem_attacker = RUS
	rem_defender = TUR
	rem_defender = WAL
	rem_defender = MOL
}
