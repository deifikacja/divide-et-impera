name = "Old Zurich War"
casus_belli = cb_conquest

1440.11.2 = {
	add_attacker = SWI
	add_defender = ZUR
	add_defender = HAB
	add_defender = FRA
}

# Peace of Einsiedeln
1450.7.13 = {
	rem_attacker = SWI
	rem_defender = ZUR
	rem_defender = HAB
	rem_defender = FRA
}
