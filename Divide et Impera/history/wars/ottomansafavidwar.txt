name = "Ottoman-Safavid War"
casus_belli = cb_conquest

1514.1.1 = {
	add_attacker = PER
	add_defender = TUR
}

1514.8.23 = {
	battle = {
		name = "Chaldiran"
		location = 414
		attacker = {
			commander = "Ismail I"	# Ismail I
			infantry = 122000
			cavalry = 42500
			losses = 5	# percent
			country = PER
		}
		defender = {
			commander = "Selim I"	# Selim I
			infantry = 46300
			cavalry = 25000
			artillery = 220
			losses = 45	# percent
			country = TUR
		}
		result = loss
	}
}

# Peace
1516.1.1 = {
	rem_attacker = PER
	rem_defender = TUR
}
