name = "War of the Austrian Succession"
casus_belli = cb_claim_throne

1740.12.16 = {
	add_attacker = HAB
	add_defender = PRU
}

1740.12.16 = {
	add_defender = BAV
}

# Numphenburg alliance
1741.1.1 = {
	add_defender = SPA
	add_defender = FRA
}

1741.4.10 = {
	battle = {
		name = "Mollwitz"
		location = 264
		attacker = {
			commander = "Reinhard von Neipperg"	#
			infantry = 10000
			cavalry = 6000
			losses = 10	# percent
			country = HAB
		}
		defender = {
			commander = "Frederick the Great"	# Frederick the Great & Kurt Christoph Graf von Schwerin
			infantry = 15000
			cavalry = 7000
			losses = 5	# percent	
			country = PRU
		}
		result = loss
	}
}

# Joined by Saxony
1741.6.1 = {
	add_defender = SAX
}

# Treaty of Berlin
1742.7.28 = {
	rem_defender = PRU
}

1743.1.1 = {
	add_attacker = SAR
	rem_defender = SAX
	add_attacker = SAX
}

1743.6.16 = {
	battle = {
		name = "Dettingen"
		location = 68
		attacker = {
			commander = "George II"	# George II
			infantry = 35000
			cavalry = 15000
			losses = 1	# percent
			country = HAB
		}
		defender = {
			commander = "duc de Noailles"	# duc de Noailles
			infantry = 50000
			cavalry = 20000
			losses = 12	# percent
			country = FRA
		}
		result = win
	}
}

# The Second Silesian war
1744.1.1 = {
	add_defender = PRU
}

# Alliance between Austria, Saxony, Britain & Holland
1745.1.1 = {
	add_attacker = GBR
	add_attacker = NED
}

# Death of Charles VII
1745.1.20 = {
	rem_defender = BAV
}

1745.5.11 = {
	battle = {
		name = "Fontenoy"
		location = 88
		attacker = {
			commander = "William Augustus"	# William Augustus, Duke of Cumberland
			infantry = 33000
			cavalry = 15000
			losses = 6	# percent
			country = GBR
		}
		defender = {
			commander = "Moritz Graf von Sachsen"	# Moritz Graf von Sachsen
			infantry = 30000
			cavalry = 18000
			losses = 7	# percent
			country = FRA
		}
		result = loss
	}
}

# Treaty of Dresden
1745.12.25 = {
	rem_defender = PRU
	rem_attacker = SAX
}

# Treaty of Aix-la-Chapelle.
1748.10.18 = {
	rem_defender = SPA
	rem_defender = FRA
	rem_attacker = HAB
	rem_attacker = GBR
	rem_attacker = NED
	rem_attacker = SAR
}
