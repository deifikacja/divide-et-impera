#I'm pretty sure that I missed many participants, especially vassals
name = "Onin War"
casus_belli = cb_border_war

1467.5.20 = {
	add_attacker = OUC
	add_attacker = YAM
	add_defender = HOS
	add_defender = ASK
}

1477.12.16 = {
	rem_attacker = OUC
	rem_attacker = YAM
	rem_defender = HOS
	rem_defender = ASK
}
