name = "Chilean War of Independence"
casus_belli = cb_liberation

1810.9.18  = {
	add_attacker = CHL
	add_defender = SPA
}

1818.4.5 = {
	rem_attacker = CHL
	rem_defender = SPA
}
