name = "Swiss-Habsburg war 1385-88"	
casus_belli = cb_conquest

1385.1.1 = {
	add_attacker = HAB
	add_defender = SWI
}

1386.7.9 = {
	battle = {
		name = "Sempach"
		location = 166
		attacker = {
			commander = "Leopold III"
			infantry = 4000
			losses = 45	# percent
			country = HAB
		}
		defender = {
			commander = "Patermann von Gundoldingen"
			infantry = 1600
			losses = 16	# percent
			country = SWI
		}
		result = loss
	}
}

1388.4.9 = {
	battle = {
		name = "N�fels"
		location = 166
		attacker = {
			commander = "Donat von Toggenburg"
			infantry = 5000
			cavalry = 1000
			losses = 28	# percent
			country = HAB
		}
		defender = {
			commander = "Patermann von Gundoldingen"
			infantry = 650
			losses = 8	# percent
			country = SWI
		}
		result = loss
	}
}

#Truce
1388.9.1 = {
	rem_attacker = HAB
	rem_defender = SWI
}
