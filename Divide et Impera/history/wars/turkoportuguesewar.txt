name = "Portuguese-muslim War"
casus_belli = cb_conquest

1509.1.1 = {
	add_attacker = POR
	add_defender = TUR
	add_defender = MAM
	add_defender = GUJ
	add_defender = CLC
	add_defender = VEN
	add_defender = RAG
}

# Battle of Diu
1509.2.3 = {
	battle = {
		name = "Diu"
		location = 1838
		attacker = {
			commander = "Afonso de Albuquerque"
			big_ship = 10
			losses = 1
			country = POR
		}
		defender = {
			commander = "Amir Husain Al-Kurdi"
			galley = 20
			losses = 10
			country = GUJ
		}
		result = win
	}
}

# Portugal is now in the control of spice trade
1510.1.1 = {
	rem_attacker = POR
	rem_defender = TUR
	rem_defender = MAM
	rem_defender = GUJ
	rem_defender = CLC
	rem_defender = VEN
	rem_defender = RAG
}
