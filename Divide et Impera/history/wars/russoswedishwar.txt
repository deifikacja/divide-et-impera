name = "Russo-Swedish War"
casus_belli = cb_conquest

1554.1.1 = {
	add_attacker = SWE
	add_defender = RUS
}

1557.1.1 = {
	rem_attacker = SWE
	rem_defender = RUS
}
