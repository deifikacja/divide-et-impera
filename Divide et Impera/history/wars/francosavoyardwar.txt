name = "Franco-Savoyard War"
casus_belli = cb_core

1590.1.1 = {
	add_attacker = SAV
	add_defender = FRA
}

1593.1.1 = {
	rem_attacker = SAV
	rem_defender = FRA
}
