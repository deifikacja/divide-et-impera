name = "Delhi Mughal War"
casus_belli = cb_conquest

1525.1.1 = {
	add_attacker = MUG
	add_defender = DLH
}
1526.1.1 = {
	rem_attacker = MUG
	rem_defender = DLH
}
