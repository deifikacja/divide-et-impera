name = "Argentine War of Independence"
casus_belli = cb_liberation

1816.7.9  = {
	add_attacker = LAP
	add_defender = SPA
}

1817.2.12 = {
	battle = {
		name = "Chacabuco"
		location = 787
		attacker = {
			commander = "Jos� de San Martin"	# Jos� de San Martin
			infantry = 4000
			cavalry = 600
			losses = 2	# percent
			country = LAP
		}
		defender = {
			commander = "Rafael Maroto"	# Rafael Maroto
			infantry = 1500
			losses = 58	# percent
			country = SPA
		}
		result = win
	}
}

1818.4.5 = {
	battle = {
		name = "Maip�"
		location = 787
		attacker = {
			commander = "Jos� de San Martin"	# Jos� de San Martin
			infantry = 5000
			losses = 20	# percent
			country = LAP
		}
		defender = {
			commander = "Mariano Osorio"	# Mariano Osorio
			infantry = 5000
			losses = 90	# percent
			country = SPA
		}
		result = win
	}
}

1818.4.5 = {
	rem_attacker = LAP
	rem_defender = SPA
}
