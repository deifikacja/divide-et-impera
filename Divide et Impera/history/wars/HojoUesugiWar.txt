name = "Hojo-Uesugi War"
casus_belli = cb_border_war

1530.1.1 = {
	add_attacker = HOJ
	add_defender = UES
}

1552.1.1 = {
	rem_attacker = HOJ
	rem_defender = UES
}