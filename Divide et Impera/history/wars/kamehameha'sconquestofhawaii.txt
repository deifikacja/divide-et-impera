name = "Kamehameha's conquest of Hawaii"
casus_belli = cb_conquest

1795.1.1 = {
	add_attacker = HAW
	add_defender = MAU
}
1795.5.1 = {
	rem_attacker = HAW
	rem_defender = MAU
}
1796.1.1 = {
	add_attacker = HAW
	add_defender = KAU
}
1810.1.1 = {
	rem_attacker = HAW
	rem_defender = KAU
}