name = "Siamese invasion of Khmer"
casus_belli = cb_conquest

1352.1.1 = {
	add_attacker = AYU
	add_defender = KHM
}

1357.1.1 = {
	rem_attacker = AYU
	rem_defender = KHM
}
