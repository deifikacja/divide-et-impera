name = "Second Barbary War"
casus_belli = cb_insult

1815.3.3  = {
	add_attacker = ALG
	add_attacker = TRP
	add_defender = USA
}

# Treaty with Algeria
1816.12.23 = {
	rem_attacker = ALG
	rem_attacker = TRP
	rem_defender = USA
}
