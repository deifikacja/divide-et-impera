name = "Lithuanian-Tartar war"
casus_belli = cb_border_war

1398.10.1 = {
	add_attacker = LIT
	add_attacker = POL
	add_attacker = TEU
	add_defender = GOL
	add_defender = TIM
}

1399.8.12 = {
	battle = {
		name = "Worskla"
		location = 290
		attacker = {
			commander = Vytautas	# Vytautas
			infantry = 5000
			cavalry = 10000
			losses = 65	# percent
			country = LIT
		}
		defender = {
			commander = "Timur"	# Timur
			infantry = 0
			cavalry = 15000
			losses = 20	# percent
			country = TIM
		}
		result = loss
	}
}

# No further actions after the battle
1400.1.1 = {
	rem_attacker = LIT
	rem_attacker = POL
	rem_attacker = TEU
	rem_defender = GOL
	rem_defender = TIM
}
