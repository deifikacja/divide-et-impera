name = "Polish lithuanian war"
casus_belli = cb_sphere_of_influence

1340.1.1 = {
	add_attacker = LIT
	add_attacker = GOL
	add_attacker = BLZ
	add_attacker = BOH
	add_defender = POL
	add_defender = PSK
}
1348.1.1 = { 
	rem_attacker = BOH
	rem_attacker = GOL
	}
1355.1.1 = {
	rem_attacker = LIT
	rem_attacker = BLZ
	rem_defender = POL	
	rem_defender = PSK
}
