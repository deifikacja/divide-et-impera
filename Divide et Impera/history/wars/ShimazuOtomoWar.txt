name = "Shimazu-Otomo War"
casus_belli = cb_border_war

1575.1.1 = {
	add_attacker = SMZ
	add_defender = OTO
}

1587.5.8 = {
	rem_attacker = SMZ
	rem_defender = OTO
}
