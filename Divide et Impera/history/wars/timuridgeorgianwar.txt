name = "Timurid-Georgian War"
casus_belli = cb_conquest

1386.1.1 = {
	add_attacker = TIM
	add_defender = GEO
}
#Georgia vassalized
1387.1.1 = {
	rem_attacker = TIM
	rem_defender = GEO
}
