name = "Russo-Turkish War"
casus_belli = cb_conquest

1676.1.1 = {
	add_attacker = TUR
	add_attacker = CRI
	add_defender = RUS
}

1681.1.3 = {
	rem_attacker = TUR
	rem_attacker = CRI
	rem_defender = RUS
}
