name = "Sixth Siamese-Burmese War"
casus_belli = cb_conquest

1804.1.1 = {
	add_attacker = AYU
	add_defender = TAU
}

1804.12.1 = {
	rem_attacker = AYU
	rem_defender = TAU
}
