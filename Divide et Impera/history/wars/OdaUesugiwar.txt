name = "The Takeda - Tokugawa war"
casus_belli = cb_border_war

1577.1.1 = {
	add_attacker = UES
	add_defender = ODA
	add_defender = HAK
}

1577.1.2 = {
	battle = {
		name = "Tedorigawa"
		location = 1030
		attacker = {
			commander = "Uesugi Kenshin"
			infantry = 20000
			cavalry = 10000
			losses = 20	# percent
			country = UES
		}
		defender = {
			commander = "Shibata Katsuie"
			infantry = 30000
			cavalry = 20000
			losses = 10	# percent
			country = ODA
		}
		result = loss
	}
}

1582.6.21 = { add_defender = TKG rem_defender = ODA }#Oda is succeded by Iyeasu
# Uesugi are destroyed by Iyeasu
1586.1.1 = {
	rem_attacker = UES
	rem_defender = TKG
}
