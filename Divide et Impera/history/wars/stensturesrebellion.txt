name = "Sten Sture Rebellion"
casus_belli = cb_liberation

1501.8.1 = {
	add_attacker = SWE
	add_defender = DAN
}

1503.12.14 = {
	rem_attacker = SWE
	rem_defender = DAN
}
