name = "Kappel Wars"
casus_belli = cb_heretic

1529.1.1 = {
	add_attacker = ZUR
	add_defender = SWI
	add_defender = HAB
}
1531.10.11 = {
	add_attacker = BER
}
# Peace of Einsiedeln
1531.11.20 = {
	rem_attacker = BER
	rem_attacker = ZUR
	rem_defender = SWI
	rem_defender = HAB
}
