name = "Talikota War"
casus_belli = cb_conquest

1564.9.1 = {
	add_attacker = BAS
	add_attacker = BRR
	add_attacker = BIJ
	add_attacker = GOC
	add_attacker = BID
	add_defender = VIJ
}

# Battle of Talikota
1565.1.26 = {
	battle = {
		name = "Talikota"
		location = 532
		attacker = {
			commander = "Nizam Shah I"	# Nizam Shah I
			infantry = 80000
			cavalry = 30000
			losses = 30	# percent
			country = GOC
		}
		defender = {
			commander = "Rama Raya"	# Rama Raya
			infantry = 140000
			cavalry = 10000
			losses = 70	# percent
			country = VIJ
		}
		result = win
	}
}

1565.7.1 = {
	rem_attacker = BAS
	rem_attacker = BRR
	rem_attacker = BIJ
	rem_attacker = GOC
	rem_attacker = BID
	rem_defender = VIJ
}
