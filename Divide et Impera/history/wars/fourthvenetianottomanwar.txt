name = "Fourth Ottoman-Venetian War"
casus_belli = cb_conquest

1537.5.17 = {
	add_attacker = TUR
	add_defender = VEN
}

# The Holy League
1538.2.8 = {
	add_defender = SPA
	add_defender = GEN
	add_defender = PAP
	add_defender = KNI
}

# Battle of Preveza
1538.9.28 = { 
	battle = { 
		name = "Preveza" 
		location = 1312
		attacker = { 
			commander = "Barbarossa Hayreddin Pasha"			# Barbarossa Hayreddin Pasha
			galley = 122
			losses = 0			# percent 
			country = TUR
		} 
		defender = { 
			commander = "Andrea Doria"		# Andrea Doria
			galley = 302
			losses = 49			# percent 
			country = GEN
		} 
		result = win
	} 
} 

# Peace
1540.10.2 = {
	rem_attacker = TUR
	rem_defender = VEN
	rem_defender = SPA
	rem_defender = GEN
	rem_defender = PAP
	rem_defender = KNI
}
