name = "Ottoman-Byzantine War"
casus_belli = cb_conquest

1364.1.1 = {
	add_attacker = TUR
	add_defender = BYZ
	add_defender = SER
	add_defender = BUL
	add_defender = BOS
}

1365.7.12 = {
	battle = {
		name = "Battle of Adrianople"
		location = 149
		attacker = {
			commander = "Murad I"
			infantry = 10000
			cavalry = 8000
			losses = 10	# percent
			country = TUR
		}
		defender = {
			commander = "Ioannes V"
			infantry = 6000
			cavalry = 4000
			losses = 45	# percent
			country = BYZ
		}
		result = win
	}
}

1371.9.26 = {
	battle = {
		name = "Battle of Maritsa"
		location = 149
		attacker = {
			commander = "Lala Sahin Pasa"
			infantry = 7000
			cavalry = 3000
			losses = 8	# percent
			country = TUR
		}
		defender = {
			commander =  "Vukasin Mrnjavcevic"
			infantry = 30000
			cavalry = 10000
			losses = 30	# percent
			country = SER
		}
		result = win
	}
}

1373.1.1 = {
	rem_attacker = TUR
	rem_defender = BYZ
	rem_defender = SER
	rem_defender = BUL
	rem_defender = BOS
}
