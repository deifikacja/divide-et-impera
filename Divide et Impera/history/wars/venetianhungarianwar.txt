name = "Venetian - Hungarian War"
casus_belli = cb_conquest

1355.1.1 = {
	add_attacker = AQU
	add_attacker = GOR
	add_attacker = PAD
	add_attacker = HAB
	add_attacker = HUN
	add_defender = VEN
}

# Treaty of Zadar
1358.2.17 = {
	rem_attacker = AQU
	rem_attacker = GOR
	rem_attacker = PAD
	add_attacker = HAB
	rem_attacker = HUN
	rem_defender = VEN
}
