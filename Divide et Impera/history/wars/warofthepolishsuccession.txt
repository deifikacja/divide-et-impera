name = "War of the Polish Succession"
casus_belli = cb_claim_throne

1733.8.14 = {
	add_attacker = RZP
	add_defender = HAB
	add_defender = RUS
}

1733.10.10 = {
	add_attacker = FRA
	add_attacker = SAV
}

1733.11.7 = {
	add_attacker = SPA
}

# Peace of Vienna
1735.10.3 = {
	rem_attacker = RZP
	rem_attacker = FRA
	rem_attacker = SAV
	rem_attacker = SPA
	rem_defender = RUS
	rem_defender = HAB
}
