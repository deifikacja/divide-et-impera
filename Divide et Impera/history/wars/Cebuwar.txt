name = "The Padri war"
casus_belli = cb_conquest

1521.4.1 = {
	add_attacker = SPA
	add_attacker = CEB
	add_defender = MAC
}

1521.4.27 = {
	battle = {
		name = "Mactan"
		location = 326
		attacker = {
			commander = "Magellan"
			infantry = 1000
			cavalry = 0
			losses = 40	# percent
			country = SPA
		}
		defender = {
			commander = "Lapu Lapu"
			infantry = 1000
			cavalry = 0
			losses = 10	# percent
			country = CEB
		}
		result = win
	}
}

# Last point of resistance captured
1521.4.30 = {
	rem_attacker = SPA
	rem_attacker = CEB
	rem_defender = MAC
}
