name = "War of the Third Coalition"
casus_belli = cb_imperial

# Britain declares war on France
1803.5.18 = {
	add_attacker = GBR
	add_defender = RFR
	add_defender = NED
	add_defender = ETR
	add_defender = BAV
	add_defender = ITA
}

# The Convention of Artlenburg
1803.7.5 = {
	add_attacker = HAN
}

# Spain declares war on Britain
1804.12.12 = {
	add_defender = SPA
}

# Treaty of St. Petersburg, Russia joins
1805.4.11 = {
	add_attacker = RUS
}

# Austria joins the Coalition shortly after
1805.8.9 = {
	add_attacker = HAB
	add_attacker = NAP
}

1805.9.25 = {
	battle = {
		name = "Cape Finisterre"
		location = 1290
		attacker = {
			commander = "Robert Calder"	# Robert Calder
			big_ship = 15
			losses = 2	# percent
			country = GBR
		}
		defender = {
			commander = "Pierre de Villeneuve"	# Pierre de Villeneuve
			big_ship = 14
			losses = 3	# percent
			country = RFR
		}
		result = win
	}
}

1805.10.8 = {
	battle = {
		name = "Wertingen"
		location = 64
		attacker = {
			commander = "Franz Xavier Auffenberg"	# Franz Xavier Auffenberg
			infantry = 12000
			losses = 7	# percent
			country = HAB
		}
		defender = {
			commander = "Joachim Murat"	# Joachim Murat
			infantry = 5500
			losses = 2	# percent
			country = RFR
		}
		result = loss
	}
}

1805.10.11 = {
	battle = {
		name = "Haslach-Jungingen"
		location = 74
		attacker = {
			commander = "Karl Mack von Lieberich"	# Karl Mack von Lieberich
			infantry = 25000
			losses = 2	# percent
			country = HAB
		}
		defender = {
			commander = "Pierre Dupont de l'Etang"	# Pierre Dupont de l'Etang
			infantry = 6000
			losses = 16	# percent
			country = RFR
		}
		result = loss
	}
}

1805.10.16 = {
	battle = {
		name = "Ulm"
		location = 70
		attacker = {
			commander = "Karl Mack von Liebereich"	# Karl Mack von Liebereich
			infantry = 72000
			losses = 16	# percent
			country = HAB
		}
		defender = {
			commander = "Napol�on Bonaparte"	# Napol�on Bonaparte
			infantry = 150000
			losses = 4	# percent
			country = RFR
		}
		result = loss
	}
}

# W�rttemberg signs an alliance with France
1805.10.18 = {
	add_defender = WUR
}

1805.10.21 = {
	battle = {
		name = "Trafalgar"
		location = 1292
		attacker = {
			commander = "Horatio Nelson" 	# Horatio Nelson
			big_ship = 27
			losses = 2	# percent
			country = GBR
		}
		defender = {
			commander = "Pierre de Villeneuve"	# Pierre de Villeneuve
			big_ship = 33
			light_ship = 8
			losses = 22	# percent
			country = RFR
		}
		result = win
	}
}

1805.10.30 = {
	battle = {
		name = "Caldiero"
		location = 108
		attacker = {
			commander = "Charles of Austria" 	# Charles of Austria
			infantry = 50000
			losses = 6	# percent
			country = HAB
		}
		defender = {
			commander = "Andr� Mass�na"	# Andr� Mass�na
			infantry = 37000
			losses = 11	# percent
			country = RFR
		}
		result = loss
	}
}

# Sweden joins the Third Coalition
1805.10.31 = {
	add_attacker = SWE
}

# Treaty of Potsdam
1805.11.3 = {
	add_attacker = PRU
}

1805.11.5 = {
	battle = {
		name = "Amstetten"
		location = 134
		attacker = {
			commander = "Pyotr Bagration" 	# Pyotr Bagration
			infantry = 6700
			losses = 7	# percent
			country = RUS
		}
		defender = {
			commander = "Marshal Murat" 	# Marshal Murat
			infantry = 10000
			losses = 10	# percent
			country = RFR
		}
		result = loss
	}
}

1805.11.11 = {
	battle = {
		name = "D�renstein"
		location = 134
		attacker = {
			commander = "Mikhail Illarionovich Kutuzov" 	# Mikhail Illarionovich Kutuzov
			infantry = 24000
			losses = 8	# percent
			country = RUS
		}
		defender = {
			commander = "�douard Mortier"	# �douard Mortier
			infantry = 8000
			losses = 22	# percent
			country = RFR
		}
		result = loss
	}
}

1805.11.16 = {
	battle = {
		name = "Sch�ngrabern"
		location = 134
		attacker = {
			commander = "Petr Bagration"	# Petr Bagration
			infantry = 7300
			losses = 34	# percent
			country = RUS
		}
		defender = {
			commander = "Joachim Murat"	# Joachim Murat
			infantry = 21000
			losses = 6	# percent
			country = RFR
		}
		result = loss
	}
}

1805.12.2 = {
	battle = {
		name = "Austerlitz"
		location = 265
		attacker = {
			commander = "Aleksandr I Pavlovich" 	# Aleksandr I Pavlovich
			infantry = 73000
			losses = 21	# percent
			country = RUS
		}
		defender = {
			commander = "Napol�on Bonaparte"	# Napol�on Bonaparte
			infantry = 65000
			losses = 2	# percent
			country = RFR
		}
		result = loss
	}
}

# Treaty of Sch�nbrunn
1805.12.15 = {
	rem_attacker = PRU
	rem_attacker = HAN
}

# Treaty of Pressburg
1805.12.26 = {
	rem_defender = RFR
	rem_defender = NED
	rem_defender = ETR
	rem_defender = SPA
	rem_defender = WUR
	rem_defender = BAV
	rem_defender = ITA
	rem_attacker = HAB
	rem_attacker = RUS
	rem_attacker = GBR
	rem_attacker = NAP
	rem_attacker = SWE
}

#########################################
#	War of the Fourth Coalition	#
#########################################

name = "War of the Fourth Coalition"

1805.12.26 = {
	add_defender = RFR
	add_defender = ETR
	add_defender = NED
	add_defender = SWI
	add_defender = ITA
	add_defender = SPA
	add_attacker = GBR
	add_attacker = RUS
	add_attacker = RAG
	add_attacker = SWE
}

# Ferdinand flees to Sicily
1806.3.11 = {
	add_attacker = SIC
}

# The Confederation of the Rhine
1806.7.25 = {
	add_defender = BAV
	add_defender = BAD
	add_defender = HES
	add_defender = ANH
	add_defender = WUR
}

# Prussia declared war on France
1806.8.26 = {
	add_attacker = PRU
	add_attacker = SAX
}

# W�rzburg joins the Confederation of the Rhine
1806.9.15 = {
	add_defender = WBG
}

# Treaty of Poznan
1806.12.11 = {
	rem_attacker = SAX
	add_defender = SAX
}

# Mecklenburg joins the Confederation of the Rhine
1806.12.15 = {
	add_defender = MKL
}

1806.7.4 = {
	battle = {
		name = "Maida"
		location = 123
		attacker = {
			commander = "John Stuart"	# John Stuart
			infantry = 5100
			losses = 1	#percent
			country = GBR
		}
		defender = {
			commander = "Reynier"	# Reynier
			infantry = 7000
			losses = 10	#percent
			country = RFR
		}
		result = win
	}
}

1806.10.10 = {
	battle = {
		name = "Saalfeld"
		location = 62
		attacker = {
			commander = "John Stuart"	# John Stuart
			infantry = 8000
			losses = 5	#percent
			country = GBR
		}
		defender = {
			commander = "Louis Ferdinand"	# Louis Ferdinand
			infantry = 12000
			artillery = 15
			losses = 2	#percent
			country = RFR
		}
		result = loss
	}
}

1806.10.14 = {
	battle = {
		name = "Jena-Auerstedt"
		location = 62
		attacker = {
			commander = "Karl Wilhelm Ferdinand" 	# Duke of Brunswick 
			infantry = 115000
			losses = 33	#percent
			country = PRU
		}
		defender = {
			commander = "Napol�on Bonaparte"	# Napol�on Bonaparte
			infantry = 100000
			losses = 12	#percent
			country = RFR
		}
		result = loss
	}
}

1806.12.26 = {
	battle = {
		name = "Golymin"
		location = 257
		attacker = {
			commander = "Dmitriy Golitsyn"
			infantry = 35000
			artillery = 128
			losses = 14	#percent
			country = RUS
		}
		defender = {
			commander = "Joachim Murat"
			infantry = 25000
			losses = 28	#percent
			country = RFR
		}
		result = loss
	}
}

1806.12.26 = {
	battle = {
		name = "Pultusk"
		location = 257
		attacker = {
			commander = "Dmitriy Vladimirovich Golitsyn"	# Dmitriy Vladimirovich Golitsyn
			infantry = 18000
			losses = 4	#percent
			country = RUS
		}
		defender = {
			commander = "Joachim Murat"	# Joachim Murat
			infantry = 38000
			losses = 2	#percent
			country = RFR
		}
		result = win
	}
}

1807.2.7 = {
	battle = {
		name = "Eylau"
		location = 42
		attacker = {
			commander = "Levin August Theophil" # Levin August Theophil
			infantry = 67000
			artillery = 450
			losses = 38	#percent
			country = RUS
		}
		defender = {
			commander = "Napol�on Bonaparte"	# Napol�on Bonaparte
			infantry = 45000
			artillery = 200
			losses = 55	#percent
			country = RFR
		}
		result = loss
	}
}

# Sweden signs armistice
1807.5.18 = {
	rem_attacker = SWE
}

1807.6.10 = {
	battle = {
		name = "Heilsberg"
		location = 42
		attacker = {
			commander = "Levin August Theophil"	# Levin August Theophil
			infantry = 115000
			losses = 7	#percent
			country = RUS
		}
		defender = {
			commander = "Joachim Murat"	# Joachim Murat
			infantry = 220000
			losses = 5	#percent
			country = RFR
		}
		result = loss
	}
}

1807.6.14 = {
	battle = {
		name = "Friedland"
		location = 42
		attacker = {
			commander = "Levin August Theophil"	# Levin August Theophil
			infantry = 60000
			artillery = 120
			losses = 33	#percent
			country = RUS
		}
		defender = {
			commander = "Napol�on Bonaparte"	# Napol�on Bonaparte
			infantry = 80000
			artillery = 120
			losses = 10	#percent
			country = RFR
		}
		result = loss
	}
}

# The First Treaty of Tilsit
1807.7.7 = {
	rem_attacker = RUS
}

# The Second Treaty of Tilsit
1807.7.9 = {
	rem_attacker = PRU
}

# Outbreak of the Peninsular War
1807.10.24 = {
	rem_attacker = GBR
	rem_attacker = SIC
	rem_attacker = RAG
	rem_defender = RFR
	rem_defender = ETR
	rem_defender = BAV
	rem_defender = BAD
	rem_defender = HES
	rem_defender = ANH
	rem_defender = WUR
	rem_defender = WBG
	rem_defender = SAX
	rem_defender = MKL
	rem_defender = SPA
	rem_defender = NED
	rem_defender = SWI
	rem_defender = ITA
}
