name = "First Polish-Swedish War"
casus_belli = cb_conquest

1600.1.1 = {
	add_attacker = RZP
	add_defender = SWE
}

1605.9.27 = {
	battle = {
		name = "Kircholm"
		location = 38
		attacker = {
			commander = "Jan Karol Chodkiewicz"	# Jan Karol Chodkiewicz
			infantry = 1300
			cavalry = 2500
			losses = 21	# percent
			country = RZP
		}
		defender = {
			commander = "Karl IX"	# Karl IX
			infantry = 9000
			cavalry = 3000
			losses = 59	# percent
			country = SWE
		}
		result = win
	}
}

# Truce
1611.1.1 = {
	rem_attacker = RZP
	rem_defender = SWE
}
