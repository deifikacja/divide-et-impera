name = "Swedish Campaign Against Norway"
casus_belli = cb_conquest

1814.5.17 = {
	add_attacker = SWE
	add_defender = NOR
}

# Convention of Moss
1814.8.14 = {
	rem_attacker = SWE
	rem_defender = NOR
}
