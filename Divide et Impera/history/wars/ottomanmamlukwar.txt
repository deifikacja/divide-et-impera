name = "Ottoman-Mamluk War"
casus_belli = cb_conquest

1485.1.1 = {
	add_attacker = TUR
	add_defender = MAM
}

1491.1.1 = {
	rem_attacker = TUR
	rem_defender = MAM
}
