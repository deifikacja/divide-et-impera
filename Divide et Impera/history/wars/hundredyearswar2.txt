name = "Hundred Years War - The Third Phase"
casus_belli = cb_claim_throne

1369.5.20 = {
	add_attacker = FRA
	add_attacker = ORL
	add_attacker = AMG
	add_attacker = PRO
	add_attacker = BOU
	add_defender = ENG
	add_defender = BRI
}

1389.1.1 = {
	rem_attacker = FRA
	rem_attacker = ORL
	rem_attacker = AMG
	rem_attacker = PRO
	rem_attacker = BOU
	rem_defender = ENG
	rem_defender = BRI
}
