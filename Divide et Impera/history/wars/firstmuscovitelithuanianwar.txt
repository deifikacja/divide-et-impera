name = "First Muscovite-Lithuanian War"
casus_belli = cb_nationalist

1486.1.1 = {
	add_attacker = MOS
	add_defender = LIT
}

1492.1.1 = {
	add_attacker = CRI
}
# Marriage between Helena and Alexander
1495.1.1 = {
	rem_attacker = MOS
	rem_attacker = CRI
	rem_defender = LIT
}
