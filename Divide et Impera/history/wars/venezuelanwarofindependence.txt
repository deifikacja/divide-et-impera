name = "Venezuelan War of Independence"
casus_belli = cb_liberation

1811.7.5  = {
	add_attacker = VNZ
	add_defender = SPA
}

# The establishment of Gran Colombia
1819.12.17 = {
	rem_attacker = VNZ
	rem_defender = SPA
}
