name = "Mughal-Persian War"
casus_belli = cb_conquest

1649.1.1 = {
	add_attacker = PER
	add_defender = MUG
}

1654.1.1 = {
	rem_attacker = PER
	rem_defender = MUG
}
