name = "Third Ottoman-Safavid War"
casus_belli = cb_conquest


1548.3.29 = {
	add_attacker = TUR
	add_defender = PER
}

# Peace
1549.12.1 = {
	rem_attacker = TUR
	rem_defender = PER
}
