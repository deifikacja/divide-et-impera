name = "Manchu Invasion of Tibet"
casus_belli = cb_conquest

1718.1.1 = {
	add_attacker = QNG
	add_defender = TIB
}

1720.1.1 = {
	rem_attacker = QNG
	rem_defender = TIB
}
