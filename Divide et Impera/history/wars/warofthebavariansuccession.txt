name = "Potato War"
casus_belli = cb_sphere_of_influence

1778.1.1 = {
	add_attacker = PRU
	add_defender = HAB
}

# Treaty of Teschen
1779.5.13 = {
	rem_attacker = PRU
	rem_defender = HAB
}
