name = "Lithuanian invasion of Mazovia"
casus_belli = cb_border_war

1368.1.1 = {
	add_attacker = LIT
	add_defender = MAZ
}

1369.1.1 = {
	rem_attacker = LIT
	rem_defender = MAZ
}
