name = "Persian-Mughal War"
casus_belli = cb_conquest

1738.1.1 = {
	add_attacker = PER
	add_defender = MUG
}

1739.2.24 = {
	battle = {
		name = Karnal
		location = 521
		attacker = {
			commander = "Nadir Shah"	# Nadir Shah
			infantry = 48000
			cavalry = 5000
			losses = 10	# percent
			country = PER
		}
		defender = {
			commander = "Muhammad Shah"	# Muhammad Shah
			infantry = 85000
			losses = 45	# percent
			country = MUG
		}
		result = loss
	}
}

# Nadir Shah and his troops withdraw
1739.5.1 = {
	rem_attacker = PER
	rem_defender = MUG
}
