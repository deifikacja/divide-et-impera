name = "Second Russo-Swedish War"
casus_belli = cb_border_war

1788.6.1 = {
	add_attacker = SWE
	add_defender = RUS
}

1788.6.17 = {
	battle = {
		name = "Hogland"
		location = 1254
		attacker = {
			commander = "Karl of Södermanland"	# Karl of Södermanland
			big_ship = 15
			light_ship = 7
			losses = 1	# percent
			country = SWE
		}
		defender = {
			commander = "Samuel Greigh"	# Samuel Greigh
			big_ship = 15
			light_ship = 7
			losses = 1	# percent
			country = RUS
		}
		result = loss
	}
}

1789.7.26 = {
	battle = {
		name = "Öland"
		location = 3
		attacker = {
			commander = "Karl of Södermanland"	# Karl of Södermanland
			big_ship = 14
			light_ship = 5
			losses = 1	# percent
			country = SWE
		}
		defender = {
			commander = "Vasili Chichagov"	# Vasili Chichagov
			big_ship = 12
			light_ship = 6
			losses = 1	# percent
			country = RUS
		}
		result = loss
	}
}

# The Lingonberry War / Teaterkriget
1788.9.24 = {
	add_defender = DAN
}

1789.5.13 = {
	battle = {
		name = "Reval"
		location = 1254
		attacker = {
			commander = "Karl of Södermanland"	# Karl of Södermanland
			big_ship = 22
			light_ship = 4
			losses = 2	# percent
			country = SWE
		}
		defender = {
			commander = "Vasili Chichagov"	 # Vasili Chichagov
			big_ship = 9
			light_ship = 5
			losses = 3	# percent
			country = RUS
		}
		result = loss
	}
}

1789.7.9 = {
	rem_defender = DAN
}

1790.7.4 = {
	battle = {
		name = "Viborg"
		location = 1254
		attacker = {
			commander = "Karl of Södermanland"	# Karl of Södermanland
			big_ship = 21
			light_ship = 380
			losses = 5	# percent
			country = SWE
		}
		defender = {
			commander = "Vasili Chichagov"	# Vasili Chichagov
			big_ship = 50
			light_ship = 150
			losses = 2	# percent
			country = RUS
		}
		result = loss
	}
}

1790.7.10 = {
	battle = {
		name = "Svensksund"
		location = 1254
		attacker = {
			commander = "Gustav III"	# Gustav III
			big_ship = 6
			light_ship = 170
			losses = 6	# percent
			country = SWE
		}
		defender = {
			commander = "Charles of Nassau-Siegen"	# Charles of Nassau-Siegen
			big_ship = 35
			light_ship = 100
			losses = 10	# percent
			country = RUS
		}
		result = win
	}
}

# Treaty of Wereloe
1790.8.14 = {
	rem_attacker = SWE
	rem_defender = RUS
}
