name = "Conquest of the Chimu"
casus_belli = cb_conquest

1468.1.1 = {
	add_attacker = INC
	add_defender = CHM
}

1471.9.1 = {
	rem_attacker = INC
	rem_defender = CHM
}