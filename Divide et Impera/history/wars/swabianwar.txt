name = "Swabian War"
casus_belli = cb_vassalize_mission

1499.1.1 = {
	add_attacker = HAB
	add_defender = SWI
	add_defender = URI
}

1499.2.20 = { 
	battle = { 
		name = "Hard" 
		location = 73
		attacker = { 
			commander = "Maximilian"		# 
			infantry = 9800
			artillery = 15
			losses = 20		# percent
			country = HAB
		} 
		defender = { 
			commander = "Wilhelm Tell"		#
			infantry = 4200
			losses = 6		# percent 
			country = SWI
		} 
		result = loss
	} 
}

# Peace treaty of Basel
1499.9.22 = {
	rem_attacker = HAB
	rem_defender = SWI
	rem_defender = URI
}
