name = "Fulani War"
casus_belli = cb_conquest

1804.1.1  = {
	add_attacker = SOK
	add_defender = KAN
	add_defender = KTS
	add_defender = HAD
	add_defender = ZAR
	add_defender = ZAM
	add_defender = GOB
	add_defender = KBO
}

# Peace
1810.1.1 = {
	rem_attacker = SOK
	rem_defender = KAN
	rem_defender = KTS
	rem_defender = HAD
	rem_defender = ZAR
	rem_defender = ZAM
	rem_defender = GOB
	rem_defender = KBO
}
