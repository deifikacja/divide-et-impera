name = "Sächsischer Bruderkrieg"
casus_belli = cb_claim_throne


# Altenburger Teilung
1446.1.1 = {
	add_attacker = THU
	add_defender = SAX
}

# Peace at Naumburg
1451.1.27 = {
	rem_attacker = THU
	rem_defender = SAX
}