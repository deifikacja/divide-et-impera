name = "Anglo-Spanish War"
casus_belli = cb_defender_of_the_faith

1585.8.20 = {
	add_attacker = ENG
	add_defender = SPA
}

1588.7.29 = {
	battle = {
		name = "Gravelines"
		location = 90
		attacker = {
			commander = "Francis Drake"	# Charles Howard & Francis Drake
			big_ship = 55
			losses = 1	# percent
			country = ENG
		}
		defender = {
			commander = "Alonso de Guzm�n El Bueno"	# Alonso de Guzm�n El Bueno
			big_ship = 42
			losses = 5	# percent
			country = SPA
		}
		result = win
	}
}

# Treaty of London
1604.8.1 = {
	rem_attacker = ENG
	rem_defender = SPA
}
