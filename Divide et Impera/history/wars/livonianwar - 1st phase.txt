name = "Livonian War"
casus_belli = cb_fabricated_claims

1558.1.22 = {
	add_attacker = RUS
	add_attacker = SWE
	add_defender = POL
	add_defender = LIT
	add_defender = LIV
	add_defender = DAN
	
}

1561.11.28 = {
	rem_defender = LIV
} #Livonian Order is secularisated & inherit by Poland-Lithuania

1568.1.1 = {
	rem_attacker = SWE
	add_defender = SWE
	rem_defender = DAN
	add_attacker = DAN
} # king Johan III & his polish wife, averting of the alliances

1569.7.4 = {
	rem_defender = POL
	rem_defender = LIT
	add_defender = RZP
} # union of Lublin

1570.6.22 = { 
	rem_attacker = RUS
	rem_attacker = DAN
	rem_defender = RZP
	rem_defender = SWE
}