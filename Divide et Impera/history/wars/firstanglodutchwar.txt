name = "First Anglo-Dutch War"
casus_belli = cb_trade_war

1652.7.10 = {
	add_attacker = ENG
	add_defender = NED
}

1652.10.8 = {
	battle = {
		name = "Kentish Knock"
		location = 1271
		attacker = {
			commander = "Robert Blake"	# Robert Blake
			big_ship = 68
			losses = 1	# percent
			country = ENG
		}
		defender = {
			commander = "Witte Corneliszoon de With"	# Witte Corneliszoon de With
			light_ship = 57
			losses = 3	# percent
			country = NED
		}
		result = win
	}
}

1653.2.28 = {
	battle = {
		name = "Portland"
		location = 1272
		attacker = {
			commander = "Robert Blake"	# Robert Blake
			big_ship = 65
			losses = 1	# percent
			country = ENG
		}
		defender = {
			commander = "Maarten Tromp"	# Maarten Tromp
			light_ship = 92
			losses = 1	# percent
			country = NED
		}
		result = win
	}
}

1653.3.14 = {
	battle = {
		name = "Leghorn"
		location = 1298
		attacker = {
			commander = "Henry Appleton"	# Henry Appleton
			big_ship = 6
			losses = 33	# percent
			country = ENG
		}
		defender = {
			commander = "Jahan van Galen"	# Jahan van Galen
			light_ship = 16
			losses = 1	# percent
			country = NED
		}
		result = loss
	}
}

1653.6.12 = {
	battle = {
		name = "North Foreland"
		location = 1270
		attacker = {
			commander = "George Monck and Richard Deane"	# George Monck and Richard Deane
			big_ship = 100
			losses = 1	# percent
			country = ENG
		}
		defender = {
			commander = "Maarten Tromp"	# Maarten Tromp
			light_ship = 98
			losses = 1	# percent
			country = NED
		}
		result = win
	}
}

1653.8.8 = {
	battle = {
		name = "Scheveningen"
		location = 1271
		attacker = {
			commander = "George Monck"	# George Monck
			big_ship = 120
			losses = 16	# percent
			country = ENG
		}
		defender = {
			commander = "Maarten Tromp"	# Maarten Tromp
			light_ship = 100
			losses = 5	# percent
			country = NED
		}
		result = loss
	}
}

# Treaty of Westminster
1654.4.5 = {
	rem_attacker = ENG
	rem_defender = NED
}
