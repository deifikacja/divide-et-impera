name = "Bohemian-Hungarian War"
casus_belli = cb_claim_throne

1468.5.31 = {
	add_attacker = HUN
	add_defender = BOH
}

# Ladislaus inherits
1471.3.22 = {
	add_defender = POL
}

# Peace
1475.2.1 = {
	rem_attacker = HUN
	rem_defender = BOH
	rem_defender = POL
}
