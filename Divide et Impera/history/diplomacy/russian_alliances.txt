#Inner Horde
vassal = {
	first = RUS
	second = BUQ
	start_date = 1801.1.1
	end_date = 1845.1.1
}
#Kalmyk Khanate
vassal = {
	first = RUS
	second = KLM
	start_date = 1608.1.1
	end_date = 1771.1.1
}
#Russian suzerainity over the Lesser Horde
vassal = {
	first = RUS
	second = KZI
	start_date = 1746.1.1
	end_date = 1841.1.1
}
#Vyazma as sub-smolenskian principality
vassal = {
	first = SMO
	second = WOR
	start_date = 1355.1.1
	end_date = 1403.1.1
}
#Pereyaslav and Mongols
vassal = {
	first = GOL
	second = PRY
	start_date = 1355.1.1
	end_date = 1362.1.1
}
#Severia and Mongols
vassal = {
	first = GOL
	second = SVR
	start_date = 1355.1.1
	end_date = 1370.1.1
}
#Rostov and Mongols
vassal = {
	first = GOL
	second = ROS
	start_date = 1355.1.1
	end_date = 1474.1.1
}

#Gorodets
vassal = {
	first = GOL
	second = GOM
	start_date = 1355.1.1
	end_date = 1393.1.1
}
#Galich
vassal = {
	first = MOS
	second = GAL
	start_date = 1355.1.1
	end_date = 1451.1.1
}
#Murom
vassal = {
	first = GOL
	second = MRM
	start_date = 1355.1.1
	end_date = 1393.1.1
}
#Galich
alliance = {
	first = MOS
	second = GAL
	start_date = 1355.1.1
	end_date = 1451.1.1
}
#Suzdal
vassal = {
	first = GOL
	second = SUZ
	start_date = 1355.1.1
	end_date = 1451.1.1
}
#Nizhny Novgorod
vassal = {
	first = GOL
	second = NIJ
	start_date = 1355.1.1
	end_date = 1480.1.1
}
#Chernigov
vassal = {
	first = GOL
	second = CHR
	start_date = 1355.1.1
	end_date = 1370.1.1
}
#Kiev
vassal = {
	first = GOL
	second = KIE
	start_date = 1355.1.1
	end_date = 1362.1.1
}
#Podolia
vassal = {
	first = GOL
	second = POD
	start_date = 1355.1.1
	end_date = 1362.1.1
}
#Suzdal-Nizhnynovgorod?
union = {
	first = SUZ
	second = NIJ
	start_date = 1359.1.1
	end_date = 1365.1.1
}
#Mikhail of Mikulin
union = {
	first = MIK
	second = TVE
	start_date = 1366.1.1
	end_date = 1485.1.1
}

#Mikulin as Tverian sub-principality
vassal = {
	first = TVE
	second = MIK
	start_date = 1355.1.1
	end_date = 1485.1.1
}
#Beloozero as Muscovite vassal
vassal = {
	first = MOS
	second = BLZ
	start_date = 1355.1.1
	end_date = 1486.1.1
}
#Beloozero as Muscovite vassal
alliance = {
	first = MOS
	second = BLZ
	start_date = 1355.1.1
	end_date = 1486.1.1
}
#Viatka as Nizhnegorodian vassal
vassal = {
	first = NIJ
	second = VIA
	start_date = 1355.1.1
	end_date = 1392.1.1
}
#Viatka as Mongol vassal
vassal = {
	first = MOS
	second = VIA
	start_date = 1392.1.1
	end_date = 1403.1.1
}
#Viatka as Mongol vassal
vassal = {
	first = GAL
	second = VIA
	start_date = 1403.1.1
	end_date = 1434.1.1
}
#Viatka as Mongol vassal
vassal = {
	first = MOS
	second = VIA
	start_date = 1434.1.1
	end_date = 1489.1.1
}
#Khanate of Kazan
vassal = {
	first = KAZ
	second = BSH
	start_date = 1438.1.1
	end_date = 1574.1.1
}

#Komi are virtually dependent
vassal = {
	first = NOV
	second = PRM
	start_date = 1355.1.1
	end_date = 1451.1.1
}
#Komi are virtually dependent
alliance = {
	first = NOV
	second = PRM
	start_date = 1355.1.1
	end_date = 1451.1.1
}
#Komi are virtually dependent
vassal = {
	first = MOS
	second = PRM
	start_date = 1451.1.1
	end_date = 1503.3.22
}
#Komi are virtually dependent
alliance = {
	first = MOS
	second = PRM
	start_date = 1451.1.1
	end_date = 1503.3.22
}
#Komi are virtually dependent
vassal = {
	first = RUS
	second = PRM
	start_date = 1503.3.22
	end_date = 1505.1.1
}

#Tartar rule in Russia
vassal = {
	first = GOL
	second = RYA
	start_date = 1355.1.1
	end_date = 1456.1.1
}

#Tartar rule in Russia
vassal = {
	first = GOL
	second = MOS
	start_date = 1355.1.1
	end_date = 1480.1.1
}

#Tartar rule in Russia
vassal = {
	first = GOL
	second = TVE
	start_date = 1355.1.1
	end_date = 1480.1.1
}
#Tartar rule in Russia
vassal = {
	first = GOL
	second = WRC
	start_date = 1355.1.1
	end_date = 1390.1.1
}
#Tartar rule in Russia
vassal = {
	first = MOS
	second = WRC
	start_date = 1494.3.21
	end_date = 1503.2.22
}
#Tartar rule in Russia
vassal = {
	first = RUS
	second = WRC
	start_date = 1503.2.22
	end_date = 1547.1.1
}
#Tartar rule in Russia
vassal = {
	first = GOL
	second = SMO
	start_date = 1355.1.1
	end_date = 1386.1.1
}
#Lithuanian rule in Smolensk
vassal = {
	first = LIT
	second = SMO
	start_date = 1386.1.1
	end_date = 1401.1.1
}

#Viatka as Muscovite sub-principality
vassal = {
	first = MOS
	second = VIA
	start_date = 1392.1.1
	end_date = 1403.1.1
}

#Viatka as Galitzian sub-principality
vassal = {
	first = GAL
	second = VIA
	start_date = 1403.1.1
	end_date = 1434.1.1
}

#Viatka as Muscovite sub-principality
vassal = {
	first = MOS
	second = VIA
	start_date = 1434.1.1
	end_date = 1489.1.1
}

#Galich as Muscovite sub-principality
vassal = {
	first = MOS
	second = GAL
	start_date = 1389.1.1
	end_date = 1450.1.1
}

#Ediger khan seeks for help in Russia
vassal = {
	first = RUS
	second = SIB
	start_date = 1555.1.1
	end_date = 1563.1.1
}

#Yugra is vassalized by Moscow
vassal = {
	first = RUS
	second = PEL
	start_date = 1503.3.22
	end_date = 1563.1.1
}

#Yugra is vassalized by Moscow
vassal = {
	first = MOS
	second = PEL
	start_date = 1500.1.1
	end_date = 1503.3.22
}

#Yugra is vassalized by Moscow
vassal = {
	first = MOS
	second = PEL
	start_date = 1484.1.1
	end_date = 1499.1.1
}

#Yugra is virtually subject to Novgorod
vassal = {
	first = NOV
	second = PEL
	start_date = 1355.1.1
	end_date = 1478.1.1
}

#Russian vassal - Qasim khanate
vassal = {
	first = MOS
	second = QAS
	start_date = 1452.1.1
	end_date = 1503.3.21
}

#Russian vassal - Qasim khanate
vassal = {
	first = RUS
	second = QAS
	start_date = 1503.3.22
	end_date = 1681.1.1
}

# Russian vassal
vassal = {
	first = RUS
	second = UKR
	start_date = 1654.1.1
	end_date = 1735.1.1
}

# Russian vassal
vassal = {
	first = RUS
	second = KUR
	start_date = 1710.11.11
	end_date = 1795.3.28
}

# Muscovy and Denmark
alliance = {
	first = MOS
	second = DAN
	start_date = 1493.11.4
	end_date = 1503.1.1
}

# Russia and Sweden
alliance = {
	first = RUS
	second = SWE
	start_date = 1567.2.16
	end_date = 1568.9.29 
}

# Russia and Sweden
alliance = {
	first = RUS 
	second = SWE
	start_date = 1609.2.28
	end_date = 1610.7.19
}

# Russia and Poland (the Eternal Peace)
alliance = {
	first = RUS
	second = POL
	start_date = 1686.5.6
	end_date = 1699.1.26
}

# Anti-Swedish alliance
alliance = {
	first = RUS
	second = POL
	start_date = 1699.11.22
	end_date = 1706.9.24
}

# Anti-Swedish alliance
alliance = {
	first = RUS
	second = POL
	start_date = 1709.8.8
	end_date = 1721.1.1
}

# Anti-Swedish alliance
alliance = {
	first = RUS
	second = SAX
	start_date = 1699.11.22
	end_date = 1706.9.24
}

# Anti-Swedish alliance
alliance = {
	first = RUS
	second = SAX
	start_date = 1709.8.8
	end_date = 1721.1.1
}

# Anti-Swedish alliance
alliance = {
	first = RUS
	second = DAN
	start_date = 1699.11.22
	end_date = 1700.8.18
}

# Anti-Swedish alliance
alliance = {
	first = RUS
	second = DAN
	start_date = 1709.10.28
	end_date = 1721.1.1
}

# Anti-Swedish alliance
alliance = {
	first = RUS
	second = PRU
	start_date = 1715.1.1
	end_date = 1721.1.1
}

# Anti-Swedish alliance
alliance = {
	first = RUS
	second = HAN
	start_date = 1715.1.1
	end_date = 1721.1.1
}

# Treaty of Hamburg, Russia & Prussia
alliance = {
	first = RUS
	second = PRU
	start_date = 1762.5.22
	end_date = 1762.7.17
}

# Russia and Prussia
alliance = {
	first = RUS
	second = PRU
	start_date = 1764.4.11
	end_date = 1781.5.1
}

# Treaty of Georgievsk
alliance = {
	first = RUS
	second = GEO
	start_date = 1783.7.24
	end_date = 1800.1.1
}

# The Second Coalition, led by Paul I
alliance = {
	first = RUS
	second = GBR
	start_date = 1798.12.24
	end_date = 1801.10.8
}

# The Second Coalition, Treaty of LunÚville
alliance = {
	first = RUS
	second = HAB
	start_date = 1798.12.24
	end_date = 1801.2.9
}

# The Second Coalition, Treaty of Florence
alliance = {
	first = RUS
	second = SIC
	start_date = 1798.12.24
	end_date = 1801.3.18
}

# The Second Coalition, led by Paul I
alliance = {
	first = RUS
	second = POR
	start_date = 1798.12.24
	end_date = 1801.9.1
}

# The Second Coalition, led by Paul I
alliance = {
	first = RUS
	second = TUR
	start_date = 1798.12.24
	end_date = 1801.2.9
}

# Bavaria joins the Second Coalition, led by Paul I
alliance = {
	first = RUS
	second = BAV
	start_date = 1799.1.1
	end_date = 1801.2.9
}

# Ivan III and Maria of Tver
royal_marriage = {
	first = MOS
	second = TVE
	start_date = 1452.1.1
	end_date = 1467.1.1
}

#Yugra is vassalized by Sibir
vassal = {
	first = SIB
	second = PEL
	start_date = 1563.1.1
	end_date = 1589.1.1
}

#Yugra is vassalized by Sibir
alliance = {
	first = SIB
	second = PEL
	start_date = 1563.1.1
	end_date = 1589.1.1
}
#Suzdal princes acting as Knyaz of Novgorod
alliance = {
	first = SUZ
	second = NOV
	start_date = 1359.1.1
	end_date = 1362.1.1
}

#Muscovite princes acting as Knyaz of Novgorod
alliance = {
	first = MOS
	second = NOV
	start_date = 1353.1.1
	end_date = 1359.1.1
}
#Muscovite princes acting as Knyaz of Novgorod
alliance = {
	first = MOS
	second = NOV
	start_date = 1362.1.1
	end_date = 1478.1.14
}
