#Maratha Empire
vassal = {
	first = MAR
	second = PAN
	start_date = 1731.1.1
	end_date = 1818.6.3
}
#Maratha Empire
vassal = {
	first = MAR
	second = BHA
	start_date = 1761.1.1
	end_date = 1775.1.1
}
#Maratha Empire
vassal = {
	first = MAR
	second = PHA
	start_date = 1693.1.1
	end_date = 1818.6.3
}
#Maratha Empire
vassal = {
	first = MAR
	second = KRU
	start_date = 1688.1.1
	end_date = 1818.6.3
}
#Maratha Empire
vassal = {
	first = MAR
	second = NAG
	start_date = 1685.1.1
	end_date = 1818.6.3
}
#Maratha Empire
vassal = {
	first = MAR
	second = JAT
	start_date = 1686.1.1
	end_date = 1818.6.3
}
#Maratha Empire
vassal = {
	first = MAR
	second = JHA
	start_date = 1674.6.16
	end_date = 1818.6.3
}
#Maratha Empire
vassal = {
	first = MAR
	second = JFR
	start_date = 1674.6.16
	end_date = 1818.6.3
}
#Maratha Empire
vassal = {
	first = MAR
	second = DUN
	start_date = 1691.1.1
	end_date = 1818.6.3
}
#Maratha Empire
vassal = {
	first = MAR
	second = BIK
	start_date = 1698.1.1
	end_date = 1818.6.3
}
#Maratha Empire
vassal = {
	first = MAR
	second = BRW
	start_date = 1675.1.1
	end_date = 1818.6.3
}
#Maratha Empire
vassal = {
	first = MAR
	second = JAW
	start_date = 1761.1.1
	end_date = 1768.1.1
}
#Defensive and offensive treaty
alliance = {
	first = GBR
	second = JFR
	start_date = 1733.12.6
	end_date = 1834.1.1
}
#vassal of Mughal
vassal = {
	first = MUG
	second = RAJ
	start_date = 1561.1.1
	end_date = 1679.1.1
}
#vassal of Mughal
vassal = {
	first = MUG
	second = JAI
	start_date = 1561.1.1
	end_date = 1679.1.1
}
#Maratha Empire
vassal = {
	first = MAR
	second = KLH
	start_date = 1710.1.1
	end_date = 1818.1.1
}
#Maratha Empire
vassal = {
	first = MAR
	second = GAK
	start_date = 1721.1.1
	end_date = 1818.1.1
}
#Maratha Empire
vassal = {
	first = MAR
	second = GAK
	start_date = 1700.1.1
	end_date = 1818.1.1
}
#Maratha Empire
vassal = {
	first = MAR
	second = TNJ
	start_date = 1674.1.1
	end_date = 1763.1.1
}
#Protection
vassal = {
	first = GBR
	second = TNJ
	start_date = 1763.1.1
	end_date = 1855.1.1
}
#vassal of Delhhi
vassal = {
	first = DLH
	second = BDI
	start_date = 1355.1.1
	end_date = 1365.1.1
}
#vassal of Delhhi
vassal = {
	first = DLH
	second = SND
	start_date = 1355.1.1
	end_date = 1398.6.1
}
#vassal of Timurid
vassal = {
	first = TIM
	second = SND
	start_date = 1398.6.1
	end_date = 1447.3.12
}
#vassal of Herat
vassal = {
	first = HER
	second = SND
	start_date = 1447.3.12
	end_date = 1506.1.1
}
#vassal of Delhhi
vassal = {
	first = MUG
	second = SND
	start_date = 1526.1.1
	end_date = 1558.1.1
}
#vassal of Delhhi
vassal = {
	first = DLH
	second = JAW
	start_date = 1355.1.1
	end_date = 1396.1.1
}
#vassal of Delhhi
vassal = {
	first = DLH
	second = PHA
	start_date = 1355.1.1
	end_date = 1396.1.1
}
#vassal of Delhhi
vassal = {
	first = DLH
	second = UDA
	start_date = 1355.1.1
	end_date = 1398.6.1
}
#Triple anti Mughal alliance
alliance = {
	first = RAJ
	second = UDA
	start_date = 1679.1.1
	end_date = 1818.1.1
}
#Triple anti Mughal alliance
alliance = {
	first = RAJ
	second = JAI
	start_date = 1679.1.1
	end_date = 1818.1.1
}
#Triple anti Mughal alliance
alliance = {
	first = JAI
	second = UDA
	start_date = 1679.1.1
	end_date = 1818.1.1
}
#Protection
vassal = {
	first = GWA
	second = UDA
	start_date = 1770.1.1
	end_date = 1818.1.1
}
#Maratha Empire
vassal = {
	first = MAR
	second = GWA
	start_date = 1726.1.1
	end_date = 1843.1.1
}
#Protection
vassal = {
	first = MUG
	second = UDA
	start_date = 1597.1.1
	end_date = 1679.1.1
}
#Protection
vassal = {
	first = GBR
	second = BIK
	start_date = 1818.5.9
	end_date = 1949.1.1
}
#Akbar's alliance
alliance = {
	first = JAI
	second = BIK
	start_date = 1740.1.1
	end_date = 1780.1.1
}
#Akbar's alliance
vassal = {
	first = MUG
	second = BIK
	start_date = 1556.3.4
	end_date = 1719.1.1
}
#Akbar's alliance
alliance = {
	first = MUG
	second = BIK
	start_date = 1556.3.4
	end_date = 1719.1.1
}
#Sher Shah's allies
alliance = {
	first = SRI
	second = BIK
	start_date = 1540.1.1
	end_date = 1556.1.1
}
#British protecorate & Pudukkotai
vassal = {
	first = MAD
	second = PUD
	start_date = 1686.1.1
	end_date = 1763.1.1
}
#Madurai & Pudukkotai
vassal = {
	first = MAD
	second = PUD
	start_date = 1686.1.1
	end_date = 1763.1.1
}
#Mudhol as a Jagir
vassal = {
	first = MYS
	second = MUD
	start_date = 1355.1.1
	end_date = 1670.1.1
}
#Nagodh
vassal = {
	first = GBR
	second = NAG
	start_date = 1809.1.1
	end_date = 1948.1.1
}
#Nagodh
vassal = {
	first = GBR
	second = NAG
	start_date = 1802.1.1
	end_date = 1807.1.1
}
#Nagodh
vassal = {
	first = PAN
	second = NAG
	start_date = 1807.1.1
	end_date = 1809.1.1
}
#Jaisalmer
union = {
	first = BDI
	second = KOT
	start_date = 1579.1.1
	end_date = 1632.1.1
}
#Jaisalmer
royal_marriage = {
	first = JSM
	second = BIK
	start_date = 1530.1.1
	end_date = 1820.1.1
}
#Jaisalmer
royal_marriage = {
	first = JSM
	second = UDA
	start_date = 1530.1.1
	end_date = 1820.1.1
}
#Jaisalmer
royal_marriage = {
	first = RAJ
	second = JSM
	start_date = 1530.1.1
	end_date = 1820.1.1
}
#Ajmer Chand and daughter of Fateh Shah
vassal = {
	first = BDK
	second = CHI
	start_date = 1634.1.1
	end_date = 1712.1.1
}
#
royal_marriage = {
	first = THG
	second = GWA
	start_date = 1684.1.1
	end_date = 1695.1.1
}
#Ajmer Chand and daughter of Fateh Shah
royal_marriage = {
	first = JAM
	second = CHB
	start_date = 1735.1.1
	end_date = 1800.1.1
}
#Chamba tributary to Jammu
vassal = {
	first = JAM
	second = CHB
	start_date = 1809.1.1
	end_date = 1816.1.1
}
#Chamba tributary to Jammu
vassal = {
	first = LHR
	second = CHB
	start_date = 1816.1.1
	end_date = 1846.1.1
}
#Bahmani Gujarati war
alliance = {
	first = BAH
	second = KHD
	start_date = 1429.1.1
	end_date = 1431.1.1
}
#Delhi-appointed governor
vassal = {
	first = DLH
	second = GUJ
	start_date = 1396.1.1
	end_date = 1403.1.1
}
#Loyal service in Mughali army
vassal = {
	first = MUG
	second = RJK
	start_date = 1620.1.1
	end_date = 1720.1.1
}
#Loyal service in Mughali army
alliance = {
	first = MUG
	second = RJK
	start_date = 1620.1.1
	end_date = 1720.1.1
}
#Karnatik as French satellite
vassal = {
	first = FRA
	second = KRK
	start_date = 1697.1.1
	end_date = 1763.1.1
}
#Karnatik as British satellite
vassal = {
	first = GBR
	second = KRK
	start_date = 1763.1.1
	end_date = 1825.1.1
}
#Cochin as Potuguese satellite
vassal = {
	first = POR
	second = COC
	start_date = 1503.9.27
	end_date = 1663.1.7
}
#Cochin as Dutch satellite
vassal = {
	first = NED
	second = COC
	start_date = 1663.1.7
	end_date = 1795.1.1
}
#Cochin as British satellite
vassal = {
	first = GBR
	second = COC
	start_date = 1795.1.1
	end_date = 1948.1.1
}
#Karauli as fief of Jaipur
vassal = {
	first = RAJ
	second = KRU
	start_date = 1355.1.1
	end_date = 1817.11.15
}
#Protectorate
vassal = {
	first = GBR
	second = KRU
	start_date = 1817.11.15
	end_date = 1947.1.1
}
#Mughal fief
vassal = {
	first = MUG
	second = BGP
	start_date = 1686.1.1
	end_date = 1724.1.1
}
#Hyderabadi fief
vassal = {
	first = HYD
	second = BGP
	start_date = 1724.1.1
	end_date = 1761.6.3
}
#Mysore fief
vassal = {
	first = MYS
	second = BGP
	start_date = 1761.6.3
	end_date = 1784.1.1
}
#Cannanore as Portuguese vassal
vassal = {
	first = POR
	second = KNN
	start_date = 1545.6.3
	end_date = 1663.2.15
}
#Portuguese-Gujarati agreement
alliance = {
	first = POR
	second = GUJ
	start_date = 1535.12.21
	end_date = 1537.1.1
}
#Delhi and Jaisalmer
vassal = {
	first = DLH
	second = JSM
	start_date = 1355.1.1
	end_date = 1399.1.1
}
#Delhi and Jaisalmer
vassal = {
	first = DLH
	second = JSM
	start_date = 1413.1.1
	end_date = 1526.1.1
}
#Mughal and Jaisalmer
vassal = {
	first = MUG
	second = JSM
	start_date = 1526.1.1
	end_date = 1818.1.1
}
#GBR and Jaisalmer
vassal = {
	first = GBR
	second = JSM
	start_date = 1818.1.1
	end_date = 1948.1.1
}
#Lo within Nepal
vassal = {
	first = NPL
	second = LOK
	start_date = 1790.1.1
	end_date = 1912.1.1
}
# Suri Empire
union = {
	first = BNG
	second = MUG
	start_date = 1540.6.1
	end_date = 1553.1.1
}

alliance = {
	first = MUG
	second = RAJ
	start_date = 1556.1.1
	end_date = 1658.1.1
}

# Battle of Talikota, Ahmednagar, Berar, Bijapur & Golconda
alliance = {
	first = BAS
	second = BRR
	start_date = 1564.1.1
	end_date = 1565.7.26
}

# Battle of Talikota, Ahmednagar, Berar, Bijapur & Golconda
alliance = {
	first = BAS
	second = BIJ 
	start_date = 1564.1.1
	end_date = 1565.7.26
}

# Battle of Talikota, Ahmednagar, Berar, Bijapur & Golconda
alliance = {
	first = BAS
	second = GOC 
	start_date = 1564.1.1
	end_date = 1565.7.26
}

# Battle of Talikota, Ahmednagar, Berar, Bijapur & Golconda
alliance = {
	first = BAS
	second = BID
	start_date = 1564.1.1
	end_date = 1565.7.26
}

# The battle of Buxar
alliance = {
	first = MUG
	second = ODH
	start_date = 1764.1.1
	end_date = 1764.10.1
}

# The battle of Buxar
alliance = {
	first = MUG
	second = BNG
	start_date = 1764.1.1
	end_date = 1764.10.1
}

#Vassal of Delhi
vassal = {
	first = DLH
	second = AHM
	start_date = 1393.1.1
	end_date = 1399.1.1
}

#Vassal of Vijayanagar
vassal = {
	first = VIJ
	second = MYS
	start_date = 1393.1.1
	end_date = 1565.1.1
}

#Vassal of Vijayanagar
vassal = {
	first = VIJ
	second = TRV
	start_date = 1393.1.1
	end_date = 1565.1.1
}

#Vassal of Vijayanagar
vassal = {
	first = VIJ
	second = MAD
	start_date = 1393.1.1
	end_date = 1565.1.1
}
