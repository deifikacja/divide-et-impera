#Marriage
royal_marriage = {
	first = BYZ
	second = EPI
	start_date = 1356.3.1
	end_date = 1359.6.1 #Est
}
#Marriage
vassal = {
	first = SER
	second = BRK
	start_date = 1371.10.26
	end_date = 1412.1.1 #Est
}
#Marriage
vassal = {
	first = TUR
	second = VEL
	start_date = 1371.10.26
	end_date = 1395.5.17 #Est
}
#These states always had close ties
alliance = {
	first = CYP
	second = AMI
	start_date = 1342.1.1
	end_date = 1375.1.1 #Est
}
#Orhan I and Theodora Maria Cantakouzene
royal_marriage = {
	first = TUR
	second = BYZ
	start_date = 1346.1.1
	end_date = 1392.1.1 #Est
}
#Orhan I and Theodora of Serbia
royal_marriage = {
	first = TUR
	second = SER
	start_date = 1345.1.1
	end_date = 1371.1.1
}
#Murad I and Maria of Bulgaria
royal_marriage = {
	first = TUR
	second = BUL
	start_date = 1373.1.1
	end_date = 1378.1.1
}
#ban Tvrtko
vassal = {
	first = HUN
	second = BOS
	start_date = 1355.1.1
	end_date = 1377.9.26
}
#Helen of Bulgaria
royal_marriage = {
	first = SER
	second = BUL
	start_date = 1355.1.1
	end_date = 1359.1.1
}
#Louis and Elizabeth
royal_marriage = {
	first = HUN
	second = BOS
	start_date = 1355.1.1
	end_date = 1387.1.1
}
#Hum
vassal = {
	first = SER
	second = ARR
	start_date = 1358.1.1
	end_date = 1367.1.1
}
#Hum
vassal = {
	first = EPI
	second = ARR
	start_date = 1367.1.1
	end_date = 1411.2.1
}

#Hum
vassal = {
	first = TUR
	second = MRN
	start_date = 1371.9.26
	end_date = 1395.5.14
}
#Hum
vassal = {
	first = SER
	second = HUM
	start_date = 1355.1.1
	end_date = 1448.1.1
}
#Hum
vassal = {
	first = BUL
	second = KRV
	start_date = 1355.1.1
	end_date = 1393.7.17
}
#Hum
vassal = {
	first = BUL
	second = VID
	start_date = 1355.1.1
	end_date = 1393.7.17
}
#Hum
alliance = {
	first = BUL
	second = VID
	start_date = 1355.1.1
	end_date = 1393.7.17
}
#Hum
alliance = {
	first = SER
	second = HUM
	start_date = 1355.1.1
	end_date = 1448.1.1
}
#Hum
vassal = {
	first = HUN
	second = VID
	start_date = 1369.1.1
	end_date = 1388.1.1
}
#Hum
vassal = {
	first = TUR
	second = VID
	start_date = 1388.1.1
	end_date = 1396.1.1
}

#Saruhan
vassal = {
	first = TUR
	second = SRH
	start_date = 1355.1.1
	end_date = 1390.1.1
}

#Saruhan
alliance = {
	first = TUR
	second = SRH
	start_date = 1355.1.1
	end_date = 1390.1.1
}
#Candaroglu
alliance = {
	first = TUR
	second = CND
	start_date = 1389.1.1
	end_date = 1393.1.1
}
#Candaroglu
vassal = {
	first = TUR
	second = CND
	start_date = 1389.1.1
	end_date = 1393.1.1
}
#Epirus
vassal = {
	first = SER
	second = EPI
	start_date = 1367.1.1
	end_date = 1384.1.1
}

#Epirus
vassal = {
	first = TUR
	second = EPI
	start_date = 1389.1.1
	end_date = 1430.10.9
}

#Mamluk influence in Anatoila
vassal = {
	first = MAM
	second = DIY
	start_date = 1355.1.1
	end_date = 1468.1.1
}

#Ramazanoglu as Mamluk vassal
vassal = {
	first = MAM
	second = RAM
	start_date = 1353.1.1
	end_date = 1512.1.1
}

#Ramazanoglu as Turkish vassal from 1512
vassal = {
	first = TUR
	second = RAM
	start_date = 1512.1.1
	end_date = 1608.1.1
}

#Timurid client states
vassal = {
	first = TIM
	second = GRM
	start_date = 1402.1.1
	end_date = 1405.1.1
}

#Timurid client states
vassal = {
	first = TIM
	second = AYD
	start_date = 1402.1.1
	end_date = 1405.1.1
}

vassal = {
	first = BYZ
	second = MOE
	start_date = 1393.1.1
	end_date = 1453.5.29
}
# Wallachia and Transylvania
union = {
	first = WAL
	second = TRA
	start_date = 1599.1.1
	end_date = 1601.1.1
}

# Wallachia and Moldavia
union = {
	first = WAL
	second = MOL
	start_date = 1600.1.1
	end_date = 1601.1.1
}

# Ottomans and Transylvania
alliance = {
	first = TUR
	second = TRA
	start_date = 1528.2.28
	end_date = 1562.6.1
}
# Serbia and the Ottomans
vassal  = {
	first = TUR
	second = SER
	start_date = 1389.1.1
	end_date = 1402.7.1
}

# Serbia and the Ottomans
alliance  = {
	first = TUR
	second = SER
	start_date = 1389.1.1
	end_date = 1402.7.1
}

# Bosnia and the Ottomans
vassal  = {
	first = TUR
	second = BOS
	start_date = 1389.1.1
	end_date = 1402.7.1
}

# Bosnia and the Ottomans
alliance  = {
	first = TUR
	second = BOS
	start_date = 1389.1.1
	end_date = 1402.7.1
}

# Bosnia and Serbia under Stjepan Tomasevic
union  = {
	first = BOS
	second = SER
	start_date = 1461.7.11
	end_date = 1463.6.1
}

# Wallachia and Transylvania
alliance = {
	first = WAL
	second = TRA
	start_date = 1599.1.1
	end_date = 1601.1.1
}

# Wallachia and Moldavia
alliance = {
	first = WAL
	second = MOL
	start_date = 1600.1.1
	end_date = 1601.1.1
}

# Ottoman-Prussian against Austria & Russia
alliance = {
	first = TUR
	second = PRU
	start_date = 1790.1.1
	end_date = 1800.1.1
}
# Athens, a Sicilian vassal
union = {
	first = SIC
	second = ATH
	start_date = 1355.10.16
	end_date = 1388.1.1
}
# Athens, a Sicilian vassal
alliance = {
	first = SIC
	second = ATH
	start_date = 1355.10.16
	end_date = 1388.1.1
}
# Athens, a Florentine vassal
vassal = {
	first = TUS
	second = ATH
	start_date = 1388.1.1
	end_date = 1402.1.1
}
# Athens, an Ottoman vassal
vassal = {
	first = TUR
	second = ATH
	start_date = 1402.1.1
	end_date = 1444.1.1
}
# Athens, an Ottoman vassal
vassal = {
	first = MOE
	second = ATH
	start_date = 1444.1.1
	end_date = 1458.1.1
}
# Achaea, an Anjou vassal
vassal = {
	first = NAP
	second = ACH
	start_date = 1355.1.1
	end_date = 1432.1.1
}
# Achaea, an Anjou vassal
union = {
	first = NAP
	second = ACH
	start_date = 1373.1.1
	end_date = 1380.7.27
}
# Achaea, an Anjou vassal
alliance = {
	first = NAP
	second = ACH
	start_date = 1373.1.1
	end_date = 1380.7.27
}
# Morea, a Byzantine vassal
vassal = {
	first = BYZ
	second = MOE
	start_date = 1355.1.1
	end_date = 1460.1.1
}

# Duchy of the Archipelago
vassal = {
	first = NAP
	second = NAX
	start_date = 1355.1.1
	end_date = 1418.1.1
}
# Ottoman vassal
vassal = {
	first = VEN
	second = NAX
	start_date = 1418.1.1
	end_date = 1566.1.1
}

# Ottoman vassal
vassal = {
	first = TUR
	second = NAX
	start_date = 1566.1.1
	end_date = 1579.1.1
}

# Ottoman vassal
vassal = {
	first = TUR
	second = CRI
	start_date = 1475.6.1
	end_date = 1777.1.1
}

# Russian vassal
vassal = {
	first = RUS
	second = CRI
	start_date = 1777.1.1
	end_date = 1783.7.21
}

# Ottoman vassal
vassal = {
	first = TUR
	second = WAL
	start_date = 1418.1.1
	end_date = 1456.4.1
}

# Hungarian vassal
vassal = {
	first = HUN
	second = WAL
	start_date = 1456.4.1
	end_date = 1462.11.1
}

# Ottoman vassal
vassal = {
	first = TUR
	second = WAL
	start_date = 1462.11.1
	end_date = 1599.1.1
}

# Ottoman vassal
vassal = {
	first = TUR
	second = WAL
	start_date = 1601.1.1
	end_date = 1800.1.1
}

# Ottoman vassal
vassal = {
	first = TUR
	second = MOL
	start_date = 1512.1.1
	end_date = 1600.1.1
}

# Ottoman vassal
vassal = {
	first = TUR
	second = MOL
	start_date = 1601.1.1
	end_date = 1800.1.1
}

# Ottoman vassal
vassal = {
	first = TUR
	second = TRA
	start_date = 1528.1.1
	end_date = 1599.1.1
}

# Ottoman vassal
vassal = {
	first = TUR
	second = TRA
	start_date = 1601.1.1
	end_date = 1699.1.1
}

# Ottoman vassal
vassal = {
	first = TUR
	second = RAG
	start_date = 1528.1.1
	end_date = 1806.1.1
}

# The Septinsular Republic
vassal = {
	first = TUR
	second = CEP
	start_date = 1799.3.1
	end_date = 1807.7.7
}

alliance = {
	first = TUR
	second = SHM
	start_date = 1817.12.17
	end_date = 1822.1.1
}