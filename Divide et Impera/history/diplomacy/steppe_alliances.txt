#Khakas and Mongols
vassal = {
	first = KHA
	second = KHK
	start_date = 1552.1.1
	end_date = 1623.1.1
}
#Khakas and Russia
vassal = {
	first = RUS
	second = KHK
	start_date = 1623.1.1
	end_date = 1727.8.20
}
#Abu Khair as Khan of Tura
union = {
	first = SHY
	second = TYU
	start_date = 1428.1.1
	end_date = 1468.1.1
}
#Chagatids
union = {
	first = CHG
	second = AQS
	start_date = 1508.1.1
	end_date = 1514.1.1
}
#Chagatids
union = {
	first = CHG
	second = AQS
	start_date = 1533.1.1
	end_date = 1560.1.1
}
#Chagatids
union = {
	first = AQS
	second = KSG
	start_date = 1596.1.1
	end_date = 1614.1.1
}
#Feodoro
vassal = {
	first = GOL
	second = THD
	start_date = 1356.1.1
	end_date = 1380.1.1
}
#Feodoro
alliance = {
	first = TRE
	second = THD
	start_date = 1356.1.1
	end_date = 1461.1.1
}