# Asai Kyogoku
vassal = {
	first = ASK
	second = SAT
	start_date = 1356.1.1
	end_date = 1573.1.1
}
# Asai Kyogoku
vassal = {
	first = AAI
	second = KYO
	start_date = 1516.1.1
	end_date = 1560.1.1
}
# Asai Asakura
alliance = {
	first = AAI
	second = ASA
	start_date = 1570.1.1
	end_date = 1573.1.1
}
# Toyotomi and Tokugawa
vassal = {
	first = TOY
	second = TKG
	start_date = 1589.1.1
	end_date = 1598.8.18
}
# Oda and Tokugawa
alliance = {
	first = ODA
	second = TKG
	start_date = 1570.1.1
	end_date = 1582.6.21
}
# Oda and Hakateyama
vassal = {
	first = ODA
	second = HAK
	start_date = 1568.1.1
	end_date = 1581.1.1
}
# Oda and Ashikaga
alliance = {
	first = ODA
	second = ASK
	start_date = 1568.1.1
	end_date = 1582.6.1
}
# Tokugawa and Takeda
alliance = {
	first = TKG
	second = TAK
	start_date = 1570.1.1
	end_date = 1571.1.1
}
# Takeda and Hojo
alliance = {
	first = HOJ
	second = TAK
	start_date = 1571.1.1
	end_date = 1580.1.1
}

# Korea and Tamna
vassal = {
	first = KOR
	second = TMN
	start_date = 1355.1.1
	end_date = 1404.1.1
}

# Japanese vassal
vassal = {
	first = ODA
	second = MRI
	start_date = 1582.1.1
	end_date = 1600.1.1
}

# Japanese vassal
vassal = {
	first = AMA
	second = YAM
	start_date = 1500.1.1
	end_date = 1582.1.1
}

# Japanese vassal
vassal = {
	first = MRI
	second = UKI
	start_date = 1523.01.01
	end_date = 1550.1.1
}

# Japanese vassal
vassal = {
	first = JAP
	second = RYU
	start_date = 1609.04.05
	end_date = 1879.1.1
}

# Japanese daimyo
vassal = {
	first = OUC
	second = SHN
	start_date = 1355.1.1
	end_date = 1399.1.1
}

# Japanese daimyo
alliance = {
	first = SMZ
	second = OUC
	start_date = 1355.1.1
	end_date = 1568.1.1
}

# Japanese daimyo
alliance = {
	first = OTO
	second = OUC
	start_date = 1355.1.1
	end_date = 1568.1.1
}

# Japanese daimyo
vassal = {
	first = SMZ
	second = ASO
	start_date = 1355.1.1
	end_date = 1568.1.1
}

# Japanese daimyo
vassal = {
	first = SMZ
	second = ITO
	start_date = 1355.1.1
	end_date = 1568.1.1
}

# Japanese daimyo
vassal = {
	first = OTO
	second = ITO
	start_date = 1568.1.1
	end_date = 1650.1.1
}

# Japanese daimyo
vassal = {
	first = SHN
	second = SOC
	start_date = 1395.1.1
	end_date = 1443.9.29
}

# Treaty of Gyehae
vassal = {
	first = KOR
	second = SOC
	start_date = 1443.9.29
	end_date = 1593.1.1
}

# Japanese daimyo
vassal = {
	first = OUC
	second = MRI
	start_date = 1355.1.1
	end_date = 1557.1.1
}

# Japanese daimyo
vassal = {
	first = RYZ
	second = OTO
	start_date = 1570.1.1
	end_date = 1578.1.1
}


# Japanese daimyo
vassal = {
	first = SMZ
	second = OTO
	start_date = 1578.1.1
	end_date = 1650.1.1
}

# Japanese daimyo
vassal = {
	first = OTO
	second = RYZ
	start_date = 1565.1.1
	end_date = 1585.1.1
}

# Japanese daimyo
vassal = {
	first = OUC
	second = ICH
	start_date = 1399.1.1
	end_date = 1470.1.1
}

# Japanese daimyo
vassal = {
	first = CHO
	second = ICH
	start_date = 1470.1.1
	end_date = 1550.1.1
}

# Japanese daimyo
vassal = {
	first = SMZ
	second = NBS
	start_date = 1585.1.1
	end_date = 1600.1.1
}


alliance = {
	first = OUC
	second = MRI
	start_date = 1355.1.1
	end_date = 1555.1.1 #Sue Harukata's coup
}

alliance = {
	first = UES
	second = IMA
	start_date = 1545.1.1
	end_date = 1552.1.1
}

alliance = {
	first = ODA
	second = SAI
	start_date = 1549.1.1
	end_date = 1560.1.1
}

alliance = {
	first = ODA
	second = TKG
	start_date = 1562.1.11
	end_date = 1582.6.2
}

alliance = {
	first = TAK
	second = IMA
	start_date = 1554.1.1
	end_date = 1564.1.1
}

# Ouchi-Mori
vassal = {
	first = OUC
	second = MRI
	start_date = 1355.1.1
	end_date = 1555.1.1 #Sue Harukata's coup
}

# Hosokawa-Miyoshi
vassal = {
	first = HOS
	second = MIY
	start_date = 1477.11.11
	end_date = 1532.1.1
}

# Hosokawa is in control of Ashikaga shoguns after the Onin war
vassal = {
	first = HOS
	second = ASK
	start_date = 1477.12.16
	end_date = 1558.1.1
}