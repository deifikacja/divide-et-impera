#############################################################
#
# ADVISORS FOR THE REGION 'AUSTRIA+HUNGARY+BOHEMIA+SLOVENIA'
#
# CURRENT TOTAL IS = 100
#
#############################################################
#############################################################
#provinces:
#############################################################
#Austria:
#############################################################
#73 - Tirol
#76 - Salzburg
#128 - Karnten - Klagenfurt
#130 - Istria
#132 - Steiermark
#133 - Linz
#134 - Wien
#############################################################
#Hungary:
#############################################################
#135 - Sopron
#152 - Slavonia
#153 - Pecs
#154 - Ersekujvar - bratislava
#155 - Szolnok
#156 - Banat
#157 - Partium
#162 - Carpathia
#############################################################
#Bohemia:
#############################################################
#263 - Ratibor
#264 - Breslau
#265 - Moravia
#266 - Bohemia - Prague
#############################################################
#Slovenia:
#############################################################
#129 - Krain - Ljubljana
#############################################################
#Advisor types:
#############################################################
#philosopher
#natural_scientist
#artist
#statesman
#treasurer 
#naval_reformer
#army_reformer 
#trader
#theologian 
#spymaster 
#colonial_governor 
#diplomat
#############################################################
# Era #1  1450 - 1540  (25/25 used currently)
#############################################################

advisor = {
	name = "Nikolaus von Kues" #of the greatest geniuses and polymaths of the 15th century
	location = 73 #Tirol #Blixen
	skill = 5
	type = natural_scientist #could also be theologian 
	date = 1431.1.1 #30 years old
	death_date = 1464.8.11 #died
}

advisor = {
	name = "Ulrich von Cilli" #Prince of the Realm and powerful nobleman
	location = 129 #Krain
	skill = 3
	type = statesman
	date = 1439.1.1 #33 years old
	death_date = 1456.1.1 #died
}

advisor = {
	name = "Ulrich von Eyczing" #Leader of the Mailberger Bund of Nobles against the crown
	location = 132 #Steiermark
	skill = 3
	type = diplomat
	date = 1444.1.1 #46 years old 
	death_date = 1460.11.20 #died
}

advisor = {
	name = "Georg Peurbach" #credited with the invention of several scientific instruments
	location = 134 #Wien 
	skill = 2
	type = natural_scientist
	date = 1453.5.30 #30 years old
	death_date = 1461.4.8 #died
}

advisor = {
	name = "Antonio Bonfini" #Italian Humanist and court historian of Hungary
	location = 153 #Pecs
	skill = 3
	type = philosopher
	date = 1458.1.1 #31 years old
	death_date = 1503.6.6 #died
}

advisor = {
	name = "Michael Pacher" #painter and sculptor
	location = 73 #Tirol
	skill = 2
	type = artist  
	date = 1465.1.1 #30 years old
	death_date = 1498.1.1 #died
}

advisor = {
	name = "Regiomontanus" #important mathematician, astronomer and astrologer
	location = 153 #Pecs
	skill = 3
	type = natural_scientist
	date = 1466.6.6 #30 years old
	death_date = 1476.7.6 #died
}

advisor = {
	name = "Tam�s Bak�cz" #leading statesman of Hungary and mainly responsible for foreign policy
	location = 153 #Pecs (assumed. Could perhaps be another location)
	skill = 3
	type = statesman  
	date = 1472.1.1 #30 years old
	death_date = 1521.6.15 #died
}

advisor = {
	name = "Ulrich Fugger" #elder brother of Jacob Fugger
	location = 134 #Wien, lived in Augsburg but worked for the Habsburgs
	skill = 3
	type = treasurer
	date = 1473.1.1	#provided new suits of clothes to Frederick
	death_date = 1510.1.1 #estimate
}

advisor = {
	name = "Johann Widman" #invented the addition (+) and the subtraction (-) signs
	location = 266 #Bohemia
	skill = 1
	type = natural_scientist  
	date = 1489.1.1 #29 years old
	death_date = 1498.1.1 #died
}

advisor = {
	name = "Paul Hofhaimer" #organist and composer, regarded as the finest organist of his age by many writers
	location = 76 #Salzburg
	skill = 3
	type = artist  
	date = 1489.1.25 #30 years old
	death_date = 1537.1.1 #died
}

advisor = {
	name = "Jacob Fugger" #a banker and a member of the Fugger family. The richest and most famous of them all.
	location = 134 #Wien, lived in Augsburg but worked for the Habsburgs
	skill = 6
	type = treasurer
	date = 1489.3.6	#30 years old
	death_date = 1525.12.30 #died
}

advisor = {
	name = "Johannes Cuspinianus" #Johan Spie�haymer. Austrian humanist, scientist, diplomat, and historian
	location = 133 #Linz
	skill = 2
	type = natural_scientist #could also be diplomat
	date = 1492.1.1 #19 yrs old, became a professor of medicine at the University of Vienna 
	death_date = 1529.4.19 #died
}

advisor = {
	name = "Mercurino Gattinara" #Italian statesman and jurist, Chancellor to Carlos V, his most influental advisor
	location = 134 #Wien (could also be Spain or Italy)
	skill = 5
	type = statesman
	date = 1495.1.1 #30 years old
	death_date = 1530.1.1 #died
}

advisor = {
	name = "Andreas Stoberl" #Stiborius. Austrian astronomer, mathematician, and theologian
	location = 134 #Wien
	skill = 2
	type = natural_scientist
	date = 1495.1.1 #30 yrs old, leading astronomer in Vienna, Advisor to Max I
	death_date = 1515.1.1 #died
}

advisor = {
	name = "Istv�n B�thory" #Hungarian Governor of Transylvania
	location = 153 #Pecs (assumed. Could perhaps be another location)
	skill = 2
	type = statesman  
	date = 1503.1.1 #30 years old
	death_date = 1534.1.1 #died
}

advisor = {
	name = "Georg Tannstetter" #Georgius Collimitius. Humanist, medical docter, astronomer.
	location = 134 #Wien
	skill = 2
	type = natural_scientist
	date = 1503.1.1 #21 yrs old, began career as a Mathmatics teacher in Vienna, Advisor to Max I
	death_date = 1535.3.26 #died
}

advisor = {
	name = "Thomas Murner" #German satirist, poeta laureatus
	location = 134 #Wien
	skill = 1
	type = artist 
	date = 1505.12.24 #30 years old, crowned poeta laureatus by Maximilian
	death_date = 1537.1.1 #died
}

advisor = {
	name = "von Herberstein" #Siegmund Freiherr von Herberstein, outstanding Hapsburg diplomat and Eastern European expert
	location = 129 #Krain
	skill = 5
	type = diplomat #could also be artist
	date = 1515.1.1 #29 years old
	death_date = 1566.3.28 #died
}

advisor = {
	name = "Joachim Vadian" #Joachim von Watt, a Swiss Humanist and scholar.
	location = 134 #Wien
	skill = 3
	type = artist # leading poet. Could also be natural_scientist
	date = 1516.1.1 #32 years old Dean of University of Vienna, Advisor to Max I
	death_date = 1551.4.6 #died
}

advisor = {
	name = "Jacob Hutter" #Anabaptist leader and founder of the Hutterites
	location = 73 #Tirol
	skill = 2
	type = theologian
	date = 1520.1.1 #30 years old 
	death_date = 1536.1.1 #died
}

advisor = {
	name = "Michael Gaismar" #Reformist Priest who rebelled in Tyrol
	location = 73 #Tyrol
	skill = 1
	type = theologian 
	date = 1520.1.1 #30 years old
	death_date = 1532.1.1 #died
}

advisor = {
	name = "Anton Fugger" #successor of Jacob
	location = 134 #Wien, lived in Augsburg but worked for the Habsburgs
	skill = 4
	type = treasurer
	date = 1523.1.1	#30 years old
	death_date = 1560.9.14 #died
}

advisor = {
	name = "Conrad Haas" #Austrian military engineer
	location = 134 #Wien 
	skill = 4
	type = army_reformer 
	date = 1529.1.1 #20 years old, started to describe rocket technology
	death_date = 1576.1.1 #died
}

advisor = {
	name = "C. de Schepper" #Cornelius Duplicius De Schepper. Ambassador of Charles V
	location = 134 #Wien 
	skill = 1
	type = diplomat
	date = 1530.1.1 #28 years old
	death_date = 1555.1.1 #died
}

advisor = {
	name = "Primoz Trubar" #founder and the first superintendent of the Protestant Church of Slovenia
	location = 129 #Krain (Ljubljana)
	skill = 3
	type = theologian  
	date = 1538.6.9 #30 years old
	death_date = 1586.6.28 #died
}

#####################################################################
# Era #2  1540 - 1630   (25/25 used currently) 
#####################################################################

advisor = {
	name = "Rheticus" #Georg Joachim Rheticus. Cartographer, navigational and other instrument maker
	location = 154 #Ersekujvar (assumed Ko�ice is in this province) 
	skill = 1
	type = naval_reformer
	date = 1544.1.1 #30 years old
	death_date = 1574.1.1 #died
}

advisor = {
	name = "Petrus Canisius" #Peter Kanis. Founder of Jesuit Colleges in Austria, fought against the spread of Protestantism
	location = 134 #Wien
	skill = 4
	type = theologian
	date = 1551.5.8 #30 years old
	death_date = 1597.12.21 #died
}

advisor = {
	name = "Anton Brus" #Anton Brus von M�glitz. Arch Bishop of Prague and counter reformer
	location = 265 #Moravia
	skill = 2
	type = theologian
	date = 1552.1.1 #33 years old. Generalgro�meister der Kreuzherren.
	death_date = 1580.8.28 #died
}

advisor = {
	name = "Moreinu ha-Rav Loew" #Rabih Judah L�w ben Bezalel, important Talmudic scholar
	location = 266 #Bohemia
	skill = 5
	type = theologian 
	date = 1555.1.1 #30 years old
	death_date = 1609.8.22 #died
}

advisor = {
	name = "Adam Dietrichstein" #Count Adam von Dietrichstein. Imperial diplomat
	location = 265 #Moravia
	skill = 2
	type = diplomat
	date = 1557.10.17 #30 years old
	death_date = 1590.2.5 #died
}

advisor = {
	name = "Giussepe Arcimboldo" #Court Painter of Emperors Maximilian II and Rudolf II
	location = 134 #Wien
	skill = 3
	type = artist 
	date = 1562.1.1 #35 years old, appointed as Imperial Portraitist
	death_date = 1593.1.1 #died
}

advisor = {
	name = "P�ter Bornemisza" #Outspoken Protestant 
	location = 153 #Pecs
	skill = 2
	type = theologian
	date = 1564.1.1 #29 years old 
	death_date = 1584.6.6 #died
}

advisor = {
	name = "Vilem Ro�mberk" #Wealthy Bohemian magnate with a weakness for the occult
	location = 266 #Bohemia
	skill = 1
	type = treasurer
	date = 1570.1.1 #25 years old  
	death_date = 1592.1.1 #died
}

#advisor = {
#	name = "Elizabeth B�thory" #Infamous mistress of Cachtice Castle. The Blood Countess
#	location = 129 #Krain
#	skill = 4
#	type = spymaster
#	date = 1580.1.1 #20 years old
#	death_date = 1614.8.21 #died
#}

advisor = {
	name = "Wolfgang von Rumpf" #chief minister and long-time companion of Rudolf II 
	location = 134 #Wien
	skill = 2
	type = statesman 
	date = 1580.11.30 #30 years old
	death_date = 1607.1.1 #died
}

advisor = {
	name = "Melchior Klesl" #Austrian statesman and cardinal of the Roman Catholic
	location = 134 #Wien
	skill = 4
	type = theologian  
	date = 1582.2.19 #30 years old
	death_date = 1630.9.18 #died
}

advisor = {
	name = "Juraj Thurzo" #Palatine of Hungary and nemesis of Elizabeth Bathory
	location = 129 #Krain
	skill = 2
	type = statesman
	date = 1585.1.1 #18 years old 
	death_date = 1616.1.1 #died
}

advisor = {
	name = "Edward Kelley" #Alchemist and Charlatan made Baron by Rudolf II
	location = 266 #Bohemia
	skill = 1
	type = trader
	date = 1586.1.1 #34 years old, found the patronage of the Bohemian count Vilem Ro�mberk 
	death_date = 1597.1.1 #died
}

advisor = {
	name = "Karel stars�" #ze �erot�na, head of the moravian nobility during Thirty Years' War
	location = 265 #Moravia
	skill = 2
	type = statesman  
	date = 1594.9.15 #30 years old
	death_date = 1636.11.9 #died
}

advisor = {
	name = "Hans von Eggenberg" #Prince Hans Ulrich von Eggenberg. Chancellor and chief adviser of Ferdinand.
	location = 76 #Salzburg
	skill = 2 
	type = statesman
	date = 1596.1.1 #28 years old, became a trusted servant of the archduke of Styria
	death_date = 1634.1.1 #died
}

advisor = {
	name = "Zdenko Adalbert" #Zdenko Adalbert Poppel, Prince of Lobkowicz, High Chancellor of Bohemia 
	location = 266 #Bohemia
	skill = 1 
	type = statesman
	date = 1598.1.1 #30 years old
	death_date = 1628.7.16 #died
}

advisor = {
	name = "Franz Dietrichstein" #Cardinal and Prince of the realm, secret advisor to Rudolf II. Protector of the Moravian Jews and tax collector
	location = 265 #Moravia
	skill = 2
	type = treasurer
	date = 1599.1.1 #29 years old, became Bishop of Olm�tz
	death_date = 1636.9.19 #died
}

advisor = {
	name = "Paulus van Vianen" #Dutch metalsmith, who became 'Kammergoldschmied' under Rudolf II
	location = 266 #Bohemia
	skill = 3
	type = artist
	date = 1600.1.1 #30 years old
	death_date = 1613.1.1 #died
}

advisor = {
	name = "P�ter P�zm�ny" #important figure in the Counter-Reformation in the Kingdom of Hungary
	location = 154 #Ersekujvar (Bratislava)
	skill = 3
	type = theologian
	date = 1600.10.4 #30 years old
	death_date = 1637.3.19 #died
}

advisor = {
	name = "Diego de Quiroga" #Confessor to the King of Hungary and instrumental in the conspiracy against Wallenstein
	location = 153 #Pecs
	skill = 3
	type = spymaster
	date = 1605.1.1 #31 years old, became priest
	death_date = 1649.10.10 #died
}

advisor = {
	name = "Jaroslav Borsita" #Jaroslav Borsita von Martinic. Treasurer and assessor of the Bohemian regional court
	location = 266 #Bohemia
	skill = 1
	type = treasurer
	date = 1610.1.1 #28 years old
	death_date = 1649.11.21 #died
}

advisor = {
	name = "Wilhelm Lamormain" #Jesuit priest and confessor of Ferdinand II
	location = 266 #Bohemia
	skill = 1
	type = theologian
	date = 1619.1.1 #49 years old, Ferdinand II becomes Emperor
	death_date = 1648.1.1 #died
}

advisor = {
	name = "Jan Amos Komensky" #Czech teacher, scientist, educator and writer
	location = 265 #Moravia
	skill = 1
	type = philosopher #because of his ideas about education
	date = 1622.3.28 #30 years old
	death_date = 1670.11.15 #died
}

advisor = {
	name = "Sezyma Rasin" #Wallenstein's chief agent and betrayer
	location = 266 #Bohemia
	skill = 2
	type = spymaster
	date = 1625.1.1 #30 years old 
	death_date = 1665.1.1 #70 years after birth. No idea about actual death date.
}

###################################################################
# Era #3  1630 - 1720 (25/25 used currently) 
###################################################################

advisor = {
	name = "Anton von Eggenberg" #Duke Johann Anton I. von Eggenberg, Leader of the Golden Carriage mission to Rome
	location = 76 #Salzburg
	skill = 2
	type = diplomat
	date = 1637.1.1 #27 years old, Ferdinand II dies
	death_date = 1649.2.19 #died
}

advisor = {
	name = "Trautmansdorff" #Count Maximilian von Trautmansdorff. Chief Imperial plenipotentiary at Westphalia
	location = 134 #Wien
	skill = 5 
	type = diplomat
	date = 1645.1.1 #28 years old
	death_date = 1684.1.1 #died
}

advisor = {
	name = "Andreas Gryphius" #German lyric poet and dramatist
	location = 264 #Breslau 
	skill = 2
	type = artist  
	date = 1646.10.11 #30 years old
	death_date = 1664.7.16 #died
}

#advisor = {
#	name = "Katharina Galler" #Katharina Elisabeth Freifrau von Galler. Freewoman and Heiress
#	location = 132 #Steiermark
#	skill = 2
#	type = treasurer
#	date = 1648.1.1 #41 years old
#	death_date = 1672.2.12 #died
#}

advisor = {
	name = "Ludovico Burnacini" #Ludovico Ottavio Burnacini. Italian architect and stage designer under Austrian patronage
	location = 134 #Wien
	skill = 2
	type = artist
	date = 1660.1.1 #24 years old
	death_date = 1707.6.10 #died
}

advisor = {
	name = "Johann von Eggenberg" #Prince Johann Christian von Eggenberg, Property speculator and patron of the arts
	location = 76 #Salzburg
	skill = 2 
	type = trader
	date = 1661.1.1 #20 years old
	death_date = 1710.12.14 #died
}

advisor = {
	name = "Philipp von H�rnigk" #Philipp Wilhelm von H�rnigk, writer of '�sterreich �ber alles, wenn es nur will'
	location = 134 #Wien
	skill = 2
	type = treasurer 
	date = 1670.1.1 #30 years old
	death_date = 1714.10.23 #died
}

advisor = {
	name = "Janez V. Valvasor" #Janez Vajkard Valvasor. Slovenian nobleman, scholar, and polymath
	location = 129 #Krain (Ljubljana)
	skill = 3
	type = natural_scientist  
	date = 1671.5.28 #30 years old
	death_date = 1693.9.16 #died
}

advisor = {
	name = "Weisskircher" #Hans Adam Weisskircher (Wei�enkirchner). Court painter
	location = 76 #Salzburg
	skill = 1 
	type = artist
	date = 1678.1.1 #32 years old
	death_date = 1695.1.26 #died
}

advisor = {
	name = "Francesco Menegatti" #Jesuit priest and Confessor of Leopold I. Interepted the word 'pretender' to enable Leopold to exclude James II and heirs from the English throne 
	location = 134 #Wien
	skill = 2
	type = diplomat
	date = 1680.1.1 #49 years old
	death_date = 1700.1.1 #died in this year probably
}

advisor = {
	name = "Franz Ferdinand" #Franz Ferdinand von Rummel. Religous teacher to Joseph I
	location = 134 #Wien
	skill = 2
	type = philosopher #could also be theologian
	date = 1684.1.1 #40 years old
	death_date = 1716.3.15 #died
}

advisor = {
	name = "Fischer von Erlach" #Johann Bernhard Fischer von Erlach. Most influential Austrian architect of the Baroque period
	location = 132 #Steiermark
	skill = 4
	type = artist
	date = 1688.1.1 #32 years old
	death_date = 1723.4.5 #died
}

advisor = {
	name = "Johann Fux" #Austrian composer, music theorist and pedagogue of the late Baroque era
	location = 134 #Wien
	skill = 2
	type = artist  
	date = 1690.1.1 #30 years old
	death_date = 1741.2.13 #died
}

advisor = {
	name = "Carlo Agostino Badia" #Italian operatic composer who found fame in Vienna
	location = 134 #Wien
	skill = 2
	type = artist
	date = 1697.1.1 #25 years old, 'Bacco, vincitore dell'India' 
	death_date = 1738.9.23 #died
}

advisor = {
	name = "Karl von Zinzendorf" #Count Karl von Zinzendorf, Imperial plenipotentiary for Austria
	location = 265 #Moravia
	skill = 2
	type = diplomat
	date = 1700.1.1 #30 years old
	death_date = 1740.1.1 #Death date unknown. 70 years old.
}

advisor = {
	name = "P�lffy J�nos IV" #Count Palffy Janos IV. Palantine of Hungary
	location = 152 #Slavonia
	skill = 3 
	type = statesman
	date = 1700.1.1 #22 years old
	death_date = 1750.3.24 #died
}

advisor = {
	name = "Johann Wratislaw" #Graf Johann Wenzel Wratislaw. Brought about the alliance between Emperor Leopold I and England in 1701 in the War of the Spanish Succession
	location = 266 #Bohemia
	skill = 4
	type = diplomat 
	date = 1700.1.1 #31 years old
	death_date = 1712.12.21 #died
}

advisor = {
	name = "Johann C. Hackhofer" #Johann Cyriak Hackhofer. Baroque paiter
	location = 73 #Tirol
	skill = 2
	type = artist
	date = 1700.1.1 #25 years old
	death_date = 1731.9.5 #died
}

advisor = {
	name = "Franciszek Rakoczy" #Hungarian rebel from a powerful family
	location = 153 #Pecs
	skill = 2
	type = statesman
	date = 1704.1.1 #28 years old
	death_date = 1738.1.1 #died
}

advisor = {
	name = "Frantisek Kanka" #Franti�ek Maxmili�n Kanka. Czech architect and builder
	location = 266 #Bohemia (Prague)
	skill = 2
	type = artist  
	date = 1704.8.9 #30 years old
	death_date = 1766.7.14 #died
}

advisor = {
	name = "Jan Santini Aichel" #Jan Bla�ej Santini Aichel. Czech architect of the baroque
	location = 266 #Bohemia (Prague)
	skill = 2
	type = artist  
	date = 1707.2.3 #30 years old
	death_date = 1723.12.7 #died
}

advisor = {
	name = "Francis Kinsky" #Francis Ferdinand Kinsky, First Chancellor and Grand Venor of the Kingdom of Bohemia 
	location = 266 #Bohemia
	skill = 3
	type = statesman
	date = 1708.1.1 #30 years old
	death_date = 1741.1.1 #died
}

advisor = {
	name = "Johann Lukas" #Johann Lukas von Hildebrandt, architect who built many palaces including Eugenes  
	location = 134 #Wien
	skill = 3
	type = artist 
	date = 1713.1.1 #45 years old. Employed by the wealthy and powerful Kinsky family 
	death_date = 1745.1.1 #died
}

advisor = {
	name = "Carlo Carlone" #prolific Italian church painter who found work in Austria  
	location = 134 #Wien
	skill = 2 
	type = artist
	date = 1716.1.1 #30 years old
	death_date = 1775.1.1 #died
}

advisor = {
	name = "Johann Bartenstein" #Johann Christof Freiherr von Bartenstein, Chancellor and Minister for foreign affairs 
	location = 134 #Wien 
	skill = 2
	type = diplomat #could also be statesman
	date = 1719.10.23 #30 years old
	death_date = 1767.8.6 #died
}

####################################################################
# Era #4  1720 - 1810 (25/25 used currently) 
#####################################################################

advisor = {
	name = "Josef Wenzel" #F�rst Liechtenstein. Reformed Austrian artillery
	location = 134 #Wien 
	skill = 3
	type = army_reformer 
	date = 1726.8.9 #30 years old
	death_date = 1772.2.10 #died
}

advisor = {
	name = "Gregor Joseph Werner" #Austrian composer in the services of Paul II Anton Esterh�zy
	location = 135 #Sopron (Is Eisenstadt located in this province?)
	skill = 2 
	type = artist
	date = 1728.1.1 #35 years old
	death_date = 1766.3.3 #died
}

advisor = {
	name = "Gerard van Swieten" #implemented a transformation of the Austrian health service
	location = 134 #Wien 
	skill = 2
	type = natural_scientist 
	date = 1730.5.7 #30 years old
	death_date = 1772.6.18 #died
}

advisor = {
	name = "Johann Ernst Eberlin" #composer and organist
	location = 76 #Salzburg
	skill = 2
	type = artist  
	date = 1732.3.27 #30 years old
	death_date = 1762.6.19 #died
}

advisor = {
	name = "Joseph Harrach" #Johann Joseph Philipp Graf Harrach. Hofkriegsratspr�sident.
	location = 134 #Wien
	skill = 2
	type = statesman 
	date = 1738.1.1 #60 years old, President of War Council
	death_date = 1764.8.8 #died
}

advisor = {
	name = "Johann Stamitz" #Johann Wenzel Anton Stamitz. Composer and violinist.
	location = 266 #Bohemia (Prague)
	skill = 2
	type = artist  
	date = 1741.1.1 #24 years old, became first violin
	death_date = 1757.3.27 #died
}

advisor = {
	name = "Wenzel Anton Kaunitz" #Chancellor of state and minister of foreign affairs
	location = 265 #Moravia 
	skill = 5
	type = statesman  
	date = 1741.2.2 #30 years old
	death_date = 1794.6.27 #died
}

advisor = {
	name = "Christoph W. Gluck" #One of the most important opera composers of the Classical music era
	location = 134 #Wien
	skill = 3
	type = artist  
	date = 1744.7.2 #30 years old
	death_date = 1787.11.15 #died
}

advisor = {
	name = "Anthony Corfiz" #Count Uhlfeld. Austrian diplomat
	location = 134 #Wien
	skill = 1
	type = diplomat
	date = 1745.1.1 #21 years old
	death_date = 1769.6.6
}

advisor = {
	name = "Leopold Mozart" #father of Wolfgang Amadeus Mozart, but well-known himself in his own time.
	location = 134 #Wien 
	skill = 3
	type = artist  
	date = 1749.11.14 #30 years old
	death_date = 1787.5.28 #died
}

advisor = {
	name = "Bernardo Bellotto" #Italian urban landscape painter
	location = 134 #Wien, could also be other locations
	skill = 3
	type = artist  
	date = 1750.1.30 #30 years old
	death_date = 1780.10.17 #died
}

advisor = {
	name = "Maximilian Hell" #German-Hungarian astronomer, director of the Vienna Observatory
	location = 154 #Slovakia?
	skill = 3
	type = natural_scientist  
	date = 1750.5.15 #30 years old
	death_date = 1792.4.14 #died
}

advisor = {
	name = "Leopold Auenbrugger" #physician who invented percussion
	location = 134 #Wien
	skill = 3
	type = natural_scientist  
	date = 1752.11.19 #30 years old
	death_date = 1809.5.17 #died
}

advisor = {
	name = "Maurus Lindemayr" #Kajetan Benedikt Maximilian Lindemayr. Prior, dramatist and Poet
	location = 133 #Linz
	skill = 2
	type = artist
	date = 1754.1.1 #31 years old, became Prior of the Benedictine Monastery
	death_date = 1783.7.19 #died
}

advisor = {
	name = "Anton Raphael Mengs" #painter
	location = 266 #Bohemia (born there)
	skill = 1
	type = artist  
	date = 1758.3.12 #30 years old
	death_date = 1779.6.29 #died
}

advisor = {
	name = "Johann Thugut" #Johann Amadeus Francis de Paula, Baron of Thugut, special envoy of the Austrian court 
	location = 133 #Linz
	skill = 1
	type = diplomat
	date = 1760.1.1 #24 years old, was sent to the school of Oriental languages
	death_date = 1818.5.28 #died
}

advisor = {
	name = "C. Anton Migazzi" #Cardinal Christoph Anton Migazzi. Conservative Arch-bishop of Vienna 
	location = 73 #Tirol
	skill = 2
	type = theologian
	date = 1761.1.1 #46 years old
	death_date = 1803.4.14 #died
}

advisor = {
	name = "Joseph Haydn" #leading composer of the Classical period
	location = 134 #Wien
	skill = 5
	type = artist  
	date = 1762.3.31 #30 years old
	death_date = 1809.5.31 #died
}

advisor = {
	name = "Franz Mesmer" #discovered what he called animal magnetism
	location = 134 #Wien
	skill = 3
	type = natural_scientist  
	date = 1764.5.23 #30 years old
	death_date = 1815.3.5 #died
}

advisor = {
	name = "Johann Cobenzl" #Johann Ludwig Graf von Cobenzl. Vice Chancellor and successor to Kaunitz 
	location = 129 #krain
	skill = 3
	type = diplomat
	date = 1772.1.1 #19 years old, entered Austrian service
	death_date = 1809.2.22 #died
}

advisor = {
	name = "Joseph Eckhel" #Joseph Hilarius Eckhel. Jesuit priest and numismatist
	location = 132 #Steiermark
	skill = 1
	type = treasurer
	date = 1772.1.1 #37 years old, appointed as keeper of the cabinet of coins
	death_date = 1798.5.16 #died
}

advisor = {
	name = "Wolfgang A. Mozart" #Arguable the greatest composer of the EU3 time period
	location = 134 #Wien
	skill = 6
	type = artist  
	date = 1774.1.27 #18 years old
	death_date = 1791.12.5 #died
}

advisor = {
	name = "Antonio Salieri" #one of the most important and famous musicians of his time
	location = 134 #Wien
	skill = 5
	type = artist  
	date = 1780.8.18 #30 years old
	death_date = 1825.5.7 #died
}

advisor = {
	name = "Anton Tomaz Linhart" #Anton Toma� Linhart. Slovenian dramatist and historian
	location = 129 #Krain (Ljubljana)
	skill = 2
	type = artist  
	date = 1780.12.11 #24 years old, first poems published
	death_date = 1795.7.14 #died
}

advisor = { 
	name = "Muzio Clementi" #Italian classical composer and piano player
	location = 134 #Wien
	skill = 2
	type = artist  
	date = 1782.1.24 #30 years old
	death_date = 1832.3.10 #dies
}

advisor = {
	name = "Jurij Vega" #Slovenian mathematician, his method of calculating p is still mentioned today
	location = 129 #Krain (Ljubljana)
	skill = 2
	type = natural_scientist  
	date = 1784.3.23 #30 years old
	death_date = 1802.9.26 #died
}

advisor = {
	name = "Franz Joseph Gall" #German neuroanatomist and physiologist
	location = 134 #Wien
	skill = 4
	type = natural_scientist
	date = 1800.1.1	#developed cranioscopy
	death_date = 1828.8.22 #died
}

advisor = {
	name = "Ludwig van Beethoven" #One of the greatest composers in the history of music
	location = 134 #Wien
	skill = 6
	type = artist  
	date = 1800.12.17 #30 years old
	death_date = 1827.3.26 #died
}

advisor = {
	name = "Metternich" #Klemens Wenzel Lothar von Metternich, one of the most important diplomats of his era
	location = 134 #Wien
	skill = 6
	type = diplomat  
	date = 1803.5.15 #30 years old
	death_date = 1859.6.11 #dies
}

advisor = {
	name = "Johann Spurzheim" #German physician
	location = 134 #Wien
	skill = 2
	type = natural_scientist
	date = 1806.12.3	#30 years old
	death_date = 1832.1.1 #died
}

############################################################
#Could also be added
############################################################
#Balthasar Hubmaier, Influential German/Moravian Anabaptist leader 1512.1.1-1528.1.1
#Jacobus Gallus, Slovenian composer, 1579.1.1-1591.7.18