government = daimyo
aristocracy_plutocracy = -4
centralization_decentralization = 3
innovative_narrowminded = 2
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = -1
offensive_defensive = -2
land_naval = 0
quality_quantity = 4
serfdom_freesubjects = -5
primary_culture = japanese
religion = shinto
technology_group = chinese
capital = 1019	# Harima
daimyo = yes



1354.1.1 = { 
	monarch = { 
		name = "Norisuke" 
		dynasty = "Akamatsu" 
		adm = 5 
		dip = 6 
		mil = 8 
		leader = { name = "Akamatsu Norisuke" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
	} 
} 

1354.1.1 = {
    heir = {
        name = "Mitsusuke"
        monarch_name =  "Mitsusuke" 
        dynasty = "Akamatsu"
        birth_date = 1343.1.1
        death_date = 1441.9.25
        claim = 94
        adm = 5
        dip = 8
        mil = 7
        leader = {
            name =  "Mitsusuke" 
            type = general
            rank = 0
            fire = 1
            shock = 2
            manuever = 2
            siege = 1
        }
	}
}



1371.1.1 = { 
	monarch = { 
		name = "Mitsusuke" 
		dynasty = "Akamatsu" 
		adm = 5 
		dip = 8 
		mil = 7 
		leader = { name = "Akamatsu Mitsusuke" type = general rank = 0 fire = 1 shock = 2 manuever = 2 siege = 1 } 
	} 
} 

1371.1.1 = {
    heir = {
        name = "Harumasa"
        monarch_name =  "Harumasa" 
        dynasty = "Akamatsu"
        birth_date = 1366.1.1
        death_date = 1500.1.1
        claim = 90
        adm = 4
        dip = 4
        mil = 6
        leader = {
            name =  "Harumasa" 
            type = general
            rank = 0
            fire = 0
            shock = 1
            manuever = 2
            siege = 0
        }
   }
}



1441.9.25 = { 
	monarch = { 
		name = "Harumasa" 
		dynasty = "Akamatsu" 
		adm = 4 
		dip = 4 
		mil = 6 
		leader = { name = "Akamatsu Harumasa" type = general rank = 0 fire = 0 shock = 1 manuever = 2 siege = 0 } 
	} 
} 


1441.9.25 = {
    heir = {
        name = "Yoshisuke"
        monarch_name =  "Yoshisuke" 
        dynasty = "Akamatsu"
        birth_date = 1427.1.1
        death_date = 1576.1.1
        claim = 89
        adm = 3
        dip = 3
        mil = 3
        leader = {
            name =  "Yoshisuke" 
            type = general
            rank = 0
            fire = 0
            shock = 1
            manuever = 1
            siege = 0
        }
   }
}


1500.1.1 = { 
	monarch = { 
		name = "Yoshisuke" 
		dynasty = "Akamatsu" 
		adm = 3 
		dip = 3 
		mil = 3 
		leader = { name = "Akamatsu Yoshisuke" type = general rank = 0 fire = 0 shock = 1 manuever = 1 siege = 0 } 
	} 
} 

1500.1.1 = {
    heir = {
        name = "Norifusa"
        monarch_name = "Norifusa" 
        dynasty = "Akamatsu"
        birth_date = 1488.1.1
        death_date = 1579.1.1
        claim = 80
        adm = 5
        dip = 7
        mil = 5
        leader = {
            name =  "Norifusa" 
            type = general
            rank = 0
            fire = 0
            shock = 2
            manuever = 0
            siege = 0
        }
   }
}


1576.1.1 = { 
	monarch = { 
		name = "Norifusa" 
		dynasty = "Akamatsu" 
		adm = 5 
		dip = 7 
		mil = 5 
		leader = { name = "Akamatsu Norifusa" type = general rank = 0 fire = 0 shock = 2 manuever = 0 siege = 0 } 
	} 
} 


1576.1.1 = { 
	heir = { 
		name = "Masamoto" 
		monarch_name = "Masamoto" 
		dynasty = "Akamatsu" 
		birth_date = 1576.1.1 
		death_date = 1600.1.5 
		claim = 95 
		adm = 7 
		dip = 7 
		mil = 8 
		leader = { name = "Masamoto" type = general rank = 0 fire = 0 shock = 1 manuever = 2 siege = 0 } 
	} 
} 

