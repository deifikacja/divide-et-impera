government = eastern_despotism
aristocracy_plutocracy = -3
centralization_decentralization = 3
innovative_narrowminded = 1
mercantilism_freetrade = 1
imperialism_isolationism = 0
secularism_theocracy = -2
offensive_defensive = 0
land_naval = 2
quality_quantity = -1
serfdom_freesubjects = -1
primary_culture = malayan
religion = hinduism
technology_group = chinese
capital = 640	# Sukadana

1356.1.1 = {
	monarch = {
		name = "Bravidjaya"
		dynasty = Sailendra
		adm = 4
		dip = 4
		mil = 5
	}
}
1430.1.1 = {
	monarch = {
		name = "Bapurang"
		dynasty = Sailendra
		adm = 4
		dip = 4
		mil = 5
	}
}
1480.1.1 = {
	monarch = {
		name = "Pundong Prasap"
		dynasty = Sailendra
		adm = 4
		dip = 4
		mil = 5
	}
}
1500.1.1 = {
	monarch = {
		name = "Bandala"
		dynasty = Sailendra
		adm = 4
		dip = 4
		mil = 5
	}
}
1510.1.1 = {
	monarch = {
		name = "Anom"
		dynasty = Sailendra
		adm = 4
		dip = 4
		mil = 5
	}
}
1530.1.1 = {
	monarch = {
		name = "Ajer Mala"
		dynasty = Sailendra
		adm = 4
		dip = 4
		mil = 5
	}
}
1550.1.1 = {
	monarch = {
		name = "Di Baruh Sungei Matan"
		dynasty = Sailendra
		adm = 4
		dip = 4
		mil = 5
	}
}
1590.1.1 = {
	monarch = {
		name = "Giri Kusuma"
		dynasty = Sailendra
		adm = 4
		dip = 4
		mil = 5
	}
}
1608.1.1 = {
	monarch = {
		name = "Piri Bunku"
		dynasty = Sailendra
		adm = 4
		dip = 4
		mil = 5
	}
}
1622.1.1 = {
	religion = sunni
	monarch = {
		name = "Giri Mustafa"
		dynasty = Matarama
		adm = 4
		dip = 4
		mil = 5
	}
}
1677.1.1 = {
	monarch = {
		name = "Muhammad Zain ad-Din"
		dynasty = Matarama
		adm = 4
		dip = 4
		mil = 5
	}
}
1699.1.1 = {
	monarch = {
		name = "Agung"
		dynasty = Matarama
		adm = 4
		dip = 4
		mil = 5
	}
}
1720.1.1 = {
	monarch = {
		name = "Muhammad Zain ad-Din"
		dynasty = Matarama
		adm = 4
		dip = 4
		mil = 5
	}
}
1725.1.1 = {
	religion = hinduism
	monarch = {
		name = "Ratu"
		dynasty = Matarama
		adm = 4
		dip = 4
		mil = 5
	}
}
1727.1.1 = {
	religion = sunni
	monarch = {
		name = "Muhammad Zain ad-Din"
		dynasty = Matarama
		adm = 4
		dip = 4
		mil = 5
	}
}
1732.1.1 = {
	monarch = {
		name = "Agong"
		dynasty = Matarama
		adm = 4
		dip = 4
		mil = 5
	}
}
1736.1.1 = {
	monarch = {
		name = "Ala ad-Din Manku Ratu"
		dynasty = Matarama
		adm = 4
		dip = 4
		mil = 5
	}
}
1750.1.1 = {
	monarch = {
		name = "Muizz ad-Din Girilaja"
		dynasty = Matarama
		adm = 4
		dip = 4
		mil = 5
	}
}
1770.1.1 = {
	monarch = {
		name = "Ahmad Kamal ad-Din Inralaja"
		dynasty = Matarama
		adm = 4
		dip = 4
		mil = 5
	}
}
1790.1.1 = {
	monarch = {
		name = "Muhammad Jamluddin bin Ahmad Kamaluddin Indralaya"
		dynasty = Matarama
		adm = 4
		dip = 4
		mil = 5
	}
}