government = caste_monarchy
aristocracy_plutocracy = -2
centralization_decentralization = 3
innovative_narrowminded = 1
mercantilism_freetrade = -2
imperialism_isolationism = 0
secularism_theocracy = 1
offensive_defensive = 5
land_naval = -3
quality_quantity = -2
serfdom_freesubjects = -2
technology_group = indian
primary_culture = marathi
religion = jainism
capital = 1963	# Jawhar


#Deshmukh
1686.1.1 = {
	monarch = {
		name = "Satvaji Rao"
		dynasty = Dafle
		adm = 7
		dip = 7
		mil = 9
	}

}

1706.7.1 = {
	monarch = {
		name = "Yesu Bai"
		dynasty = Dafle
		adm = 5
		dip = 5
		mil = 7
		female = yes
	}

}

1754.1.1 = {
	monarch = {
		name = "Yeshwant Rao"
		dynasty = Dafle
		adm = 6
		dip = 5
		mil = 7
	}

}

1759.1.1 = {
	monarch = {
		name = "Amrit Rao I"
		dynasty = Dafle
		adm = 8
		dip = 4
		mil = 6
	}

}

1790.1.1 = {
	monarch = {
		name = "Khanji Rao"
		dynasty = Dafle
		adm = 6
		dip = 4
		mil = 4
	}

}

1810.1.1 = {
	monarch = {
		name = "Renuka Bai"
		dynasty = Dafle
		adm = 4
		dip = 5
		mil = 3
		female = yes
	}
}