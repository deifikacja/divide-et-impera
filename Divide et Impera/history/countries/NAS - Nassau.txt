government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -3
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 1
land_naval = -1
quality_quantity = 0
serfdom_freesubjects = -4
primary_culture = hessian
religion = catholic
technology_group = western
capital = 83	# Nassau

 

#House of Nassau - Ottonian line - Nassau-Dilenburg
1350.1.1 = {
	monarch = {
		name = "Johann I"
		dynasty = "von Nassau"
		DIP = 5
		ADM = 5
		MIL = 3
	}
}
1416.1.1 = {
	monarch = {
		name = "Adolf"
		dynasty = "von Nassau"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1420.1.1 = {
	monarch = {
		name = "Johann II"
		dynasty = "von Nassau"
		DIP = 4
		ADM = 4
		MIL = 6
	}
}

1442.1.1 = {
	monarch = {
		name = "Johann IV"
		dynasty = "von Nassau"
		DIP = 6
		ADM = 7
		MIL = 3
	}
}

1475.1.1 = {
	monarch = {
		name = "Johann V"
		dynasty = "von Nassau"
		DIP = 4
		ADM = 5
		MIL = 8
	}
}

1516.1.1 = {
	monarch = {
		name = "Wilhelm der Reich"
		dynasty = "von Nassau"
		DIP = 4
		ADM = 4
		MIL = 5
	}
}

1559.1.1 = {
	monarch = {
		name = "Johann VI der Alter"
		dynasty = "von Nassau"
		DIP = 5
		ADM = 4
		MIL = 4
	}
}

1606.1.1 = {
	monarch = {
		name = "Wilhelm Ludwig" #Stadthouder of Frisia
		dynasty = "von Nassau"
		DIP = 4
		ADM = 4
		MIL = 7
	}
}

1620.1.1 = {
	monarch = {
		name = "Georg of Nassau-Beilstein"
		dynasty = "von Nassau"
		DIP = 4
		ADM = 5
		MIL = 4
	}
}
#Princes
1623.1.1 = {
	monarch = {
		name = "Ludwig Heinrich"
		dynasty = "von Nassau"
		adm = 6
		dip = 7
		mil = 5
	}
	government = absolute_monarchy
}

1662.1.1 = {
	monarch = {
		name = "Heinrich"
		dynasty = "von Nassau"
		adm = 4
		dip = 4
		mil = 5
	}
}

1701.1.1 = {
	monarch = {
		name = "Wilhelm"
		dynasty = "von Nassau"
		adm = 6
		dip = 7
		mil = 7
	}
}

1724.1.1 = {
	monarch = {
		name = "Christian"
		dynasty = "von Nassau"
		adm = 6
		dip = 6
		mil = 6
	}
}
#Nassau-Orange, Stadthouders of the Netherlands
1734.1.1 = {
	monarch = {
		name = "Willem IV"
		dynasty = "von Nassau"
		adm = 3
		dip = 4
		mil = 3
	}
}

1751.10.22 = {
	monarch = {
		name = "Willem V"
		dynasty = "von Nassau"
		adm = 5
		dip = 5
		mil = 4
	}
}

1806.6.5 = {
	monarch = {
		name = "Willem VI"
		dynasty = "von Nassau"
		adm = 6
		dip = 6
		mil = 5
	}
}

#Nassau-Weilburg
1814.1.1 = {
	monarch = {
		name = "Friedrich-Wilhelm II"
		dynasty = "von Nassau"
		adm = 6
		dip = 6
		mil = 5
	}
}

1816.1.1 = {
	monarch = {
		name = "Wilhelm"
		dynasty = "von Nassau"
		adm = 6
		dip = 6
		mil = 5
	}
}