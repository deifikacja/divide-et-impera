government = constitutional_republic
aristocracy_plutocracy = 1
centralization_decentralization = 3
innovative_narrowminded = -1
mercantilism_freetrade = -2
imperialism_isolationism = 0
secularism_theocracy = -2
offensive_defensive = 1
land_naval = -1
quality_quantity = 1
serfdom_freesubjects = 5
technology_group = western
religion = catholic
primary_culture = castillian
capital = 490	# Santo Domingo


1804.1.1 = {
	monarch = {
		name = "Jos� N�nez de C�ceres"
		adm = 4
		dip = 4
		mil = 7
	}
}
