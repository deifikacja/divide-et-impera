government = despotic_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -3
imperialism_isolationism = 1
secularism_theocracy = 1
offensive_defensive = 2
land_naval = -1
quality_quantity = 1
serfdom_freesubjects = -5
primary_culture = georgian
religion = eastern_churches
technology_group = muslim
capital = 423	# T'bilisi


1446.1.1 = {
	monarch = {
		name = "Giorgi VII"
		dynasty = "Bagrationi"
		adm = 3
		dip = 3
		mil = 6
	}
}

1465.1.1 = {
	monarch = {
		name = "Bagrat VI"
		dynasty = "Bagrationi"
		adm = 3
		dip = 4
		mil = 6
	}
}

1478.1.1 = {
	monarch = {
		name = "Konstantin� III"
		dynasty = "Bagrationi"
		adm = 6
		dip = 5
		mil = 5
	}
}

1505.1.1 = {
	monarch = {
		name = "Davit VII"
		dynasty = "Bagrationi"
		adm = 5
		dip = 6
		mil = 5
	}
}

1524.1.1 = {
	monarch = {
		name = "Giorgi IX"
		dynasty = "Bagrationi"
		adm = 5
		dip = 4
		mil = 4
	}
}

1534.1.1 = {
	monarch = {
		name = "Luarsab I"
		dynasty = "Bagrationi"
		adm = 6
		dip = 5
		mil = 7
	}
}

1723.5.2 = {
	monarch = {
		name = "Iese"
		dynasty = "Bagrationi"
		adm = 3
		dip = 4
		mil = 3
	}
}

1727.1.2 = {
	monarch = {
		name = "Konstantin� IV"
		dynasty = "Bagrationi"
		adm = 7
		dip = 6
		mil = 6
	}
}

1732.11.24 = {
	monarch = {
		name = "Teimuraz II"
		dynasty = "Bagrationi"
		adm = 8
		dip = 7
		mil = 7
		leader = {	name = "Teimuraz II"           	type = general	rank = 0	fire = 3	shock = 2	manuever = 3	siege = 0}
	}
}

1762.1.19 = {
	monarch = {
		name = "Irakli II"
		dynasty = "Bagrationi"
		adm = 8
		dip = 8
		mil = 9
		leader = {	name = "Irakli II"             	type = general	rank = 0	fire = 3	shock = 4	manuever = 5	siege = 1}
	}
}

1798.1.23 = {
	monarch = {
		name = "Giorgi XII"
		dynasty = "Bagrationi"
		adm = 3
		dip = 3
		mil = 5
	}
}

1801.1.10 = {
	monarch = {
		name = "Davit X"
		dynasty = "Bagrationi"
		adm = 4
		dip = 5
		mil = 4
	}
}

1819.5.26 = {
	monarch = {
		name = "Ioan�"
		dynasty = "Bagrationi"
		adm = 3
		dip = 4
		mil = 4
	}
}

