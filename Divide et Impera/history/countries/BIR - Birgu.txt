government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = -2
land_naval = -5
quality_quantity = 2
serfdom_freesubjects = -5
technology_group = african
religion = sunni
primary_culture = kanuri
capital = 1303	# Ouaddai

1550.1.1 = {
	monarch = {
		name = "Kolak"
		dynasty="Kolak"
		adm = 3
		dip = 4
		mil = 2
	}
}