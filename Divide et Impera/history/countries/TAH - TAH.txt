government = tribal_federation
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = 1
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = 0
land_naval = 5
quality_quantity = 0
serfdom_freesubjects = 0
primary_culture = tahitan
religion = animism
technology_group = pacific
capital = 1244 # Tahiti

#Ari`i rahi of Otaheiti
1350.1.1 = {
	monarch = {
		name = "Ari`i rahi"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}
1768.1.1 = {
	monarch = {
		name = "Vai ra`a toa T' Pomare I"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1803.9.3 = {
	monarch = {
		name = "Pomare II"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1808.12.22 = {
	monarch = {
		name = "Interregnum"
		DIP = 1
		ADM = 1
		MIL = 1
	}
}

1815.11.15 = {
	monarch = {
		name = "Pomare II"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1821.12.7 = {
	monarch = {
		name = "Teri`i tari`a Pomare III"
		DIP = 8
		ADM = 8
		MIL = 2
	}
}

1827.1.8 = {
	monarch = {
		name = "`Aimata Pomare IV Vahine"
		DIP = 6
		ADM = 5
		MIL = 2
		female = yes
	}
}