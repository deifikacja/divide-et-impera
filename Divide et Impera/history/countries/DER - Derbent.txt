government = eastern_despotism
aristocracy_plutocracy = -2
centralization_decentralization = 4
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = 0
land_naval = -4
quality_quantity = 4
serfdom_freesubjects = -2
primary_culture = azerbadjani
religion = shiite
technology_group = muslim
capital = 425	# Derbent

#Hashimid Dynasty
1747.1.1 = {
	monarch = {
		name = "Muhammad Hussein Khan"
		dynasty = Hashimid
		adm = 4
		dip = 7
		mil = 5
	}
}

1765.1.1 = {
	monarch = {
		name = "Tuti Bike"
		dynasty = Hashimid
		adm = 7
		dip = 6
		mil = 5
		female = yes
	}
}

1789.1.1 = {
	monarch = {
		name = "Sheyhali Khan"
		dynasty = Hashimid
		adm = 2
		dip = 4
		mil = 5
	}
}