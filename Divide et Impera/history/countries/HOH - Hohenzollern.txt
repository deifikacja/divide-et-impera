government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -3
offensive_defensive = -1
imperialism_isolationism = 0
secularism_theocracy = 0
land_naval = -1
quality_quantity = 0
serfdom_freesubjects = -4
technology_group = western
religion = catholic
primary_culture = swabian
capital = 2148	# Stuttgart

 

#Counts of Zollern
1344.1.1 = {
	monarch = {
		name = "Friedrich IX"
		dynasty = "von Hohenzollern"
		adm = 6
		dip = 6
		mil = 3
	}
}

1356.1.1 = {
	heir = {
		name = "Friedrich"
		monarch_name = "Friedrich XI"
		dynasty = "von Hohenzollern"
		birth_date = 1356.1.1
		death_date = 1401.1.1
		claim = 95
		adm = 6
		dip = 6
		mil = 3
	}
}

1377.1.1 = {
	monarch = {
		name = "Friedrich XI"
		dynasty = "von Hohenzollern"
		adm = 6
		dip = 6
		mil = 3
	}
}

1401.1.1 = {
	monarch = {
		name = "Friedrich XII"
		dynasty = "von Hohenzollern"
		adm = 6
		dip = 6
		mil = 3
	}
}

1426.1.1 = {
	monarch = {
		name = "Eitel Friedrich I"
		dynasty = "von Hohenzollern"
		adm = 6
		dip = 6
		mil = 3
	}
}
1439.1.1 = {
	monarch = {
		name = "Jobst Nikolaus"
		dynasty = "von Hohenzollern"
		adm = 6
		dip = 6
		mil = 3
	}
}
1488.1.1 = {
	monarch = {
		name = "Eitel Friedrich II"
		dynasty = "von Hohenzollern"
		adm = 6
		dip = 6
		mil = 3
	}
}
1512.1.1 = {
	monarch = {
		name = "Eitel Friedrich III"
		dynasty = "von Hohenzollern"
		adm = 6
		dip = 6
		mil = 3
	}
}
1525.1.1 = {
	monarch = {
		name = "Karl I"
		dynasty = "von Hohenzollern"
		adm = 6
		dip = 6
		mil = 3
	}
}
#Counts Hohenzollern-Hechingen
1575.1.1 = {
	monarch = {
		name = "Eitel Friedrich IV"
		dynasty = "von Hohenzollern"
		adm = 6
		dip = 6
		mil = 3
	}
}
#Princes of Hohenzollern-Hechinengen
1604.1.1 = {
	monarch = {
		name = "Johann Georg"
		dynasty = "von Hohenzollern"
		adm = 6
		dip = 6
		mil = 3
	}
}
1623.1.1 = {
	monarch = {
		name = "Eitel Friedrich V"
		dynasty = "von Hohenzollern"
		adm = 6
		dip = 6
		mil = 3
	}
}
1661.1.1 = {
	monarch = {
		name = "Philipp Friedrich Christopher"
		dynasty = "von Hohenzollern"
		adm = 6
		dip = 6
		mil = 3
	}
}
1671.1.1 = {
	monarch = {
		name = "Friedrich Wilhelm"
		dynasty = "von Hohenzollern"
		adm = 6
		dip = 6
		mil = 3
	}
}
1735.1.1 = {
	monarch = {
		name = "Friedrich Ludwig"
		dynasty = "von Hohenzollern"
		adm = 6
		dip = 6
		mil = 3
	}
}
1750.1.1 = {
	monarch = {
		name = "Josef Wilhelm Franz"
		dynasty = "von Hohenzollern"
		adm = 6
		dip = 6
		mil = 3
	}
}
1798.1.1 = {
	monarch = {
		name = "Herman Friedrich Otto"
		dynasty = "von Hohenzollern"
		adm = 6
		dip = 6
		mil = 3
	}
}
1810.1.1 = {
	monarch = {
		name = "Friedrich Hermann"
		dynasty = "von Hohenzollern"
		adm = 6
		dip = 6
		mil = 3
	}
}