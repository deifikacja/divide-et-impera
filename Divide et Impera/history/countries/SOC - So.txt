government = daimyo
aristocracy_plutocracy = -4
centralization_decentralization = 2
innovative_narrowminded = 2
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = -1
offensive_defensive = -1
land_naval = 0
quality_quantity = 4
serfdom_freesubjects = -5
primary_culture = japanese
religion = shinto
technology_group = chinese
capital = 1362	# Tsushima
daimyo = yes


1349.1.1 = { 
monarch = { 
name = "Tsunehige" 
dynasty = "So" 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "So Tsuneshige" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1366.1.1 } 
} 
} 


1349.1.1 = { 
heir = { 
name = "Sumishige" 
monarch_name = "Sumishige" 
dynasty = "So" 
birth_date = 1349.1.1 
death_date = 1370.1.1 
claim = 90 
adm = 5 
dip = 8 
mil = 7 
leader = { 
name = "Sumishige" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1366.1.1 = { 
monarch = { 
name = "Sumishige" 
dynasty = "So" 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "So Sumishige" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1370.1.1 } 
} 
} 


1366.1.1 = { 
heir = { 
name = "Yorishige" 
monarch_name = "Yorishige" 
dynasty = "So" 
birth_date = 1358.1.1 
death_date = 1402.1.1 
claim = 80 
adm = 5 
dip = 8 
mil = 7 
leader = { 
name = "Yorishige" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1370.1.1 = { 
monarch = { 
name = "Yorishige" 
dynasty = "So" 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "So Yorishige" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1402.1.1 } 
} 
} 


1370.1.1 = { 
heir = { 
name = "Sadashige" 
monarch_name = "Sadashige" 
dynasty = "So" 
birth_date = 1358.1.1 
death_date = 1419.1.1 
claim = 92 
adm = 4 
dip = 4 
mil = 6 
leader = { 
name = "Sadashige" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1402.1.1 = { 
monarch = { 
name = "Sadashige" 
dynasty = "So" 
adm = 4 
dip = 4 
mil = 6 
leader = { name = "So Sadashige" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1419.1.1 } 
} 
} 


1402.1.1 = { 
heir = { 
name = "Sadamori" 
monarch_name = "Sadamori" 
dynasty = "So" 
birth_date = 1387.1.1 
death_date = 1452.1.1 
claim = 89 
adm = 3 
dip = 3 
mil = 3 
leader = { 
name = "Sadamori" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1419.1.1 = { 
monarch = { 
name = "Sadamori" 
dynasty = "So" 
adm = 3 
dip = 3 
mil = 3 
leader = { name = "So Sadamori" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1452.1.1 } 
} 
} 


1419.1.1 = { 
heir = { 
name = "Shigemoto" 
monarch_name = "Shigemoto" 
dynasty = "So" 
birth_date = 1411.1.1 
death_date = 1468.1.1 
claim = 74 
adm = 5 
dip = 7 
mil = 5 
leader = { 
name = "Shigemoto" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1452.1.1 = { 
monarch = { 
name = "Shigemoto" 
dynasty = "So" 
adm = 5 
dip = 7 
mil = 5 
leader = { name = "So Shigemoto" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1468.1.1 } 
} 
} 


1452.1.1 = { 
heir = { 
name = "Sadakuni" 
monarch_name = "Sadakuni" 
dynasty = "So" 
birth_date = 1448.1.1 
death_date = 1492.1.1 
claim = 72 
adm = 5 
dip = 5 
mil = 3 
leader = { 
name = "Sadakuni" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1468.1.1 = { 
monarch = { 
name = "Sadakuni" 
dynasty = "So" 
adm = 5 
dip = 5 
mil = 3 
leader = { name = "So Sadakuni" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1462.1.1 } 
} 
} 


1468.1.1 = { 
heir = { 
name = "Kimori" 
monarch_name = "Kimori" 
dynasty = "So" 
birth_date = 1457.1.1 
death_date = 1505.1.1 
claim = 92 
adm = 5 
dip = 7 
mil = 4 
leader = { 
name = "Kimori" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1492.1.1 = { 
monarch = { 
name = "Kimori" 
dynasty = "So" 
adm = 5 
dip = 7 
mil = 4 
leader = { name = "So Kimori" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1505.1.1 } 
} 
} 


1492.1.1 = { 
heir = { 
name = "Yoshimori" 
monarch_name = "Yoshimori" 
dynasty = "So" 
birth_date = 1489.1.1 
death_date = 1520.1.1 
claim = 77 
adm = 4 
dip = 3 
mil = 5 
leader = { 
name = "Yoshimori" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1505.1.1 = { 
monarch = { 
name = "Yoshimori" 
dynasty = "So" 
adm = 4 
dip = 3 
mil = 5 
leader = { name = "So Yoshimori" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1520.1.1 } 
} 
} 


1505.1.1 = { 
heir = { 
name = "Morinaga" 
monarch_name = "Morinaga" 
dynasty = "So" 
birth_date = 1502.1.1 
death_date = 1526.1.1 
claim = 75 
adm = 6 
dip = 7 
mil = 7 
leader = { 
name = "Morinaga" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1520.1.1 = { 
monarch = { 
name = "Morinaga" 
dynasty = "So" 
adm = 6 
dip = 7 
mil = 7 
leader = { name = "So Morinaga" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1526.1.1 } 
} 
} 


1520.1.1 = { 
heir = { 
name = "Masamori" 
monarch_name = "Masamori" 
dynasty = "So" 
birth_date = 1511.1.1 
death_date = 1539.1.1 
claim = 90 
adm = 7 
dip = 5 
mil = 4 
leader = { 
name = "Masamori" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1526.1.1 = { 
monarch = { 
name = "Masamori" 
dynasty = "So" 
adm = 7 
dip = 5 
mil = 4 
leader = { name = "So Masamori" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1539.1.1 } 
} 
} 


1526.1.1 = { 
heir = { 
name = "Haruyasu" 
monarch_name = "Haruyasu" 
dynasty = "So" 
birth_date = 1525.1.1 
death_date = 1553.1.1 
claim = 84 
adm = 7 
dip = 5 
mil = 4 
leader = { 
name = "Haruyasu" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1539.1.1 = { 
monarch = { 
name = "Haruyasu" 
dynasty = "So" 
adm = 7 
dip = 5 
mil = 4 
leader = { name = "So Haruyasu" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1553.1.1 } 
} 
} 


1539.1.1 = { 
heir = { 
name = "Yoshishige" 
monarch_name = "Yoshishige" 
dynasty = "So" 
birth_date = 1539.1.1 
death_date = 1566.1.1 
claim = 79 
adm = 7 
dip = 5 
mil = 4 
leader = { 
name = "Yoshishige" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1553.1.1 = { 
monarch = { 
name = "Yoshishige" 
dynasty = "So" 
adm = 7 
dip = 5 
mil = 4 
leader = { name = "So Yoshishige" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1566.1.1 } 
} 
} 


1553.1.1 = { 
heir = { 
name = "Shigehisa" 
monarch_name = "Shigehisa" 
dynasty = "So" 
birth_date = 1548.1.1 
death_date = 1569.1.1 
claim = 90 
adm = 7 
dip = 5 
mil = 4 
leader = { 
name = "Shigehisa" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1566.1.1 = { 
monarch = { 
name = "Shigehisa" 
dynasty = "So" 
adm = 7 
dip = 5 
mil = 4 
leader = { name = "So Shigehisa" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1569.1.1 } 
} 
} 


1566.1.1 = { 
heir = { 
name = "Yoshizumi" 
monarch_name = "Yoshizumi" 
dynasty = "So" 
birth_date = 1555.1.1 
death_date = 1579.1.1 
claim = 72 
adm = 7 
dip = 5 
mil = 4 
leader = { 
name = "Yoshizumi" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1569.1.1 = { 
monarch = { 
name = "Yoshizumi" 
dynasty = "So" 
adm = 7 
dip = 5 
mil = 4 
leader = { name = "So Yoshizumi" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1579.1.1 } 
} 
} 


1569.1.1 = { 
heir = { 
name = "Terukage" 
monarch_name = "Terukage" 
dynasty = "So" 
birth_date = 1556.1.1 
death_date = 1589.1.1 
claim = 86 
adm = 7 
dip = 5 
mil = 4 
leader = { 
name = "Terukage" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1579.1.1 = { 
monarch = { 
name = "Terukage" 
dynasty = "So" 
adm = 7 
dip = 5 
mil = 4 
leader = { name = "So Terukage" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1589.1.1 } 
} 
} 



1589.1.1 = { 
monarch = { 
name = "Yoshishige" 
dynasty = "So" 
adm = 7 
dip = 5 
mil = 4 
leader = { name = "So Yoshishige" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1592.1.1 } 
} 
} 


1589.1.1 = { 
heir = { 
name = "Yoshitoshi" 
monarch_name = "Yoshitoshi" 
dynasty = "So" 
birth_date = 1584.1.1 
death_date = 1615.1.1 
claim = 94 
adm = 7 
dip = 5 
mil = 4 
leader = { 
name = "Yoshitoshi" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1592.1.1 = { 
monarch = { 
name = "Yoshitoshi" 
dynasty = "So" 
adm = 7 
dip = 5 
mil = 4 
leader = { name = "So Yoshitoshi" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1615.1.1 } 
} 
} 


1592.1.1 = { 
heir = { 
name = "Yoshinari" 
monarch_name = "Yoshinari" 
dynasty = "So" 
birth_date = 1584.1.1 
death_date = 1657.1.1 
claim = 84 
adm = 7 
dip = 5 
mil = 4 
leader = { 
name = "Yoshinari" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1615.1.1 = { 
monarch = { 
name = "Yoshinari" 
dynasty = "So" 
adm = 7 
dip = 5 
mil = 4 
leader = { name = "So Yoshinari" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1657.1.1 } 
} 
} 


1615.1.1 = { 
heir = { 
name = "Yoshizane" 
monarch_name = "Yoshizane" 
dynasty = "So" 
birth_date = 1610.1.1 
death_date = 1692.1.1 
claim = 92 
adm = 7 
dip = 5 
mil = 4 
leader = { 
name = "Yoshizane" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1657.1.1 = { 
monarch = { 
name = "Yoshizane" 
dynasty = "So" 
adm = 7 
dip = 5 
mil = 4 
leader = { name = "So Yoshizane" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1692.1.1 } 
} 
} 


1657.1.1 = { 
heir = { 
name = "Yoshitsugu" 
monarch_name = "Yoshitsugu" 
dynasty = "So" 
birth_date = 1657.1.1 
death_date = 1694.1.1 
claim = 75 
adm = 7 
dip = 5 
mil = 4 
leader = { 
name = "Yoshitsugu" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1692.1.1 = { 
monarch = { 
name = "Yoshitsugu" 
dynasty = "So" 
adm = 7 
dip = 5 
mil = 4 
leader = { name = "So Yoshitsugu" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1694.1.1 } 
} 
} 


1692.1.1 = { 
heir = { 
name = "Yoshimichi" 
monarch_name = "Yoshimichi" 
dynasty = "So" 
birth_date = 1687.1.1 
death_date = 1718.1.1 
claim = 83 
adm = 7 
dip = 5 
mil = 4 
leader = { 
name = "Yoshimichi" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1694.1.1 = { 
monarch = { 
name = "Yoshimichi" 
dynasty = "So" 
adm = 7 
dip = 5 
mil = 4 
leader = { name = "So Yoshimichi" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1718.1.1 } 
} 
} 


1694.1.1 = { 
heir = { 
name = "Yoshinobu" 
monarch_name = "Yoshinobu" 
dynasty = "So" 
birth_date = 1687.1.1 
death_date = 1730.1.1 
claim = 86 
adm = 7 
dip = 5 
mil = 4 
leader = { 
name = "Yoshinobu" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1718.1.1 = { 
monarch = { 
name = "Yoshinobu" 
dynasty = "So" 
adm = 7 
dip = 5 
mil = 4 
leader = { name = "So Yoshinobu" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1730.1.1 } 
} 
} 


1718.1.1 = { 
heir = { 
name = "Michihiro" 
monarch_name = "Michihiro" 
dynasty = "So" 
birth_date = 1715.1.1 
death_date = 1732.1.1 
claim = 75 
adm = 7 
dip = 5 
mil = 4 
leader = { 
name = "Michihiro" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1730.1.1 = { 
monarch = { 
name = "Michihiro" 
dynasty = "So" 
adm = 7 
dip = 5 
mil = 4 
leader = { name = "So Michihiro" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1732.1.1 } 
} 
} 


1730.1.1 = { 
heir = { 
name = "Yoshiyuki" 
monarch_name = "Yoshiyuki" 
dynasty = "So" 
birth_date = 1730.1.1 
death_date = 1752.1.1 
claim = 93 
adm = 7 
dip = 5 
mil = 4 
leader = { 
name = "Yoshiyuki" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1732.1.1 = { 
monarch = { 
name = "Yoshiyuki" 
dynasty = "So" 
adm = 7 
dip = 5 
mil = 4 
leader = { name = "So Yoshiyuki" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1752.1.1 } 
} 
} 



1752.1.1 = { 
monarch = { 
name = "Yoshishige" 
dynasty = "So" 
adm = 7 
dip = 5 
mil = 4 
leader = { name = "So Yoshishige" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1762.1.1 } 
} 
} 


1752.1.1 = { 
heir = { 
name = "Yoshinaga" 
monarch_name = "Yoshinaga" 
dynasty = "So" 
birth_date = 1745.1.1 
death_date = 1778.1.1 
claim = 86 
adm = 7 
dip = 5 
mil = 4 
leader = { 
name = "Yoshinaga" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1762.1.1 = { 
monarch = { 
name = "Yoshinaga" 
dynasty = "So" 
adm = 7 
dip = 5 
mil = 4 
leader = { name = "So Yoshinaga" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1778.1.1 } 
} 
} 


1762.1.1 = { 
heir = { 
name = "Yoshikatsu" 
monarch_name = "Yoshikatsu" 
dynasty = "So" 
birth_date = 1760.1.1 
death_date = 1785.1.1 
claim = 75 
adm = 7 
dip = 5 
mil = 4 
leader = { 
name = "Yoshikatsu" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1778.1.1 = { 
monarch = { 
name = "Yoshikatsu" 
dynasty = "So" 
adm = 7 
dip = 5 
mil = 4 
leader = { name = "So Yoshikatsu" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1785.1.1 } 
} 
} 



1785.1.1 = { 
monarch = { 
name = "Yoshikatsu" 
dynasty = "So" 
adm = 7 
dip = 5 
mil = 4 
leader = { name = "So Yoshikatsu" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1812.1.1 } 
} 
} 


1785.1.1 = { 
heir = { 
name = "Yoshitada" 
monarch_name = "Yoshitada" 
dynasty = "So" 
birth_date = 1779.1.1 
death_date = 1805.1.1 
claim = 71 
adm = 7 
dip = 5 
mil = 4 
leader = { 
name = "Yoshitada" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 



1812.1.1 = { 
monarch = { 
name = "Yoshitada" 
dynasty = "So" 
adm = 7 
dip = 5 
mil = 4 
leader = { name = "So Yoshitada" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 death_date = 1838.1.1 } 
} 
} 

