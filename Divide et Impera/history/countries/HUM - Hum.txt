government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -3
imperialism_isolationism = 0
secularism_theocracy = 1
offensive_defensive = 2
land_naval = -1
quality_quantity = 1
serfdom_freesubjects = -5
primary_culture = bosnian
religion = greek_orthodox
technology_group = eastern
capital = 139	# Mostar

 

1350.1.1 = {
	monarch = {
		name = "Vuk"
		dynasty = Kosa�a
		adm = 3
		dip = 4
		mil = 4
	}
}

1356.1.1 = {
	heir = {
		name = "Vlatko Vukovi�"
		monarch_name = "Vlatko Vukovi�"
		dynasty = Kosa�a
		birth_date = 1330.1.1
		death_date = 1392.1.1
		claim = 95
		adm = 4
		dip = 3
		mil = 9
		leader = { name = "Vlatko Vukovi�"	type = general	rank = 0	fire = 2	shock = 2	manuever = 3	siege = 0 }
	}
}

1356.1.1 = {
	heir = {
		name = "Hranj Vukovi�"
		monarch_name = "Vlatko Vukovi�"
		dynasty = Kosa�a
		birth_date = 1356.1.1
		death_date = 1392.1.1
		claim = 95
		adm = 4
		dip = 3
		mil = 3
	}
}

1359.1.1 = {
	monarch = {
		name = "Vlatko Vukovi�"
		dynasty = Kosa�a
		adm = 4
		dip = 3
		mil = 3
		leader = { name = "Vlatko Vukovi�"	type = general	rank = 0	fire = 2	shock = 2	manuever = 3	siege = 0 }
	}
}

1370.1.1 = {
	heir = {
		name = "Sandalj Hrani�"
		monarch_name = "Sandalj Hrani�"
		dynasty = Kosa�a
		birth_date = 1370.1.1
		death_date = 1435.3.15
		claim = 95
		adm = 4
		dip = 5
		mil = 3
	}
}

1392.1.1 = {
	monarch = {
		name = "Sandalj Hrani�"
		dynasty = Kosa�a
		adm = 4
		dip = 5
		mil = 3
	}
}

1392.1.1 = {
	heir = {
		name = "Stjepan Vukci�"
		monarch_name = "Stjepan Vukci�"
		dynasty = Kosa�a
		birth_date = 1392.1.1
		death_date = 1466.1.1
		claim = 95
		adm = 4
		dip = 5
		mil = 3
	}
}

1435.3.15 = {
	monarch = {
		name = "Stjepan Vukci�"
		dynasty = Kosa�a
		adm = 3
		dip = 3
		mil = 4
	}
}

1392.1.1 = {
	heir = {
		name = "Vladislav Hercegovi�"
		monarch_name = "Vladislav Hercegovi�"
		dynasty = Kosa�a
		birth_date = 1426.1.1
		death_date = 1490.1.1
		claim = 95
		adm = 6
		dip = 5
		mil = 5
	}
}

1466.1.1 = {
	monarch = {
		name = "Vladislav Hercegovi�"
		dynasty = Kosa�a
		adm = 6
		dip = 5
		mil = 5
	}
}