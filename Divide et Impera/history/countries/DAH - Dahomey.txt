government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = -1
land_naval = -5
quality_quantity = 2
serfdom_freesubjects = -5
technology_group = african
religion = animism
primary_culture = yorumba
capital = 1140	# Abomey


1600.1.1 = {
	monarch = {
		name = "Gangnihessou"
		dynasty = Agasuvi
		adm = 8
		dip = 4
		mil = 5
	}
}

1620.1.1 = {
	monarch = {
		name = "Dakodonou"
		dynasty = Agasuvi
		adm = 3
		dip = 3
		mil = 4
	}
}

1645.1.1 = {
	monarch = {
		name = "Houegbadja"
		dynasty = Agasuvi
		adm = 4
		dip = 5
		mil = 3
	}
}

1685.1.1 = {
	monarch = {
		name = "Akaba"
		dynasty = Agasuvi
		adm = 3
		dip = 3
		mil = 2
	}
}

1708.1.1 = {
	monarch = {
		name = "Agadja"
		dynasty = Agasuvi
		adm = 4
		dip = 6
		mil = 4
	}
}

1732.1.1 = {
	monarch = {
		name = "Tegbessou"
		dynasty = Agasuvi
		adm = 3
		dip = 3
		mil = 4
	}
}

1774.1.1 = {
	monarch = {
		name = "Kpengla"
		dynasty = Agasuvi
		adm = 3
		dip = 3
		mil = 2
	}
}

1789.1.1 = {
	monarch = {
		name = "Agonglo"
		dynasty = Agasuvi
		adm = 1
		dip = 1
		mil = 1
	}
}

1797.1.1 = {
	monarch = {
		name = "Adandozan"
		dynasty = Agasuvi
		adm = 3
		dip = 3
		mil = 2
	}
}

1818.1.1 = {
	monarch = {
		name = "Ghezo"
		dynasty = Agasuvi
		adm = 3
		dip = 3
		mil = 2
	}
}
