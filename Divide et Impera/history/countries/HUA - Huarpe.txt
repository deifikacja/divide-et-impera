government = tribal_federation
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 5
mercantilism_freetrade = -3
imperialism_isolationism = 0
secularism_theocracy = -2
offensive_defensive = 0
land_naval = -5
quality_quantity = 4
serfdom_freesubjects = -5
primary_culture = mapudungun
religion = animism
technology_group = new_world
capital = 788	# Cuyo


1356.1.1 = {
	monarch = {
		name = "Chief of Huarpe"
		adm = 5
		dip = 5
		mil = 5
	}
}

