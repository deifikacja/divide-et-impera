government = steppe_horde
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 3
mercantilism_freetrade = -3
offensive_defensive = -1
imperialism_isolationism = -1
secularism_theocracy = -1
land_naval = -5
quality_quantity = 3
serfdom_freesubjects = -2
technology_group = muslim
religion = sunni
primary_culture = uzbehk
capital = 454	# Samarkand

1447.3.12 = {
	monarch = {
		name = "Ulugh Beg"
		dynasty = Timurid
		adm = 7	
		dip = 5	
		mil = 4	
	}
centralization_decentralization = 3 # Assassinated, empire crumbles
imperialism_isolationism = 0 secularism_theocracy = 0 # One of the greatest astronomers ever. So what? :P
}

1449.10.27 = {
	monarch = {
		name = "'Abd ul-Latif"
		dynasty = Timurid
		adm = 4	
		dip = 5	
		mil = 3	
	}
centralization_decentralization = 4
}

1450.5.10 = {
	monarch = {
		name = "'Abd All�h M�rz�"
		dynasty = Timurid
		adm = 3	
		dip = 3	
		mil = 5	
	}
centralization_decentralization = 5
}

1453.1.1 = { set_country_flag = total_war_series set_country_flag = total_war }

1457.6.23 = {
	monarch = {
		name = "Ab� Sa'id"
		dynasty = Timurid
		adm = 3	
		dip = 5	
		mil = 7	
		leader = {	name = "Ab� Sa'id"             	type = general	rank = 0	fire = 3	shock = 3	manuever = 3	siege = 0 }
	}
centralization_decentralization = 3 #united Transoxiana and Khorasan, last ruler of empire as a whole
}

1469.8.27 = {
	monarch = {
		name = "Ahmad"	
		dynasty = Timurid
		adm = 3	
		dip = 3	
		mil = 5	
	}
centralization_decentralization = 5
}

1494.7.1 = {
	monarch = {
		name = "Mahmud"	
		dynasty = Timurid
		adm = 3	
		dip = 3	
		mil = 3	
	}
}

1495.7.1 = {
	monarch = {
		name = "Mas'ud"	
		dynasty = Timurid
		adm = 3	
		dip = 3	
		mil = 3	
	}
}

1495.9.1 = {
	monarch = {
		name = "B�y Sunqur"
		dynasty = Timurid
		adm = 3	
		dip = 3	
		mil = 3
	}
}

1499.8.1 = {
	monarch = {
		name = "'Ali"
		dynasty = Timurid
		adm = 3	
		dip = 3	
		mil = 3	
	}
}

1500.4.1 = {
	monarch = {
		name = "B�bur"
		dynasty = Timurid
		adm = 4	
		dip = 4	
		mil = 8	
	}
#Not as good, as leter ruler of India - incompetent and unpopular, routed by Uzbekhs, lost Samarkhand permanently
}

1504.4.1 = { quality_quantity = 0 add_accepted_culture = panjabi } # Babur's Ambition
#Utilized as Shaybanid Samarkanda
1510.1.1 = { 
	monarch = { 
		name = "Kochkunju" 
		dynasty = Shaybanid
		dip = 3 
		adm = 4 
		mil = 5 
	}
}

1524.1.1 = { 
	monarch = { 
		name = "Baraq Khan" 
		dynasty = Shaybanid
		dip = 5 
		adm = 5 
		mil = 7 
	}
} 

1531.1.1 = { 
	monarch = { 
		name = "Muzzafar ad-Din" 
		dynasty = Shaybanid
		dip = 4 
		adm = 4 
		mil = 5 
	}	
} 

1534.1.1 = { 
	monarch = { 
		name = "Baraq Khan" 
		dynasty = Shaybanid
		dip = 5 
		adm = 5 
		mil = 9
	}
} 

1556.1.1 = { 
	monarch = { 
		name = "Pir Muhhamad I" 
		dynasty = Shaybanid
		dip = 4 
		adm = 3 
		mil = 3 
	}
} 

1561.1.1 = { 
	monarch = {
		name = "Khusrau Sultan" 
		dynasty = Shaybanid
		dip = 4 
		adm = 4 
		mil = 4 
	}
} 

1567.1.1 = { 
	monarch = {
		name = "Sultan Sa'id" 
		dynasty = Shaybanid
		dip = 5 
		adm = 5 
		mil = 7 
	}
} 

1572.1.1 = { 
	monarch = {
		name = "Djuwanmard 'Ali" 
		dynasty = Shaybanid
		dip = 3 
		adm = 3 
		mil = 3 
	}
} 

#To Shaibanids of Bokhara