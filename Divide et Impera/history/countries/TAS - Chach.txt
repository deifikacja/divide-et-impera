government = steppe_horde
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 3
mercantilism_freetrade = -3
imperialism_isolationism = -1
secularism_theocracy = -1
offensive_defensive = 0
land_naval = -5
quality_quantity = 3
serfdom_freesubjects = -2
technology_group = muslim
religion = sunni
primary_culture = uzbehk
capital = 457	# Tashkent


1510.1.1 = { 
	monarch = { 
		name = "Suunjiq Hodja" 
		dynasty = Shaybanid
		dip = 3 
		adm = 4 
		mil = 5 
	}
}

1524.1.1 = { 
	monarch = { 
		name = "Baraq Khan" 
		dynasty = Shaybanid
		dip = 5 
		adm = 5 
		mil = 7 
	}
} 

1582.1.1 = { 
	monarch = { 
		name = "Baba Khan" 
		dynasty = Shaybanid
		dip = 4 
		adm = 4 
		mil = 5 
	}	
} 

1597.1.1 = { 
	monarch = { 
		name = "Tawakel ibn Haq Nazar" 
		dynasty = Shaybanid
		dip = 5 
		adm = 5 
		mil = 9
	}
} 

1613.1.1 = { 
	monarch = { 
		name = "Tursyn" 
		dynasty = Shaybanid
		dip = 4 
		adm = 3 
		mil = 3 
	}
} 

1723.1.1 = { 
	monarch = {
		name = "Zholbarys" 
		dynasty = Shaybanid
		dip = 4 
		adm = 4 
		mil = 4 
	}
} 

1784.1.1 = { 
	monarch = {
		name = "Yunus" 
		dynasty = Shaybanid
		dip = 5 
		adm = 5 
		mil = 7 
	}
} 

#To Shaibanids of Kokhand