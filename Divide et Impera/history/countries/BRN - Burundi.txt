government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -2
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 1
land_naval = -5
quality_quantity = 2
serfdom_freesubjects = -5
primary_culture = nilotic
religion = shamanism
technology_group = african
capital = 1293	# Burundi

1356.1.1 = {
	monarch = {
		name = "Dynasty"
		dynasty = Karemera
		adm = 4
		dip = 5
		mil = 6
	}
}

1450.1.1 = {
	monarch = {
		name = "Dynasty"
		dynasty = Baganwa
		adm = 4
		dip = 5
		mil = 6
	}
}

1680.1.1 = {
	monarch = {
		name = "Ntare III Rushatsi"
		dynasty = Baganwa
		adm = 5
		dip = 5
		mil = 7
	}
}

1709.1.1 = {
	monarch = {
		name = "Mwezi III Ndagushimiye"
		dynasty = Baganwa
		adm = 6
		dip = 6
		mil = 5
	}
}

1739.1.1 = {
	monarch = {
		name = "Muraga III Senyamwiza"
		dynasty = Baganwa
		adm = 4
		dip = 4
		mil = 4
	}
}

1767.1.1 = {
	monarch = {
		name = "Mwambutsa III Syarushambo Butama"
		dynasty = Baganwa
		adm = 5
		dip = 4
		mil = 5
	}
}

1796.1.1 = {
	monarch = {
		name = "Ntare IV Rutaganzwa Rugamba"
		dynasty = Baganwa
		adm = 4
		dip = 4
		mil = 7
	}
}