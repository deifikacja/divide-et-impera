government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 4
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = -1
land_naval = -3
quality_quantity = 4
serfdom_freesubjects = -4
primary_culture = inca
religion = animism
technology_group = new_world
capital = 806	# Nazca


1352.1.1 = {
	monarch = {
		name = "Huama"
		adm = 5
		dip = 5
		mil = 5
	}
}

