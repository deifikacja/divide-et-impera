government = daimyo
aristocracy_plutocracy = -4
centralization_decentralization = 6
innovative_narrowminded = 2
mercantilism_freetrade = -5
imperialism_isolationism = -1
secularism_theocracy = -1
offensive_defensive = -5
land_naval = 0
quality_quantity = 4
serfdom_freesubjects = -5
primary_culture = japanese
religion = shinto
technology_group = chinese
capital = 1350	# Hida
daimyo = yes

1582.6.21 = { 
monarch = { 
name = "Hideyoshi" 
dynasty = Toyotomi 
adm = 8 
dip = 7 
mil = 9 
leader = { name = "Toyotomi Hideyoshi" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1582.6.21 = {
    heir = {
        name = "Hideyori"
        monarch_name =  "Hideyori" 
        dynasty =  Toyotomi 
        birth_date = 1575.1.1
        death_date = 1592.1.1
        claim = 83
        adm = 5
        dip = 6
        mil = 5
   }
}

1584.1.1 = { set_country_flag = civil_war } #Iyeasu supports Nobukatsu against Hideyoshi 

1589.1.1 = { clr_country_flag = civil_war } #Peace arranged 

1598.9.18 = { set_country_flag = civil_war } #Iyeasu supports Nobukatsu against Hideyoshi 

1598.9.18 = { 
monarch = { 
name = "Hideyori" 
dynasty = Toyotomi 
adm = 5 
dip = 6 
mil = 5 
} 
} 

1600.10.21 = { clr_country_flag = civil_war } #Sekihagara 

