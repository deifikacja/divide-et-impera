government = tribal_despotism
aristocracy_plutocracy = -2
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 0
land_naval = -2
quality_quantity = 3
serfdom_freesubjects = -2
primary_culture = mali
religion = sunni
technology_group = sahel
capital = 1116	# Wolof


#Buur-ba Jolof

1549.1.1 = {
	monarch = {
		name = "Detye Fu-N'-Diogu"
		dynasty = Damel
		DIP = 3
		ADM = 5
		MIL = 4
	}
}

1549.5.1 = {
	monarch = {
		name = "Amari I"
		dynasty = Damel
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1593.1.1 = {
	monarch = {
		name = "Samba I"
		dynasty = Damel
		DIP = 4
		ADM = 4
		MIL = 4
	}
}

1600.1.1 = {
	monarch = {
		name = "Khuredya"
		dynasty = Damel
		DIP = 6
		ADM = 5
		MIL = 5
	}
}

1610.1.1 = {
	monarch = {
		name = "Biram Manga"
		dynasty = Damel
		DIP = 6
		ADM = 5
		MIL = 4
	}
}

1640.1.1 = {
	monarch = {
		name = "Dauda Demba"
		dynasty = Damel
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1647.1.1 = {
	monarch = {
		name = "Dyor"
		dynasty = Damel
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1664.1.1 = {
	monarch = {
		name = "Birayma Yaasin-Bubu"
		dynasty = Damel
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1681.1.1 = {
	monarch = {
		name = "Detye Maram N'Galgu"
		dynasty = Damel
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1683.1.1 = {
	monarch = {
		name = "Faly"
		dynasty = Damel
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1684.1.1 = {
	monarch = {
		name = "Khureda Kumba Dyodyo"
		dynasty = Damel
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1691.1.1 = {
	monarch = {
		name = "Birayma Mbenda-Tyilor"
		dynasty = Damel
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1693.1.1 = {
	monarch = {
		name = "Dyakhere"
		dynasty = Damel
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1697.1.1 = {
	monarch = {
		name = "Lat Sukaabe"
		dynasty = Damel
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1719.1.1 = {
	monarch = {
		name = "Isa-Tende"
		dynasty = Damel
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1748.1.1 = {
	monarch = {
		name = "Isa Bige N'Gone"
		dynasty = Damel
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1749.1.1 = {
	monarch = {
		name = "M'Batho Samb"
		dynasty = Damel
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1757.1.1 = {
	monarch = {
		name = "Birayma Kodu"
		dynasty = Damel
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1758.1.1 = {
	monarch = {
		name = "Isa Bige N'Gone"
		dynasty = Damel
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1759.1.1 = {
	monarch = {
		name = "Birayma Yamb"
		dynasty = Damel
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1760.1.1 = {
	monarch = {
		name = "Isa Bige N'Gone"
		dynasty = Damel
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1763.1.1 = {
	monarch = {
		name = "Dyor Yaasin Isa"
		dynasty = Damel
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1766.1.1 = {
	monarch = {
		name = "Kudu Kumba"
		dynasty = Damel
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1777.1.1 = {
	monarch = {
		name = "Birayma Faata-Penda"
		dynasty = Damel
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1790.1.1 = {
	monarch = {
		name = "Amari II"
		dynasty = Damel
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1809.1.1 = {
	monarch = {
		name = "Birayma Fatma"
		dynasty = Damel
		DIP = 5
		ADM = 5
		MIL = 5
	}
}