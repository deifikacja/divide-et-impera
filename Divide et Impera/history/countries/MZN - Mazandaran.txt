government = eastern_despotism
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 1
offensive_defensive = 1
land_naval = -1
quality_quantity = 1
serfdom_freesubjects = -4
primary_culture = al_iraqiya_arabic
religion = sunni
technology_group = muslim
capital = 426	# Iraq-i-arab

1334.1.1 = {
	monarch = {
		name = "Jalal ud-Din Iskandar I"
		dynasty = Baduspanid
		adm = 5
		dip = 6
		mil = 5
	}
}

1356.1.1 = {
	heir = {
		name = "Fakr ud-Dawlah Shah Ghazi"
		monarch_name = "Fakr ud-Dawlah Shah Ghazi"
		dynasty = Baduspanid
		birth_date = 1356.1.1
		death_date = 1379.5.1
		claim = 95
		adm = 5
		dip = 6
		mil = 5
	}
}

1360.1.1 = {
	monarch = {
		name = "Fakr ud-Dawlah Shah Ghazi"
		dynasty = Baduspanid
		adm = 5
		dip = 6
		mil = 5
	}
}

1379.5.1 = {
	monarch = {
		name = "'Adud ud-Dawlah Qubad"
		dynasty = Baduspanid
		adm = 5
		dip = 6
		mil = 5
	}
}
1399.1.1 = {
	monarch = {
		name = "Sa'ad ud-Dawlah Tus"
		dynasty = Baduspanid
		adm = 5
		dip = 6
		mil = 5
	}
}

1404.1.1 = {
	monarch = {
		name = "Kayumarth I"
		dynasty = Baduspanid
		adm = 4
		dip = 3
		mil = 3
	}
}
#1453 partitioned between Nur and Kujur. Rulers of Nur

1453.3.1 = {
	monarch = {
		name = "Kai-Ka'us II"
		dynasty = Baduspanid
		adm = 3
		dip = 3
		mil = 3
	}
}

1467.1.1 = {
	monarch = {
		name = "Jahangir I"
		dynasty = Baduspanid
		adm = 4
		dip = 4
		mil = 6
	}
}


1499.1.1 = {
	monarch = {
		name = "Bisutun II"
		dynasty = Baduspanid
		adm = 3
		dip = 3
		mil = 3
	}
}


1507.5.1 = {
	monarch = {
		name = "Bahman"
		dynasty = Baduspanid
		adm = 4
		dip = 3
		mil = 4
	}
}

1507.5.1 = {
	monarch = {
		name = "Bahman"
		dynasty = Baduspanid
		adm = 4
		dip = 3
		mil = 4
	}
}

1550.5.1 = {
	monarch = {
		name = "Kayumarth II"
		dynasty = Baduspanid
		adm = 4
		dip = 3
		mil = 4
	}
}

1570.5.1 = {
	monarch = {
		name = "Sultan Aziz"
		dynasty = Baduspanid
		adm = 4
		dip = 3
		mil = 4
	}
}

1580.5.1 = {
	monarch = {
		name = "Jahangir II"
		dynasty = Baduspanid
		adm = 4
		dip = 3
		mil = 4
	}
}
#To Persia after 1594