government = steppe_horde
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 3
mercantilism_freetrade = -2
imperialism_isolationism = -1
secularism_theocracy = -1
offensive_defensive = -4
land_naval = -5
quality_quantity = 3
serfdom_freesubjects = -5
primary_culture = khazak
religion = sunni
technology_group = ordu
capital = 471	# Turkestan


1718.1.1 = {
	monarch = {
		name = "Muhammad Abu'l-Khair"
		dynasty = "Khazakh"
		adm = 4
		dip = 3
		mil = 5
	}
}

1748.1.1 = {
	monarch = {
		name = "Abu'l Faiz ibn Abul Mambet"
		dynasty = "Khazakh"
		adm = 4
		dip = 7
		mil = 6
	}
}

1768.1.1 = {
	monarch = {
		name = "Nurali"
		dynasty = "Khazakh"
		adm = 3
		dip = 4
		mil = 7
	}
}

1790.1.1 = {
	monarch = {
		name = "Yesim II"
		dynasty = "Khazakh"
		adm = 5
		dip = 4
		mil = 5
	}
}

1791.1.1 = {
	monarch = {
		name = "Yeraly"
		dynasty = "Khazakh"
		adm = 4
		dip = 4
		mil = 4
	}
}

1794.1.1 = {
	monarch = {
		name = "Yesim III"
		dynasty = "Khazakh"
		adm = 4
		dip = 4
		mil = 4
	}
}

1797.1.1 = {
	monarch = {
		name = "Aishuak"
		dynasty = "Khazakh"
		adm = 4
		dip = 4
		mil = 4
	}
}

1803.1.1 = {
	monarch = {
		name = "Zhantore"
		dynasty = "Khazakh"
		adm = 4
		dip = 4
		mil = 4
	}
}

1806.1.1 = {
	monarch = {
		name = "Karatai"
		dynasty = "Khazakh"
		adm = 4
		dip = 4
		mil = 4
	}
}

1812.1.1 = {
	monarch = {
		name = "Sergazy"
		dynasty = "Khazakh"
		adm = 4
		dip = 4
		mil = 4
	}
}