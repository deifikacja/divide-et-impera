government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -3
imperialism_isolationism = -1
secularism_theocracy = 1
offensive_defensive = 0
land_naval = -1
quality_quantity = 1
serfdom_freesubjects = -5
primary_culture = wallachian
add_accepted_culture = moldavian
religion = greek_orthodox
technology_group = eastern
capital = 161	# TÓrgoviste

 

1650.1.1 = { government = administrative_monarchy }
