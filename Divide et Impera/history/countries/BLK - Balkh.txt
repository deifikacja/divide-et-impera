government = steppe_horde
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 3
mercantilism_freetrade = -3
imperialism_isolationism = 0
secularism_theocracy = -1
offensive_defensive = -3
land_naval = -5
quality_quantity = 3
serfdom_freesubjects = -5
primary_culture = tartar
religion = sunni
technology_group = muslim
capital = 450	# Balkh


1599.1.1 = {
	monarch = {
		name = "Wali Muhammad"
		dynasty=Astrakhanid
		adm = 4
		dip = 4
		mil = 5
	}
}

1611.1.1 = {
	monarch = {
		name = "Nadir Muhammad"
		dynasty=Astrakhanid
		adm = 5
		dip = 4
		mil = 6
	}
}

1651.1.1 = {
	monarch = {
		name = "Subhan Quli"
		dynasty=Astrakhanid
		adm = 5
		dip = 5
		mil = 3
	}
}