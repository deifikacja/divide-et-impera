government = eastern_despotism
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = -1
offensive_defensive = -2
land_naval = -1
quality_quantity = -1
serfdom_freesubjects = -1
primary_culture = turkish
religion = sunni
technology_group = ottoman
capital = 325	# Kastamonu


1345.6.30 = {
	monarch = {
		name = "'Adil"
		dynasty = Candaroglu
		adm = 6
		dip = 5
		mil = 6
	}
}

1361.2.2 = {
	monarch = {
		name = "Bayezid K�t�r�m"
		dynasty = Candaroglu
		adm = 4
		dip = 4
		mil = 4
	}
}

1385.4.10 = {
	monarch = {
		name = "Isfandiyar"
		dynasty = Candaroglu
		adm = 3
		dip = 7
		mil = 4
	}
}

# 1392-1402 to the Ottoman Empire
1398.1.1 = { capital = 328 }#Sinope
1402.9.1 = {
	monarch = {
		name = "Isfandiyar"
		dynasty = Candaroglu
		adm = 3
		dip = 7
		mil = 4
	}
} # restored by Tamerlan

1440.1.1 = {
	monarch = {
		name = "Ibrahim II"
		dynasty = Candaroglu
		adm = 3
		dip = 4
		mil = 4
	}
}

1443.1.1 = {
	monarch = {
		name = "Ism�'�l"
		dynasty = Candaroglu
		adm = 5
		dip = 6
		mil = 5
	}
}

1461.1.1 = {
	monarch = {
		name = "Qyzyl Ahmad"
		dynasty = Candaroglu
		adm = 6
		dip = 4
		mil = 3
	}
}

