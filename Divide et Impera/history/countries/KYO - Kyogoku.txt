government = daimyo
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -5
offensive_defensive = -1
imperialism_isolationism = 0
secularism_theocracy = -1
land_naval = 0
quality_quantity = 4
serfdom_freesubjects = -5
primary_culture = japanese
religion = shinto
technology_group = chinese
capital = 1018	# Izumo
daimyo = yes


1356.1.1 = { 
monarch = { 
name = "" 
dynasty = Kyogoku 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Kyogoku" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1581.1.1 = { 
monarch = { 
name = "Takatsugu" 
dynasty = Kyogoku 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Kyogoku Takatsugu" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1609.1.1 = { 
monarch = { 
name = "Tadataka" 
dynasty = Kyogoku 
adm = 4 
dip = 4 
mil = 6 
leader = { name = "Kyogoku Tadataka" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 
