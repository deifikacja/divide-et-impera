government = tribal_despotism
aristocracy_plutocracy = -2
centralization_decentralization = 1
innovative_narrowminded = 0
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = 3
land_naval = -5
quality_quantity = 0
serfdom_freesubjects = 3
offensive_defensive = -3
primary_culture = mississippian
religion = animism
technology_group = new_world
capital = 929	#Etowah

1350.1.1 = {
	monarch = {
		name = "Chief of Etowah"
		adm = 6
		dip = 6
		mil = 5
	}
}
