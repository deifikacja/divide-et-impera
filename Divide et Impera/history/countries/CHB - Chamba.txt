government = caste_monarchy
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -4
secularism_theocracy = -1
offensive_defensive = 2
land_naval = -3
quality_quantity = -5
serfdom_freesubjects = -2
primary_culture = panjabi
religion = hinduism
technology_group = indian
capital = 1976	# Chamba


#Paramanabhattaraka Maharajadhiraja
920.1.1 = {
	monarch = {
		name = "Raja Sahil Verma"
		dynasty = Mushana
		adm = 7
		dip = 3
		mil = 3
	}
}
1475.1.1 = {
	monarch = {
		name = "Anand Verma"
		dynasty = Mushana
		adm = 7
		dip = 3
		mil = 3
	}
}
1512.1.1 = {
	monarch = {
		name = "Ganesa Verma"
		dynasty = Mushana
		adm = 7
		dip = 3
		mil = 3
	}
}
1559.1.1 = {
	monarch = {
		name = "Pratapsingh Verma"
		dynasty = Mushana
		adm = 7
		dip = 3
		mil = 3
	}
}

1586.1.1 = {
	monarch = {
		name = "Vir Vahnu Verma"
		dynasty = Mushana
		adm = 7
		dip = 3
		mil = 3
	}
}
1589.1.1 = {
	monarch = {
		name = "Balbhadra Verma"
		dynasty = Mushana
		adm = 7
		dip = 3
		mil = 3
	}
}
1641.1.1 = {
	monarch = {
		name = "Prithvi Singh"
		dynasty = Mushana
		adm = 7
		dip = 3
		mil = 3
	}
}
1664.1.1 = {
	monarch = {
		name = "Chhatar Singh"
		dynasty = Mushana
		adm = 7
		dip = 3
		mil = 3
	}
}
1690.1.1 = {
	monarch = {
		name = "Udai Singh"
		dynasty = Mushana
		adm = 3
		dip = 4
		mil = 7
	}
}
1720.1.1 = {
	monarch = {
		name = "Ugra Singh"
		dynasty = Mushana
		adm = 5
		dip = 3
		mil = 4
	}
}
1734.1.1 = {
	monarch = {
		name = "Dalel Singh"
		dynasty = Mushana
		adm = 9
		dip = 7
		mil = 6
	}
}

1748.1.1 = {
	monarch = {
		name = "Umed Singh"
		dynasty = Mushana
		adm = 5
		dip = 5
		mil = 5
	}
}

1764.5.13 = {
	monarch = {
		name = "Raj Singh"
		dynasty = Mushana
		adm = 5
		dip = 5
		mil = 5
	}
}

1794.1.1 = {
	monarch = {
		name = "Jit Singh"
		dynasty = Mushana
		adm = 3
		dip = 3
		mil = 6
	}
}
#Raja Shri
1808.1.1 = {
	monarch = {
		name = "Charhat Singh"
		dynasty = Mushana
		adm = 3
		dip = 4
		mil = 5
	}
}