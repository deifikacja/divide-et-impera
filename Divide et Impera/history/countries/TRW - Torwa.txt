government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 0
land_naval = -5
quality_quantity = 2
serfdom_freesubjects = -5
primary_culture = shona
religion = shamanism
technology_group = african
capital = 1277	# Khami


1430.1.1 = {
	monarch = {
		name = "Dlembeu"
		dynasty = Torwa
		adm = 4
		dip = 5
		mil = 6
	}
}