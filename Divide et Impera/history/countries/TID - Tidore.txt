government = caste_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 0
innovative_narrowminded = 2
mercantilism_freetrade = 0
imperialism_isolationism = 2
secularism_theocracy = 0
offensive_defensive = 0
land_naval = 2
quality_quantity = -1
serfdom_freesubjects = -1
primary_culture = papuan
religion = hinduism
technology_group = chinese
capital = 650	# Tidore


1356.1.1 = {
	monarch = {
		name = "Sah Jati"
		dynasty = "Kiema Kolano"
		adm = 6
		dip = 6
		mil = 4
	}
}

1402.1.1 = {
	monarch = {
		name = "Busamuangi"
		dynasty = "Kiema Kolano"
		adm = 8
		dip = 7
		mil = 7
	}
}

1414.1.1 = {
	monarch = {
		name = "Suhu"
		dynasty = "Kiema Kolano"
		adm = 5
		dip = 7
		mil = 4
	}
}

1426.1.1 = {
	monarch = {
		name = "Balibungah"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1438.1.1 = {
	monarch = {
		name = "Duhu Madoya"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1450.1.1 = {
	monarch = {
		name = "Kie Matiti"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1462.1.1 = {
	monarch = {
		name = "Sele"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1474.1.1 = {
	monarch = {
		name = "Matagena"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1495.1.1 = {
	monarch = {
		name = "Ciri Leliati Jamal ud-din"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1510.1.1 = {
	monarch = {
		name = "al-Mansur"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
	religion = sunni government = eastern_despotism
}

1526.1.1 = {
	monarch = {
		name = "Amiruddin Iskandar Du'lkarna'in"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1560.1.1 = {
	monarch = {
		name = "Kie Mansur"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1570.1.1 = {
	monarch = {
		name = "Iskandar Sani"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1580.1.1 = {
	monarch = {
		name = "Gapi Baguna"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1599.1.1 = {
	monarch = {
		name = "Molemajimu"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1627.1.1 = {
	monarch = {
		name = "Ngarolamo"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1634.1.1 = {
	monarch = {
		name = "Gorontalo"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1640.1.1 = {
	monarch = {
		name = "Sa'di"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1657.1.1 = {
	monarch = {
		name = "Saif ud-Din"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1689.1.1 = {
	monarch = {
		name = "Hamja Fahar ud-Din"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1705.1.1 = {
	monarch = {
		name = "Abdul Falali Mansur I"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1708.1.1 = {
	monarch = {
		name = "Hasan ud Din"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1728.1.1 = {
	monarch = {
		name = "Malikulmanan"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1757.1.1 = {
	monarch = {
		name = "Amir Muhammad Massud Jamal ud Din"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1780.1.1 = {
	monarch = {
		name = "Patra Alam"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1784.1.1 = {
	monarch = {
		name = "Kamal ud Din"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1797.1.1 = {
	monarch = {
		name = "Amir ud Din"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1805.1.1 = {
	monarch = {
		name = "Muhammad Jawal Abdidin Syah"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}

1810.1.1 = {
	monarch = {
		name = "Muhammad Tahir Mossel"
		dynasty = "Kiema Kolano"
		adm = 3
		dip = 4
		mil = 4
	}
}