# Princes of Kashin
government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -1
imperialism_isolationism = 0
secularism_theocracy = 2
land_naval = -3
quality_quantity = 2
serfdom_freesubjects = -5
primary_culture = russian
religion = orthodox
technology_group = eastern
capital = 2241	# Kashin

 

1366.1.1 = {
	monarch = {
		name = "Mikhail"
		dynasty = Rurikovich
		DIP = 4
		ADM = 5
		MIL = 6
	}
}

1373.1.1 = {
	monarch = {
		name = "Vasily II"
		dynasty = Rurikovich
		DIP = 4
		ADM = 5
		MIL = 6
	}
}

1382.1.1 = {
	monarch = {
		name = "Alexandr"
		dynasty = Rurikovich
		DIP = 4
		ADM = 5
		MIL = 6
	}
}
1389.1.1 = {
	monarch = {
		name = "Boris"
		dynasty = Rurikovich
		DIP = 4
		ADM = 5
		MIL = 6
	}
}
1395.1.1 = {
	monarch = {
		name = "Vasily III"
		dynasty = Rurikovich
		DIP = 4
		ADM = 5
		MIL = 6
	}
}
1399.1.1 = {
	monarch = {
		name = "Ivan"
		dynasty = Rurikovich
		DIP = 4
		ADM = 5
		MIL = 6
	}
}
1403.1.1 = {
	monarch = {
		name = "Vasily III"
		dynasty = Rurikovich
		DIP = 4
		ADM = 5
		MIL = 6
	}
}
1412.1.1 = {
	monarch = {
		name = "Ivan"
		dynasty = Rurikovich
		DIP = 4
		ADM = 5
		MIL = 6
	}
}
1420.1.1 = {
	monarch = {
		name = "Vasily III"
		dynasty = Rurikovich
		DIP = 4
		ADM = 5
		MIL = 6
	}
}
#To Smolensk 1403