government = daimyo
aristocracy_plutocracy = -4
centralization_decentralization = 2
innovative_narrowminded = 2
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = -1
offensive_defensive = -1
land_naval = 0
quality_quantity = 4
serfdom_freesubjects = -5
primary_culture = japanese
religion = shinto
technology_group = chinese
capital = 1346	# Shimotsuke
daimyo = yes

1351.1.1 = { 
monarch = { 
name = "Yoshinori" 
dynasty = Uesugi 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Uesugi Yoshinori" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1351.1.1 = {
    heir = {
        name = "Noriharu"
        monarch_name =  "Noriharu" 
        dynasty =  Uesugi 
        birth_date = 1341.1.1
        death_date = 1379.1.1
        claim = 88
        adm = 5
        dip = 8
        mil = 7
        leader = {
            name =  "Noriharu" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1378.1.1 = { 
monarch = { 
name = "Noriharu" 
dynasty = Uesugi 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Uesugi Noriharu" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1378.1.1 = {
    heir = {
        name = "Norikata"
        monarch_name =  "Norikata" 
        dynasty =  Uesugi 
        birth_date = 1377.1.1
        death_date = 1394.1.1
        claim = 89
        adm = 5
        dip = 8
        mil = 7
        leader = {
            name =  "Norikata" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1379.1.1 = { 
monarch = { 
name = "Norikata" 
dynasty = Uesugi 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Uesugi Norikata" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1379.1.1 = {
    heir = {
        name = "Norimoto"
        monarch_name =  "Norimoto" 
        dynasty =  Uesugi 
        birth_date = 1365.1.1
        death_date = 1418.1.1
        claim = 84
        adm = 5
        dip = 8
        mil = 7
        leader = {
            name =  "Norimoto" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1394.1.1 = { 
monarch = { 
name = "Norimoto" 
dynasty = Uesugi 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Ueasugi Norimoto" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1394.1.1 = {
    heir = {
        name = "Norizane"
        monarch_name =  "Norizane" 
        dynasty =  Uesugi 
        birth_date = 1387.1.1
        death_date = 1466.1.1
        claim = 81
        adm = 7
        dip = 7
        mil = 8
        leader = {
            name =  "Norizane" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1418.1.1 = { 
monarch = { 
name = "Norizane" 
dynasty = Uesugi 
adm = 7 
dip = 7 
mil = 8 
leader = { name = "Uesugi Norizane" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1418.1.1 = {
    heir = {
        name = "Akisada"
        monarch_name =  "Akisada" 
        dynasty =  Uesugi 
        birth_date = 1417.1.1
        death_date = 1522.1.1
        claim = 89
        adm = 3
        dip = 3
        mil = 3
        leader = {
            name =  "Akisada" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1466.1.1 = { 
monarch = { 
name = "Akisada" 
dynasty = Uesugi 
adm = 3 
dip = 3 
mil = 3 
leader = { name = "Uesugi Akisada" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1466.1.1 = {
    heir = {
        name = "Norimasa"
        monarch_name =  "Norimasa" 
        dynasty =  Uesugi 
        birth_date = 1466.1.1
        death_date = 1579.1.1
        claim = 90
        adm = 3
        dip = 3
        mil = 3
        leader = {
            name =  "Norimasa" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1522.1.1 = { 
monarch = { 
name = "Norimasa" 
dynasty = Uesugi 
adm = 3 
dip = 3 
mil = 3 
leader = { name = "Uesugi Norimasa" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1522.1.1 = {
    heir = {
        name = "Kagekatsu"
        monarch_name =  "Kagekatsu" 
        dynasty =  Uesugi 
        birth_date = 1521.1.1
        death_date = 1542.1.1
        claim = 74
        adm = 3
        dip = 3
        mil = 3
        leader = {
            name =  "Kagekatsu" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1579.1.1 = { 
monarch = { 
name = "Kagekatsu" 
dynasty = Uesugi 
adm = 3 
dip = 3 
mil = 3 
leader = { name = "Uesugi Kagekatsu" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

