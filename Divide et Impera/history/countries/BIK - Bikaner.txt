government = military_federation
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = -4
land_naval = -1
quality_quantity = -1
serfdom_freesubjects = -2
primary_culture = rajput
religion = hinduism
technology_group = indian
capital = 520	# Bikaner

# Rathor

1465.1.1 = {
	monarch = {
		name = "Bika Rao"
		dynasty = Rathor
		DIP = 5
		MIL = 6
		ADM = 5
	}
}

1504.1.1 = {
	monarch = {
		name = "Naro"
		dynasty = Rathor
		DIP = 3
		MIL = 4
		ADM = 3
	}
}

1505.1.1 = {
	monarch = {
		name = "Lunkaran"
		dynasty = Rathor
		DIP = 4
		MIL = 5
		ADM = 3
	}
}

1526.1.1 = {
	monarch = {
		name = "Jetsi"
		dynasty = Rathor
		DIP = 4
		MIL = 4
		ADM = 4
	}
}

1542.1.1 = {
	monarch = {
		name = "Kalyan Singh"
		dynasty = Rathor
		DIP = 5
		MIL = 5
		ADM = 3
	}
}
1571.1.1 = {
	monarch = {
		name = "Raya Singh Raja"
		dynasty = Rathor
		DIP = 5
		MIL = 6
		ADM = 7
	}
}
1612.1.1 = {
	monarch = {
		name = "Dalpat Singh"
		dynasty = Rathor
		DIP = 4
		MIL = 6
		ADM = 5
	}
}
1613.1.1 = {
	monarch = {
		name = "Sur Singh"
		dynasty = Rathor
		DIP = 3
		MIL = 6
		ADM = 4
	}
}
1631.1.1 = {
	monarch = {
		name = "Karan Singh"
		dynasty = Rathor
		DIP = 4
		MIL = 5
		ADM = 4
	}
}
1669.1.1 = {
	monarch = {
		name = "Anup Singh Maharaja"
		dynasty = Rathor
		DIP = 3
		MIL = 7
		ADM = 5
	}
}
1698.1.1 = {
	monarch = {
		name = "Sarup Singh"
		dynasty = Rathor
		DIP = 4
		MIL = 5
		ADM = 3
	}
}
1700.1.1 = {
	monarch = {
		name = "Sujan Singh"
		dynasty = Rathor
		DIP = 6
		MIL = 6
		ADM = 5
	}
}
1736.1.1 = {
	monarch = {
		name = "Zorawar Singh"
		dynasty = Rathor
		DIP = 3
		MIL = 5
		ADM = 3
	}
}
1745.1.1 = {
	monarch = {
		name = "Gaja Singh"
		dynasty = Rathor
		DIP = 6
		MIL = 4
		ADM = 4
	}
}
1787.1.1 = {
	monarch = {
		name = "Raja Singh"
		dynasty = Rathor
		DIP = 4
		MIL = 5
		ADM = 6
	}
}
1787.4.1 = {
	monarch = {
		name = "Pratap Singh"
		dynasty = Rathor
		DIP = 7
		MIL = 8
		ADM = 6
	}
}
1787.8.1 = {
	monarch = {
		name = "Surat Singh"
		dynasty = Rathor
		DIP = 4
		MIL = 5
		ADM = 4
	}
}