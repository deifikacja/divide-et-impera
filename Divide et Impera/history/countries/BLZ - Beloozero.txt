government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -1
imperialism_isolationism = 1
secularism_theocracy = 2
offensive_defensive = 1
land_naval = -3
quality_quantity = 2
serfdom_freesubjects = -5
technology_group = eastern
religion = orthodox
primary_culture = russian
capital = 314	# Beloozero

 

#Princes
1339.1.1 = {
	monarch = {
		name = "Roman"
		dynasty = "Rurikovich"
		DIP = 3
		ADM = 4
		MIL = 4
	}
}

1339.1.1 = {
	heir = {
		name = "Fyodor"
		monarch_name = "Fyodor II"
		dynasty = "Rurikovich"
		birth_date = 1339.1.1
		death_date = 1380.1.1
		claim = 95
		adm = 6
		dip = 5
		mil = 5
	}
}

1368.1.1 = {
	monarch = {
		name = "Fyodor II"
		dynasty = "Rurikovich"
		DIP = 6
		ADM = 5
		MIL = 5
	}
}

1368.1.1 = {
	heir = {
		name = "Fyodor"
		monarch_name = "Fyodor II"
		dynasty = "Rurikovich"
		birth_date = 1368.1.1
		death_date = 1389.1.1
		claim = 95
		adm = 4
		dip = 3
		mil = 3
	}
}

1380.1.1 = {
	monarch = {
		name = "Georgi"
		dynasty = "Rurikovich"
		DIP = 4
		ADM = 3
		MIL = 3
	}
}

1380.1.1 = {
	heir = {
		name = "Georgi"
		monarch_name = "Georgi"
		dynasty = "Rurikovich"
		birth_date = 1380.1.1
		death_date = 1432.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1389.1.1 = {
	monarch = {
		name = "Andrei"
		dynasty = "Rurikovich"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1389.1.1 = {
	heir = {
		name = "Georgi"
		monarch_name = "Georgi"
		dynasty = "Rurikovich"
		birth_date = 1389.1.1
		death_date = 1450.1.1
		claim = 95
		adm = 4
		dip = 5
		mil = 4
	}
}

1432.1.1 = {
	monarch = {
		name = "Mikhail II"
		dynasty = "Rurikovich"
		DIP = 4
		ADM = 5
		MIL = 4
	}
}

#Annexed by Muscovy in 1463