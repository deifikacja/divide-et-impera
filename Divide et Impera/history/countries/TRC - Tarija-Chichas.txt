government = tribal_federation
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 5
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = -2
offensive_defensive = 0
land_naval = -5
quality_quantity = 4
serfdom_freesubjects = -3
primary_culture = guarani
religion = animism
technology_group = new_world
capital = 795	# Chchas


1352.1.1 = {
	monarch = {
		name = "Chief of Tarija-Chichas"
		adm = 5
		dip = 5
		mil = 5
	}
}

