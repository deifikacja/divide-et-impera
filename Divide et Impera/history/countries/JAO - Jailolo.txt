government = caste_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 1
land_naval = 2
quality_quantity = -1
serfdom_freesubjects = -1
primary_culture = papuan
religion = hinduism
technology_group = chinese
capital = 648	# Halmahera


1350.1.1 = {
	monarch = {
		name = "Jikoma Kolano"
		adm = 6
		dip = 6
		mil = 4
	}
}
1500.1.1 = { religion = sunni government = eastern_despotism }