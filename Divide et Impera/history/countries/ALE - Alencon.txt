government = feudal_monarchy
aristocracy_plutocracy = -2
centralization_decentralization = -5
innovative_narrowminded = 2
mercantilism_freetrade = -1
offensive_defensive = 5
land_naval = -4
quality_quantity = -3
serfdom_freesubjects = -3
primary_culture = cosmopolitan_french
religion = catholic
technology_group = western
capital = 2261 # Alencon

 

1346.8.26 = {
	monarch = {
		name = "Charles III"
		dynasty = "de Valois"
		adm = 4
		dip = 3
		mil = 3
	}
}
1356.1.1 = {
	heir = {
		name = "Peter"
		monarch_name = "Peter II"
		dynasty="de Valois"
		claim = 75
		birth_date = 1340.1.1
		death_date = 1404.9.20
		adm = 4
		dip = 3
		mil = 3
	}
}
1361.1.1 = {
	monarch = {
		name = "Peter II"
		dynasty = "de Valois"
		adm = 4
		dip = 3
		mil = 3
	}
}

1404.1.1 = {
	monarch = {
		name = "Jean I"
		dynasty = "de Valois"
		adm = 3
		dip = 4
		mil = 5
	}
}

1414.1.1 = {
	monarch = {
		name = "Jean II"
		dynasty = "de Valois"
		adm = 3
		mil = 5
		dip = 7
	}
}

1478.1.1 = {
	monarch = {
		name = "Ren�"
		dynasty = "de Valois"
		adm = 4
		mil = 4
		dip = 6
	}
}

1492.1.1 = {
	monarch = {
		name = "Charles IV"
		dynasty = "de Valois"
		adm = 4
		mil = 5
		dip = 4
	}
}


1515.1.2 = { government = despotic_monarchy }

# 1525 To Armagnac

# 1549 To royal Domain

1589.8.3 = { government = administrative_monarchy }

1661.3.9 = { government = absolute_monarchy }
