government = merchant_republic
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = 3
imperialism_isolationism = 0
secularism_theocracy = -2
land_naval = -1
quality_quantity = -1
serfdom_freesubjects = -1
technology_group = western
religion = catholic
primary_culture = rheinlaender
capital = 1926	#Worms


1352.1.1 = {
	monarch = {
		name = "Magistrat"
		adm = 7
		dip = 5
		mil = 5
	}
}
1546.4.19  = { religion = protestant }
