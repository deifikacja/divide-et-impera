government = caste_monarchy
aristocracy_plutocracy = -2
centralization_decentralization = 4
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 1
secularism_theocracy = 0
offensive_defensive = 0
land_naval = 2
quality_quantity = 2
serfdom_freesubjects = -1
primary_culture = sulawesi
technology_group = chinese
religion = hinduism
capital = 627	# Blambangan

1500.1.1 = {
	monarch = {
		name = "Menak Pentor"
		dynasty = Blambangan
		adm = 7
		dip = 6
		mil = 6
	}
}

1541.1.1 = {
	monarch = {
		name = "Mina Cucu"
		dynasty = Blambangan
		adm = 5
		dip = 5
		mil = 4
	}
}

1550.1.1 = {
	monarch = {
		name = "Sontoguno"
		dynasty = Blambangan
		adm = 5
		dip = 5
		mil = 4
	}
}
1597.1.1 = {
	monarch = {
		name = "Mas Karian"
		dynasty = Blambangan
		adm = 5
		dip = 5
		mil = 4
	}
}

1633.1.1 = {
	monarch = {
		name = "Sunan Tawangalun I"
		dynasty = Blambangan
		adm = 5
		dip = 5
		mil = 5
	}
}

1645.1.1 = {
	monarch = {
		name = "Prabhu Tawangalun II"
		dynasty = Blambangan
		adm = 6
		dip = 5
		mil = 5
	}
}

1691.1.1 = {
	monarch = {
		name = "Macanapura"
		dynasty = Blambangan
		adm = 5
		dip = 4
		mil = 6
	}
}

1697.1.1 = {
	monarch = {
		name = "Pangeran Putra"
		dynasty = Blambangan
		adm = 5
		dip = 6
		mil = 5
	}
}

1736.1.1 = {
	monarch = {
		name = "Danuningrat"
		dynasty = Blambangan
		adm = 5
		dip = 5
		mil = 5
	}
}

1736.1.1 = {
	monarch = {
		name = "Pangeran Wilis"
		dynasty = Blambangan
		adm = 5
		dip = 5
		mil = 5
	}
}