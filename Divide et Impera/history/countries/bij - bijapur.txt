government = eastern_despotism
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 0
mercantilism_freetrade = -4
imperialism_isolationism = -1
secularism_theocracy = -2
offensive_defensive = -1
land_naval = -1
quality_quantity = -1
serfdom_freesubjects = -1
technology_group = indian
religion = sunni
primary_culture = kannada
capital = 532	# Bijapur city


1490.1.1 = {
	monarch = {
		name = "Yusuf"
		dynasty = "Adilshahi"
		DIP = 7
		ADM = 7
		MIL = 8
	}
}

1498.1.1 = {
	heir = {
		name = "Ismail"
		monarch_name = "Ismail"
		dynasty = "Adilshahi"
		birth_date = 1498.1.1
		death_date = 1534.8.27
		claim = 95
		adm = 6
		dip = 5
		mil = 5
	}
}

1511.1.1 = {
	monarch = {
		name = "Ismail"
		dynasty = "Adilshahi"
		DIP = 5
		ADM = 6
		MIL = 5
	}
}

1511.1.1 = {
	heir = {
		name = "Malik"
		monarch_name = "Malik"
		dynasty = "Adilshahi"
		birth_date = 1511.1.1
		death_date = 1535.2.20
		claim = 95
		adm = 3
		dip = 3
		mil = 3
	}
}
		
1534.8.27 = {
	monarch = {
		name = "Malik"
		dynasty = "Adilshahi"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1534.8.27 = {
	heir = {
		name = "Ibrahim"
		monarch_name = "Ibrahim"
		dynasty = "Adilshahi"
		birth_date = 1524.1.1
		death_date = 1557.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1535.2.20 = {
	monarch = {
		name = "Ibrahim"
		dynasty = "Adilshahi"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1535.2.20 = {
	heir = {
		name = "Ali"
		monarch_name = "Ali"
		dynasty = "Adilshahi"
		birth_date = 1535.2.20
		death_date = 1580.4.11
		claim = 95
		adm = 5
		dip = 5
		mil = 4
	}
}

1557.1.1 = {
	monarch = {
		name = "Ali"
		dynasty = "Adilshahi"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1557.1.1 = {
	heir = {
		name = "Ibrahim"
		monarch_name = "Ibrahim II"
		dynasty = "Adilshahi"
		birth_date = 1556.1.1
		death_date = 1626.1.1
		claim = 95
		adm = 7
		dip = 6
		mil = 8
	}
}

1580.4.11 = {
	leader = { name = "Sidhu Sumbal"	type = general rank = 1 fire = 3 shock = 4 manuever = 4 siege = 1 death_date = 1601.1.1 }
	monarch = {
		name = "Ibrahim II"
		dynasty = "Adilshahi"
		DIP = 6
		ADM = 7
		MIL = 8
	}
}

1616.1.1 = {
	heir = {
		name = "Muhammad"
		monarch_name = "Muhammad"
		dynasty = "Adilshahi"
		birth_date = 1616.1.1
		death_date = 1656.1.1
		claim = 95
		adm = 5
		dip = 6
		mil = 5
	}
}

1626.1.1 = {
	monarch = {
		name = "Muhammad"
		dynasty = "Adilshahi"
		DIP = 6
		ADM = 5
		MIL = 5
	}
}

1637.1.1 = {
	heir = {
		name = "Ali"
		monarch_name = "Ali II"
		dynasty = "Adilshahi"
		birth_date = 1637.1.1
		death_date = 1673.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 4
	}
}

1656.1.1 = {
	monarch = {
		name = "Ali II"
		dynasty = "Adilshahi"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1668.1.1 = {
	heir = {
		name = "Sikandar"
		monarch_name = "Sikandar"
		dynasty = "Adilshahi"
		birth_date = 1668.1.1
		death_date = 1686.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 3
	}
}

1673.1.1 = {
	monarch = {
		name = "Sikandar"
		dynasty = "Adilshahi"
		DIP = 5
		ADM = 5
		MIL = 3
	}
}

#Annexed by the Mughals 1686
