government = eastern_despotism
aristocracy_plutocracy = -2
centralization_decentralization = 3
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 2
secularism_theocracy = 0
offensive_defensive = 1
land_naval = 0
quality_quantity = 3
serfdom_freesubjects = -2
primary_culture = central_thai
religion = buddhism
technology_group = chinese
capital = 601	# Sukhotai


1354.1.1 = {
	monarch = {
		name = "Li Thai Mahadharmaraja I"
		dynasty = "Phra Ruang"
		adm = 4
		dip = 5
		mil = 6
	}
}

1356.1.1 = {
	heir = {
		name = "Mahadharmaraja"
		monarch_name = "Mahadharmaraja II"
		dynasty = "Phra Ruang"
		birth_date = 1356.1.1
		death_date = 1406.1.1
		claim = 90
		adm = 5
		dip = 5
		mil = 5
	}
}

1376.1.1 = {
	monarch = {
		name = "Mahadharmaraja II"
		dynasty = "Phra Ruang"
		adm = 4
		dip = 5
		mil = 3
	}
}

1376.1.1 = {
	heir = {
		name = "Mahadharmaraja"
		monarch_name = "Mahadharmaraja III"
		dynasty = "Phra Ruang"
		birth_date = 1376.1.1
		death_date = 1419.1.1
		claim = 90
		adm = 5
		dip = 5
		mil = 5
	}
}

1406.1.1 = {
	monarch = {
		name = "Mahadharmaraja III"
		dynasty = "Phra Ruang"
		adm = 7
		dip = 5
		mil = 8
	}
}

1406.1.1 = {
	heir = {
		name = "Mahadharmaraja"
		monarch_name = "Mahadharmaraja IV"
		dynasty = "Phra Ruang"
		birth_date = 1376.1.1
		death_date = 1438.1.1
		claim = 90
		adm = 5
		dip = 5
		mil = 5
	}
}

1419.1.1 = {
	monarch = {
		name = "Mahadharmaraja IV"
		dynasty = "Phra Ruang"
		adm = 5
		dip = 6
		mil = 8
	}
}
#To Ayutthaya from 1438