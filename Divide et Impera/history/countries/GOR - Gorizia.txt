government = feudal_monarchy
aristocracy_plutocracy = -2
centralization_decentralization = 6
innovative_narrowminded = 0
mercantilism_freetrade = -3
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 1
land_naval = 0
quality_quantity = 0
serfdom_freesubjects = -1
technology_group = western
religion = catholic
primary_culture = austrian
capital = 1931	# Gorz

 

1327.1.1 = {
	monarch = {
		name = "Meinhard V"
		dynasty = "Meinharder"
		adm = 6
		dip = 7
		mil = 5
	}
}

1356.1.1 = {
	heir = {
		name = "Heinrich"
		monarch_name = "Heinrich V"
		dynasty = "Meinharder"
		birth_date = 1356.1.1
		death_date = 1454.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 4
	}
}

1356.1.1 = {
	heir = {
		name = "Johann"
		monarch_name = "Johann Meinhard"
		dynasty = "Meinharder"
		birth_date = 1356.1.1
		death_date = 1429.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 4
	}
}

1385.1.1 = {
	monarch = {
		name = "Heinrich V"
		dynasty = "Meinharder"
		adm = 5
		dip = 5
		mil = 4
	}
}

1454.1.1 = {
	monarch = {
		name = "Leonhard"
		dynasty = "Meinharder"
		adm = 5
		dip = 7
		mil = 4
	}
}
