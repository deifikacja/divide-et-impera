government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 1
land_naval = -5
quality_quantity = 2
serfdom_freesubjects = -5
primary_culture = bantu
religion = animism
technology_group = african
capital = 1288	# Ankole

1356.1.1 = {
	monarch = {
		name = "Bahinda"
		dynasty = Bachwezi
		adm = 4
		dip = 4
		mil = 5
	}
}

1392.1.1 = {
	monarch = {
		name = "Omugabe"
		dynasty = Bachwezi
		adm = 4
		dip = 5
		mil = 6
	}
}

1430.1.1 = {
	monarch = {
		name = "Ruhinda"
		dynasty = Bachwezi
		adm = 4
		dip = 5
		mil = 6
	}
}

1446.1.1 = {
	monarch = {
		name = "Nkuba"
		dynasty = Bachwezi
		adm = 4
		dip = 5
		mil = 6
	}
}

1503.1.1 = {
	monarch = {
		name = "Nyabugaro Ntare I"
		dynasty = Bachwezi
		adm = 4
		dip = 5
		mil = 6
	}
}

1531.1.1 = {
	monarch = {
		name = "Rushango"
		dynasty = Bachwezi
		adm = 4
		dip = 5
		mil = 6
	}
}

1559.1.1 = {
	monarch = {
		name = "Ntare II Kagwejegyerera"
		dynasty = Bachwezi
		adm = 4
		dip = 5
		mil = 6
	}
}

1587.1.1 = {
	monarch = {
		name = "Ntare III Rugaamba"
		dynasty = Bachwezi
		adm = 4
		dip = 5
		mil = 6
	}
}

1615.1.1 = {
	monarch = {
		name = "Kasasira"
		dynasty = Bachwezi
		adm = 4
		dip = 5
		mil = 6
	}
}

1643.1.1 = {
	monarch = {
		name = "Kitera"
		dynasty = Bachwezi
		adm = 4
		dip = 5
		mil = 6
	}
}

1671.1.1 = {
	monarch = {
		name = "Mirindi"
		dynasty = Bachwezi
		adm = 4
		dip = 5
		mil = 6
	}
}

1699.1.1 = {
	monarch = {
		name = "Natare IV Kitabanyoro"
		dynasty = Bachwezi
		adm = 4
		dip = 5
		mil = 6
	}
}

1727.1.1 = {
	monarch = {
		name = "Macwa"
		dynasty = Bachwezi
		adm = 4
		dip = 5
		mil = 6
	}
}

1755.1.1 = {
	monarch = {
		name = "Rwabirere"
		dynasty = Bachwezi
		adm = 4
		dip = 5
		mil = 6
	}
}

1760.1.1 = {
	monarch = {
		name = "Karara"
		dynasty = Bachwezi
		adm = 4
		dip = 5
		mil = 6
	}
}

1765.1.1 = {
	monarch = {
		name = "Karaiga"
		dynasty = Bachwezi
		adm = 4
		dip = 5
		mil = 6
	}
}

1770.1.1 = {
	monarch = {
		name = "Kahaya"
		dynasty = Bachwezi
		adm = 4
		dip = 5
		mil = 6
	}
}

1775.1.1 = {
	monarch = {
		name = "Nyakashaija"
		dynasty = Bachwezi
		adm = 4
		dip = 5
		mil = 6
	}
}

1780.1.1 = {
	monarch = {
		name = "Bwarenga"
		dynasty = Bachwezi
		adm = 4
		dip = 5
		mil = 6
	}
}
1795.1.1 = {
	monarch = {
		name = "Rwebishengye"
		dynasty = Bachwezi
		adm = 4
		dip = 5
		mil = 6
	}
}

1811.1.1 = {
	monarch = {
		name = "Kayungu"
		dynasty = Bachwezi
		adm = 4
		dip = 5
		mil = 6
	}
}

1820.1.1 = {
	monarch = {
		name = "Rwanga"
		dynasty = Bachwezi
		adm = 4
		dip = 5
		mil = 6
	}
}