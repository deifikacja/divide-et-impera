government = theocratic_government
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -3
imperialism_isolationism = 0
secularism_theocracy = 3
offensive_defensive = 3
land_naval = 1
quality_quantity = 0
serfdom_freesubjects = -4
primary_culture = swabian
religion = catholic
technology_group = western
capital = 2150	# Chur
historical_friend = SWI

1355.1.1 = {
	monarch = {
		name = "Peter I Gelyto"
		adm = 4
		dip = 6
		mil = 4
	}
}

1368.1.1 = {
	monarch = {
		name = "Friedrich II von Erdingen"
		adm = 4
		dip = 7
		mil = 7
	}
}

1376.1.1 = {
	monarch = {
		name = "Johann II von Ehingen"
		adm = 5
		dip = 6
		mil = 3
	}
}

1388.1.1 = {
	monarch = {
		name = "Bartholom�us"
		adm = 5
		dip = 5
		mil = 3
	}
}

1390.1.1 = {
	monarch = {
		name = "Hartmann II"
		adm = 4
		dip = 7
		mil = 5
	}
}

1416.1.1 = {
	monarch = {
		name = "Johan III Ambundi"
		adm = 6
		dip = 5
		mil = 4
	}
}

1417.1.1 = {
	monarch = {
		name = "Johan IV Naso"
		adm = 4
		dip = 5
		mil = 4
	}
}

1440.1.1 = {
	monarch = {
		name = "Konrad IV von Rechberg"
		adm = 4
		dip = 6
		mil = 5
	}
}

1441.1.1 = {
	monarch = {
		name = "Heinrich V von H�wen"
		adm = 6
		dip = 6
		mil = 6
	}
}

1453.1.1 = {
	monarch = {
		name = "Leonhard Wyssmayer"
		adm = 4
		dip = 5
		mil = 5
	}
}

1458.1.1 = {
	monarch = {
		name = "Ortlieb von Brandis"
		adm = 6
		dip = 6
		mil = 4
	}
}
#Bishop's powers limited by creation of The Three Leagues