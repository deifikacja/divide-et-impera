government = eastern_despotism
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 1
offensive_defensive = 0
land_naval = 1
quality_quantity = 3
serfdom_freesubjects = -4
primary_culture = swahili
religion = sunni
technology_group = sahel
capital = 1202  #Mombassa

#Sultans of Mombassa
1502.1.1 = {
	monarch = {
		name = "Mazru'i"
		dynasty = "al-Mazru'i"
		adm = 3
		dip = 4
		mil = 6
	}
}

1515.1.1 = {
	monarch = {
		name = "Ab� Bakr"
		dynasty = "al-Mazru'i"
		adm = 3
		dip = 4
		mil = 6
	}
}

1545.1.1 = {
	monarch = {
		name = "Malindi"
		dynasty = "al-Mazru'i"
		adm = 5
		dip = 5
		mil = 3
	}
}

1580.1.1 = {
	monarch = {
		name = "Sabo"
		dynasty = "al-Mazru'i"
		adm = 4
		dip = 4
		mil = 4
	}
}

1583.1.1 = {
	monarch = {
		name = "Ahmad"
		dynasty = "al-Mazru'i"
		adm = 4
		dip = 6
		mil = 5
	}
}

1599.1.1 = {
	monarch = {
		name = "al-Hasan"
		dynasty = "al-Mazru'i"
		adm = 4
		dip = 4
		mil = 3
	}
}

1604.1.1 = {
	monarch = {
		name = "Munganaye"
		dynasty = "al-Mazru'i"
		adm = 5
		dip = 5
		mil = 4
	}
}

1620.8.24 = {
	monarch = {
		name = "Y�suf"
		dynasty = "al-Mazru'i"
		adm = 3
		dip = 6
		mil = 4
	}
}

1698.1.1 = {
	monarch = {
		name = "Nasir"
		dynasty = "al-Mazru'i"
		adm = 5
		dip = 5
		mil = 5
	}
}

1730.1.1 = {
	monarch = {
		name = "Mohammed"
		dynasty = "al-Mazru'i"
		adm = 5
		dip = 5
		mil = 5
	}
}

1744.1.1 = {
	monarch = {
		name = "'Ali"
		dynasty = "al-Mazru'i"
		adm = 5
		dip = 5
		mil = 5
	}
}

1754.1.1 = {
	monarch = {
		name = "Mas'ud"
		dynasty = "al-Mazru'i"
		adm = 5
		dip = 5
		mil = 5
	}
}

1779.1.1 = {
	monarch = {
		name = "'Abdullah"
		dynasty = "al-Mazru'i"
		adm = 5
		dip = 5
		mil = 5
	}
}

1782.1.1 = {
	monarch = {
		name = "Ahmad"
		dynasty = "al-Mazru'i"
		adm = 5
		dip = 5
		mil = 5
	}
}

1815.1.1 = {
	monarch = {
		name = "'Abdullah II"
		dynasty = "al-Mazru'i"
		adm = 5
		dip = 5
		mil = 5
	}
}

1823.1.1 = {
	monarch = {
		name = "Sulayman"
		adm = 5
		dip = 5
		mil = 5
	}
}

1825.1.1 = {
	monarch = {
		name = "Salim"
		dynasty = "al-Mazru'i"
		adm = 5
		dip = 5
		mil = 5
	}
}