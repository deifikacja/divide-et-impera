government = oligarchical_monarchy
aristocracy_plutocracy = -2
centralization_decentralization = 5
innovative_narrowminded = 3
mercantilism_freetrade = -4
secularism_theocracy = 0
offensive_defensive = -3
land_naval = -3
quality_quantity = 3
serfdom_freesubjects = -2
technology_group = sahel
religion = animism
primary_culture = mossi
capital = 1137	# Wagadugu


#Rulers of Ougadougou
1182.1.1 = {
	monarch = {
		name = "Moogo-naaba"
		dynasty = Mossi
		DIP = 8
		ADM = 8
		MIL = 6
	}
}
1690.1.1 = {
	monarch = {
		name = "Naaba Zana"
		dynasty = Mossi
		DIP = 6
		ADM = 8
		MIL = 8
	}
offensive_defensive = -5
land_naval = -5
}

1700.1.1 = {
	monarch = {
		name = "Naaba Gilinga"
		dynasty = Mossi
		DIP = 7
		ADM = 4
		MIL = 4
	}
}
1710.1.1 = {
	monarch = {
		name = "Naaba Ubra"
		dynasty = Mossi
		DIP = 6
		ADM = 4
		MIL = 5
	}
}
1729.1.1 = {
	monarch = {
		name = "Naaba Muatiba"
		dynasty = Mossi
		DIP = 7
		ADM = 6
		MIL = 5
	}
}
1737.1.1 = {
	monarch = {
		name = "Naaba Warga"
		dynasty = Mossi
		DIP = 3
		ADM = 6
		MIL = 4
	}
}
1744.1.1 = {
	monarch = {
		name = "Naaba Zombre"
		dynasty = Mossi
		DIP = 3
		ADM = 3
		MIL = 3
	}
}
1784.7.1 = {
	monarch = {
		name = "Naaba Koom I"
		dynasty = Mossi
		DIP = 4
		ADM = 4
		MIL = 4
	}
}
1790.1.1 = {
	monarch = {
		name = "Naaba Saaga I"
		dynasty = Mossi
		DIP = 5
		ADM = 5
		MIL = 7
	}
}
1795.1.1 = {
	monarch = {
		name = "Naaba Dulugu"
		dynasty = Mossi
		DIP = 6
		ADM = 5
		MIL = 4
	}
}
1802.1.1 = {
	monarch = {
		name = "Naaba Sawadogo"
		dynasty = Mossi
		DIP = 6
		ADM = 7
		MIL = 7
	}
}