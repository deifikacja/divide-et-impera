government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = -4
land_naval = -5
quality_quantity = 5
serfdom_freesubjects = -5
primary_culture = mississippian
religion = animism
technology_group = new_world
capital = 917	#Cahokia

1350.1.1 = {
	monarch = {
		name = "Chief of Cahokia"
		adm = 6
		dip = 6
		mil = 5
	}
}