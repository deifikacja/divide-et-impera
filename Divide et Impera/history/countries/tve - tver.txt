government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -1
imperialism_isolationism = 0
secularism_theocracy = 1
offensive_defensive = 1
land_naval = -3
quality_quantity = 2
serfdom_freesubjects = -5
primary_culture = russian
religion = orthodox
technology_group = eastern
capital = 294	# Tver

 

#Rurikovich, Mongol vassals
1351.1.1 = {
	monarch = {
		name = "Vasili"
		dynasty = Rurikovich
		adm = 5
		dip = 4
		mil = 6
	}
}

1366.8.21 = {
	monarch = {
		name = "Mikhail II"
		dynasty = Rurikovich
		adm = 5
		dip = 4
		mil = 6
	}
}

1399.1.1 = {
	monarch = {
		name = "Ivan"
		dynasty = Rurikovich
		adm = 4
		dip = 5
		mil = 5
	}
}

1425.5.1 = {
	monarch = {
		name = "Alexandr II"
		dynasty = Rurikovich
		adm = 4
		dip = 3
		mil = 3
	}
}

1425.8.1 = {
	monarch = {
		name = "Yuri"
		dynasty = Rurikovich
		adm = 4
		dip = 3
		mil = 3
	}
}
1426.1.1 = {
	monarch = {
		name = "Boris"
		dynasty = Rurikovich
		adm = 4
		dip = 3
		mil = 3
	}
}
1461.2.10 = {
	monarch = {
		name = "Mikhail III"
		dynasty = Rurikovich
		adm = 3
		dip = 3
		mil = 3
	}
}
1466.1.1 = { leader = {	name = "Afanasy Nikitin"    	type = explorer		rank = 9	fire = 3	shock = 2	manuever = 5	siege = 0	death_date = 1472.2.1 } }
1485.9.13 = {
	monarch = {
		name = "Ivan II"
		dynasty = Rurikovich
		adm = 5
		dip = 5
		mil = 4
	}
}
