government = eastern_despotism
aristocracy_plutocracy = -2
centralization_decentralization = 5
innovative_narrowminded = 3
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 1
land_naval = 3
quality_quantity = 3
serfdom_freesubjects = -2
primary_culture = malayan
religion = sunni
technology_group = chinese
capital = 1375	# Asahan

1630.1.1 = {
	monarch = { 
		name = "Abdul Jalil Shah I"
		dynasty="Pinangawan"
		adm = 3
		dip = 3
		mil = 4
	}
}

1680.1.1 = {
	monarch = { 
		name = "Sa'id Shah"
		dynasty="Pinangawan"
		adm = 5
		dip = 4
		mil = 4
	}
}

1700.1.1 = {
	monarch = { 
		name = "Muhammad Mahrum Shah"
		dynasty="Pinangawan"
		adm = 5
		dip = 4
		mil = 4
	}
}

1760.1.1 = {
	monarch = { 
		name = "Abdul Jalil Shah II"
		dynasty="Pinangawan"
		adm = 5
		dip = 4
		mil = 4
	}
}

1765.1.1 = {
	monarch = { 
		name = "Deva Shah"
		dynasty="Pinangawan"
		adm = 5
		dip = 4
		mil = 4
	}
}

1805.1.1 = {
	monarch = { 
		name = "Sa'id Musa Shah"
		dynasty="Pinangawan"
		adm = 5
		dip = 4
		mil = 4
	}
}

1808.1.1 = {
	monarch = { 
		name = "Muhammad 'Ali Shah"
		dynasty="Pinangawan"
		adm = 5
		dip = 4
		mil = 4
	}
}

1813.1.1 = {
	monarch = { 
		name = "Muhammad Husain Rahmad Shah I"
		dynasty="Pinangawan"
		adm = 5
		dip = 4
		mil = 4
	}
}