government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 3
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = 2
land_naval = -5
quality_quantity = 4
serfdom_freesubjects = -4
technology_group = new_world
primary_culture = mayan
religion = animism
capital = 2064	# Mixco Viejo

1350.1.1 = {
	monarch = {
		name = "Halach Uinik"
		dynasty = Pokomam
		adm = 4
		dip = 3
		mil = 6
	}
}