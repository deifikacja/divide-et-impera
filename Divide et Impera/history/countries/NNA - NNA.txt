government = eastern_despotism
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -3
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 0
land_naval = -2
quality_quantity = 3
serfdom_freesubjects = -2
technology_group = chinese
religion = buddhism
primary_culture = northern_thai
capital = 1994	# Nan


1356.1.1 = {
	monarch = {
		name = "Meuang"
		dynasty = Nan
		DIP = 6
		MIL = 8
		ADM = 6
	}
}

1786.1.1 = {
	monarch = {
		name = "Attawalapanyo"
		dynasty = Nan
		DIP = 3
		MIL = 3
		ADM = 3
	}
}

1810.1.1 = {
	monarch = {
		name = "Sumanathewalat"
		dynasty = Nan
		DIP = 6
		MIL = 4
		ADM = 4
	}
}