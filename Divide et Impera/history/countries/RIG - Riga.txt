government = theocratic_government
aristocracy_plutocracy = -5
centralization_decentralization = 4
innovative_narrowminded = 2
mercantilism_freetrade = -2
imperialism_isolationism = 1
secularism_theocracy = 3
offensive_defensive = 2
land_naval = 0
quality_quantity = -2
serfdom_freesubjects = -5
technology_group = western
primary_culture = east_german
add_accepted_culture = latvian
religion = catholic
capital = 38	# Riga

#Archbishops

1348.10.19 = {
	monarch = {
		name = "Fromhold von Vyshusen"
		DIP = 3
		ADM = 3
		MIL = 5
	}
}

1370.1.2 = {
	monarch = {
		name = "Siegfried Blomberg"
		DIP = 4
		ADM = 5
		MIL = 4
	}
}

1374.3.13 = {
	monarch = {
		name = "Johann IV von Zinten"
		DIP = 7
		ADM = 7
		MIL = 6
	}
}

1393.1.1 = {
	monarch = {
		name = "Johann V von Wallenrodt"
		DIP = 6
		ADM = 7
		MIL = 6
	}
}

1418.7.1 = {
	monarch = {
		name = "Johann VI Ambundi"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1424.7.1 = {
	monarch = {
		name = "Henning Scharffenberg"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1448.10.9 = {
	monarch = {
		name = "Silvester Stodwescher"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1479.7.12 = {
	monarch = {
		name = "Stephan Grube"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1483.12.10 = {
	monarch = {
		name = "Michael Hildebrand"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1509.2.6 = {
	monarch = {
		name = "Jasper Linde"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1524.6.29 = {
	monarch = {
		name = "Johann VII Blankenfelde"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1528.2.6= {
	monarch = {
		name = "Thomas Sch�ning"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1539.8.11 = {
	monarch = {
		name = "Wilhelm von Brandenburg"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1555.11.1 = {
	monarch = {
		name = "Christoph von Mecklenburg"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}
# Annexed by Poland 1563.8.4