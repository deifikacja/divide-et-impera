government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -3
offensive_defensive = 0
imperialism_isolationism = 0
secularism_theocracy = 1
land_naval = 1
quality_quantity = -1
serfdom_freesubjects = -2
technology_group = western
religion = catholic
primary_culture = sicilian
capital = 121	# Napoli

 

1355.10.16 = {
	monarch = { 
		name = "Frederico III"
		dynasty = "d'Arag�"
		dip = 3
		mil = 2
		adm = 3
	}
}

1363.7.2 = {
	heir = {
		name = "Maria"
		monarch_name = "Maria I"
		dynasty = "d'Arag�"
		birth_date = 1363.7.2
		death_date = 1401.5.25
		claim = 95
		dip = 6
		mil = 7
		adm = 5
		female = yes
	}
}

1377.1.28 = {
	monarch = { 
		name = "Maria I"
		dynasty = "d'Arag�"
		dip = 6
		mil = 7
		adm = 5
		female = yes
	}
}

1713.4.12 = {
	monarch = {
		name = "Vittorio Amedeo II"
		dynasty = "di Savoia"
		adm = 7
		dip = 9
		mil = 8
		leader = { name = "Vittorio Amedeo II" type = general rank = 0 fire = 4 shock = 4 manuever = 2 siege = 2 }
	}
}

# 1720.1.1 Vittorio Amedeo II becomes king of Sardinia-Piemont

1806.3.11 = { government = enlightened_despotism }

# Ferdinand/Ferrante flees to Sicily
1806.3.11 = {
	monarch = {
		name = "Ferrante IV"
		dynasty = "de Bourbon"
		dip = 3
		mil = 3
		adm = 3
	}		
}
