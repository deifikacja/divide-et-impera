government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -3
imperialism_isolationism = 0
secularism_theocracy = 1
offensive_defensive = -1
land_naval = 1
quality_quantity = -1
serfdom_freesubjects = -2
primary_culture = piemontese
religion = catholic
technology_group = western
capital = 2253	# Chambery

 

1349.1.1 = {
	monarch = {
		name = "Catarina"
		dynasty = "di Savoia"
		adm = 6
		dip = 9
		mil = 5
		female = yes
	}
}
1798.1.1 = {
government = constitutional_republic
	monarch = {
		name = "Lemanese Republic"
		adm = 6
		dip = 9
		mil = 5
		regent = yes
	}
}