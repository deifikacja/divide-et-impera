government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -3
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 2
land_naval = -1
quality_quantity = 0
serfdom_freesubjects = -3
primary_culture = hannoverian
religion = catholic
technology_group = western
capital = 55	# Oldenburg

 

1345.3.14 = {
	monarch = {
		name = "Conrad I"
		dynasty = "von Oldenburg"
		adm = 4
		dip = 5
		mil = 4
	}
}

1356.1.1 = {
	heir = {
		name = "Conrad"
		monarch_name = "Conrad II"
		dynasty = "von Oldenburg"
		birth_date = 1356.1.1
		death_date = 1386.1.12
		claim = 95
		adm = 6
		dip = 7
		mil = 4
	}
}

1368.7.22 = {
	monarch = {
		name = "Conrad II"
		dynasty = "von Oldenburg"
		adm = 6
		dip = 7
		mil = 4
	}
}

1368.7.22 = {
	heir = {
		name = "Maurice"
		monarch_name = "Maurice III"
		dynasty = "von Oldenburg"
		birth_date = 1368.7.22
		death_date = 1398.11.3
		claim = 95
		adm = 4
		dip = 4
		mil = 3
	}
}

1386.1.12 = {
	monarch = {
		name = "Maurice III"
		dynasty = "von Oldenburg"
		adm = 4
		dip = 4
		mil = 3
	}
}

1386.1.12 = {
	heir = {
		name = "Christian"
		monarch_name = "Christian V"
		dynasty = "von Oldenburg"
		birth_date = 1386.1.12
		death_date = 1423.1.1
		claim = 95
		adm = 4
		dip = 6
		mil = 4
	}
}
1398.11.3 = {
	monarch = {
		name = "Christian V"
		dynasty = "von Oldenburg"
		adm = 4
		dip = 6
		mil = 4
	}
}

1398.11.3 = {
	heir = {
		name = "Dietrich"
		monarch_name = "Dietrich"
		dynasty = "von Oldenburg"
		birth_date = 1390.1.1
		death_date = 1440.2.14
		claim = 95
		adm = 5
		dip = 5
		mil = 3
	}
}

1423.1.1 = {
	monarch = {
		name = "Dietrich"
		dynasty = "von Oldenburg"
		adm = 5
		dip = 5
		mil = 3
	}
}

1426.1.1 = {
	heir = {
		name = "Christian"
		monarch_name = "Christian VI"
		dynasty = "von Oldenburg"
		birth_date = 1426.1.1
		death_date = 1481.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1440.2.14 = {
	monarch = {
		name = "Christian VI"
		dynasty = "von Oldenburg"
		adm = 5
		dip = 5
		mil = 5
	}
}

1440.2.14 = {
	heir = {
		name = "Gerhard"
		monarch_name = "Gerhard der Mutige"
		dynasty = "von Oldenburg"
		birth_date = 1430.1.1
		death_date = 1500.2.22
		claim = 95
		adm = 5
		dip = 6
		mil = 6
	}
}

1454.1.1 = {
	monarch = {
		name = "Gerhard der Mutige"
		dynasty = "von Oldenburg"
		adm = 5
		dip = 6
		mil = 6
	}
}

1458.1.1 = {
	heir = {
		name = "Adolf"
		monarch_name = "Adolf"
		dynasty = "von Oldenburg"
		birth_date = 1458.1.1
		death_date = 1500.1.1
		claim = 95
		adm = 5
		dip = 4
		mil = 7
	}
}

1483.1.1 = {
	monarch = {
		name = "Adolf"
		dynasty = "von Oldenburg"
		adm = 5
		dip = 4
		mil = 7
	}
}

1483.1.1 = {
	heir = {
		name = "Johann"
		monarch_name = "Johann V"
		dynasty = "von Oldenburg"
		birth_date = 1460.1.1
		death_date = 1526.2.10
		claim = 95
		adm =6
		dip = 5
		mil = 5
	}
}

1500.1.1 = {
	monarch = {
		name = "Johann V"
		dynasty = "von Oldenburg"
		adm = 4
		dip = 6
		mil = 5
	}
}

1500.1.1 = {
	heir = {
		name = "Johann"
		monarch_name = "Johann VI"
		dynasty = "von Oldenburg"
		birth_date = 1500.1.1
		death_date = 1548.1.1
		claim = 95
		adm = 6
		dip = 5
		mil = 5
	}
}

1526.2.10 = {
	monarch = {
		name = "Johann VI"
		dynasty = "von Oldenburg"
		adm = 6
		dip = 5
		mil = 5
	}
}

1526.2.10 = {
	heir = {
		name = "Anton"
		monarch_name = "Anton I"
		dynasty = "von Oldenburg"
		birth_date = 1505.1.1
		death_date = 1573.1.23
		claim = 95
		adm = 6
		dip = 6
		mil = 4
	}
}

1529.1.1 = { religion = protestant }

1566.1.1 = {
	monarch = {
		name = "Anton I"
		dynasty = "von Oldenburg"
		adm = 6
		dip = 6
		mil = 4
	}
}

1566.1.1 = {
	heir = {
		name = "Johann"
		monarch_name = "Johann VII"
		dynasty = "von Oldenburg"
		birth_date = 1540.1.1
		death_date = 1603.1.1
		claim = 95
		adm = 4
		dip = 7
		mil = 5
	}
}

1573.1.23 = {
	monarch = {
		name = "Johann VII"
		dynasty = "von Oldenburg"
		adm = 4
		dip = 7
		mil = 5
	}
}

1583.11.10 = {
	heir = {
		name = "Anton G�nther"
		monarch_name = "Anton G�nther I"
		dynasty = "von Oldenburg"
		birth_date = 1583.11.10
		death_date = 1667.6.19
		claim = 95
		adm = 7
		dip = 6
		mil = 5
	}
}

1603.11.13 = {
	monarch = {
		name = "Anton G�nther I"
		dynasty = "von Oldenburg"
		adm = 7
		dip = 6
		mil = 5
	}
}

# United with Denmark 1667-1773

1773.12.11 = {
	monarch = {
		name = "Paul I von Ru�land "
		dynasty = "von Oldenburg"
		adm = 4
		dip = 4
		mil = 3
	}
}

1773.12.15 = {
	monarch = {
		name = "Friedrich August I"
		dynasty = "von Holstein-Gottorp"
		adm = 5
		dip = 5
		mil = 6
	}
}

1773.12.15 = {
	heir = {
		name = "Peter Friedrich Wilhelm"
		monarch_name = "Wilhelm I"
		dynasty = "von Holstein-Gottorp"
		birth_date = 1754.1.3
		death_date = 1823.7.2
		claim = 95
		adm = 7
		dip = 6
		mil = 6
	}
}

1785.7.7 = {
	monarch = {
		name = "Wilhelm I"
		dynasty = "von Holstein-Gottorp"
		adm = 7
		dip = 6
		mil = 6
	}
}

1785.7.7 = {
	heir = {
		name = "Peter"
		monarch_name = "Peter I"
		dynasty = "von Holstein-Gottorp"
		birth_date = 1755.1.17
		death_date = 1829.5.21
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}
