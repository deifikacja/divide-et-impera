government = tribal_democracy
aristocracy_plutocracy = 5
centralization_decentralization = 5
innovative_narrowminded = 5
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = 0
land_naval = -5
quality_quantity = 0
serfdom_freesubjects = 5
primary_culture = creek
religion = shamanism
technology_group = new_world
capital = 929

1350.1.1 = {
	monarch = {
		name = "Brim"
		adm = 7
		dip = 5
		mil = 7
	}
}

1550.1.1 = {
	monarch = {
		name = "Chigelli"
		adm = 6
		dip = 6
		mil = 7
	}
}

1700.1.1 = {
	monarch = {
		name = "Tomochichi"
		adm = 4
		dip = 5
		mil = 6
	}
}

1739.1.1 = {
	monarch = {
		name = "Chekilli"
		adm = 7
		dip = 6
		mil = 5
	}
}

