government = eastern_despotism
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = 1
imperialism_isolationism = 2
secularism_theocracy = 0
offensive_defensive = 2
land_naval = 2
quality_quantity = -1
serfdom_freesubjects = -1
technology_group = chinese
religion = confucianism
primary_culture = filipino
capital = 656	# Tondo

#Rulers of Tondo
1300.1.1 = {
	monarch = {
		name = "Alon"
		dynasty = Tondo
		DIP = 3
		ADM = 4
		MIL = 4
	}
}
1390.1.1 = {
	monarch = {
		name = "Gambang"
		dynasty = Tondo
		DIP = 3
		ADM = 4
		MIL = 4
	}
}
1430.1.1 = {
	monarch = {
		name = "Lontok"
		dynasty = Tondo
		DIP = 3
		ADM = 4
		MIL = 4
	}
}
1450.1.1 = {
	monarch = {
		name = "Kaylangitan"
		dynasty = Tondo
		DIP = 3
		ADM = 4
		MIL = 4
		female = yes
	}
}
1515.1.1 = {
	religion = sunni government = eastern_despotism
	monarch = {
		name = "Sulaiman I"
		dynasty = Tondo
		DIP = 3
		ADM = 4
		MIL = 4
	}
}
1558.1.1 = {
	monarch = {
		name = "Lak�nd�l�"
		dynasty = Tondo
		DIP = 3
		ADM = 4
		MIL = 4
	}
}
#Rulers of Manila
1571.1.1 = {
	monarch = {
		name = "Sulaiman II"
		dynasty = Tondo
		DIP = 3
		ADM = 4
		MIL = 4
	}
}
1575.1.1 = {
	monarch = {
		name = "Magat Salamat"
		dynasty = Tondo
		DIP = 3
		ADM = 4
		MIL = 4
	}
}