government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 3
innovative_narrowminded = 1
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = 0
land_naval = -5
quality_quantity = 2
serfdom_freesubjects = -5
primary_culture = bantu
religion = shamanism
technology_group = african
capital = 1291	# Fort Portal

1356.1.1 = {
	monarch = {
		name = "Omukama"
		dynasty = Abakama
		adm = 4
		dip = 5
		mil = 6
	}
}

1822.1.1 = {
	monarch = {
		name = "Olimi I Kaboyo I"
		dynasty = Abakama
		adm = 4
		dip = 5
		mil = 6
	}
}