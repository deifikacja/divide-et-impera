government = daimyo
aristocracy_plutocracy = -4
centralization_decentralization = 2
innovative_narrowminded = 2
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = -1
offensive_defensive = -1
land_naval = 0
quality_quantity = -1
serfdom_freesubjects = -5
primary_culture = japanese
religion = shinto
technology_group = chinese
capital = 1013	# Hizen
daimyo = yes

1454.1.1 = { 
monarch = { 
name = "Tanehisa" 
dynasty = Ryuzoji 
adm = 5 
dip = 5 
mil = 8 
} 
} 

1454.1.1 = {
    heir = {
        name = "Takanobu"
        monarch_name =  "Takanobu" 
        dynasty =  Ryuzoji 
        birth_date = 1450.1.1
        death_date = 1584.1.1
        claim = 74
        adm = 5
        dip = 6
        mil = 8
   }
}


1530.1.1 = { 
monarch = { 
name = "Takanobu" 
dynasty = Ryuzoji 
adm = 5 
dip = 6 
mil = 8 
} 
} 

1530.1.1 = {
    heir = {
        name = "Masaie"
        monarch_name =  "Masaie" 
        dynasty =  Ryuzoji 
        birth_date = 1527.1.1
        death_date = 1538.1.1
        claim = 92
        adm = 5
        dip = 6
        mil = 8
   }
}


1584.1.1 = { 
monarch = { 
name = "Masaie" 
dynasty = Ryuzoji 
adm = 5 
dip = 6 
mil = 8 
} 
} 

