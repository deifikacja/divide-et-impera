government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -3
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 2
land_naval = -3
quality_quantity = 0
serfdom_freesubjects = -4
primary_culture = rheinlaender
religion = catholic
technology_group = western
capital = 2154	# Cleves

 

1347.7.30 = {
	monarch = {
		name = "Johann"
		dynasty = "von Kleve"
		adm = 3
		dip = 5
		mil = 4
	}
}

1368.2.22 = {
	monarch = {
		name = "Adolph III"
		dynasty = "von der Mark"
		adm = 4
		dip = 7
		mil = 7
	}
}

1373.8.2 = {
	heir = {
		name = "Adolph"
		monarch_name = "Adolph IV"
		dynasty = "von der Mark"
		birth_date = 1373.8.2
		death_date = 1448.9.23
		claim = 95
		adm = 4
		dip = 3
		mil = 4
	}
}

1394.4.12 = {
	monarch = {
		name = "Adolph IV"
		dynasty = "von der Mark"
		adm = 4
		dip = 3
		mil = 4
	}
}

1419.2.16 = {
	heir = {
		name = "Johann"
		monarch_name = "Johann I"
		dynasty = "von der Mark"
		birth_date = 1419.2.16
		death_date = 1481.9.5
		claim = 95
		adm = 6
		dip = 6
		mil = 4
	}
}

1448.9.24 = {
	monarch = {
		name = "Johann I"
		dynasty = "von der Mark"
		adm = 6
		dip = 6
		mil = 4
	}
}

1458.4.13 = {
	heir = {
		name = "Johann"
		monarch_name = "Johann II"
		dynasty = "von der Mark"
		birth_date = 1458.4.13
		death_date = 1521.3.15
		claim = 95
		adm = 6
		dip = 6
		mil = 7
		leader = {	name = "Johann II"             	type = general	rank = 0	fire = 3	shock = 2	manuever = 3	siege = 0}
	}
}

1481.9.6 = {
	monarch = {
		name = "Johann II"
		dynasty = "von der Mark"
		adm = 6
		dip = 6
		mil = 7
		leader = {	name = "Johann II"             	type = general	rank = 0	fire = 3	shock = 2	manuever = 3	siege = 0}
	}
}

1490.11.10 = {
	heir = {
		name = "Johann"
		monarch_name = "Johann III"
		dynasty = "von der Mark"
		birth_date = 1490.11.10
		death_date = 1539.2.6
		claim = 95
		adm = 6
		dip = 6
		mil = 6
	}
}

1521.3.16 = {
	monarch = {
		name = "Johann III"
		dynasty = "von der Mark"
		adm = 6
		dip = 6
		mil = 6
	}
}

1521.3.16 = {
	heir = {
		name = "Wilhelm"
		monarch_name = "Wilhelm der Reiche"
		dynasty = "von der Mark"
		birth_date = 1516.7.28
		death_date = 1592.1.5
		claim = 95
		adm = 7
		dip = 3
		mil = 4
	}
}

1539.2.6 = {
	monarch = {
		name = "Wilhelm der Reiche"
		dynasty = "von der Mark"
		adm = 7
		dip = 3
		mil = 4
	}
}

1562.5.29 = {
	heir = {
		name = "Johann Wilhelm"
		monarch_name = "Johann Wilhelm"
		dynasty = "von der Mark"
		birth_date = 1562.5.29
		death_date = 1609.3.25
		claim = 95
		adm = 4
		dip = 7
		mil = 4
	}
}

1592.1.6 = {
	monarch = {
		name = "Johann Wilhelm"
		dynasty = "von der Mark"
		adm = 4
		dip = 7
		mil = 3
	}
}
