government = military_federation
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = -3
land_naval = -2
quality_quantity = -1
serfdom_freesubjects = -1
primary_culture = rajput
religion = jainism
technology_group = indian
capital = 525	# Bundi

# Hara Chauhan


1342.1.1 = {
	monarch = {
		name = "Deva"
		dynasty = "Hara Chauhan"
		DIP = 5
		MIL = 6
		ADM = 4
	}
}

1342.1.1 = {
	heir = {
		name = "Napuji"
		monarch_name = "Napuji"
		dynasty = "Hara Chauhan"
		birth_date = 1342.1.1
		death_date = 1384.1.1
		claim = 95
		adm = 3
		dip = 4
		mil = 3
	}
}

1366.1.1 = {
	monarch = {
		name = "Napuji"
		dynasty = "Hara Chauhan"
		DIP = 3
		MIL = 4
		ADM = 3
	}
}

1366.1.1 = {
	heir = {
		name = "Hamuji"
		monarch_name = "Hamuji"
		dynasty = "Hara Chauhan"
		birth_date = 1366.1.1
		death_date = 1400.1.1
		claim = 95
		adm = 4
		dip = 5
		mil = 3
	}
}

1384.1.1 = {
	monarch = {
		name = "Hamuji"
		dynasty = "Hara Chauhan"
		DIP = 4
		MIL = 5
		ADM = 3
	}
}

1384.1.1 = {
	heir = {
		name = "Vir Singh"
		monarch_name = "Vir Singh"
		dynasty = "Hara Chauhan"
		birth_date = 1384.1.1
		death_date = 1415.1.1
		claim = 95
		adm = 4
		dip = 4
		mil = 4
	}
}

1400.1.1 = {
	monarch = {
		name = "Vir Singh"
		dynasty = "Hara Chauhan"
		DIP = 4
		MIL = 4
		ADM = 4
	}
}
1400.1.1 = {
	heir = {
		name = "Biru"
		monarch_name = "Biru"
		dynasty = "Hara Chauhan"
		birth_date = 1400.1.1
		death_date = 1470.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 3
	}
}
1415.1.1 = {
	monarch = {
		name = "Biru"
		dynasty = "Hara Chauhan"
		DIP = 5
		MIL = 5
		ADM = 3
	}
}
1415.1.1 = {
	heir = {
		name = "Bandu"
		monarch_name = "Bandu"
		dynasty = "Hara Chauhan"
		birth_date = 1415.1.1
		death_date = 1491.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 3
	}
}
1470.1.1 = {
	monarch = {
		name = "Bandu"
		dynasty = "Hara Chauhan"
		DIP = 5
		MIL = 6
		ADM = 7
	}
}
1470.1.1 = {
	heir = {
		name = "Narajan Das"
		monarch_name = "Narajan Das"
		dynasty = "Hara Chauhan"
		birth_date = 1470.1.1
		death_date = 1527.1.1
		claim = 95
		adm = 4
		dip = 6
		mil = 5
	}
}
1491.1.1 = {
	monarch = {
		name = "Narajan Das"
		dynasty = "Hara Chauhan"
		DIP = 4
		MIL = 6
		ADM = 5
	}
}
1491.1.1 = {
	heir = {
		name = "Suraj Mal"
		monarch_name = "Suraj Mal"
		dynasty = "Hara Chauhan"
		birth_date = 1491.1.1
		death_date = 1531.1.1
		claim = 95
		adm = 3
		dip = 6
		mil = 4
	}
}
1527.1.1 = {
	monarch = {
		name = "Suraj Mal"
		dynasty = "Hara Chauhan"
		DIP = 3
		MIL = 6
		ADM = 4
	}
}
1527.1.1 = {
	heir = {
		name = "Suraj Mal"
		monarch_name = "Suraj Mal"
		dynasty = "Hara Chauhan"
		birth_date = 1527.1.1
		death_date = 1544.1.1
		claim = 95
		adm = 4
		dip = 5
		mil = 4
	}
}
1531.1.1 = {
	monarch = {
		name = "Surtan Singh"
		dynasty = "Hara Chauhan"
		DIP = 4
		MIL = 5
		ADM = 4
	}
}
1531.1.1 = {
	heir = {
		name = "Surjan Singh"
		monarch_name = "Surjan Singh"
		dynasty = "Hara Chauhan"
		birth_date = 1531.1.1
		death_date = 1585.1.1
		claim = 95
		adm = 3
		dip = 7
		mil = 5
	}
}
1544.1.1 = {
	monarch = {
		name = "Surjan Singh"
		dynasty = "Hara Chauhan"
		DIP = 3
		MIL = 7
		ADM = 5
	}
}
1544.1.1 = {
	heir = {
		name = "Surjan Singh"
		monarch_name = "Surjan Singh"
		dynasty = "Hara Chauhan"
		birth_date = 1544.1.1
		death_date = 1608.1.1
		claim = 95
		adm = 4
		dip = 5
		mil = 3
	}
}
1585.1.1 = {
	monarch = {
		name = "Bhoj Singh"
		dynasty = "Hara Chauhan"
		DIP = 4
		MIL = 5
		ADM = 3
	}
}
1585.1.1 = {
	heir = {
		name = "Surjan Singh"
		monarch_name = "Surjan Singh"
		dynasty = "Hara Chauhan"
		birth_date = 1585.1.1
		death_date = 1632.1.1
		claim = 95
		adm = 6
		dip = 6
		mil = 5
	}
}
1608.1.1 = {
	monarch = {
		name = "Ratan Singh"
		dynasty = "Hara Chauhan"
		DIP = 6
		MIL = 6
		ADM = 5
	}
}
1608.1.1 = {
	heir = {
		name = "Chatra Singh"
		monarch_name = "Chatra Singh"
		dynasty = "Hara Chauhan"
		birth_date = 1608.1.1
		death_date = 1658.1.1
		claim = 95
		adm = 3
		dip = 5
		mil = 3
	}
}
1632.1.1 = {
	monarch = {
		name = "Chatra Singh"
		dynasty = "Hara Chauhan"
		DIP = 3
		MIL = 5
		ADM = 3
	}
}
1632.1.1 = {
	heir = {
		name = "Bhao Singh"
		monarch_name = "Bhao Singh"
		dynasty = "Hara Chauhan"
		birth_date = 1632.1.1
		death_date = 1682.1.1
		claim = 95
		adm = 6
		dip = 4
		mil = 4
	}
}
1658.1.1 = {
	monarch = {
		name = "Bhao Singh"
		dynasty = "Hara Chauhan"
		DIP = 6
		MIL = 4
		ADM = 4
	}
}
1658.1.1 = {
	heir = {
		name = "Anirudh Singh"
		monarch_name = "Anirudh Singhh"
		dynasty = "Hara Chauhan"
		birth_date = 1658.1.1
		death_date = 1696.1.1
		claim = 95
		adm = 4
		dip = 5
		mil = 6
	}
}
1682.1.1 = {
	monarch = {
		name = "Anirudh Singh"
		dynasty = "Hara Chauhan"
		DIP = 4
		MIL = 5
		ADM = 6
	}
}
1682.1.1 = {
	heir = {
		name = "Budh Singh"
		monarch_name = "Budh Singhh"
		dynasty = "Hara Chauhan"
		birth_date = 1682.1.1
		death_date = 1735.1.1
		claim = 95
		adm = 7
		dip = 8
		mil = 6
	}
}
1696.1.1 = {
	monarch = {
		name = "Budh Singh"
		dynasty = "Hara Chauhan"
		DIP = 7
		MIL = 8
		ADM = 6
	}
}
1696.1.1 = {
	heir = {
		name = "Dalel Singh"
		monarch_name = "Dalel Singh"
		dynasty = "Hara Chauhan"
		birth_date = 1696.1.1
		death_date = 1749.1.1
		claim = 95
		adm = 4
		dip = 5
		mil = 4
	}
}
1735.1.1 = {
	monarch = {
		name = "Dalel Singh"
		dynasty = "Hara Chauhan"
		DIP = 4
		MIL = 5
		ADM = 4
	}
}
1735.1.1 = {
	heir = {
		name = "Umaid Singh"
		monarch_name = "Umaid Singh"
		dynasty = "Hara Chauhan"
		birth_date = 1735.1.1
		death_date = 1770.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 3
	}
}
1749.1.1 = {
	monarch = {
		name = "Umaid Singh"
		dynasty = "Hara Chauhan"
		DIP = 5
		MIL = 5
		ADM = 3
	}
}
1749.1.1 = {
	heir = {
		name = "Ajit Singh"
		monarch_name = "Ajit Singh"
		dynasty = "Hara Chauhan"
		birth_date = 1749.1.1
		death_date = 1773.1.1
		claim = 95
		adm = 3
		dip = 6
		mil = 4
	}
}
1770.1.1 = {
	monarch = {
		name = "Ajit Singh"
		dynasty = "Hara Chauhan"
		DIP = 3
		MIL = 6
		ADM = 4
	}
}
1770.1.1 = {
	heir = {
		name = "Bishen Singh"
		monarch_name = "Bishen Singh"
		dynasty = "Hara Chauhan"
		birth_date = 1770.1.1
		death_date = 1821.1.1
		claim = 95
		adm = 6
		dip = 5
		mil = 5
	}
}
1773.1.1 = {
	monarch = {
		name = "Bishen Singh"
		dynasty = "Hara Chauhan"
		DIP = 6
		MIL = 5
		ADM = 5
	}
}
1773.1.1 = {
	heir = {
		name = "Ram Singh"
		monarch_name = "Ram Singh"
		dynasty = "Hara Chauhan"
		birth_date = 1773.1.1
		death_date = 1889.1.1
		claim = 95
		adm = 4
		dip = 3
		mil = 3
	}
}
1821.1.1 = {
	monarch = {
		name = "Ram Singh"
		dynasty = "Hara Chauhan"
		DIP = 4
		MIL = 3
		ADM = 3
	}
}