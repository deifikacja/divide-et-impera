government = caste_monarchy
aristocracy_plutocracy = -2
centralization_decentralization = 3
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 1
secularism_theocracy = 0
offensive_defensive = 1
land_naval = 0
quality_quantity = 3
serfdom_freesubjects = -2
primary_culture = malayalam
religion = hinduism
technology_group = indian
capital = 1961	# Kannur

#'Ali Raja dynasty
1545.1.1 = {
	monarch = {
		name = "'Ali Adi-Raja I"
		dynasty = Arakkal
		adm = 7
		dip = 6
		mil = 7
	}
}

1591.1.1 = {
	monarch = {
		name = "Abu Bakr Adi-Raja I"
		dynasty = Arakkal
		adm = 7
		dip = 6
		mil = 7
	}
}

1607.1.1 = {
	monarch = {
		name = "Abu Bakr Adi-Raja II"
		dynasty = Arakkal
		adm = 3
		dip = 4
		mil = 4
	}
}

1610.1.1 = {
	monarch = {
		name = "Mohammed 'Ali Adi-Raja I"
		dynasty = Arakkal
		adm = 4
		dip = 4
		mil = 4
	}
}

1647.1.1 = {
	monarch = {
		name = "Mohammed 'Ali Adi-Raja II"
		dynasty = Arakkal
		adm = 6
		dip = 4
		mil = 7
	}
}

1655.1.1 = {
	monarch = {
		name = "Kamal Adi-Raja"
		dynasty = Arakkal
		adm = 4
		dip = 4
		mil = 5
	}
}

1656.1.1 = {
	monarch = {
		name = "Muhammad 'Ali Adi-Raja III"
		dynasty = Arakkal
		adm = 8
		dip = 8
		mil = 8
	}
}

1691.1.1 = {
	monarch = {
		name = "'Ali Adi-Raja II"
		dynasty = Arakkal
		adm = 5
		dip = 5
		mil = 4
	}
}

1704.1.1 = {
	monarch = {
		name = "Kunhi Amsa Adi-Raja I"
		dynasty = Arakkal
		adm = 6
		dip = 6
		mil = 5
	}
}

1720.1.1 = {
	monarch = {
		name = "Muhammad 'Ali Adi-Raja IV"
		dynasty = Arakkal
		adm = 3
		dip = 3
		mil = 3
	}
}

1728.1.1 = {
	monarch = {
		name = "Harrabichi Kadavube Adi-Raja Bibi"
		dynasty = Arakkal
		adm = 6
		dip = 6
		mil = 5
		female = yes
	}
}

1732.1.1 = {
	monarch = {
		name = "Junumabe Adi-Raja Bibi I"
		dynasty = Arakkal
		adm = 4
		dip = 4
		mil = 5
		female = yes
	}
}

1745.1.1 = {
	monarch = {
		name = "Kunhi Amsa Adi-Raja II"
		dynasty = Arakkal
		adm = 3
		dip = 3
		mil = 4
	}
}

1777.1.1 = {
	monarch = {
		name = "Junumabe Adi-Raja Bibi II"
		dynasty = Arakkal
		adm = 3
		dip = 3
		mil = 3
		female = yes
	}
}

1819.1.1 = {
	monarch = {
		name = "Maraiambe Adi-Raja Bibi"
		dynasty = Arakkal
		adm = 8
		dip = 7
		mil = 7
		female = yes
	}
}