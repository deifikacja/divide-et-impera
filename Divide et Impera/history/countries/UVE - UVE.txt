government = tribal_federation
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = 1
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = 0
land_naval = 5
quality_quantity = 0
serfdom_freesubjects = 0
primary_culture = malay-polynesian
religion = animism
technology_group = pacific
capital = 2051 # Wallis

#Tui `Uvea or Hau of Uvea
1350.1.1 = {
	monarch = {
		name = "Tui `Uvea or Hau"
		dynasty = Takumasiva
		DIP = 5
		ADM = 5
		MIL = 4
	}
}
1650.1.1 = {
	monarch = {
		name = "Munigoto"
		dynasty = Takumasiva
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1700.1.1 = {
	monarch = {
		name = "Atuvaha"
		dynasty = Takumasiva
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1726.1.1 = {
	monarch = {
		name = "Fanalua"
		dynasty = Takumasiva
		DIP = 5
		ADM = 4
		MIL = 3
	}
}

1756.1.1 = {
	monarch = {
		name = "Vaivaikava"
		dynasty = Takumasiva
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1760.1.1 = {
	monarch = {
		name = "Finekata"
		dynasty = Takumasiva
		DIP = 8
		ADM = 8
		MIL = 2
	}
}

1767.1.1 = {
	monarch = {
		name = "Manuka"
		dynasty = Takumasiva
		DIP = 6
		ADM = 5
		MIL = 2
	}
}

1810.1.1 = {
	monarch = {
		name = "Tufele I"
		dynasty = Takumasiva
		DIP = 6
		ADM = 5
		MIL = 2
	}
}

1818.1.1 = {
	monarch = {
		name = "Kulitea"
		dynasty = Takumasiva
		DIP = 6
		ADM = 5
		MIL = 2
	}
}

1819.1.1 = {
	monarch = {
		name = "Lavekava"
		dynasty = Takumasiva
		DIP = 6
		ADM = 5
		MIL = 2
	}
}
1820.1.1 = {
	monarch = {
		name = "Hiva"
		dynasty = Takumasiva
		DIP = 6
		ADM = 5
		MIL = 2
	}
}
1820.5.1 = {
	monarch = {
		name = "Muliakaaka"
		dynasty = Takumasiva
		DIP = 6
		ADM = 5
		MIL = 2
	}
}