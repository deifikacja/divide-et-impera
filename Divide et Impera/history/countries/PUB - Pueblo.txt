government = tribal_democracy
aristocracy_plutocracy = 5
centralization_decentralization = 5
innovative_narrowminded = 0
mercantilism_freetrade = 0
imperialism_isolationism = 1
secularism_theocracy = 0
offensive_defensive = 5
land_naval = -5
quality_quantity = 0
serfdom_freesubjects = 5
primary_culture = pueblo
religion = shamanism
technology_group = new_world
capital = 879


1351.1.1 = {
	monarch = {
		name = "Pueblo Chief"
		adm = 5
		dip = 4
		mil = 5
	}
}