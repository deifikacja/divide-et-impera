government = tribal_democracy
aristocracy_plutocracy = 0
centralization_decentralization = 0
innovative_narrowminded = -1
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = 0
land_naval = 0
quality_quantity = 0
serfdom_freesubjects = 0
offensive_defensive = 0
primary_culture = miqmaq
religion = shamanism
technology_group = new_world
capital = 985


1350.1.1 = {
	monarch = {
		name = "Chief of Mi'kmaq"
		adm = 6
		dip = 6
		mil = 5
	}
}

