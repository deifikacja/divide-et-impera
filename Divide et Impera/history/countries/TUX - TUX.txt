government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 3
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = 2
land_naval = -5
quality_quantity = 4
serfdom_freesubjects = -4
technology_group = new_world
primary_culture = mayan
religion = animism
capital = 842	# Pet�n



1356.1.1 = {
	monarch = {
		name = "Tutul"
		dynasty = "Xiu"
		adm = 4
		dip = 5
		mil = 5
	}
}

1356.1.1 = {
	heir = {
		name = "Ah Xupan"
		monarch_name = "Ah Xupan"
		dynasty = "Xiu"
		birth_date = 1356.1.1
		death_date = 1464.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 4
	}
}

1441.1.1 = {
	monarch = {
		name = "Ah Xupan"
		dynasty = "Xiu"
		adm = 4
		dip = 3
		mil = 5
	}
}

1441.1.1 = {
	heir = {
		name = "Tutul"
		monarch_name = "Tutul"
		dynasty = "Xiu"
		birth_date = 1420.1.1
		death_date = 1492.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 4
	}
}

1464.1.1 = {
	monarch = {
		name = "Tutul"
		dynasty = "Xiu"
		adm = 5
		dip = 5
		mil = 4
	}
}

1464.1.1 = {
	heir = {
		name = "Napot"
		monarch_name = "Napot"
		dynasty = "Xiu"
		birth_date = 1464.1.1
		death_date = 1512.1.1
		claim = 95
		adm = 5
		dip = 6
		mil = 5
	}
}

1492.1.1 = {
	monarch = {
		name = "Napot"
		dynasty = "Xiu"
		adm = 5
		dip = 6
		mil = 5
	}
}
1541.1.1 = { religion = catholic }#Lord of Mani converts to christianity