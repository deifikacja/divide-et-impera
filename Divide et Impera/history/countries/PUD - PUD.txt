government = caste_monarchy
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -2
imperialism_isolationism = 1
secularism_theocracy = 0
offensive_defensive = 0
land_naval = -2
quality_quantity = 3
serfdom_freesubjects = -3
technology_group = indian
religion = hinduism
primary_culture = tamil
capital = 1988	# Pudukkotai

1686.1.1 = {
	monarch = {
		name = "Raghunatha Raya"
		dynasty = Tondaiman
		adm = 5
		dip = 5
		mil = 6
	}
}

1730.1.1 = {
	monarch = {
		name = "Vijaya Raghunatha Raya"
		dynasty = Tondaiman
		adm = 3
		dip = 4
		mil = 5
	}
}

1769.1.1 = {
	monarch = {
		name = "Raya Raghunatha"
		dynasty = Tondaiman
		adm = 4
		dip = 4
		mil = 5
	}
}

1789.12.1 = {
	monarch = {
		name = "Vijaya Raghunatha"
		dynasty = Tondaiman
		adm = 4
		dip = 3
		mil = 4
	}
}

1807.2.1 = {
	monarch = {
		name = "Vijaya Raghunatha Raya"
		dynasty = Tondaiman
		adm = 5
		dip = 5
		mil = 5
	}
}