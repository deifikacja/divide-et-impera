government = tribal_federation
aristocracy_plutocracy = -2
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = -1
land_naval = 0
quality_quantity = 3
serfdom_freesubjects = -2
primary_culture = baluchi
religion = sunni
technology_group = muslim
capital = 577	# Quetta

1350.1.1 = { 
	monarch = {
		name = "Baluchi Tribes"
		DIP = 3
		ADM = 3
		MIL = 4
	}
}

1471.1.1 = {
	government = federation
	monarch = {
		name = "Mir Zunnun Beg"
		dynasty="Ahmadzadi"
		DIP = 5
		ADM = 4
		MIL = 6
	}
}

1504.1.1 = { 
	monarch = {
		name = "Baluchi Tribes"
		DIP = 3
		ADM = 3
		MIL = 4
	}
}

1510.1.1 = { religion = shiite }

1638.1.1 = {
	monarch = {
		name = "Mir Hassan"
		dynasty="Ahmadzadi"
		DIP = 4
		ADM = 5
		MIL = 7
	}
}

1666.1.1 = {
	monarch = {
		name = "Mir Ahmad"
		dynasty="Ahmadzadi"
		DIP = 5
		ADM = 6
		MIL = 7
	}
}

1695.1.1 = {
	monarch = {
		name = "Mir Samandar"
		dynasty="Ahmadzadi"
		DIP = 5
		ADM = 5
		MIL = 6
	}
}

1714.1.1 = {
	monarch = {
		name = "Mir 'Abdullah"
		dynasty="Ahmadzadi"
		DIP = 5
		ADM = 4
		MIL = 4
	}
}

1734.1.1 = {
	monarch = {
		name = "Mir Mohabar"
		dynasty="Ahmadzadi"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1749.1.1 = {
	monarch = {
		name = "Mir Nasir I"
		dynasty="Ahmadzadi"
		DIP = 4
		ADM = 4
		MIL = 3
	}
}

1817.1.1 = {
	monarch = {
		name = "Mir Mahmud I"
		dynasty="Ahmadzadi"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

