government = military_federation
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -2
imperialism_isolationism = -2
secularism_theocracy = 0
offensive_defensive = -5
land_naval = -3
quality_quantity = -2
serfdom_freesubjects = -2
technology_group = indian
primary_culture = marathi
religion = hinduism
capital = 546	# Satara


#Maratha Leaders (at Kolahpur)
1647.1.1 = {
	monarch = {
		name = "Shivaji I"
		dynasty = "Bhonsle"
		adm = 7
		dip = 7
		mil = 9
	}
}

1657.5.14 = {
	heir = {
		name = "Shambaji"
		monarch_name = "Shambaji"
		dynasty = "Bhonsle"
		birth_date = 1657.5.14
		death_date = 1689.3.11
		claim = 95
		adm = 5
		dip = 5
		mil = 7
	}
}

1680.4.3 = {
	monarch = {
		name = "Shambaji"
		dynasty = "Bhonsle"
		adm = 5
		dip = 5
		mil = 7
	}
}

1680.4.3 = {
	heir = {
		name = "Rajaram"
		monarch_name = "Rajaram I"
		dynasty = "Bhonsle"
		birth_date = 1670.1.1
		death_date = 1700.3.12
		claim = 95
		adm = 6
		dip = 5
		mil = 7
	}
}

1689.3.11 = {
	monarch = {
		name = "Rajaram I"
		dynasty = "Bhonsle"
		adm = 6
		dip = 5
		mil = 7
	}
}

1689.3.11 = {
	heir = {
		name = "Shivaji"
		monarch_name = "Shivaji II"
		dynasty = "Bhonsle"
		birth_date = 1682.1.1
		death_date = 1749.1.1
		claim = 95
		adm = 8
		dip = 4
		mil = 6
	}
}

1700.3.12 = {
	monarch = {
		name = "Shivaji II"
		dynasty = "Bhonsle"
		adm = 8
		dip = 4
		mil = 6
	}
}

1700.3.12 = {
	heir = {
		name = "Sahuji"
		monarch_name = "Sahuji"
		dynasty = "Bhonsle"
		birth_date = 1680.1.1
		death_date = 1713.11.6
		claim = 95
		adm = 6
		dip = 4
		mil = 4
	}
}
		
1707.10.10 = {
	monarch = {
		name = "Sahuji I"
		dynasty = "Bhonsle"
		adm = 6
		dip = 4
		mil = 4
	}
}

1749.12.15 = {
	monarch = {
		name = "Rajaram II"
		dynasty = "Bhonsle"
		adm = 6
		dip = 5
		mil = 7
	}

}

1777.12.11 = {
	monarch = {
		name = "Shahuji II"
		dynasty = "Bhonsle"
		adm = 6
		dip = 4
		mil = 4
	}

}

1808.5.3 = {
	monarch = {
		name = "Pratap Singh"
		dynasty = "Bhonsle"
		adm = 6
		dip = 4
		mil = 5
	}
}
