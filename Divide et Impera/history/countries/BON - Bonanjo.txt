government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = -1
land_naval = -5
quality_quantity = 2
serfdom_freesubjects = -5
technology_group = african
religion = animism
primary_culture = duala
capital = 1164	# Douala


1792.1.1 = {
	monarch = {
		name = "Belle Ba Doo"
		dynasty = Bell
		adm = 8
		dip = 4
		mil = 5
	}
}

1810.1.1 = {
	monarch = {
		name = "Bebe Bell II"
		dynasty = Bell
		adm = 4
		dip = 5
		mil = 4
	}
}