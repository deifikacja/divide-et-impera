government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 3
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 1
land_naval = -5
quality_quantity = 2
serfdom_freesubjects = -5
technology_group = sahel
religion = animism
primary_culture = kanuri
capital = 1160	# Mandara

1459.1.1 = {
	monarch = {
		name = "Soukda"
		dynasty = Wandala
		adm = 8
		dip = 4
		mil = 5
		female = yes
	}
}

1500.1.1 = {
	monarch = {
		name = "Dabara"
		dynasty = Wandala
		adm = 8
		dip = 4
		mil = 5
	}
}

1550.1.1 = {
	monarch = {
		name = "Ajawa Kossa"
		dynasty = Wandala
		adm = 8
		dip = 4
		mil = 5
	}
}

1600.1.1 = {
	monarch = {
		name = "Chivakala"
		dynasty = Wandala
		adm = 8
		dip = 4
		mil = 5
	}
}

1650.1.1 = {
	monarch = {
		name = "Digra"
		dynasty = Wandala
		adm = 8
		dip = 4
		mil = 5
	}
}

1715.1.1 = {
	religion = sunni
}

1715.1.1 = {
	monarch = {
		name = "Bukar Azi"
		dynasty = Wandala
		adm = 8
		dip = 4
		mil = 5
	}
}

1737.1.1 = {
	monarch = {
		name = "Madi-a Makirza"
		dynasty = Wandala
		adm = 8
		dip = 4
		mil = 5
	}
}

1755.1.1 = {
	monarch = {
		name = "Bladi-a"
		dynasty = Wandala
		adm = 8
		dip = 4
		mil = 5
	}
}

1773.1.1 = {
	monarch = {
		name = "Buka-a-Jama"
		dynasty = Wandala
		adm = 8
		dip = 4
		mil = 5
	}
}

1828.1.1 = {
	monarch = {
		name = "Ilyara"
		dynasty = Wandala
		adm = 8
		dip = 4
		mil = 5
	}
}