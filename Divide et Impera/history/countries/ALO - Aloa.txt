government = tribal_despotism
aristocracy_plutocracy = -2
centralization_decentralization = 4
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 3
offensive_defensive = 3
land_naval = -4
quality_quantity = 4
serfdom_freesubjects = -4
primary_culture = nubian
religion = eastern_churches
technology_group = sahel
capital = 1222	# Sennar

1355.7.1 = {
	monarch = {
		name = "Ador"
		dynasty = "Aloa"
		adm = 4
		dip = 4
		mil = 5
	}
}
1355.7.1 = {
	heir = {
		name = "Eltecit"
		monarch_name = "'Eltecit"
		dynasty = "Aloa"
		birth_date = 1355.1.1
		death_date = 1464.1.1
		claim = 95
		adm = 3
		dip = 4
		mil = 3
	}
}
1390.1.1 = {
	monarch = {
		name = "Eltecit"
		dynasty = "Aloa"
		adm = 3
		dip = 4
		mil = 3
	}
}
1390.1.1 = {
	heir = {
		name = "Joel"
		monarch_name = "Joel"
		dynasty = "Aloa"
		birth_date = 1390.1.1
		death_date = 1484.1.1
		claim = 95
		adm = 3
		dip = 4
		mil = 4
	}
}
1464.1.1 = {
	monarch = {
		name = "Joel"
		dynasty = "Aloa"
		adm = 3
		dip = 4
		mil = 4
	}
}
1464.1.1 = {
	heir = {
		name = "Koudlaniel"
		monarch_name = "Koudlaniel"
		dynasty = "Aloa"
		birth_date = 1464.1.1
		death_date = 1503.1.1
		claim = 95
		adm = 3
		dip = 4
		mil = 3
	}
}
1484.1.1 = {
	monarch = {
		name = "Koudlaniel"
		dynasty = "Aloa"
		adm = 3
		dip = 4
		mil = 3
	}
}