government = eastern_despotism
aristocracy_plutocracy = -2
centralization_decentralization = 4
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 0
land_naval = -1
quality_quantity = 3
serfdom_freesubjects = -4
technology_group = indian
religion = sunni
primary_culture = marathi
capital = 1979	# Jafrabad

#Thanadars
1650.1.1 = {
	monarch = {
		name = "Fath Khan"
		dynasty = Thanadar
		DIP = 6
		ADM = 8
		MIL = 9
	}
}

1665.1.1 = {
	monarch = {
		name = "Sambhol Yaqut Khan"
		dynasty = Thanadar
		DIP = 6
		ADM = 6
		MIL = 5
	}
}

1678.1.1 = {
	monarch = {
		name = "Kasim Yaqut Khan"
		dynasty = Thanadar
		DIP = 3
		ADM = 4
		MIL = 3
	}
}

1734.1.1 = {
	monarch = {
		name = "Surur Khan"
		dynasty = Thanadar
		DIP = 7
		ADM = 7
		MIL = 9
	}
}

1759.1.1 = {
	monarch = {
		name = "Ibrahim I Khan"
		dynasty = Thanadar
		DIP = 4
		ADM = 4
		MIL = 4
	}
}

1761.1.1 = {
	monarch = {
		name = "Yaqut Khan"
		dynasty = Thanadar
		DIP = 3
		ADM = 5
		MIL = 6
	}
}

1772.1.1 = {
	monarch = {
		name = "Abdul Rahman Khan"
		dynasty = Thanadar
		DIP = 6
		ADM = 6
		MIL = 3
	}
}

1784.1.1 = {
	monarch = {
		name = "Jowhar Khan"
		dynasty = Thanadar
		DIP = 3
		ADM = 5
		MIL = 3
	}
}

1789.1.1 = {
	monarch = {
		name = "Ibrahim II Khan"
		dynasty = Thanadar
		DIP = 6
		ADM = 6
		MIL = 3
	}
}
1792.1.1 = {
	monarch = {
		name = "Jumrud Khan"
		dynasty = Thanadar
		DIP = 3
		ADM = 5
		MIL = 3
	}
}
#Nawabs
1803.1.1 = {
	monarch = {
		name = "Ibrahim II Khan"
		dynasty = Thanadar
		DIP = 6
		ADM = 6
		MIL = 3
	}
}
#Annexed by the Mughals October 1687