government = tribal_despotism
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -3
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 0
land_naval = -1
quality_quantity = 0
serfdom_freesubjects = -4
primary_culture = madagasque
religion = animism
technology_group = african
capital = 1271	# Antakarana


1356.1.1 = {
	monarch = {
		name = "Chief"
		dynasty="Antakarana"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1675.1.1 = {
	monarch = {
		name = "Kazobe"
		dynasty="Antakarana"
		DIP = 6
		ADM = 9
		MIL = 7
	}
}

1710.8.1 = {
	monarch = {
		name = "Lototso"
		dynasty="Antakarana"
		DIP = 4
		ADM = 6
		MIL = 9
	}
}

1740.5.1 = {
	monarch = {
		name = "Andriantsirotso"
		dynasty="Antakarana"
		DIP = 3
		ADM = 5
		MIL = 4
	}
}

1765.1.1 = {
	monarch = {
		name = "Lamboina"
		dynasty="Antakarana"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1790.3.1 = {
	monarch = {
		name = "Tshimbola"
		dynasty="Antakarana"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1812.1.1 = {
	monarch = {
		name = "Tsialama I"
		dynasty="Antakarana"
		DIP = 7
		ADM = 8
		MIL = 7
	}
}

1825.1.1 = {
	monarch = {
		name = "Tsimiharo"
		dynasty="Antakarana"
		DIP = 7
		ADM = 8
		MIL = 7
	}
}

#Madagascar from 17 october 1828