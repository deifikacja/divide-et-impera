government = steppe_horde
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 3
mercantilism_freetrade = -2
imperialism_isolationism = 0
secularism_theocracy = -1
offensive_defensive = -4
land_naval = -5
quality_quantity = 3
serfdom_freesubjects = -5
primary_culture = khazak
religion = sunni
technology_group = muslim
capital = 477	# Turkestan


1731.1.1 = {
	monarch = {
		name = "Semeke Shaikh Muhambet ibn Tawke"
		dynasty = "Khazakh"
		adm = 4
		dip = 3
		mil = 5
	}
}

1737.1.1 = {
	monarch = {
		name = "Abu'l Mambet ibn Bolat"
		dynasty = "Khazakh"
		adm = 4
		dip = 7
		mil = 6
	}
}

1748.1.1 = {
	monarch = {
		name = "Quchuq Sultan"
		dynasty = "Khazakh"
		adm = 3
		dip = 4
		mil = 7
	}
}

1750.1.1 = {
	monarch = {
		name = "Abylai Khan Abu'l Mansur"
		dynasty = "Khazakh"
		adm = 5
		dip = 4
		mil = 5
	}
}

1781.1.1 = {
	monarch = {
		name = "Abdallah ibn Abylai"
		dynasty = "Khazakh"
		adm = 4
		dip = 4
		mil = 4
	}
}

1782.1.1 = {
	monarch = {
		name = "Wali Khan"
		dynasty = "Khazakh"
		adm = 4
		dip = 4
		mil = 4
	}
}