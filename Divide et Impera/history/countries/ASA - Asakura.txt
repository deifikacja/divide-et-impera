government = daimyo
aristocracy_plutocracy = -4
centralization_decentralization = 4
innovative_narrowminded = 2
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = -1
offensive_defensive = -2
land_naval = 0
quality_quantity = 4
serfdom_freesubjects = -5
primary_culture = japanese
religion = shinto
technology_group = chinese
capital = 1023	# Echizen
daimyo = yes

1428.1.1 = { 
monarch = { 
name = "Toshikage" 
dynasty = "Asakura" 
adm = 4 
dip = 4 
mil = 6 
leader = { name = "Asakura Toshikage" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1428.1.1 = {
    heir = {
        name = "Ujikage"
        monarch_name =  "Ujikage" 
        dynasty =  "Asakura" 
        birth_date = 1419.1.1
        death_date = 1486.8.3
        claim = 86
        adm = 5
        dip = 8
        mil = 7
        leader = {
            name =  "Ujikage" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1470.1.1 = { 
monarch = { 
name = "Ujikage" 
dynasty = "Asakura" 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Ujikage Norifusa" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1470.1.1 = {
    heir = {
        name = "Sadakage"
        monarch_name =  "Sadakage" 
        dynasty =  "Asakura" 
        birth_date = 1466.1.1
        death_date = 1512.1.1
        claim = 84
        adm = 7
        dip = 7
        mil = 8
        leader = {
            name =  "Sadakage" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1486.8.3 = { 
monarch = { 
name = "Sadakage" 
dynasty = "Asakura" 
adm = 7 
dip = 7 
mil = 8 
leader = { name = "Asakura Sadakage" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1486.8.3 = {
    heir = {
        name = "Takakage"
        monarch_name =  "Takakage" 
        dynasty =  "Asakura" 
        birth_date = 1473.1.1
        death_date = 1546.1.1
        claim = 71
        adm = 3
        dip = 3
        mil = 3
        leader = {
            name =  "Takakage" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1512.1.1 = { 
monarch = { 
name = "Takakage" 
dynasty = "Asakura" 
adm = 3 
dip = 3 
mil = 3 
leader = { name = "Asakura Takakage" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1512.1.1 = {
    heir = {
        name = "Yoshikage"
        monarch_name =  "Yoshikage" 
        dynasty =  "Asakura" 
        birth_date = 1506.1.1
        death_date = 1517.1.1
        claim = 71
        adm = 3
        dip = 3
        mil = 3
        leader = {
            name =  "Yoshikage" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}

1546.1.1 = { 
monarch = { 
name = "Yoshikage" 
dynasty = "Asakura" 
adm = 3 
dip = 3 
mil = 3 
leader = { name = "Asakura Yosshikage" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 


1546.1.1 = { 
heir = { 
name = "Kagetake" 
monarch_name = "Kagetake" 
dynasty = "Akamatsu" 
birth_date = 1536.1.1 
death_date = 1575.1.1 
claim = 95 
adm = 5 
dip = 8 
mil = 7 
leader = { 
name = "Kagetake" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 

