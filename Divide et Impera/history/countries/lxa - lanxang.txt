government = eastern_despotism
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 1
secularism_theocracy = 0
offensive_defensive = 1
land_naval = -1
quality_quantity = 3
serfdom_freesubjects = -3
technology_group = chinese
primary_culture = lao
religion = buddhism
capital = 615	# Luang Prabang (Xieng dong Xieng Tong)


1353.1.1 = {
	monarch = {
		name = "Fa Ladhuraniya"
		dynasty = "Khun Lo"
		DIP = 6
		ADM = 7
		MIL = 8
	}
}

1356.1.1 = {
	heir = {
		name = "Samu Sena Daya"
		monarch_name = "Samu Sena Daya"
		dynasty = "Khun Lo"
		birth_date = 1356.1.1
		death_date = 1417.1.1
		claim = 95
		adm = 5
		dip = 4
		mil = 3
	}
}

1371.1.1 = {
	monarch = {
		name = "Samu Sena Daya"
		dynasty = "Khun Lo"
		DIP = 5
		ADM = 4
		MIL = 3
	}
}

1375.1.1 = {
	heir = {
		name = "Lamakamadinga"
		monarch_name = "Lamakamadinga"
		dynasty = "Khun Lo"
		birth_date = 1375.1.1
		death_date = 1428.1.1
		claim = 95
		adm = 6
		dip = 4
		mil = 5
	}
}

1417.1.1 = {
	monarch = {
		name = "Lamakamadinga"
		dynasty = "Khun Lo"
		DIP = 4
		ADM = 6
		MIL = 5
	}
}

1417.1.1 = {
	heir = {
		name = "Bhumadarada"
		monarch_name = "Bhumadarada"
		dynasty = "Khun Lo"
		birth_date = 1400.1.1
		death_date = 1429.1.1
		claim = 95
		adm = 6
		dip = 5
		mil = 3
	}
}

1428.1.1 = {
	monarch = {
		name = "Bhumadarada"
		dynasty = "Khun Lo"
		DIP = 5
		ADM = 6
		MIL = 3
	}
}

1428.1.1 = {
	heir = {
		name = "Yugandhara"
		monarch_name = "Yugandhara"
		dynasty = "Khun Lo"
		birth_date = 1420.1.1
		death_date = 1430.1.1
		claim = 95
		adm = 4
		dip = 4
		mil = 4
	}
}

1429.1.1 = {
	monarch = {
		name = "Yugandhara"
		dynasty = "Khun Lo"
		DIP = 4
		ADM = 4
		MIL = 4
	}
}

1429.1.1 = {
	heir = {
		name = "Gunakama"
		monarch_name = "Gunakama"
		dynasty = "Khun Lo"
		birth_date = 1400.1.1
		death_date = 1432.1.1
		claim = 95
		adm = 6
		dip = 6
		mil = 5
	}
}

1430.1.1 = {
	monarch = {
		name = "Gunakama"
		dynasty = "Khun Lo"
		DIP = 6
		ADM = 6
		MIL = 5
	}
}

1430.1.1 = {
	heir = {
		name = "Lohajaya"
		monarch_name = "Lohajaya"
		dynasty = "Khun Lo"
		birth_date = 1415.1.1
		death_date = 1433.1.1
		claim = 95
		adm = 4
		dip = 4
		mil = 5
	}
}

1432.1.1 = {
	monarch = {
		name = "Lohajaya"
		dynasty = "Khun Lo"
		DIP = 4
		ADM = 4
		MIL = 5
	}
}

1432.1.1 = {
	heir = {
		name = "Kaya Buwanabana"
		monarch_name = "Kaya Buwanabana"
		dynasty = "Khun Lo"
		birth_date = 1432.1.1
		death_date = 1436.1.1
		claim = 95
		adm = 6
		dip = 3
		mil = 3
	}
}

1433.1.1 = {
	monarch = {
		name = "Kaya Buwanabana"
		dynasty = "Khun Lo"
		DIP = 3
		ADM = 6
		MIL = 3
	}
}

1433.1.1 = {
	heir = {
		name = "Kama Kirti"
		monarch_name = "Kama Kirti"
		birth_date = 1420.1.1
		death_date = 1438.1.1
		claim = 95
		adm = 4
		dip = 5
		mil = 6
	}
}

1436.1.1 = {
	monarch = {
		name = "Kama Kirti"
		dynasty = "Khun Lo"
		DIP = 5
		ADM = 4
		MIL = 6
	}
}

1436.1.1 = {
	heir = {
		name = "Sai Tiakap'at"
		monarch_name = "Sai Tiakap'at"
		dynasty = "Khun Lo"
		birth_date = 1420.1.1
		death_date = 1479.1.1
		claim = 95
		adm = 7
		dip = 7
		mil = 5
	}
}

1438.1.1 = {
	monarch = {
		name = "Sai Tiakap'at"
		dynasty = "Khun Lo"
		DIP = 7
		ADM = 7
		MIL = 5
	}
}

1438.1.1 = {
	heir = {
		name = "T'ene Kham"
		monarch_name = "T'ene Kham"
		dynasty = "Khun Lo"
		birth_date = 1430.1.1
		death_date = 1486.1.1
		claim = 95
		adm = 4
		dip = 4
		mil = 5
	}
}

1479.1.1 = {
	monarch = {
		name = "T'ene Kham"
		dynasty = "Khun Lo"
		DIP = 4
		ADM = 4
		MIL = 5
	}
}

1479.1.1 = {
	heir = {
		name = "La Sene T'ai"
		monarch_name = "La Sene T'ai"
		dynasty = "Khun Lo"
		birth_date = 1470.1.1
		death_date = 1496.1.1
		claim = 95
		adm = 4
		dip = 5
		mil = 4
	}
}

1486.1.1 = {
	monarch = {
		name = "La Sene T'ai"
		dynasty = "Khun Lo"
		DIP = 5
		ADM = 4
		MIL = 4
	}
}

1486.1.1 = {
	heir = {
		name = "Som-P'ou"
		monarch_name = "Som-P'ou"
		dynasty = "Khun Lo"
		birth_date = 1480.1.1
		death_date = 1501.1.1
		claim = 95
		adm = 4
		dip = 5
		mil = 4
	}
}

1496.1.1 = {
	monarch = {
		name = "Som-P'ou"
		dynasty = "Khun Lo"
		DIP = 5
		ADM = 4
		MIL = 4
	}
}

1496.1.1 = {
	heir = {
		name = "Visoun"
		monarch_name = "Visoun"
		dynasty = "Khun Lo"
		birth_date = 1490.1.1
		death_date = 1520.1.1
		claim = 95
		adm = 6
		dip = 6
		mil = 5
	}
}

1501.1.1 = {
	monarch = {
		name = "Visoun"
		dynasty = "Khun Lo"
		DIP = 6
		ADM = 6
		MIL = 5
	}
}

1501.1.1 = {
	heir = {
		name = "P'ot'israt"
		monarch_name = "P'ot'israt I"
		dynasty = "Khun Lo"
		birth_date = 1501.1.1
		death_date = 1548.1.1
		claim = 95
		adm = 6
		dip = 6
		mil = 6
	}
}

1520.1.1 = {
	monarch = {
		name = "P'ot'israt I"
		dynasty = "Khun Lo"
		DIP = 6
		ADM = 6
		MIL = 6
	}
}

1520.1.1 = {
	heir = {
		name = "Sett'at'irat"
		monarch_name = "Sett'at'irat"
		dynasty = "Khun Lo"
		birth_date = 1520.1.1
		death_date = 1571.1.1
		claim = 95
		adm = 8
		dip = 6
		mil = 5
	}
}

1548.1.1 = {
	monarch = {
		name = "Sett'at'irat"
		dynasty = "Khun Lo"
		DIP = 6
		ADM = 8
		MIL = 5
	}
}

1548.1.1 = {
	heir = {
		name = "Sene Soulint'a"
		monarch_name = "Sene Soulint'a"
		dynasty = "Khun Lo"
		birth_date = 1530.1.1
		death_date = 1582.1.1
		claim = 95
		adm = 3
		dip = 3
		mil = 4
	}
}

1560.1.1 = { capital = 614 } # Vientiane

1571.1.1 = {
	monarch = {
		name = "Sene Soulint'a"
		dynasty = "Khun Lo"
		DIP = 3
		ADM = 3
		MIL = 4
	}
}

1571.1.1 = {
	heir = {
		name = "Maha Oupahat"
		monarch_name = "Maha Oupahat"
		dynasty = "Khun Lo"
		birth_date = 1565.1.1
		death_date = 1580.1.1
		claim = 95
		adm = 3
		dip = 3
		mil = 5
	}
}

1575.1.1 = {
	monarch = {
		name = "Maha Oupahat"
		dynasty = "Khun Lo"
		DIP = 3
		ADM = 3
		MIL = 5
	}
}

1575.1.1 = {
	heir = {
		name = "Sene Soulint'a"
		monarch_name = "Sene Soulint'a"
		dynasty = "Khun Lo"
		birth_date = 1530.1.1
		death_date = 1582.1.1
		claim = 95
		adm = 3
		dip = 3
		mil = 4
	}
}

1580.1.1 = {
	monarch = {
		name = "Sene Soulint'a" # Restored
		dynasty = "Khun Lo"
		DIP = 3
		ADM = 3
		MIL = 4
	}
}

1580.1.1 = {
	heir = {
		name = "Nakhone Noi"
		monarch_name = "Nakhone Noi"
		dynasty = "Khun Lo"
		birth_date = 1560.1.1
		death_date = 1591.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 4
	}
}

1582.1.1 = {
	monarch = {
		name = "Nakhone Noi"
		dynasty = "Khun Lo"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1582.1.1 = {
	heir = {
		name = "Nokeo Koumane"
		monarch_name = "Nokeo Koumane"
		dynasty = "Khun Lo"
		birth_date = 1570.1.1
		death_date = 1596.1.1
		claim = 95
		adm = 4
		dip = 5
		mil = 4
	}
}

1591.1.1 = {
	monarch = {
		name = "Nokeo Koumane"
		dynasty = "Khun Lo"
		DIP = 5
		ADM = 4
		MIL = 4
	}
}

1591.1.1 = {
	heir = {
		name = "T'ammikarat"
		monarch_name = "T'ammikarat"
		dynasty = "Khun Lo"
		birth_date = 1590.1.1
		death_date = 1622.1.1
		claim = 95
		adm = 6
		dip = 6
		mil = 5
	}
}

1596.1.1 = {
	monarch = {
		name = "T'ammikarat"
		dynasty = "Khun Lo"
		DIP = 6
		ADM = 6
		MIL = 5
	}
}

1596.1.1 = {
	heir = {
		name = "Oupagnouvarat"
		monarch_name = "Oupagnouvarat"
		dynasty = "Khun Lo"
		birth_date = 1590.1.1
		death_date = 1623.1.1
		claim = 95
		adm = 3
		dip = 3
		mil = 3
	}
}

1622.1.1 = {
	monarch = {
		name = "Oupagnouvarat"
		dynasty = "Khun Lo"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1622.1.1 = {
	heir = {
		name = "P'ot'isarat"
		monarch_name = "P'ot'isarat II"
		dynasty = "Khun Lo"
		birth_date = 1600.1.1
		death_date = 1627.1.1
		claim = 95
		adm = 3
		dip = 4
		mil = 3
	}
}

1623.1.1 = {
	monarch = {
		name = "P'ot'isarat II"
		dynasty = "Khun Lo"
		DIP = 4
		ADM = 3
		MIL = 3
	}
}

1623.1.1 = {
	heir = {
		name = "Mone Keo"
		monarch_name = "Mone Keo"
		birth_date = 1600.1.1
		death_date = 1629.1.1
		claim = 95
		adm = 3
		dip = 3
		mil = 4
	}
}

1627.1.1 = {
	monarch = {
		name = "Mone Keo"
		dynasty = "Khun Lo"
		DIP = 3
		ADM = 3
		MIL = 4
	}
}

1627.1.1 = {
	heir = {
		name = "Oupagnouvarat"
		monarch_name = "Oupagnouvarat"
		birth_date = 1590.1.1
		death_date = 1631.1.1
		claim = 95
		adm = 3
		dip = 3
		mil = 3
	}
}

1629.1.1 = {
	monarch = {
		name = "Oupagnouvarat"
		dynasty = "Khun Lo"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1629.1.1 = {
	heir = {
		name = "Tone Kham"
		monarch_name = "Tone Kham"
		dynasty = "Khun Lo"
		birth_date = 1580.1.1
		death_date = 1635.1.1
		claim = 95
		adm = 6
		dip = 6
		mil = 6
	}
}

1631.1.1 = {
	monarch = {
		name = "Tone Kham"
		dynasty = "Khun Lo"
		DIP = 6
		ADM = 6
		MIL = 6
	}
}

1631.1.1 = {
	heir = {
		name = "Visai"
		monarch_name = "Visai"
		dynasty = "Khun Lo"
		birth_date = 1600.1.1
		death_date = 1637.1.1
		claim = 95
		adm = 3
		dip = 3
		mil = 3
	}
}

1635.1.1 = {
	monarch = {
		name = "Visai"
		dynasty = "Khun Lo"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1635.1.1 = {
	heir = {
		name = "Souligna Vongsa"
		monarch_name =" Souligna Vongsa"
		dynasty = "Khun Lo"
		birth_date = 1600.1.1
		death_date = 1648.1.1
		claim = 95
		adm = 8
		dip = 8
		mil = 8
	}
}

1637.1.1 = {
	monarch = {
		name = "Souligna Vongsa"
		dynasty = "Khun Lo"
		DIP = 8
		ADM = 8
		MIL = 8
	}
}
