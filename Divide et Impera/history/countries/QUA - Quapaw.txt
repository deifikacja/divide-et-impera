government = tribal_despotism
aristocracy_plutocracy = -2
centralization_decentralization = 1
innovative_narrowminded = 0
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = 3
land_naval = -5
quality_quantity = 0
serfdom_freesubjects = 5
offensive_defensive = -3
primary_culture = mississippian
religion = animism
technology_group = new_world
capital = 897	#Pacaha

1350.1.1 = {
	monarch = {
		name = "Chief of Quapaw"
		adm = 6
		dip = 6
		mil = 5
	}
}
1550.1.1 = {
	government = tribal_democracy
	aristocracy_plutocracy = 5
	centralization_decentralization = 5
	innovative_narrowminded = 2
	religion = shamanism
	primary_culture = dakota
}
#End of Mississipian Culture