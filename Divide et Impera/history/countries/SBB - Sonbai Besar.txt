government = tribal_federation
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = 1
offensive_defensive = 0
imperialism_isolationism = 0
secularism_theocracy = -2
land_naval = 2
quality_quantity = -1
serfdom_freesubjects = -1
primary_culture = malay-polynesian
religion = animism
technology_group = chinese
capital = 1405	# East Timor

1550.1.1 = {
	monarch = {
		name = "Laban"
		dynasty = Wahali
		adm = 4
		dip = 4
		mil = 5
	}
}
1570.1.1 = {
	monarch = {
		name = "Nati"
		dynasty = Wahali
		adm = 4
		dip = 4
		mil = 5
	}
}
1600.1.1 = {
	monarch = {
		name = "Faluk"
		dynasty = Wahali
		adm = 4
		dip = 4
		mil = 5
	}
}
1620.1.1 = {
	monarch = {
		name = "Lele"
		dynasty = Wahali
		adm = 4
		dip = 4
		mil = 5
	}
}

1650.1.1 = {
	monarch = {
		name = "Tukula Sonbai"
		dynasty = Wahali
		adm = 4
		dip = 4
		mil = 5
	}
}

1670.1.1 = {
	monarch = {
		name = "Nai Manas Sonbai"
		dynasty = Wahali
		adm = 4
		dip = 4
		mil = 5
	}
}

1700.1.1 = {
	monarch = {
		name = "Neno Sonbai"
		dynasty = Wahali
		adm = 4
		dip = 4
		mil = 5
	}
}

1745.1.1 = {
	monarch = {
		name = "Bernardo"
		dynasty = Wahali
		adm = 5
		dip = 3
		mil = 4
	}
	religion = catholic
}

1760.1.1 = {
	monarch = {
		name = "Nai Tafin Sonbai"
		dynasty = Wahali
		adm = 7
		dip = 7
		mil = 5
	}
}

1768.1.1 = {
	monarch = {
		name = "Nai Kau Sonbai"
		dynasty = Wahali
		adm = 7
		dip = 7
		mil = 5
	}
}

1819.1.1 = {
	monarch = {
		name = "Nai Sobe Sonbai II"
		dynasty = Wahali
		adm = 7
		dip = 7
		mil = 5
	}
}