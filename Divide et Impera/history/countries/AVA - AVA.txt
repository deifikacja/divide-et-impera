government = eastern_despotism
aristocracy_plutocracy = -2
centralization_decentralization = 3
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = -1
land_naval = 0
quality_quantity = 3
serfdom_freesubjects = -2
primary_culture = burmese
religion = buddhism
technology_group = chinese
capital = 584	# Ava

#Pinya kingdom
1350.1.1 = {
	monarch = {
		name = "Kyanswange"
		dynasty="Myinsaing"
		adm = 3
		dip = 4
		mil = 5
	}
}
1350.1.1 = {
	heir = {
		name = "Narathu"
		monarch_name = "Narathu II"
		dynasty = "Myinsaing"
		birth_date = 1350.1.1
		death_date = 1364.1.1
		claim = 80
		adm = 4
		dip = 5
		mil = 3
	}
}
1359.1.1 = {
	monarch = {
		name = "Narathu II"
		dynasty="Myinsaing"
		adm = 4
		dip = 5
		mil = 3
	}
}
1359.1.1 = {
	heir = {
		name = "Uzana Pyaung"
		monarch_name = "Uzana Pyaung"
		dynasty = "Myinsaing"
		birth_date = 1359.1.1
		death_date = 1364.4.1
		claim = 80
		adm = 4
		dip = 5
		mil = 3
	}
}
1364.1.1 = {
	monarch = {
		name = "Uzana Pyaung"
		dynasty="Myinsaing"
		adm = 3
		dip = 3
		mil = 3
	}
}
#Ava kingdom
1364.4.1 = {
	monarch = {
		name = "Thadominbya"
		dynasty="Ava"
		adm = 6
		dip = 4
		mil = 7
	}
}
1364.4.1 = {
	heir = {
		name = "Minkyiswasawke"
		monarch_name = "Minkyiswasawke"
		dynasty = "Myinsaing"
		birth_date = 1364.1.1
		death_date = 1401.4.1
		claim = 80
		adm = 4
		dip = 5
		mil = 3
	}
}
1368.1.1 = {
	monarch = {
		name = "Minkyiswasawke"
		dynasty="Ava"
		adm = 3
		dip = 3
		mil = 3
	}
}

1401.1.1 = {
	monarch = {
		name = "Tarabya"
		dynasty="Ava"
		adm = 5
		dip = 5
		mil = 5
	}
}

1401.5.1 = {
	monarch = {
		name = "Minhkaung I"
		dynasty="Ava"
		adm = 6
		dip = 6
		mil = 5
	}
}

1422.1.1 = {
	monarch = {
		name = "Thihathu"
		dynasty="Ava"
		adm = 3
		dip = 3
		mil = 3
	}
}

1426.1.1 = {
	monarch = {
		name = "Minhlange"
		dynasty="Ava"
		adm = 7
		dip = 6
		mil = 6
	}
}

1426.5.1 = {
	monarch = {
		name = "Kalekyetaungnvo"
		dynasty="Ava"
		adm = 9
		dip = 8
		mil = 8
	}
}

1427.1.1 = {
	monarch = {
		name = "Mohnyinthado"
		dynasty="Ava"
		adm = 7
		dip = 6
		mil = 7
	}
}

1440.1.1 = {
	monarch = {
		name = "Minrekyawswa"
		dynasty="Ava"
		adm = 3
		dip = 3
		mil = 4
	}
}

1443.1.1 = {
	monarch = {
		name = "Narapati I"
		dynasty="Ava"
		adm = 8
		dip = 8
		mil = 6
	}
}

1469.1.1 = {
	monarch = {
		name = "Thihathura"
		dynasty="Ava"
		adm = 8
		dip = 5
		mil = 8
	}
}

1481.1.1 = {
	monarch = {
		name = "Minhkaung II"
		dynasty="Ava"
		adm = 3
		dip = 4
		mil = 4
	}
}

1502.1.1 = {
	monarch = {
		name = "Shwenankyanshin"
		dynasty="Ava"
		adm = 3
		dip = 3
		mil = 5
	}
}

1527.1.1 = {
	monarch = {
		name = "Thohanbwa"
		dynasty="Ava"
		adm = 3
		dip = 3
		mil = 3
	}
}

1543.1.1 = {
	monarch = {
		name = "Hkonmaing"
		dynasty="Ava"
		adm = 4
		dip = 4
		mil = 4
	}
}

1546.1.1 = {
	monarch = {
		name = "Mobye Narapati II"
		dynasty="Ava"
		adm = 3
		dip = 3
		mil = 3
	}
}

1552.1.1 = {
	monarch = {
		name = "Sithkyawhtin"
		dynasty="Ava"
		adm = 3
		dip = 4
		mil = 4
	}
}
#To Toungu thereafter