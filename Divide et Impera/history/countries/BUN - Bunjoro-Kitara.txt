government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 0
land_naval = -5
quality_quantity = 2
serfdom_freesubjects = -5
primary_culture = bantu
religion = shamanism
technology_group = african
capital = 1289	# Bunjoro

#Kingdom of Kitara
1351.1.1 = {
	monarch = {
		name = "Bachwezi"
		dynasty = Bachwezi
		adm = 6
		dip = 5
		mil = 7
	}
}
#Kitara broken into several minor states - Bunyoro-Kitara
1450.1.1 = {
	monarch = {
		name = "Rukidi Mpugu"
		dynasty = Babito
		adm = 4
		dip = 5
		mil = 6
	}
}

1470.1.1 = {
	monarch = {
		name = "Ocaki Rwangirra"
		dynasty = Babito
		adm = 4
		dip = 5
		mil = 6
	}
}

1490.1.1 = {
	monarch = {
		name = "Oyo Nyimba"
		dynasty = Babito
		adm = 4
		dip = 5
		mil = 6
	}
}

1500.1.1 = {
	monarch = {
		name = "Winyi I Rumbeka"
		dynasty = Babito
		adm = 4
		dip = 5
		mil = 6
	}
}

1510.1.1 = {
	monarch = {
		name = "Olimi I Kalimbi"
		dynasty = Babito
		adm = 4
		dip = 5
		mil = 6
	}
}

1530.1.1 = {
	monarch = {
		name = "Nyabongo I Rulemu"
		dynasty = Babito
		adm = 4
		dip = 5
		mil = 6
	}
}

1550.1.1 = {
	monarch = {
		name = "Winyi II Rubagiramasega"
		dynasty = Babito
		adm = 4
		dip = 5
		mil = 6
	}
}

1570.1.1 = {
	monarch = {
		name = "Olimi II Ruhundwangeye"
		dynasty = Babito
		adm = 4
		dip = 5
		mil = 6
	}
}

1590.1.1 = {
	monarch = {
		name = "Nyara Omuzzara Kyaro"
		dynasty = Babito
		adm = 4
		dip = 5
		mil = 6
	}
}
1610.1.1 = {
	monarch = {
		name = "Chwa I Mali Rumoma Mahanga"
		dynasty = Babito
		adm = 4
		dip = 5
		mil = 6
	}
}

1630.1.1 = {
	monarch = {
		name = "Mashamba"
		dynasty = Babito
		adm = 4
		dip = 5
		mil = 6
	}
}

1650.1.1 = {
	monarch = {
		name = "Kyebambe I Omuzikya"
		dynasty = Babito
		adm = 4
		dip = 5
		mil = 6
	}
}

1670.1.1 = {
	monarch = {
		name = "Winyi III Ruguruka"
		dynasty = Babito
		adm = 4
		dip = 5
		mil = 6
	}
}

1690.1.1 = {
	monarch = {
		name = "Nyaika"
		dynasty = Babito
		adm = 4
		dip = 5
		mil = 6
	}
}

1700.1.1 = {
	monarch = {
		name = "Kyebambe II Bikaju"
		dynasty = Babito
		adm = 4
		dip = 5
		mil = 6
	}
}

1710.1.1 = {
	monarch = {
		name = "Olimi III Isansa"
		dynasty = Babito
		adm = 4
		dip = 5
		mil = 6
	}
}

1731.1.1 = {
	monarch = {
		name = "Duhaga I Chwe Mujwiga"
		dynasty = Babito
		adm = 4
		dip = 5
		mil = 6
	}
}

1782.1.1 = {
	monarch = {
		name = "Olimi IV Kasoma"
		dynasty = Babito
		adm = 4
		dip = 5
		mil = 6
	}
}

1786.1.1 = {
	monarch = {
		name = "Kyebambe III Nyamutukura"
		dynasty = Babito
		adm = 4
		dip = 5
		mil = 6
	}
}