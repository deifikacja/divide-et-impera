government = caste_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = -2
offensive_defensive = 0
land_naval = -1
quality_quantity = -1
serfdom_freesubjects = -1
technology_group = indian
religion = hinduism
primary_culture = kannada
capital = 1970	# Banganapalle


#Raja
1580.1.1 = {
	monarch = {
		name = "Nanda Chakravathy"
		dynasty = "Najm-i-Sani"
		adm = 6
		dip = 7
		mil = 6
	}
}
1601.1.1 = { religion = sunni government = eastern_despotism }
1601.1.1 = {
	monarch = {
		name = "Siddhu"
		dynasty = Sumbal
		adm = 5
		dip = 7
		mil = 6
	}
}

1665.1.1 = {
	monarch = {
		name = "Muhammad Beg Khan-e"
		dynasty = "Rosebahani"
		adm = 5
		dip = 7
		mil = 6
	}
}

1686.1.1 = {
	monarch = {
		name = "Fadli `Ali Khan I"
		dynasty = "Najm-i-Sani"
		adm = 4
		dip = 6
		mil = 7
	}
}

1758.1.1 = {
	monarch = {
		name = "Fadli `Ali Khan II"
		dynasty = "Najm-i-Sani"
		adm = 5
		dip = 6
		mil = 5
	}
}

1769.5.7 = {
	monarch = {
		name = "Hosayn `Ali Khan"
		dynasty = "Naqdi"
		adm = 6
		dip = 4
		mil = 4
	}
}

1783.8.23 = {
	monarch = {
		name = "Gholam `Ali Khan I"
		dynasty = "Naqdi"
		adm = 5
		dip = 4
		mil = 6
	}
}
#Annexed by Mysore in 1784, regained indepedence 1790 with the same monarch