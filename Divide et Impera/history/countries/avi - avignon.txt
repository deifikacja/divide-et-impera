government = theocratic_government
aristocracy_plutocracy = -5
centralization_decentralization = 3
innovative_narrowminded = 2
mercantilism_freetrade = -1
imperialism_isolationism = 1
secularism_theocracy = 5
offensive_defensive = 2
land_naval = -3
quality_quantity = 3
serfdom_freesubjects = -2
religion = catholic
primary_culture = occitain
add_accepted_culture = cosmopolitan_french
technology_group = western
capital = 202	# Avignon


1378.9.20 = {
	monarch = { 
		name = "Clement VII"
		adm = 4
		dip = 3
		mil = 6
	}
}

1394.5.10 = {
	monarch = { 
		name = "Benedict XIII"
		adm = 4
		dip = 6
		mil = 3
	}
} 

#1417 end of  the West Schism