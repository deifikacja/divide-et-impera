government = tribal_federation
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 3
mercantilism_freetrade = 1
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 1
land_naval = 2
quality_quantity = -1
serfdom_freesubjects = -1
technology_group = chinese
religion = animism
primary_culture = filipino
capital = 654	# Cebu


1350.1.1 = {
	monarch = {
		name = "Cebu Raja"
		dynasty = Chola
		DIP = 3
		ADM = 4
		MIL = 4
	}
}

1450.1.1 = {
	monarch = {
		name = "Sri Bantug Lamay"
		dynasty = Chola
		DIP = 6
		ADM = 6
		MIL = 4
	}
}

1510.1.1 = {
	monarch = {
		name = "Humabon"
		dynasty = Chola
		DIP = 6
		ADM = 6
		MIL = 4
	}
leader = { name = "Lapu-Lapu" type = general rank = 1 fire = 1 shock = 3 manuever = 3 siege = 0 death_date = 1542.1.1 }
}
1530.1.1 = { religion = catholic }#Estimated

1540.1.1 = {
	monarch = {
		name = "Tupas"
		dynasty = Chola
		DIP = 4
		ADM = 4
		MIL = 5
	}
	religion = animism
}