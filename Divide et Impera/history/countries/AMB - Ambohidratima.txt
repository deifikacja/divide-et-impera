government = tribal_despotism
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -3
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 1
land_naval = -1
quality_quantity = 0
serfdom_freesubjects = -3
primary_culture = madagasque
religion = animism
technology_group = african
capital = 1280	# Tananarivo

1345.1.1 = {
	monarch = {
		name = "Andriantodratima"
		dynasty="Ambohidratrimo"
		DIP = 5
		ADM = 6
		MIL = 7
	}
}

1710.1.1 = {
	monarch = {
		name = "Andriantompoinimerina I"
		dynasty="Ambohidratrimo"
		DIP = 7
		ADM = 8
		MIL = 7
	}
}

1737.1.1 = {
	monarch = {
		name = "Andriantrimonibemihisatra"
		dynasty="Ambohidratrimo"
		DIP = 6
		ADM = 5
		MIL = 5
	}
}

1747.1.1 = {
	monarch = {
		name = "Andriamanananimerina"
		dynasty="Ambohidratrimo"
		DIP = 5
		ADM = 7
		MIL = 7
	}
}

1767.1.1 = {
	monarch = {
		name = "Andriantompoinimerina II"
		dynasty="Ambohidratrimo"
		DIP = 4
		ADM = 3
		MIL = 4
	}
}

1774.1.1 = {
	monarch = {
		name = "Andriambelo"
		dynasty="Ambohidratrimo"
		DIP = 5
		ADM = 3
		MIL = 4
	}
}

1780.1.1 = {
	monarch = {
		name = "Andriambelononana"
		dynasty="Ambohidratrimo"
		DIP = 3
		ADM = 4
		MIL = 6
	}
}

1785.1.1 = {
	monarch = {
		name = "Ramanandrianjaka"
		dynasty="Ambohidratrimo"
		DIP = 6
		ADM = 6
		MIL = 5
		female = yes
	}
}

1790.1.1 = {
	monarch = {
		name = "Rabehety"
		dynasty="Ambohidratrimo"
		DIP = 7
		ADM = 6
		MIL = 4
		female = yes
	}
}