government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 2
land_naval = -5
quality_quantity = 2
serfdom_freesubjects = -5
primary_culture = bantu
religion = shamanism
technology_group = african
capital = 1302	# Serowe

1351.1.1 = {
	monarch = {
		name = "Kgosi"
		dynasty="Ngwato"
		adm = 4
		dip = 4
		mil = 5
	}
}

1610.1.1 = {
	monarch = {
		name = "Seogola"
		dynasty="Ngwato"
		adm = 4
		dip = 5
		mil = 6
	}
}

1660.1.1 = {
	monarch = {
		name = "Madirana"
		dynasty="Ngwato"
		adm = 4
		dip = 5
		mil = 6
	}
}

1710.1.1 = {
	monarch = {
		name = "Kesitihoe"
		dynasty="Ngwato"
		adm = 4
		dip = 5
		mil = 6
	}
}

1711.1.1 = {
	monarch = {
		name = "Makgasama"
		dynasty="Ngwato"
		adm = 4
		dip = 5
		mil = 6
	}
}

1720.1.1 = {
	monarch = {
		name = "Molete"
		dynasty="Ngwato"
		adm = 4
		dip = 5
		mil = 6
	}
}

1770.1.1 = {
	monarch = {
		name = "Mokgadi"
		dynasty="Ngwato"
		adm = 4
		dip = 5
		mil = 6
	}
}

1780.1.1 = {
	monarch = {
		name = "Mathiba a Moleta"
		dynasty="Ngwato"
		adm = 4
		dip = 5
		mil = 6
	}
}

1795.1.1 = {
	monarch = {
		name = "Kgama I a Mathiba"
		dynasty="Ngwato"
		adm = 4
		dip = 5
		mil = 6
	}
}

1817.1.1 = {
	monarch = {
		name = "Kgari a Kgama"
		dynasty="Ngwato"
		adm = 4
		dip = 5
		mil = 6
	}
}

1828.1.1 = {
	monarch = {
		name = "Sedimo a Molosiwa"
		dynasty="Ngwato"
		adm = 4
		dip = 5
		mil = 6
	}
}
