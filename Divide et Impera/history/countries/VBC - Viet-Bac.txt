# Viet Bac http://my.raex.com/~obsidian/seasia.html#Viet-Bac
government = eastern_despotism
aristocracy_plutocracy = -2
centralization_decentralization = 3
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 1
land_naval = -3
quality_quantity = 3
serfdom_freesubjects = -2
primary_culture = vietnamese
religion = buddhism
technology_group = chinese
capital = 613	# Hanoi

#Witihn Dai-Viet to 1533
#Mac dynasty
1527.1.1 = {
	monarch = {
		name = "Dang-Dung"
		dynasty = Mac
		adm = 4
		dip = 6
		mil = 3
	}
}

1530.1.1 = {
	monarch = {
		name = "Dang-Doanh"
		dynasty = Mac
		adm = 3
		dip = 5
		mil = 3
	}
}

1540.1.1 = {
	monarch = {
		name = "Phuc-Hai"
		dynasty = Mac
		adm = 3
		dip = 3
		mil = 6
	}
}

1546.1.1 = {
	monarch = {
		name = "Phuc-Nguyen"
		dynasty = Mac
		adm = 4
		dip = 3
		mil = 5
	}
}

1562.1.1 = {
	monarch = {
		name = "Mau-Hop"
		dynasty = Mac
		adm = 3
		dip = 3
		mil = 3
	}
}

1592.1.1 = {
	monarch = {
		name = "Toan"
		dynasty = Mac
		adm = 4
		dip = 3
		mil = 3
	}
}

1592.5.1 = {
	monarch = {
		name = "Kinh-Chi"
		dynasty = Mac
		adm = 4
		dip = 3
		mil = 3
	}
}

1592.5.1 = {
	monarch = {
		name = "Kinh-Cung"
		dynasty = Mac
		adm = 4
		dip = 3
		mil = 3
	}
}

1592.5.1 = {
	monarch = {
		name = "Kinh-Khoan"
		dynasty = Mac
		adm = 4
		dip = 3
		mil = 3
	}
}

1592.5.1 = {
	monarch = {
		name = "Kinh-Hoan"
		dynasty = Mac
		adm = 4
		dip = 3
		mil = 3
	}
}
#To Tonkin therefater