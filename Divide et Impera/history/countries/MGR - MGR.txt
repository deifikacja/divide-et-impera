government = tribal_federation
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = 1
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = 0
land_naval = 5
quality_quantity = 0
serfdom_freesubjects = 0
primary_culture = tahitan
religion = animism
technology_group = pacific
capital = 2035 # Mangareva

#`Akariki of Mangareva
1500.1.1 = {
	monarch = {
		name = "Apeiti"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}
1550.1.1 = {
	monarch = {
		name = "Mei`ara-tu`arua"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1600.1.1 = {
	monarch = {
		name = "Pokau"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1650.1.1 = {
	monarch = {
		name = "O`oke`u"
		DIP = 4
		ADM = 4
		MIL = 5
	}
}

1700.1.1 = {
	monarch = {
		name = "Te-Makorotau-eriki"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1750.1.1 = {
	monarch = {
		name = "Te-Mangi-tutavake"
		DIP = 8
		ADM = 8
		MIL = 2
	}
}

1800.1.1 = {
	monarch = {
		name = "Te-`Akariki-tea"
		DIP = 6
		ADM = 5
		MIL = 2
	}
}