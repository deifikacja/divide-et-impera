government = tribal_despotism
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -3
offensive_defensive = -2
imperialism_isolationism = 0
secularism_theocracy = 0
land_naval = -3
quality_quantity = 0
serfdom_freesubjects = -3
primary_culture = hausa
religion = sunni
technology_group = sahel
capital = 1154	# Katsina

#Zazzau dynasty
1356.1.1 = {
	monarch = {
		name = "Zazzau"
		dynasty = Habe
		adm = 5
		dip = 6
		mil = 6
	}
}

1505.1.1 = {
	monarch = {
		name = "Monan Abu"
		dynasty = Habe
		adm = 5
		dip = 6
		mil = 6
	}
}

1530.1.1 = {
	monarch = {
		name = "Gidan dan Masukanan"
		dynasty = Habe
		adm = 5
		dip = 6
		mil = 6
	}
}

1532.1.1 = {
	monarch = {
		name = "Nohir"
		dynasty = Habe
		adm = 5
		dip = 6
		mil = 6
	}
}

1535.1.1 = {
	monarch = {
		name = "Kawanissa"
		dynasty = Habe
		adm = 5
		dip = 6
		mil = 6
	}
}

1536.1.1 = {
	monarch = {
		name = "Bakwa Tunuru"
		dynasty = Habe
		adm = 5
		dip = 6
		mil = 6
		female = yes
	}
}

1539.10.1 = {
	monarch = {
		name = "Ibrahim I"
		dynasty = Habe
		adm = 5
		dip = 6
		mil = 6
	}
}

1566.1.1 = {
	monarch = {
		name = "Karama"
		dynasty = Habe
		adm = 5
		dip = 6
		mil = 6
	}
}

1576.1.1 = {
	monarch = {
		name = "Kafo"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1578.1.1 = {
	monarch = {
		name = "Bako I"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1581.1.1 = {
	monarch = {
		name = "'Aliyu I"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1587.1.1 = {
	monarch = {
		name = "Izmail"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1598.1.1 = {
	monarch = {
		name = "Musa"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1598.5.1 = {
	monarch = {
		name = "Gadi"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1601.1.1 = {
	monarch = {
		name = "Hamza"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1601.5.1 = {
	monarch = {
		name = "'Abdullah"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1610.1.1 = {
	monarch = {
		name = "Burema I"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1613.1.1 = {
	monarch = {
		name = "'Aliyu II"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1640.1.1 = {
	monarch = {
		name = "Muhama Rabo"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1641.1.1 = {
	monarch = {
		name = "Ibrahim II Basuki"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1654.1.1 = {
	monarch = {
		name = "Bako II"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1657.5.5 = {
	monarch = {
		name = "Sukana"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1658.1.1 = {
	monarch = {
		name = "'Aliyu III"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1665.1.1 = {
	monarch = {
		name = "Ibrahim III"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1668.1.1 = {
	monarch = {
		name = "Muhammad Abu"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1686.1.1 = {
	monarch = {
		name = "Sayo 'Ali"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1696.1.1 = {
	monarch = {
		name = "Bako III dan Musa"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1701.1.1 = {
	monarch = {
		name = "Ishaq"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1703.1.1 = {
	monarch = {
		name = "Burema II Ashakuka"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1704.1.1 = {
	monarch = {
		name = "Bako IV dan Sunkuru"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1715.1.1 = {
	monarch = {
		name = "Muhammad dan Gunguma"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1726.1.1 = {
	monarch = {
		name = "Uban Bawa"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}

1733.1.1 = {
	monarch = {
		name = "Muhammad Gani"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}
1734.1.1 = {
	monarch = {
		name = "Abu Muhammad Gani"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}
1734.1.5 = {
	monarch = {
		name = "Dan Ashakuka"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}
1737.1.1 = {
	monarch = {
		name = "Muhammad Abu"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}
1757.1.1 = {
	monarch = {
		name = "Bawo"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}
1759.1.1 = {
	monarch = {
		name = "Yunusa"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}
1764.1.1 = {
	monarch = {
		name = "Yaqub"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}
1773.1.1 = {
	monarch = {
		name = "Chikkoku"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}
1779.1.1 = {
	monarch = {
		name = "Muhama Maigamo"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}
1757.1.1 = {
	monarch = {
		name = "Ishaq II Jatau"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}
#Sokoto dominance
1806.1.1 = {
	monarch = {
		name = "Muhammad Makau"
		dynasty = Habe
		adm = 4
		dip = 9
		mil = 5
	}
}
#Fulani rule
1808.1.1 = {
	monarch = {
		name = "Malam Musa"
		dynasty = Fulani
		adm = 4
		dip = 9
		mil = 5
	}
}
1821.1.1 = {
	monarch = {
		name = "Yamusa"
		dynasty = Fulani
		adm = 4
		dip = 9
		mil = 5
	}
}