government = daimyo
aristocracy_plutocracy = -4
centralization_decentralization = 4
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = -1
offensive_defensive = -2
land_naval = 0
quality_quantity = 4
serfdom_freesubjects = -5
primary_culture = japanese
religion = shinto
technology_group = chinese
capital = 1025	# Shimotsuke
daimyo = yes

1356.1.1 = { 
monarch = { 
name = "Kaneyori" 
dynasty = "Mogami" 
adm = 4 
dip = 5 
mil = 7 
leader = { name = "Mogami Kaneyori" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 
1450.1.1 = { 
monarch = { 
name = "Mitsuie" 
dynasty = "Mogami" 
adm = 4 
dip = 5 
mil = 7 
leader = { name = "Mogami Mitsuie" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 
1490.1.1 = { 
monarch = { 
name = "Yoshisada" 
dynasty = "Mogami" 
adm = 4 
dip = 5 
mil = 7 
leader = { name = "Mogami Yoshisada" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 
1522.1.1 = { 
monarch = { 
name = "Yoshimori" 
dynasty = "Mogami" 
adm = 4 
dip = 5 
mil = 7 
leader = { name = "Mogami Yoshimori" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 
1550.1.1 = { 
monarch = { 
name = "Yoshiaki" 
dynasty = "Mogami" 
adm = 4 
dip = 5 
mil = 7 
leader = { name = "Mogami Kaneyori" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 