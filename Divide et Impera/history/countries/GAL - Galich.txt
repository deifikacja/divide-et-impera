government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -1
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = 0
land_naval = -3
quality_quantity = 2
serfdom_freesubjects = -5
technology_group = eastern
religion = orthodox
primary_culture = russian
capital = 1083	# Viatka

 

#Princes

1341.1.1 = {
	monarch = {
		name = "Dimitr I"
		dynasty = Rurikovich
		DIP = 4
		ADM = 5
		MIL = 3
	}
}

1364.1.1 = {
	monarch = {
		name = "Dimitr II"
		dynasty = Rurikovich
		DIP = 8
		ADM = 7
		MIL = 8
	}
}

1389.1.1 = {
	monarch = {
		name = "Georgij"
		dynasty = Rurikovich
		DIP = 3
		ADM = 4
		MIL = 4
	}
}

1434.1.1 = {
	monarch = {
		name = "Dimitr III"
		dynasty = Rurikovich
		DIP = 3
		ADM = 4
		MIL = 4
	}
}

1440.1.1 = {
	monarch = {
		name = "Dimitr IV Shemyaka"
		dynasty = Rurikovich
		DIP = 3
		ADM = 4
		MIL = 4
	}
}

1450.1.1 = {
	monarch = {
		name = "Vasilii Temny"
		dynasty = Rurikovich
		DIP = 3
		ADM = 4
		MIL = 4
	}
}