government = republican_dictatorship
aristocracy_plutocracy = 1
centralization_decentralization = 3
innovative_narrowminded = -1
mercantilism_freetrade = 0
imperialism_isolationism = -2
secularism_theocracy = 3
offensive_defensive = -3
land_naval = -3
quality_quantity = -2
serfdom_freesubjects = 2
primary_culture = xxx #czech
religion = hussite
capital = 266	#Prag
technology_group = western

1415.1.1 = { primary_culture = czech }

1419.1.1 = {
	monarch = {
		name = "Jan Zizka"
		adm = 7
		dip = 4
		mil = 9
		leader = {	name = "Jan Zizka"             	type = general	rank = 0	fire = 6	shock = 3	manuever = 5	siege = 2}
	}
}

1421.1.1 = { leader = {	name = "Prokop Hol�"           	type = general	rank = 1	fire = 5	shock = 2	manuever = 5	siege = 1}}

1424.10.11 = {
	monarch = {
		name = "Prokop Hol�"
		adm = 7
		dip = 5
		mil = 8
	}
}

1434.5.30 = {
	monarch = {
		name = "Utrakvist�"
		adm = 6
		dip = 5
		mil = 7
	}
	offensive_defensive = 0 aristocracy_plutocracy = -1 serfdom_freesubjects = 0
}

1457.11.24 = {
	monarch = {
		name = "Jiri z Podebrad"
		adm = 8
		dip = 5
		mil = 6
	}
}

1500.1.1 = { primary_culture = xxx }