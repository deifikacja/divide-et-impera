government = military_federation
aristocracy_plutocracy = 3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = 5
imperialism_isolationism = 0
secularism_theocracy = -3
offensive_defensive = -3
land_naval = 3
quality_quantity = 0
serfdom_freesubjects = 2
primary_culture = berber
religion = sunni
technology_group = muslim
capital = 342	# Toubkal

1625.1.1 = {
	monarch = {
		name = "Murat Reis"
		adm = 4
		dip = 3
		mil = 7
	}
}