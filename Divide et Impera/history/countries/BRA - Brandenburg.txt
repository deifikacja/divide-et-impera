government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -3
imperialism_isolationism = -1
secularism_theocracy = 0
offensive_defensive = -2
land_naval = -1
quality_quantity = 0
serfdom_freesubjects = -4
elector = yes
technology_group = western
primary_culture = east_german
religion = catholic
historical_friend = PRU
historical_friend = TEU
capital = 50	# Berlin

 

1351.1.1 = {
	monarch = {
		name = "Otto V"
		dynasty = "von Wittelsbach"
		adm = 6
		dip = 5
		mil = 6
	}
}

1373.1.1 = {
	monarch = {
		name = "Karel I"
		dynasty = "von Luxemburg"
		adm = 9
		dip = 9
		mil = 8
	}
}

1378.11.29 = {
	monarch = {
		name = "Siegmund I"
		dynasty = "von Luxemburg"
		adm = 6
		dip = 6
		mil = 5
	}
}

1388.1.1 = {
	monarch = {
		name = "Jobst I"
		dynasty = "von Luxemburg"
		adm = 5
		dip = 5
		mil = 5
	}
}

1411.1.18 = {
	monarch = {
		name = "Siegmund I"
		dynasty = "von Luxemburg"
		adm = 6
		dip = 6
		mil = 5
	}
}

1415.1.1 = {
	monarch = {
		name = "Friedrich I"
		dynasty = "von Hohenzollern"
		adm = 6
		dip = 6
		mil = 5
	}
}

1415.1.1 = {
	heir = {
		name = "Friedrich"
		monarch_name = "Friedrich II"
		dynasty = "von Hohenzollern"
		birth_date = 1413.11.19
		death_date = 1471.2.10
		claim = 95
		adm = 6
		dip = 6
		mil = 5
	}
}
	

1440.9.21 = {
	monarch = {
		name = "Friedrich II"
		dynasty = "von Hohenzollern"
		adm = 6
		dip = 6
		mil = 5
	}
}

1440.9.21 = {
	heir = {
		name = "Albrecht Achilles"
		monarch_name = "Albrecth III Achilles"
		dynasty = "von Hohenzollern"
		birth_date = 1414.11.9
		death_date = 1486.3.11
		claim = 95
		adm = 5
		dip = 5
		mil = 4
	}
}


1470.2.10 = {
	monarch = {
		name = "Albrecht III Achilles"
		dynasty = "von Hohenzollern"
		adm = 5
		dip = 5
		mil = 4
	}
}

1470.8.2 = {
	heir = {
		name = "Johann Cicero"
		monarch_name = "Johann Cicero I"
		dynasty = "von Hohenzollern"
		birth_date = 1455.8.2
		death_date = 1499.1.9
		claim = 95
		adm = 5
		dip = 8
		mil = 7
	}
}

1486.3.12 = {
	monarch = {
		name = "Johann Cicero I"
		dynasty = "von Hohenzollern"
		adm = 5
		dip = 8
		mil = 7
	}
}

1486.3.12 = {
	heir = {
		name = "Joachim Nestor"
		monarch_name = "Joachim I Nestor"
		dynasty = "von Hohenzollern"
		birth_date = 1484.2.21
		death_date = 1535.6.11
		claim = 95
		adm = 6
		dip = 8
		mil = 5
	}
}

1499.1.1 = { leader = {	name = "Casimir"	type = general	rank = 0	fire = 3	shock = 2	manuever = 2	siege = 1	death_date = 1527.9.21 } }

1499.1.10 = {
	monarch = {
		name = "Joachim I Nestor"
		dynasty = "von Hohenzollern"
		adm = 6
		dip = 8
		mil = 5
	}
}

1505.1.13 = {
	heir = {
		name = "Joachim Hektor"
		monarch_name = "Joachim II Hektor"
		dynasty = "von Hohenzollern"
		birth_date = 1505.1.13
		death_date = 1571.1.3
		claim = 95
		adm = 6
		dip = 5
		mil = 5
	}
}

1527.1.1 = { leader = {	name = "Georg von Frundsberg"		type = general	rank = 0	fire = 2	shock = 2	manuever = 3	siege = 0	death_date = 1528.8.20 } }

1535.7.12 = {
	monarch = {
		name = "Joachim II Hektor"
		dynasty = "von Hohenzollern"
		adm = 6
		dip = 5
		mil = 5
	}
}

1537.7.12 = {
	heir = {
		name = "Johann Georg"
		monarch_name = "Johann Georg I"
		dynasty = "von Hohenzollern"
		birth_date = 1525.9.11
		death_date = 1598.1.8
		claim = 95
		adm = 5
		dip = 9
		mil = 8
		leader = { name = "Johann Georg I"	type = general	rank = 0	fire = 2	shock = 3	manuever = 3	siege = 1}

	}
}

1539.1.1 = { religion = protestant }

1543.1.1 = { leader = {	name = "Albrecht Alcibiades"	type = general	rank = 1	fire = 2	shock = 4	manuever = 3	siege = 1	death_date = 1557.1.8 } }

1571.1.3 = {
	monarch = {
		name = "Johann Georg I"
		dynasty = "von Hohenzollern"
		adm = 5
		dip = 9
		mil = 6
		leader = { name = "Johann Georg I"	type = general	rank = 0	fire = 2	shock = 3	manuever = 3	siege = 1}
	}
}

1571.1.3 = {
	heir = {
		name = "Joachim Friedrich"
		monarch_name = "Joachim Friedrich I"
		dynasty = "von Hohenzollern"
		birth_date = 1546.1.27
		death_date = 1608.7.18
		claim = 95
		adm = 8
		dip = 4
		mil = 6
	}
}

1598.1.9 = {
	monarch = {
		name = "Joachim Friedrich I"
		dynasty = "von Hohenzollern"
		adm = 8
		dip = 4
		mil = 6
	}
}

1598.1.9 = {
	heir = {
		name = "Johann Sigismund"
		monarch_name = "Johann I Sigismund"
		dynasty = "von Hohenzollern"
		birth_date = 1572.11.8
		death_date = 1619.12.23
		claim = 95
		adm = 6
		dip = 5
		mil = 5
	}
}
	

1608.7.19 = {
	monarch = {
		name = "Johann Sigismund I"
		dynasty = "von Hohenzollern"
		adm = 6
		dip = 5
		mil = 5
	}
}

1608.7.19 = {
	heir = {
		name = "Georg Wilhelm"
		monarch_name = "Georg Wilhelm I"
		dynasty = "von Hohenzollern"
		birth_date = 1595.11.13
		death_date = 1640.12.1
		claim = 95
		adm = 8
		dip = 8
		mil = 6
		leader = { name = "Georg Wilhelm I"	type = general	rank = 0	fire = 3	shock = 3	manuever = 3	siege = 0}
	}
}

1619.12.24 = {
	monarch = {
		name = "Georg Wilhelm I"
		dynasty = "von Hohenzollern"
		adm = 8
		dip = 8
		mil = 6
		leader = { name = "Georg Wilhelm I"	type = general	rank = 0	fire = 3	shock = 3	manuever = 3	siege = 0}
	}
}

1620.2.16 = {
	heir = {
		name = "Friedrich Wilhelm"
		monarch_name = "Friedrich I Wilhelm"
		dynasty = "von Hohenzollern"
		birth_date = 1620.2.16
		death_date = 1688.4.29
		claim = 95
		adm = 7
		dip = 8
		mil = 8
		leader = { name = "Friedrich Wilhelm I"	type = general	rank = 0	fire = 3	shock = 3	manuever = 2	siege = 0}

	}
}
		

1640.1.1 = { land_naval = -4 offensive_defensive = -2 quality_quantity = -3 } # Reform of the Brandenburg Army

1640.12.2 = {
	monarch = {
		name = "Friedrich Wilhelm I"
		dynasty = "von Hohenzollern"
		adm = 7
		dip = 8
		mil = 8
		leader = { name = "Friedrich Wilhelm I"	type = general	rank = 0	fire = 3	shock = 3	manuever = 2	siege = 0}
	}
}

1645.1.1 = { leader = {	name = "Otto von Sparr"		type = general	rank = 2	fire = 3	shock = 2	manuever = 2	siege = 2	death_date = 1668.5.9 } }

1654.1.1 = { leader = {	name = "Georg von Derfflinger"	type = general	rank = 1	fire = 4	shock = 3	manuever = 3	siege = 2	death_date = 1695.2.14 } }

1657.7.11 = {
	heir = {
		name = "Friedrich"
		monarch_name = "Friedrich III"
		dynasty = "von Hohenzollern"
		birth_date = 1657.7.11
		death_date = 1713.2.25
		claim = 95
		adm = 9
		dip = 7
		mil = 7
		leader = { name = "Friedrich III"	type = general	rank = 0	fire = 3	shock = 3	manuever = 3	siege = 0}

	}
}

1670.1.1 = { leader = {	name = "F. von Hessen-Homburg"	type = general	rank = 3	fire = 2	shock = 4	manuever = 3	siege = 2	death_date = 1708.1.24 } }

1688.5.10 = {
	monarch = {
		name = "Friedrich III"
		dynasty = "von Hohenzollern"
		adm = 9
		dip = 7
		mil = 7
		leader = { name = "Friedrich III"	type = general	rank = 0	fire = 3	shock = 3	manuever = 3	siege = 0}
	}
}

1688.5.10 = { government = absolute_monarchy }

1690.1.1 = { leader = {	name = "von Dessau"            	type = general	rank = 0	fire = 4	shock = 4	manuever = 4	siege = 3	death_date = 1747.4.7 } }

1701.1.18 = { elector = no }
