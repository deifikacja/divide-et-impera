government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -3
imperialism_isolationism = 0
secularism_theocracy = 1
offensive_defensive = 2
land_naval = -1
quality_quantity = 1
serfdom_freesubjects = -5
technology_group = eastern
primary_culture = bulgarian
religion = greek_orthodox
capital = 2177	# Vidin

 

1356.1.1 = {
	monarch = {
		name = "Ivan Stratsimir"
		dynasty = Shishman
		DIP = 5
		ADM = 3
		MIL = 4
	}
}

1356.1.1 = {
	heir = {
		name = "Konstantin"
		monarch_name = "Konstantin II"
		dynasty = Shishman
		birth_date = 1356.1.1
		death_date = 1400.8.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1397.1.1 = {
	monarch = {
		name = "Konstantin II"
		dynasty = Shishman
		DIP = 3
		ADM = 3
		MIL = 4
	}
}