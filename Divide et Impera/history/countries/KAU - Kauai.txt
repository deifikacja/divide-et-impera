government = tribal_federation
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = 1
imperialism_isolationism = 1
secularism_theocracy = 2
offensive_defensive = 0
land_naval = 5
quality_quantity = 0
serfdom_freesubjects = 0
primary_culture = hawaian
religion = animism
technology_group = pacific
capital = 2054	# Kauai

#Alii Aimoku of Kauai
1340.1.1 = {
	monarch = {
		name = "Manokalanipo"
		dynasty = Mulielealii
		DIP = 6
		ADM = 5
		MIL = 2
	}
}

1370.3.1 = {
	monarch = {
		name = "Kamakamano"
		dynasty = Mulielealii
		DIP = 3
		ADM = 5
		MIL = 1
	}
}

1400.1.1 = {
	monarch = {
		name = "Kahakuakane"
		dynasty = Mulielealii
		DIP = 4
		ADM = 4
		MIL = 4
	}
}

1430.1.1 = {
	monarch = {
		name = "Kuwalupaukamoku"
		dynasty = Mulielealii
		DIP = 2
		ADM = 3
		MIL = 2
	}
}

1465.1.1 = {
	monarch = {
		name = "Kahakumakapaweo"
		dynasty = Mulielealii
		DIP = 6
		ADM = 6
		MIL = 6
	}
}

1490.1.1 = {
	monarch = {
		name = "Kalanikukuma"
		dynasty = Mulielealii
		DIP = 6
		ADM = 6
		MIL = 6
	}
}

1520.1.1 = {
	monarch = {
		name = "Kahakumakalina"
		dynasty = Mulielealii
		DIP = 3
		ADM = 3
		MIL = 2
	}
}

1550.1.1 = {
	monarch = {
		name = "Kamakapu"
		dynasty = Mulielealii
		DIP = 3
		ADM = 3
		MIL = 2
	}
}

1580.1.1 = {
	monarch = {
		name = "Kawelomahamahaia"
		dynasty = Mulielealii
		DIP = 3
		ADM = 3
		MIL = 2
	}
}
1610.1.1 = {
	monarch = {
		name = "Kawelomakualua"
		dynasty = Mulielealii
		DIP = 9
		ADM = 5
		MIL = 9
	}
}
1640.1.1 = {
	monarch = {
		name = "Kaweloiankanaka"
		dynasty = Mulielealii
		DIP = 2
		ADM = 2
		MIL = 2
	}
}

1670.1.1 = {
	monarch = {
		name = "Kawelo'a'maihunali'i"
		dynasty = Mulielealii
		DIP = 5
		ADM = 6
		MIL = 5
	}
}
1700.1.1 = {
	monarch = {
		name = "Kualii"
		dynasty = Mulielealii
		DIP = 4
		ADM = 4
		MIL = 3
	}
}
1730.1.1 = {
	monarch = {
		name = "Peleioholani"
		dynasty = Mulielealii
		DIP = 4
		ADM = 4
		MIL = 2
	}
}
1770.1.1 = {
	monarch = {
		name = "Kamakahelei"
		dynasty = Mulielealii
		DIP = 2
		ADM = 2
		MIL = 2
	}
}
1794.1.1 = {
	monarch = {
		name = "Kaumuali'i"
		dynasty = Mulielealii
		DIP = 2
		ADM = 2
		MIL = 2
	}
}
#To Hawaii 1824