government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -1
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = -1
land_naval = -5
quality_quantity = 2
serfdom_freesubjects = -5
technology_group = sahel
religion = sunni
primary_culture = kanuri
capital = 1156	# Damagaram


#Sultans
1731.1.1 = {
	monarch = {
		name = "Malam Yunus dan Ibram"
		dynasty = Mallamid
		DIP = 6
		ADM = 6
		MIL = 6
	}
}
1746.1.1 = {
	monarch = {
		name = "Baba dan Malam"
		dynasty = Mallamid
		DIP = 5
		ADM = 5
		MIL = 5
	}
}
1757.1.1 = {
	monarch = {
		name = "Tanimun I Babami"
		dynasty = Mallamid
		DIP = 5
		ADM = 5
		MIL = 5
	}
}
1775.1.1 = {
	monarch = {
		name = "Asafa dan Tanimun"
		dynasty = Mallamid
		DIP = 5
		ADM = 5
		MIL = 5
	}
}
1782.1.1 = {
	monarch = {
		name = "Abaza dan Tanimun"
		dynasty = Mallamid
		DIP = 5
		ADM = 5
		MIL = 5
	}
}
1787.1.1 = {
	monarch = {
		name = "Mahaman Babu Tsaba"
		dynasty = Mallamid
		DIP = 5
		ADM = 5
		MIL = 5
	}
}
1790.1.1 = {
	monarch = {
		name = "zda'udu dan Tanimun"
		dynasty = Mallamid
		DIP = 5
		ADM = 5
		MIL = 5
	}
}
1799.1.1 = {
	monarch = {
		name = "Amadu I dan Tanimun"
		dynasty = Mallamid
		DIP = 5
		ADM = 5
		MIL = 5
	}
}
1812.1.1 = {
	monarch = {
		name = "Saleman I dan Tintuma"
		dynasty = Mallamid
		DIP = 7
		ADM = 7
		MIL = 8
	}
	quality_quantity = 0
	centralization_decentralization = 3
}
1822.3.29 = {
	monarch = {
		name = "Ibram I dan Saleman"
		dynasty = Mallamid
		DIP = 6
		ADM = 6
		MIL = 5
	}
	quality_quantity = 1
	centralization_decentralization = 2
	
}