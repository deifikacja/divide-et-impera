government = despotic_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 0
innovative_narrowminded = 1
mercantilism_freetrade = -3
secularism_theocracy = 2
imperialism_isolationism = 2
offensive_defensive = 0
land_naval = -1
quality_quantity = 1
serfdom_freesubjects = -1
primary_culture = pontic
religion = greek_orthodox
technology_group = eastern
capital = 285	# Kaffa

1355.1.1 = {
	monarch = {
		name = "Gabras"
		dynasty = Gabras
		adm = 4
		dip = 5
		mil = 4
	}
}

1362.1.1 = {
	monarch = {
		name = "Chuitani-Dmitri"
		dynasty = Gabras
		adm = 5
		dip = 5
		mil = 3
	}
}


1374.1.1 = {
	monarch = {
		name = "Basil Chowra"
		dynasty = Gabras
		adm = 5
		dip = 5
		mil = 3
	}
}

1380.5.6 = {
	monarch = {
		name = "Stepan Surogski Chowra"
		dynasty = Gabras
		adm = 5
		dip = 5
		mil = 3
	}
}


1403.2.12 = {
	monarch = {
		name = "Alexei"
		dynasty = Gabras
		adm = 6
		dip = 6
		mil = 6
	}
}

1447.11.29 = {
	monarch = {
		name = "Olobei"
		dynasty = Gabras
		adm = 3
		dip = 4
		mil = 4
	}
}

1458.8.23 = {
	monarch = {
		name = "Isaac"
		dynasty = Gabras
		adm = 4
		dip = 6
		mil = 4
	}
}

1474.3.14 = {
	monarch = {
		name = "Alexander"
		dynasty = Gabras
		adm = 5
		dip = 4
		mil = 4
	}
}

# 1475 annexed by Ottomans