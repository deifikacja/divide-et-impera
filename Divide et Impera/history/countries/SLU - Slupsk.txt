government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -3
imperialism_isolationism = 1
secularism_theocracy  = 0
offensive_defensive = 1
land_naval = -1
quality_quantity = 0
serfdom_freesubjects = -4
primary_culture = pommeranian
religion = catholic
technology_group = western
capital = 2156	# Slupsk

 

1368.8.14 = {
	monarch = {
		name = "Boguslaw V"
		dynasty = Gryf
		adm = 6
		dip = 4
		mil = 3
	}
}

1374.1.1 = {
	monarch = {
		name = "Kazimierz IV"
		dynasty = Gryf
		adm = 6
		dip = 4
		mil = 3
	}
}

1377.1.1 = {
	monarch = {
		name = "Warcis�aw VII"
		dynasty = Gryf
		adm = 6
		dip = 6
		mil = 4
	}
}

1395.1.1 = {
	monarch = {
		name = "Boguslaw VIII"
		dynasty = Gryf
		adm = 3
		dip = 5
		mil = 4
	}
}

1402.1.1 = {
	monarch = {
		name = "Barnim V"
		dynasty = Gryf
		adm = 5
		dip = 7
		mil = 5
	}
}

1403.1.1 = {
	monarch = {
		name = "Boguslaw VIII"
		dynasty = Gryf
		adm = 5
		dip = 4
		mil = 6
	}
}

1418.1.1 = {
	monarch = {
		name = "Boguslaw II"
		dynasty = Gryf
		adm = 4
		dip = 5
		mil = 6
	}
}

1446.1.1 = {
	monarch = {
		name = "Eryk I"
		dynasty = Gryf
		adm = 7
		dip = 6
		mil = 5
	}
}
