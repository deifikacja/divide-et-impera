government = tribal_federation
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = 2
imperialism_isolationism = 0
secularism_theocracy = 1
offensive_defensive = -1
land_naval = 3
quality_quantity = -1
serfdom_freesubjects = -1
primary_culture = malayan
religion = hinduism
technology_group = chinese
capital = 1382	# Seremban

1356.1.1 = {
	monarch = {
		name = "Chief"
		dynasty = Minangkabau
		adm = 5
		dip = 6
		mil = 4
	}
}
1416.1.1 = { religion = sunni }

1773.1.1 = {
	government = eastern_despotism #Tribes united
	centralization_decentralization = 4
	monarch = {
		name = "Melawar"
		dynasty = "Sri Menanti"
		adm = 8
		dip = 7
		mil = 6
	}
}

1795.1.1 = {
	monarch = {
		name = "Hitam"
		dynasty = "Sri Menanti"
		adm = 5
		dip = 4
		mil = 3
	}
}

1808.1.1 = {
	monarch = {
		name = "Lenggang"
		dynasty = "Sri Menanti"
		adm = 4
		dip = 4
		mil = 5
	}
}