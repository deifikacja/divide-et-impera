government = daimyo
aristocracy_plutocracy = -4
centralization_decentralization = 4
innovative_narrowminded = 2
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = -1
offensive_defensive = -2
land_naval = 0
quality_quantity = 4
serfdom_freesubjects = -5
primary_culture = japanese
religion = shinto
technology_group = chinese
capital = 2274	# Samuki
daimyo = yes

1446.1.1 = { 
monarch = { 
name = "" 
dynasty = Kikkawa 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Kikkawa" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1520.1.1 = { 
monarch = { 
name = "Tsuneie" 
dynasty = Kikkawa 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Kikkawa Tsuneie" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 
