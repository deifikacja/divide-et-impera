government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -1
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = 0
land_naval = -3
quality_quantity = 2
serfdom_freesubjects = -5
technology_group = eastern
religion = orthodox
primary_culture = russian
capital = 2164	# Suzdal

 

#Princes
1354.1.1 = {
	monarch = {
		name = "Andrei"
		dynasty = "Rurikovich"
		DIP = 3
		ADM = 4
		MIL = 4
	}
}

1356.1.1 = {
	heir = {
		name = "Dmitri"
		monarch_name = "Dmitri Suzdalsky"
		dynasty = "Rurikovich"
		birth_date = 1324.1.1
		death_date = 1383.6.5
		claim = 95
		DIP = 6
		ADM = 5
		MIL = 5
	}
}

1359.11.13 = { capital = 307 }#Vladimir
1362.1.1 = { capital = 2164 }#Suzdal
1365.1.1 = {
	monarch = {
		name = "Dmitri Suzdalsky"
		dynasty = "Rurikovich"
		DIP = 6
		ADM = 5
		MIL = 5
	}
}

1383.1.1 = {
	monarch = {
		name = "Boris"
		dynasty = "Rurikovich"
		DIP = 4
		ADM = 4
		MIL = 5
	}
}

1387.1.1 = {
	monarch = {
		name = "Vasily Kirdyapa"
		dynasty = "Rurikovich"
		DIP = 3
		ADM = 5
		MIL = 5
	}
}

1391.1.1 = {
	monarch = {
		name = "Boris"
		dynasty = "Rurikovich"
		DIP = 4
		ADM = 4
		MIL = 5
	}
}
#Annexed by Muscovy in 1392