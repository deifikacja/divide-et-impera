government = tribal_federation
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = 1
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = 0
land_naval = 5
quality_quantity = 0
serfdom_freesubjects = 0
primary_culture = hawaian
religion = animism
technology_group = pacific
capital = 2057	# Molokai

#Alii Aimoku of Molokai
1350.1.1 = {
	monarch = {
		name = "Kahokuohua"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1600.1.1 = {
	monarch = {
		name = "Kalanipehu"
		DIP = 6
		ADM = 3
		MIL = 3
	}
}

1700.1.1 = {
	monarch = {
		name = "Kane'alai"
		DIP = 3
		ADM = 5
		MIL = 2
		female = yes
	}
}
