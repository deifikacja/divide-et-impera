government = oligarchical_monarchy
aristocracy_plutocracy = -2
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 0
land_naval = -1
quality_quantity = 3
serfdom_freesubjects = -2
technology_group = muslim
religion = shiite
primary_culture = najdi_arabic
capital = 394	# Al Hasa


# Usfurid dynasty
1353.1.1 = {
	monarch = {
		name = "Usufr"
		dynasty = "Usufrid"
		DIP = 4
		MIL = 5
		ADM = 5
	}
}
#Jabrid dynasty
1440.1.1 = {
	monarch = {
		name = "Ajwad ibn Zamil"
		dynasty = "Jabrid"
		DIP = 4
		MIL = 5
		ADM = 5
	}
}
1440.1.1 = {
	heir = {
		name = "Muqrin"
		monarch_name = "Muqrin ibn Zamil"
		dynasty = "Jabrid"
		birth_date = 1440.1.1
		death_date = 1510.1.1
		claim = 95
		adm = 6
		dip = 5
		mil = 4
	}
}
1507.1.1 = {
	monarch = {
		name = "Muqrin ibn Zamil"
		dynasty = "Jabrid"
		DIP = 4
		MIL = 5
		ADM = 5
	}
}
1507.1.1 = {
	heir = {
		name = "Muhammad"
		monarch_name = "Muhammad I"
		dynasty = "Jabrid"
		birth_date = 1507.1.1
		death_date = 1691.1.1
		claim = 95
		adm = 6
		dip = 5
		mil = 4
	}
}
#Usufrid dynasty
1682.1.1 = {
	monarch = {
		name = "Muhammad I"
		dynasty = "Jabrid"
		DIP = 4
		MIL = 5
		ADM = 7
	}
}

1682.1.1 = {
	heir = {
		name = "Sadun"
		monarch_name = "Sadun I"
		dynasty = "Jabrid"
		birth_date = 1660.1.1
		death_date = 1708.1.1
		claim = 95
		adm = 6
		dip = 5
		mil = 4
	}
}

1691.1.1 = {
	monarch = {
		name = "Sadun I"
		dynasty = "Jabrid"
		DIP = 5
		MIL = 4
		ADM = 6
	}
}

1691.1.1 = {
	heir = {
		name = "Barrak"
		monarch_name = "Barrak I"
		dynasty = "Jabrid"
		birth_date = 1670.1.1
		death_date = 1723.1.1
		claim = 95
		adm = 3
		dip = 3
		mil = 3
	}
}

1708.1.1 = {
	 monarch = {
		name = "Barrak I"
		dynasty = "Jabrid"
		DIP = 3
		MIL = 3
		ADM = 3
	}
}

1708.1.1 = {
	heir = {
		name = "Ali"
		monarch_name = "Ali"
		dynasty = "Jabrid"
		birth_date = 1700.1.1
		death_date = 1736.1.1
		claim = 95
		adm = 6
		dip = 6
		mil = 5
	}
}

1723.1.1 = {
	 monarch = {
		name = "Ali"
		dynasty = "Jabrid"
		DIP = 6
		MIL = 5
		ADM = 6
	}
}

1723.1.1 = {
	heir = {
		name = "Sulyaman"
		monarch_name = "Sulyaman"
		dynasty = "Jabrid"
		birth_date = 1700.1.1
		death_date = 1752.1.1
		claim = 95
		adm = 8
		dip = 5
		mil = 5
	}
}

1736.1.1 = {
	 monarch = {
		name = "Sulyaman"
		dynasty = "Jabrid"
		DIP = 5
		MIL = 5
		ADM = 8
	}
}

1736.1.1 = {
	heir = {
		name = "Urayir ibn Dudjayn"
		monarch_name = "Urayir ibn Dudjayn"
		dynasty = "Jabrid"
		birth_date = 1730.1.1
		death_date = 1774.1.1
		claim = 95
		adm = 6
		dip = 7
		mil = 4
	}
}

1752.1.1 = {
	 monarch = {
		name = "Urayir ibn Dudjayn"
		dynasty = "Jabrid"
		DIP = 7
		MIL = 4
		ADM = 6
	}
}

1752.1.1 = {
	heir = {
		name = "Sadun"
		monarch_name = "Sadun II"
		dynasty = "Jabrid"
		birth_date = 1740.1.1
		death_date = 1789.1.1
		claim = 95
		adm = 5
		dip = 6
		mil = 5
	}
}

1774.1.1 = {
	 monarch = {
		name = "Sadun II"
		dynasty = "Jabrid"
		DIP = 6
		MIL = 5
		ADM = 5
	}
}

1774.1.1 = {
	heir = {
		name ="Zayid"
		monarch_name = "Zayid"
		dynasty = "Jabrid"
		birth_date = 1770.1.1
		death_date = 1794.1.1
		claim = 95
		adm = 3
		dip = 3
		mil = 3
	}
}

1789.1.1 = {
	 monarch = {
		name = "Zayid"
		dynasty = "Jabrid"
		DIP = 3
		MIL = 3
		ADM = 3
	}
}

1789.1.1 = {
	heir = {
		name = "Barrak"
		monarch_name ="Barrak II"
		dynasty = "Jabrid"
		birth_date = 1780.1.1
		death_date = 1794.1.1
		claim = 95
		adm = 7
		dip = 6
		mil = 3
	}
}

1794.1.1 = {
	 monarch = {
		name = "Barrak II"
		dynasty = "Jabrid"
		DIP = 6
		MIL = 3
		ADM = 7
	}
}

1794.1.1 = {
	heir = {
		name = "Nadjim ibn Dyhaynim"
		monarch_name = "Nadjim ibn Dyhaynim"
		dynasty = "Jabrid"
		birth_date = 1770.1.1
		death_date= 1798.1.1
		claim = 95
		adm = 3
		dip = 5
		mil = 7
	}
}

1795.1.1 = {
	 monarch = {
		name = "Nadjim ibn Duhaynim"
		dynasty = "Jabrid"
		DIP = 5
		MIL = 7
		ADM = 3
	}
}

1795.1.1 = {
	heir = {
		name = "Sulayman ibn Muhammad"
		monarch_name = "Sulayman ibn Muhammad"
		dynasty = "Jabrid"
		birth_date = 1790.1.1
		death_date = 1820.1.1
		claim = 95
		adm = 4
		dip = 3
		mil = 6
	}
}

1798.1.1 = {
	 monarch = {
		name = "Sulayman ibn Muhammad"
		dynasty = "Jabrid"
		DIP = 3
		MIL = 6
		ADM = 4
	}
}
