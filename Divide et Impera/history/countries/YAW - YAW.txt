government = tribal_despotism
aristocracy_plutocracy = -2
centralization_decentralization = 2
innovative_narrowminded = 2
mercantilism_freetrade = -4
offensive_defensive = 1
imperialism_isolationism = 0
secularism_theocracy = 0
land_naval = -4
quality_quantity = 3
serfdom_freesubjects = -2
technology_group = chinese
religion = buddhism
primary_culture = shan
capital = 588

1356.1.1 = {
	monarch = {
		name = "Kambawsarahta Thiri Pawaramahawuntha Thudamaraza"
		dynasty = Chiangmai
		DIP = 6
		MIL = 7
		ADM = 4
	}
}
1762.1.1 = {
	monarch = {
		name = "Sao Yun"
		dynasty = Chiangmai
		DIP = 6
		MIL = 7
		ADM = 4
	}
}
1815.1.1 = {
	monarch = {
		name = "Sao U"
		dynasty = Chiangmai
		DIP = 6
		MIL = 4
		ADM = 4
	}
}