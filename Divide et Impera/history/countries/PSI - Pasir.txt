government = eastern_despotism
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = 1
imperialism_isolationism = 0
secularism_theocracy = -2
offensive_defensive = 0
land_naval = 2
quality_quantity = -1
serfdom_freesubjects = -1
primary_culture = malay-polynesian
religion = sunni
technology_group = chinese
capital = 1399	# Pasir

1516.1.1 = {
	monarch = {
		name = "Puteri di Dalam Petung"
		dynasty = Pasir
		adm = 4
		dip = 4
		mil = 5
		female = yes
	}
}
1567.1.1 = {
	monarch = {
		name = "Mas Pati Indra"
		dynasty = Pasir
		adm = 4
		dip = 4
		mil = 5
	}
}
1607.1.1 = {
	monarch = {
		name = "Aji Mas Anom Indra bin Aji Mas Pati Indra"
		dynasty = Pasir
		adm = 4
		dip = 4
		mil = 5
	}
}

1644.1.1 = {
	monarch = {
		name = "Aji Anom Singa Amulana bin Aji Mas Anom Indra"
		dynasty = Pasir
		adm = 4
		dip = 4
		mil = 5
	}
}

1667.1.1 = {
	monarch = {
		name = "Aji Perdana bin Aji Anom Singa Maulana"
		dynasty = Pasir
		adm = 5
		dip = 3
		mil = 4
	}
}

1680.1.1 = {
	monarch = {
		name = "Aji Duwo bin Aji Mas Anom Singa Maulana"
		dynasty = Pasir
		adm = 7
		dip = 7
		mil = 5
	}
}

1730.1.1 = {
	monarch = {
		name = "Aji Geger bin Aji Anom Singa Maulana"
		dynasty = Pasir
		adm = 7
		dip = 7
		mil = 5
	}
}

1738.1.1 = {
	monarch = {
		name = "Aji Negara bin Sultan Aji Muhammad Alamsyah"
		dynasty = Pasir
		adm = 5
		dip = 7
		mil = 9
	}
}

1768.1.1 = {
	monarch = {
		name = "Aji Dipati bin Panembahan Adam"
		dynasty = Pasir
		adm = 5
		dip = 6
		mil = 5
	}
}

1799.1.1 = {
	monarch = {
		name = "Aji Panji bin Ratu Agung"
		dynasty = Pasir
		adm = 6
		dip = 3
		mil = 5
	}
}

1811.1.1 = {
	monarch = {
		name = "Aji Sembilan bin Aji Muhammad Alamsyah"
		dynasty = Pasir
		adm = 7
		dip = 4
		mil = 5
	}
}

1815.1.1 = {
	monarch = {
		name = "Aji Karang bin Sultan Sulaiman Alamsyah"
		dynasty = Pasir
		adm = 6
		dip = 4
		mil = 6
	}
}