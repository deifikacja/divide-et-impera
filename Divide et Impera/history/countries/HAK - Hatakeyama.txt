government = daimyo
aristocracy_plutocracy = -4
centralization_decentralization = 4
innovative_narrowminded = 2
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = -1
offensive_defensive = 1
land_naval = 0
quality_quantity = 4
serfdom_freesubjects = -5
primary_culture = japanese
religion = shinto
technology_group = chinese
capital = 1361	# Yamato
daimyo = yes

1356.1.1 = { 
monarch = { 
name = "Kunikiyo" 
dynasty = "Hakateyama" 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Hatakeyama Kunikiyo" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 


1356.1.1 = { 
heir = { 
name = "Motokuni" 
monarch_name = "Motokuni" 
dynasty = "Hakateyama" 
birth_date = 1356.1.1 
death_date = 1406.1.5 
claim = 95 
adm = 5 
dip = 8 
mil = 7 
leader = { 
name = "Hatakeyama Motokuni" 
type = general 
rank = 0 
fire = 1 
shock = 3 
manuever = 3 
siege = 1 
} 
} 
} 


1382.1.1 = { 
monarch = { 
name = "Motokuni" 
dynasty = "Hakateyama" 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Hatakeyama Motokuni" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1382.1.1 = {
    heir = {
        name = "Mitsunori"
        monarch_name =  "Mitsunori" 
        dynasty =  "Hakateyama" 
        birth_date = 1375.1.1
        death_date = 1408.1.1
        claim = 80
        adm = 5
        dip = 8
        mil = 7
        leader = {
            name =  "Mitsunori" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1406.1.1 = { 
monarch = { 
name = "Mitsunori" 
dynasty = "Hakateyama" 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Hatakeyama Mitsunori" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1406.1.1 = {
    heir = {
        name = "Mitsuie"
        monarch_name =  "Mitsuie" 
        dynasty =  "Hakateyama" 
        birth_date = 1397.1.1
        death_date = 1433.1.1
        claim = 80
        adm = 5
        dip = 8
        mil = 7
        leader = {
            name =  "Mitsuie" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1408.1.1 = { 
monarch = { 
name = "Mitsuie" 
dynasty = "Hakateyama" 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Hatakeyama Mitsuie" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1408.1.1 = {
    heir = {
        name = "Mochikuni"
        monarch_name =  "Mochikuni" 
        dynasty =  "Hakateyama" 
        birth_date = 1404.1.1
        death_date = 1441.1.1
        claim = 71
        adm = 5
        dip = 8
        mil = 7
        leader = {
            name =  "Mochikuni" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1433.1.1 = { 
monarch = { 
name = "Mochikuni" 
dynasty = "Hakateyama" 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Hatakeyama Mochikuni" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1433.1.1 = {
    heir = {
        name = "Mochinaga"
        monarch_name =  "Mochinaga" 
        dynasty =  "Hakateyama" 
        birth_date = 1422.1.1
        death_date = 1441.5.1
        claim = 88
        adm = 5
        dip = 8
        mil = 7
        leader = {
            name =  "Mochinaga" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1441.1.1 = { 
monarch = { 
name = "Mochinaga" 
dynasty = "Hakateyama" 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Hatakeyama Mochinaga" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 


1441.5.1 = { 
monarch = { 
name = "Mochikuni" 
dynasty = "Hakateyama" 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Hatakeyama Mochikuni" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1441.5.1 = {
    heir = {
        name = "Yoshinari"
        monarch_name =  "Yoshinari" 
        dynasty =  "Hakateyama" 
        birth_date = 1441.1.1
        death_date = 1460.1.1
        claim = 73
        adm = 5
        dip = 8
        mil = 7
        leader = {
            name =  "Yoshinari" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}
1450.1.1 = { set_country_flag = civil_war }#Split in the clan


1455.1.1 = { 
monarch = { 
name = "Yoshinari" 
dynasty = "Hakateyama" 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Hatakeyama Yoshinari" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1455.1.1 = {
    heir = {
        name = "Masanaga"
        monarch_name =  "Masanaga" 
        dynasty =  "Hakateyama" 
        birth_date = 1445.1.1
        death_date = 1467.1.1
        claim = 73
        adm = 5
        dip = 8
        mil = 7
        leader = {
            name =  "Masanaga" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}

1460.1.1 = { 
monarch = { 
name = "Masanaga" 
dynasty = "Hakateyama" 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Hatakeyama Masanaga" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1467.1.1 = { 
monarch = { 
name = "Yoshinari" 
dynasty = "Hakateyama" 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Hatakeyama Yoshinari" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1467.5.1 = { 
monarch = { 
name = "Masanaga" 
dynasty = "Hakateyama" 
adm = 4 
dip = 4 
mil = 6 
leader = { name = "Hakateyama Masanagi" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1467.5.1 = {
    heir = {
        name = "Yoshitoyo"
        monarch_name =  "Yoshitoyo" 
        dynasty =  "Hakateyama" 
        birth_date = 1458.1.1
        death_date = 1499.1.1
        claim = 78
        adm = 3
        dip = 3
        mil = 3
        leader = {
            name =  "Yoshitoyo" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1493.1.1 = { 
monarch = { 
name = "Yoshitoyo" 
dynasty = "Hakateyama" 
adm = 3 
dip = 3 
mil = 3 
leader = { name = "Hatakeyama Yoshitoyo" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1493.1.1 = {
    heir = {
        name = "Yoshihide"
        monarch_name =  "Yoshihide" 
        dynasty =  "Hakateyama" 
        birth_date = 1478.1.1
        death_date = 1504.1.1
        claim = 88
        adm = 3
        dip = 3
        mil = 3
        leader = {
            name =  "Yoshihide" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1499.1.1 = { 
monarch = { 
name = "Yoshihide" 
dynasty = "Hakateyama" 
adm = 3 
dip = 3 
mil = 3 
leader = { name = "Hatakeyama Yoshihide" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1499.1.1 = {
    heir = {
        name = "Hisayoshi"
        monarch_name =  "Hisayoshi" 
        dynasty =  "Hakateyama" 
        birth_date = 1487.1.1
        death_date = 1517.1.1
        claim = 80
        adm = 3
        dip = 3
        mil = 3
        leader = {
            name =  "Hisayoshi" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1504.1.1 = { 
monarch = { 
name = "Hisayoshi" 
dynasty = "Hakateyama" 
adm = 3 
dip = 3 
mil = 3 
leader = { name = "Hatakeyama Hisayoshi" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1504.1.1 = {
    heir = {
        name = "Tanenaga"
        monarch_name =  "Tanenaga" 
        dynasty =  "Hakateyama" 
        birth_date = 1493.1.1
        death_date = 1534.1.1
        claim = 74
        adm = 5
        dip = 7
        mil = 5
        leader = {
            name =  "Tanenaga" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1517.1.1 = { 
monarch = { 
name = "Tanenaga" 
dynasty = "Hakateyama" 
adm = 5 
dip = 7 
mil = 5 
leader = { name = "Hakateyama Tanenaga" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1517.1.1 = {
    heir = {
        name = "Nagatsune"
        monarch_name =  "Nagatsune" 
        dynasty =  "Hakateyama" 
        birth_date = 1504.1.1
        death_date = 1538.1.1
        claim = 89
        adm = 5
        dip = 7
        mil = 5
        leader = {
            name =  "Nagatsune" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1534.1.1 = { 
monarch = { 
name = "Nagatsune" 
dynasty = "Hakateyama" 
adm = 5 
dip = 7 
mil = 5 
leader = { name = "Hakateyama Nagatsune" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1534.1.1 = {
    heir = {
        name = "Arjuji"
        monarch_name =  "Arjuji" 
        dynasty =  "Hakateyama" 
        birth_date = 1531.1.1
        death_date = 1542.1.1
        claim = 85
        adm = 5
        dip = 7
        mil = 5
        leader = {
            name =  "Arjuji" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1538.1.1 = { 
monarch = { 
name = "Arjuji" 
dynasty = "Hakateyama" 
adm = 5 
dip = 7 
mil = 5 
leader = { name = "Hakateyama Ariuji" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 


1542.1.1 = { 
monarch = { 
name = "Tanenaga" 
dynasty = "Hakateyama" 
adm = 5 
dip = 7 
mil = 5 
leader = { name = "Hakateyama Tanenaga" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1542.1.1 = {
    heir = {
        name = "Haruhiro"
        monarch_name =  "Haruhiro" 
        dynasty =  "Hakateyama" 
        birth_date = 1530.1.1
        death_date = 1545.5.1
        claim = 88
        adm = 5
        dip = 7
        mil = 5
        leader = {
            name =  "Haruhiro" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1545.1.1 = { 
monarch = { 
name = "Haruhiro" 
dynasty = "Hakateyama" 
adm = 5 
dip = 7 
mil = 5 
leader = { name = "Hakateyama Haruhiro" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1545.1.1 = {
    heir = {
        name = "Masakuni"
        monarch_name =  "Masakuni" 
        dynasty =  "Hakateyama" 
        birth_date = 1534.1.1
        death_date = 1550.1.1
        claim = 91
        adm = 5
        dip = 7
        mil = 5
        leader = {
            name =  "Masakuni" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1545.5.1 = { 
monarch = { 
name = "Masakuni" 
dynasty = "Hakateyama" 
adm = 5 
dip = 7 
mil = 5 
leader = { name = "Hakateyama Masakuni" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1545.5.1 = {
    heir = {
        name = "Takamasa"
        monarch_name =  "Takamasa" 
        dynasty =  "Hakateyama" 
        birth_date = 1541.1.1
        death_date = 1569.1.1
        claim = 72
        adm = 5
        dip = 7
        mil = 5
        leader = {
            name =  "Takamasa" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1550.1.1 = { 
monarch = { 
name = "Takamasa" 
dynasty = "Hakateyama" 
adm = 5 
dip = 7 
mil = 5 
leader = { name = "Hakateyama Takamasa" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1550.1.1 = {
    heir = {
        name = "Akitaka"
        monarch_name =  "Akitaka" 
        dynasty =  "Hakateyama" 
        birth_date = 1543.1.1
        death_date = 1589.1.1
        claim = 87
        adm = 5
        dip = 7
        mil = 5
        leader = {
            name =  "Akitaka" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1569.1.1 = { 
monarch = { 
name = "Akitaka" 
dynasty = "Hakateyama" 
adm = 5 
dip = 7 
mil = 5 
leader = { name = "Hakateyama Akitaka" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

