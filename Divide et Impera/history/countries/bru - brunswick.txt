government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -3
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 2
land_naval = -1
quality_quantity = 0
serfdom_freesubjects = -4
technology_group = western
primary_culture = hannoverian
religion = catholic
capital = 53	# L�neburg

 

#Dukes of Braunschweig-Wolfenb�ttel
1345.1.1 = {
	monarch = {
		name = "Magnus I"
		dynasty = "von Welf"
		DIP = 4
		ADM = 5
		MIL = 3
	}
}

1345.1.1 = {
	heir = {
		name = "Magnus"
		monarch_name = "Magnus II"
		dynasty = "von Welf"
		birth_date = 1323.1.1
		death_date = 1373.1.1
		claim = 95
		adm = 4
		dip = 4
		mil = 4
	}
}

1369.7.19 = {
	monarch = {
		name = "Magnus II"
		dynasty = "von Welf"
		DIP = 4
		ADM = 7
		MIL = 6
	}
}

1369.1.1 = {
	heir = {
		name = "Heinrich"
		monarch_name = "Heinrich der Milde"
		dynasty = "von Welf"
		birth_date = 1369.1.1
		death_date = 1416.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1373.6.2 = {
	monarch = {
		name = "Heinrich der Milde"
		dynasty = "von Welf"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1411.1.1 = {
	heir = {
		name = "Heinrich"
		monarch_name = "Heinrich IV"
		dynasty = "von Welf"
		birth_date = 1411.1.1
		death_date = 1473.12.17
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1416.1.1 = {
	monarch = {
		name = "Heinrich IV"
		dynasty = "von Welf"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1416.1.1 = {
	heir = {
		name = "Wilhelm"
		monarch_name = "Wilhelm I"
		dynasty = "von Welf"
		birth_date = 1392.1.1
		death_date = 1482.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

#Dukes of Braunschweig-Wolfenb�ttel

1473.12.7 = {
	monarch = {
		name = "Wilhelm I"
		dynasty = "von Welf"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}
# remark="Also Wilhelm I of L�neburg"

1473.12.7 = {
	heir = {
		name = "Wilhelm"
		monarch_name = "Wilhelm II"
		dynasty = "von Welf"
		birth_date = 1425.1.1
		death_date = 1503.7.7
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1482.7.25 = {
	monarch = {
		name = "Wilhelm II"
		dynasty = "von Welf"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1482.7.25 = {
	heir = {
		name = "Heinrich"
		monarch_name = "Heinrich der �ltere"
		dynasty = "von Welf"
		birth_date = 1463.6.24
		death_date = 1514.6.23
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1503.10.7 = {
	monarch = {
		name = "Heinrich der �ltere"
		dynasty = "von Welf"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1503.10.7 = {
	heir = {
		name = "Heinrich"
		monarch_name = "Heinrich der J�ngere"
		dynasty = "von Welf"
		birth_date = 1489.11.10
		death_date = 1568.6.11
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1514.6.23 = {
	monarch = {
		name = "Heinrich der J�ngere"
		dynasty = "von Welf"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1528.6.29 = {
	heir = {
		name = "Julius"
		monarch_name = "Julius I"
		dynasty = "von Welf"
		birth_date = 1528.6.29
		death_date = 1589.5.3
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1529.1.1 = { religion = protestant }

1568.6.11 = {
	monarch = {
		name = "Julius I"
		dynasty = "von Welf"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1568.6.11 = {
	heir = {
		name = "Heinrich Julius"
		monarch_name = "Heinrich Julius I"
		dynasty = "von Welf"
		birth_date = 1564.10.15
		death_date = 1613.7.20
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1589.5.13 = {
	monarch = {
		name = "Heinrich Julius I"
		dynasty = "von Welf"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1591.4.15 = {
	heir = {
		name = "Friedrich Ulrich"
		monarch_name = "Friedrich Ulrich I"
		dynasty = "von Welf"
		birth_date = 1591.4.15
		death_date = 1634.8.21
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1613.7.20 = {
	monarch = {
		name = "Friedrich Ulrich I"
		dynasty = "von Welf"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

#On Frederick Ulrich's death, his territories passed to a line of #distant cousins

1613.7.20 = {
	heir = {
		name = "August"
		monarch_name = "August II"
		dynasty = "von Welf"
		birth_date = 1579.4.10
		death_date = 1666.9.17
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1635.1.1 = {
	monarch = {
		name = "August II"
		dynasty = "von Welf"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1635.1.1 = {
	heir = {
		name = "Rudolf August"
		monarch_name = "Rudolf August I"
		dynasty = "von Welf"
		birth_date = 1627.5.16
		death_date = 1704.1.26
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1666.9.17 = {
	monarch = {
		name = "Rudolf August I"
		dynasty = "von Welf"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1666.9.17 = {
	heir = {
		name = "Anton Ulrich"
		monarch_name = "Anton Ulrich I"
		dynasty = "von Welf"
		birth_date = 1633.10.4
		death_date = 1714.3.27
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1704.1.26 = {
	monarch = {
		name = "Anton Ulrich I"
		dynasty = "von Welf"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1704.1.26 = {
	heir = {
		name = "August Wilhelm"
		monarch_name = "August Wilhelm I"
		dynasty = "von Welf"
		birth_date = 1662.3.8
		death_date = 1731.3.23
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1714.1.27 = {
	monarch = {
		name = "August Wilhelm I"
		dynasty = "von Welf"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1714.1.27 = {
	heir = {
		name = "Ludwig Rudolf"
		monarch_name = "Ludwig Rudolf I"
		dynasty = "von Welf"
		birth_date = 1671.7.22
		death_date = 1735.3.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1731.3.23 = {
	monarch = {
		name = "Ludwig Rudolf I"
		dynasty = "von Welf"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1731.3.23 = {
	heir = {
		name = "Ferdinand Albrecht"
		monarch_name = "Ferdinand Albrecht I"
		dynasty = "von Welf"
		birth_date = 1680.5.29
		death_date = 1735.9.2
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1735.3.1 = {
	monarch = {
		name = "Ferdinand Albrecht I"
		dynasty = "von Welf"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1735.3.1 = {
	heir = {
		name = "Karl"
		monarch_name = "Karl I"
		dynasty = "von Welf"
		birth_date = 1713.8.1
		death_date = 1780.3.26
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

# Dukes of Braunschweig (after 1753)
1735.9.13 = {
	monarch = {
		name = "Karl I"
		dynasty = "von Welf"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1735.10.9 = {
	heir = {
		name = "Karl Wilhelm Ferdinand"
		monarch_name = "Karl II Wilhelm Ferdinand"
		dynasty = "von Welf"
		birth_date = 1735.10.9
		death_date = 1806.10.16
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1780.3.26 = {
	monarch = {
		name = "Karl II Wilhelm Ferdinand"
		dynasty = "von Welf"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1780.3.26 = {
	heir = {
		name = "Friedrich Wilhelm"
		monarch_name = "Friedrich Wilhelm"
		birth_date = 1771.10.9
		death_date = 1815.6.6
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1806.11.1 = {
	monarch = {
		name = "Frederick William"
		dynasty = "von Welf"
		DIP = 4
		ADM = 5
		MIL = 7
	}
}

1780.3.26 = {
	heir = {
		name = "Friedrich Wilhelm"
		monarch_name = "Friedrich Wilhelm"
		birth_date = 1771.10.9
		death_date = 1815.6.6
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1815.6.16 = {
	monarch = {
		name = "Karl II"
		dynasty = "von Welf"
		DIP = 4
		ADM = 4
		MIL = 4
	}
}