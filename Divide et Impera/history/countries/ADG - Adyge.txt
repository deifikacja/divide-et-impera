government = tribal_federation
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -2
imperialism_isolationism = 0
secularism_theocracy = -2
offensive_defensive = -2
land_naval = -5
quality_quantity = -5 # Legendary soldiers
serfdom_freesubjects = -2
primary_culture = circassian
religion = eastern_churches
technology_group = eastern
capital = 462	# Adyge


1355.1.1 = {
	monarch = {
		name = "Chief"
		dynasty="Kassog"
		adm = 3
		dip = 3
		mil = 6
	}
}
1600.1.1 = { religion = sunni } # Conversion to Islam
1700.1.1 = {
	monarch = {
		name = "Aslan Kaytouko"
		dynasty="Kassog"
		adm = 3
		dip = 3
		mil = 6
	}
}
