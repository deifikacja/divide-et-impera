government = tribal_federation
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 0
land_naval = -5
quality_quantity = 2
serfdom_freesubjects = 0
technology_group = sahel
religion = sunni
primary_culture = berber
capital = 1297	# Agadez

1405.1.1 = {
	monarch = {
		name = "Yunus"
		adm = 6
		dip = 4
		mil = 6
	}
}


1687.1.1 = {
	monarch = {
		name = "Muhammad Agg-Agba"
		adm = 7
		dip = 4
		mil = 6
	}
}

1721.1.1 = {
	monarch = {
		name = "Muhammad al-Amin"
		adm = 3
		dip = 6
		mil = 4
	}
}

1721.1.3 = {
	monarch = {
		name = "al-Wali ibn Muhammad"
		adm = 6
		dip = 6
		mil = 6
	}
}

1721.1.6 = {
	monarch = {
		name = "Muhammad al-Amin"
		adm = 5
		dip = 3
		mil = 5
	}
}

1721.1.9 = {
	monarch = {
		name = "Muhammad al-Mu'min"
		adm = 5
		dip = 4
		mil = 4
	}
}

1721.1.11 = {
	monarch = {
		name = "Uthman ibn Muhammad"
		adm = 5
		dip = 3
		mil = 4
	}
}

1722.1.1 = {
	monarch = {
		name = "Muhammad Agg-`A�isha"
		adm = 3
		dip = 5
		mil = 5
	}
}

1735.1.1 = {
	monarch = {
		name = "Muhammad Humad"
		adm = 3
		dip = 5
		mil = 5
	}
}

1739.1.1 = {
	monarch = {
		name = "Muhammad Guma"
		adm = 3
		dip = 5
		mil = 5
	}
}

1744.1.1 = {
	monarch = {
		name = "Muhammad Humad"
		adm = 3
		dip = 5
		mil = 5
	}
}

1759.1.1 = {
	monarch = {
		name = "Muhammad Guma"
		adm = 3
		dip = 5
		mil = 5
	}
}

1763.1.1 = {
	monarch = {
		name = "Muhammad Humad"
		adm = 3
		dip = 5
		mil = 5
	}
}

1768.1.1 = {
	monarch = {
		name = "Muhammad al-`Adil"
		adm = 3
		dip = 5
		mil = 5
	}
}

1810.1.1 = {
	monarch = {
		name = "Muhammad ad-Dani"
		adm = 3
		dip = 5
		mil = 5
	}
}

1815.1.1 = {
	monarch = {
		name = "Muhammad al-Baqiri"
		adm = 3
		dip = 5
		mil = 5
	}
}

1816.1.1 = {
	monarch = {
		name = "Muhammad Guma Tabdali"
		adm = 3
		dip = 5
		mil = 4
	}
}

1821.1.1 = {
	monarch = {
		name = "Ibrahim Waffa"
		adm = 3
		dip = 5
		mil = 4
	}
}

1828.1.1 = {
	monarch = {
		name = "Muhammad Guma Tabdali"
		adm = 3
		dip = 5
		mil = 4
	}
}