government = caste_monarchy
aristocracy_plutocracy = -2
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 2
land_naval = 2
quality_quantity = 2
serfdom_freesubjects = -1
primary_culture = malay-polynesian
technology_group = chinese
religion = hinduism
capital = 1394	# Berau


1356.1.1 = {
	monarch = {
		name = "Raja"
		dynasty=Berau
		adm = 4
		dip = 4
		mil = 5
	}
}

1480.1.1 = {
	monarch = {
		name = "Surjamula Kusuma"
		dynasty=Berau
		adm = 5
		dip = 3
		mil = 4
	}
}

1500.1.1 = {
	monarch = {
		name = "Nikullan"
		dynasty=Berau
		adm = 5
		dip = 3
		mil = 4
	}
}

1520.1.1 = {
	monarch = {
		name = "Nikutak"
		dynasty=Berau
		adm = 5
		dip = 3
		mil = 4
	}
}

1540.1.1 = {
	monarch = {
		name = "Nigindang"
		dynasty=Berau
		adm = 5
		dip = 3
		mil = 4
	}
}

1570.1.1 = {
	monarch = {
		name = "Panjang Ruma"
		dynasty=Berau
		adm = 5
		dip = 3
		mil = 4
	}
}

1600.1.1 = {
	monarch = {
		name = "Barani"
		dynasty=Berau
		adm = 5
		dip = 3
		mil = 4
	}
}

1630.1.1 = {
	monarch = {
		name = "Suraraja"
		dynasty=Berau
		adm = 5
		dip = 3
		mil = 4
	}
}

1660.1.1 = {
	monarch = {
		name = "Gadong"
		dynasty=Berau
		adm = 5
		dip = 3
		mil = 4
	}
}

1660.5.1 = {
	monarch = {
		name = "Palu"
		dynasty=Berau
		adm = 5
		dip = 3
		mil = 4
	}
}

1660.8.1 = {
	monarch = {
		name = "Kotok"
		dynasty=Berau
		adm = 5
		dip = 3
		mil = 4
	}
}

1450.1.1 = {
	heir = {
		name = "Kurin"
		monarch_name = "Kurin dan Malaka"
		dynasty = "Berau"
		birth_date = 1660.8.1
		death_date = 1700.1.1
		claim = 60
		DIP = 5
		ADM = 3
		MIL = 4
		female=yes
	}
}

1683.1.1 = {
	monarch = {
		name = "Kurin dan Malaka"
		dynasty=Berau
		adm = 5
		dip = 3
		mil = 4
		female=yes
	}
}

1683.1.1 = {
	heir = {
		name = "Bodi Bodi"
		monarch_name = "Bodi Bodi di Patang"
		dynasty = "Berau"
		birth_date = 1683.1.1
		death_date = 1720.1.1
		claim = 60
		DIP = 5
		ADM = 3
		MIL = 4
	}
}

1700.1.1 = {
	monarch = {
		name = "Bodi Bodi di Patang"
		dynasty=Berau
		adm = 5
		dip = 3
		mil = 4
	}
}

1700.1.1 = {
	heir = {
		name = "Lajak"
		monarch_name = "Lajak"
		dynasty = "Berau"
		birth_date = 1700.1.1
		death_date = 1750.1.1
		claim = 90
		DIP = 5
		ADM = 3
		MIL = 4
	}
}

1720.1.1 = {
	monarch = {
		name = "Lajak"
		dynasty=Berau
		adm = 5
		dip = 3
		mil = 4
	}
}

1720.1.1 = {
	heir = {
		name = "Sapar ad-Din"
		monarch_name = "Sapar ad-Din"
		dynasty = "Berau"
		birth_date = 1720.1.1
		death_date = 1760.1.1
		claim = 90
		DIP = 5
		ADM = 3
		MIL = 4
	}
}

1750.1.1 = {
	monarch = {
		name = "Sapar ad-Din"
		dynasty=Berau
		adm = 5
		dip = 3
		mil = 4
	}
	religion = sunni 
	government = eastern_despotism
}

1750.1.1 = {
	heir = {
		name = "Adipati"
		monarch_name = "Adipati"
		dynasty = "Berau"
		birth_date = 1750.1.1
		death_date = 1780.1.1
		claim = 70
		DIP = 5
		ADM = 3
		MIL = 4
	}
}

1760.1.1 = {
	monarch = {
		name = "Adipati"
		dynasty=Berau
		adm = 5
		dip = 3
		mil = 4
	}
}

1760.1.1 = {
	heir = {
		name = "Kuning"
		monarch_name = "Kuning"
		dynasty = "Berau"
		birth_date = 1760.1.1
		death_date = 1790.1.1
		claim = 70
		DIP = 5
		ADM = 3
		MIL = 4
	}
}

1780.1.1 = {
	monarch = {
		name = "Kuning"
		dynasty=Berau
		adm = 5
		dip = 3
		mil = 4
	}
}

1780.1.1 = {
	heir = {
		name = "Hasan ad-Din"
		monarch_name = "Hasan ad-Din I"
		dynasty = "Berau"
		birth_date = 1780.1.1
		death_date = 1800.1.1
		claim = 90
		DIP = 5
		ADM = 3
		MIL = 4
	}
}

1790.1.1 = {
	monarch = {
		name = "Hasan ad-Din I"
		dynasty=Berau
		adm = 5
		dip = 3
		mil = 4
	}
}

1790.1.1 = {
	heir = {
		name = "Zajn al-Abidin"
		monarch_name = "Zajn al-Abidin I"
		dynasty = "Berau"
		birth_date = 1790.1.1
		death_date = 1810.1.1
		claim = 90
		DIP = 5
		ADM = 3
		MIL = 4
	}
}

1800.1.1 = {
	monarch = {
		name = "Zajn al-Abidin I"
		dynasty=Berau
		adm = 5
		dip = 3
		mil = 4
	}
}

1800.1.1 = {
	heir = {
		name = "Badr ad-Din"
		monarch_name = "Badr ad-Din"
		dynasty = "Berau"
		birth_date = 1800.1.1
		death_date = 1815.1.1
		claim = 90
		DIP = 5
		ADM = 3
		MIL = 4
	}
}

1810.1.1 = {
	monarch = {
		name = "Badr ad-Din"
		dynasty=Berau
		adm = 5
		dip = 3
		mil = 4
	}
}

1810.1.1 = {
	heir = {
		name = "Salah ad-Din"
		monarch_name = "Salah ad-Din"
		dynasty = "Berau"
		birth_date = 1810.1.1
		death_date = 1820.1.1
		claim = 90
		DIP = 5
		ADM = 3
		MIL = 4
	}
}

1815.1.1 = {
	monarch = {
		name = "Salah ad-Din"
		dynasty=Berau
		adm = 5
		dip = 3
		mil = 4
	}
}

1815.1.1 = {
	heir = {
		name = "Amir al-Muminin"
		monarch_name = "Amir al-Muminin"
		dynasty = "Berau"
		birth_date = 1815.1.1
		death_date = 1830.1.1
		claim = 90
		DIP = 5
		ADM = 3
		MIL = 4
	}
}