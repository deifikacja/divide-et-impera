government = military_order
aristocracy_plutocracy = -5
centralization_decentralization = 4
innovative_narrowminded = 2
mercantilism_freetrade = -2
imperialism_isolationism = 0
secularism_theocracy = 3
offensive_defensive = 2
land_naval = -1
quality_quantity = -2
serfdom_freesubjects = -3
primary_culture = east_german
add_accepted_culture = estonian
add_accepted_culture = latvian
religion = catholic
historical_friend = TEU
#historical_friend = HSA
technology_group = western
capital = 273	# Wenden


1345.4.11 = {
	monarch = {
		name = "Arnold I von Vietinghoff"
		adm = 3
		dip = 4
		mil = 5
	}
}

1359.9.24 = {
	monarch = {
		name = "Arnold I von Vietinghoff"
		adm = 4
		dip = 3
		mil = 4
	}
}

1364.1.7 = {
	monarch = {
		name = "Wilhelm I von Freimersheim"
		adm = 6
		dip = 5
		mil = 4
	}
}

1384.3.10 = {
	monarch = {
		name = "Robin I von Elz"
		adm = 4
		dip = 4
		mil = 5
	}
}

1389.1.8 = {
	monarch = {
		name = "Wennemar I von Bruggenei"
		adm = 7
		dip = 6
		mil = 5
	}
}

1401.1.8 = {
	monarch = {
		name = "Konrad III von Vietinghoff"
		adm = 5
		dip = 6
		mil = 5
	}
}

1413.1.8 = {
	monarch = {
		name = "Dytryk II Tork"
		adm = 4
		dip = 5
		mil = 5
	}
	imperialism_isolationism = 1
}

1415.1.8 = {
	monarch = {
		name = "Zygfryd I von Lander"
		adm = 3
		dip = 3
		mil = 4
	}
}

1424.1.8 = {
	monarch = {
		name = "Cysse I von Rutenberg"
		adm = 4
		dip = 3
		mil = 4
	}
}

1433.1.8 = {
	monarch = {
		name = "Franke I von Kerskorff"
		adm = 6
		dip = 4
		mil = 5
	}
}

1435.1.8 = {
	monarch = {
		name = "Henryk III von Bockenworden"
		adm = 3
		dip = 3
		mil = 3
	}
}

1439.1.1 = {
	monarch = {
		name = "Heidenreich von Owerberg"
		adm = 5
		dip = 7
		mil = 4
	}
}

1450.11.8 = {
	monarch = {
		name = "Jan I von Mengede"
		adm = 3
		dip = 3
		mil = 3
	}
}

1470.4.5 = {
	monarch = {
		name = "Jan II von Wolthuss Borch"
		adm = 5
		dip = 6
		mil = 5
	}
}

1471.2.19 = {
	monarch = {
		name = "Bernd von Borch"
		adm = 5
		dip = 5
		mil = 6
	}
}

1485.1.5 = {
	monarch = {
		name = "Jan III Freitag von Lorinhoff"
		adm = 4
		dip = 4
		mil = 3
	}
}

1494.1.1 = { 
	monarch = {
		name = "Wolter I von Plettenberg"
		adm = 6
		dip = 7
		mil = 8
		leader = {	name = "Wolter I von Plettenberg"           	type = general	rank = 0	fire = 3	shock = 4	manuever = 3	siege = 1 } 
	}
}

1535.8.26 = {
	monarch = {
		name = "Herman II von Bruggenei"
		adm = 4
		dip = 5
		mil = 5
	}
}

1549.12.14 = {
	monarch = {
		name = "Jan IV von Recke"
		adm = 4
		dip = 6
		mil = 4
	}
}



1550.1.1 = { leader = {	name = "Magnus"                	type = general	rank = 0	fire = 3	shock = 2	manuever = 4	siege = 1}
}

1551.4.9 = {
	monarch = {
		name = "Henryk IV von Galen"
		adm = 5
		dip = 4
		mil = 6
	}
}

1557.2.27 = {
	monarch = {
		name = "Johann Wilhelm von Fürstenberg"
		adm = 5
		dip = 4
		mil = 6
	}
}

1559.2.5 = {
	monarch = {
		name = "Johann von der Recke"
		adm = 5
		dip = 4
		mil = 4
	}
}

1561.5.19 = {
	monarch = {
		name = "Gotthard Kettler"
		adm = 5
		dip = 4
		mil = 5
	}
}