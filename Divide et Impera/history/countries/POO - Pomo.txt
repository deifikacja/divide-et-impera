government = tribal_democracy
aristocracy_plutocracy = 5
centralization_decentralization = 5
innovative_narrowminded = 0
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 5
land_naval = -5
quality_quantity = 0
serfdom_freesubjects = 5
primary_culture = pomoan
religion = animism
technology_group = new_world
capital = 871	# Pomo


1351.1.1 = {
	monarch = {
		name = "Chief of Pomo"
		adm = 4
		dip = 5
		mil = 5
	}
}