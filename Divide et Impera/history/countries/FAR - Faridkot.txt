government = caste_monarchy
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = -1
offensive_defensive = 2
land_naval = -3
quality_quantity = -5
serfdom_freesubjects = -2
primary_culture = panjabi
religion = hinduism
technology_group = indian
capital = 1959	# Faridkot


#Rajas of Faridkot
1600.1.1 = {
	monarch = {
		name = "Ballan"
		dynasty = Jaisal
		adm = 5
		dip = 3
		mil = 3
	}
}
1643.1.1 = {
	monarch = {
		name = "Kapur Singh"
		dynasty = Jaisal
		adm = 3
		dip = 4
		mil = 7
	}
}
1708.1.1 = {
	monarch = {
		name = "Sajja Singh"
		dynasty = Jaisal
		adm = 1
		dip = 1
		mil = 1
	}
}
1710.1.1 = {
	monarch = {
		name = "Sukhia Singh"
		dynasty = Jaisal
		adm = 9
		dip = 7
		mil = 6
	}
}

1731.1.1 = {
	monarch = {
		name = "Hamir Singh"
		dynasty = Jaisal
		adm = 5
		dip = 5
		mil = 5
	}
}

1782.1.1 = {
	monarch = {
		name = "Mohr Singh"
		dynasty = Jaisal
		adm = 5
		dip = 5
		mil = 5
	}
}

1798.1.1 = {
	monarch = {
		name = "Charat Singh"
		dynasty = Jaisal
		adm = 3
		dip = 3
		mil = 6
	}
}

1804.1.1 = {
	monarch = {
		name = "Dal Singh"
		dynasty = Jaisal
		adm = 3
		dip = 4
		mil = 5
	}
}

1804.5.1 = {
	monarch = {
		name = "Ghulab Singh"
		dynasty = Jaisal
		adm = 3
		dip = 3
		mil = 6
	}
}