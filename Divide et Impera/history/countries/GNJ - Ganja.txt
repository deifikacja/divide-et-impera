government = eastern_despotism
aristocracy_plutocracy = -2
centralization_decentralization = 3
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = 0
land_naval = -4
quality_quantity = 3
serfdom_freesubjects = -2
primary_culture = azerbadjani #local branch of Safavids
religion = shiite
technology_group = muslim
capital = 2186	# Ganja

#Ziyad oghlu 
1747.1.1 = {
	monarch = {
		name = "Shah Warrdi"
		dynasty = Ziadhanovlar
		adm = 4
		dip = 7
		mil = 5
	}
}

1761.1.1 = {
	monarch = {
		name = "Muhammad Hasan"
		dynasty = Ziadhanovlar
		adm = 7
		dip = 6
		mil = 5
	}
}

1780.1.1 = {
	monarch = {
		name = "Muhammad"
		dynasty = Ziadhanovlar
		adm = 3
		dip = 4
		mil = 4
	}
}

1784.1.1 = {
	monarch = {
		name = "Hajji Beg"
		dynasty = Ziadhanovlar
		adm = 5
		dip = 3
		mil = 6
	}
}

1785.1.1 = {
	monarch = {
		name = "Rahim Khan"
		dynasty = Ziadhanovlar
		adm = 4
		dip = 3
		mil = 5
	}
}

1786.1.1 = {
	monarch = {
		name = "al-Jawwad"
		dynasty = Ziadhanovlar
		adm = 4
		dip = 4
		mil = 4
	}
}
