government = tribal_federation
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = 1
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = 0
land_naval = 5
quality_quantity = 0
serfdom_freesubjects = 0
primary_culture = tokelau
religion = animism
technology_group = pacific
capital = 2046 # Nukunonu

#Aliki of Nukunonu
1700.1.1 = {
	monarch = {
		name = "Sunga"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}
1750.1.1 = {
	monarch = {
		name = "Kakaia"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1800.1.1 = {
	monarch = {
		name = "Ngala"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}