government = confessional_state
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = 3
imperialism_isolationism = 0
secularism_theocracy = 3
offensive_defensive = 0
land_naval = 2
quality_quantity = 3
serfdom_freesubjects = -3
primary_culture = omani_arabic
religion = ibadi
technology_group = muslim
capital = 400	# Muscat


1334.1.1 = {
	monarch = {
		name = "Abu Muhammad"
		dynasty = "Nabhan"
		adm = 4
		dip = 4
		mil = 4
	}
}

1334.1.1 = {
	heir = {
		name = "Mahzum"
		monarch_name = "Mahzum"
		dynasty = "Nabhan"
		birth_date = 1334.1.1
		death_date = 1450.1.1
		claim = 95
		adm = 4
		dip = 4
		mil = 5
	}
}

1377.1.1 = {
	monarch = {
		name = "Mahzum"
		dynasty = "Nabhan"
		adm = 4
		dip = 4
		mil = 5
	}
}

1430.1.1 = {
	heir = {
		name = "Sulaym�n"
		monarch_name = "Sulaym�n"
		dynasty = "Nabhan"
		birth_date = 1430.1.1
		death_date = 1488.1.1
		claim = 95
		adm = 7
		dip = 7
		mil = 6
	}
}

1450.1.1 = {
	monarch = {
		name = "Sulaym�n"
		dynasty = "Nabhan"
		adm = 7
		dip = 7
		mil = 6
	}
}

#Rulers of Nizwa

1488.1.1 = {
	monarch = {
		name = "'Umar"
		dynasty = "al-Julanda"
		adm = 6
		dip = 5
		mil = 4
	}
}

1488.1.1 = {
	heir = {
		name = "Muhammaed"
		monarch_name = "Muhammaed I"
		dynasty = "al-Julanda"
		birth_date = 1470.1.1
		death_date = 1492.1.1
		claim = 95
		adm = 3
		dip = 7
		mil = 6
	}
}

1489.1.1 = {
	monarch = {
		name = "Muhammaed I"
		dynasty = "al-Julanda"
		adm = 3
		dip = 7
		mil = 6
	}
}

1489.1.1 = {
	heir = {
		name = "Ahmad"
		monarch_name = "Ahmad"
		dynasty = "al-Julanda"
		birth_date = 1470.1.1
		death_date = 1499.1.1
		claim = 95
		adm = 4
		dip = 6
		mil = 5
	}
}

1492.1.1 = {
	monarch = {
		name = "Ahmad"
		dynasty = "al-Julanda"
		adm = 4
		dip = 6
		mil = 5
	}
}

1492.1.1 = {
	heir = {
		name = "Abu'l-Hasan"
		monarch_name = "Abu'l-Hasan"
		dynasty = "al-Julanda"
		birth_date = 1490.1.1
		death_date = 1500.1.1
		claim = 95
		adm = 5
		dip = 7
		mil = 7
	}
}

1499.1.1 = {
	monarch = {
		name = "Abu'l-Hasan"
		dynasty = "al-Julanda"
		adm = 5
		dip = 7
		mil = 7
	}
}

1499.1.1 = {
	heir = {
		name = "Muhammad"
		monarch_name = "Muhammad II"
		dynasty = "al-Julanda"
		birth_date = 1480.1.1
		death_date = 1535.1.1
		claim = 95
		adm = 3
		dip = 3
		mil = 4
	}
}

1500.1.1 = {
	monarch = {
		name = "Muhammad II"
		dynasty = "al-Julanda"
		adm = 3
		dip = 3
		mil = 4
	}
}

1500.1.1 = {
	heir = {
		name = "Barak�t"
		monarch_name = "Barak�t"
		dynasty = "al-Julanda"
		birth_date = 1500.1.1
		death_date = 1560.1.1
		claim = 95
		adm = 6
		dip = 6
		mil = 5
	}
}

1535.1.1 = {
	monarch = {
		name = "Barak�t"
		dynasty = "Nabhan"
		adm = 6
		dip = 6
		mil = 5
	}
}

#Rulers of Suhar

1560.1.1 = {
	monarch = {
		name = "Makhz�m"
		dynasty = "Nabhan"
		adm = 4
		dip = 6
		mil = 6
	}
}

1560.1.1 = {
	heir = {
		name = "Habh�n"
		monarch_name = "Habh�n"
		dynasty = "Nabhan"
		birth_date = 1560.1.1
		death_date = 1617.2.16
		claim = 95
		adm = 7
		dip = 3
		mil = 5
	}
}

1615.1.1 = {
	monarch = {
		name = "Habh�n"
		dynasty = "Nabhan"
		adm = 7
		dip = 3
		mil = 5
	}
}

1615.1.1 = {
	heir = {
		name = "'Umayr"
		monarch_name = "'Umayr"
		dynasty = "Nabhan"
		birth_date = 1600.1.1
		death_date = 1624.1.1
		claim = 95
		adm = 5
		dip = 4
		mil = 5
	}
}

1617.2.16 = {
	monarch = {
		name = "'Umayr"
		dynasty = "Nabhan"
		adm = 5
		dip = 4
		mil = 5
	}
}

#To Yarubids, Oman

1624.1.1 = {
	monarch = {
		name = "N�sir"
		dynasty = "Ya'ariba"
		adm = 7
		dip = 6
		mil = 3
	}
}

1624.1.1 = {
	heir = {
		name = "Sult�n"
		monarch_name = "Sult�n I"
		dynasty = "Ya'ariba"
		birth_date = 1620.1.1
		death_date = 1668.11.12
		claim = 95
		adm = 5
		dip = 4
		mil = 7
	}
}

1649.4.5 = {
	monarch = {
		name = "Sult�n I"
		dynasty = "Ya'ariba"
		adm = 5
		dip = 4
		mil = 7
	}
}

1649.4.5 = {
	heir = {
		name = "Abi'l-'Arab"
		monarch_name = "Abi'l-'Arab I"
		dynasty = "Ya'ariba"
		birth_date = 1640.1.1
		death_date = 1679.1.1
		claim = 95
		adm = 6
		dip = 4
		mil = 5
	}
}

1668.11.12 = {
	monarch = {
		name = "Abi'l-'Arab I"
		dynasty = "Ya'ariba"
		adm = 6
		dip = 4
		mil = 5
	}
}

1668.11.12 = {
	heir = {
		name = "Sayf"
		monarch_name = "Sayf I"
		dynasty = "Ya'ariba"
		birth_date = 1660.1.1
		death_date = 1711.10.16
		claim = 95
		adm = 5
		dip = 6
		mil = 5
	}
}
		

1679.1.1 = {
	monarch = {
		name = "Sayf I"
		dynasty = "Ya'ariba"
		adm = 5
		dip = 6
		mil = 5
	}
}

1679.1.1 = {
	heir = {
		name = "Sult�n II"
		monarch_name = "Sult�n II"
		dynasty = "Ya'ariba"
		birth_date = 1679.1.1
		death_date = 1718.1.1
		claim = 95
		adm = 5
		dip = 3
		mil = 4
	}
}

1711.10.16 = {
	monarch = {
		name = "Sult�n II"
		dynasty = "Ya'ariba"
		adm = 5
		dip = 3
		mil = 4
	}
}

1711.10.16 = {
	heir = {
		name = "Muhann�"
		monarch_name = "Muhann�"
		dynasty  = "Ya'ariba"
		birth_date = 1700.1.1
		death_date = 1720.1.1
		claim = 95
		adm = 6
		dip = 6
		mil = 4
	}
}

1718.1.1 = {
	monarch = {
		name = "Muhann�"
		dynasty = "Ya'ariba"
		adm = 6
		dip = 6
		mil = 4
	}
}

1718.1.1 = {
	heir = {
		name = "Sayf"
		monarch_name = "Sayf II"
		dynasty = "Ya'ariba"
		birth_date = 1700.1.1
		death_date = 1724.10.3
		claim = 95
		adm = 3
		dip = 3
		mil = 5
	}
}

1720.1.1 = {
	monarch = {
		name = "Sayf II"
		dynasty = "Ya'ariba"
		adm = 3
		dip = 3
		mil = 5
	}
}



1724.10.3 = {
	monarch = {
		name = "Muhammad III"
		dynasty = "Banu Ghafir"
		adm = 5
		dip = 4
		mil = 4
	}
}

1728.4.12 = {
	monarch = {
		name = "Sayf II"
		dynasty = "Ya'ariba"
		adm = 3
		dip = 3
		mil = 5
	}
}

1728.4.12 = {
	heir = {
		name = "Sult�n"
		monarch_name = "Sult�n III"
		birth_date = 1700.1.1
		death_date = 1743.7.2
		claim = 95
		adm = 3
		dip = 4
		mil = 5
	}
}

1742.2.1 = {
	monarch = {
		name = "Sult�n III"
		dynasty = "Ya'ariba"
		adm = 3
		dip = 4
		mil = 5
	}
}

1743.7.2 = {
	monarch = {
		name = "Abi'l-'Arab II"
		dynasty = "Ya'ariba"
		adm = 6
		dip = 4
		mil = 6
	}
}

1749.6.10 = {
	monarch = {
		name = "Ahmad"
		dynasty = "Al Said"
		adm = 7
		dip = 6
		mil = 6
	}
}

1750.1.1 = {
	heir = {
		name = "S�'�d"
		monarch_name = "S�'�d I"
		dynasty = "Al Said"
		birth_date = 1750.1.1
		death_date = 1804.11.16
		claim = 95
		adm = 6
		dip = 4
		mil = 5
	}
}

1783.12.16 = {
	monarch = {
		name = "S�'�d I"
		dynasty = "Al Said"
		adm = 6
		dip = 4
		mil = 5
	}
}

1783.12.16 = {
	heir = {
		name = "S�lim"
		monarch_name = "S�lim I"
		dynasty = "Al Said"
		birth_date = 1780.1.1
		death_date = 1806.9.14
		claim = 95
		adm = 5
		dip = 4
		mil = 5
	}
}

1804.11.16 = {
	monarch = {
		name = "S�lim I"
		dynasty = "Al Said"
		adm = 5
		dip = 4
		mil = 5
	}
}

1804.11.6 = {
	heir = {
		name = "S�'�d"
		monarch_name = "S�'�d II"
		dynasty = "Al Said"
		birth_date = 1797.6.5
		death_date = 1856.10.19
		claim = 95
		adm = 6
		dip = 5
		mil = 5
	}
}

1806.9.14 = {
	monarch = {
		name = "S�'�d II"
		dynasty = "Al Said"
		adm = 6
		dip = 5
		mil = 5
	}
}
