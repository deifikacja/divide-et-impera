government = eastern_despotism
aristocracy_plutocracy = 0
centralization_decentralization = 5
innovative_narrowminded = 0
mercantilism_freetrade = 1
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 0
land_naval = -5
quality_quantity = 2
serfdom_freesubjects = 2
technology_group = chinese
primary_culture = sulawesi
religion = sunni
capital = 1390	# Sumedang

1617.1.1 = {
	monarch = {
		name = "Kusumanidata I"
		dynasty = Matarama
		DIP = 5
		MIL = 5
		ADM = 5
	}
}

1650.1.1 = {
	monarch = {
		name = "Geusan Ulun"
		dynasty = Matarama
		DIP = 5
		MIL = 5
		ADM = 5
	}
}
1680.1.1 = {
	monarch = {
		name = "Rangga Gede"
		dynasty = Matarama
		DIP = 5
		MIL = 5
		ADM = 5
	}
}
1700.1.1 = {
	monarch = {
		name = "Gempol"
		dynasty = Matarama
		DIP = 5
		MIL = 5
		ADM = 5
	}
}
1720.1.1 = {
	monarch = {
		name = "Tanumraja"
		dynasty = Matarama
		DIP = 5
		MIL = 5
		ADM = 5
	}
}
1737.1.1 = {
	monarch = {
		name = "Kusumanidata II"
		dynasty = Matarama
		DIP = 5
		MIL = 5
		ADM = 5
	}
}
1748.1.1 = {
	monarch = {
		name = "Kusumanidata III"
		dynasty = Matarama
		DIP = 5
		MIL = 5
		ADM = 5
	}
}
1761.1.1 = {
	monarch = {
		name = "Surianegara I"
		dynasty = Matarama
		DIP = 5
		MIL = 5
		ADM = 5
	}
}
1765.1.1 = {
	monarch = {
		name = "Kusumanidata IV"
		dynasty = Matarama
		DIP = 5
		MIL = 5
		ADM = 5
	}
}
1773.1.1 = {
	monarch = {
		name = "Tanubaja I"
		dynasty = Matarama
		DIP = 5
		MIL = 5
		ADM = 5
	}
}
1775.1.1 = {
	monarch = {
		name = "Tanubaja II"
		dynasty = Matarama
		DIP = 5
		MIL = 5
		ADM = 5
	}
}
1789.1.1 = {
	monarch = {
		name = "Surianegara II"
		dynasty = Matarama
		DIP = 5
		MIL = 5
		ADM = 5
	}
}
