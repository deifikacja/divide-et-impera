government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 3
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = 2
land_naval = -5
quality_quantity = 4
serfdom_freesubjects = -4
technology_group = new_world
primary_culture = miskito
religion = protestant
capital = 838	# Miskito



1661.1.1 = {
	monarch = {
		name = "Oldman I"
		dynasty = Miskiti
		adm = 4
		dip = 3
		mil = 6
	}
}


1677.1.1 = {
	monarch = {
		name = "Oldman II"
		dynasty = Miskiti
		adm = 6
		dip = 4
		mil = 5
	}
}

1686.1.1 = {
	monarch = {
		name = "Jeremy I"
		dynasty = Miskiti
		adm = 7
		dip = 5
		mil = 7
	}
}

1720.1.1 = {
	monarch = {
		name = "Jeremy II"
		dynasty = Miskiti
		adm = 4
		dip = 5
		mil = 5
	}
}

1729.1.1 = {
	monarch = {
		name = "Peter I"
		dynasty = Miskiti
		adm = 4
		dip = 5
		mil = 5
	}
}

1739.1.1 = {
	monarch = {
		name = "Edward I"
		dynasty = Miskiti
		adm = 4
		dip = 5
		mil = 5
	}
}

1755.1.1 = {
	monarch = {
		name = "George I"
		dynasty = Miskiti
		adm = 4
		dip = 5
		mil = 5
	}
}

1776.1.1 = {
	monarch = {
		name = "George II Frederick"
		dynasty = Miskiti
		adm = 4
		dip = 5
		mil = 5
	}
}

1801.1.1 = {
	monarch = {
		name = "George Frederic Augustus II"
		dynasty = Miskiti
		adm = 4
		dip = 5
		mil = 5
	}
}