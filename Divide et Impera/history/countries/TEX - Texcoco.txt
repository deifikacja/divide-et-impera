government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = -4
land_naval = -5
quality_quantity = 5
serfdom_freesubjects = -5
primary_culture = nahuatl
religion = animism
technology_group = new_world
capital = 2077	#Texcoco

1300.1.1 = {
	monarch = {
		name = "Quinatzin"
		dynasty = Acolhua
		adm = 6
		dip = 6
		mil = 5
		leader = {	name = "Quinatzin"           	type = general	rank = 0	fire = 0	shock = 2	manuever = 2	siege = 0}
	}
}
1357.1.1 = {
	monarch = {
		name = "Techotlala"
		dynasty = Acolhua
		adm = 6
		dip = 8
		mil = 6
		leader = {	name = "Techotlala"           	type = general	rank = 0	fire = 0	shock = 3	manuever = 3	siege = 0 }
	}
}
1409.1.1 = {
	monarch = {
		name = "Ixtlilxochitl"
		dynasty = Acolhua
		adm = 7
		dip = 7
		mil = 6
		leader = {	name = "Ixtlilxochitl"           	type = general	rank = 0	fire = 1	shock = 3	manuever = 4	siege = 0 }
	}
}

1418.1.1 = {
	monarch = {
		name = "Tezozomoc"
		dynasty = Acolhua
		adm = 5
		dip = 5
		mil = 4
		leader = {	name = "Tezozomoc"           	type = general	rank = 0	fire = 1	shock = 3	manuever = 4	siege = 0 }
	}
}

1426.1.1 = {
	monarch = {
		name = "Maxtla"
		dynasty = Acolhua
		adm = 5
		dip = 7
		mil = 7
		leader = {	name = "Maxtla"           	type = general	rank = 0	fire = 1	shock = 4	manuever = 4	siege = 1 }
	}
}

1428.1.1 = {
	monarch = {
		name = "Nezahualcoyotl"
		dynasty = Acolhua
		adm = 7
		dip = 6
		mil = 8
		leader = {	name = "Nezahualcoyotl"           	type = general	rank = 0	fire = 3	shock = 4	manuever = 4	siege = 1 }
	}
}
#Aztek Triple Alliance