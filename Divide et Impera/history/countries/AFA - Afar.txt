government = tribal_despotism
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -3
imperialism_isolationism = 0
secularism_theocracy = 1
offensive_defensive = 1
land_naval = -3
quality_quantity = 1
serfdom_freesubjects = -5
technology_group = sahel
religion = sunni
primary_culture = afar
capital = 1214	# Afar

1734.1.1 = {
	monarch = {
		name= "Kedafu"
		dynasty="Mudaito"
		DIP = 3
		MIL = 3
		ADM = 3
	}
}
1734.1.1 = {
	heir = {
		name = "Kedafu Muhammad"
		monarch_name = "Kedafu Muhammad"
		dynasty = "Mudaito"
		birth_date = 1734.1.1
		death_date = 1779.1.1
		claim = 95
		adm = 4
		dip = 4
		mil = 3
	}
}
1749.1.1 = {
	monarch = {
		name= "Kedafu Muhammad"
		dynasty="Mudaito"
		DIP = 4
		MIL = 4
		ADM = 3
	}
}
1749.1.1 = {
	heir = {
		name = "Ijdahis"
		monarch_name = "Ijdahis"
		dynasty = "Mudaito"
		birth_date = 1749.1.1
		death_date = 1801.1.1
		claim = 95
		adm = 4
		dip = 4
		mil = 3
	}
}
1779.1.1 = {
	monarch = {
		name= "Ijdahis"
		dynasty="Mudaito"
		DIP = 3
		MIL = 4
		ADM = 3
	}
}
1779.1.1 = {
	heir = {
		name = "Ijdahis Muhammad"
		monarch_name = "Ijdahis Muhammad"
		dynasty = "Mudaito"
		birth_date = 1779.1.1
		death_date = 1801.1.1
		claim = 95
		adm = 4
		dip = 4
		mil = 3
	}
}
1801.1.1 = {
	monarch = {
		name= "Ijdahis Muhammad"
		dynasty="Mudaito"
		DIP = 4
		MIL = 4
		ADM = 5
	}
} 
1801.1.1 = {
	heir = {
		name = "Anfari"
		monarch_name = "Anfari ibn Ijdahis"
		dynasty = "Mudaito"
		birth_date = 1801.1.1
		death_date = 1862.1.1
		claim = 95
		adm = 4
		dip = 4
		mil = 3
	}
}
1802.1.1 = {
	monarch = {
		name= "Anfari ibn Ijdahis"
		dynasty="Mudaito"
		DIP = 3
		MIL = 4
		ADM = 3
	}
}
