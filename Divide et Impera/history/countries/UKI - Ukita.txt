government = daimyo
aristocracy_plutocracy = -4
centralization_decentralization = 2
innovative_narrowminded = 2
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = -1
offensive_defensive = -1
land_naval = 0
quality_quantity = 4
serfdom_freesubjects = -5
primary_culture = japanese
religion = shinto
technology_group = chinese
capital = 1355	# Bizen
daimyo = yes

1356.1.1 = { 
monarch = { 
name = "Kojima Takanori" 
dynasty = Ukita 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Kojima Takanori" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1356.1.1 = {
    heir = {
        name = "Takanori"
        monarch_name =  "Takanori" 
        dynasty =  Ukita 
        birth_date = 1342.1.1
        death_date = 1450.1.1
        claim = 71
        adm = 5
        dip = 8
        mil = 7
        leader = {
            name =  "Takanori" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1400.1.1 = { 
monarch = { 
name = "Takanori" 
dynasty = Ukita 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Ukita Takanori" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1400.1.1 = {
    heir = {
        name = "Hisaie"
        monarch_name =  "Hisaie" 
        dynasty =  Ukita 
        birth_date = 1400.1.1
        death_date = 1450.1.1
        claim = 74
        adm = 5
        dip = 8
        mil = 7
        leader = {
            name =  "Hisaie" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1450.1.1 = { 
monarch = { 
name = "Hisaie" 
dynasty = Ukita 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Ukita Hisaie" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 


1450.1.1 = { 
monarch = { 
name = "Muneie" 
dynasty = Ukita 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Ukita Muneie" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1450.1.1 = {
    heir = {
        name = "Yoshiie"
        monarch_name =  "Yoshiie" 
        dynasty =  Ukita 
        birth_date = 1440.1.1
        death_date = 1534.1.1
        claim = 90
        adm = 5
        dip = 8
        mil = 7
        leader = {
            name =  "Yoshiie" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1500.1.1 = { 
monarch = { 
name = "Yoshiie" 
dynasty = Ukita 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Ukita Yoshiie" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1500.1.1 = {
    heir = {
        name = "Okiie"
        monarch_name =  "Okiie" 
        dynasty =  Ukita 
        birth_date = 1485.1.1
        death_date = 1536.1.1
        claim = 80
        adm = 5
        dip = 8
        mil = 7
        leader = {
            name =  "Okiie" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1534.1.1 = { 
monarch = { 
name = "Okiie" 
dynasty = Ukita 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Ukita Okiie" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1534.1.1 = {
    heir = {
        name = "Naoie"
        monarch_name =  "Naoie" 
        dynasty =  Ukita 
        birth_date = 1519.1.1
        death_date = 1582.2.1
        claim = 91
        adm = 5
        dip = 8
        mil = 7
        leader = {
            name =  "Naoie" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1536.1.1 = { 
monarch = { 
name = "Naoie" 
dynasty = Ukita 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Ukita Naoie" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1536.1.1 = {
    heir = {
        name = "Hideie"
        monarch_name =  "Hideie" 
        dynasty =  Ukita 
        birth_date = 1530.1.1
        death_date = 1568.1.1
        claim = 95
        adm = 5
        dip = 8
        mil = 7
        leader = {
            name =  "Hideie" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1582.2.1 = { 
monarch = { 
name = "Hideie" 
dynasty = Ukita 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Ukita Hideie" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

