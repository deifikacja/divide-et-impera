government = caste_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 2
mercantilism_freetrade = 2
imperialism_isolationism = -1
secularism_theocracy = 1
offensive_defensive = -1
land_naval = 3
quality_quantity = -1
serfdom_freesubjects = -1
primary_culture = malayan
religion = hinduism
technology_group = chinese
capital = 596	# Malacca

#King of Malacca, styled "Sultan" later
1399.1.1 = {
	monarch = {
		name = "Sam Agi"
		dynasty = "Srivijaya"
		adm = 6
		dip = 4
		mil = 3
	}
}

1399.1.1 = {
	heir = {
		name = "Parameswara"
		monarch_name = "Parameswara"
		dynasty = "Srivijaya"
		birth_date = 1370.1.1
		death_date = 1414.1.1
		claim = 95
		adm = 4
		dip = 3
		mil = 4
	}
}

1402.1.1 = {
	monarch = {
		name = "Parameswara"
		dynasty = "Srivijaya"
		adm = 4
		dip = 3
		mil = 4
	}
}

1402.1.1 = {
	heir = {
		name = "Megat Iskandar Sh�h"
		monarch_name = "Megat Iskandar Sh�h"
		dynasty = "Malacca"
		birth_date = 1380.1.1
		death_date = 1424.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1409.1.1 = { 
	religion = sunni 
	government = eastern_despotism
}
1414.1.1 = {
	monarch = {
		name = "Megat Iskandar Sh�h"
		dynasty = "Malacca"
		adm = 5
		dip = 5
		mil = 5
	}
}

1414.1.1 = {
	heir = {
		name = "Muhammad Sh�h"
		monarch_name = "Muhammad Sh�h"
		dynasty = "Malacca"
		birth_date = 1400.1.1
		death_date = 1444.1.1
		claim = 95
		adm = 4
		dip = 3
		mil = 4
	}
}
		

1424.1.1 = {
	monarch = {
		name = "Muhammad Sh�h"
		dynasty = "Malacca"
		adm = 4
		dip = 3
		mil = 4
	}
}

1424.1.1 = {
	heir = {
		name = "Abu Syahid"
		monarch_name = "Abu Syahid"
		dynasty = "Malacca"
		birth_date = 1424.1.1
		death_date = 1446.1.1
		claim = 95
		adm = 5
		dip = 6
		mil = 5
	}
}

1444.1.1 = {
	monarch = {
		name = "Abu Syahid"
		dynasty = "Malacca"
		adm = 5
		dip = 6
		mil = 5
	}
}

1444.1.1 = {
	heir = {
		name = "Muzaffar Sh�h"
		monarch_name = "Muzaffar Sh�h"
		dynasty = "Malacca"
		birth_date = 1420.1.1
		death_date = 1459.1.1
		claim = 95
		adm = 4
		dip = 8
		mil = 6
	}
}

1446.1.1 = {
	monarch = {
		name = "Muzaffar Sh�h"
		dynasty = "Malacca"
		adm = 4
		dip = 8
		mil = 6
	}
}

1446.1.1 = {
	heir = {
		name = "Mans�r Sh�h"
		monarch_name = "Mans�r Sh�h"
		dynasty = "Malacca"
		birth_date = 1440.1.1
		death_date = 1477.10.2
		claim = 95
		adm = 5
		dip = 5
		mil = 7
	}
}

1458.1.1 = { leader = {	name = "Hang Jebat"            	type = general	rank = 1	fire = 3	shock = 3	manuever = 3	siege = 1	death_date = 1477.1.1 } }

1459.1.1 = {
	monarch = {
		name = "Mans�r Sh�h"
		dynasty = "Malacca"
		adm = 5
		dip = 5
		mil = 7
	}
}

1459.1.1 = {
	heir = {
		name = "Ri'�yat Sh�h"
		monarch_name = "Ri'�yat Sh�h"
		dynasty  = "Malacca"
		birth_date = 1450.1.1
		death_date = 1488.1.1
		claim = 95
		adm = 6
		dip = 4
		mil = 8
		leader = { name = "Ri�yat Sh�h"		type = general	rank = 0	fire = 3	shock = 2	manuever = 3	siege = 0 }
	}
}

1460.1.1 = { leader = {	name = "Hang Tuah"             	type = admiral	rank = 1	fire = 3	shock = 4	manuever = 4	siege = 0	death_date = 1488.1.1 } }

1477.10.2 = {
	monarch = {
		name = "Ri'�yat Sh�h"
		dynasty = "Malacca"
		adm = 6
		dip = 4
		mil = 8
		leader = { name = "Ri�yat Sh�h"		type = general	rank = 0	fire = 3	shock = 2	manuever = 3	siege = 0 }
	}
}

1477.10.2 = {
	heir = {
		name = "Mahm�d Sh�h"
		monarch_name = "Mahm�d Sh�h"
		dynasty = "Malacca"
		birth_date = 1470.1.1
		death_date = 1528.1.1
		claim = 95
		adm = 3
		dip = 3
		mil = 7
	}
}

1488.1.1 = {
	monarch = {
		name = "Mahm�d Sh�h"
		dynasty = "Malacca"
		adm = 3
		dip = 3
		mil = 7
	}
}

1488.1.1 = {
	heir = {
		name = "Ri'�yat Sh�h"
		monarch_name = "Ri'�yat Sh�h I"
		dynasty = "Malacca"
		birth_date = 1488.1.1
		death_date = 1564.1.1
		claim = 95
		adm = 5
		dip = 7
		mil = 7
	}
}

1511.1.1 = { leader = {	name = "Hang Nadim"            	type = admiral	rank = 1	fire = 3	shock = 3	manuever = 3	siege = 0	death_date = 1528.1.1 } }

# Annexed by Portugal 1511

1528.1.1 = {
	monarch = {
		name = "Ri'�yat Sh�h I"
		dynasty = "Malacca"
		adm = 5
		dip = 7
		mil = 7
	}
}

1564.1.1 = {
	monarch = {
		name = "Muzaffar Sh�h"
		dynasty = "Malacca"
		adm = 4
		dip = 5
		mil = 4
	}
}

1580.1.1 = {
	monarch = {
		name = "'Abd al-Jal�l Sh�h I"
		dynasty = "Malacca"
		adm = 4
		dip = 7
		mil = 5
	}
}

1597.1.1 = {
	monarch = {
		name = "Ri'�yat Sh�h II"
		dynasty = "Malacca"
		adm = 3
		dip = 3
		mil = 5
	}
}

1613.1.1 = {
	monarch = {
		name = "Ma'ayat Sh�h"
		dynasty = "Malacca"
		adm = 4
		dip = 4
		mil = 4
	}
}

1623.1.1 = {
	monarch = {
		name = "'Abd al-Jal�l Sh�h II"
		dynasty = "Malacca"
		adm = 5
		dip = 8
		mil = 8
		leader = {	name = "Abd Jal�l Sh�h II"     	type = general	rank = 0	fire = 3	shock = 2	manuever = 3	siege = 1}
	}
}

1677.1.1 = {
	monarch = {
		name = "Ibr�h�m Sh�h"
		dynasty = "Malacca"
		adm = 3
		dip = 4
		mil = 3
	}
}

1685.2.15 = {
	monarch = {
		name = "Mahm�d Sh�h"
		dynasty = "Malacca"
		adm = 3
		dip = 3
		mil = 3
	}
}

1699.1.1 = {
	monarch = {
		name = "Ri'�yat Sh�h"
		dynasty = "Malacca"
		adm = 5
		dip = 4
		mil = 4
	}
}

1717.1.1 = {
	monarch = {
		name = "Rahmat Sh�h"
		dynasty = "Malacca"
		adm = 5
		dip = 5
		mil = 5
	}
}

1722.1.1 = {
	monarch = {
		name = "Sulaym�n Badr Sh�h"
		dynasty = "Malacca"
		adm = 3
		dip = 3
		mil = 3
	}
}

1760.1.1 = {
	monarch = {
		name = "Mu'azzam Sh�h"
		dynasty = "Malacca"
		adm = 4
		dip = 5
		mil = 6
	}
}

1761.1.1 = {
	monarch = {
		name = "Mahm�d Ri'�yat Sh�h"
		dynasty = "Malacca"
		adm = 4
		dip = 3
		mil = 3
	}
}

1812.1.1 = {
	monarch = {
		name = "'Abd ar-Rahm�n Sh�h"
		dynasty = "Malacca"
		adm = 3
		dip = 4
		mil = 3
	}
}
