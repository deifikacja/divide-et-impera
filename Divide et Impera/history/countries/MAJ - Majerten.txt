government = confessional_state
aristocracy_plutocracy = 0
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = 4
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = 1
land_naval = 3
quality_quantity = 2
serfdom_freesubjects = 0
technology_group = sahel
religion = sunni
primary_culture = somali
capital = 1206	# Majerten



1350.1.1 = { 
monarch = { 
name = "Ajuran" 
dynasty = "Gareen" 
DIP = 4 
MIL = 4 
ADM = 4 
} 
} 

1350.1.1 = {
    heir = {
        name = "Arliqo"
        monarch_name =  "Arliqo" 
        dynasty =  "Gareen" 
        birth_date = 1346.1.1
        death_date = 1450.1.1
        claim = 77
        adm = 25
        dip = 2
        mil = 376
   }
}


1400.1.1 = { 
monarch = { 
name = "Arliqo" 
dynasty = "Gareen" 
DIP = 4 
MIL = 4 
ADM = 4 
} 
} 

1400.1.1 = {
    heir = {
        name = "Sarjelle"
        monarch_name =  "Sarjelle" 
        dynasty =  "Gareen" 
        birth_date = 1396.1.1
        death_date = 1500.1.1
        claim = 76
        adm = 25
        dip = 2
        mil = 376
   }
}


1450.1.1 = { 
monarch = { 
name = "Sarjelle" 
dynasty = "Gareen" 
DIP = 4 
MIL = 4 
ADM = 4 
} 
} 

1450.1.1 = {
    heir = {
        name = "Fadumo"
        monarch_name =  "Fadumo" 
        dynasty =  "Gareen" 
        birth_date = 1442.1.1
        death_date = 1600.1.1
        claim = 90
        adm = 25
        dip = 2
        mil = 376
   }
}

1500.1.1 = { 
monarch = { 
name = "Fadumo" 
dynasty = "Gareen" 
DIP = 4 
MIL = 4 
ADM = 4 
} 
} 

1500.1.1 = {
    heir = {
        name = "Umur"
        monarch_name =  "Umur" 
        dynasty =  "Gareen" 
        birth_date = 1496.1.1
        death_date = 1650.1.1 #Ajuuraan
        claim = 73
        adm = 25
        dip = 2
        mil = 376
   }
}

1600.1.1 = { 
monarch = { 
name = "Umur" 
dynasty = "Gareen" 
DIP = 4 
MIL = 4 
ADM = 4 
} 
} 

1600.1.1 = {
    heir = {
        name = "Sultan"
        monarch_name =  "Sultan" 
        dynasty =  "Harti" 
        birth_date = 1598.1.1
        death_date = 1800.1.1
        claim = 82
        adm = 25
        dip = 2
        mil = 376
   }
}

#Ajuuraan state extinguished, Maajeerteen from now on 
1650.1.1 = { 
monarch = { 
name = "Sultan" 
dynasty = "Harti" 
DIP = 4 
MIL = 4 
ADM = 4 
} 
} 

1650.1.1 = {
    heir = {
        name = "Mahmud"
        monarch_name =  "Mahmud IV" 
        dynasty =  "Harti" 
        birth_date = 1650.1.1
        death_date = 1815.1.1
        claim = 80
        adm = 2
        dip = 2
        mil = 3
   }
}
1800.1.1 = { 
monarch = { 
name = "Mahmud IV" 
dynasty = "Harti" 
DIP = 3 
MIL = 3 
ADM = 3 
} 
} 

1800.1.1 = {
    heir = {
        name = "'Uthman"
        monarch_name =  "'Uthman II" 
        dynasty =  "Harti" 
        birth_date = 1799.1.1
        death_date = 1821.1.1
        claim = 76
        adm = 25
        dip = 2
        mil = 376
   }
}

1815.1.1 = { 
monarch = { 
name = "'Uthman II" 
dynasty = "Harti" 
DIP = 3 
MIL = 3 
ADM = 3 
} 
} 

