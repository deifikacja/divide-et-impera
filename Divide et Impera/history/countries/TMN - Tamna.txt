government = eastern_despotism
aristocracy_plutocracy = -4
centralization_decentralization = -1
innovative_narrowminded = 1
mercantilism_freetrade = -2
imperialism_isolationism = 0
secularism_theocracy = 1
offensive_defensive = 1
land_naval = -1
quality_quantity = 0
serfdom_freesubjects = -4
primary_culture = dzedzu
religion = buddhism
technology_group = chinese
capital = 1364	# Jeju


1356.1.1 = {
	monarch = {
		name = "Sovergin of Tamna"
		dynasty = Goryeo
		adm = 7
		dip = 5
		mil = 7
	}
}
1500.1.1 = { religion = confucianism }