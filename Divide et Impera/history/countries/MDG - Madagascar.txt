government = tribal_despotism
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -3
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = -1
land_naval = -1
quality_quantity = 1
serfdom_freesubjects = -3
primary_culture = madagasque
religion = animism
technology_group = african
capital = 1274	# Tananarivo

1817.8.23 = {
	monarch = {
		name = "Radama I Lehidama"
		dynasty = Merina
		DIP = 7
		ADM = 8
		MIL = 7
	}
}
