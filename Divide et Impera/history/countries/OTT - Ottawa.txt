government = tribal_federation
aristocracy_plutocracy = 0
centralization_decentralization = 4
innovative_narrowminded = 0
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = -5
land_naval = -5
quality_quantity = 5
serfdom_freesubjects = 5
primary_culture = ojibwe
religion = shamanism
technology_group = new_world
capital = 991


1350.1.1 = {
	monarch = {
		name = "Chief of Ottawa"
		adm = 7
		dip = 2
		mil = 8
	}
}

1720.1.1 = {
	monarch = {
		name = "Pontiac"#The very same
		adm = 5
		dip = 9
		mil = 9
		leader = {	name = "Pontiac"               	type = conquistador	rank = 0	fire = 3	shock = 5	manuever = 6	siege = 1}
	}
}
