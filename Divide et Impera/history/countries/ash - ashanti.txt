government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = -2
land_naval = -5
quality_quantity = 2
serfdom_freesubjects = -5
primary_culture = aka
religion = animism
technology_group = african
capital = 1138	# Kumasi

#Ashanti Chiefs

1500.1.1 = {
	monarch = {
		name = "Agyinamoa Mpatu"
		dynasty = "Oyoko Abohyen"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1570.1.1 = {
	monarch = {
		name = "Twum"
		dynasty = "Oyoko Abohyen"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1585.1.1 = {
	monarch = {
		name = "Antwi"
		dynasty = "Oyoko Abohyen"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1600.1.1 = {
	monarch = {
		name = "Kobia Amama"
		dynasty = "Oyoko Abohyen"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1630.1.1 = {
	monarch = {
		name = "Oti Akenten"
		dynasty = "Oyoko Abohyen"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1660.1.1 = {
	monarch = {
		name = "Obiri Eboa"
		dynasty = "Oyoko Abohyen"
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1675.1.1 = {
	monarch = {
		name = "Osei Tutu"
		dynasty = "Oyoko Abohyen"
		DIP = 7
		ADM = 9
		MIL = 7
	}
}

1701.1.1 = { government = tribal_federation } #Ashanti confederation formed

1717.1.1 = {
	monarch = {
		name = "Maniampon" #Regency
		regent=yes
		DIP = 2
		ADM = 2
		MIL = 2
	}
}

1731.1.1 = {
	monarch = {
		name = "Opoku Ware I"
		dynasty = "Oyoko Abohyen"
		DIP = 5
		ADM = 8
		MIL = 6
	}
}

1731.1.1 = {
	heir = {
		name = "Kusi Obodum"
		monarch_name = "Kusi Obodum"
		dynasty = "Oyoko Abohyen"
		birth_date = 1720.1.1
		death_date = 1764.1.1
		claim = 95
		adm = 7
		dip = 4
		mil = 3
	}
}

1750.1.1 = {
	monarch = {
		name = "Kusi Obodum"
		dynasty = "Oyoko Abohyen"
		DIP = 4
		ADM = 7
		MIL = 3
	}
}

1750.1.1 = {
	heir = {
		name = "Osei Kojo"
		monarch_name = "Osei Kojo"
		dynasty = "Oyoko Abohyen"
		birth_date = 1750.1.1
		death_date = 1781.1.1
		claim = 95
		adm = 6
		dip = 5
		mil = 4
	}
}

1764.1.1 = {
	monarch = {
		name = "Osei Kojo"
		dynasty = "Oyoko Abohyen"
		DIP = 5
		ADM = 6
		MIL = 4
	}
}

1781.1.1 = {
	heir = {
		name = "Osei Kwame"
		monarch_name = "Osei Kwame"
		dynasty = "Oyoko Abohyen"
		birth_date = 1760.1.1
		death_date = 1803.1.1
		claim = 95
		adm = 7
		dip = 3
		mil = 3
	}
}

1781.1.1 = {
	monarch = {
		name = "Osei Kwame"
		dynasty = "Oyoko Abohyen"
		DIP = 3
		ADM = 7
		MIL = 3
	}
}

1781.1.1 = {
	heir = {
		name = "Opoku Fofie"
		monarch_name = "Opoku Fofie"
		dynasty = "Oyoko Abohyen"
		birth_date = 1760.1.1
		death_date = 1803.1.1
		claim = 95
		adm = 4
		dip = 2
		mil = 2
	}
}

1803.1.1 = {
	monarch = {
		name = "Opoku Fofie"
		dynasty = "Oyoko Abohyen"
		DIP = 2
		ADM = 4
		MIL = 2
	}
}

1804.1.1 = {
	monarch = {
		name = "Osei Bonsu"
		dynasty = "Oyoko Abohyen"
		DIP = 5
		ADM = 7
		MIL = 6
	}
}
