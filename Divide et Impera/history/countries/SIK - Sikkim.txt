government = eastern_despotism
aristocracy_plutocracy = -2
centralization_decentralization = 3
innovative_narrowminded = 3
mercantilism_freetrade = -4
offensive_defensive = 0
imperialism_isolationism = 0
secularism_theocracy = 0
land_naval = -2
quality_quantity = 2
serfdom_freesubjects = -2
technology_group = chinese
religion = buddhism
primary_culture = tibetan
capital = 1342	# Yoksam

#To Tibet to 1642
1642.1.1 = {
	monarch = {
		name = "Phunsong"
		dynasty = Namgyal
		adm = 9
		dip = 9
		mil = 7
	}
}

1670.1.1 = {
	monarch = {
		name = "Tensung"
		dynasty = Namgyal
		adm = 5
		dip = 5
		mil = 5
	}
}

1686.1.1 = {
	monarch = {
		name = "Chador"
		dynasty = Namgyal
		adm = 5
		dip = 5
		mil = 5
	}
}

1717.1.1 = {
	monarch = {
		name = "Gyurmed"
		dynasty = Namgyal
		adm = 5
		dip = 5
		mil = 5
	}
}

1733.1.1 = {
	monarch = {
		name = "Nyamgal"
		dynasty = Namgyal
		adm = 5
		dip = 5
		mil = 5
	}
}

1780.1.1 = {
	monarch = {
		name = "Tenzing"
		dynasty = Namgyal
		adm = 5
		dip = 5
		mil = 5
	}
}

1793.1.1 = {
	monarch = {
		name = "Tsugphud"
		dynasty = Namgyal
		adm = 4
		dip = 5
		mil = 7
	}
}