government = tribal_federation
aristocracy_plutocracy = 0
centralization_decentralization = 5
innovative_narrowminded = 0
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = -5
land_naval = -5
quality_quantity = -2
serfdom_freesubjects = 0
primary_culture = navajo
religion = shamanism
technology_group = new_world
capital = 878	# Navajo


1351.1.1 = {
	monarch = {
		name = "Chief of Navajo"
		adm = 4
		dip = 2
		mil = 8
	}
}