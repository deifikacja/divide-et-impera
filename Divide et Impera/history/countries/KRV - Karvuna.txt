government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -3
imperialism_isolationism = 0
secularism_theocracy = 1
offensive_defensive = 2
land_naval = -1
quality_quantity = 1
serfdom_freesubjects = -5
technology_group = eastern
primary_culture = bulgarian
religion = greek_orthodox
capital = 159	# Sofia

 

1346.1.1 = { leader = {	name = "Fedor" type = general rank = 1 fire = 3	shock = 3 manuever = 2	siege = 0 death_date = 1390.1.1 } }
1348.1.1 = {
	monarch = {
		name = "Dobrotici"
		dynasty = Dobrogi
		DIP = 5
		ADM = 3
		MIL = 4
		leader = { name = "Dobrotici" type = general rank = 0 fire = 3	shock = 3 manuever = 2	siege = 0 death_date = 1390.1.1 }
	}
}

1386.1.1 = {
	monarch = {
		name = "Ivanko"
		dynasty = Dobrogi
		DIP = 3
		ADM = 3
		MIL = 4
	}
}