government = caste_monarchy
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = -1
offensive_defensive = 2
land_naval = -3
quality_quantity = -5
serfdom_freesubjects = -2
primary_culture = kashmiri
religion = hinduism
technology_group = indian
capital = 1982	# Nabha


#Rajas
1652.1.1 = {
	monarch = {
		name = "Phul"
		dynasty = Phulkian
		adm = 7
		dip = 3
		mil = 3
	}
}
1718.1.1 = {
	monarch = {
		name = "Gurditta"
		dynasty = Phulkian
		adm = 7
		dip = 3
		mil = 3
	}
}
1754.1.1 = {
	monarch = {
		name = "Hamir Singh"
		dynasty = Phulkian
		adm = 7
		dip = 3
		mil = 3
	}
}
1783.12.1 = {
	monarch = {
		name = "Jashwant Singh"
		dynasty = Phulkian
		adm = 7
		dip = 3
		mil = 3
	}
}