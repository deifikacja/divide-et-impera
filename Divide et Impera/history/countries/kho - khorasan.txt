government = religion_federation 
aristocracy_plutocracy = 0
centralization_decentralization = 5
innovative_narrowminded = -2
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = 3
offensive_defensive = -4
land_naval = -4
quality_quantity = -1
serfdom_freesubjects = -1
technology_group = muslim
religion = sufism
primary_culture = persian
capital = 445  # Marv

1353.5.12 = {
	monarch = {
		name = "Yahya ibn Karawi"
		dynasty = "Sarbardar"
		adm = 4
		dip = 3
		mil = 5
	}
}

1358.1.1 = {
	monarch = {
		name = "Zahir ad-Din"
		dynasty = "Sarbardar"
		adm = 5
		dip = 3
		mil = 3
	}
}

1359.1.1 = {
	monarch = {
		name = "Haidar al-Qassab"
		dynasty = "Sarbardar"
		adm = 3
		dip = 3
		mil = 3
	}
}


1360.1.1 = {
	monarch = {
		name = "Lutf Allah"
		dynasty = "Sarbardar"
		adm = 3
		dip = 4
		mil = 3
	}
}

1361.1.1 = {
	monarch = {
		name = "Hasan al-Damghani"
		dynasty = "Sarbardar"
		adm = 5
		dip = 4
		mil = 4
	}
}

1364.1.1 = {
	monarch = {
		name = "Khwaja 'Ali Muyad"
		dynasty = "Sarbardar"
		adm = 5
		dip = 7
		mil = 5
	}
}

#Conquered by Timur
1449.1.1 = { government = eastern_despotism
aristocracy_plutocracy = -2
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -4
secularism_theocracy = 0
offensive_defensive = -2
land_naval = 1
quality_quantity = 3
serfdom_freesubjects = -2
primary_culture = persian
religion = shiite
technology_group = muslim
capital = 436 }	# Khurasan
1449.1.1 = {
	monarch = {
		name = "Babur Ibn-Baysunkur"
		dynasty = "Timurid"
		adm = 4
		dip = 5
		mil = 5
	}
}

1449.1.1 = {
	heir = {
		name = "Ibrahim"
		monarch_name = "Ibrahim"
		dynasty = "Timurid"
		birth_date = 1430.1.1
		death_date = 1459.1.1
		claim = 95
		adm = 4
		dip = 4
		mil = 5
	}
}

1457.1.1 = {
	monarch = {
		name = "Ibrahim"
		dynasty = "Timurid"
		adm = 4
		dip = 4
		mil = 5
	}
}

1457.1.1 = {
	heir = {
		name = "Ab� Sa'id"
		monarch_name = "Ab� Sa'id"
		dynasty = "Timurid"
		birth_date = 1424.1.1
		death_date = 1469.8.27
		claim = 95
		adm = 4
		dip = 5
		mil = 5
	}
}

1459.1.1 = {
	monarch = {
		name = "Ab� Sa'id"
		dynasty = "Timurid"
		adm = 3
		dip = 5
		mil = 6
	}
}

1459.1.1 = {
	heir = {
		name = "Hussein Baiqara"
		monarch_name = "Hussein Baiqara"
		dynasty = "Timurid"
		birth_date = 1438.1.1
		death_date = 1506.5.4
		claim = 95
		adm = 4
		dip = 5
		mil = 5
	}
}

1469.8.27 = {
	monarch = {
		name = "Hussein Baiqara"
		dynasty = "Timurid"
		adm = 4
		dip = 5
		mil = 5
	}
}

1760.1.1 = {
	monarch = {
		name = "Shah Rukh"
		dynasty = "Afsharid"
		adm = 3
		dip = 3
		mil = 3
	}
}
