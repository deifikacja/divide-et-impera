government = eastern_despotism
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 1
offensive_defensive = -2
land_naval = -1
quality_quantity = -1
serfdom_freesubjects = -1
primary_culture = turkish
religion = sunni
technology_group = ottoman
capital = 324	# Mut


1355.11.22 = {
	monarch = {
		name = "Seyfeddin S�leyman"
		dynasty = "Karamanli"
		adm = 3
		dip = 4
		mil = 4
	}
}

1356.1.1 = {
	heir = {
		name = "Damad Al�eddin Ali"
		monarch_name = "Damad Al�eddin Ali I"
		dynasty = "Karamanli"
		birth_date = 1357.1.1
		death_date = 1398.1.1
		claim = 95
		adm = 6
		dip = 6
		mil = 3
	}
}

1357.1.1 = {
	monarch = {
		name = "Damad Al�eddin Ali I"
		dynasty = Karamanli
		adm = 5
		dip = 4
		mil = 5
	}
}

1357.1.1 = {
	heir = {
		name = "Sultanz�de N�sireddin Mehmed"
		monarch_name = "Sultanz�de N�sireddin Mehmed"
		dynasty = "Karamanli"
		birth_date = 1357.1.1
		death_date = 1398.1.1
		claim = 95
		adm = 6
		dip = 6
		mil = 3
	}
}

1398.1.1 = {
	monarch = {
		name = "Sultanz�de N�sireddin Mehmed"
		dynasty = Karamanli
		adm = 6
		dip = 6
		mil = 3
	}
}

1407.1.1 = {
	heir = {
		name = "Damad Bengi Al�eddin Ali"
		monarch_name = "Damad Bengi Al�eddin Ali"
		dynasty = "Karamanli"
		birth_date = 1407.1.1
		death_date = 1424.1.1
		claim = 95
		adm = 4
		dip = 6
		mil = 4
	}
}

1418.1.1 = {
	monarch = {
		name = "Damad Bengi Al�eddin Ali"
		dynasty = Karamanli
		adm = 4
		dip = 6
		mil = 4
	}
}

1418.1.1 = {
	heir = {
		name = "Damad Ibrahim"
		monarch_name = "Damad Ibrahim"
		dynasty = "Karamanli"
		birth_date = 1418.1.1
		death_date = 1464.1.1
		claim = 95
		adm = 4
		dip = 5
		mil = 5
	}
}

1420.1.1 = { capital = 324 } #Konya taken by the Ottomans

1424.1.1 = {
	monarch = {
		name = "Damad Ibrahim"
		dynasty = Karamanli
		adm = 4
		dip = 5
		mil = 5
	}
}

1424.1.1 = {
	heir = {
		name = "Sultanz�de Ishak"
		monarch_name = "Sultanz�de Ishak"
		dynasty = "Karamanli"
		birth_date = 1424.1.1
		death_date = 1465.1.1
		claim = 95
		adm = 3
		dip = 3
		mil = 3
	}
}

1464.1.1 = {
	monarch = {
		name = "Sultanz�de Ishak"
		dynasty = Karamanli
		adm = 3
		dip = 3
		mil = 3
	}
}

1464.1.1 = {
	heir = {
		name = "Sultanz�de P�r Ahmed"
		monarch_name = "Sultanz�de P�r Ahmed"
		dynasty = "Karamanli"
		birth_date = 1450.1.1
		death_date =1474.1.1
		claim = 95
		adm = 5
		dip = 4
		mil = 3
	}
}
		

1465.1.1 = {
	monarch = {
		name = "Sultanz�de P�r Ahmed"
		dynasty = Karamanli
		adm = 5
		dip = 4
		mil = 3
	}
}

1465.1.1 = {
	heir = {
		name = "Kasim"
		monarch_name = "Kasim"
		dynasty = "Karamanli"
		birth_date = 1460.1.1
		death_date = 1483.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 4
	}
}

1474.1.1 = {
	monarch = {
		name = "Kasim"
		dynasty = Karamanli
		adm = 5
		dip = 5
		mil = 4
	}
}

1474.1.1 = {
	heir = {
		name = "Turgutoglu Mahmud"
		monarch_name = "Turgutoglu Mahmud"
		dynasty = "Karamanli"
		birth_date = 1470.1.1
		death_date = 1487.1.1
		claim = 95
		adm = 5
		dip = 3
		mil = 3
	}
}

1483.1.1 = {
	monarch = {
		name = "Turgutoglu Mahmud"
		dynasty = Karamanli
		adm = 5
		dip = 3
		mil = 3
	}
}