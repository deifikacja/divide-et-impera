government = tribal_federation
aristocracy_plutocracy = 5
centralization_decentralization = 5
innovative_narrowminded = 0
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = 3
land_naval = -5
quality_quantity = 0
serfdom_freesubjects = 5
primary_culture = shawnee
religion = shamanism
technology_group = new_world
capital = 943	# Wyandot


1350.1.1 = {
	monarch = {
		name = "Chief of Miami"
		adm = 5
		dip = 4
		mil = 5
	}
}
