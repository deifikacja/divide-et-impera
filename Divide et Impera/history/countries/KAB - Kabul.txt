government = eastern_despotism
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 3
mercantilism_freetrade = -3
imperialism_isolationism = -1
secularism_theocracy = -1
offensive_defensive = -4
land_naval = -4
quality_quantity = 3
serfdom_freesubjects = -5
technology_group = muslim
religion = sunni
primary_culture = baluchi
capital = 451	# Kabul


#Timurid Dynasty
1419.1.1 = {
	monarch = {
		name = "Suurgatmish ibn Shah Rukh"
		dynasty = Timurid
		adm = 6
		dip = 8
		mil = 8
	}
}

1427.1.1 = {
	monarch = {
		name = "Masud"
		dynasty = Timurid
		adm = 3
		dip = 4
		mil = 5
	}
}

1439.1.1 = {
	monarch = {
		name = "Karuchar"
		dynasty = Timurid
		adm = 4
		dip = 4
		mil = 8
	}
}

1451.1.1 = {
	monarch = {
		name = "Abu al-Qasim Babur"
		dynasty = Timurid
		adm = 3
		dip = 4
		mil = 3
	}
}

1461.1.1 = {
	monarch = {
		name = "Ulug Beg II"
		dynasty = Timurid
		adm = 4
		dip = 3
		mil = 4
		
	}
}

#Mihrabanid Dynasty

1501.1.1 = {
	monarch = {
		name = "Zu-n-Nun Beg Argun"
		dynasty = Mihrabanid
		adm = 6
		dip = 6
		mil = 9
	}
}

#Timurid Dynasty

1502.1.1 = {
	monarch = {
		name = "Abd al-Razzak"
		dynasty = Timurid
		adm = 4
		dip = 5
		mil = 6
	}
}

#Mihrabanid Dynasty

1503.1.1 = {
	monarch = {
		name = "Zu-n-Nun Beg Argun"
		dynasty = Mihrabanid
		adm = 4
		dip = 4
		mil = 6
	}
}

#Durrani Dynasty

1817.1.1 = {
	monarch = {
		name = "Sultan 'Ali"
		dynasty = Durrani
		adm = 3
		dip = 3
		mil = 3
	}
}

1819.1.1 = {
	monarch = {
		name = "Ayub Khan"
		dynasty = Durrani
		adm = 5
		dip = 5
		mil = 6
	}
}

#Barakzai Dynasty

1823.1.1 = {
	monarch = {
		name = "Yar Muhammad Khan ibn Painda Khan"
		dynasty = Barakzai
		adm = 3
		dip = 3
		mil = 3
	}
}
