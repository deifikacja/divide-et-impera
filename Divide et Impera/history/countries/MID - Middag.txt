government = tribal_despotism
aristocracy_plutocracy = -4
centralization_decentralization = 0
innovative_narrowminded = 1
mercantilism_freetrade = -1
imperialism_isolationism = 0
secularism_theocracy = 1
offensive_defensive = 2
land_naval = 0
quality_quantity = 0
serfdom_freesubjects = 1
technology_group = chinese
religion = animism
primary_culture = taiwanese
capital = 1364	


1540.1.1 = {
	monarch = {
		name = "Lelian"
		adm = 5
		dip = 4
		mil = 3
	}
}

1600.1.1 = {
	monarch = {
		name = "Dorida Camachat"
		adm = 5
		dip = 6
		mil = 4
	}
}

1648.1.1 = {
	monarch = {
		name = "Camachat Maloe"
		adm = 4
		dip = 7
		mil = 6
	}
}