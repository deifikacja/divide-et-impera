government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -1
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = -1
land_naval = -3
quality_quantity = 2
serfdom_freesubjects = -5
technology_group = eastern
primary_culture = russian #russified tartar dynasty
add_accepted_culture = tartar
religion = orthodox
capital = 301	# Kasimov

 

#Princes of Gorodets-Meschersky
1356.1.1 = {
	monarch = {
		name = "Dmitri"
		dynasty = Naruchad
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1356.1.1 = {
	heir = {
		name = "Grigorij"
		monarch_name = "Grigorij"
		dynasty = Naruchad
		birth_date = 1356.1.1
		death_date = 1392.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1376.1.1 = {
	monarch = {
		name = "Grigorij"
		dynasty = Naruchad
		DIP = 5
		ADM = 5
		MIL = 5
	}
}
#To Muscovy from 1393