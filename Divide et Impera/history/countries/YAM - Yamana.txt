government = daimyo
aristocracy_plutocracy = -4
centralization_decentralization = 3
innovative_narrowminded = 2
mercantilism_freetrade = -5
offensive_defensive = -2
imperialism_isolationism = 0
secularism_theocracy = -1
land_naval = 0
quality_quantity = 4
serfdom_freesubjects = -5
primary_culture = japanese
religion = shinto
technology_group = chinese
capital = 1353	# Tajima
daimyo = yes


1356.1.1 = { 
monarch = { 
name = "Tokiuji" 
dynasty = Yamana 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Yamana Tokiuji" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1356.1.1 = {
    heir = {
        name = "Tsunehisa"
        monarch_name =  "Tsunehisa" 
        dynasty =  Yamana 
        birth_date = 1351.1.1
        death_date = 1404.1.1
        claim = 83
        adm = 4
        dip = 4
        mil = 6
        leader = {
            name =  "Tsunehisa" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1372.1.1 = { 
monarch = { 
name = "Tsunehisa" 
dynasty = Yamana 
adm = 4 
dip = 4 
mil = 6 
leader = { name = "Yamana Tsunehisa" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1372.1.1 = {
    heir = {
        name = "Sozen"
        monarch_name =  "Sozen" 
        dynasty =  Yamana 
        birth_date = 1364.1.1
        death_date = 1473.1.1
        claim = 76
        adm = 3
        dip = 3
        mil = 3
        leader = {
            name =  "Sozen" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1404.1.1 = { 
monarch = { 
name = "Sozen" 
dynasty = Yamana 
adm = 3 
dip = 3 
mil = 3 
leader = { name = "Yamana Sozen" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1404.1.1 = {
    heir = {
        name = "Koretoyo"
        monarch_name =  "Koretoyo" 
        dynasty =  Yamana 
        birth_date = 1398.1.1
        death_date = 1548.1.1
        claim = 79
        adm = 5
        dip = 7
        mil = 5
        leader = {
            name =  "Koretoyo" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1473.1.1 = { 
monarch = { 
name = "Koretoyo" 
dynasty = Yamana 
adm = 5 
dip = 7 
mil = 5 
leader = { name = "Yamana Koretoyo" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

1473.1.1 = {
    heir = {
        name = "Toyokuni"
        monarch_name =  "Toyokuni" 
        dynasty =  Yamana 
        birth_date = 1473.1.1
        death_date = 1478.1.1
        claim = 85
        adm = 5
        dip = 5
        mil = 3
        leader = {
            name =  "Toyokuni" 
            type = general
            rank = 0
            fire = 1
            shock = 3
            manuever = 3
            siege = 1
        }
   }
}


1548.1.1 = { 
monarch = { 
name = "Toyokuni" 
dynasty = Yamana 
adm = 5 
dip = 5 
mil = 3 
leader = { name = "Yamana Toyokuni" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 

