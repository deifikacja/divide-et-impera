government = eastern_despotism
aristocracy_plutocracy = -2
centralization_decentralization = 5
innovative_narrowminded = 3
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 0
land_naval = 3
quality_quantity = 3
serfdom_freesubjects = -2
primary_culture = malayan
religion = sunni
technology_group = chinese
capital = 617	# Banda Aceh

1356.1.1 = {
	monarch = { 
		name = "Inayat Shah"
		dynasty = "Mug�yat"
		adm = 3
		dip = 3
		mil = 4
	}
}
1356.1.1 = {
	heir = {
		name = "Muzaffar Shah"
		monarch_name = "Muzaffar Shah"
		dynasty = "Mug�yat"
		birth_date = 1356.1.1
		death_date = 1460.1.1
		claim = 95
		adm = 5
		dip = 4
		mil = 4
	}
}
1442.1.1 = {
	monarch = { 
		name = "Muzaffar Shah"
		dynasty = "Mug�yat"
		adm = 5
		dip = 4
		mil = 4
	}
}
1442.1.1 = { centralization_decentralization = 4 innovative_narrowminded = 2 } #Sultanat founded
1442.1.1 = {
	heir = {
		name = "Shams"
		monarch_name = "Shams ad-Din Shah"
		dynasty = "Mug�yat"
		birth_date = 1442.1.1
		death_date = 1495.1.1
		claim = 95
		adm = 5
		dip = 4
		mil = 4
	}
}
1460.1.1 = {
	monarch = { 
		name = "Shams ad-Din Shah"
		dynasty = "Mug�yat"
		adm = 5
		dip = 4
		mil = 4
	}
}
1460.1.1 = {
	heir = {
		name = "'Al�"
		monarch_name = "'Al� Sh�h"
		dynasty = "Mug�yat"
		birth_date = 1460.1.1
		death_date = 1528.1.1
		claim = 95
		adm = 6
		dip = 6
		mil = 6
	}
}
1495.1.1 = {
	monarch = {
		name = "'Al� Sh�h"
		dynasty = "Mug�yat"
		adm = 6
		dip = 6
		mil = 6
	}
}
1495.1.1 = {
	heir = {
		name = "Sal�h ad-D�n"
		monarch_name = "Sal�h ad-D�n Ibr�h�m"
		dynasty = "Mug�yat"
		birth_date = 1495.1.1
		death_date = 1537.1.1
		claim = 95
		adm = 4
		dip = 5
		mil = 5
	}
}
1528.1.1 = {
	monarch = {
		name = "Sal�h ad-D�n Ibr�h�m"
		dynasty = "Mug�yat"
		adm = 4
		dip = 4
		mil = 5
	}
}
1528.1.1 = {
	heir = {
		name = "'Al� ad-D�n"
		monarch_name = "'Al� ad-D�n al-Qahhar"
		dynasty = "Mug�yat"
		birth_date = 1528.1.1
		death_date = 1568.1.1
		claim = 95
		adm = 8
		dip = 6
		mil = 4
	}
}
1537.1.1 = {
	monarch = {
		name = "'Al� ad-D�n al-Qahhar"
		dynasty = "Mug�yat"
		adm = 8
		dip = 6
		mil = 4
	}
}
1537.1.1 = {
	heir = {
		name = "Husayn 'Al�"
		monarch_name = "Husayn 'Al� Ri'�yat Sh�h"
		dynasty = "Mug�yat"
		birth_date = 1537.1.1
		death_date = 1575.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}
1568.1.1 = {
	monarch = {
		name = "Husayn 'Al� Ri'�yat Sh�h"
		dynasty = "Mug�yat"
		adm = 5
		dip = 5
		mil = 5
	}
}
1568.1.1 = {
	heir = {
		name = "Sult�n Sri"
		monarch_name = "Sult�n Sri '�lam"
		dynasty = "Mug�yat"
		birth_date = 1568.1.1
		death_date = 1576.1.1
		claim = 95
		adm = 3
		dip = 3
		mil = 3
	}
}
1575.1.1 = {
	monarch = {
		name = "Sult�n Sri '�lam"
		dynasty = "Mug�yat"
		adm = 3
		dip = 3
		mil = 3
	}
}
1575.1.1 = {
	heir = {
		name = "Zayn"
		monarch_name = "Zayn al '�bid�n"
		dynasty = "Mug�yat"
		birth_date = 1568.1.1
		death_date = 1577.1.1
		claim = 95
		adm = 3
		dip = 3
		mil = 3
	}
}
1576.1.1 = {
	monarch = {
		name = "Zayn al '�bid�n"
		dynasty = "Mug�yat"
		adm = 3
		dip = 3
		mil = 3
	}
}
1576.1.1 = {
	heir = {
		name = "'Al�' ad-D�n"
		monarch_name = "'Al�' ad-D�n Mans�r Sh�h"
		dynasty = "Mug�yat"
		birth_date = 1576.1.1
		death_date = 1585.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 4
	}
}
1577.1.1 = {
	monarch = {
		name = "'Al�' ad-D�n Mans�r Sh�h"
		dynasty = "Mug�yat"
		adm = 5
		dip = 5
		mil = 4
	}
}
1577.1.1 = {
	heir = {
		name = "'Al� Ri'�yat"
		monarch_name = "'Al� Ri'�yat Sh�h"
		dynasty = "Mug�yat"
		birth_date = 1577.1.1
		death_date = 1588.1.1
		claim = 95
		adm = 5
		dip = 6
		mil = 4
	}
}
1585.1.1 = {
	monarch = {
		name = "'Al� Ri'�yat Sh�h"
		dynasty = "Mug�yat"
		adm = 5
		dip = 6
		mil = 4
	}
}
1585.1.1 = {
	heir = {
		name = "Buyong"
		monarch_name = "Sult�n Buyong"
		dynasty = "Mug�yat"
		birth_date = 1585.1.1
		death_date = 1596.1.1
		claim = 95
		adm = 6
		dip = 4
		mil = 4
	}
}
1588.1.1 = {
	monarch = {
		name = "Sult�n Buyong"
		dynasty = "Mug�yat"
		adm = 6
		dip = 4
		mil = 4
	}
}
1588.1.1 = {
	heir = {
		name = "'Al�' ad-D�n"
		monarch_name = "'Al�' ad-D�n Ri'�yat Sh�h"
		dynasty = "Mug�yat"
		birth_date = 1588.1.1
		death_date = 1604.1.1
		claim = 95
		adm = 6
		dip = 4
		mil = 4
	}
}
1596.1.1 = {
	monarch = {
		name = "'Al�' ad-D�n Ri'�yat Sh�h"
		dynasty = "Mug�yat"
		adm = 4
		dip = 4
		mil = 4
	}
}
1596.1.1 = {
	heir = {
		name = "'Al� Mug�t"
		monarch_name = "'Al� Mug�t Sh�h"
		dynasty = "Mug�yat"
		birth_date = 1596.1.1
		death_date = 1607.1.1
		claim = 95
		adm = 6
		dip = 4
		mil = 4
	}
}
1604.1.1 = {
	monarch = {
		name = "'Al� Mug�t Sh�h"
		dynasty = "Mug�yat"
		adm = 3
		dip = 3
		mil = 3
	}
}
1604.1.1 = {
	heir = {
		name = "Iskandar"
		monarch_name = "Iskandar Sh�h"
		dynasty = "Mug�yat"
		birth_date = 1604.1.1
		death_date = 1636.1.1
		claim = 95
		adm = 6
		dip = 4
		mil = 4
	}
}
1607.1.1 = {
	monarch = {
		name = "Iskandar Sh�h"
		dynasty = "Mug�yat"
		adm = 7
		dip = 5
		mil = 5
	}
}
1607.1.1 = {
	heir = {
		name = "Iskandar Tan� 'Al� ad-D�n"
		monarch_name = "Iskandar Tan� 'Al� ad-D�n"
		dynasty = "Mug�yat"
		birth_date = 1600.1.1
		death_date = 1641.1.1
		claim = 95
		adm = 3
		dip = 4
		mil = 4
	}
}

1636.1.1 = {
	monarch = {
		name = "Iskandar Tan� 'Al� ad-D�n"
		dynasty = "Mug�yat"
		adm = 3
		dip = 4
		mil = 4
	}
}

1636.1.1 = {
	heir = {
		name = "Saf�yat ad-D�n T�j"
		monarch_name = "Saf�yat ad-D�n T�j"
		dynasty = "Mug�yat"
		birth_date = 1630.1.1
		death_date = 1675.1.1
		claim = 95
		adm = 6
		dip = 6
		mil = 5
		female = yes
	}
}

1641.1.1 = {
	monarch = {
		name = "Saf�yat ad-D�n T�j"
		dynasty = "Mug�yat"
		adm = 6
		dip = 6
		mil = 5
		female = yes
	}
}

1650.1.1 = {
	heir = {
		name = "Neq�yat ad-D�n N�r"
		monarch_name = "Neq�yat ad-D�n N�r"
		dynasty = "Mug�yat"
		birth_date = 1650.1.1
		death_date = 1678.1.1
		claim = 95
		adm = 3
		dip = 3
		mil = 4
		female = yes
	}
}

1675.1.1 = {
	monarch = {
		name = "Neq�yat ad-D�n N�r"
		dynasty = "Mug�yat"
		adm = 3
		dip = 3
		mil = 4
		female = yes
	}
}

1675.1.1 = {
	heir = {
		name = "Zaq�yat ad-D�n 'In�ya"
		monarch_name = "Zaq�yat ad-D�n 'In�ya"
		dynasty = "Mug�yat"
		birth_date = 1650.1.1
		death_date = 1688.1.1
		claim = 95
		adm = 5
		dip = 4
		mil = 4
		female = yes
	}
}

1678.1.1 = {
	monarch = {
		name = "Zaq�yat ad-D�n 'In�ya"
		dynasty = "Mug�yat"
		adm = 5
		dip = 4
		mil = 4
		female = yes
	}
}

1678.1.1 = {
	heir = {
		name = "Zirat ad-D�n Kam�lat Sh�h"
		monarch_name = "Zirat ad-D�n Kam�lat Sh�h"
		dynasty = "Mug�yat"
		birth_date = 1660.1.1
		death_date = 1699.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 4
		female = yes
	}
}

1688.1.1 = {
	monarch = {
		name = "Zirat ad-D�n Kam�lat Sh�h"
		dynasty = "Mug�yat"
		adm = 5
		dip = 5
		mil = 4
		female = yes
	}
}

1688.1.1 = {
	heir = {
		name = "Badr ad-D�n Shar�f H�shim"
		monarch_name = "Badr ad-D�n Shar�f H�shim"
		dynasty = "Mug�yat"
		birth_date = 1688.1.1
		death_date = 1702.1.1
		claim = 95
		adm = 4
		dip = 3
		mil = 3
	}
}

1699.1.1 = {
	monarch = {
		name = "Badr ad-D�n Shar�f H�shim"
		dynasty = "Mug�yat"
		adm = 4
		dip = 3
		mil = 3
	}
}

1702.1.1 = {
	monarch = {
		name = "Perkara '�lam Shar�f"
		dynasty = "Mug�yat"
		adm = 3
		dip = 3
		mil = 3
	}
}

1703.1.1 = {
	monarch = {
		name = "Jam�l al-'�lam Badr"
		dynasty = "Mug�yat"
		adm = 6
		dip = 5
		mil = 4
	}
}

1703.1.1 = {
	heir = {
		name = "Shams al'-�lam"
		monarch_name = "Shams al'-�lam"
		dynasty = "Mug�yat"
		birth_date = 1700.1.1
		death_date = 1727.1.1
		claim = 95
		adm = 3
		dip = 3
		mil = 3
	}
}

1726.1.1 = {
	monarch = {
		name = "Shams al'-�lam"
		dynasty = "Mug�yat"
		adm = 3
		dip = 3
		mil = 3
	}
}

1726.1.1 = {
	heir = {
		name = "'Al�' ad-D�n Ahmad Sh�h"
		monarch_name = "'Al�' ad-D�n Ahmad Sh�h"
		dynasty = "Mug�yat"
		birth_date = 1700.1.1
		death_date = 1735.1.1
		claim = 95
		adm = 4
		dip = 4
		mil = 3
	}
}
		

1727.1.1 = {
	monarch = {
		name = "'Al�' ad-D�n Ahmad Sh�h"
		dynasty = "Mug�yat"
		adm = 4
		dip = 4
		mil = 3
	}
}

1727.1.1 = {
	heir = {
		name = "'Al�' ad-D�n Sh�h Jah�n"
		monarch_name = "'Al�' ad-D�n Sh�h Jah�n"
		dynasty = "Mug�yat"
		birth_date = 1710.1.1
		death_date = 1760.1.1
		claim = 95
		adm = 7
		dip = 6
		mil = 6
	}
}

1735.1.1 = {
	monarch = {
		name = "'Al�' ad-D�n Sh�h Jah�n"
		dynasty = "Mug�yat"
		adm = 7
		dip = 6
		mil = 6
	}
}

1735.1.1 = {
	heir = {
		name = "Mahm�d Sh�h"
		monarch_name = "Mahm�d Sh�h I"
		dynasty = "Mug�yat"
		birth_date = 1735.1.1
		death_date = 1781.1.1
		claim = 95
		adm = 7
		dip = 5
		mil = 6
	}
}

1760.1.1 = {
	monarch = {
		name = "Mahm�d Sh�h I"
		dynasty = "Mug�yat"
		adm = 7
		dip = 5
		mil = 6
	}
}

1760.1.1 = {
	heir = {
		name = "'Al�' ad-D�n Muhammad"
		monarch_name = "'Al�' ad-D�n Muhammad"
		dynasty = "Mug�yat"
		birth_date = 1760.1.1
		death_date = 1795.1.1
		claim = 95
		adm = 6
		dip = 5
		mil = 4
	}
}

1781.1.1 = {
	monarch = {
		name = "'Al�' ad-D�n Muhammad"
		dynasty = "Mug�yat"
		adm = 6
		dip = 5
		mil = 4
	}
}

1781.1.1 = {
	heir = {
		name = "'Al�' ad-D�n Jauhar"
		monarch_name = "'Al�' ad-D�n Jauhar"
		dynasty = "Mug�yat"
		birth_date = 1770.1.1
		death_date = 1815.1.1
		claim = 95
		adm = 4
		dip = 6
		mil = 5
	}
}

1795.1.1 = {
	monarch = {
		name = "'Al�' ad-D�n Jauhar"
		dynasty = "Mug�yat"
		adm = 4
		dip = 6
		mil = 5
	}
}

1795.1.1 = {
	heir = {
		name = "Syarif Saif ul Alam"
		monarch_name = "Syarif Saif ul Alam"
		dynasty = "Mug�yat"
		birth_date = 1795.1.1
		death_date = 1818.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1815.1.1 = {
	monarch = {
		name = "Syarif Saif ul Alam"
		dynasty = "Mug�yat"
		adm = 5
		dip = 5
		mil = 5
	}
}

1815.1.1 = {
	heir = {
		name = "'Al�' ad-D�n Jauhar"
		monarch_name = "'Al�' ad-D�n Jauhar"
		dynasty = "Mug�yat"
		birth_date = 1800.1.1
		death_date = 1824.1.1
		claim = 95
		adm = 4
		dip = 6
		mil = 5
	}
}

1818.1.1 = {
	monarch = {
		name = "'Al�' ad-D�n Jauhar"
		dynasty = "Mug�yat"
		adm = 4
		dip = 6
		mil = 5
	}
}
