government = daimyo
aristocracy_plutocracy = -4
centralization_decentralization = 4
innovative_narrowminded = 2
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = -1
offensive_defensive = 0
land_naval = 0
quality_quantity = -2
serfdom_freesubjects = -5
primary_culture = japanese
religion = shinto
technology_group = chinese
capital = 1013	# Hizen
daimyo = yes

1537.1.1 = { 
monarch = { 
name = "Naoshige" 
dynasty = Nabeshima 
adm = 5 
dip = 6 
mil = 8 
} 
} 

1537.1.1 = {
    heir = {
        name = "Katsushige"
        monarch_name =  "Katsushige" 
        dynasty =  Nabeshima 
        birth_date = 1529.1.1
        death_date = 1657.1.1
        claim = 95
        adm = 5
        dip = 6
        mil = 8
   }
}


1619.1.1 = { 
monarch = { 
name = "Katsushige" 
dynasty = Nabeshima 
adm = 5 
dip = 6 
mil = 8 
} 
} 

1619.1.1 = {
    heir = {
        name = "Motoshige"
        monarch_name =  "Motoshige" 
        dynasty =  Nabeshima 
        birth_date = 1613.1.1
        death_date = 1814.1.1
        claim = 76
        adm = 5
        dip = 6
        mil = 8
   }
}


1657.1.1 = { 
monarch = { 
name = "Motoshige" 
dynasty = Nabeshima 
adm = 5 
dip = 6 
mil = 8 
} 
} 

1657.1.1 = {
    heir = {
        name = "Naomasa"
        monarch_name =  "Naomasa" 
        dynasty =  Nabeshima 
        birth_date = 1646.1.1
        death_date = 1666.1.1
        claim = 87
        adm = 5
        dip = 6
        mil = 8
   }
}


1814.1.1 = { 
monarch = { 
name = "Naomasa" 
dynasty = Nabeshima 
adm = 5 
dip = 6 
mil = 8 
} 
} 

