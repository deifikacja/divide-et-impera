government = feudal_monarchy
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -3
imperialism_isolationism = 1
secularism_theocracy = 0
offensive_defensive = 1
land_naval = -3
quality_quantity = 0
serfdom_freesubjects = -3
technology_group = western
primary_culture = occitain
religion = catholic
capital = 2185	# Valentinois

 

1345.1.1 = {
	monarch = {
		name = "Aimar VI"
		dynasty = "de Valentinois"
		adm = 5
		dip = 6
		mil = 4
	}
}

1356.1.1 = {
	heir = {
		name = "Louis"
		monarch_name = "Louis II"
		dynasty = "de Valentinois"
		birth_date = 1356.1.1
		death_date = 1404.1.1
		claim = 90
		adm = 3
		dip = 4
		mil = 6
	}
}

1373.1.1 = {
	monarch = {
		name = "Louis II"
		dynasty = "de Valentinois"
		adm = 3
		dip = 4
		mil = 6
	}
}
#To France, title as an appanage