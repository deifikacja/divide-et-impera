government = steppe_horde
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -3
offensive_defensive = -2
imperialism_isolationism = -2
secularism_theocracy = 0
land_naval = -2
quality_quantity = 3
serfdom_freesubjects = -5
primary_culture = tunguz
religion = shamanism
technology_group = chinese
capital = 1049	# Shenyang


1368.1.1 = {
	monarch = {
		name = "Wanyan"
		dynasty = "Yeren"
		adm = 3
		dip = 3
		mil = 5
	}
}