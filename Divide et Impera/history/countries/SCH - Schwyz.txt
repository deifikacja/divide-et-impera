government = administrative_republic
aristocracy_plutocracy = 4
centralization_decentralization = 5
innovative_narrowminded = 0
mercantilism_freetrade = 0
imperialism_isolationism = 1
secularism_theocracy = -1
offensive_defensive = 3
land_naval = -5
quality_quantity = -2
serfdom_freesubjects = 4
technology_group = western
religion = catholic
primary_culture = rheinlaender
capital = 166 # Schwyz

