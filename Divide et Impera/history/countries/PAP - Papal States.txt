government = papal_government
aristocracy_plutocracy = -5
centralization_decentralization = 1
innovative_narrowminded = 1
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 5
offensive_defensive = 4
land_naval = 0
quality_quantity = 0
serfdom_freesubjects = -2
primary_culture = umbrian
religion = catholic
technology_group = western
capital = 202	# Avignon Papacy

1000.1.1 = { set_country_flag = idea_patron_of_art }
1000.1.1 = { set_country_flag = idea_church_attendance_duty }
1000.1.1 = { set_country_flag = idea_cabinet }

1000.1.1 = { set_country_flag = inf_country } #Magna Mundi
1000.1.1 = { set_country_flag = Roman_Catholic }
1000.1.1 = { set_country_flag = vote_for_Roman_pope }
1000.1.1 = { set_country_flag = curia_controller }

1352.12.18 = {
	monarch = {
		name = "Innocentius VI"
		adm = 7
		dip = 7
		mil = 6
	}
}

1362.9.28 = {
	monarch = {
		name = "Urbanus V"
		adm = 5
		dip = 5
		mil = 3
	}
}

1370.12.30 = {
	monarch = {
		name = "Gregorius XI"
		adm = 5
		dip = 6
		mil = 3
	}
}

1377.1.1 = { capital = 118 } #Pope come back to Rome

1378.4.8 = {
	monarch = {
		name = "Urbanus VI"
		adm = 3
		dip = 3
		mil = 3
	}
}

1378.9.20 = { centralization_decentralization = 3 set_country_flag = church_schism } # West Schism

1389.11.2 = {
	monarch = {
		name = "Bonifacius IX"
		adm = 3
		dip = 3
		mil = 3
	}
}

1399.1.1 = { decision = blasphemy_act }

1404.10.17 = {
	monarch = {
		name = "Innocentius VII"
		adm = 4
		dip = 3
		mil = 3
	}
}

1406.11.30 = {
	monarch = {
		name = "Gregorius XII"
		adm = 3
		dip = 4
		mil = 3
	}
}

1415.7.4 = {
	monarch = {
		name = "Vacant"
		adm = 3
		dip = 3
		mil = 3
	}
}

1417.10.18 = { centralization_decentralization = -3 clr_country_flag = church_schism } # The end of the West Schism 

1417.11.11 = {
	monarch = {
		name = "Martin V"
		adm = 7
		dip = 6
		mil = 5
	}
}

1431.3.3 = {
	monarch = {
		name = "Eugenius IV"
		adm = 4
		dip = 4
		mil = 4
	}
}

1447.2.23 = {
	monarch = {
		name = "Nicolaus V" #A competent pope and famed patron of the arts
		adm = 7
		dip = 6
		mil = 4
	}
	set_country_flag = reformer_pope
}

1455.3.25 = {
	monarch = {
		name = "Callistus III"
		adm = 4
		dip = 5
		mil = 5
	}
	clr_country_flag = reformer_pope
	set_country_flag = secular_pope
}

1458.8.7 = {
	monarch = {
		name = "Pius II"
		adm = 7
		dip = 4
		mil = 4
	}
}

1460.1.1 = { set_country_flag = start_after_1460 }

1464.8.16 = {
	monarch = {
		name = "Paulus II" #Attempted many reforms but achieved few
		adm = 7 
		dip = 5 #Was 7 - excommunicated Bohemia's king but failed to achieve much
		mil = 4
	}
	clr_country_flag = secular_pope
	set_country_flag = reformer_pope
}

1469.1.1 = { set_country_flag = byzantine_princess }

1470.1.1 = { set_country_flag = start_after_1470 }

1471.7.27 = {
	monarch = {
		name = "Xystus IV" #Sixtus IV, patron of the arts and sciences, incredible administrator, founded museums, nepotist
		adm = 9 #Was 7 - famed for rebuilding Rome, founding museums, churches, aquaducts, etc.
		dip = 7 #Couldn't convince France to drop the Pragmatic Sanction, Italian princes united against him
		mil = 6 #Fought several wars in Italy
	}
	clr_country_flag = reformer_pope
	set_country_flag = secular_pope
	set_country_flag = swiss_mercenaries #Relationship began sometime in Sixtus IV's reign
}

1480.1.1 = { set_country_flag = start_after_1480 }

1484.8.13 = {
	monarch = {
		name = "Innocentius VIII" #lavishly nepotistic, had multiple children, abused excommunication
		adm = 4
		dip = 7 #excommunicated Ferdinand, invited France to take Naples
		mil = 4 #called a crusade but also made deals with the Sultan
	}
	clr_country_flag = secular_pope
	set_country_flag = corrupt_pope
}

#1490.1.1 = { leader = {	name = "d'Este"                	type = general	rank = 0	fire = 1	shock = 0	manuever = 2	siege = 1}}

1490.1.1 = { set_country_flag = start_after_1490 }

1492.1.1 = { set_country_flag = america1 set_country_flag = america2 }

1492.7.26 = {
	monarch = {
		name = "Alexander VI" #Lived an openly debased life, accused nobles of crimes to confiscate lands, sold offices, nepotistic
		adm = 3 #Was 5 - could care less about Rome, but did try to retake Romagna
		dip = 4 #Was 6 - made many enemies but was able to bribe his way out of being deposed
		mil = 5 #Fought wars but lost them
	}
}

1494.6.7 = { set_country_flag = tordesillas } #Treaty of Tordesillas

1497.1.1 = { leader = {	name = "Cesare Borgia"             	type = general	rank = 0	fire = 2	shock = 1	manuever = 2	siege = 3	death_date = 1503.11.1 } }

1500.1.1 = { set_country_flag = start_after_1500 }

1503.8.19 = {
	monarch = {
		name = "Pius III" #Elected after Borgias forced to leave Rome, attempted to reform court & arrest Borgia, possibly murdered
		adm = 7 #Was 5 - he was very effective in the 26 days he lived
		dip = 5
		mil = 5 #Was 4 - he took a hard line that could have led to war
	}
	clr_country_flag = corrupt_pope
	set_country_flag = reformer_pope
}

1503.10.19 = {
	monarch = {
		name = "Iulius II"#Julius II, probably bribed his way to the papacy, morally indifferent, strategist, politician
		adm = 6 #Was 4 - Improved & fortified Rome, began St. Peter's Basilica
		dip = 7 #Considered a crucial ally of France and the Emperor, founded the League of Cambrai against Venice and the Holy 
		mil = 6 #Was 5 - led sieges personally, freed Bologna and Perugia
		leader = {	name = "Iulius II"             	type = general	rank = 0	fire = 2	shock = 1	manuever = 2	siege = 1}
	}
	clr_country_flag = reformer_pope
	set_country_flag = corrupt_pope
}

1506.1.2 = { set_country_flag = swiss_guard }

1510.1.1 = { set_country_flag = start_after_1510 }

1510.1.1 = { set_country_flag = third_rome_exists }

1511.1.1 = { set_country_flag = church_synod } #the Pisan Schism begins

1511.7.18 = { set_country_flag = church_council } #5th Lateran Council called

1513.2.22 = {
	monarch = {
		name = "Leo X" #A liberal, not a priest before election, practiced nepotism, did try some reforms
		adm = 3 #Was 5 - pleasure-loving, spend far more than income, borrowed heavily, pawned furniture, died in debt
		dip = 4 #Constant political intrigue, Spain felt he betrayed them, but did survive the 5th Lateran Council
		mil = 4 #Lost a very costly war
	}
	clr_country_flag = corrupt_pope
	set_country_flag = secular_pope
}

1513.12.19 = { clr_country_flag = church_synod } #Healing of the Pisan Schism

1516.1.1 = { set_country_flag = new_latin_bible } #Textus Receptus (Erasmus)

1517.1.1 = { set_country_flag = indulgences }
1517.1.1 = { set_country_flag = papal_infallibility } #Lateran council condemns conciliarism
1517.1.1 = { innovative_narrowminded = 0 }

1517.3.16 = { clr_country_flag = church_council } #5th Lateran council ends

1520.1.1 = { set_country_flag = start_after_1520 }

1522.1.1 = {
	monarch = {
		name = "Hadrianus VI" #Adrian VI, Charles V's tutor (but neutral), a holy man, tried to launch a Catholic Reformation
		adm = 7 #Was 6 - very austere monk, cut costs drastically, tried to reform church (indulgences, matrimonial dispensations)
		dip = 6 #Was 3 - Made enemies by reforming, stayed officially neutral, but adored by Charles V
		mil = 3 #Pacifist
	}
	clr_country_flag = secular_pope
	set_country_flag = reformer_pope
}

1523.9.15 = {
	monarch = {
		name = "Clemens VII" #Clement VII, indifferent to the Reformation, ignored calls for a general council
		adm = 5 
		dip = 5
		mil = 3 #Was 4 - surrendered most of the Papal States when Rome was sacked
	}
	clr_country_flag = reformer_pope
	set_country_flag = secular_pope
}

1524.1.1 = { set_country_flag = peasants1 } #Beginning of the Peasants War

1525.1.1 = { set_country_flag = first_reformation } #Prussia was first to openly break from Rome

1526.1.1 = { set_country_flag = peasants2 } #End of the Peasants War

1527.5.6 = { set_country_flag = sack_of_rome } #Rome sacked by mercenaries in the employ of the Hapsburg emperor

1530.1.1 = { set_country_flag = start_after_1530 }

1534.9.26 = {
	monarch = {
		name = "Paulus III" #Paul III, called the Council of Trent, recognized Jesuits, reformed Inquisition, but also nepotist
		adm = 5 
		dip = 6 #Was 3 - called the council of Trent, got widespread church support for it
		mil = 4
	}
	clr_country_flag = secular_pope
	set_country_flag = reformer_pope
}

1537.5.29 = { set_country_flag = sublimus_dei } #Papal Bull Sublimus Dei banned enslavement of natives

1540.1.1 = { innovative_narrowminded = 3 } # The Foundation of Societas Jesu

1540.1.1 = { set_country_flag = start_after_1540 }

1540.9.27 = { 
  set_country_flag = jesuits_founded
  set_country_flag = jesuit_influence
  set_country_flag = jesuit_influence_moderate
  set_country_flag = jesuit_influence_strong
  innovative_narrowminded = 1
  }

1542.1.1 = { innovative_narrowminded = 2 } # Reorganisation of the Holy Inquisition

1545.12.13 = { set_global_flag = counter_reformation }	# Council of Trent

1545.12.13 = { 
	set_country_flag = church_council #The Council of Trent 1st session
	set_country_flag = church_council_called
	}

1546.4.8 = { 
  set_country_flag = tradition_counts #Decree on Scripture
  innovative_narrowminded = 3
  }

1547.1.13 = { set_country_flag = good_works_needed } #Decree on Justification

1549.9.17 = { 
	clr_country_flag = church_council #The Council of Trent is indefinitely postponed
	set_country_flag = clerical_residence
	}

1550.1.1 = { leader = {	name = "M. Colonna"               	type = admiral	rank = 0	fire = 3	shock = 3	manuever = 2	siege = 0	death_date = 1577.1.1 } }

1550.1.1 = {
	monarch = {
		name = "Iulius III" #Julius III, reopened the council of Trent, otherwise did very little of note
		adm = 5
		dip = 4 #Was 6 - Shocked many by appointing a 17-year-old boy a Cardinal, possibly a pederast, had a luxurious mansion
		mil = 3
	}
	clr_country_flag = reformer_pope
	set_country_flag = secular_pope
	set_country_flag = start_after_1550
}

1551.5.1 = { 
	set_country_flag = church_council #The Council of Trent 2nd session
	}

1552.4.28 = { 
	clr_country_flag = church_council #The Council of Trent is broken up by advancing Saxon troops
	}

1555.3.24 = {
	monarch = {
		name = "Marcellus II" #Tried to reform the church (died of illness and exhaustion)
		adm = 6 #Reputation for ability
		dip = 6 #Was 3 - reputation for tact and integrity
		mil = 3
	}
	clr_country_flag = secular_pope
	set_country_flag = reformer_pope
}

1555.5.2 = {
	monarch = {
		name = "Paulus IV" #Paul IV, highly nationalistic, hated the emperor, anti-semitic, nepotistic
		adm = 4
		dip = 3 #Was 4 - openly opposed the emperor, filled with hate
		mil = 5 #Was 4 - very aggressive
	}
	clr_country_flag = reformer_pope
	set_country_flag = secular_pope
}

1557.1.1 = { set_country_flag = forbidden_index }
1557.1.1 = { innovative_narrowminded = 4 }

1559.8.19 = {
	monarch = {
		name = "Pius IV" #Reopened Trent, Began the Tridentine Creed, major reformer
		adm = 6 #Was 3 - Major church reforms
		dip = 6 #Was 4 - granted the use of the cup to the laity of Bohemia and Austria (pretty savvy compromise)
		mil = 4
	}
	clr_country_flag = secular_pope
	set_country_flag = reformer_pope
}

1560.1.1 = { set_country_flag = start_after_1560 }

1562.1.18 = { set_country_flag = church_council } #The Council of Trent 3rd session begins

1562.6.16 = { set_country_flag = elite_priests } #Decree on the Eucharist

1562.9.9 = { set_country_flag = latin_only } #Decree on Standardizing Masses

1563.12.4 = { 
	clr_country_flag = church_council #The Council of Trent ends
	clr_country_flag = church_council_called
	set_country_flag = counter_reformation
	set_country_flag = seminaries
  innovative_narrowminded = 5
	}

1565.12.10 = {
	monarch = {
		name = "Pius V" #Saint Pius, a major reformer and Dominican monk, standardized the Tridentine mass
		adm = 8 #Was 5 - totally reorganized and reformed the Papal Court and Bishoprics (compelled residence), regulated Rome
		dip = 3 #Made many enemies among the Cardinals & clergy
		mil = 4
	}
}

1567.1.1 = { set_country_flag = indulgences_banned }

1570.1.1 = { set_country_flag = start_after_1570 }

1572.5.2 = {
	monarch = {
		name = "Gregorius XIII" #Gregory XIII, brilliant, determined reformer, elected in under 24 hours!, famous for Calendar reform
		adm = 6 #Was 5 - Confiscated property in papal states creating tension and anarchy, decreased power of cardinals considerably
		dip = 4 #Was 5 - elected with Spanish backing, supported the Spanish armada against Protestant England, created many enemies at home
		mil = 4 #Supported two badly planned Irish rebellions against England
	}
}

1580.1.1 = { set_country_flag = start_after_1580 }

1585.4.11 = {
	monarch = {
		name = "Xystus V" #Sixtus V, an enemy of Gregory XIII elected to deal with his mess
		adm = 6 #An amazing amount of public works created in his pontificate, raised massive sums and became solvent again
		dip = 6
		mil = 5 #Was 3 - Brought thousands of brigands to justice and made the realm safe again
	}
	set_country_flag = secular_pope
	clr_country_flag = reformer_pope
}

1590.1.1 = { set_country_flag = start_after_1590 }

1590.8.28 = {
	monarch = {
		name = "Urbanus VII" #Pope for a mere 13 days before his death
		adm = 4 #Was 8 - Banned smoking
		dip = 4 #Was 6 - Elected with Spanish support
		mil = 4
	}
}

1590.9.28 = {
	monarch = {
		name = "Gregorius XIV" #Gregory XIV, sickly throughout his papacy, elected with Spanish force
		adm = 4 #Was 6 - the future Innocent IX ruled while Gregory suffered from Malaria
		dip = 6 #Freed all Filipino slaves
		mil = 4 #Raised an army to fight the Huguenots
	}
	clr_country_flag = secular_pope
	set_country_flag = reformer_pope
}

1591.10.17 = {
	monarch = {
		name = "Innocentius IX" #Innocent IX, pope for only a few months
		adm = 6 #Was 4 - experienced administrator
		dip = 4 #Elected with Spanish support
		mil = 5 #Was 4 - Papal army fought in the French Wars of Religion
	}
	set_country_flag = secular_pope
	clr_country_flag = reformer_pope
}

1592.1.1 = {
	monarch = {
		name = "Clemens VIII" #Clement VIII, elected by anti-Spaniards, anti-semitic but 
		adm = 4 #Was 5 - hurt Avignon by forbidding Jews to sell goods, merciless to his enemies
		dip = 7 #Was 4 - reconciled the church with Henry IV of France, brokered peace between Spain and France
		mil = 4 #Was 3 - Papal armies occupied Ferrara, but more of a diplomatic victory than a military one
	}
}

1600.1.1 = { innovative_narrowminded = 5 } # Giordano Bruno

1600.1.1 = { set_country_flag = start_after_1600 }

1605.3.4 = {
	monarch = {
		name = "Leo XI" #Pope for 27 days
		adm = 4 #Was 6 
		dip = 4 #Was 8 - France spent vast sums on his election
		mil = 4 #Was 5
	}
}

1605.4.28 = {
	monarch = {
		name = "Paulus V" #Paul V, a compromise candidate
		adm = 4 #Was 3 - nothing special
		dip = 4 #Was 5 - Made enemies with his insistence on clergy being tried by church courts, excommunicated Venice
		mil = 4
	}
	clr_country_flag = secular_pope
	set_country_flag = reformer_pope
}

1621.1.29 = {
	monarch = {
		name = "Gregorius XV" #Gregory XV, lessened punishments for witchcraft, canonized many saints
		adm = 5 
		dip = 4 #Financially supported the war against the Turks
		mil = 3
	}
}

1623.7.9 = {
	monarch = {
		name = "Urbanus VIII" #Urban VIII, reformer of church missions, demanded Galileo recant
		adm = 3 #Was 6 - Practiced nepotism on a grand scale, incurred massive debts (80% of papal states income spent on debt relief by his death)
		dip = 4 #Largely ignored the 30 years war in favor of Italian campaigns
		mil = 6 #Was 5 - Last pope to expand the papal states by force, established arms factories!
	}
	set_country_flag = corrupt_pope
	clr_country_flag = reformer_pope
}

1644.7.30 = {
	monarch = {
		name = "Innocentius X" #Innocent X, a compromise candidate that the French tried to veto
		adm = 5 #Involved in Italian factionalism, often struggled with France over papal authority and cardinal independence
		dip = 3 #Was 4 - Objected in vain to the Peace of Westphalia, supported Ireland (!) in the English Civil War
		mil = 4 #Fought a war against Parma and destroyed the city of Castro
	}
	clr_country_flag = corrupt_pope
	set_country_flag = secular_pope
}

1648.10.24 = { set_country_flag = decline_of_papacy } #Peace of Westphalia

1655.1.8 = {
	monarch = {
		name = "Alexander VII" #The infamous Alexander VII
		adm = 3 #Was 5 - Gave palaces and offices to his nephews and brother, disliked the business of state
		dip = 6 #Was 4 - Very active diplomatically across Europe, won concessions from Venice, but not as successful with France
		mil = 4 #Was 5 - nothing special
	}
	set_country_flag = corrupt_pope
	clr_country_flag = secular_pope
}

1667.5.23 = {
	monarch = {
		name = "Clemens IX" #Clement IX
		adm = 5
		dip = 5 #Was 3 - brokered a peace treaty between France, Spain, England and the Netherlands
		mil = 5 #Was 4 - Worked to strengthen Venetian defenses against the Turks
	}
	clr_country_flag = corrupt_pope
	set_country_flag = secular_pope
}

1669.12.10 = {
	monarch = {
		name = "Clemens X" #Clement X, elected at age 80, had to be dragged screaming to be made pope (!)
		adm = 4 #Helped reorganize the church in the New World, did not have total control of his own administration however
		dip = 7 #Beloved, elected almost unanimously, the Grand Duke of Moscow sought the title Tsar from him
		mil = 3 #Loved peace and abhorred war
	}
	set_country_flag = reformer_pope
	clr_country_flag = secular_pope
}

1676.7.23 = {
	monarch = {
		name = "Innocentius XI" #Innocent XI, rejected twice by the French
		adm = 7 #Was 5 - Reformed the Curia, reduced expenses, passed ordinances against nepotism
		dip = 6 #Was 3 - Reestablished contact with England, struggled with France over Cologne and won (!)
		mil = 5 #Held firm against France despite threats and worse
	}
}

1689.8.13 = {
	monarch = {
		name = "Alexander VIII" #Elected with French support, given back Avignon
		adm = 3 #Was 5 - Grand nepotism, exhausted the treasury
		dip = 6
		mil = 5 #Sent galleys and troops to help Venice in Albania
	}
	clr_country_flag = reformer_pope
	set_country_flag = corrupt_pope
}

1691.3.20 = {
	monarch = {
		name = "Innocentius XII" #Innocent XII, banned papal nepotism and said "the poor were his nephews"
		adm = 5 #Reformed the justice system of the Papal States
		dip = 5 #Was 3 - Benevolent and well-loved, leaned towards France
		mil = 4
	}
  set_country_flag = nepotism_banned
	clr_country_flag = corrupt_pope
	set_country_flag = reformer_pope
}

1700.9.28 = {
	monarch = {
		name = "Clemens XI" #Clement XI
		adm = 5 #Avoided nepotism
		dip = 6
		mil = 3 #Was 4 - Lost several cities including Parma to Austria
	}
	set_country_flag = secular_pope
	clr_country_flag = reformer_pope
}

1721.3.20 = {
	monarch = {
		name = "Innocentius XIII" #Innocent XIII, supported James II, disliked Jesuits
		adm = 4
		dip = 3
		mil = 4
	}
}

1724.3.8 = {
	monarch = {
		name = "Benedictus XIII" #Benedict XIII, an aesthetic who was elected pope against his wishes
		adm = 3 #Was 5 - his secretary embezzled fortunes and led the papal states to debt
		dip = 4
		mil = 5
	}
	clr_country_flag = secular_pope
	set_country_flag = reformer_pope
}

1730.2.22 = {
	monarch = {
		name = "Clemens XII" #Clement XII, elected after FOUR YEARS of deliberations
		adm = 6 #Was 4 - restored papal finances, revived the public lottery, paved the streets of Rome & built highways
		dip = 4 #Was 6 - not very successful diplomatically
		mil = 3 #Was 4 - papal forces tried to conquer San Marino and failed
	}
	set_country_flag = secular_pope
	clr_country_flag = reformer_pope
}

1740.2.7 = {
	monarch = {
		name = "Benedictus XIV" #Benedict XIV, a strict reformer, spoke out against slavery
		adm = 5 #Was 4 - Reformed the education of priests
		dip = 3 #Was 5 - many converts left the church due to his reforms
		mil = 4
	}
	clr_country_flag = secular_pope
	set_country_flag = reformer_pope
}

1758.5.4 = {
	monarch = {
		name = "Clemens XIII" #Clement XIII, enemy of the Jesuits, possibly poisoned
		adm = 3
		dip = 4
		mil = 4
	}
}

1769.2.3 = {
	monarch = {
		name = "Clemens XIV"
		adm = 6
		dip = 6
		mil = 4
	}
}

1774.9.22 = {
	monarch = {
		name = "Pius VI"
		adm = 3
		dip = 3
		mil = 3
	}
}

1778.1.1 = { innovative_narrowminded = 3 } # The Cleansing of the Pontinian Marches

1800.3.14 = {
	monarch = {
		name = "Pius VII"
		adm = 4
		dip = 3
		mil = 3
	}
}
