government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -1
imperialism_isolationism = 0
secularism_theocracy = 1
offensive_defensive = 2
land_naval = -1
quality_quantity = 0
serfdom_freesubjects = -4
primary_culture = pommeranian
religion = catholic
technology_group = western
capital = 48	# Stettin

 

#Not unified till 1620
1618.2.3 = {
	monarch = {
		name = "Franciszek I"
		dynasty = "Gryf"
		adm = 4
		dip = 4
		mil = 4
	}
}

1618.2.3 = {
	heir = {
		name = "Boguslaw"
		monarch_name = "Boguslaw XIV"
		dynasty = "Gryf"
		birth_date = 1580.3.31
		death_date = 1637.3.10
		claim = 95
		adm = 6
		dip = 4
		mil = 3
	}
}

1620.11.28 = {
	monarch = {
		name = "Boguslaw XIV"
		dynasty = "Gryf"
		adm = 6
		dip = 4
		mil = 3
	}
}
