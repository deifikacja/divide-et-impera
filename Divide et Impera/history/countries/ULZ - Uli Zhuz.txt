government = steppe_horde
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 3
mercantilism_freetrade = -2
imperialism_isolationism = 0
secularism_theocracy = -1
offensive_defensive = -4
land_naval = -5
quality_quantity = 3
serfdom_freesubjects = -3
primary_culture = khazak
religion = sunni
technology_group = ordu
capital = 456	# Turkestan


1718.1.1 = {
	monarch = {
		name = "Bolat ibn Tawke"
		dynasty = Kazakh
		adm = 4
		dip = 3
		mil = 5
	}
}

1729.1.1 = {
	monarch = {
		name = "Zholbarys and Abu'l Mambet"
		dynasty = Kazakh
		adm = 4
		dip = 7
		mil = 6
	}
}

1771.1.1 = {
	monarch = {
		name = "Abylai Abu'l Mansur"
		dynasty = Kazakh
		adm = 3
		dip = 4
		mil = 7
	}
}

1781.1.1 = {
	monarch = {
		name = "Wali"
		dynasty = Kazakh
		adm = 5
		dip = 4
		mil = 5
	}
}

1818.1.1 = {
	monarch = {
		name = "Suyuk"
		dynasty = Kazakh
		adm = 4
		dip = 4
		mil = 4
	}
}