government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -1
imperialism_isolationism = 1
secularism_theocracy = 1
offensive_defensive = 2
land_naval = -3
quality_quantity = 2
serfdom_freesubjects = -3
primary_culture = byelorussian
religion = orthodox
technology_group = eastern
capital = 276	# Minsk

 