government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -3
imperialism_isolationism = 1
secularism_theocracy = 0
offensive_defensive = 1
land_naval = 3
quality_quantity = 0
serfdom_freesubjects = -2
primary_culture = frisian
religion = catholic
technology_group = western
capital = 2157	# Norden

 

1356.1.1 = {
	monarch = {
		name = "Edzard"
		dynasty = Cirksena
		adm = 6
		dip = 5
		mil = 5
	}
}
1400.1.1 = {
	monarch = {
		name = "Enno I Edzardsna"
		dynasty = Cirksena
		adm = 5
		dip = 4
		mil = 6
	}
}

1450.1.1 = {
	monarch = {
		name = "Urlich"
		dynasty = Cirksena
		adm = 4
		dip = 4
		mil = 4
	}
}

1466.1.1 = {
	monarch = {
		name = "Enno I"
		dynasty = Cirksena
		adm = 4
		dip = 4
		mil = 4
	}
}

1491.1.1 = {
	monarch = {
		name = "Edzart I"
		dynasty = Cirksena
		adm = 4
		dip = 4
		mil = 4
	}
}

1528.1.1 = {
	monarch = {
		name = "Urlich II"
		dynasty = Cirksena
		adm = 4
		dip = 4
		mil = 4
	}
}

1528.5.1 = {
	monarch = {
		name = "Enno II"
		dynasty = Cirksena
		adm = 8
		dip = 7
		mil = 8
	}
}

1540.1.1 = {
	monarch = {
		name = "Edzard II"
		dynasty = Cirksena
		adm = 8
		dip = 7
		mil = 8
	}
}

1599.1.1 = {
	monarch = {
		name = "Enno III"
		dynasty = Cirksena
		adm = 8
		dip = 7
		mil = 8
	}
}

1625.1.1 = {
	monarch = {
		name = "Rudolf Christian"
		dynasty = Cirksena
		adm = 8
		dip = 7
		mil = 8
	}
}

1628.1.1 = {
	monarch = {
		name = "Urlich III"
		dynasty = Cirksena
		adm = 8
		dip = 7
		mil = 8
	}
}

1648.1.1 = {
	monarch = {
		name = "Enno Ludwig"
		dynasty = Cirksena
		adm = 8
		dip = 7
		mil = 8
	}
}

1660.1.1 = {
	monarch = {
		name = "Georg Christian"
		dynasty = Cirksena
		adm = 8
		dip = 7
		mil = 8
	}
}

1665.1.1 = {
	monarch = {
		name = "Christian Eberhard"
		dynasty = Cirksena
		adm = 8
		dip = 7
		mil = 8
	}
}
1708.1.1 = {
	monarch = {
		name = "Georg Albrecht"
		dynasty = Cirksena
		adm = 8
		dip = 7
		mil = 8
	}
}
1734.1.1 = {
	monarch = {
		name = "Karl Edzard"
		dynasty = Cirksena
		adm = 8
		dip = 7
		mil = 8
	}
}