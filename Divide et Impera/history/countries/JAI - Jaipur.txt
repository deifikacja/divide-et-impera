government = caste_monarchy
aristocracy_plutocracy = -5
centralization_decentralization = 3
innovative_narrowminded = 1
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 1
offensive_defensive = -4
land_naval = -5
quality_quantity = -1
serfdom_freesubjects = -2
primary_culture = rajput
religion = hinduism
technology_group = indian
capital = 514	# Jaipur

# Rajas of Jaipur


1317.1.1 = {
	monarch = {
		name = "Jansi"
		dynasty = Kachwaha
		DIP = 5
		MIL = 6
		ADM = 5
	}
}

1356.1.1 = {
	heir = {
		name = "Udayakarna"
		monarch_name = "Udayakarna"
		dynasty = Kachwaha
		birth_date = 1356.1.1
		death_date = 1388.1.1
		claim = 95
		adm = 3
		dip = 3
		mil = 3
	}
}

1366.1.1 = {
	monarch = {
		name = "Udayakarna"
		dynasty = Kachwaha
		DIP = 3
		MIL = 4
		ADM = 3
	}
}

1366.1.1 = {
	heir = {
		name = "Nara Singh"
		monarch_name = "Nara Singh"
		dynasty = Kachwaha
		birth_date = 1366.1.1
		death_date = 1413.1.1
		claim = 95
		adm = 4
		dip = 5
		mil = 3
	}
}

1388.1.1 = {
	monarch = {
		name = "Nara Singh"
		dynasty = Kachwaha
		DIP = 4
		MIL = 5
		ADM = 3
	}
}

1388.1.1 = {
	heir = {
		name = "Banbir"
		monarch_name = "Banbir"
		dynasty = Kachwaha
		birth_date = 1388.1.1
		death_date = 1424.1.1
		claim = 95
		adm = 4
		dip = 4
		mil = 4
	}
}

1413.1.1 = {
	monarch = {
		name = "Banbir"
		dynasty = Kachwaha
		DIP = 4
		MIL = 4
		ADM = 4
	}
}

1413.1.1 = {
	heir = {
		name = "Udha Rao"
		monarch_name = "Udha Rao"
		dynasty = Kachwaha
		birth_date = 1413.1.1
		death_date = 1453.1.1
		claim = 95
		adm = 4
		dip = 4
		mil = 4
	}
}

1424.1.1 = {
	monarch = {
		name = "Udha Rao"
		dynasty = Kachwaha
		DIP = 4
		MIL = 4
		ADM = 4
	}
}

1424.1.1 = {
	heir = {
		name = "Chandrasena"
		monarch_name = "Chandrasena"
		dynasty = Kachwaha
		birth_date = 1424.1.1
		death_date = 1502.1.1
		claim = 95
		adm = 3
		dip = 6
		mil = 4
	}
}

1453.1.1 = {
	monarch = {
		name = "Chandrasena"
		dynasty = Kachwaha
		DIP = 3
		MIL = 6
		ADM = 4
	}
}

1453.1.1 = {
	heir = {
		name = "Prithvi Singh"
		monarch_name = "Prithvi Singh I"
		dynasty = Kachwaha
		birth_date = 1453.1.1
		death_date = 1527.1.1
		claim = 95
		adm = 4
		dip = 5
		mil = 3
	}
}

1502.1.1 = {
	monarch = {
		name = "Prithvi Singh I"
		dynasty = Kachwaha
		DIP = 4
		MIL = 5
		ADM = 3
	}
}

1502.1.1 = {
	heir = {
		name = "Puranmal"
		monarch_name = "Puranmal"
		dynasty = Kachwaha
		birth_date = 1502.1.1
		death_date = 1534.1.1
		claim = 95
		adm = 6
		dip = 6
		mil = 5
	}
}

1527.1.1 = {
	monarch = {
		name = "Puranmal"
		dynasty = Kachwaha
		DIP = 6
		MIL = 6
		ADM = 5
	}
}

1527.1.1 = {
	heir = {
		name = "Udha Rao"
		monarch_name = "Udha Rao"
		dynasty = Kachwaha
		birth_date = 1527.1.1
		death_date = 1537.1.1
		claim = 95
		adm = 3
		dip = 5
		mil = 3
	}
}

1534.1.1 = {
	monarch = {
		name = "Bhima"
		dynasty = Kachwaha
		DIP = 3
		MIL = 5
		ADM = 3
	}
}

1534.1.1 = {
	heir = {
		name = "Ratan"
		monarch_name = "Ratan"
		dynasty = Kachwaha
		birth_date = 1534.1.1
		death_date = 1547.1.1
		claim = 95
		adm = 6
		dip = 4
		mil = 4
	}
}

1537.1.1 = {
	monarch = {
		name = "Ratan"
		dynasty = Kachwaha
		DIP = 6
		MIL = 4
		ADM = 4
	}
}

1537.1.1 = {
	heir = {
		name = "Baharmalla"
		monarch_name = "Baharmalla"
		dynasty = Kachwaha
		birth_date = 1537.1.1
		death_date = 1574.1.1
		claim = 95
		adm = 4
		dip = 5
		mil = 6
	}
}

1547.1.1 = {
	monarch = {
		name = "Baharmalla"
		dynasty = Kachwaha
		DIP = 4
		MIL = 5
		ADM = 6
	}
}

1547.1.1 = {
	heir = {
		name = "Bhagwan Das"
		monarch_name = "Bhagwan Das"
		dynasty = Kachwaha
		birth_date = 1547.1.1
		death_date = 1589.1.1
		claim = 95
		adm = 7
		dip = 8
		mil = 6
	}
}

1574.1.1 = {
	monarch = {
		name = "Bhagwan Das"
		dynasty = Kachwaha
		DIP = 7
		MIL = 8
		ADM = 6
	}
}

1574.1.1 = {
	heir = {
		name = "Man Singh"
		monarch_name = "Man Singh I"
		dynasty = Kachwaha
		birth_date = 1574.1.1
		death_date = 1614.1.1
		claim = 95
		adm = 4
		dip = 5
		mil = 4
	}
}

1589.1.1 = {
	monarch = {
		name = "Man Singh I"
		dynasty = Kachwaha
		DIP = 4
		MIL = 5
		ADM = 4
	}
}

1589.1.1 = {
	heir = {
		name = "Jagat Singh"
		monarch_name = "Jagat Singh I"
		dynasty = Kachwaha
		birth_date = 1589.1.1
		death_date = 1622.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 3
	}
}

1614.1.1 = {
	monarch = {
		name = "Jagat Singh I"
		dynasty = Kachwaha
		DIP = 5
		MIL = 5
		ADM = 3
	}
}

1614.1.1 = {
	heir = {
		name = "Jaya Singh"
		monarch_name = "Jaya Singh I"
		dynasty = Kachwaha
		birth_date = 1614.1.1
		death_date = 1667.1.1
		claim = 95
		adm = 3
		dip = 6
		mil = 4
	}
}

1622.1.1 = {
	monarch = {
		name = "Jaya Singh I"
		dynasty = Kachwaha
		DIP = 3
		MIL = 6
		ADM = 4
	}
}

1622.1.1 = {
	heir = {
		name = "Rama Singh"
		monarch_name = "Rama Singh I"
		dynasty = Kachwaha
		birth_date = 1622.1.1
		death_date = 1688.1.1
		claim = 95
		adm = 6
		dip = 5
		mil = 5
	}
}

1667.1.1 = {
	monarch = {
		name = "Rama Singh I"
		dynasty = Kachwaha
		DIP = 6
		MIL = 5
		ADM = 5
	}
}

1667.1.1 = {
	heir = {
		name = "Bishan Singh"
		monarch_name = "Bishan Singh"
		dynasty = Kachwaha
		birth_date = 1667.1.1
		death_date = 1700.1.1
		claim = 95
		adm = 4
		dip = 3
		mil = 3
	}
}

1688.1.1 = {
	monarch = {
		name = "Bishan Singh"
		dynasty = Kachwaha
		DIP = 4
		MIL = 3
		ADM = 3
	}
}

1688.1.1 = {
	heir = {
		name = "Sawai Jaya Singh"
		monarch_name = "Sawai Jaya Singh II"
		dynasty = Kachwaha
		birth_date = 1688.1.1
		death_date = 1743.1.1
		claim = 95
		adm = 4
		dip = 3
		mil = 3
	}
}

1700.1.1 = {
	monarch = {
		name = "Sawai Jaya Singh II"
		dynasty = Kachwaha
		DIP = 6
		MIL = 5
		ADM = 5
	}
}

1700.1.1 = {
	heir = {
		name = "Ishwari Singh"
		monarch_name = "Ishwari Singh"
		dynasty = Kachwaha
		birth_date = 1700.1.1
		death_date = 1750.1.1
		claim = 95
		adm = 6
		dip = 5
		mil = 5
	}
}

1743.1.1 = {
	monarch = {
		name = "Ishwari Singh"
		dynasty = Kachwaha
		DIP = 6
		MIL = 5
		ADM = 5
	}
}

1743.1.1 = {
	heir = {
		name = "Madhu Singh"
		monarch_name = "Madhu Singh I"
		dynasty = Kachwaha
		birth_date = 1743.1.1
		death_date = 1750.1.1
		claim = 95
		adm = 6
		dip = 5
		mil = 5
	}
}

1750.1.1 = {
	monarch = {
		name = "Madhu Singh I"
		dynasty = Kachwaha
		DIP = 6
		MIL = 5
		ADM = 5
	}
}

1750.1.1 = {
	heir = {
		name = "Prithvi Singh"
		monarch_name = "Prithvi Singh II"
		dynasty = Kachwaha
		birth_date = 1750.1.1
		death_date = 1778.1.1
		claim = 95
		adm = 6
		dip = 5
		mil = 5
	}
}

1768.1.1 = {
	monarch = {
		name = "Prithvi Singh II"
		dynasty = Kachwaha
		DIP = 6
		MIL = 5
		ADM = 5
	}
}

1768.1.1 = {
	heir = {
		name = "Pratap Singh"
		monarch_name = "Pratap Singh"
		dynasty = Kachwaha
		birth_date = 1768.1.1
		death_date = 1803.1.1
		claim = 95
		adm = 6
		dip = 5
		mil = 5
	}
}

1778.1.1 = {
	monarch = {
		name = "Pratap Singh"
		dynasty = Kachwaha
		DIP = 6
		MIL = 5
		ADM = 5
	}
}

1778.1.1 = {
	heir = {
		name = "Jagat Singh"
		monarch_name = "Jagat Singh II"
		dynasty = Kachwaha
		birth_date = 1778.1.1
		death_date = 1818.1.1
		claim = 95
		adm = 6
		dip = 5
		mil = 5
	}
}

1803.1.1 = {
	monarch = {
		name = "Jagat Singh II"
		dynasty = Kachwaha
		DIP = 6
		MIL = 5
		ADM = 5
	}
}

1803.1.1 = {
	heir = {
		name = "Man Singh"
		monarch_name = "Man Singh II"
		dynasty = Kachwaha
		birth_date = 1803.1.1
		death_date = 1819.1.1
		claim = 95
		adm = 6
		dip = 5
		mil = 5
	}
}

1818.1.1 = {
	monarch = {
		name = "Man Singh II"
		dynasty = Kachwaha
		DIP = 6
		MIL = 5
		ADM = 5
	}
}

1818.1.1 = {
	heir = {
		name = "Jaya Singh"
		monarch_name = "Jaya Singh III"
		dynasty = Kachwaha
		birth_date = 1818.1.1
		death_date = 1835.1.1
		claim = 95
		adm = 6
		dip = 5
		mil = 5
	}
}

1819.1.1 = {
	monarch = {
		name = "Jaya Singh III"
		dynasty = Kachwaha
		DIP = 6
		MIL = 5
		ADM = 5
	}
}
