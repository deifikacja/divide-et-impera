government = tribal_federation
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = 1
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = 0
land_naval = 5
quality_quantity = 0
serfdom_freesubjects = 0
primary_culture = tahitan
religion = animism
technology_group = pacific
capital = 2031	# Raiatea

#Ari`i of Raiatea
1350.1.1 = {
	monarch = {
		name = "Ari`i"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}
1750.1.1 = {
	monarch = {
		name = "Tamatoa I"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1800.1.1 = {
	monarch = {
		name = "Taraoari`i Tamatoa II fa`o"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1815.1.1 = {
	monarch = {
		name = "Vete`a ra`i Tamatoa III"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}