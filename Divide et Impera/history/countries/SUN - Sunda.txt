government = caste_monarchy
aristocracy_plutocracy = -2
centralization_decentralization = 3
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 0
land_naval = 2
quality_quantity = 2
serfdom_freesubjects = -1
primary_culture = sulawesi
technology_group = chinese
religion = hinduism
capital = 625	# Sunda

1340.1.1 = {
	monarch = {
		name = "Prabu Maharaja Lingga Buana"
		dynasty = Sunda
		adm = 4
		dip = 7
		mil = 7
	}
}


1357.1.1 = {
	monarch = {
		name = "Mangkubumi Suradipati"
		dynasty = Sunda
		adm = 4
		dip = 7
		mil = 7
	}
}


1371.1.1 = {
	monarch = {
		name = "Prabu Raja Wastu"
		dynasty = Sunda
		adm = 4
		dip = 7
		mil = 7
	}
}

1421.1.1 = {
	monarch = {
		name = "Niskala Wastu Kancana"
		dynasty = Sunda
		adm = 6
		dip = 8
		mil = 5
	}
}

1475.1.1 = {
	monarch = {
		name = "Prabu Susuk Tunggal"
		dynasty = Sunda
		adm = 6
		dip = 8
		mil = 5
	}
}

1482.1.1 = {
	capital = 630
	monarch = {
		name = "Sri Baduga Maharaja"
		dynasty = Sunda
		adm = 3
		dip = 5
		mil = 4
	}
}