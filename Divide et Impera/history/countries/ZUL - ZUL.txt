government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -5
imperialism_isolationism = -1
secularism_theocracy = 0
offensive_defensive = 2
land_naval = -5
quality_quantity = 2
serfdom_freesubjects = -5
primary_culture = bantu
religion = shamanism
technology_group = african
capital = 1279	# Zulu

1700.1.1 = {
	monarch = {
		name = "Zulu"
		dynasty = Zulu
		adm = 3
		dip = 3
		mil = 5
	}
}
1710.1.1 = {
	monarch = {
		name = "Gumede"
		dynasty = Zulu
		adm = 3
		dip = 3
		mil = 5
	}
}
1720.1.1 = {
	monarch = {
		name = "Phunga"
		dynasty = Zulu
		adm = 3
		dip = 3
		mil = 5
	}
}
1730.1.1 = {
	monarch = {
		name = "Mageba"
		dynasty = Zulu
		adm = 3
		dip = 3
		mil = 5
	}
}
1740.1.1 = {
	monarch = {
		name = "Ndaba"
		dynasty = Zulu
		adm = 3
		dip = 3
		mil = 5
	}
}
1763.1.1 = {
	monarch = {
		name = "Jama"
		dynasty = Zulu
		adm = 3
		dip = 3
		mil = 5
	}
}
1781.1.1 = {
	monarch = {
		name = "Senzangakhona"
		dynasty = Zulu
		adm = 3
		dip = 3
		mil = 5
	}
}
1816.1.1 = {
	monarch = {
		name = "Shaka"
		dynasty = Zulu
		adm = 5
		dip = 5
		mil = 8
	}
}
