government = eastern_despotism
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 0
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = -2
offensive_defensive = -1
land_naval = -1
quality_quantity = -1
serfdom_freesubjects = -1
technology_group = indian
religion = sunni
primary_culture = kannada
capital = 1991	# Bidar


1489.1.1 = {
	monarch = {
		name = "Quasim I"
		dynasty = Barid 
		DIP = 7
		ADM = 7
		MIL = 8
	}
}
1504.1.1 = {
	monarch = {
		name = "Amir Shah I"
		dynasty = Barid
		DIP = 5
		ADM = 6
		MIL = 5
	}
}
1542.1.1 = {
	monarch = {
		name = "'Ali Shah I"
		dynasty = Barid
		DIP = 3
		ADM = 3
		MIL = 3
	}
}
1579.1.1 = {
	monarch = {
		name = "Ibrahim Shah"
		dynasty = Barid
		DIP = 5
		ADM = 5
		MIL = 5
	}
}
1586.1.1 = {
	monarch = {
		name = "Quasim Shah II"
		dynasty = Barid
		DIP = 5
		ADM = 5
		MIL = 4
	}
}
1589.1.1 = {
	monarch = {
		name = "Amir Shah II"
		dynasty = Barid
		DIP = 6
		ADM = 7
		MIL = 8
	}
}
1601.1.1 = {
	monarch = {
		name = "Mirza 'Ali Shah"
		dynasty = Barid
		DIP = 6
		ADM = 5
		MIL = 5
	}
}
1609.1.1 = {
	monarch = {
		name = "'Ali Shah II"
		dynasty = Barid
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

#Annexed by Bijapur 1619