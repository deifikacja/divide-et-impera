government = eastern_despotism
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = -1
offensive_defensive = 2
land_naval = 2
quality_quantity = -1
serfdom_freesubjects = -2
primary_culture = sulawesi
religion = sunni
technology_group = chinese
capital = 629	# Pajang

1548.1.1 = {
	monarch = {
		name = "Jaka Tingir Pangeran Adivijaya Surya Alam"
		dynasty = Pajang
		adm = 3
		dip = 3
		mil = 3
	}
}

1588.1.1 = {
	monarch = {
		name = "Pangeran Benawa"
		dynasty = Pajang
		adm = 4
		dip = 4
		mil = 8
	}
}

1589.1.1 = {
	monarch = {
		name = "Pangeran Benawa II"
		dynasty = Pajang
		adm = 4
		dip = 4
		mil = 8
	}
}