government = caste_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -4
imperialism_isolationism = 1
secularism_theocracy = -2
offensive_defensive = 2
land_naval = -1
quality_quantity = -1
serfdom_freesubjects = -1
technology_group = indian
religion = hinduism
primary_culture = avadhi
capital = 550	# Kalinjar

#Rajas of Orchha

1339.1.1 = {
	monarch = {
		name = "Ram Singh"
		dynasty = Bundela
		adm = 5
		dip = 6
		mil = 7
	}

}

1339.1.1 = {
	heir = {
		name = "Ramchandra Singh"
		monarch_name = "Ramchandra Singh"
		dynasty = Bundela
		birth_date = 1339.1.1
		death_date = 1384.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 4
	}
}

1375.1.1 = {
	monarch = {
		name = "Ramchandra Singh"
		dynasty = Bundela
		adm = 5
		dip = 5
		mil = 4
	}

}

1375.1.1 = {
	heir = {
		name = "Mednepal Singh"
		monarch_name = "Mednepal Singh"
		dynasty = Bundela
		birth_date = 1375.1.1
		death_date = 1437.1.1
		claim = 95
		adm = 6
		dip = 5
		mil = 6
	}
}

1384.1.1 = {
	monarch = {
		name = "Mednepal Singh"
		dynasty = Bundela
		adm = 6
		dip = 5
		mil = 6
	}

}

1384.1.1 = {
	heir = {
		name = "Arjun Deo"
		monarch_name = "Arjun Deo"
		dynasty = Bundela
		birth_date = 1384.1.1
		death_date = 1468.1.1
		claim = 95
		adm = 4
		dip = 4
		mil = 3
	}
}

1437.1.1 = {
	monarch = {
		name = "Arjun Deo"
		dynasty = Bundela
		adm = 4
		dip = 4
		mil = 3
	}

}

1437.1.1 = {
	heir = {
		name = "Malkhan Singh"
		monarch_name = "Malkhan Singh"
		dynasty = Bundela
		birth_date = 1437.1.1
		death_date = 1501.1.1
		claim = 95
		adm = 5
		dip = 4
		mil = 5
	}
}

1468.1.1 = {
	monarch = {
		name = "Malkhan Singh"
		dynasty = Bundela
		adm = 5
		dip = 4
		mil = 5
	}

}

1468.1.1 = {
	heir = {
		name = "Rudra-pratap Singh"
		monarch_name = "Rudra-pratap Singh"
		dynasty = Bundela
		birth_date = 1468.1.1
		death_date = 1531.1.1
		claim = 95
		adm = 8
		dip = 6
		mil = 6
	}
}

1501.1.1 = {
	monarch = {
		name = "Rudra-pratap Singh"
		dynasty = Bundela
		adm = 8
		dip = 6
		mil = 6
	}

}

1501.1.1 = { government = caste_monarchy }

1501.1.1 = {
	heir = {
		name = "Bharti-candra"
		monarch_name = "Bharti-candra I"
		dynasty = Bundela
		birth_date = 1501.1.1
		death_date = 1534.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1531.1.1 = {
	monarch = {
		name = "Bharti-candra I"
		dynasty = Bundela
		adm = 5
		dip = 5
		mil = 5
	}

}

1531.1.1 = {
	heir = {
		name = "Madukar Shah"
		monarch_name = "Madukar Shah"
		dynasty = Bundela
		birth_date = 1531.1.1
		death_date = 1592.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1554.1.1 = {
	monarch = {
		name = "Madukar Shah"
		dynasty = Bundela
		adm = 5
		dip = 5
		mil = 5
	}

}

1554.1.1 = {
	heir = {
		name = "Madukar Shah"
		monarch_name = "Madukar Shah"
		dynasty = Bundela
		birth_date = 1554.1.1
		death_date = 1604.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1592.1.1 = {
	monarch = {
		name = "Ram Shah"
		dynasty = Bundela
		adm = 5
		dip = 5
		mil = 5
	}

}

1592.1.1 = {
	heir = {
		name = "Bir Singh Deo"
		monarch_name = "Bir Singh Deo I"
		dynasty = Bundela
		birth_date = 1592.1.1
		death_date = 1626.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1604.1.1 = {
	monarch = {
		name = "Bir Singh Deo I"
		dynasty = Bundela
		adm = 5
		dip = 5
		mil = 5
	}

}

1604.1.1 = {
	heir = {
		name = "Jujhar Singh"
		monarch_name = "Jujhar Singh"
		dynasty = Bundela
		birth_date = 1604.1.1
		death_date = 1635.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1626.1.1 = {
	monarch = {
		name = "Jujhar Singh"
		dynasty = Bundela
		adm = 5
		dip = 5
		mil = 5
	}

}

1626.1.1 = {
	heir = {
		name = "Devi Singh"
		monarch_name = "Devi Singh"
		dynasty = Bundela
		birth_date = 1626.1.1
		death_date = 1641.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1635.1.1 = {
	monarch = {
		name = "Devi Singh"
		dynasty = Bundela
		adm = 5
		dip = 5
		mil = 5
	}

}

1635.1.1 = {
	heir = {
		name = "Devi Singh"
		monarch_name = "Devi Singh"
		dynasty = Bundela
		birth_date = 1635.1.1
		death_date = 1653.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1641.1.1 = {
	monarch = {
		name = "Pahar Singh"
		dynasty = Bundela
		adm = 5
		dip = 5
		mil = 5
	}

}

1641.1.1 = {
	heir = {
		name = "Devi Singh"
		monarch_name = "Devi Singh"
		dynasty = Bundela
		birth_date = 1641.1.1
		death_date = 1672.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1653.1.1 = {
	monarch = {
		name = "Sujan Singh I"
		dynasty = Bundela
		adm = 5
		dip = 5
		mil = 5
	}

}

1653.1.1 = {
	heir = {
		name = "Indramani Singh"
		monarch_name = "Indramani Singh"
		dynasty = Bundela
		birth_date = 1653.1.1
		death_date = 1675.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1672.1.1 = {
	monarch = {
		name = "Indramani Singh"
		dynasty = Bundela
		adm = 5
		dip = 5
		mil = 5
	}

}

1672.1.1 = {
	heir = {
		name = "Jaswant Singh"
		monarch_name = "Jaswant Singh"
		dynasty = Bundela
		birth_date = 1672.1.1
		death_date = 1684.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1675.1.1 = {
	monarch = {
		name = "Jaswant Singh"
		dynasty = Bundela
		adm = 5
		dip = 5
		mil = 5
	}

}

1675.1.1 = {
	heir = {
		name = "Baghwat Singh"
		monarch_name = "Baghwat Singh"
		dynasty = Bundela
		birth_date = 1675.1.1
		death_date = 1689.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1684.1.1 = {
	monarch = {
		name = "Baghwat Singh"
		dynasty = Bundela
		adm = 5
		dip = 5
		mil = 5
	}

}

1684.1.1 = {
	heir = {
		name = "Udwat Singh"
		monarch_name = "Udwat Singh"
		dynasty = Bundela
		birth_date = 1684.1.1
		death_date = 1735.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1689.1.1 = {
	monarch = {
		name = "Udwat Singh"
		dynasty = Bundela
		adm = 5
		dip = 5
		mil = 5
	}

}

1689.1.1 = {
	heir = {
		name = "Prithvi Singh"
		monarch_name = "Prithvi Singh"
		dynasty = Bundela
		birth_date = 1689.1.1
		death_date = 1752.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1735.1.1 = {
	monarch = {
		name = "Prithvi Singh"
		dynasty = Bundela
		adm = 5
		dip = 5
		mil = 5
	}

}

1735.1.1 = {
	heir = {
		name = "Sawant Singh Mahendra"
		monarch_name = "Sawant Singh Mahendra"
		dynasty = Bundela
		birth_date = 1735.1.1
		death_date = 1765.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1752.1.1 = {
	monarch = {
		name = "Sawant Singh Mahendra"
		dynasty = Bundela
		adm = 5
		dip = 5
		mil = 5
	}

}

1752.1.1 = {
	heir = {
		name = "Hati Singh Mahendra"
		monarch_name = "Hati Singh Mahendra"
		dynasty = Bundela
		birth_date = 1752.1.1
		death_date = 1768.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1765.1.1 = {
	monarch = {
		name = "Hati Singh Mahendra"
		dynasty = Bundela
		adm = 5
		dip = 5
		mil = 5
	}

}

1765.1.1 = {
	heir = {
		name = "Man Singh Mahendra"
		monarch_name = "Man Singh Mahendra"
		dynasty = Bundela
		birth_date = 1765.1.1
		death_date = 1775.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1768.1.1 = {
	monarch = {
		name = "Man Singh Mahendra"
		dynasty = Bundela
		adm = 5
		dip = 5
		mil = 5
	}

}

1768.1.1 = {
	heir = {
		name = "Bharti-candra"
		monarch_name = "Bharti-candra II Mahndra"
		dynasty = Bundela
		birth_date = 1768.1.1
		death_date = 1776.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1775.1.1 = {
	monarch = {
		name = "Bharti-candra II Mahndra"
		dynasty = Bundela
		adm = 5
		dip = 5
		mil = 5
	}

}

1775.1.1 = {
	heir = {
		name = "Vikramajit Mahendra"
		monarch_name = "Vikramajit Mahendra"
		dynasty = Bundela
		birth_date = 1775.1.1
		death_date = 1834.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1776.1.1 = {
	monarch = {
		name = "Vikramajit Mahendra"
		dynasty = Bundela
		adm = 5
		dip = 5
		mil = 5
	}

}

1776.1.1 = {
	heir = {
		name = "Pal"
		monarch_name = "Pal"
		dynasty = Bundela
		birth_date = 1776.1.1
		death_date = 1834.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1817.1.1 = {
	monarch = {
		name = "Pal"
		dynasty = Bundela
		adm = 5
		dip = 5
		mil = 5
	}

}