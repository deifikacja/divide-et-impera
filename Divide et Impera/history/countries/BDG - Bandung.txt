government = oligarchical_monarchy
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 2
land_naval = 2
quality_quantity = 2
serfdom_freesubjects = -1
primary_culture = sulawesi
technology_group = chinese
religion = sunni
capital = 625	# Bandung

1678.1.1 = {
	monarch = {
		name = "Pangeran"
		dynasty="Wiranatakusuma"
		adm = 4
		dip = 5
		mil = 5
	}
}

1750.1.1 = {
	monarch = {
		name = "Adipati I"
		dynasty="Wiranatakusuma"
		adm = 3
		dip = 4
		mil = 3
	}
}

1792.1.1 = {
	monarch = {
		name = "Adipati II"
		dynasty="Wiranatakusuma"
		adm = 3
		dip = 4
		mil = 3
	}
}