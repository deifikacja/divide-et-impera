government = tribal_democracy
aristocracy_plutocracy = 5
centralization_decentralization = 5
innovative_narrowminded = 3
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = -3
land_naval = -5
quality_quantity = 0
serfdom_freesubjects = 5
primary_culture = iroquis
religion = shamanism
technology_group = new_world
capital = 964	# Lenape
