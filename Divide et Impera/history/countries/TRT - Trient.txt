government = theocratic_government
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -3
imperialism_isolationism = 2
secularism_theocracy = 3
offensive_defensive = 1
land_naval = 1
quality_quantity = 0
serfdom_freesubjects = -4
primary_culture = hannoverian
religion = catholic
technology_group = western
capital = 110	# Trent

1349.1.1 = {
	monarch = {
		name = "Meinhard von Neuhaus"
		adm = 4
		dip = 4
		mil = 4
	}
}

1362.1.1 = {
	monarch = {
		name = "Albert V von Ortenburg"
		adm = 4
		dip = 4
		mil = 4
	}
}

1390.10.1 = {
	monarch = {
		name = "Georg I von Liechtenstein"
		adm = 4
		dip = 4
		mil = 4
	}
}

1421.1.1 = {
	monarch = {
		name = "Armand de Cilly"
		adm = 5
		dip = 7
		mil = 4
	}
}

1422.1.1 = {
	monarch = {
		name = "Heinrich IV Flechtel"
		adm = 5
		dip = 5
		mil = 4
	}
}

1424.1.1 = {
	monarch = {
		name = "Aleksandr of Mazovia"
		adm = 6
		dip = 7
		mil = 6
	}
}

1444.1.1 = {
	monarch = {
		name = "Benedetto"
		adm = 3
		dip = 3
		mil = 4
	}
}

1446.1.1 = {
	monarch = {
		name = "Georg II Haak von Themeswald"
		adm = 4
		dip = 5
		mil = 4
	}
}

1465.1.1 = {
	monarch = {
		name = "Johan V von Hinderbach"
		adm = 4
		dip = 6
		mil = 5
	}
}

1486.1.1 = {
	monarch = {
		name = "Ulrich III von Frundsberg"
		adm = 6
		dip = 6
		mil = 6
	}
}

1493.12.5 = {
	monarch = {
		name = "Ulrich IV von Liechtenstein"
		adm = 4
		dip = 5
		mil = 5
	}
}

1505.1.1 = {
	monarch = {
		name = "Georg II von Neideck"
		adm = 6
		dip = 6
		mil = 4
	}
}

1514.1.1 = {
	monarch = {
		name = "Bernardo III Clesio"
		adm = 5
		dip = 6
		mil = 4
	}
}

1539.1.1 = {
	monarch = {
		name = "Cristofo di Madruzzo"
		adm = 6
		dip = 5
		mil = 5
	}
}

1567.1.1 = {
	monarch = {
		name = "Louis di Madruzzo"
		adm = 6
		dip = 7
		mil = 6
	}
}

1600.1.1 = {
	monarch = {
		name = "Carlo Gaudenzio di Madruzzo"
		adm = 5
		dip = 7
		mil = 4
	}
}

1629.1.1 = {
	monarch = {
		name = "Carlo Emmanuele di Madruzzo"
		adm = 5
		dip = 5
		mil = 5
	}
}

1659.1.1 = {
	monarch = {
		name = "Sigismund Franz von Österreichn"
		adm = 5
		dip = 4
		mil = 4
	}
}

1665.1.1 = {
	monarch = {
		name = "Ernst Adalbert von Harrach"
		adm = 5
		dip = 6
		mil = 5
	}
}

1668.1.1 = {
	monarch = {
		name = "Sigismund Alfons von Thun"
		adm = 4
		dip = 4
		mil = 4
	}
}

1677.1.1 = {
	monarch = {
		name = "Francesco Alberti di Pola"
		adm = 5
		dip = 6
		mil = 4
	}
}

1689.1.1 = {
	monarch = {
		name = "Giuseppe Vittorio Alberti di Enno"
		adm = 6
		dip = 5
		mil = 6
	}
}

1696.3.7 = {
	monarch = {
		name = "Johann Michael Graf von Spaur"
		adm = 5
		dip = 4
		mil = 4
	}
}

1725.10.9 = {
	monarch = {
		name = "Giovanni Benedetto Gentilotti"
		adm = 5
		dip = 6
		mil = 3
	}
}

1725.11.26 = {
	monarch = {
		name = "Anton Dominik Graf von Wolkenstein"
		adm = 4
		dip = 5
		mil = 4
	}
}

1730.6.19 = {
	monarch = {
		name = "Dominik Anton Graf von Thun"
		adm = 7
		dip = 5
		mil = 5
	}
}

1748.10.7 = {
	monarch = {
		name = "Leopold Ernest Graf von Firmian"
		adm = 7
		dip = 7
		mil = 5
	}
}

1758.10.7 = {
	monarch = {
		name = "Francesco Felice Alberti di Enno"
		adm = 3
		dip = 4
		mil = 4
	}
}

1763.7.2 = {
	monarch = {
		name = "Cristofo Francesco Sizzo de Norris"
		adm = 4
		dip = 4
		mil = 4
	}
}

1776.5.29 = {
	monarch = {
		name = "Peter Michael Vigil Graf von Thun und Hohenstein"
		adm = 6
		dip = 5
		mil = 4
	}
}
