government = tribal_federation
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = 1
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = 0
land_naval = 5
quality_quantity = 0
serfdom_freesubjects = 0
primary_culture = tongan
religion = animism
technology_group = pacific
capital = 2042	# Niue

#Patuiki of Niue
1700.1.1 = {
	monarch = {
		name = "Tihamau"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}
1720.1.1 = {
	monarch = {
		name = "Tepunua Mutalau"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1740.1.1 = {
	monarch = {
		name = "Leivalu"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1760.1.1 = {
	monarch = {
		name = "Hetalangi"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1780.1.1 = {
	monarch = {
		name = "Fakahinaiki"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1800.1.1 = {
	monarch = {
		name = "Punimata"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1815.1.1 = {
	monarch = {
		name = "Ihunga"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}