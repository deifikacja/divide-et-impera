government = tribal_despotism
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = 2
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 0
land_naval = 2
quality_quantity = 3
serfdom_freesubjects = -4
primary_culture = somali
religion = sunni
technology_group = african
capital = 1205	# Maqdishu


1354.1.1 = {
	monarch = {
		name = "Abgaal I"
		dynasty = Abgaal
		adm = 6
		dip = 5
		mil = 6
	}
}

1385.1.1 = {
	monarch = {
		name = "Abgaal II"
		dynasty = Abgaal
		adm = 7
		dip = 4
		mil = 4
	}
}

1393.1.1 = {
	monarch = {
		name = "'Umar"		
		dynasty = Abgaal
		adm = 4
		dip = 5
		mil = 6
	}
}

1441.1.1 = {
	monarch = {
		name = "Ahmad I"
		dynasty = Abgaal
		adm = 3
		dip = 6
		mil = 6
	}
}

1479.1.1 = {
	monarch = {
		name = "Hasan VII"
		dynasty = Abgaal
		adm = 6
		dip = 6
		mil = 7
	}
}

1522.1.1 = {
	monarch = {
		name = "Muhammad I"
		dynasty = Abgaal
		adm = 5
		dip = 5
		mil = 5
	}
}

1557.1.1 = {
	monarch = {
		name = "Ahmad II"
		dynasty = Abgaal
		adm = 5
		dip = 4
		mil = 4
	}
}

1601.1.1 = {
	monarch = {
		name = "Mahmud"
		dynasty = Abgaal
		adm = 6
		dip = 3
		mil = 4
	}
}

1644.7.23 = {
	monarch = {
		name = "'Ali"
		dynasty = Abgaal
		adm = 3
		dip = 4
		mil = 3
	}
}

1678.1.1 = {
	monarch = {
		name = "'Uthman"
		dynasty = Abgaal
		adm = 3
		dip = 5
		mil = 3
	}
}

1720.1.1 = {
	monarch = {
		name = "Muhammad II"
		dynasty = Abgaal
		adm = 4
		dip = 3
		mil = 4
	}
}

1781.1.1 = {
	monarch = {
		name = "Ahmad III"
		dynasty = Abgaal
		adm = 6
		dip = 6
		mil = 5
	}
}