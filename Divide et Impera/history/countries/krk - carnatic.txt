government = caste_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 0
land_naval = -1
quality_quantity = 3
serfdom_freesubjects = -4
primary_culture = kannada
religion = hinduism
technology_group = indian
capital = 1952	# Arcot

#Nawabs of Arcot nominally appointed by Mughal Emperor, de facto French puppets
1692.1.1 = {
	monarch = {
		name = "Zulfikar Ali Khan"
		adm = 7
		dip = 7
		mil = 8
	leader = {	name = "Zulfikar Ali Khan"          	type = general	rank = 0	fire = 2	shock = 3	manuever = 3	siege = 1 }
	}
}


1703.1.1 = {
	monarch = {
		name = "Nawab Daud Khan"
		adm = 7
		dip = 6
		mil = 8
	leader = {	name = "Nawab Daud Khan"          	type = general	rank = 0	fire = 2	shock = 3	manuever = 3	siege = 1 }
	}
}
1710.1.1 = {
	monarch = {
		name = "Muhammad Sa'adat I"
		dynasty = "Saadatullahid"
		adm = 7
		dip = 7
		mil = 8
	}
}

1710.1.1 = {
	heir = {
		name = "Dust 'Ali"
		monarch_name = "Dust 'Ali Khan"
		dynasty = "Saadatullahid"
		birth_date = 1700.1.1
		death_date = 1740.1.1
		claim = 95
		adm = 6
		dip = 7
		mil = 7
	}
}

1712.1.1 = { religion = sunni }

1732.1.1 = {
	monarch = {
		name = "Dust 'Ali Khan"
		dynasty = "Saadatullahid"
		adm = 6
		dip = 7
		mil = 7
	}
}

1732.1.1 = {
	heir = {
		name = "Safdar 'Ali"
		monarch_name = "Safdar 'Ali Khan"
		dynasty = "Saadatullahid"
		birth_date = 1720.1.1
		death_date = 1742.10.2
		claim = 95
		adm = 5
		dip = 3
		mil = 5
	}
}

1740.1.1 = {
	monarch = {
		name = "Safdar 'Ali Khan"
		dynasty = "Saadatullahid"
		adm = 5
		dip = 3
		mil = 5
	}
}

1740.1.1 = {
	heir = {	
		name = "Muhammad Sa'adat"
		monarch_name = "Muhammad Sa'adat II"
		dynasty = "Saadatullahid"
		birth_date = 1720.1.1
		death_date = 1744.7.4
		claim = 95
		adm = 3
		dip = 3
		mil = 3
	}
}

1742.10.2 = {
	monarch = {
		name = "Muhammad Sa'adat II"
		dynasty = "Saadatullahid"
		adm = 3
		dip = 3
		mil = 3
	}
}

1744.7.4 = {
	monarch = {
		name = "Anwar ad-Din Muhammad"
		dynasty = "Anwarudid"
		adm = 7
		dip = 4
		mil = 7
	}
}

1744.7.4 = {
	heir = {
		name = "Wala Jah Muhammad 'Ali"
		monarch_name = "Wala Jah Muhammad 'Ali"
		dynasty = "Anwarudid"
		birth_date = 1720.1.1
		death_date = 1795.1.1
		claim = 95
		adm = 5
		dip = 3
		mil = 4
	}
}

1749.8.3 = {
	monarch = {
		name = "Wala Jah Muhammad 'Ali"
		dynasty = "Anwarudid"
		adm = 5
		dip = 3
		mil = 4
	}
}

1760.1.1 = {
	heir = {
		name = "Umdat Ul-Umra"
		monarch_name = "Umdat Ul-Umra"
		dynasty = "Anwarudid"
		birth_date = 1760.1.1
		death_date = 1801.1.1
		claim = 95
		adm = 5
		dip = 4
		mil = 5
	}
}

1795.10.16 = {
	monarch = {
		name = "Umdat Ul-Umra"
		dynasty = "Anwarudid"
		adm = 5
		dip = 4
		mil = 5
	}
}

1795.10.16 = {
	heir = {
		name = "Azimuddaula"
		monarch_name = "Azimuddaula"
		dynasty = "Anwarudid"
		birth_date = 1780.1.1
		death_date = 1819.1.1
		claim = 95
		adm = 4
		dip = 4
		mil = 5
	}
}

1801.1.1 = {
	monarch = {
		name = "Azimuddaula"
		dynasty = "Anwarudid"
		adm = 4
		dip = 4
		mil = 5
	}
}

1801.1.1 = {
	heir = {
		name = "Azam Jah"
		monarch_name = "Azam Jah"
		dynasty = "Anwarudid"
		birth_date = 1801.1.1
		death_date = 1825.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}

1819.1.1 = {
	monarch = {
		name = "Azam Jah"
		dynasty = "Anwarudid"
		adm = 5
		dip = 5
		mil = 5
	}
}
