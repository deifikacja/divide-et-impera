government = tribal_federation
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = 1
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = 0
land_naval = 5
quality_quantity = 0
serfdom_freesubjects = 0
primary_culture = malay-polynesian
religion = animism
technology_group = pacific
capital = 2038 # Pohnpei

#Sau Deleur
1350.1.1 = {
	monarch = {
		name = "Sau Deleur"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}
1600.1.1 = {
	monarch = {
		name = "San Memuo"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}
#Chiefs
1628.1.1 = {
	monarch = {
		name = "Isohkelekel"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1630.1.1 = {
	monarch = {
		name = "Musei Maur"
		DIP = 5
		ADM = 4
		MIL = 3
	}
}

1700.1.1 = {
	monarch = {
		name = "Luhkenmalada"
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1750.1.1 = {
	monarch = {
		name = "Luhkenkasik"
		DIP = 8
		ADM = 8
		MIL = 2
	}
}