government = tribal_despotism
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -3
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = -1
land_naval = -1
quality_quantity = 0
serfdom_freesubjects = -3
primary_culture = madagasque
religion = animism
technology_group = african
capital = 1274	# Tananarivo


1356.1.1 = {
	monarch = {
		name = "Chief"
		dynasty = Merina
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1500.1.1 = {
	monarch = {
		name = "Andrinamponga"
		dynasty = Merina
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1576.1.1 = {
	monarch = {
		name = "Rakambo"
		dynasty = Merina
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1670.1.1 = {
	monarch = {
		name = "Andriamasinavalona"
		dynasty = Merina
		DIP = 6
		ADM = 9
		MIL = 7
	}
}

1710.8.1 = {
	monarch = {
		name = "Andriantsimitoviaminiandriandrazaka"
		dynasty = Merina
		DIP = 4
		ADM = 6
		MIL = 9
	}
}

1730.5.1 = {
	monarch = {
		name = "Andriambelomasina"
		dynasty = Merina
		DIP = 3
		ADM = 5
		MIL = 4
	}
}

1770.1.1 = {
	monarch = {
		name = "Andrianjafy"
		dynasty = Merina
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1787.3.1 = {
	monarch = {
		name = "Andrianampoinmerina"
		dynasty = Merina
		DIP = 5
		ADM = 5
		MIL = 5
	}
}

1810.1.1 = {
	monarch = {
		name = "Radama I Lehidama"
		dynasty = Merina
		DIP = 7
		ADM = 8
		MIL = 7
	}
}

#Madagascar from 17 october 1828