government = tribal_federation
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = 1
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = 0
land_naval = 5
quality_quantity = 0
serfdom_freesubjects = 0
primary_culture = rapanui
religion = animism
technology_group = pacific
capital = 2058	# Rapa Nui

#'ariki henua of Rapanui
1350.1.1 = {
	monarch = {
		name = "Miru 'a Hata"
		dynasty = Miru
		DIP = 5
		ADM = 5
		MIL = 4
	}
}
1400.1.1 = {
	monarch = {
		name = "Mitiake"
		dynasty = Miru
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1430.1.1 = {
	monarch = {
		name = "Ataranga 'a Miru"
		dynasty = Miru
		DIP = 5
		ADM = 5
		MIL = 4
	}
}

1450.1.1 = {
	monarch = {
		name = "Atu'u Raranga"
		dynasty = Miru
		DIP = 6
		ADM = 9
		MIL = 1
	}
}

1470.1.1 = {
	monarch = {
		name = "Urakikena"
		dynasty = Miru
		DIP = 8
		ADM = 5
		MIL = 2
	}
}

1490.1.1 = {
	monarch = {
		name = "Kahui Tuhunga"
		dynasty = Miru
		DIP = 8
		ADM = 8
		MIL = 2
	}
}

1510.1.1 = {
	monarch = {
		name = "Te Tuhunga Nui"
		dynasty = Miru
		DIP = 6
		ADM = 5
		MIL = 2
	}
}

1530.3.1 = {
	monarch = {
		name = "Te Tuhunga Marakapau"
		dynasty = Miru
		DIP = 3
		ADM = 5
		MIL = 1
	}
}

1550.1.1 = {
	monarch = {
		name = "Ahu Arihao"
		dynasty = Miru
		DIP = 7
		ADM = 8
		MIL = 7
	}
}

1570.1.1 = {
	monarch = {
		name = "Nui Te Patu"
		dynasty = Miru
		DIP = 2
		ADM = 3
		MIL = 2
	}
}

1590.1.1 = {
	monarch = {
		name = "Hirakau Tehito"
		dynasty = Miru
		DIP = 6
		ADM = 6
		MIL = 6
	}
}

1610.1.1 = {
	monarch = {
		name = "Tupu Itetoki"
		dynasty = Miru
		DIP = 6
		ADM = 6
		MIL = 6
	}
}

1630.1.1 = {
	monarch = {
		name = "Kura Ta Hongo"
		dynasty = Miru
		DIP = 3
		ADM = 3
		MIL = 2
	}
}

1650.1.1 = {
	monarch = {
		name = "Hiti Rua Anea"
		dynasty = Miru
		DIP = 3
		ADM = 3
		MIL = 2
	}
}

1670.1.1 = {
	monarch = {
		name = "Havi Nikoro"
		dynasty = Miru
		DIP = 3
		ADM = 3
		MIL = 2
	}
}
1695.1.1 = {
	monarch = {
		name = "Te Ravarava"
		dynasty = Miru
		DIP = 4
		ADM = 5
		MIL = 3
	}
}
1710.1.1 = {
	monarch = {
		name = "Te Raha'i"
		dynasty = Miru
		DIP = 2
		ADM = 2
		MIL = 2
	}
}

1730.1.1 = {
	monarch = {
		name = "Korohaura"
		dynasty = Miru
		DIP = 5
		ADM = 6
		MIL = 5
	}
}
1754.5.1 = {
	monarch = {
		name = "Te Ririkatea"
		dynasty = Miru
		DIP = 4
		ADM = 4
		MIL = 3
	}
}
1770.1.1 = {
	monarch = {
		name = "Kai Mako'i"
		dynasty = Miru
		DIP = 5
		ADM = 5
		MIL = 5
	}
}
1790.1.1 = {
	monarch = {
		name = "Te Hetukarakura"
		dynasty = Miru
		DIP = 5
		ADM = 4
		MIL = 2
	}
}
1810.1.1 = {
	monarch = {
		name = "Huero"
		dynasty = Miru
		DIP = 5
		ADM = 4
		MIL = 2
	}
}