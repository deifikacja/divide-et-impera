government = merchant_republic
aristocracy_plutocracy = -1
centralization_decentralization = 4
innovative_narrowminded = 0
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = -2
offensive_defensive = 3
land_naval = -1
quality_quantity = -1
serfdom_freesubjects = -1
technology_group = western
religion = catholic
primary_culture = hannoverian
capital = 2259	# Bremen

1356.1.1 = {
	monarch = {
		name = "Burgemeister"
		adm = 7
		dip = 6
		mil = 5
	}
}

1529.1.1 = { religion = protestant centralization_decentralization = 2 }

1690.1.1 = {
	monarch = {
		name = "Friedrich Tilemann"
		adm = 5
		dip = 4
		mil = 5
	}
}

1721.1.1 = {
	monarch = {
		name = "Johann von Rheden"
		adm = 7
		dip = 5
		mil = 5
	}
}

1725.1.1 = {
	monarch = {
		name = "Liborius von Line II"
		adm = 5
		dip = 6
		mil = 6
	}
}

1731.1.1 = {
	monarch = {
		name = "Heinrich Smidt"
		adm = 7
		dip = 7
		mil = 8
	}
}

1736.1.1 = {
	monarch = {
		name = "Daniel von B�ren IV"
		adm = 4
		dip = 6
		mil = 4
	}
}

1749.1.1 = {
	monarch = {
		name = "Volkhard Mindemann"
		adm = 3
		dip = 4
		mil = 4
	}
}

1781.1.1 = {
	monarch = {
		name = "Albrecht Gr�ning"
		adm = 5
		dip = 5
		mil = 8
	}
}

1781.6.1 = {
	monarch = {
		name = "Martin Eelking"
		adm = 7
		dip = 5
		mil = 6
	}
}

1782.1.1 = {
	monarch = {
		name = "Gerhard von dem Busch"
		adm = 6
		dip = 4
		mil = 4
	}
}

1798.1.1 = {
	monarch = {
		name = "Jakob Breuls"
		adm = 4
		dip = 4
		mil = 3
	}
}

1803.1.1 = {
	monarch = {
		name = "Heinrich Lampe"
		adm = 4
		dip = 5
		mil = 4
	}
}
