government = caste_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = 1
imperialism_isolationism = 0
secularism_theocracy = -2
offensive_defensive = 0
land_naval = 2
quality_quantity = -1
serfdom_freesubjects = -1
primary_culture = malayan
religion = hinduism
technology_group = chinese
capital = 639	# Banjarmasin


1354.1.1 = {
	monarch = {
		name = "Pangeran Tamanggung"
		dynasty="Negara Daha"
		adm = 4
		dip = 4
		mil = 5
	}
}
1354.1.1 = {
	heir = {
		name = "Sultan"
		monarch_name = "Sultan"
		dynasty = "Negara Daha"
		birth_date = 1354.1.1
		death_date = 1400.1.1
		claim = 95
		adm = 4
		dip = 5
		mil = 6
	}
}
1386.1.1 = { religion = sunni government = eastern_despotism }
1400.1.1 = {
	monarch = {
		name = "Lembu Mangkurat Rar Raden"
		dynasty = "Negara Daha"
		adm = 4
		dip = 4
		mil = 5
	}
	
}

1460.1.1 = {
	monarch = {
		name = "Tunjung Buih"
		dynasty = "Negara Daha"
		adm = 4
		dip = 4
		mil = 5
	}
	
}

1470.1.1 = {
	monarch = {
		name = "Surjanata I"
		dynasty = "Negara Daha"
		adm = 4
		dip = 4
		mil = 5
	}
	
}

1480.1.1 = {
	monarch = {
		name = "Ganggawangsa"
		dynasty = "Negara Daha"
		adm = 4
		dip = 4
		mil = 5
	}
	
}

1490.1.1 = {
	monarch = {
		name = "Kalungsung"
		dynasty = "Negara Daha"
		adm = 4
		dip = 4
		mil = 5
	}
	
}

1495.1.1 = {
	monarch = {
		name = "Sakar Sungsang"
		dynasty = "Negara Daha"
		adm = 4
		dip = 4
		mil = 5
	}
	
}

1500.1.1 = {
	monarch = {
		name = "Sukarama"
		dynasty = "Negara Daha"
		adm = 4
		dip = 4
		mil = 5
	}
	
}

1510.1.1 = {
	monarch = {
		name = "Suryan Syah"
		dynasty = "Negara Daha"
		adm = 4
		dip = 4
		mil = 5
	}
	
}

1540.1.1 = {
	monarch = {
		name = "Rahmatullah"
		dynasty = "Negara Daha"
		adm = 5
		dip = 3
		mil = 4
	}
}

1580.1.1 = {
	monarch = {
		name = "Hidayatullah"
		dynasty = "Negara Daha"
		adm = 7
		dip = 7
		mil = 5
	}
}

1612.1.1 = {
	monarch = {
		name = "Marhum Panembahan"
		dynasty = "Negara Daha"
		adm = 7
		dip = 7
		mil = 5
	}
}

1642.1.1 = {
	monarch = {
		name = "Inayatullah"
		dynasty = "Negara Daha"
		adm = 5
		dip = 7
		mil = 9
	}
}

1650.1.1 = {
	monarch = {
		name = "Saidullah"
		dynasty = "Negara Daha"
		adm = 5
		dip = 6
		mil = 5
	}
}

1660.1.1 = {
	monarch = {
		name = "Amrullah"
		dynasty = "Negara Daha"
		adm = 6
		dip = 3
		mil = 5
	}
	set_country_flag = civil_war
}

1660.5.1 = {
	monarch = {
		name = "Ri'ayatullah"
		dynasty = "Negara Daha"
		adm = 7
		dip = 4
		mil = 5
	}
}

1663.1.1 = {
	monarch = {
		name = "Surya Nata II"
		dynasty = "Negara Daha"
		adm = 6
		dip = 4
		mil = 6
	}
}

1679.1.1 = {
	monarch = {
		name = "Amrullah"
		dynasty = "Negara Daha"
		adm = 5
		dip = 7
		mil = 6
	}
	clr_country_flag = civil_war
}

1700.1.1 = {
	monarch = {
		name = "Tahmid Illah I"
		dynasty = "Negara Daha"
		adm = 9
		dip = 5
		mil = 6
	}
}

1660.1.1 = {
	monarch = {
		name = "Muhammad 'Al�"
		dynasty = "Negara Daha"
		adm = 5
		dip = 5
		mil = 4
	}
}

1717.1.1 = {
	monarch = {
		name = "Panembahan Kusuma Dilaga"
		dynasty = "Negara Daha"
		adm = 3
		dip = 4
		mil = 6
	}
}

1730.1.1 = {
	monarch = {
		name = "Hamidullah"
		dynasty = "Negara Daha"
		adm = 7
		dip = 5
		mil = 4
	}
}

1734.1.1 = {
	monarch = {
		name = "Tamjid Illah I"
		dynasty = "Negara Daha"
		adm = 6
		dip = 4
		mil = 5
	}
}

1759.1.1 = {
	monarch = {
		name = "Muhammad Aminullah"
		dynasty = "Negara Daha"
		adm = 7
		dip = 5
		mil = 6
	}
}

1761.1.1 = {
	monarch = {
		name = "Sultan Nata"
		dynasty = "Negara Daha"
		adm = 5
		dip = 4
		mil = 5
	}
}

1801.1.1 = {
	monarch = {
		name = "Sulaiman Saidullah"
		dynasty = "Negara Daha"
		adm = 6
		dip = 6
		mil = 5
	}
}