government = caste_monarchy
aristocracy_plutocracy = -2
centralization_decentralization = 5
innovative_narrowminded = 3
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 1
offensive_defensive = 3
land_naval = -2
quality_quantity = 2
serfdom_freesubjects = -2
technology_group = chinese
primary_culture = turkmeni
religion = hinduism
capital = 1950	# Dullu

1350.1.1 = {
	monarch = {
		name = "Sangha"
		dynasty = Malla
		adm = 4
		dip = 4
		mil = 4
	}
}

1372.1.1 = {
	monarch = {
		name = "Jithar"
		dynasty = Malla
		adm = 5
		dip = 3
		mil = 4
	}
}

1399.1.1 = {
	monarch = {
		name = "Ji"
		dynasty = Malla
		adm = 4
		dip = 4
		mil = 3
	}
}

1421.1.1 = {
	monarch = {
		name = "Kalan"
		dynasty = Malla
		adm = 5
		dip = 6
		mil = 5
	}
}

1445.1.1 = {
	monarch = {
		name = "Parte"
		dynasty = Malla
		adm = 3
		dip = 4
		mil = 5
	}
}

1464.1.1 = {
	monarch = {
		name = "Puni"
		dynasty = Malla
		adm = 3
		dip = 6
		mil = 5
	}
}

1472.1.1 = {
	monarch = {
		name = "Priti"
		dynasty = Malla
		adm = 5
		dip = 5
		mil = 5
	}
}
#Partitioned between Delhi, Tibet and Kathmandu