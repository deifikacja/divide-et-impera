government = eastern_despotism
aristocracy_plutocracy = -3
centralization_decentralization = 3
innovative_narrowminded = 1
mercantilism_freetrade = -2
imperialism_isolationism = 0
secularism_theocracy = 1
offensive_defensive = 0
land_naval = -1
quality_quantity = 1
serfdom_freesubjects = -3
technology_group = indian
primary_culture = gujarati
religion = sunni
capital = 526 #Radhanpur

1715.1.1 = {
	monarch = {
		name = "Jawan Mard Khan I Bahadur"
		dynasty = Babi
		adm = 5
		dip = 4
		mil = 5
	}

}

1735.4.3 = {
	monarch = {
		name = "Muhammed Sher Khan"
		dynasty = Babi
		adm = 6
		dip = 5
		mil = 4
	}

}

1753.3.21 = {
	monarch = {
		name = "Jawan Mard Khan II Bahadur"
		dynasty = Babi
		adm = 5
		dip = 5
		mil = 7
	}

}

1765.1.1 = {
	monarch = {
		name = "Muhammad Najm ud-din Khan"
		dynasty = Babi
		adm = 4
		dip = 5
		mil = 4
	}

}

1787.9.30 = {
	monarch = {
		name = "Muhammad Ghazi ud-din Khan"
		dynasty = Babi
		adm = 4
		dip = 3
		mil = 3
	}

}

1813.1.6 = {
	monarch = {
		name = "Muhammad Sher Khan I Ghazi"
		dynasty = Babi
		adm = 4
		dip = 4
		mil = 3
	}

}