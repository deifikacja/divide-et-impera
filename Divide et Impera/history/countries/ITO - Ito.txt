government = daimyo
aristocracy_plutocracy = -4
centralization_decentralization = 4
innovative_narrowminded = 2
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = -1
offensive_defensive = 0
land_naval = 0
quality_quantity = 0
serfdom_freesubjects = -5
primary_culture = japanese
religion = shinto
technology_group = chinese
capital = 1358	# Hyuga
daimyo = yes

1350.1.1 = { 
monarch = { 
name = "Daimyo" 
dynasty = Ito 
adm = 5 
dip = 8 
mil = 7 
} 
} 

1350.1.1 = {
    heir = {
        name = "Koremitsu"
        monarch_name =  "Koremitsu" 
        dynasty =  Ito 
        birth_date = 1348.1.1
        death_date = 1378.1.1
        claim = 72
        adm = 5
        dip = 8
        mil = 7
   }
}


1581.1.1 = { 
monarch = { 
name = "Koremitsu" 
dynasty = Ito 
adm = 5 
dip = 8 
mil = 7 
} 
} 

