government = eastern_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 1
offensive_defensive = -1
land_naval = -5
quality_quantity = 3
serfdom_freesubjects = -5
primary_culture = al_iraqiya_arabic
religion = sunni
technology_group = muslim
capital = 429	# Fars

1314.1.1 = {
	monarch = {
		name = "Mubariz ad-Din Muhammad"
		dynasty = Muzaffarid
		adm = 5
		dip = 6
		mil = 5
	}
}

1314.1.1 = {
	heir = {
		name = "Qutb Al-Din Shah Mahmud"
		monarch_name = "Qutb Al-Din Shah Mahmud"
		dynasty = Muzaffarid
		birth_date = 1314.1.1
		death_date = 1366.5.1
		claim = 95
		adm = 5
		dip = 6
		mil = 5
	}
}

1358.1.1 = {
	monarch = {
		name = "Qutb Al-Din Shah Mahmud"
		dynasty = Muzaffarid
		adm = 5
		dip = 6
		mil = 5
	}
}
1358.1.1 = {
	heir = {
		name = "Abu'l Fawaris Djamal ad-Din Shah Shuja"
		monarch_name = "Abu'l Fawaris Djamal ad-Din Shah Shuja"
		dynasty = Muzaffarid
		birth_date = 1358.1.1
		death_date = 1384.1.1
		claim = 95
		adm = 5
		dip = 6
		mil = 5
	}
}

1366.5.1 = {
	monarch = {
		name = "Abu'l Fawaris Djamal ad-Din Shah Shuja"
		dynasty = Muzaffarid
		adm = 5
		dip = 6
		mil = 5
	}
}
1366.5.1 = {
	heir = {
		name = "Mujahid ad-Din Zain Al-Abidin 'Ali"
		monarch_name = "Mujahid ad-Din Zain Al-Abidin 'Ali"
		dynasty = Muzaffarid
		birth_date = 1366.5.1
		death_date = 1387.1.1
		claim = 95
		adm = 5
		dip = 6
		mil = 5
	}
}
1384.1.1 = {
	monarch = {
		name = "Mujahid ad-Din Zain Al-Abidin 'Ali"
		dynasty = Muzaffarid
		adm = 5
		dip = 6
		mil = 5
	}
}
1384.1.1 = {
	heir = {
		name = "Mubariz ad-Din Shah Yahya"
		monarch_name = "Mubariz ad-Din Shah Yahya"
		dynasty = Muzaffarid
		birth_date = 1384.1.1
		death_date = 1391.3.1
		claim = 95
		adm = 4
		dip = 3
		mil = 3
	}
}
1387.1.1 = {
	monarch = {
		name = "Mubariz ad-Din Shah Yahya"
		dynasty = Muzaffarid
		adm = 4
		dip = 3
		mil = 3
	}
}
1387.1.1 = {
	heir = {
		name = "Shah Mansur"
		monarch_name = "Shah Mansur"
		dynasty = Muzaffarid
		birth_date = 1387.1.1
		death_date = 1400.3.1
		claim = 95
		adm = 3
		dip = 3
		mil = 3
	}
}
1391.3.1 = {
	monarch = {
		name = "Shah Mansur"
		dynasty = Muzaffarid
		adm = 3
		dip = 3
		mil = 3
	}
}
#To the Timurids