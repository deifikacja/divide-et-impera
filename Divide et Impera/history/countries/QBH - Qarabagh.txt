government = eastern_despotism
aristocracy_plutocracy = -2
centralization_decentralization = 3
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = 0
land_naval = -4
quality_quantity = 3
serfdom_freesubjects = -2
primary_culture = azerbadjani #local branch of Safavids
religion = shiite
technology_group = muslim
capital = 420	# Bayat

#Djevanshin
1722.1.1 = {
	monarch = {
		name = "Dawud Khan"
		dynasty = Djevanshin
		adm = 4
		dip = 7
		mil = 5
	}
}

1728.1.1 = {
	monarch = {
		name = "Makhitar"
		dynasty = Djevanshin
		adm = 7
		dip = 6
		mil = 5
	}
}

1730.1.1 = {
	monarch = {
		name = "Ughurlu Khan"
		dynasty = Djevanshin
		adm = 3
		dip = 4
		mil = 4
	}
}

1738.1.1 = {
	monarch = {
		name = "Khan Chemskeseka"
		dynasty = Djevanshin
		adm = 5
		dip = 3
		mil = 6
	}
}
#Djevanshir Dynasty
1747.1.1 = {
	monarch = {
		name = "Panah 'Ali Khan"
		dynasty = Djevanshin
		adm = 4
		dip = 3
		mil = 5
	}
}

1759.1.1 = {
	monarch = {
		name = "Ibrahim Khalil"
		dynasty = Djevanshin
		adm = 4
		dip = 4
		mil = 4
	}
}

1806.1.1 = {
	monarch = {
		name = "Mahdi Quli Khan"
		dynasty = Djevanshin
		adm = 4
		dip = 4
		mil = 5
	}
}