government = eastern_despotism
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = -1
offensive_defensive = 0
land_naval = 2
quality_quantity = -1
serfdom_freesubjects = -1
primary_culture = sulawesi
religion = sunni
technology_group = chinese
capital = 1388	# Cianjur

1640.1.1 = {
	monarch = {
		name = "Aria I"
		dynasty = Wiratanu
		adm = 3
		dip = 3
		mil = 3
	}
}

1686.1.1 = {
	monarch = {
		name = "Aria II"
		dynasty = Wiratanu
		adm = 4
		dip = 4
		mil = 8
	}
}

1707.1.1 = {
	monarch = {
		name = "Aria III"
		dynasty = Wiratanu
		adm = 5
		dip = 4
		mil = 5
	}
}

1727.1.1 = {
	monarch = {
		name = "Adipati Wiratanudatar IV"
		dynasty = Wiratanu
		adm = 6
		dip = 8
		mil = 7
	}
}

1761.1.1 = {
	monarch = {
		name = "Adipati Wiratanudatar V"
		dynasty = Wiratanu
		adm = 4
		dip = 6
		mil = 6
	}
}

1776.1.1 = {
	monarch = {
		name = "Adipati Wiratanudatar VI"
		dynasty = Wiratanu
		adm = 4
		dip = 5
		mil = 4
	}
}

1813.1.1 = {
	monarch = {
		name = "Adipati Prawiradiredja I"
		dynasty = Wiratanu
		adm = 3
		dip = 4
		mil = 5
	}
}