government = feudal_monarchy
aristocracy_plutocracy = -1
centralization_decentralization = 4
innovative_narrowminded = 2
mercantilism_freetrade = 1
imperialism_isolationism = 0
secularism_theocracy = -1
offensive_defensive = 1
land_naval = -4
quality_quantity = -1
serfdom_freesubjects = 1
technology_group = western
religion = catholic
primary_culture = gascon
capital = 176	# Pau

 