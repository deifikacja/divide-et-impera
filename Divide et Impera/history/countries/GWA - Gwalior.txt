government = caste_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = -4
land_naval = -1
quality_quantity = -1
serfdom_freesubjects = -2
primary_culture = rajput
religion = hinduism
technology_group = indian
capital = 1969	# Gwalior

# Kachwaha Rajputs

995.1.1 = {
	monarch = {
		name = "Kachwaha"
		DIP = 5
		MIL = 6
		ADM = 5
	}
}
#Tomar Rajputs
1356.1.1 = {
	monarch = {
		name = "Tomar"
		dynasty = Tomar
		DIP = 3
		MIL = 4
		ADM = 3
	}
}

1375.1.1 = {
	monarch = {
		name = "Virsingh"
		dynasty = Tomar
		DIP = 4
		MIL = 5
		ADM = 3
	}
}

1400.1.1 = {
	monarch = {
		name = "Uddharandev"
		dynasty = Tomar
		DIP = 4
		MIL = 4
		ADM = 4
	}
}

1405.1.1 = {
	monarch = {
		name = "Vikramdev"
		dynasty = Tomar
		DIP = 5
		MIL = 5
		ADM = 3
	}
}
1419.1.1 = {
	monarch = {
		name = "Ganapatidev"
		dynasty = Tomar
		DIP = 5
		MIL = 6
		ADM = 7
	}
}
1430.1.1 = {
	monarch = {
		name = "Dungar Singh"
		dynasty = Tomar
		DIP = 4
		MIL = 6
		ADM = 5
	}
}

1486.1.1 = {
	monarch = {
		name = "Man Singh"
		dynasty = Tomar
		DIP = 4
		MIL = 6
		ADM = 5
	}
}
#To Delhi 1517-18
#To Mughal 1518-1731
#Refounded
#Sisodia clan
1731.1.1 = {
	monarch = {
		name = "Ranojirao"
		dynasty = Scindia
		DIP = 5
		MIL = 6
		ADM = 5
	}
}

1745.7.19 = {
	monarch = {
		name = "Jayapparao"
		dynasty = Scindia
		DIP = 3
		MIL = 4
		ADM = 3
	}
}

1755.7.25 = {
	monarch = {
		name = "Jankojirao I"
		dynasty = Scindia
		DIP = 4
		MIL = 5
		ADM = 3
	}
}

1761.1.15 = {
	monarch = {
		name = "Interregnum"
		regent = yes
		DIP = 1
		MIL = 1
		ADM = 1
	}
}

1763.11.25 = {
	monarch = {
		name = "Kadarjirao"
		dynasty = Scindia
		DIP = 5
		MIL = 5
		ADM = 3
	}
}
1764.7.10 = {
	monarch = {
		name = "Manajirao"
		dynasty = Scindia
		DIP = 5
		MIL = 6
		ADM = 7
	}
}
1768.1.18 = {
	monarch = {
		name = "Madhavrao I"
		dynasty = Scindia
		DIP = 4
		MIL = 6
		ADM = 5
	}
}

1794.2.12 = {
	monarch = {
		name = "Daulatrao"
		dynasty = Scindia
		DIP = 4
		MIL = 6
		ADM = 5
	}
}