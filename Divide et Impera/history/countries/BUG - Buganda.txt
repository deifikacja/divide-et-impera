government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -2
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 1
land_naval = -5
quality_quantity = 2
serfdom_freesubjects = -5
primary_culture = bantu
religion = shamanism
technology_group = african
capital = 1290	# Buganda

1350.1.1 = {
	monarch = {
		name = "Chwa I"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1350.1.1 = {
	heir = {
		name = "Kimera"
		monarch_name = "Kimera"
		dynasty = Abalasangeye
		birth_date = 1350.1.1
		death_date = 1404.1.1
		claim = 95
		adm = 4
		dip = 5
		mil = 6
	}
}

1374.1.1 = {
	monarch = {
		name = "Kimera"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1404.1.1 = {
	monarch = {
		name = "Ttembo"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1434.1.1 = {
	monarch = {
		name = "Kiggala"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1464.1.1 = {
	monarch = {
		name = "Kiyimba"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1484.1.1 = {
	monarch = {
		name = "Kayima"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1524.1.1 = {
	monarch = {
		name = "Nakibinge"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1555.1.1 = {
	monarch = {
		name = "Mulondo"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1564.1.1 = {
	monarch = {
		name = "Jemba"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1564.1.1 = {
	monarch = {
		name = "Suuna I"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1614.1.1 = {
	monarch = {
		name = "Sekamaanya"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1634.1.1 = {
	monarch = {
		name = "Kimbugwe"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1644.1.1 = {
	monarch = {
		name = "Kateregga"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1674.1.1 = {
	monarch = {
		name = "Mutebi I"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1680.1.1 = {
	monarch = {
		name = "Juuko"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1690.1.1 = {
	monarch = {
		name = "Kayemba"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1704.1.1 = {
	monarch = {
		name = "Tebandeke"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1724.1.1 = {
	monarch = {
		name = "Ndawula"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1734.1.1 = {
	monarch = {
		name = "Kagulu"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1740.1.1 = {
	monarch = {
		name = "Mwanga I"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1741.1.1 = {
	monarch = {
		name = "Namuggala"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1750.1.1 = {
	monarch = {
		name = "Kyabaggu"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1780.1.1 = {
	monarch = {
		name = "Junju"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1797.1.1 = {
	monarch = {
		name = "Semakookiro"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}

1814.1.1 = {
	monarch = {
		name = "Kamaanya"
		dynasty = Abalasangeye
		adm = 4
		dip = 5
		mil = 6
	}
}