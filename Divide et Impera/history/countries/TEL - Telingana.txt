government = caste_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 3
innovative_narrowminded = 1
mercantilism_freetrade = -4
imperialism_isolationism = 1
secularism_theocracy = 0
offensive_defensive = 0
land_naval = -1
quality_quantity = -1
serfdom_freesubjects = -1
primary_culture = telegu
religion = hinduism
technology_group = indian
capital = 542	# Telingana


1356.1.1 = {
	monarch = {
		name = "Arimatta"
		dynasty = Telinga
		adm = 5
		dip = 4
		mil = 3
	}

}

1400.11.1 = {
	monarch = {
		name = "Indra Narayan"
		dynasty = Telinga
		adm = 8
		dip = 5
		mil = 6
	}

}

1415.3.1 = {
	monarch = {
		name = "Nilambar"
		dynasty = Telinga
		adm = 4
		dip = 5
		mil = 6
	}

}
