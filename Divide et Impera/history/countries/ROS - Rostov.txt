government = feudal_monarchy
aristocracy_plutocracy = -3
centralization_decentralization = 4
innovative_narrowminded = 1
mercantilism_freetrade = -1
offensive_defensive = 0
imperialism_isolationism = 0
secularism_theocracy = 2
land_naval = -3
quality_quantity = 2
serfdom_freesubjects = -5
technology_group = eastern
religion = orthodox
primary_culture = russian
capital = 2162	# Rostov

 

#Princes of Rostov-Borissoglebsk
1321.1.1 = {
	monarch = {
		name = "Konstantin III"
		dynasty = Rurikovich
		DIP = 4
		ADM = 4
		MIL = 4
	}
}

1321.1.1 = {
	heir = {
		name = "Ivan"
		monarch_name = "Ivan I"
		dynasty = Rurikovich
		birth_date = 1321.1.1
		death_date = 1370.1.1
		claim = 95
		DIP = 3
		ADM = 4
		MIL = 4
	}
}

1365.1.1 = {
	monarch = {
		name = "Ivan I"
		dynasty = Rurikovich
		DIP = 3
		ADM = 4
		MIL = 4
	}
}

1370.1.1 = {
	monarch = {
		name = "Vasili III"
		dynasty = Rurikovich
		DIP = 6
		ADM = 5
		MIL = 5
	}
}

1380.1.1 = {
	monarch = {
		name = "Andrei II"
		dynasty = Rurikovich
		DIP = 5
		ADM = 5
		MIL = 5
	}
}
1417.1.1 = {
	monarch = {
		name = "Fedor II"
		dynasty = Rurikovich
		DIP = 5
		ADM = 5
		MIL = 5
	}
}
1418.1.1 = {
	monarch = {
		name = "Ivan II"
		dynasty = Rurikovich
		DIP = 5
		ADM = 5
		MIL = 5
	}
}
1450.1.1 = {
	monarch = {
		name = "Ivan III"
		dynasty = Rurikovich
		DIP = 5
		ADM = 5
		MIL = 5
	}
}
#Annexed by Muscovy in 1474