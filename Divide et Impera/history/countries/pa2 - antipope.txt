government = papal_government
aristocracy_plutocracy = 0
centralization_decentralization = 4
innovative_narrowminded = -1
mercantilism_freetrade = -3
imperialism_isolationism = 1 
secularism_theocracy = 3
offensive_defensive = -1
land_naval = 1
quality_quantity = -1
serfdom_freesubjects = -2
primary_culture = occitain
religion = catholic
technology_group = western
capital = 118	# Roma

1000.1.1 = { set_country_flag = idea_divine_supremacy }
1000.1.1 = { set_country_flag = idea_church_attendance_duty }
1000.1.1 = { set_country_flag = idea_deus_vult }

1000.1.1 = { set_country_flag = event_only_DV }
1000.1.1 = { set_country_flag = inf_country }
1000.1.1 = { set_country_flag = support_antipope }
1000.1.1 = { set_country_flag = vote_for_anti_pope }

1378.11.20 = { 
	monarch = {
		name = "Clement VII"
		adm = 5
		dip = 5
		mil = 5
		}
	}
1394.9.13 = { 
	monarch = { 
		name = "Papal Curia"
		regent = yes
		adm = 4
		dip = 4
		mil = 4
		}
	}
1394.9.28 = { 
	monarch = { 
		name = "Benedict XIII"
		adm = 3 #Abandoned by most of his own cardinals
		dip = 3 #Abandoned by France
		mil = 3 #Forced out of Avignon
		}
	}

1416.1.1 = { primary_culture = umbrian }

1460.1.1 = { set_country_flag = start_after_1460 }

1470.1.1 = { set_country_flag = start_after_1470 }

1471.7.27 = { set_country_flag = swiss_mercenaries }

1480.1.1 = { set_country_flag = start_after_1480 }

1490.1.1 = { set_country_flag = start_after_1490 }

1492.1.1 = { set_country_flag = america1 set_country_flag = america2 }

1494.6.7 = { set_country_flag = tordesillas } #Treaty of Tordesillas

1500.1.1 = { set_country_flag = start_after_1500 }

1506.1.2 = { set_country_flag = swiss_guard }

1510.1.1 = { set_country_flag = start_after_1510 }

1516.1.1 = { set_country_flag = new_latin_bible } #Textus Receptus (Erasmus)

1520.1.1 = { set_country_flag = start_after_1520 }

1524.1.1 = { set_country_flag = peasants1 } #Beginning of the Peasants War

1525.1.1 = { set_country_flag = first_reformation } #Prussia was first to openly break from Rome

1526.1.1 = { set_country_flag = peasants2 } #End of the Peasants War

1527.5.6 = { set_country_flag = sack_of_rome } #Rome sacked by mercenaries in the employ of the Hapsburg emperor

1530.1.1 = { set_country_flag = start_after_1530 }

1537.5.29 = { set_country_flag = sublimus_dei } #Papal Bull Sublimus Dei banned enslavement of natives

1540.1.1 = { set_country_flag = start_after_1540 }

1540.9.27 = { set_country_flag = jesuits_founded }

1549.9.17 = { set_country_flag = clerical_residence }

1550.1.1 = { set_country_flag = start_after_1550 }

1557.1.1 = { set_country_flag = forbidden_index }

1560.1.1 = { set_country_flag = start_after_1560 }

1563.12.4 = { 
	set_country_flag = counter_reformation
	set_country_flag = seminaries
  }

1567.1.1 = { set_country_flag = indulgences_banned }

1570.1.1 = { set_country_flag = start_after_1570 }

1580.1.1 = { set_country_flag = start_after_1580 }

1590.1.1 = { set_country_flag = start_after_1590 }

1600.1.1 = { set_country_flag = start_after_1600 }

1648.10.24 = { set_country_flag = decline_of_papacy } #Peace of Westphalia

