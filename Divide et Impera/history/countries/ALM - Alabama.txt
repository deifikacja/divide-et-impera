government = tribal_democracy
aristocracy_plutocracy = 5
centralization_decentralization = 5
innovative_narrowminded = 5
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = 0
land_naval = -5
quality_quantity = 0
serfdom_freesubjects = 5
primary_culture = creek
religion = shamanism
technology_group = new_world
capital = 924

1350.1.1 = {
	monarch = {
		name = "Chief of Alabama"
		adm = 7
		dip = 5
		mil = 7
	}
}


