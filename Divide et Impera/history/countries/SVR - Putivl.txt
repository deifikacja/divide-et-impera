government = feudal_monarchy
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 3
mercantilism_freetrade = -3
imperialism_isolationism = 0
secularism_theocracy = 1
offensive_defensive = 2
land_naval = -3
quality_quantity = 3
serfdom_freesubjects = -5
primary_culture = ruthenian
religion = orthodox
technology_group = eastern
capital = 2166	# Severia

 

1320.1.1 = {
	monarch = {
		name = "Ivan II"
		dynasty = Rurikovich
		DIP = 3
		ADM = 4
		MIL = 4
	}
}
1370.1.1 = {
	monarch = {
		name = "Aleksandr II Zvenigorodsky"
		dynasty = Rurikovich
		DIP = 4
		ADM = 4
		MIL = 5
	}
}

1390.1.1 = {
	monarch = {
		name = "Fedor II"
		dynasty = Rurikovich
		DIP = 4
		ADM = 4
		MIL = 5
	}
}
1453.1.1 = {
	monarch = {
		name = "Ivan Shemyakin Rilsky"
		dynasty = Rurikovich
		DIP = 4
		ADM = 4
		MIL = 5
	}
}

1480.1.1 = {
	monarch = {
		name = "Vasili II Rilsky"
		dynasty = Rurikovich
		DIP = 4
		ADM = 4
		MIL = 5
	}
}