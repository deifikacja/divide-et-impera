government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 1
land_naval = -5
quality_quantity = 2
serfdom_freesubjects = -5
technology_group = african
religion = animism
primary_culture = yorumba
capital = 1151	# Bonny


1310.1.1 = {
	monarch = {
		name = "Amanyambo"
		dynasty = Ibani
		adm = 8
		dip = 4
		mil = 5
	}
}
1450.1.1 = {
	monarch = {
		name = "Alagbariye"
		dynasty = Ibani
		adm = 8
		dip = 4
		mil = 5
	}
}
1599.1.1 = {
	monarch = {
		name = "Asimini"
		dynasty = Ibani
		adm = 4
		dip = 5
		mil = 4
	}
}

1620.1.1 = {
	monarch = {
		name = "Edimini"
		dynasty = Ibani
		adm = 3
		dip = 4
		mil = 4
	}
}

1640.1.1 = {
	monarch = {
		name = "Kambasa"
		dynasty = Ibani
		adm = 5
		dip = 5
		mil = 7
		female = yes
	}
}

1660.1.1 = {
	monarch = {
		name = "Kamalu"
		dynasty = Ibani
		adm = 6
		dip = 3
		mil = 4
	}
}

1680.1.1 = {
	monarch = {
		name = "Ama Kiri"
		dynasty = Ibani
		adm = 5
		dip = 5
		mil = 6
	}
}

1700.1.1 = {
	monarch = {
		name = "Apia"
		dynasty = Ibani
		adm = 4
		dip = 4
		mil = 4
	}
}

1720.1.1 = {
	monarch = {
		name = "Warri"
		dynasty = Ibani
		adm = 4
		dip = 5
		mil = 6
	}
}

1759.1.1 = {
	monarch = {
		name = "Awusa"
		dynasty = Ibani
		adm = 5
		dip = 3
		mil = 5
	}
}

1760.1.1 = {
	monarch = {
		name = "Perekule I"
		dynasty = Ibani
		adm = 5
		dip = 4
		mil = 7
	}
}

1780.1.1 = {
	monarch = {
		name = "Fubara I Agbaa"
		dynasty = Ibani
		adm = 4
		dip = 5
		mil = 5
	}
}

1792.1.1 = {
	monarch = {
		name = "Opubo Fubara"
		dynasty = Ibani
		adm = 7
		dip = 5
		mil = 3
	}
}