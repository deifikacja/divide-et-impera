government = daimyo
aristocracy_plutocracy = -4
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -5
offensive_defensive = -1
imperialism_isolationism = 0
secularism_theocracy = -1
land_naval = 0
quality_quantity = 4
serfdom_freesubjects = -5
primary_culture = japanese
religion = shinto
technology_group = chinese
capital = 2281	# Shiribeshi
daimyo = yes

1580.1.1 = { 
monarch = { 
name = "Yoshihiro" 
dynasty = Matsumae 
adm = 5 
dip = 8 
mil = 9 
leader = { name = "Matsumae Yoshihiro" type = general rank = 0 fire = 1 shock = 5 manuever = 3 siege = 1 } 
} 
} 

1616.11.20 = { 
monarch = { 
name = "Kinhiro" 
dynasty = Matsumae 
adm = 5 
dip = 8 
mil = 7 
leader = { name = "Matsumae Kinhiro" type = general rank = 0 fire = 1 shock = 3 manuever = 3 siege = 1 } 
} 
} 
