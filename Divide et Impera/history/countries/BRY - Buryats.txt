government = tribal_federation
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 4
mercantilism_freetrade = -2
imperialism_isolationism = -1
secularism_theocracy = -1
offensive_defensive = 0
land_naval = -5
quality_quantity = 4
serfdom_freesubjects = -2
technology_group = ordu
religion = tibetan_buddhism
primary_culture = tuvan
capital = 1060	# Irkutsk

1598.1.1 = {
	monarch = {
		name = "Tului Mergen"
		dynasty = "Genghisid"
		adm = 4
		dip = 3
		mil = 6
	}
}

1639.1.1 = {
	monarch = {
		name = "Nishan"
		dynasty = "Genghisid"
		adm = 5
		dip = 3
		mil = 5
	}
}

1657.1.1 = {
	monarch = {
		name = "Yandash"
		dynasty = "Genghisid"
		adm = 5
		dip = 5
		mil = 5
	}
}

1693.1.1 = {
	monarch = {
		name = "Ashir"
		dynasty = "Genghisid"
		adm = 4
		dip = 4
		mil = 5
	}
}

1719.1.1 = {
	monarch = {
		name = "Bulat Ba'atur"
		dynasty = "Genghisid"
		adm = 4
		dip = 3
		mil = 4
	}
}

1744.1.1 = {
	monarch = {
		name = "Enkhe"
		dynasty = "Genghisid"
		adm = 5
		dip = 5
		mil = 5
	}
}

1779.1.1 = { 
	monarch = {
		name = "Rinchen Dorzhin"
		dynasty = "Genghisid"
		adm = 4
		dip = 3
		mil = 6
	}
}