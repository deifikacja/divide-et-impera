government = tribal_federation
aristocracy_plutocracy = 0
centralization_decentralization = 3
innovative_narrowminded = 2
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = 3
land_naval = -5
quality_quantity = 5
serfdom_freesubjects = 5
primary_culture = catawban
religion = shamanism
technology_group = new_world
capital = 913


1352.1.1 = {
	monarch = {
		name = "Chief of Catawba"
		adm = 5
		dip = 4
		mil = 5
	}
}