government = tribal_federation
aristocracy_plutocracy = 0
centralization_decentralization = 5
innovative_narrowminded = 0
mercantilism_freetrade = 0
imperialism_isolationism = 0
secularism_theocracy = 0
offensive_defensive = -5
land_naval = -5
quality_quantity = 0
serfdom_freesubjects = 5
primary_culture = dakota
religion = shamanism
technology_group = new_world
capital = 904


1351.1.1 = {
	monarch = {
		name = "Chief of Nakota"
		adm = 4
		dip = 3
		mil = 7
	}
}