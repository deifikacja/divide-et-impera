government = eastern_despotism
aristocracy_plutocracy = -2
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = 1
land_naval = -1
quality_quantity = 3
serfdom_freesubjects = -2
primary_culture = azerbadjani
religion = shiite
technology_group = muslim
capital = 1340	# Ardebil


1747.1.1 = {
	monarch = {
		name = "Badir Khan"
		dynasty="Badiroglu"
		adm = 4
		dip = 7
		mil = 5
	}
}

1763.1.1 = {
	monarch = {
		name = "Nazarali Khan"
		dynasty="Badiroglu"
		adm = 7
		dip = 6
		mil = 5
	}
}

1792.1.1 = {
	monarch = {
		name = "Nasir Khan"
		dynasty="Badiroglu"
		adm = 9
		dip = 8
		mil = 8
	}
}