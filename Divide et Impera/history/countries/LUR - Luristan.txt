government = eastern_despotism
aristocracy_plutocracy = -3
centralization_decentralization = 5
innovative_narrowminded = 1
mercantilism_freetrade = -4
imperialism_isolationism = 0
secularism_theocracy = -1
offensive_defensive = -2
land_naval = -2
quality_quantity = 1
serfdom_freesubjects = -4
primary_culture = persian
religion = sunni
technology_group = muslim
capital = 413	# Luristan

1349.1.1 = {
	monarch = {
		name = "al-Malik 'Izz ad-Din Hussein II"
		dynasty = "Banu Hussein"
		adm = 5
		dip = 6
		mil = 5
	}
}

1405.1.1 = {
	monarch = {
		name = "Sayyid Ahmad II"
		dynasty = "Banu Hussein"
		adm = 5
		dip = 6
		mil = 5
	}
}

1412.5.1 = {
	monarch = {
		name = "Shah Husayn al-Abbasi"
		dynasty = "Banu Hussein"
		adm = 5
		dip = 6
		mil = 5
	}
}
1468.1.1 = {
	monarch = {
		name = "Shah Rustam"
		dynasty = "Banu Hussein"
		adm = 5
		dip = 6
		mil = 5
	}
}
#To Persia 1503
#Chaos after Nadir Shah's death
1747.1.1 = {
	monarch = {
		name = "Ismail I"
		dynasty = "Luristani"
		adm = 4
		dip = 3
		mil = 3
	}
}


1760.3.1 = {
	monarch = {
		name = "Muhammad"
		dynasty = "Luristani"
		adm = 3
		dip = 3
		mil = 3
	}
}
