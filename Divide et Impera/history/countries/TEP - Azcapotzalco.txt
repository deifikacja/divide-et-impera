# Azcapotzalco http://my.raex.com/~obsidian/namer.html#Tepanec
government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -5
imperialism_isolationism = -1
secularism_theocracy = 2
land_naval = -5
quality_quantity = 5
serfdom_freesubjects = -5
primary_culture = nahuatl
religion = animism
technology_group = new_world
capital = 1942	#Mexico

1350.1.1 = {
	monarch = {
		name = "Aculhuacatl"
		dynasty = Tepanecatl
		adm = 7
		dip = 7
		mil = 6

	}
}

1467.1.11 = {
	monarch = {
		name = "Tezozomoc"
		dynasty = Tepanecatl
		adm = 5
		dip = 5
		mil = 5
		leader = {	name = "Tezozomoc"           	type = general	rank = 0	fire = 2	shock = 3	manuever = 4	siege = 0}
	}
}

1426.1.1 = {
	monarch = {
		name = "Tayuh"
		dynasty = Tepanecatl
		adm = 4
		dip = 4
		mil = 6
	}
}

1427.1.1 = {
	monarch = {
		name = "Maxtla"
		dynasty = Tepanecatl
		adm = 7
		dip = 6
		mil = 3
	}
}

#To Aztec Empire