government = caste_monarchy
aristocracy_plutocracy = -2
centralization_decentralization = 3
innovative_narrowminded = 2
mercantilism_freetrade = -4
imperialism_isolationism = 1
secularism_theocracy = 0
offensive_defensive = 0
land_naval = 0
quality_quantity = 3
serfdom_freesubjects = -2
technology_group = indian
religion = hinduism
primary_culture = malayalam
capital = 535	# Padmanabhapuram


#Rajas of Venad#
1350.1.1 = {
	monarch = {
		name = "Iravi Iravi Varma"
		dynasty = "Kulasekhara"
		DIP = 7
		ADM = 8
		MIL = 6
	}
}

1356.1.1 = {
	heir = {
		name = "Adiyta Varma Sarvanyanatha"
		monarch_name = "Adiyta Varma Sarvanyanatha"
		dynasty = "Kulasekhara"
		birth_date = 1356.1.1
		death_date = 1383.1.1
		claim = 95
		DIP = 7
		ADM = 8
		MIL = 6
	}
}

1376.1.1 = {
	monarch = {
		name = "Adiyta Varma Sarvanyanatha"
		dynasty = "Kulasekhara"
		DIP = 7
		ADM = 8
		MIL = 6
	}
}

1376.1.1 = {
	heir = {
		name = "Chera Udaya Varma"
		monarch_name = "Chera Udaya Varma"
		dynasty = "Kulasekhara"
		birth_date = 1376.1.1
		death_date = 1400.1.1
		claim = 95
		DIP = 6
		ADM = 4
		MIL = 5
	}
}

1383.1.1 = {
	monarch = {
		name = "Chera Udaya Varma"
		dynasty = "Kulasekhara"
		DIP = 6
		ADM = 4
		MIL = 5
	}
}

1400.1.1 = {
	heir = {
		name = "Venad Mootha"
		monarch_name = "Venad Mootha Raja"
		dynasty = "Kulasekhara"
		birth_date = 1400.1.1
		death_date = 1458.1.1
		claim = 95
		adm = 6
		dip = 6
		mil = 3
	}
}

1444.1.1 = {
	monarch = {
		name = "Venad Mootha Raja"
		dynasty = "Kulasekhara"
		DIP = 6
		ADM = 6
		MIL = 3
	}
}

1444.1.1 = {
	heir = {
		name = "Sri Vira Martanda Varma"
		monarch_name = "Sri Vira Martanda Varma"
		dynasty = "Kulasekhara"
		birth_date = 1440.1.1
		death_date = 1471.1.1
		claim = 95
		adm = 5
		dip = 4
		mil = 4
	}
}

1458.1.1 = {
	monarch = {
		name = "Sri Vira Martanda Varma"
		dynasty = "Kulasekhara"
		DIP = 4
		ADM = 5
		MIL = 4
	}
}

1458.1.1 = {
	heir = {
		name = "Aditya Varma"
		monarch_name = "Aditya Varma"
		dynasty = "Kulasekhara"
		birth_date = 1450.1.1
		death_date = 1478.1.1
		claim = 95
		adm = 3
		dip = 7
		mil = 5
	}
}

1471.1.1 = {
	monarch = {
		name = "Aditya Varma" 
		dynasty = "Kulasekhara"
		DIP = 7
		ADM = 3
		MIL = 5
	}
}

1471.1.1 = {
	heir = {
		name = "Aditya Varma"
		monarch_name = "Aditya Varma"
		dynasty = "Kulasekhara"
		birth_date = 1460.1.1
		death_date = 1504.1.1
		claim = 95
		adm = 6
		dip = 3
		mil = 3
	}
}

1478.1.1 = {
	monarch = {
		name = "Ravi Varma"
		dynasty = "Kulasekhara"
		DIP = 3
		ADM = 6
		MIL = 3
	}
}

1478.1.1 = {
	heir = {
		name = "Sri Vira Ravi Varma"
		monarch_name = "Sri Vira Ravi Varma"
		dynasty = "Kulasekhara"
		birth_date = 1478.1.1
		death_date = 1528.1.1
		claim = 95
		adm = 3
		dip = 6
		mil = 4
	}
}

1504.1.1 = {
	monarch = {
		name = "Sri Vira Ravi Varma"
		dynasty = "Kulasekhara"
		DIP = 6
		ADM = 3
		MIL = 4
	}
}

1504.1.1 = {
	heir = {
		name = "Martanda Varma"
		monarch_name = "Martanda Varma"
		dynasty = "Kulasekhara"
		birth_date = 1504.1.1
		death_date = 1537.1.1
		claim = 95
		adm = 4
		dip = 5
		mil = 3
	}
}

1528.1.1 = {
	monarch = {
		name = "Martanda Varma"
		dynasty = "Kulasekhara"
		DIP = 5
		ADM = 4
		MIL = 3
	}
}

1528.1.1 = {
	heir = {
		name = "Udaya Martanda Varma"
		monarch_name = "Udaya Martanda Varma"
		dynasty = "Kulasekhara"
		birth_date = 1515.1.1
		death_date = 1550.1.1
		claim = 95
		adm = 6
		dip = 6
		mil = 4
	}
}

1537.1.1 = {
	monarch = {
		name = "Udaya Martanda Varma"
		dynasty = "Kulasekhara"
		DIP = 6
		ADM = 6
		MIL = 4
	}
}

1537.1.1 = {
	heir = {
		name = "Kerala Varma"
		monarch_name = "Kerala Varma"
		dynasty = "Kulasekhara"
		birth_date = 1530.1.1
		death_date = 1552.1.1
		claim = 95
		adm = 4
		dip = 8
		mil = 4
	}
}

1550.1.1 = {
	monarch = {
		name = "Kerala Varma"
		dynasty = "Kulasekhara"
		DIP = 8
		ADM = 4
		MIL = 4
	}
}

1550.1.1 = {
	heir = {
		name = "Aditya Varma"
		monarch_name = "Aditya Varma"
		dynasty = "Kulasekhara"
		birth_date = 1540.1.1
		death_date = 1567.1.1
		claim = 95
		adm = 6
		dip = 3
		mil = 5
	}
}

1553.1.1 = {
	monarch = {
		name = "Aditya Varma"
		dynasty = "Kulasekhara"
		DIP = 3
		ADM = 6
		MIL = 5
	}
}

1553.1.1 = {
	heir = {
		name = "Udaya Martanda Varma"
		monarch_name = "Udaya Martanda Varma"
		dynasty = "Kulasekhara"
		birth_date = 1530.1.1
		death_date = 1594.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 3
	}
}

1567.1.1 = {
	monarch = {
		name = "Udaya Martanda Varma"
		dynasty = "Kulasekhara"
		DIP = 5
		ADM = 5
		MIL = 3
	}
}

1570.1.1 = {
	heir = {
		name = "Sri Vira Kulasekhara"
		monarch_name = "Sri Vira Kulasekhara"
		dynasty = "Kulasekhara"
		birth_date = 1570.1.1
		death_date = 1604.1.1
		claim = 95
		adm = 5
		dip = 4
		mil = 5
	}
}

1594.1.1 = {
	monarch = {
		name = "Sri Vira Kulasekhara"
		dynasty = "Kulasekhara"
		DIP = 4
		ADM = 5
		MIL = 5
	}
}

1594.1.1 = {
	heir = {
		name = "Sri Vira Varma"
		monarch_name = "Sri Vira Varma"
		dynasty = "Kulasekhara"
		birth_date = 1580.1.1
		death_date = 1606.1.1
		claim = 95
		adm = 7
		dip = 4
		mil = 3
	}
}

1604.1.1 = {
	monarch = {
		name = "Sri Vira Varma"
		dynasty = "Kulasekhara"
		DIP = 4
		ADM = 7
		MIL = 3
	}
}

1604.1.1 = {
	heir = {
		name = "Ravi Varma"
		monarch_name = "Ravi Varma"
		dynasty = "Kulasekhara"
		birth_date = 1580.1.1
		death_date = 1619.1.1
		claim = 95
		adm = 3
		dip = 3
		mil = 3
	}
}

1606.1.1 = {
	monarch = {
		name = "Ravi Varma"
		dynasty = "Kulasekhara"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}

1606.1.1 = {
	heir = {
		name = "Unni Kerala Varma"
		monarch_name = "Unni Kerala Varma"
		dynasty = "Kulasekhara"
		birth_date = 1600.1.1
		death_date = 1625.1.1
		claim = 95
		adm = 6
		dip = 4
		mil = 6
	}
}

1619.1.1 = {
	monarch = {
		name = "Unni Kerala Varma"
		dynasty = "Kulasekhara"
		DIP = 4
		ADM = 6
		MIL = 6
	}
}

1619.1.1 = {
	heir = {
		name = "Ravi Varma"
		monarch_name = "Ravi Varma"
		dynasty = "Kulasekhara"
		birth_date = 1600.1.1
		death_date = 1631.1.1
		claim = 95
		adm = 7
		dip = 3
		mil = 5
	}
}

1625.1.1 = {
	monarch = {
		name = "Ravi Varma"
		dynasty = "Kulasekhara"
		DIP = 3
		ADM = 7
		MIL = 5
	}
}

1625.1.1 = {
	heir = {
		name = "Unni Kerala Varma"
		monarch_name = "Unni Kerala Varma II"
		dynasty = "Kulasekhara"
		birth_date = 1615.1.1
		death_date = 1661.1.1
		claim = 95
		adm = 4
		dip = 7
		mil = 4
	}
}

1631.1.1 = {
	monarch = {
		name = "Unni Kerala Varma II"
		dynasty = "Kulasekhara"
		DIP = 7
		ADM = 4
		MIL = 4
	}
}

1640.1.1 = {
	heir = {
		name = "Aditya Varma"
		monarch_name = "Aditya Varma"
		dynasty = "Kulasekhara"
		birth_date = 1640.1.1
		death_date = 1678.1.1
		claim = 95
		adm = 3
		dip = 3
		mil = 6
	}
}

1661.1.1 = {
	monarch = {
		name = "Aditya Varma"
		dynasty = "Kulasekhara"
		DIP = 3
		ADM = 3
		MIL = 6
	}
}

1661.1.1 = {
	heir = {
		name = "Raja Ravi Varma"
		monarch_name = "Raja Ravi Varma"
		dynasty = "Kulasekhara"
		birth_date = 1660.1.1
		death_date = 1704.1.1
		claim = 95
		adm = 6
		dip = 5
		mil = 5
	}
}
		
1678.1.1 = {
	monarch = {
		name = "Raja Ravi Varma"
		dynasty = "Kulasekhara"
		DIP = 5
		ADM = 6
		MIL = 5
	}
}

1678.1.1 = {
	heir = {
		name = "Unni Kerala Varma"
		monarch_name = "Unni Kerala Varma III"
		dynasty = "Kulasekhara"
		birth_date = 1678.1.1
		death_date = 1718.1.1
		claim = 95
		adm = 5
		dip = 7
		mil = 5
	}
}

1704.1.1 = {
	monarch = {
		name = "Unni Kerala Varma III"
		dynasty = "Kulasekhara"
		DIP = 7
		ADM = 5
		MIL = 5
	}
}

1704.1.1 = {
	heir = {
		name = "Raja Aditya Varma"
		monarch_name = "Raja Aditya Varma"
		dynasty = "Kulasekhara"
		birth_date = 1704.1.1
		death_date = 1721.1.1
		claim = 95
		adm = 6
		dip = 6
		mil = 3
	}
}

1718.1.1 = {
	monarch = {
		name = "Raja Aditya Varma"
		dynasty = "Kulasekhara"
		DIP = 6
		ADM = 6
		MIL = 3
	}
}

1718.1.1 = {
	heir = {
		name = "Raja Rama Varma"
		monarch_name = "Raja Rama Varma"
		dynasty = "Kulasekhara"
		birth_date = 1700.1.1
		death_date = 1729.1.27
		claim = 95
		adm = 3
		dip = 4
		mil = 4
	}
}

1721.1.1 = {
	monarch = {
		name = "Raja Rama Varma"
		dynasty = "Kulasekhara"
		DIP = 4
		ADM = 3
		MIL = 4
	}
}

1721.1.1 = {
	heir = {
		name = "Martanda Varma"
		monarch_name = "Maharaj Martanda Varma I"
		dynasty = "Kulasekhara"
		birth_date = 1700.1.1
		death_date = 1758.7.7
		claim = 95
		adm = 7
		dip = 8
		mil = 9
	}
}

1729.1.27 = {
	monarch = {
		name = "Maharaj Martanda Varma I"
		dynasty = "Kulasekhara"
		DIP = 8
		ADM = 7
		MIL = 9
	}
}

1729.1.27 = {
	heir = {
		name = "Rama Varma"
		monarch_name = "Maharaj Rama Varma II"
		dynasty = "Kulasekhara"
		birth_date = 1729.1.1
		death_date = 1798.2.17
		claim = 95
		adm = 5
		dip = 7
		mil = 6
	}
}

1758.7.7 = {
	monarch = {
		name = "Maharaj Rama Varma II"
		dynasty = "Kulasekhara"
		DIP = 7
		ADM = 5
		MIL = 6
	}
}

1758.7.7 = {
	heir = {
		name = "Bala Rama Varma"
		monarch_name = "Maharaj Bala Rama Varma I"
		dynasty = "Kulasekhara"
		birth_date = 1750.1.1
		death_date = 1810.1.1
		claim = 95
		adm = 4
		dip = 3
		mil = 3
	}
}

1798.2.17 = {
	monarch = {
		name = "Maharaj Bala Rama Varma I"
		dynasty = "Kulasekhara"
		DIP = 3
		ADM = 4
		MIL = 3
	}
}

1798.2.17 = {
	heir = {
		name = "Gouri Laksmi Bai"
		monarch_name = "Gouri Laksmi Bai"
		dynasty = "Kulasekhara"
		female = yes
		birth_date = 1780.1.1
		death_date = 1815.1.1
		claim = 95
		adm = 4
		dip = 5
		mil = 5
	}
}

1810.1.1 = {
	monarch = {
		name = "Gouri Laksmi Bai"
		dynasty = "Kulasekhara"
		DIP = 5
		ADM = 4
		MIL = 5
		female = yes
	}
}

1810.1.1 = {
	heir = {
		name = "Gouri Parvati Bai"
		monarch_name = "Gouri Parvati Bai"
		dynasty = "Kulasekhara"
		birth_date = 1800.1.1
		death_date = 1829.1.1
		claim = 95
		adm = 5
		dip = 5
		mil = 5
	}
}
		

1815.1.1 = {
	monarch = {
		name = "Gouri Parvati Bai"
		dynasty = "Kulasekhara"
		DIP = 5
		ADM = 5
		MIL = 5
		female = yes
	}
}
