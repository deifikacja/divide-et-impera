government = tribal_despotism
aristocracy_plutocracy = -5
centralization_decentralization = 5
innovative_narrowminded = 2
mercantilism_freetrade = -5
imperialism_isolationism = 0
secularism_theocracy = 2
offensive_defensive = -3
land_naval = -5
quality_quantity = 5
serfdom_freesubjects = -5
primary_culture = mixtec
religion = animism
technology_group = new_world
capital = 847	#Mixtec


1351.1.1 = {
	monarch = {
		name = "Malpozpoca"
		dynasty = Mixteca
		adm = 4
		dip = 4
		mil = 4
	}
}

1520.1.1 = {
	monarch = {
		name = "Sicuane"
		dynasty = Mixteca
		adm = 4
		dip = 4
		mil = 4
	}
}