#364 - Gaza

owner = MAM
controller = MAM
culture = al_misr_arabic
religion = sunni
capital = "Gaza"
trade_goods = cloth
hre = no
base_tax = 2
manpower = 1
citysize = 2100
fort1 = yes
add_core = MAM
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ

1450.1.1 = { citysize = 2250 }
1500.1.1 = { citysize = 2400 }
1517.1.1 = { owner = TUR controller = TUR add_core = TUR } # Conquered by the Ottomans
1550.1.1 = { citysize = 2910 }
1600.1.1 = { citysize = 3330 }
1650.1.1 = { citysize = 2005 } # Decline in trade, agriculture and population
1690.1.1 = { marketplace = yes }
1700.1.1 = { citysize = 2802 }
1750.1.1 = { citysize = 3465 }
1800.1.1 = { citysize = 3800 }