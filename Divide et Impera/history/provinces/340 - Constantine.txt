#340 - Constantine

owner = TUN
controller = TUN
culture = berber
religion = sunni
capital = "Constantine"
trade_goods = grain
hre = no
base_tax = 3
manpower = 2
citysize = 17000
fort1 = yes
add_core = ALG
add_core = QUS
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = FZZ
discovered_by = TUG

1450.1.1 = { citysize = 19600 owner = ALG controller = ALG }
1500.1.1 = { citysize = 20110 }
1550.1.1 = { citysize = 22648 }
1567.1.1 = { owner = QUS controller = QUS }
1600.1.1 = { citysize = 24657 }
1650.1.1 = { citysize = 26341 marketplace = yes }
1700.1.1 = { citysize = 28514 }
1750.1.1 = { citysize = 30177 tax_assessor = yes }
1800.1.1 = { citysize = 33210 }