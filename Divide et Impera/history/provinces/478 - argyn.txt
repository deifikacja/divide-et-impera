#478 - Argyn

owner = GOL
controller = GOL
culture = khazak
religion = sunni
capital = "Argyn"
trade_goods = wool
hre = no
base_tax = 1
manpower = 1
citysize = 800
add_core = SHY
add_core = KZH
discovered_by = KZH
discovered_by = KAZ
discovered_by = GOL

1446.1.1 = { owner = SHY controller = SHY }
1450.1.1 = { citysize = 1500 }
1460.1.1 = { owner = KZH controller = KZH }
1500.1.1 = { citysize = 1988 }
1550.1.1 = { citysize = 2570 }
1600.1.1 = { citysize = 3124 }
1622.1.1 = { discovered_by = RUS }
1650.1.1 = { citysize = 3780 }
1652.1.1 = { owner = OIR controller = OIR }#The Kalmyk conquest of Junior Juz
1680.1.1 = { owner = KZH controller = KZH }#Recaptured
1700.1.1 = { citysize = 4231}
1718.1.1 = { owner = ULZ controller = ULZ }
1731.1.1 = { owner = ORZ controller = ORZ }
1750.1.1 = { citysize = 4807 }
1798.1.1 = {	owner = RUS
		controller = RUS
		add_core = RUS
	   } # The Middle horde is conquered
1800.1.1 = { citysize = 5200 }