#1956 - Jhabua

owner = DLH
controller = DLH
culture = rajput
religion = jainism
capital = "Jhabua"
trade_goods = cattle
hre = no
base_tax = 2
manpower = 1
citysize = 2000
add_core = MLW
add_core = DLH
add_core = JHA
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1388.9.20 = { revolt_risk = 4 } #death of Firuz Shah, dynasty crisis began
1389.2.19 = { revolt_risk = 8 } #Tughluq Shah II is murdered in Delhi
1390.1.1 = { owner = MLW controller = MLW revolt_risk = 0 }
1450.1.1 = { citysize = 7070 }
1498.1.1 = { discovered_by = POR }
1500.1.1 = { citysize = 8350 }
1518.1.1 = {	revolt_risk = 5
		add_core = GUJ
	   } # The Malwa sultanate began to collapse after several intrusions from the Gujarat
1531.1.1 = {	owner = GUJ
		controller = GUJ
		revolt_risk = 0
	   } # Sultanate of Gujarat conquered the Malwa Sultanate
1535.1.1 = { owner = MLW controller = MLW }
1542.1.1 = {	owner = MUG
		controller = MUG
		add_core = MUG
	   } # Conquered by the Akbar
1550.1.1 = { citysize = 9753 }
1555.1.1 = { owner = MLW controller = MLW }
1562.1.1 = {	owner = MUG
		controller = MUG
	   }
1584.1.1 = {	owner = JHA
		controller = JHA
		remove_core = MLW
	   } # Jhabua founded
1600.1.1 = { citysize = 11900 marketplace = yes }
1650.1.1 = { citysize = 13320 }
1680.1.1 = { courthouse = yes }
1700.1.1 = { citysize = 14830 }
1750.1.1 = { citysize = 16000 }
1800.1.1 = { citysize = 18240 }