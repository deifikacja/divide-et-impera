#1145 - Nupe

add_core = NUP
culture = yorumba   
religion = animism
capital = "Nupe"
trade_goods = unknown
hre = no
base_tax = 2
manpower = 1
native_size = 60
native_ferocity = 4.5
native_hostileness = 9
discovered_by = OKO
discovered_by = OYO
discovered_by = BEN
discovered_by = NUP
discovered_by = IFE
discovered_by = SON
discovered_by = BUS

1450.1.1 = { owner = NUP controller = NUP trade_goods = grain citysize = 1000 }
1484.1.1 = { revolt_risk = 6 } #Campaigns by the Katsina Hausa against their Nupe enemies
1500.1.1 = { citysize = 1500 }
1600.1.1 = { discovered_by = OYO citysize = 2000 }
1670.1.1 = { owner = OYO controller = OYO  add_core = OYO revolt_risk = 3 citysize = 5000 manpower = 1 base_tax = 2 trade_goods = slaves } #Oyo establishes hegemony over Nupe
1690.1.1 = { revolt_risk = 6 } #Uprising against Oyo domination
1700.1.1 = { citysize = 6500 }
1790.1.1 = { revolt_risk = 3 } #Islamic revival led by Usman dan Fodio gains infleunce in Nupe
1791.1.1 = {    owner = NUP
		controller = NUP
		revolt_risk = 3
	   } #Nupe ejects Oyo overlords
1800.1.1 = { citysize = 7500 }
1804.1.1 = { discovered_by = SOK }