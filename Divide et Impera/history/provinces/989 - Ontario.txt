#989 - Ontario

owner = HUR
controller = HUR
culture = huron
religion = shamanism
capital = "Ontario"
trade_goods = grain
hre = no
base_tax = 3
manpower = 1
citysize = 1500
add_core = HUR
discovered_by = HUR
discovered_by = IRO
discovered_by = OTT
discovered_by = ABN

1450.1.1  = { citysize = 1900 }
1500.1.1  = { citysize = 2010 }
1550.1.1  = { citysize = 2450 }
1600.1.1  = { citysize = 3250 }
1609.1.1  = { discovered_by = FRA } # Samuel de Champlain.
1611.1.1  = { discovered_by = ENG } # Henry Hudson
1648.1.1 = { owner = IRO controller = IRO culture = iroquis citysize = 110 }#Defeat of the Huron in Beaver Wars
1673.1.1  = {	owner = FRA
		controller = FRA
		culture = cosmopolitan_french
		religion = catholic
		capital = "Fort Frontenac"
	    } # Construction of Fort Frontenac
1674.1.1  = { fort1 = yes } # Fort Frontenac
1697.1.1  = { add_core = FRA }
1700.1.1  = { citysize = 780 }
1707.5.12 = { discovered_by = GBR } 
1750.1.1  = { citysize = 1480 }
1763.2.10 = {	owner = GBR
		controller = GBR
		culture = english
		religion = episcopalism
		remove_core = FRA
	    } # Treaty of Paris
1774.1.1  = { add_core = QUE } # United with Canada under the Quebec Act of 1774
1788.2.10 = { add_core = GBR }
1791.1.1  = { add_core = CAN remove_core = QUE } # The Constitutional Act, Quebec is split into upper & lower Canada
1793.7.29 = { capital = "York" }
1800.1.1  = { citysize = 6870 } # The settlements grew quickly after the American revolution
