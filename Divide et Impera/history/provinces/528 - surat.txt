#528 - Surat

owner = DLH
controller = DLH
culture = gujarati
religion = jainism
capital = "Surat"
trade_goods = cotton
hre = no
base_tax = 7
manpower = 3
citysize = 34000
add_core = GUJ
add_core = DLH
fort1 = yes
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = MUG

1388.9.20 = { revolt_risk = 4 } #death of Firuz Shah, dynasty crisis began
1389.2.19 = { revolt_risk = 8 } #Tughluq Shah II is murdered in Delhi
1390.8.31 = { revolt_risk = 3 } #Muhammad Shah III enters Delhi
1392.6.1  = { revolt_risk = 9 } #chaos in sultanate
1393.1.1 = { revolt = { type = nationalist_rebels size = 3 } controller = REB }
1396.1.1 = { revolt = {} owner = GUJ controller = GUJ remove_core = DLH revolt_risk = 0 }
1498.1.1 = { discovered_by = POR }
1500.1.1 = { citysize = 36577 }
1540.1.1 = { fort1 = yes }
1550.1.1 = { citysize = 38670 }
1576.1.1 = {	owner = MUG
		controller = MUG
		add_core = MUG
	   } # Conquered by Akbar
1600.1.1 = { citysize = 42388 marketplace = yes } # add English trading post
1627.1.1 = { textile = yes }
1650.1.1 = { citysize = 46300 customs_house = yes } 
1660.1.1 = { add_core = MAR } # Series of attacks from the Marathas
1664.1.1 = { citysize = 35700 } # Surat began to decline after it was sacked by the Marathas
1674.6.16 = {	owner = MAR
		controller = MAR
		remove_core = MUG
	   } #  Conquered by the Marathas
1700.1.1 = { citysize = 36868 }
1750.1.1 = { add_core = GAK citysize = 37215 }
1759.1.1 = { owner = GBR controller = GBR } # Conquered by the British
1784.1.1 = { add_core = GBR }
1800.1.1 = { citysize = 61200 }
