# 202 Avignon - Principal cities: Avignon

owner = PAP
controller = PAP
capital = "Avignon"
citysize = 12500
culture = occitain
religion = catholic
hre = no
base_tax = 5
trade_goods = cloth
manpower = 1
add_core = AVI
add_core = PAP
fort1 = yes
marketplace = yes
tax_assessor = yes # Exception: center of banking and finance
temple = yes # La Palais des Papes
courthouse = yes # Exception, Papal justice well established
fort2 = yes # Large exception: Le Palais des Papes is super strong, and build on and beneath a natural rock formation
#university = yes # L'Universit� d'Avignon
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1378.9.20  = { owner = PA2 controller = PA2 add_core = PA2 } #election of Clement VII, Western Schism begins
1400.1.1   = { citysize = 14000 }
1417.10.18 = { owner = PAP controller = PAP remove_core = PA2 } #end of the West Schism
1450.1.1   = { citysize = 13000 }
1500.1.1   = { citysize = 12000 }
1530.1.1   = { constable = yes }
1550.1.1   = { citysize = 11000 customs_house = yes }
1600.1.1   = { citysize = 10000 }
1650.1.1   = { citysize = 9000 base_tax = 4 }
1670.1.1   = { add_core = FRA } # Louis XIV wants to annex the city-state (Chambres de R�union)
1700.1.1   = { citysize = 8500 }
1750.1.1   = { citysize = 9500 }
1789.7.14  = { remove_core = FRA add_core = RFR} # The storming of the Bastille
1791.9.14  = {	owner = RFR
		controller = RFR
		remove_core = PAP
		remove_core = AVI
	     } # Avignon is incorporated into France
1800.1.1   = { citysize = 12550 }
1814.4.11  = {	owner = FRA
		controller = FRA
		add_core = FRA
		remove_core = RFR
	     } # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1815.3.20  = {	owner = RFR
		controller = RFR
		add_core = RFR
		remove_core = FRA
	     } # Napoleon enters Paris
1815.7.8   = {	owner = FRA
		controller = FRA
		add_core = FRA
		remove_core = RFR
	     } # The French monarchy is restored
