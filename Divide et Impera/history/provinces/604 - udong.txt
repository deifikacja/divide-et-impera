#604 - Udong

owner = KHM
controller = KHM
culture = khmer
religion = buddhism
capital = "Phnom Penh"
trade_goods = ivory
hre = no
base_tax = 4
manpower = 3
citysize = 17500
fort1 = yes
add_core = KHM
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = indian

1352.1.1 = { controller = AYU }#Siamese invasion
1357.1.1 = { controller = KHM }#Siamese invasion
1369.1.1 = { owner = AYU controller = AYU }#Conquered by Thais
1375.1.1 = { owner = KHM controller = KHM }#Conquered by Thais
1380.1.1 = { owner = AYU controller = AYU }#Conquered by Thais - est
1389.1.1 = { owner = KHM controller = KHM }#Conquered by Thais
1450.1.1 = { citysize = 27500 }
1500.1.1 = { citysize = 31450 }
1550.1.1 = { citysize = 34890 }
1600.1.1 = { citysize = 36205 }
1650.1.1 = { citysize = 42700 }
1700.1.1 = { citysize = 45100 }
1750.1.1 = { citysize = 48500 }
1800.1.1 = { citysize = 49348 }
1811.1.1 = { revolt_risk = 5 } # The Siamese-Cambodian Rebellion
1812.1.1 = { revolt_risk = 0 }