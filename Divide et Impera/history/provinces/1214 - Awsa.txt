#1214 - Awsa

owner = ETH
controller = ETH
add_core = ETH
culture = afar
religion = sunni
capital = "Awsa"
citysize = 6000
manpower = 1
trade_goods = salt
hre = no
base_tax = 4
discovered_by = ADA
discovered_by = ETH
discovered_by = SHO
discovered_by = NUB
discovered_by = ALO
discovered_by = MKR
discovered_by = ORO
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1493.1.1 = { revolt_risk = 6 } #Uprising among the Arho against crown overlordship
1494.1.1 = { revolt_risk = 0 }
1500.1.1 = { citysize = 6500 }
1533.1.1 = { revolt_risk = 7 } #Raids by Ahmad Gran
1534.1.1 = {	owner = ADA
		controller = ADA
		add_core = ADA
		revolt_risk = 6
	   } #Ahmad Gran takes over region
1550.1.1 = { discovered_by = TUR }
1584.1.1 = { revolt_risk = 7 } #Oromo Raids
1589.1.1 = { revolt_risk = 7 } #Oromo Raids
1590.1.1 = { revolt_risk = 0 }
1647.1.1 = { revolt_risk = 7 } #Oromo Raids
1648.1.1 = { revolt_risk = 0 }
1675.1.1 = {	
		owner = AFA
		controller = AFA
		remove_core = ADA
	   } #Adal collapse leaves area under Afar control