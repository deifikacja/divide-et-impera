#341 - Tunis

owner = TUN
controller = TUN
culture = maghreb_arabic
religion = sunni
capital = "Tunis"
trade_goods = luxury_goods
hre = no
base_tax = 7
manpower = 1
citysize = 90100
fort1 = yes
add_core = TUN
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = FZZ
discovered_by = TUG

1450.1.1  = { citysize = 93102 }
1489.1.1  = { revolt_risk = 4 } # Abu Zikriya Yahya overthrown by Abul Mumin
1490.1.1  = { revolt_risk = 0 } # Abu Yahya recaptures the power
1500.1.1  = { citysize = 95473 }
1550.1.1  = { citysize = 96752 }
1570.1.1  = { controller = TUR } # Ottoman invasion
1573.1.1  = { controller = SPA  }
1574.9.13 = {	owner = TUR
		controller = TUR
		add_core = TUR
	    } # Directly controlled
1591.1.1  = { revolt_risk = 6 } # Janissary revolt
1592.1.1  = { revolt_risk = 0 }
1600.1.1  = { citysize = 108981 }
1650.1.1  = { citysize = 110548 }
1659.1.1  = { marketplace = yes } # Became the center of French commercial life
1700.1.1  = { citysize = 112879 }
1702.1.1  = { revolt_risk = 4 add_core = ALG } # Military coup, Murad III is assassinated
1703.1.1  = { revolt_risk = 0 }
1705.7.15  = { owner = TUN controller = TUN tax_assessor = yes } # Husayn ibn Ali's rule brings some prosperity
1735.1.1  = { revolt_risk = 6 } # Coup with Algerian support
1750.1.1  = { citysize = 115670 constable = yes }
1756.1.1  = { revolt_risk = 0 } # The Husaynid Dynasty is restored 
1800.1.1  = { citysize = 119000 }