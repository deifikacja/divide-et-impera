#914 - Winnebago

culture = dakota
religion = shamanism
capital = "Winnebago"
trade_goods = unknown
hre = no
base_tax = 2
manpower = 1
native_size = 15
native_ferocity = 1
native_hostileness = 6
discovered_by = FOX
discovered_by = ILL
discovered_by = POT

1634.1.1  = { discovered_by = FRA } # Jean Nicolet
1669.1.1 = { owner = POT controller = POT citysize = 1000 add_core = POT culture = ojibwe }#Potawatomi flee in front of Iroqouis invasion
1671.1.1  = {	owner = FRA
		controller = FRA
		citysize = 540
		culture = cosmopolitan_french
		religion = catholic
		trade_goods = fur
	    } # Founding of La Baye
1696.1.1  = { add_core = FRA }
1697.1.1  = { citysize = 1090 } # The fur trade attracted more settlers
1700.1.1  = { citysize = 1988 }
1712.1.1  = { revolt_risk = 5 } # Fox war
1714.1.1  = { revolt_risk = 0 }
1728.1.1  = { revolt_risk = 5 } # Second Fox war
1729.1.1  = { revolt_risk = 0 }
1750.1.1  = { citysize = 2570 }
1763.2.10 = {	owner = GBR
		controller = GBR
		remove_core = FRA
		culture = english
	    } # Treaty of Paris - ceded to Britain, France gave up its claim
1774.1.1  = { add_core = QUE } # United with Canada under the Quebec Act of 1774
1783.9.3  = {	owner = USA
		add_core = USA
		remove_core = QUE
	    } # Treaty of Paris, passed to the U.S but still under British control
1800.1.1  = { citysize = 3250 }