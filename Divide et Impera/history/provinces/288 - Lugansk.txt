#288 - Lugansk

owner = GOL
controller = GOL   
culture = tartar
religion = sunni
hre = no
base_tax = 4
trade_goods = grain
manpower = 4
capital = "Lugansk"
citysize = 2541
fort1 = yes
add_core = CRI
add_core = GOL	
discovered_by = ordu
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = CRI
discovered_by = KAZ
discovered_by = GOL
discovered_by = AST
discovered_by = NOG
discovered_by = BSH
discovered_by = QAS

1395.5.1 = { controller = TIM citysize = 2000 fort1 = no } #Destroyed by Timur
1395.5.2 = { controller = GOL }
1425.1.1  = { revolt = { type = nationalist_rebels size = 2 leader = "Haji I Giray" } controller = REB }
1450.1.1  = {  citysize = 2862 revolt = {} owner = CRI controller = CRI remove_core = GOL }
1500.1.1  = { citysize = 3695 }
1550.1.1  = { citysize = 3700 }
1572.1.1  = {	owner = RUS
		controller = RUS
		add_core = RUS
	    } # Estimated
1598.1.1  = { revolt_risk = 5 } # "Time of troubles"
1600.1.1  = {	culture = ukrainian
		religion = orthodox
		citysize = 3882 }
1613.1.1  = { revolt_risk = 0 } # Order returned, Romanov dynasty
1648.1.1  = {
		controller = REB
	    }
1650.1.1  = { citysize = 4273 }
1657.1.1  = { controller = RUS } # Rebellion fails
1700.1.1  = { citysize = 5910 }
1750.1.1  = { citysize = 6835 }
1795.1.1  = { weapons = yes } # Ammunitions factory founded
1800.1.1  = { citysize = 7100 }