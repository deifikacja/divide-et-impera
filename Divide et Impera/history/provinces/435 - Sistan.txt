#435 - Sistan

owner = SIS
controller = SIS
culture = baluchi
religion = sunni
capital = "Zahedan"
trade_goods = wool
hre = no
base_tax = 2
manpower = 2
citysize = 2670
add_core = SIS
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = TIM
discovered_by = SHY
discovered_by = MUG
discovered_by = ordu

1390.1.1 = { owner = TIM controller = TIM add_core = TIM }
1400.1.1  = { citysize = 2900 }
1430.1.1 = { owner = SIS controller = SIS }
1450.1.1  = { citysize = 3100 }
1495.1.1 = { owner = HER controller = HER }
1500.1.1  = { citysize = 3305 }
1506.1.1 = { owner = SIS controller = SIS }
1507.1.1  = {	owner = PER
		controller = PER
		add_core = PER
		remove_core = TIM
	    } # Conquered by the Safavids
1509.1.1 = { owner = SIS controller = SIS }
1538.1.1  = {	owner = PER
		controller = PER
		add_core = PER
		remove_core = TIM
	    } # Conquered by the Safavids
1550.1.1  = { citysize = 3980 }
1577.1.1 = { owner = SIS controller = SIS }
1597.12.1 = { base_tax = 3 } # The Reforms of Abbas the Great
1600.1.1  = { citysize = 4477 discovered_by = TUR }
1650.1.1  = { citysize = 5276 }
1700.1.1  = { citysize = 5900 }
1747.6.1  = {	owner = DUR
	   	controller = DUR
	   	add_core = DUR
	   	remove_core = PER
	    } # Ahmad Shah established the Durrani empire
1750.1.1  = { citysize = 6288 }
1800.1.1  = { citysize = 7100 }