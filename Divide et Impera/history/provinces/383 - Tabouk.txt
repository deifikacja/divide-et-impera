#383 - Tabouk

owner = HED
controller = HED
culture = bedouin_arabic
religion = sunni
capital = "Tabouk"
trade_goods = wool
hre = no
base_tax = 3
manpower = 1
citysize = 1054
add_core = HED
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ

1450.1.1 = { citysize = 1240 }
1500.1.1 = { citysize = 1540 }
1516.1.1 = {	owner = TUR
		controller = TUR
		add_core = TUR
	   } # Part of the Ottoman empire
1520.1.1 = { fort1 = yes }
1550.1.1 = { citysize = 1864 }
1600.1.1 = { citysize = 2132 }
1650.1.1 = { citysize = 2640 }
1700.1.1 = { citysize = 3400 }
1740.1.1 = { religion = wahhabism }
1750.1.1 = { citysize = 4144 }
1770.1.1 = {	owner = MAM
		controller = MAM
		remove_core = TUR
	   } # Ali Bey gained control of the Hijaz, reconstituting the Mamluk state
1772.1.1 = { owner = HED controller = HED }
1800.1.1 = { citysize = 5521 }
1802.1.1 = {	owner = NAJ
		controller = NAJ
		add_core = NAJ
	   } # The First Saudi State
1818.9.9 = {	owner = TUR
		controller = TUR
		add_core = TUR
		remove_core = NAJ
	   } # The end of the Saudi State
