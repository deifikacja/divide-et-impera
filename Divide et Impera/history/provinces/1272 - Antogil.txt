#1272 - Antogil

culture = madagasque
religion = animism
capital = "Louisburg"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 90
native_ferocity = 4.5
native_hostileness = 9
discovered_by = AMB

1500.8.20 = { discovered_by = POR } #Diego Dias
1675.2.11 = { discovered_by = MER }
1685.1.1  = { discovered_by = ANT discovered_by = MEN }
1690.1.1  = { discovered_by = BOI }
1691.2.11 = { discovered_by = FRA owner = FRA controller = FRA citysize = 1000 native_size = 0 trade_goods = fish } #Piratal base
1712.9.1  = { discovered_by = TAM }
1800.1.1  = { citysize = 1300 }
1817.1.1  = { owner = MDG controller = MDG add_core = MDG } #Antogil taken by Madagascar