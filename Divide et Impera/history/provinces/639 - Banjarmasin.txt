#639 - Banjarmasin

owner = BAJ
controller = BAJ
culture = malayan
religion = hinduism
capital = "Banjarmasin"
trade_goods = spices
fort1 = yes
hre = no
base_tax = 3
manpower = 2
citysize = 5800
add_core = BAJ
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM

1478.1.1 = { add_core = BEI } #Vassal of Brunei
1500.1.1 = { citysize = 6750 }
1521.1.1 = { discovered_by = POR }
1527.1.1 = { remove_core = BEI }
1550.1.1 = { citysize = 8100 }
1555.1.1 = { religion = sunni }
1600.1.1 = { citysize = 8970 }
1606.1.1 = { discovered_by = NED } #Dutch trading post
1650.1.1 = { citysize = 9560 }
#1700.1.1 = { citysize = 10200 }
#1750.1.1 = { citysize = 11348 }
1787.1.1 = { add_core = NED } #Dutch protectorate
#1800.1.1 = { citysize = 12800 }