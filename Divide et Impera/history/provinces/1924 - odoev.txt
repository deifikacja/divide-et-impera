#1924 - odoev

owner = WRC
controller = WRC        
culture = russian	
discovered_by = ordu
religion = orthodox 
hre = no
base_tax = 2
trade_goods = naval_supplies      
manpower = 1
capital = "Odoev"
citysize = 1450
add_core = WRC
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = GOL

1450.1.1  = { citysize = 1920 add_core = MOS }
1494.3.21 = { owner = MOS controller = MOS }
1500.1.1  = { citysize = 2184 }
1503.3.22 = {	owner = RUS
		controller = RUS
		add_core = RUS
		remove_core = MOS
	    }
1550.1.1  = { citysize = 3734 }
1600.1.1  = { citysize = 3498 }
1650.1.1  = { citysize = 4176 }
1670.1.1  = { revolt_risk = 8 } # Stepan Razin
1671.1.1  = { revolt_risk = 0 } # Razin is captured
1700.1.1  = { citysize = 5736 }
1750.1.1  = { citysize = 6495 }
1773.1.1  = { revolt_risk = 5 } # Emelian Pugachev, Cossack insurrection.
1774.9.14 = { revolt_risk = 0 } # Pugachev is captured.
1779.1.1  = { fort2 = yes } # Almost entirely rebuilt.
1800.1.1  = { citysize = 7100 }
