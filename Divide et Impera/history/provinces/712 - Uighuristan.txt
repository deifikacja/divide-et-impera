#712 - Uighuristan

owner = AQS
controller = AQS
culture = mongol
religion = buddhism
capital = "Aqsu"
trade_goods = grain
hre = no
base_tax = 2
manpower = 1
citysize = 5980
add_core = AQS
discovered_by = CHG
discovered_by = AQS
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = ordu

1356.1.1 = { add_core = CHG }
1450.1.1 = { citysize = 6500 }
1500.1.1 = { citysize = 7240 religion = tibetan_buddhism }
1516.1.1 = { owner = CHG controller = CHG }
1550.1.1 = { citysize = 8500 }
1600.1.1 = { citysize = 9860 }
1623.1.1 = {	owner = OIR
		controller = OIR
	   	add_core = OIR
	   	remove_core = CHG
	   } # The Oirads
1650.1.1 = { citysize = 10450 }
1670.1.1 = {	owner = KSG
		controller = KSG
		add_core = KSG
	   } # Part of the Manchu empire
1700.1.1 = { citysize = 11200 }
1750.1.1 = { citysize = 13578 }
1759.1.1 = {	owner = QNG
		controller = QNG
		add_core = QNG
	   } # Part of the Manchu empire
1800.1.1 = { citysize = 14358 }