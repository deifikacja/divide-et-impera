#906 - Lakota

add_core = LAK
religion = shamanism
capital = "Lakota"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 25
native_ferocity = 2
native_hostileness = 8
discovered_by = NAK
discovered_by = DAK
discovered_by = LAK
discovered_by = CHY

1650.1.1 = { culture = dakota citysize = 1200 trade_goods = grain owner = LAK controller = LAK }#Dakotas pushed by the Europeans settle in present day Lakota territory
1738.1.1 = { discovered_by = FRA }# Pierre Gaultier de Varennes, no real settlements until the 1800's
1803.4.3 = {	discovered_by = USA
		owner = USA
		controller = USA
		add_core = USA
		citysize = 300
		trade_goods = grain
	    } # The Louisiana purchase