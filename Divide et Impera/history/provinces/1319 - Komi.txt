#1319 - Komi

owner = NOV #Cadet branch
controller = NOV
capital = "Ust'-Sysolsk"
culture = komi
religion = shamanism
trade_goods = fur
hre = no
base_tax = 1
manpower = 1
citysize = 1000
add_core = NOV
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = PEL

1478.1.14 = {	owner = MOS
		controller = MOS
		add_core = MOS
		remove_core = NOV
	    } # Passed to Muscovy with the rest of Novgorod republic
1500.1.1  = { citysize = 1009 }
1505.1.1 = {	owner = RUS
		controller = RUS
		add_core = RUS
		remove_core = MOS
	    }
1550.1.1  = { citysize = 1042 }
1600.1.1  = { citysize = 1092 }
1650.1.1  = { citysize = 1042 }
1700.1.1  = { citysize = 1092 }
1780.1.1  = { citysize = 2042 } #Granted city rights by Catherine the Great
1800.1.1  = { citysize = 2090 }
