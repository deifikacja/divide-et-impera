#1055 - Verkne-Angarsky

culture = tunguz
religion = shamanism
capital = "Verkne-Angarsky"
trade_goods = unknown
hre = no
base_tax = 2
manpower = 1
native_size = 10
native_ferocity = 1
native_hostileness = 3

1632.1.1  = { discovered_by = RUS }
1647.1.1 = {	owner = RUS
		controller = RUS
	   	religion = orthodox
	   	culture = russian
		citysize = 440
		trade_goods = fur
	    }
1657.1.1 = {	add_core = RUS
	   	citysize = 1087
	   }
1682.1.1  = { citysize = 1230 }
1750.1.1  = { citysize = 1500 }