#294 - Tver

owner = TVE
controller = TVE   
culture = russian	
discovered_by = ordu
religion = orthodox
hre = no
base_tax = 3 
trade_goods = grain   
manpower = 1
capital = "Kashin"
citysize = 5870
fort1 = yes
add_core = TVE
add_core = WOR
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = PEL
discovered_by = CRI
discovered_by = KAZ
discovered_by = GOL
discovered_by = AST
discovered_by = NOG
discovered_by = BSH
discovered_by = QAS

1366.1.1 = { owner = WOR controller = WOR }#Tverian sub-principality
1426.1.1 = { owner = TVE controller = TVE }#Incorporated back
1438.1.1  = { discovered_by = KAZ }
1450.1.1  = { citysize = 2420 discovered_by = SIB }
1485.9.12 = {	owner = MOS
		controller = MOS
		add_core = MOS
		remove_core = TVE
		marketplace = yes
	    } # Passed to Muscovy
1500.1.1  = { citysize = 3543 }
1503.3.22 = {	owner = RUS
		controller = RUS
		add_core = RUS
		remove_core = MOS
	    }
1550.1.1  = { citysize = 4874 }
1560.1.1  = { base_tax = 4 } # Treasury reforms
1598.1.1  = { revolt_risk = 5 } # "Time of troubles"
1600.1.1  = { citysize = 6440 }
1610.1.1  = { regimental_camp = yes }
1613.1.1  = { revolt_risk = 0 } # Order returned, Romanov dynasty
1650.1.1  = { citysize = 8283 }
1670.1.1  = { revolt_risk = 8 } # Stepan Razin
1671.1.1  = { revolt_risk = 0 } # Razin is captured
1682.1.1  = { workshop = yes constable = yes }
1700.1.1  = { citysize = 10450 }
1711.1.1  = { base_tax = 7 } # Governmental reforms and the absolutism
1720.1.1  = { tax_assessor = yes }
1750.1.1  = { citysize = 13000 }
1767.1.1  = { base_tax = 8 } # Legislative reform
1773.1.1  = { revolt_risk = 5 } # Emelian Pugachev, Cossack insurrection
1774.9.14 = { revolt_risk = 0 } # Pugachev is captured
1800.1.1  = { citysize = 16000 }