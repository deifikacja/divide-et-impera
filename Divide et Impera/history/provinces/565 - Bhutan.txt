#565 - Bhutan

owner = TIB
controller = TIB
culture = tibetan
religion = tibetan_buddhism
capital = "Thimphu"
trade_goods = wool
hre = no
base_tax = 2
manpower = 1
citysize = 2500
add_core = TIB
add_core = BHU
fort1 = yes
discovered_by = indian
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM

1450.1.1 = { citysize = 2900 }
1500.1.1 = { citysize = 3310 }
1550.1.1 = { citysize = 3870 }
1600.1.1 = { citysize = 4150 }
1616.1.1 = { 	owner = BHU
		controller = BHU }
1627.1.1 = { fort2 = yes } # Simtokha Dzong
1634.1.1 = { discovered_by = POR } 
1650.1.1 = { citysize = 4888 }
1700.1.1 = { citysize = 5285 }
1750.1.1 = { citysize = 6334 }
1800.1.1 = { citysize = 7000 }