#596 - Malacca

owner = TEM
controller = TEM
culture = malayan
religion = buddhism
capital = "Malacca"
trade_goods = spices
hre = no
cot = yes
fort1 = yes
base_tax = 4
manpower = 1
citysize = 27200
add_core = TEM
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1402.1.1 = { owner = MLC controller = MLC add_core = MLC remove_core = TEM fort2 = yes }#Malacca founded
1414.1.1  = { religion = sunni }
1450.1.1  = { citysize = 31000 }
1500.1.1  = { citysize = 39940 }
1509.1.1  = { discovered_by = POR } # Diego Lopez de Sequiera
1511.9.10 = {	owner = POR
		controller = POR
		revolt_risk = 8
	    } # Conquered by the Portuguese, fierce resistance
1519.1.1  = { fort3 = yes } #La Famosa
1526.1.1  = { revolt_risk = 0 } # The Malay forces are finally subdued
1536.9.10 = { add_core = POR }
1550.1.1  = { citysize = 44110 }
1600.1.1  = { citysize = 46650 }
1641.1.14 = {	owner = NED
		controller = NED
		remove_core = POR
	    } # Conquered by the Dutch
1645.1.1  = { temple = yes } # Cheng Hoon Teng
1650.1.1  = { citysize = 58575 }
1666.1.14 = { add_core = NED }
1700.1.1  = { citysize = 65800 }
1750.1.1  = { citysize = 71398 cot = no }
1795.1.1  = {	owner = GBR
		controller = GBR
		remove_core = NED
	    } # Captured by the British
1800.1.1  = { citysize = 72450 }