#822 - Caqueta

culture = jivaro
capital = "Caqueta"
trade_goods = unknown
religion = animism
hre = no
base_tax = 1
manpower = 1
native_size = 12
native_ferocity = 3
native_hostileness = 3


discovered_by = INC
discovered_by = CHM
discovered_by = INC
discovered_by = CHM
discovered_by = HNV
discovered_by = MNO
discovered_by = PCN
discovered_by = QUT
discovered_by = CII
discovered_by = HNC
discovered_by = CAA
discovered_by = CRC
discovered_by = TRC
discovered_by = CNC
discovered_by = SCN

discovered_by = CNE
discovered_by = CNB
discovered_by = NZC
discovered_by = AYA
discovered_by = DIA
discovered_by = HUA
discovered_by = TIT
discovered_by = ATA


#1500.1.1  = { owner = INC controller = INC add_core = INC }
1533.8.29 = {	discovered_by = SPA
		owner = SPA
		controller = SPA
		culture = castillian
		religion = catholic
		citysize = 1900 
		trade_goods = gold
	    }# The death of Atahualpa
1539.1.1  = { capital = "Pasto" } # Founded by Lorenzo de Aldana
1550.1.1  = { citysize = 4700 }
1558.8.29 = { add_core = SPA }
1600.1.1  = { citysize = 5270 }
1650.1.1  = { citysize = 5700 }
1700.1.1  = { citysize = 6241 }
1750.1.1  = { citysize = 7270 add_core = COL }
1800.1.1  = { citysize = 7900 }
1810.7.20 = {	owner = COL
		controller = COL
	    } # Colombia declares independence
1819.8.7  = { remove_core = SPA } # Colombia's independence is recongnized