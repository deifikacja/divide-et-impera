#414 - Hamadan

owner = JAL
controller = JAL
culture = persian
religion = shiite
capital = "Hamedan"
trade_goods = cloth
hre = no
base_tax = 8
manpower = 3
citysize = 22855
add_core = QAR
add_core = JAL
fort1 = yes
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = TUR
discovered_by = BYZ
discovered_by = TIM
discovered_by = SHY
discovered_by = MUG
discovered_by = ordu

1380.1.1 = { owner = TIM controller = TIM add_core = TIM remove_core = JAL }#Timur the Lame
1405.2.18 = { add_core = HER }
1447.3.12  = {   owner = HER
		controller = HER
		remove_core = TIM
	    }#Shahrokh's death - partition of the Empire
1449.1.1 = { revolt = { type = pretender_rebels size = 0 } controller = REB }#Disunity
1450.1.1  = { citysize = 23200 }
1458.1.1  = { controller = QAR owner = QAR remove_core = HER }
1458.9.1  = { revolt = { type = revolutionary_rebels size = 3 } controller = REB } # Civil war in Qara Quyunlu
1458.12.1 = { revolt = {} controller = QAR }
1469.1.1  = {	owner = AKK
		controller = AKK
		add_core = AKK
		remove_core = QAR
	    }
1500.1.1  = { citysize = 23160 }
1503.6.20 = {	owner = PER
		controller = PER
		add_core = PER
		remove_core = AKK
	    } # The Safavids took over
1535.1.1  = { controller = TUR } # Wartime occupation
1536.1.1  = { controller = PER } # End of Ottoman-Safavid war
1550.1.1  = { citysize = 24120 } # Hamadan prospered
1587.1.1  = { controller = TUR } # Wartime occupation
1590.3.21 = { controller = PER } # Peace of Istanbul
1600.1.1  = { citysize = 25244 }
1650.1.1  = { citysize = 26158 }
1700.1.1  = { citysize = 27900 }
1747.1.1  = { revolt_risk = 3 } # Shah Nadir is killed, local khanates emerged
1748.1.1  = { revolt_risk = 4 } # The empire began to decline
1750.1.1  = { citysize = 28230 }
1779.1.1  = { revolt_risk = 0 } # With the Qajar dynasty the situation stabilized
1800.1.1  = { citysize = 28700 }