#845 - Campeche

owner = ACA
controller = ACA
culture = mayan 
religion = animism 
capital = "Itzamkanac" 
trade_goods = sugar 
hre = no 
base_tax = 2 
manpower = 1 
citysize = 3000
add_core = ACA
discovered_by = MAY
discovered_by = MMA
discovered_by = QUI
discovered_by = TAY
discovered_by = ACA
discovered_by = CHT
discovered_by = CPE
discovered_by = TUX
discovered_by = UAY
discovered_by = CUP
discovered_by = ECA
discovered_by = AKC

discovered_by = ZAP
discovered_by = AZT
discovered_by = MIX
discovered_by = TLA
discovered_by = YPT
discovered_by = MTT
discovered_by = TAR
discovered_by = HST

1450.1.1  = { citysize = 3900 }
1500.1.1 = { citysize = 4560 }
1526.1.1 = {	discovered_by = SPA
		owner = SPA
		controller = SPA
		citysize = 2250
		culture = castillian
		religion = catholic
	   } # First settlers 
1550.1.1 = { citysize = 1870 } 
1558.1.1 = {	
		owner = XXX
		controller = XXX
		citysize = 0
		culture = english
		religion = protestant
	   } # Taken by English pirates
1717.7.16 = {	
	capital = "Ciudad del Carmen"
		owner = SPA
		controller = SPA
		citysize = 2250
		culture = castillian
		religion = catholic
	   } # First settlers
1750.1.1 = { citysize = 2800 add_core = MEX } 
1800.1.1 = { citysize = 3708 }
1810.9.16  = { owner = MEX controller = SPA } # Mexican War of Independence