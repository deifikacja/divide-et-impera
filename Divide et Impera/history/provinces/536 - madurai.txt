#536 - Madurai

owner = MAD
controller = MAD
culture = tamil
religion = hinduism
capital = "Madurai"
trade_goods = naval_supplies
hre = no
base_tax = 7
manpower = 2
citysize = 16833
add_core = MAD
fort1 = yes
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = MUG

1378.1.1 = { owner = VIJ controller = VIJ add_core = VIJ }
1450.1.1 = { citysize = 18015 }
1498.1.1 = { discovered_by = POR }
1500.1.1 = { citysize = 20811 }
1529.1.1 = {	owner = MAD
		controller = MAD
		revolt_risk = 4
	   } # The Vijayanagar empire collapses, the Nayaks proclaimed themselves rulers
1550.1.1 = { citysize = 22347 }
1570.1.1 = { revolt_risk = 0 }
1600.1.1 = { citysize = 25780 }
1650.1.1 = { citysize = 32997 }
1659.1.1 = { revolt_risk = 4 } # The kingdom began to break up.
1660.1.1 = { revolt_risk = 8 } # Madurai was invaded by Maratha & Mysorne resulting in chaos
1689.1.1 = { revolt_risk = 0 } # The rule of Rani Mangammal stabilized the situation.
1700.1.1 = { citysize = 35740 }
1736.4.1 = {	owner = KRK
		controller = KRK
		add_core = KRK
	   	remove_core = MAD
	   }
1750.1.1 = { citysize = 40880 }
1763.1.1 = {	owner = GBR
		controller = GBR
		add_core = GBR
	   } # Ceded to the British
1800.1.1 = { citysize = 43510 }
