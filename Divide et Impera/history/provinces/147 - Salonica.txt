#147 - Salonica

owner = BYZ
controller = BYZ
culture = greek
religion = greek_orthodox
capital = "Salonica"
trade_goods = wine
hre = no
base_tax = 8
manpower = 3
citysize = 20500
fort1 = yes
add_core = BYZ
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1356.1.1 = { add_core = TUR }
1387.1.1 = { owner = TUR controller = TUR }
1402.1.1  = { revolt = { type = pretender_rebels size = 0 } controller = REB } # Interregnum
1403.1.1 = { revolt = {} owner = BYZ controller = BYZ }
1423.1.1 = { owner = VEN controller = VEN add_core = VEN }
1430.3.29 = { owner = TUR controller = TUR remove_core = VEN }
1450.1.1 = { citysize = 25887 religion = sunni }
1462.1.1 = { remove_core = BYZ }
1481.6.1  = { revolt = { type = pretender_rebels size = 0 } controller = REB } # Civil war, Bayezid & Jem
1482.7.26 = { revolt = {} controller = TUR } # Jem escapes to Rhodes
1500.1.1  = { citysize = 29526 }
1550.1.1  = { citysize = 35412 }
1555.1.1  = { revolt_risk = 5 } # General discontent with the Janissaries' dominance
1556.1.1  = { revolt_risk = 0 }
1566.1.1  = { fort2 = yes }
1600.1.1  = { citysize = 38710 }
1615.1.1  = { base_tax = 8 } #The Decentralizing Effect of the Provincial System
1623.1.1  = { revolt = { type = revolutionary_rebels size = 0 } controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { revolt = {} controller = TUR marketplace = yes } # Murad tries to quell the corruption
1630.1.1  = { regimental_camp = yes }
1650.1.1  = { citysize = 41002 }
1700.1.1  = { citysize = 43210 }
1710.1.1  = { tax_assessor = yes }
1750.1.1  = { citysize = 51020 add_core = GRE }
1800.1.1  = { citysize = 58000 }
1821.1.26 = { revolt_risk = 4 }
1821.3.17 = { revolt_risk = 6 } #revolt began in Peloponese