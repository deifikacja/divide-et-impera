#600 - Ayutthaya

owner = AYU 
controller = AYU
culture = central_thai
religion = buddhism
capital = "Ayutthaya"
trade_goods = grain
hre = no
base_tax = 9
manpower = 6
citysize = 44500
add_core = AYU
fort1 = yes
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = indian
discovered_by = YUA


1400.1.1 = { citysize = 58800 }
1450.1.1 = { citysize = 75200 }
1500.1.1 = { citysize = 82000 marketplace = yes }
1550.1.1 = { citysize = 96500 }
1564.2.1 = { add_core = TAU } # Burmese vassal
1584.5.3 = { remove_core = TAU } 
1600.1.1 = { citysize = 151400 cot = yes }
1650.1.1 = { citysize = 200570 customs_house = yes }
1700.1.1 = { citysize = 196100 }
1750.1.1 = { citysize = 169750 }
1760.1.1 = { citysize = 81222 cot = no } # Sacked
1767.4.1 = { revolt_risk = 7 } # The Ayutthaya kingdom began to crumble
1774.1.1 = { revolt_risk = 0 }
1800.1.1 = { citysize = 44880 }