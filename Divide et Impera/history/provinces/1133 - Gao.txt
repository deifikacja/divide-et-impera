#1133 - Gao

owner = SON
controller = SON
culture = songhai
citysize = 12000
manpower = 2
religion = animism
capital = "Gao"
trade_goods = grain
fort1 = yes
hre = no
base_tax = 7
add_core = SON
discovered_by = SON
discovered_by = MAL
discovered_by = KTS
discovered_by = ZAR
discovered_by = GOB
discovered_by = ZAM
discovered_by = HAD
discovered_by = KAN
discovered_by = SOF
discovered_by = FUT
discovered_by = OUG
discovered_by = YAT
discovered_by = NUN
discovered_by = AIR
discovered_by = BUS

1400.1.1  = { citysize = 19000 base_tax = 8 }
1450.1.1  = { citysize = 25000 }
1480.1.1  = { citysize = 34000 base_tax = 10 } #Era of Prosperity under Sonni Ali
1493.1.1  = { revolt_risk = 9 } #Civil War between Sunni Baare and Muhammad Ture Sylla
1494.1.1  = { revolt_risk = 0 citysize = 13000 base_tax = 9 religion = sunni } #Muhammad Ture Sylla establishes new dynasty
1525.1.1  = { citysize = 15000 } #Revival under Askiya Muhammad
1529.1.1  = { revolt_risk = 2 } #Musa overthrows father, becomes Askiya
1550.1.1  = { citysize = 20000 } #Era of Prosperity under Askiya Dawud
1575.1.1  = { citysize = 30000 } #Era of Prosperity under Askiya Dawud
1591.3.15 = { discovered_by = MOR owner = MOR controller = MOR add_core = MOR } #Moroccan victory at Tondibi
1592.1.1  = { citysize = 10000 base_tax = 7 } #Rapid Decline in wake of Moroccan occupation - city abandoned at first
1642.1.1  = { citysize = 8000 remove_core = SON } #Collapse of last vestiges of unity among Songhai
1670.1.1  = {	culture = tuareg
		citysize = 0
		base_tax = 4
		native_size = 80
		native_ferocity = 4.5	
		native_hostileness = 8 
		trade_goods = unknown	
		owner = XXX
		controller = XXX    
	    } #Arma no longer in control of Gao, collapse of urban community
1700.1.1  = { native_size = 60 } #Instability, Natural calamities and trade route changes cause major decline in Gao
1750.1.1  = { native_size = 20 } #Instability, Natural calamities and trade route changes cause major decline in Gao