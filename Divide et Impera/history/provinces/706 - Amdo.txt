#706 - Amdo

owner = TIB
controller = TIB
culture = tibetan
religion = tibetan_buddhism
capital = "Amdo"
trade_goods = wool
hre = no
base_tax = 1
manpower = 1
citysize = 10400
add_core = TIB
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = ordu

1500.1.1  = { citysize = 11985 }
1550.1.1  = { citysize = 13250 }
1600.1.1  = { citysize = 15100 }
1650.1.1  = { citysize = 16988 }
1700.1.1  = { citysize = 18140 }
1719.1.1  = { controller = QNG} # Manchu invasion of Tibet
1720.1.1  = {	owner = QNG
		add_core = QNG
	    } # Qing Empire
1750.1.1  = { citysize = 19402 }
1755.10.4 = { revolt_risk = 7 } # Rebellion aginst the Chinese
1757.10.4 = { revolt_risk = 0 }
1800.1.1  = { citysize = 21300 }