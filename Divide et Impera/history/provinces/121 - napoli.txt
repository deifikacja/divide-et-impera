#121 - Napoli

owner = NAP
controller = NAP
culture = campanian 
religion = catholic 
hre = no 
base_tax = 8 
trade_goods = wine 
manpower = 2 
fort1 = yes 
capital = "Napoli" 
citysize = 144000	# Estimated
marketplace = yes 
add_core = NAP
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1450.1.1  = { citysize = 148000 }
1494.1.1  = { add_core = FRA } # Angevine claims
1495.2.22 = { controller = FRA } # Charles VIII takes Napoli
1495.7.7  = { controller = NAP } # Charles VIII leaves Italy
1500.1.1  = { citysize = 150000 marketplace = yes } 
1502.1.1  = { owner = FRA controller = FRA } # France and Aragon partitions Naples
1503.5.13 = { owner = ARA controller = ARA add_core = ARA } # France forced to withdraw
1504.1.31 = { remove_core = FRA } # The Treaty of Lyon in 1504
1516.1.23 = {	owner = SPA
		controller = SPA
		add_core = SPA
	    	remove_core = ARA
	    } # Unification of Spain
1550.1.1  = { citysize = 212000 } 
1600.1.1  = { citysize = 281000 } 
1650.1.1  = { citysize = 176000 } 
1700.1.1  = { citysize = 216000 }
1710.1.1  = { tax_assessor = yes }
1714.3.7  = {	owner = HAB
		controller = HAB
		add_core = HAB
		remove_core = SPA
	    }
1734.6.2  = { owner = SIC controller = SIC add_core = SIC remove_core = HAB }
1750.1.1  = { citysize = 305000 } 
1800.1.1  = { citysize = 427000 }
1806.2.9  = { controller = RFR } # French troops invade
1806.3.11 = {	owner = NAP
		controller = NAP
		add_core = NAP
	    } # Joseph Bonaparte becomes king of the new Kingdom of Naples
1815.5.3  = {	owner = SIC
		controller = SIC
		remove_core = NAP
	    } # Murat is defeated