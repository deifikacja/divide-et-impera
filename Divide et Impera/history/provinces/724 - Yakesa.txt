#724 - Yakesa

owner = YUA
controller = YUA
culture = manchu
religion = confucianism
capital = "Hailar"
trade_goods = grain
hre = no
base_tax = 2
manpower = 1
citysize = 500
add_core = YUA
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = ordu

1368.1.22 = { owner = KHA controller = KHA add_core = KHA remove_core = YUA }
1450.1.1 = { citysize = 700 }
1500.1.1 = { citysize = 800 }
1550.1.1 = { citysize = 850 }
1600.1.1 = { citysize = 900 }
1650.1.1 = { citysize = 950 }
1644.1.1 = {	owner = MCH
		controller = MCH
		add_core = MCH
		remove_core = KHA
	   } # Assimilation of Eastern Mongols
1683.1.1 = {	owner = QNG
		controller = QNG
		add_core = QNG
		remove_core = MCH
	   } # The Qing Dynasty
1700.1.1 = { citysize = 990 }
1734.1.1 = { citysize = 1000 }#City founded
1800.1.1 = { citysize = 1200 }