#685 - Jiangsu

owner = YUA
controller = YUA
culture = chihan
religion = confucianism
capital = "Nanking"
trade_goods = chinaware
hre = no
base_tax = 10
manpower = 3
citysize = 138008
add_core = YUA	
discovered_by = ordu
add_core = MNG
add_core = SNG
add_core = QIN
fort1 = yes
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = YUA

1356.1.1 = { revolt = { type = nationalist_rebels size = 4 leader = "Zhu Yuanzhang" } controller = REB }
1368.1.28 = { revolt = {} owner = MNG controller = MNG remove_core = YUA }
1450.1.1 = { citysize = 145200 }
1500.1.1 = { citysize = 151000 }
1505.1.1 = { marketplace = yes }
1550.1.1 = { citysize = 165400 }
1580.1.1 = { fine_arts_academy = yes }
1600.1.1 = { citysize = 187770 }
1644.1.1 = {	controller = MCH
		capital = "Jiangning"
	   } # The Qing Dynasty
1646.1.1 = {	owner = MCH
		add_core = MCH
		remove_core = MNG
	   } # The Qing Dynasty
1650.1.1 = { citysize = 198551 }
1683.1.1 = {	owner = QNG
		controller = QNG
		add_core = QNG
		remove_core = MCH
	   } # The Qing Dynasty
1700.1.1 = { citysize = 204974 }
1740.1.1 = { tax_assessor = yes }
1750.1.1 = { citysize = 228870 }
1800.1.1 = { citysize = 252000 }