#581 - Sagaing

owner = SAG
controller = SAG
culture = burmese
religion = buddhism
capital = "Sagaing"
trade_goods = grain
hre = no
base_tax = 3
manpower = 1
citysize = 6200
add_core = SAG
add_core = MAW
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = indian
temple = yes

1364.1.1 = { owner = MAW controller = MAW add_core = MAW remove_core = SAG citysize = 1000 fort1 = no }#City is destroyed by the Maw Shans, dynasty disappears
1450.1.1 = { citysize = 1800 }
1500.1.1 = { citysize = 2320 }
1550.1.1 = { citysize = 3250 }
1555.1.1 = { owner = TAU controller = TAU add_core = TAU }
1600.1.1 = { citysize = 4140 }
1650.1.1 = { citysize = 4963 }
1700.1.1 = { citysize = 5540 }
1750.1.1 = { citysize = 6388 }
1800.1.1 = { citysize = 7450 }