#2048 - Tongatapu

owner = TGA
controller = TGA
add_core = TGA
culture = tongan
religion = animism
capital = "Nuku'alofa"
trade_goods = fish
hre = no
cot = yes
base_tax = 1
manpower = 1
citysize = 1000
discovered_by = TGA
discovered_by = HVA

1643.1.1 = { discovered_by = NED } # Abel Tasman
1812.1.1 = { capital = "Lifuka" }