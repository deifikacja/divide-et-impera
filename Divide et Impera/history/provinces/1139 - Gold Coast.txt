#1139 - Gold Coast

culture = aka
religion = animism
capital = "Accra"
trade_goods = unknown
hre = no
manpower = 1
base_tax = 2
native_size = 80
native_ferocity = 4.5
native_hostileness = 9 

1471.1.1 = { discovered_by = POR } 
1482.1.1 = { trade_goods = gold }
1500.1.1 = { citysize = 4000 trade_goods = ivory owner = DEN controller = DEN discovered_by = DEN } #strengthening of Dagomba under Na Nyaghse
1610.1.1 = { revolt_risk = 4 citysize = 5000 } #Ewe migrations in wake of turmoil in their homeland
#1637.1.1 #Dutch take over Portuguese Trade Post
1660.1.1 = { revolt_risk = 4 } #Warfare between Dankyira and Adansi disrupts region
#1665.1.1 = English take over Dutch Trade Post
#1668.1.1 = Dutch regain control over Trade post - remain dominant in region until 1790s
1670.1.1 = { trade_goods = slaves } #Rise of slave trade in late 17th C
1680.1.1 = { revolt_risk = 4 citysize = 6000 } #Wave of Akan migrations towards coast with expansio of Denyinka state
1700.1.1 = { controller = ASH } #Ashante invades Nzima region
1701.1.1 = { controller = DEN } #Denkyira becomes tributary
1710.1.1 = { revolt_risk = 4 citysize = 7000 } #Wave of Akan migrations towards coast in wake of rise of Ashanti
1720.1.1 = { add_core = ASH } #Ashante begins to claim overlordship over coastal region
1804.1.1 = { discovered_by = SOK }