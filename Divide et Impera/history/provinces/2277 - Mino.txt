#2277 - Mino

owner = TOI
controller = TOI
culture = japanese
religion = shinto
capital = "Mino"
trade_goods = grain
hre = no
base_tax = 5
manpower = 1
citysize = 7350
add_core = TOI
fort1 = yes
discovered_by = chinese

1567.1.1 = { owner = ODA controller = ODA add_core = ODA capital = "Gifu"}
1582.6.22 = { owner = TOY controller = TOY add_core = TOY }
1600.10.22 = { owner = JAP controller = JAP add_core = JAP }#Japan is unified