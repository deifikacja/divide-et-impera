#48 - Hinterpommern

owner = STE
controller = STE
add_core = STE
culture = pommeranian
religion = catholic
hre = yes
base_tax = 4
trade_goods = naval_supplies
manpower = 1
fort1 = yes
capital = "Szczecin"
citysize = 8500
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1400.1.1   = { citysize = 10500 }
1450.1.1   = { citysize = 12500 }
1464.9.8 = { owner = WOL controller = WOL }
1474.7.5 = { owner = STE controller = STE }
1478.12.17 = { owner = POM controller = POM add_core = POM }
1532.1.1 = { owner = STE controller = STE }
1480.1.1   = { marketplace = yes }
1500.1.1   = { citysize = 14500 }
1525.1.1   = { citysize = 16500 }
1529.1.1   = { add_core = BRA fort2 = yes }
1534.1.1   = { religion = protestant }
1550.1.1   = { citysize = 18500 regimental_camp = yes }
1575.1.1   = { citysize = 20400 shipyard = yes constable = yes }
1600.1.1   = { citysize = 22600 }
1620.1.1   = { owner = POM controller = POM }
1625.1.1   = { citysize = 24900 }
1630.1.1   = { citysize = 14700 revolt_risk = 7 }#Devastating population losses in Thirty Years War
1648.10.24 = { owner = BRA controller = BRA }
1650.1.1   = { citysize = 17000 }
1660.1.1   = { fort3 = yes }
1675.1.1   = { citysize = 19500 workshop = yes courthouse = yes }
1695.1.1   = { customs_house = yes }
1700.1.1   = { citysize = 21000 }
1701.1.18  = {	owner = PRU
		controller = PRU
		add_core = PRU
		remove_core = BRA
	     }
1725.1.1   = { citysize = 26500 }
1750.1.1   = { citysize = 30500 tax_assessor = yes }
1775.1.1   = { citysize = 34000 fort4 = yes }
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved