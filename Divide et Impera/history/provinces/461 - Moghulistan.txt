#461 - Moghulistan

owner = CHG
controller = CHG
culture = kirgiz
religion = sunni
capital = "Kulja"
trade_goods = wool
hre = no
base_tax = 4
manpower = 1
citysize = 3120
add_core = KZH
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = CHG
discovered_by = OIR
discovered_by = ordu

1456.1.1 = { owner = KZH controller = KZH culture = khazak }
1450.1.1 = { citysize = 3725 }
1500.1.1 = { citysize = 4500 }
1550.1.1 = { citysize = 5460 }
1600.1.1 = { citysize = 6200 }
1622.1.1 = { discovered_by = RUS }
1650.1.1 = { citysize = 6988 }
1695.1.1 = {	owner = OIR
		controller = OIR
		add_core = OIR
	   } # The Dzungars
1700.1.1 = { citysize = 5642 }
1709.1.1 = {	owner = KOK
		controller = KOK
		add_core = KOK
	   } # Independent principality established by Shaybanid Shahrukh
1750.1.1 = { citysize = 6340 }
1800.1.1 = { citysize = 7005 }