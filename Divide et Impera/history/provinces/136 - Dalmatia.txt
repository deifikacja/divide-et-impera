#136 - Dalmatia

owner = VEN
controller = VEN
culture = croatian
religion = catholic
capital = "Zadar"
trade_goods = cloth
hre = no
base_tax = 5
manpower = 4
citysize = 8400
fort1 = yes
add_core = VEN
add_core = CRO
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1356.1.1 = { add_core = HUN discovered_by = ordu }
1358.1.1   = { owner = HUN controller = HUN }
1387.1.1   = { owner = BOS controller = BOS add_core = BOS } #Tvrtko I
1392.1.1   = { owner = HUN controller = HUN remove_core = BOS }
1420.1.1   = { owner = VEN controller = VEN remove_core = HUN }
1450.1.1   = { citysize = 9800 workshop = yes }
1500.1.1   = { citysize = 10098 marketplace = yes }
1540.10.2  = { owner = TUR controller = TUR add_core = TUR } # A large part of Dalmatia is incorporated into the Ottoman empire
1550.1.1   = { citysize = 12112 }
1600.1.1   = { citysize = 13211 }
1650.1.1   = { citysize = 14680 }
1699.1.26  = { owner = VEN controller = VEN remove_core = TUR } # Peace of Karlowitz, Dalmatian hinterland given to Venice
1700.1.1   = { citysize = 16120 }
1720.1.1   = { customs_house = yes } # Intense economic and cultural growth
1750.1.1   = { citysize = 17187 tax_assessor = yes }
1757.1.1   = { base_tax = 6 } # The economic production increased in almost every aspect
1797.10.17 = {	owner = HAB
		controller = HAB
		add_core = HAB
	     } # Treaty of Campo Formio
1800.1.1   = { citysize = 18130 }
1805.12.26 = {	owner = RFR
		controller = RFR
		add_core = RFR
		remove_core = HAB
	     } # Treaty of Pressburg
1813.9.20 = { controller = HAB } # Occupied by Austrian forces
1814.4.6  = {	owner = HAB
		add_core = HAB
		remove_core = RFR
	    } # Napoleon abdicates