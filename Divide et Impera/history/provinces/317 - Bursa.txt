#317 - Bursa

owner = TUR
controller = TUR
culture = turkish
religion = sunni
capital = "Bursa"
trade_goods = cloth
hre = no
base_tax = 10
manpower = 4
citysize = 21645
fort1 = yes
add_core = TUR
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = ordu

1400.1.1  = { citysize = 24932 }
1402.1.1  = { revolt = { type = pretender_rebels size = 2 leader = "S�leyman Osmanli" } controller = REB } # Interregnum
1410.1.1  = { revolt = {} }
1410.1.2  = { revolt = { type = pretender_rebels size = 2 leader = "M�sa Osmanli" } controller = REB }
1413.1.1  = { revolt = {} controller = TUR }
1448.1.1  = { religion = sunni }
1450.1.1  = { citysize = 22832 base_tax = 9 }
1462.1.1  = { remove_core = BYZ }
1481.6.1  = { revolt = { type = pretender_rebels size = 1.5 leader = "Jem Osmanli" } controller = REB } # Civil war, Bayezid & Jem
1482.7.26 = { revolt = {} controller = TUR } # Jem escapes to Rhodes
1500.1.1  = { citysize = 24944 }
1509.1.1  = { revolt = { type = revolutionary_rebels size = 0 } controller = REB } # Civil war
1513.1.1  = { revolt = {} controller = TUR }
1550.1.1  = { citysize = 25450 }
1600.1.1  = { citysize = 29789 }
1623.1.1  = { revolt_risk = 8 } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { revolt_risk = 0 } # Murad tries to quell the corruption
1650.1.1  = { citysize = 26882 marketplace = yes }
1690.1.1  = { base_tax = 7 } #The Decentralizing Effect of the Provincial System
1700.1.1  = { citysize = 27668 }
1718.1.1  = { revolt_risk = 3 base_tax = 8 } # Lale Devri (the tulip age), not appreciated by everyone  
1720.1.1  = { revolt_risk = 0 }
1750.1.1  = { citysize = 31662 tax_assessor = yes }
1795.1.1  = { revolt_risk = 6 } # Reforms by Sultan Selim III, tried to replace the Janissary corps
1800.1.1  = { citysize = 48700 }