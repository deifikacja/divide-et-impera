#355 - Sirt

owner = TRP
controller = TRP
culture = tuareg
religion = ibadi
capital = "Sirt"
trade_goods = wool
hre = no
base_tax = 2
manpower = 1
citysize = 2034
fort1 = yes
add_core = TRP
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = FZZ

1450.1.1 = { citysize = 2232 }
1500.1.1 = { citysize = 2546 }
1550.1.1 = { citysize = 2999 }
1551.1.1  = {	owner = TUR
		controller = TUR
		add_core = TUR
		religion = sunni
		remove_core = KNI
	    } # Under direct Ottoman control until 1629
1600.1.1 = { citysize = 3465 }
1650.1.1 = { citysize = 4007 }
1700.1.1 = { citysize = 4530 marketplace = yes }
1711.1.1 = { revolt = { type = nationalist_rebels size = 0 } controller = REB }
1714.1.1  = { owner = TRP controller = TRP } # The Ottoman governor is killed by Ahmad Karmanli
1750.1.1 = { citysize = 5023 }
1800.1.1 = { citysize = 5938 }