#747 - Tucujulandia

culture = tupinamba
religion = animism
capital = "Tucujulandia"
trade_goods = unknown
hre = no
base_tax = 2
manpower = 1
native_size = 15
native_ferocity = 1
native_hostileness = 2

1500.1.1  = { discovered_by = CAS discovered_by = POR } # Pinz�n, Pedro �lvares Cabral
1516.1.23 = { discovered_by = SPA }
1637.1.1  = {	owner = POR
		controller = POR
		citysize = 500
	    	culture = portugese
	    	religion = catholic
		trade_goods = fish
	    } # Granted to Bento Manuel Parent as an hereditary captaincy
1650.1.1  = { citysize = 1005 }
1662.1.1  = { add_core = POR }
1688.1.1  = { capital = "Macap�" fort1 = yes }
1700.1.1  = { citysize = 2493 }
1750.1.1  = { citysize = 4140 add_core = BRZ }
1800.1.1  = { citysize = 7855 }