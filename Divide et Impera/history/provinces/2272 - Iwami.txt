#2272 - Iwami

owner = YAM
controller = YAM
culture = japanese
religion = shinto
capital = "Iwami"
trade_goods = grain
hre = no
base_tax = 4
manpower = 1
citysize = 5500
add_core = YAM
fort1 = yes
discovered_by = chinese

1450.1.1   = { citysize = 5910 }
1500.1.1   = { citysize = 6350 }
1526.5.6   = { trade_goods = gold } # Silver mine discovered by Kamiya Jyutei of Hakata
1542.1.1   = { discovered_by = POR }
1550.1.1   = { citysize = 7200 }
1566.1.1 = { owner = MRI controller = MRI }
1600.10.22 = { citysize = 8665 owner = JAP controller = JAP add_core = JAP }
1637.12.17 = { revolt = { type = anti_tax_rebels size = 1 } } # Shimabara rebellion
1638.4.12  = { revolt = {} }
1650.1.1   = { citysize = 9200 }
1700.1.1   = { citysize = 10350 }
1750.1.1   = { citysize = 11500 }
1800.1.1   = { citysize = 13247 }
