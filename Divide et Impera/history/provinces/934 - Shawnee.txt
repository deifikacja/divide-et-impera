#934 - Shawnee

owner = CAH
controller = CAH
culture = mississippian
religion = animism
capital = "Shawnee"
trade_goods = grain
hre = no
base_tax = 1
manpower = 1
citysize = 1200
add_core = CAH
discovered_by = PLA

discovered_by = QUA
discovered_by = ONE
discovered_by = CAD
discovered_by = CAH
discovered_by = ETO
discovered_by = SHA
discovered_by = CRE
discovered_by = CHE

1356.1.1 = { add_core = SHA }
1450.1.1  = { citysize = 1350 }
1500.1.1  = { citysize = 1685 }
1550.1.1  = { religion = shamanism citysize = 2654 owner = SHA controller = SHA remove_core = CAH }
1600.1.1  = { citysize = 3100 }
1650.1.1  = { citysize = 3620 }
1682.1.1  = { discovered_by = FRA } # Estimated
1700.1.1  = { citysize = 4170 }
1763.2.10 = {	discovered_by = GBR
		owner = GBR
		controller = GBR
		culture = english
		religion = episcopalism
		remove_core = SHA
	    } 
1750.1.1  = { citysize = 4626 }
1783.9.3  = {	owner = USA
		controller = USA
		add_core = USA
		culture = american
		capital = "Knoxville"
	    } # Admitted to the Union
1800.1.1  = { citysize = 5265 }