#386 - Asir

owner = HED
controller = HED
add_core = ASI
culture = bedouin_arabic
religion = sunni
capital = "Asir"
trade_goods = coffee
hre = no
base_tax = 3
manpower = 3
citysize = 1230
add_core = HED
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = western
discovered_by = TUR

1450.1.1 = { citysize = 1400 }
1517.1.1 = {	owner = TUR
		controller = TUR
		add_core = TUR
		citysize = 1554
	   } # Part of the Ottoman empire
1550.1.1 = { citysize = 2071 }
1600.1.1 = { citysize = 2675 }
1650.1.1 = { citysize = 3487 }
1700.1.1 = { citysize = 4112 }
1728.1.1 = {	owner = ASI
		controller = ASI
	   } # Asir founded
1750.1.1 = { citysize = 5210 }
1755.1.1 = { religion = wahhabism }
1800.1.1 = { citysize = 6534 }
1818.9.9 = {	owner = NAJ
		controller = NAJ
		add_core = NAJ 
	   } 
