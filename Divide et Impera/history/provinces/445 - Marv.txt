#445 - Marv

owner = KHO
controller = KHO
culture = turkmeni
religion = sufism
capital = "Marv"
trade_goods = wool
hre = no
base_tax = 4
manpower = 1
citysize = 4867
add_core = KHO
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = indian
discovered_by = ordu

1380.1.1 = { 	owner = TIM 
		controller = TIM 
		add_core = TIM 
		remove_core = KHO
		religion = sunni 
		citysize = 1850 
	   } #Sarbadars religion state destroyed by Timur the Lenk 
1405.2.18 = { add_core = SAM }
1447.3.12  = {   owner = SAM
		controller = SAM
		remove_core = TIM
	    }#Shahrokh's death - partition of the Empire
1450.1.1 = { citysize = 5030}
1500.1.1 = { citysize = 5280 }
1506.1.1 = {	owner = SHY
		controller = SHY
		add_core = SHY
		remove_core = SAM
	   } # The Uzbek Shaybanids brings an end to the Timurid dynasty
1510.1.1 = {	owner = PER
		controller = PER
		add_core = PER
		remove_core = SHY
	   } # Part of the Persian empire
1550.1.1 = { citysize = 5934 }
1600.1.1 = { citysize = 6530 discovered_by = TUR }
1650.1.1 = { citysize = 6941 }
1700.1.1 = { citysize = 7267 }
1747.1.1 = {	owner = SHY
		controller = SHY
		add_core = SHY
		remove_core = PER
	   } # Conquered by the Shaybanids
1750.1.1 = { citysize = 7720 }
1800.1.1 = { citysize = 8110 }