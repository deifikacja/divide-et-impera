#372 - Ulster

owner = TYR
controller = TYR
culture = irish
religion = catholic
hre = no
base_tax = 5
trade_goods = grain
manpower = 1
capital = "Carrickfergus"
citysize = 1200
add_core = TYR
fort1 = yes
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ

1450.1.1   = { citysize = 1600 }
1580.1.1   = { religion = episcopalism }
1603.1.1   = { citysize = 2100 }
1607.9.4   = {	owner = ENG
		controller = ENG
		add_core = ENG
		capital = Belfast
	     } # Flight of the Earls
1609.1.1   = { culture = english religion = reformed fort2 = yes } # Jacobean Plantations
1625.1.1   = { marketplace = yes } # Estimated
1641.10.22 = { revolt_risk = 15 }
1642.5.1   = { revolt_risk = 0 } # Estimated
1646.6.5   = { controller = IRE }
1650.6.21  = { controller = ENG } # Battle of Scarrifhollis
1675.1.1   = { courthouse = yes fort3 = yes } # Estimated
1700.1.1   = { citysize = 2500 }
1707.5.12  = {	owner = GBR
		controller = GBR
		add_core = GBR
	    	remove_core = ENG
	     }
1711.1.1   = { trade_goods = cloth } # Formation of Linen Board
1750.1.1   = { citysize = 9000 }
1775.1.1   = { workshop = yes customs_house = yes } # Estimated
1798.5.23  = { revolt_risk = 10 } # Irish rebellion
1798.7.14  = { revolt_risk = 0 } # The rebels are defeated
1800.1.1   = { citysize = 24000 }