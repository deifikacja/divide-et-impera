#1286 - Basuto

culture = bantu #Tsonga
religion = animism
capital = "Butha-Buthe"
trade_goods = unknown
hre = no
base_tax = 2
manpower = 1
native_size = 70
native_ferocity = 4.5
native_hostileness = 9 

1498.1.6 = { discovered_by = POR } #Vasco Da Gama