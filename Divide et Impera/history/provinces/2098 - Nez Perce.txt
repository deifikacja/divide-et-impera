#2098 - Nez Perce
culture = plateau_penutian
religion = shamanism
capital = "Nez Perce"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 25
native_ferocity = 2
native_hostileness = 8

1805.10.6 = { discovered_by = USA }#Lewis and Clark. Not settled until 1867