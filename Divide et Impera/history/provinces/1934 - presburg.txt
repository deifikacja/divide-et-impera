#1772 - Presburg

owner = HUN
controller = HUN  
culture = slovak
religion = catholic
capital = "Pozsony"
trade_goods = copper
hre = no
base_tax = 5
manpower = 2
citysize = 15200
fort1 = yes
add_core = HUN	
discovered_by = ordu
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1432.1.1  = { revolt_risk = 10 } # The city is attacked by Hussites
1434.1.1  = { revolt_risk = 0 }
1440.1.1  = { revolt_risk = 10 } # Struggles between the supporters of Ladislaus and Elisabeth of Hungary
1443.1.1  = { revolt_risk = 0 }
1450.1.1  = { citysize = 18580 }
1480.1.1  = { marketplace = yes }
1490.1.1  = { controller = HAB } # Occupied
1491.11.7 = { controller = HUN }
1500.1.1  = { citysize = 21980 }
1526.8.30 = {	owner = HAB
		controller = HAB
		add_core = HAB
		capital = "Pressburg"
	    } # Battle of Moh�cs, the end of the independent Kingdom of Hungary
1532.1.1  = { fort2 = yes }
1550.1.1  = { citysize = 24000 }
1563.1.1  = { temple = yes } # St. Martin's cathedral, 1452
1581.1.1  = { fort3 = yes } # Extended defense against the Ottoman troops
1600.1.1  = { citysize = 25910 }
1604.1.1  = { revolt = { type = nationalist_rebels size = 0 } controller = REB } # The nobility in Royal Hungary rebelled against Habsburg & Jesuit repression
1606.1.1  = { revolt = {} controller = HAB } # Peace treaty, guaranteed religious tolerance
1619.1.1  = { revolt = { type = nationalist_rebels size = 0 } controller = REB } # Bethlen uprisings
1626.1.1  = { revolt = {} controller = HAB constable = yes } # The 3rd Peace of Pressburg, puts an end to the Bethlen uprisings
1650.1.1  = { citysize = 26530 }
1700.1.1  = { citysize = 28780 } # The area was largely depopulated under Ottoman rule, new settlement policy under Habsburg rule
1703.1.1  = { revolt = { type = nationalist_rebels size = 0 } controller = REB } # Kuruc rebellion, lead by Francisc Rakoczy
1710.1.1  = { revolt = {} citysize = 26220 } # Plague
1711.1.1  = { controller = HAB } # The House of Habsburg recognized as the new ruler
1728.1.1  = { tax_assessor = yes } # The economy flourishes
1750.1.1  = { citysize = 29100 }
1800.1.1  = { citysize = 31300 }
