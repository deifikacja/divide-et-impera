#127 - Sardinia

owner = ARA
controller = ARA
culture = sardinian
religion = catholic 
hre = no 
base_tax = 3
trade_goods = grain
manpower = 3
fort1 = yes 
capital = "Cagliari" 
citysize = 11000 # Estimated 
add_core = ARA
add_core = SAR
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1450.1.1  = { citysize = 13500 }
1500.1.1  = { citysize = 17000 } 
1516.1.23 = {	owner = SPA
		controller = SPA
		add_core = SPA
		remove_core = ARA
	    } # Unification of Spain
1550.1.1  = { citysize = 18000 } 
1600.1.1  = { citysize = 20000 } 
1650.1.1  = { citysize = 12000 marketplace = yes } 
1700.1.1  = { citysize = 21000 }
1713.4.12 = {	owner = HAB
		controller = HAB
		add_core = HAB
		remove_core = SPA
	    }
1720.1.1   = {	owner = SPI
		controller = SPI
		add_core = SPI
		remove_core = HAB
	     } # Kingdom of Piedmont-Sardinia
1750.1.1   = { citysize = 24000 }
1796.1.1   = { controller = RFR } # French invasion
1796.4.16  = { controller = SPI } # Peace between Sardinia and France
1800.1.1   = { citysize = 25000 }
