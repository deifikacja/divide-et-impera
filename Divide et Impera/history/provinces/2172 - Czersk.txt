#2172 - Czersk

owner = MAZ
controller = MAZ
capital = "Czersk"
culture = polish
religion = catholic
trade_goods = grain
hre = no
base_tax = 2
manpower = 2
citysize = 2000
fort1 = yes
add_core = MAZ
add_core = POL
discovered_by = ordu
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1400.1.1   = { citysize = 2300 }
1450.1.1   = { citysize = 2700 }
1381.6.16  = { owner = PLO controller = PLO add_core = PLO } #death of Siemowit III partition of Mazowia
1462.1.1 = { owner = POL controller = POL remove_core = PLO }#Incorporated
1500.1.1   = { citysize = 3200 }
1526.3.9   = {	owner = POL
		controller = POL
		remove_core = MAZ
	     }
1550.1.1   = { citysize = 4100 constable = yes }
1569.7.4   = {	owner = RZP
		controller = RZP
		add_core = RZP
		remove_core = POL
	     } #Union of Lublin
1575.1.1   = { regimental_camp = yes fort2 = yes }
1588.1.1   = { revolt = { type = revolutionary_rebels size = 3 } controller = REB } # Civil war, Polish succession
1589.1.1   = { revolt = {} controller = RZP } # Coronation of Sigismund III
1600.1.1   = { citysize = 5100 }
1650.1.1   = { citysize = 6200 }
1655.9.8   = { controller = SWE } # The Deluge
1656.7.1   = { controller = RZP }
1656.7.30  = { controller = SWE } # Battle of Warsaw, against Sweden & Brandenburg
1656.9.1   = { controller = RZP } # End of Northern war
1700.1.1   = { citysize = 5300 tax_assessor = yes }
1702.5.1   = { controller = SWE } # Occupied again
1709.1.1   = { controller = RZP } # Karl XII defeated in the battle of Poltava
1733.1.1 = { revolt = { type = revolutionary_rebels size = 0 } controller = REB } # The war of Polish succession
1735.1.1 = { revolt = {} controller = RZP fort3 = yes }
1750.1.1   = { citysize = 6200 } # Several waves of rural & Dutch settlers
1756.1.1   = { fine_arts_academy = yes } # Kolegium Pijar�w founded
1768.2.29  = { revolt_risk = 8 } # Uprisings against the Polish king & Russia
1772.8.18  = { revolt_risk = 0 } # Uprisings suppressed by Russian troops
1794.3.24  = { revolt_risk = 5 } # Kosciuszko uprising
1794.11.5  = { revolt_risk = 0 } # Kosciuszko is captured
1795.10.1  = { owner = PRU controller = PRU add_core = PRU } # Third partition of Poland
1800.1.1   = { citysize = 8000 }
1806.11.3  = { revolt = { type = nationalist_rebels size = 2 } controller = REB } # Polish uprising instigated by Napoleon
1807.7.9   = {	revolt = {}
		owner = DOW
		controller = DOW
		add_core = DOW
		remove_core = RZP
	     	remove_core = PRU
	     } # The Duchy of Warsaw is established after the treaty of Tilsit, ruled by Frederick Augustus I of Saxony
1812.12.12 = { controller = RUS }
1815.6.9   = {	owner = RUS
		add_core = RUS
		add_core = POL
		remove_core = DOW
	     } # Congress Poland, under Russian control after the Congress of Vienna
