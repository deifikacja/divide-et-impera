#1230 - Massawa

owner = ETH
controller = ETH
add_core = ETH
culture = cushitic
religion = sunni
capital = "Mitsiwa"
citysize = 5000
manpower = 1
trade_goods = slaves
hre = no
base_tax = 4
fort1 = yes
discovered_by = ETH
discovered_by = SHO
discovered_by = NUB
discovered_by = ALO
discovered_by = ADA
discovered_by = MKR
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1356.1.1 = { revolt = { type = nationalist_rebels size = 1 } controller = REB }
1499.1.1 = { discovered_by = POR }
1500.1.1 = { citysize = 5500 }
1550.1.1 = { citysize = 6000 discovered_by = TUR }
1557.1.1 = {	revolt = { }
		owner = TUR
		controller = TUR
		add_core = TUR
		revolt_risk = 6
	   } #Ottomans occupy Massawa
1588.1.1 = { revolt_risk = 7 } #Raids by Sarsa Dengel
1589.1.1 = { revolt_risk = 0 }
1600.1.1 = { citysize = 6500 }
1650.1.1 = { citysize = 7000 }
1700.1.1 = { citysize = 7500 }
1750.1.1 = { citysize = 8000 }
1800.1.1 = { citysize = 9000 }