#346 - Tafilat

owner = MOR
controller = MOR  
culture = berber
religion = sunni
capital = "Tafilat"
trade_goods = wool
hre = no
base_tax = 2
manpower = 1
citysize = 2657
fort1 = yes
add_core = TAF
add_core = MOR
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = FZZ

1450.1.1 = { citysize = 2800 }
1500.1.1 = { citysize = 3014 }
1549.1.1 = { owner = MOR controller = MOR }#Morocco is reunited under the Saadi dynasty
1550.1.1 = { citysize = 3254 }
1600.1.1 = { citysize = 4089 }
1603.1.1 = { owner = TAF controller = TAF }#Morocco is partitioneed again
1650.1.1 = { citysize = 4312 }
1666.1.1 = { owner = MOR controller = MOR }#Morocco is reunited
1672.1.1 = { revolt_risk = 4 } # Oppositions against Ismail, & the idea of a unified state
1700.1.1 = { citysize = 4478 }
1727.1.1 = { revolt_risk = 0 }
1750.1.1 = { citysize = 5070 }
1800.1.1 = { citysize = 5667 }