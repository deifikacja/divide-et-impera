#1284 - Kilwa Kisiwani

owner = KIL
controller = KIL
add_core = KIL
culture = swahili
religion = sunni
capital = "Kilwa Kisiwani"
citysize = 5000
manpower = 2
trade_goods = luxury_goods
hre = no
base_tax = 3
fort1 = yes

discovered_by = ADA
discovered_by = ZZA
discovered_by = MAQ
discovered_by = ZIM
discovered_by = KIL
discovered_by = PTE
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1499.1.1 = { discovered_by = POR } 
1500.1.1 = { citysize = 6000 }
1505.1.1 = { owner = POR controller = POR } #Portuguese occupation starts
1512.1.1 = { owner = KIL controller = KIL } #Portuguese occupation ends
1597.1.1 = { owner = POR controller = POR } #Conquered by Potrugal
1680.1.1 = { owner = KIL controller = KIL discovered_by = OMA } #Independence recovered under Omani rule