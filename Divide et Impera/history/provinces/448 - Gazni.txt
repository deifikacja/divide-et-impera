#448 - Gazni

owner = HER
controller = HER
culture = pashtun
religion = sunni
capital = "Gazni"
trade_goods = grain
hre = no
base_tax = 3
manpower = 3
citysize = 4015
add_core = HER
add_core = KAB
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = indian
discovered_by = ordu

1389.1.1 = {	owner = TIM
		controller = TIM
		add_core = TIM
	   } # The Timurids
1419.1.1 = {	owner = KAB
		controller = KAB
		remove_core = TIM
	   } # Timurids collapse
1450.1.1 = { citysize = 4800 }
1451.1.1 = {	owner = HER
		controller = HER
	   } # To Herat
1457.1.1 = {	revolt_risk = 4
	   } # Timurids collapse
1461.1.1 = {	revolt_risk = 0
	   } # Timurids collapse
1500.1.1 = { citysize = 5820 marketplace = yes }
1504.1.1 = {	owner = MUG
		controller = MUG
		add_core = MUG
	   } # Shifted to Mughal rule as the Timurid's power wained
1550.1.1 = { citysize = 7410 }
1600.1.1 = { citysize = 9400 discovered_by = TUR }
1650.1.1 = { citysize = 11346 }
1700.1.1 = { citysize = 12158 }
1738.1.1 = {	owner = PER
		controller = PER
		add_core = PER
	   } # Captured by Persia, Nadir Shah
1747.6.1 = {	owner = DUR
	   	controller = DUR
	   	add_core = DUR
	   	remove_core = PER
	   } # Ahmad Shah established the Durrani empire
1750.1.1 = { tax_assessor = yes  citysize = 13700 }
1772.1.1 = { capital = "Kabal" }
1800.1.1 = { citysize = 20100 }
1817.1.1 = { owner = KAB controller = KAB }