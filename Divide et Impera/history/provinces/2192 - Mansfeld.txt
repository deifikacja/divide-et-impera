#2192 - Mansfeld

owner = MNF
controller = MNF
culture = saxon
religion = catholic
capital = "Eisleben"
trade_goods = cattle
hre = yes
base_tax = 3
manpower = 1
citysize = 3800
fort1 = yes
add_core = MNF
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1450.1.1 = { citysize = 4500 }
1500.1.1 = { citysize = 4890 }
1529.1.1 = { religion = protestant }
1550.1.1 = { citysize = 5340 }
1600.1.1 = { citysize = 5800 marketplace = yes }
1650.1.1 = { citysize = 6500 }
1700.1.1 = { citysize = 7100 constable = yes }
1750.1.1 = { citysize = 7990 }
1780.3.1 = { owner = SAX controller = SAX add_core = SAX }
1800.1.1 = { citysize = 8700 }
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
1815.6.9 = {	owner = PRU
		controller = PRU
		add_core = PRU
		remove_core = SAX
	    } # Congress of Vienna