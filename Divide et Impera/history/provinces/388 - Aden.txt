#388 - Aden

owner = ADE
controller = ADE
culture = yemeni_arabic
religion = sunni
capital = "Aden"
trade_goods = spices
hre = no
base_tax = 2
manpower = 1
citysize = 4000
add_core = ADE
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1450.1.1 = { citysize = 4320 }
1480.1.1 = { discovered_by = TUR }
1488.1.1 = { discovered_by = POR }
1500.1.1 = { citysize = 5877 }
1538.1.1 = {	owner = TUR
		controller = TUR
		add_core = TUR
	   } # Part of the Ottoman empire
1547.1.1 = { owner = POR controller = POR } # Occupied by Portugal
1550.1.1 = { citysize = 6700 }
1551.1.1 = { owner = TUR controller = TUR }
1567.1.1 = { revolt_risk = 4 } # Revolt against the Ottomans
1570.1.1 = { revolt_risk = 0 }
1597.9.1 = { revolt_risk = 5 } # Qasimi state, revolt against the Ottomans
1600.1.1 = { citysize = 8120 }
1602.1.1 = { revolt_risk = 0 }
1650.1.1 = { citysize = 9356 }
1700.1.1 = { citysize = 11768 }
1750.1.1 = { citysize = 12377 }
1800.1.1 = { citysize = 14320 }
1802.1.1 = { owner = GBR controller = GBR remove_core = ADE add_core = GBR }