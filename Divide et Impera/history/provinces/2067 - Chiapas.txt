#2067 - Chiapas

culture = mayan
religion = animism 
capital = "Chiapas" 
trade_goods = unknown
hre = no 
base_tax = 2 
manpower = 1
native_size = 16 
native_ferocity = 6 
native_hostileness = 6
discovered_by = MAY
discovered_by = MMA
discovered_by = QUI
discovered_by = TAY
discovered_by = ACA
discovered_by = CHT
discovered_by = CPE
discovered_by = TUX
discovered_by = UAY
discovered_by = CUP
discovered_by = ECA
discovered_by = AKC

discovered_by = ZAP
discovered_by = AZT
discovered_by = MIX
discovered_by = TLA
discovered_by = YPT
discovered_by = MTT
discovered_by = TAR
discovered_by = HST

1519.3.13 = { discovered_by = SPA } # Hern�n Cort�s
1560.6.24  = {	owner = SPA
		controller = SPA
		capital = "Tuxtla"
		culture = castillian
		religion = catholic
		citysize = 150
		trade_goods = coffee
	    } # Dominicanes
1562.1.1  = { add_core = SPA }
1600.1.1  = { citysize = 3570 }
1650.1.1  = { citysize = 4935 }
1700.1.1  = { citysize = 5180 }
1750.1.1  = { citysize = 5470 add_core = MEX }
1800.1.1  = { citysize = 6500 }
1810.9.16 = { owner = MEX controller = SPA } # Mexican War of Independence