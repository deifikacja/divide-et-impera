#1132 - Timbuktu

owner = MAL 
controller = MAL 
add_core = MAL 
culture = mali
citysize = 15000
manpower = 1
religion = sunni
capital = "Timbuktu"
trade_goods = luxury_goods
hre = no
cot = yes
base_tax = 10
discovered_by = MAL
discovered_by = ASH
discovered_by = SON
discovered_by = SOF
discovered_by = FUT
discovered_by = OUG
discovered_by = YAT
discovered_by = NUN
marketplace = yes
temple = yes # Sankore Mosque and Madrasa was the center of Islamis scholarship in Bilad al-Sudan
university = yes # Sankore Mosque and Madrasa was the center of Islamis scholarship in Bilad al-Sudan

1433.1.1 = { revolt = { type = revolutionary_rebels size = 0 } controller = REB }
1469.1.1 = { owner = SON controller = SON add_core = SON } #Conquered by Sunni Ali Ber of Songhai
1471.1.1 = { revolt_risk = 7 } #Muslim leaders in city launch revolt to end Songhai rule
1472.1.1 = { revolt_risk = 0 } #Sunni Ali represses Muslim leadership of Timbuktu
1525.1.1 = { citysize = 25000 base_tax = 14 } #Revival under Askiya Muhammad
1550.1.1 = { citysize = 30000 } #Era of Prosperity under Askiya Dawud
1575.1.1 = { citysize = 35000 } #Era of Prosperity under Askiya Dawud
1586.1.1 = { revolt = { type = pretender_rebels size = 0 } controller = REB } #Civil war between Al-Sadduk and Ishak
1588.1.1 = { revolt = {} controller = SON revolt_risk = 7 } #Ishak reconquers lands in revolt, uneasy on throne
1591.5.1 = { discovered_by = MOR owner = MOR controller = MOR add_core = MOR base_tax = 10 } #moroccans seize Timbuktu
1593.1.1 = { revolt_risk = 7 } #Moroccans launches purge of city elite
1600.1.1 = { citysize = 20000 } #Rapid decline in population in wake of Moroccan occupation
1600.1.1 = { remove_core = MAL } #Collapse of Mali State
1618.1.1 = { owner = MAL controller = MAL add_core = MAL remove_core = MOR } #Moroccans no longer appoint governors, local Moroccans, the Arma, rule on their own (recycle MAL tag)
1642.1.1 = { remove_core = SON } #Collapse of last vestiges of unity among Songhai
1650.1.1 = { citysize = 15000 base_tax = 9 trade_goods = salt } #Political instability, droughts, disease and change in trade routes continue city's slide
1675.1.1 = { revolt_risk = 4 } #under vassalage of Bambara kingdom of Kaladian Kulibali
1683.1.1 = { revolt_risk = 1 } #Bambara overlordship lost in wake of death of death of Kaladian Kulibali
1700.1.1 = { citysize = 10000 university = no cot = no } #Political instability, droughts, disease and change in trade routes continue city's slide
1716.1.1 = { revolt_risk = 7 } #Mansur ibn Masud's Coup and wave of repression
1720.1.1 = { revolt_risk = 0 } #Mansur ibn Masud overthrown and Arma restored
1737.1.1 = { revolt_risk = 3 } #Tuareg raids destablize the region
1750.1.1 = { citysize = 8000 base_tax = 8  } #Political instability, droughts, disease and change in trade routes continue city's slide
1752.1.1 = { revolt_risk = 4 } #Bambara kingdom of Mamali Kulibali make Timbuktu tributary of Bambara
1756.1.1 = { revolt_risk = 9 } # Denkoro seizes power in wake of father Mamali's death, civil war
1757.1.1 = { revolt_risk = 2 } #Bambara control lost in wake of civil wars in Segu
1770.1.1 = { revolt_risk = 3 citysize = 3000 base_tax = 6 } #Tuareg lay seige to city, causing famine
1771.1.1 = { revolt_risk = 0 } #Tuareg siege resolved via negotiation
1773.1.1 = { revolt_risk = 2 } #Bambara overlordship restored by Ngolo Diarra, Bambara overseers appointed in city
1790.1.1 = { citysize = 6000 } #Slow recovery of city begins