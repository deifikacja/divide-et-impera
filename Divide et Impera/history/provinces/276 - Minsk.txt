#276 - Minsk

owner = LIT
controller = LIT   
culture = byelorussian
religion = orthodox
hre = no
base_tax = 4
trade_goods = naval_supplies 
manpower = 3
capital = "Miensk"
citysize = 3850 
fort1 = yes 
add_core = LIT
add_core = MNK
discovered_by = ordu
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1400.1.1   = { citysize = 4385  }
1450.1.1   = { citysize = 4720 remove_core = MNK }
1500.1.1   = { citysize = 5150 }
1550.1.1   = { citysize = 5765 marketplace = yes }
1569.7.4   = {	owner = RZP
		controller = RZP
		add_core = RZP
		remove_core = POL
	     }
1596.3.1  = { religion = graecocatholic }
1600.1.1   = { citysize = 6913 }
1650.1.1   = { citysize = 7780 }
1655.7.11   = {	controller = RUS
		add_core = RUS
	     } # Conquered by Tsar Alexei
1660.10.1   = {	controller = RZP
		remove_core = RUS
	     } # Regained by Jan Kasimir
1700.1.1   = { citysize = 6800 }
1732.1.1   = { temple = yes } # The Marinsky Cathedral
1740.1.1   = { tax_assessor = yes }
1750.1.1   = { citysize = 8000 }
1793.1.21  = {	owner = RUS
		controller = RUS
		add_core = RUS
	     } # Second Partition of Poland
1794.3.24  = { revolt_risk = 5 } # Kosciuszko uprising, minimize the Russian influence
1794.11.16 = { revolt_risk = 0 religion = orthodox }
1800.1.1   = { citysize = 12000 }
1807.7.9  = { remove_core = RZP } # The Duchy of Warsaw is established instead of Poland
1812.7.8   = { controller = RFR } # Occupied by French troops
1812.11.18 = { controller = RUS }