# 1325 - Zhigansk

capital = "Zhigansk"
culture = yukagir
religion = animism
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 50
native_ferocity = 2
native_hostileness = 1

1632.1.1  = {	owner = RUS
		controller = RUS
		discovered_by = RUS
		add_core = RUS
		citysize = 100
		culture = russian
		religion = orthodox 
		trade_goods = fur
	    } # Founded by Beketov
1750.1.1 = { citysize = 250 }