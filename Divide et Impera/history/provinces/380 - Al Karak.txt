#380 - Najaf

owner = JAL
controller = JAL
culture = al_iraqiya_arabic
religion = shiite
trade_goods = grain
capital = "Najaf"
hre = no
base_tax = 3
manpower = 2
citysize = 9000
add_core = JAL
temple = yes
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = ordu

1401.3.1 = { owner = TIM controller = TIM }
1406.1.1 = { owner = JAL controller = JAL }
1432.1.1 = { owner = QAR controller = QAR add_core = QAR remove_core = JAL }
1450.1.1 = { citysize = 10000 }
1469.1.1   = {	owner = AKK
		controller = AKK
		add_core = AKK
		remove_core = QAR
	     }
1500.1.1 = { citysize = 11000 }
1507.1.1   = {	owner = PER
		controller = PER
		add_core = PER
		remove_core = AKK
	     } # The Safavids took over
1534.11.28 = {	controller = TUR culture = najdi_arabic }#Shammar migration
1536.1.1   = {	owner = TUR
		add_core = TUR
		remove_core = PER
	     } # Conquered by the Ottomans
1550.1.1 = { citysize = 14000 }
1600.1.1 = { citysize = 16000 }
1624.1.1   = { controller = PER }
1638.12.24 = { controller = TUR }
1650.1.1 = { citysize = 18000 } 
1670.1.1 = { revolt_risk = 0 }
1700.1.1 = { citysize = 20000 }
1750.1.1 = { citysize = 22000 add_core = IRQ }
1800.1.1 = { citysize = 24000 }
1810.1.1 = { citysize = 26000 }