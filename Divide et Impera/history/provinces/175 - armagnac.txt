# 175 Armagnac - Principal cities: Auch

owner = AMG
controller = AMG
capital = "Auch"
citysize = 2300
culture = gascon
religion = catholic
hre = no
base_tax = 3
trade_goods = wool
manpower = 3
add_core = AMG
fort1 = yes
temple = yes # La Cath�drale Sainte-Marie
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1356.1.1   = { add_core = FRA add_core = ENG }
1450.1.1   = { citysize = 2500 }
1475.8.29  = { remove_core = ENG } # Treaty of Picquigny
1497.1.1 = {	owner = ALE
		controller = ALE
		add_core = ALE
	     }
1500.1.1   = { citysize = 5000 }
1540.1.1   = { fort2 = yes }
1549.12.21 = {
	owner = FRA
	controller = FRA
	remove_core = ALE
	remove_core = AMG
}#Death of Marguerite
1550.1.1   = { citysize = 7500 marketplace = yes }
1555.1.1   = { religion = reformed }
1565.1.1   = { revolt_risk = 8 } # France is restless once again as ultra-catholic intentions become clear
1568.9.1   = { revolt_risk = 15 } # Catherine de Medici and Charles IX side with the Guise faction, religious intolerance peaks
1570.8.8   = { revolt_risk = 10 } # Edict of Saint-Germain: temporary pacification
1573.9.1   = { revolt_risk = 15 } # Saint Barthelew's Day Massacre: the consequences in the land
1574.5.1   = { revolt_risk = 7 } # Charles IX dies, situation cools a bit	
1584.1.1   = { revolt_risk = 12 } # Situation heats up again
1588.12.1  = { revolt_risk = 15 } # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1589.1.5   = { owner = NAV controller = NAV add_core = NAV remove_core = AUV }
1589.8.2   = { owner = FRA controller = FRA } # Armagnac to France proper, after Henri IV, King of France & Duke of Armagnac
1594.1.1   = { revolt_risk = 10 } # 'Paris vaut bien une messe!', Henri converts to Catholicism
1595.1.1   = { regimental_camp = yes }
1598.4.13  = { revolt_risk = 3 } # Edict of Nantes, alot more freedom to the protestants
1598.5.2   = { revolt_risk = 0 } # Peace of Vervins, formal end to the Wars of Religion
1600.1.1   = { citysize = 10000 }
1630.1.1   = { constable = yes }
1640.1.1   = { fort3 = yes }
1650.1.1   = { citysize = 12500 }
1650.1.14  = { revolt_risk = 7 } # Mazarin arrests the Princes Cond�, Conti & Longueville, the beginning of the Second Fronde
1651.4.1   = { revolt_risk = 4 } # An unstable peace is concluded
1651.12.1  = { revolt_risk = 7 } # Mazarin returns from exile, Cond� sides with Spain, situation heats up again
1652.10.21 = { revolt_risk = 0 } # The King is allowed to enter Paris again, Mazarin leaves France for good. Second Fronde over.
1670.1.1   = { fort4 = yes }
1680.1.1   = { fort5 = yes } # Vauban's 'pointed' forts in Bayonne & Dax
1685.10.18 = { revolt_risk = 8 } # Edict of Nantes revoked by Louis XIV
1686.1.17  = { religion = catholic } # Dragonnard campaign succesful: region reverts back to catholicism
1689.1.1   = { revolt_risk = 0 } # War of the Grand Alliance erupts: Louis XIV can't persue his religious policy anymore
1700.1.1   = { citysize = 15000 }
1730.1.1   = { courthouse = yes }
1750.1.1   = { citysize = 17500 }
1760.1.1   = { tax_assessor = yes }
1789.7.14  = {	owner = RFR
		controller = RFR
		add_core = RFR
		remove_core = FRA
	     } # The storming of the Bastille
1800.1.1   = { citysize = 20000 }
1814.4.11  = {	owner = FRA
		controller = FRA
		add_core = FRA
		remove_core = RFR
	     } # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1815.3.20  = {	owner = RFR
		controller = RFR
		add_core = RFR
		remove_core = FRA
	     } # Napoleon enters Paris
1815.7.8   = {	owner = FRA
		controller = FRA
		add_core = FRA
		remove_core = RFR
	     } # The French monarchy is restored