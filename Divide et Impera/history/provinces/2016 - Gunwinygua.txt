#2015 - Yermalner

culture = aboriginal
religion = animism
capital = "Gunwinygua"
hre = no
trade_goods = unknown
manpower = 1
base_tax = 2
native_size = 5
native_ferocity = 0.5
native_hostileness = 9

1644.1.1 = { discovered_by = NED } # Abel Tasman
