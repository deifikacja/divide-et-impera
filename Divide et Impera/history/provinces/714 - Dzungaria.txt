#714 - Dzungaria

owner = CHG
controller = CHG
culture = mongol
religion = sunni
capital = "Dzungaria"
trade_goods = grain
hre = no
base_tax = 3
manpower = 1
citysize = 9800
add_core = OIR
discovered_by = CHG
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = ordu

1450.1.1 = { citysize = 10854 }
1500.1.1 = { citysize = 12485 }
1550.1.1 = { citysize = 14088 }
1552.1.1 = { owner = OIR controller = OIR }
1600.1.1 = { citysize = 15487 }
1623.1.1 = {	
	   	religion = tibetan_buddhism
	   } 
1650.1.1 = { citysize = 16990 }
1700.1.1 = { citysize = 18000 }
1750.1.1 = { citysize = 20200 }
1757.1.1 = {	owner = QNG
		controller = QNG
		add_core = QNG
	   } # Part of the Manchu empire
1800.1.1 = { citysize = 24500 }