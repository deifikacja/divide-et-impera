#473 - Samara

owner = GOL
controller = GOL
culture = bashkir
religion = sunni
capital = "Samara"
trade_goods = gold
hre = no
base_tax = 5
manpower = 2
citysize = 2050
add_core = KAZ
add_core = GOL	
add_core = BSH
discovered_by = ordu
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ

1438.1.1 = { owner = KAZ controller = KAZ remove_core = GOL }
1450.1.1 = { citysize = 2435 }
1500.1.1 = { citysize = 3340 }
1552.1.1 = {	owner = RUS
		controller = RUS
		add_core = RUS
		remove_core = KAZ
	   }
1586.1.1 = {	culture = russian
		religion = orthodox
	   	fort1 = yes
	   }
1600.1.1 = { citysize = 4656 }
1650.1.1 = { citysize = 5044 }
1670.1.1 = { revolt_risk = 5 } # Bashkir rebbelion
1671.1.1 = { revolt_risk = 0 }
1700.1.1 = { citysize = 6597 }
1707.1.1 = { revolt_risk = 5 } # Bashkir rebellion
1708.1.1 = { revolt_risk = 0 remove_core = BSH } #Last Bashkir rebellion unsuccesful
1750.1.1 = { citysize = 7540 }
1773.1.1 = { revolt = { type = revolutionary_rebels size = 1 } controller = REB } # Occupied by Pugachev
1774.1.1 = { revolt = {} controller = RUS }
1800.1.1 = { citysize = 8000 }