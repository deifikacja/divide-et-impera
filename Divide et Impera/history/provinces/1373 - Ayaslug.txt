#1373 - Ayaslug

owner = AYD
controller = AYD
culture = turkish
religion = sunni
capital = "Ayaslug"
trade_goods = grain
hre = no
base_tax = 3
manpower = 3
citysize = 4600
fort1 = yes
add_core = AYD
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1356.1.1 = {  add_core = TUR }
1390.1.1  = { owner = TUR controller = TUR }
1400.1.1  = { citysize = 5035 }
1402.7.21 = { owner = AYD controller = AYD } # Timur victory in Ankara
1426.1.1  = { owner = TUR controller = TUR }
1450.1.1  = { citysize = 5350 }
1500.1.1  = { citysize = 5861 remove_core = AYD }
1509.1.1  = { revolt_risk = 5 } # Civil war
1513.1.1  = { revolt_risk = 0 }
1525.1.1  = { marketplace = yes }
1550.1.1  = { citysize = 6320 }
1600.1.1  = { citysize = 7450 }
1615.1.1  = { base_tax = 2 manpower = 2 } #The Decentralizing Effect of the Provincial System
1623.1.1  = { revolt_risk = 9 } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { revolt_risk = 0 } # Murad tries to quell the corruption
1650.1.1  = { citysize = 6812 }
1688.1.1  = { citysize = 5350 } # Devastating earthquake
1700.1.1  = { citysize = 7440 }
1740.1.1  = { tax_assessor = yes }
1750.1.1  = { citysize = 8670 }
1800.1.1  = { citysize = 10090 }