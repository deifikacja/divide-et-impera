#704 - Liaoxi

owner = YUA
controller = YUA
culture = manchu
religion = confucianism
capital = "Jinzhou"
trade_goods = grain
hre = no
base_tax = 5
manpower = 4
citysize = 8970
add_core = YUA
add_core = MNG
add_core = SNG
add_core = QIN
fort1 = yes
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = ordu

1370.1.1 = { owner = MNG controller = MNG add_core = MNG remove_core = YUA add_core = KHA }
1450.1.1 = { citysize = 9478 }
1500.1.1 = { citysize = 10150 base_tax = 6 } # Repairs of the Great Wall
1550.1.1 = { citysize = 12344 }
1600.1.1 = { citysize = 14689 }
1618.1.1 = { controller = MCH }
1626.1.1 = { controller = MNG } # Thruce
1634.1.1 = { controller = MCH }
1641.1.1 = {	owner = MCH
		add_core = MCH
		remove_core = MNG
	   } # The Qing Dynasty
1650.1.1 = { citysize = 16534 }
1683.1.1 = {	owner = QNG
		controller = QNG
		add_core = QNG
		remove_core = MCH
	   } # The Qing Dynasty
1700.1.1 = { citysize = 18140 }
1750.1.1 = { citysize = 20500 }
1800.1.1 = { citysize = 22870 }