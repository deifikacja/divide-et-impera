#1925 - Vologda

owner = NOV
controller = NOV    
culture = russian	
discovered_by = ordu 
religion = orthodox
hre = no
base_tax = 4
trade_goods = naval_supplies
manpower = 1
capital = "Vologda"
citysize = 3980
fort1 = yes
add_core = NOV
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western

1412.1.1  = {	owner = MOS
		controller = MOS
		remove_core = NOV
	    } # Vologda is ceded to Muscovy
1438.1.1  = { discovered_by = KAZ }
1450.1.1  = { citysize = 4320 }
1500.1.1  = { citysize = 5092 marketplace = yes }
1503.3.22 = {	owner = RUS
		controller = RUS
		add_core = RUS
		remove_core = MOS
	    }
1550.1.1  = { citysize = 5990 }
1580.1.1  = { marketplace = yes } # Became a wealthy and important trade and transport center
1598.1.1  = { revolt_risk = 5 } # "Time of troubles"
1600.1.1  = { citysize = 7047 }
1613.1.1  = { revolt_risk = 0 } # Order returned, Romanov dynasty
1650.1.1  = { citysize = 8291 }
1670.1.1  = { temple = yes } # Dedicated to the Domition of Mary, rebuilt in the 17th century
1700.1.1  = { citysize = 9754 }
1750.1.1  = { citysize = 11475 }
1800.1.1  = { citysize = 13500 }
