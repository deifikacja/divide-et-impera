#365 - Sinai

owner = MAM
controller = MAM
culture = al_misr_arabic
religion = sunni
capital = "Sinai"
trade_goods = salt
hre = no
base_tax = 1
manpower = 1
citysize = 1440
add_core = MAM
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ

1450.1.1 = { citysize = 1665 }
1500.1.1 = { citysize = 1840 }
1516.1.1 = {	owner = TUR
		controller = TUR
		add_core = TUR
	   } # Part of the Ottoman empire
1550.1.1 = { citysize = 2050 }
1600.1.1 = { citysize = 2270 }
1650.1.1 = { citysize = 2416 }
1700.1.1 = { citysize = 2601 }
1750.1.1 = { citysize = 2765 }
1800.1.1 = { citysize = 2910 }