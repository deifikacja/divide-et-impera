#263 - Ratibor

owner = OPP
controller = OPP
add_core = OPP
culture = schlesian
religion = catholic
hre = yes
base_tax = 3
capital = "Opole"
citysize = 4500
trade_goods = cattle
manpower = 2
fort1 = yes
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1430.1.1  = { religion = hussite }
1436.1.1  = { religion = catholic }
1450.1.1  = { citysize = 5300 }
1500.1.1  = { citysize = 7000 }
1532.1.1  = { owner = HAB controller = HAB add_core = HAB }
1546.1.1  = { religion = reformed }
1550.1.1  = { citysize = 10000 }
1600.1.1  = { citysize = 12000 }
1618.1.1  = { revolt_risk = 5 } # Religious struggles
1632.1.1  = { controller = SWE }#Occupied by Sweden during thirty years war
1634.1.1  = { controller = HAB }
1645.1.1  = { add_core = OPP  owner = OPP controller = OPP religion = catholic }
1648.1.1  = { revolt_risk = 0 }
1650.1.1  = { citysize = 10000 }#Population losses after thirty years war
1660.1.1  = { add_core = HAB  owner = HAB controller = HAB remove_core = OPP }
1742.7.1  = { add_core = PRU  owner = PRU controller = PRU }# Peace of Breslau, 1 st Silesian war against Austria
1750.1.1  = { citysize = 13000 }
1757.1.1  = { controller = HAB }#Captured by Habsburgs during seven years war
1761.1.1  = { controller = PRU }#Recaptured by allied Russian troops during seven years war
1762.1.1  = { controller = HAB }#Captured by Habsburgs during seven years war
1763.3.15 = { controller = PRU remove_core = HAB } # End of 3rd Silesian war
1800.1.1  = { citysize = 18000 }
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
1807.1.10 = { controller = RFR }#Captured by Napoleon
1807.7.9  = { controller = PRU } # The Second treaty of Tilsit