#521 - Panipat

owner = DLH
controller = DLH
culture = panjabi
religion = hinduism
capital = "Panipat"
trade_goods = cloth
hre = no
base_tax = 3
manpower = 1
citysize = 8655
add_core = DLH
add_core = LHR
fort1 = yes
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = ordu
discovered_by = MUG

1388.9.20 = { revolt_risk = 4 } #death of Firuz Shah, dynasty crisis began
1389.2.19 = { revolt_risk = 8 } #Tughluq Shah II is murdered in Delhi
1389.4.1  = { revolt = { type = pretender_rebels size = 6 leader = "Muhammad Shah" } controller = REB } #Muhammad Shah III
1390.8.31 = { revolt = {} controller = DLH revolt_risk = 3 } #Muhammad Shah III enters Delhi
1392.6.1  = { revolt_risk = 9 } #chaos in sultanate
1398.6.1 = { owner = TIM controller = TIM add_core = TIM citysize = 6440 revolt_risk = 0 }
1413.1.1 = { owner = DLH controller = DLH  remove_core = TIM }
1450.1.1 = { citysize = 8795 }
1500.1.1 = { citysize = 10540 }
1526.1.1 = {	owner = MUG
		controller = MUG
		add_core = MUG
		remove_core = DLH
	   } # Battle of Panipat
1541.1.1  = { owner = SRI controller = SRI add_core = SRI } # Sher Shah takes over
1550.1.1 = { citysize = 11764 }
1556.1.1  = {	owner = MUG
		controller = MUG
		remove_core = SRI
	   } # Sher Shah Suri's death, the Bengali empire crumbeled
1600.1.1 = { citysize = 13330 }
1611.1.1 = { religion = sikhism }
1650.1.1 = { citysize = 15320 }
1700.1.1 = { citysize = 17000 religion = sunni } #Sikhs are expelled by Aurangzeb's forces
1750.1.1 = { citysize = 19280 }
1770.1.1 = {	owner = MAR
		controller = MAR
		add_core = MAR
		remove_core = MUG
	   } # The Marathan Empire
1800.1.1 = { citysize = 21400 }
1818.6.3 = {	owner = GBR
		controller = GBR
		add_core = GBR
		remove_core = MAR
	   }
