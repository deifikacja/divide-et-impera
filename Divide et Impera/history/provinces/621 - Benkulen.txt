#621 - Benkulen

culture = malayan
religion = buddhism
capital = "Benkulen"
trade_goods = unknown
hre = no
base_tax = 2
manpower = 1
native_size = 30
native_ferocity = 2
native_hostileness = 9
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1509.1.1 = { discovered_by = POR }
1512.1.1 = { religion = sunni }
1570.1.1 = {owner = BAN controller = BAN add_core = BAN citysize = 100 trade_goods = grain }
1658.1.1 = { owner = NED controller = NED } # The Dutch gradually gained control
1683.1.1 = { add_core = NED }
1714.1.1 = { citysize = 250 } # British East India Company built Fort Marlborough
