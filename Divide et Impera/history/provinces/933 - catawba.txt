#933 - Catawba

owner = ETO
controller = ETO
culture = mississippian
religion = animism
capital = "Joara"
trade_goods = tobacco
hre = no
base_tax = 2
manpower = 1
citysize = 1000
add_core = ETO
discovered_by = PLA

discovered_by = QUA
discovered_by = ONE
discovered_by = CAD
discovered_by = CAH
discovered_by = ETO
discovered_by = CHE
discovered_by = CRE
discovered_by = SHA
discovered_by = CTW

1356.1.1 = { add_core = CTW }
1450.1.1  = { citysize = 1500 }
1500.1.1  = { citysize = 2125 }
1524.1.1  = { discovered_by = POR } # Diego Gomez
1550.1.1 = { owner = CTW controller = CTW remove_core = ETO culture = catawban
religion = shamanism }
1680.1.1  = { revolt_risk = 3 } # Conflicts with French and Spanish settlers
1703.1.1  = { revolt_risk = 0 }
1735.1.1  = {	owner = GBR
		controller = GBR
	    	culture = english
	    	religion = episcopalism
	    } # Settlement of Upcountry South Carolina
1748.1.1  = { capital = "Charlotte" }
1750.1.1  = { citysize = 2970 }
1760.1.1  = { add_core = GBR }
1760.1.19 = { revolt_risk = 5 } # Cherokee war
1761.1.1  = { revolt_risk = 0 } # Peace attempt
1764.7.1  = {	culture = american
		revolt_risk = 6
	    } # Growing unrest
1776.7.4  = {	owner = USA
		controller = USA
		add_core = USA
	    }
1782.11.1 = {	remove_core = GBR 
		revolt_risk = 0
	    } # Preliminary articles of peace, the British recognized Amercian independence
1800.1.1  = { citysize = 3250 }
