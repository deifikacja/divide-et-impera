owner = HES
controller = HES
add_core = HES
capital = "Darmstadt"
trade_goods = wool
fort1 = yes
culture = hessian
religion = catholic
base_tax = 3
manpower = 2
citysize = 5500
hre = yes
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1450.1.1    = { citysize = 6500 }
1500.1.1    = { fort2 = yes citysize = 7000 }
1527.1.1    = { marketplace = yes } # # Philipp der Grossmütige
1536.1.1    = { religion = protestant }
1547.1.1    = { fort2 = no } # Kassel's fortifications destroyed by catholics
1550.1.1    = { citysize = 9000 }
1620.1.1    = { regimental_camp = yes constable = yes fort2 = yes }
1685.1.1    = { citysize = 14000 customs_house = yes manpower = 1} # Reformed refugees find shelter in Kassel (-> Oberneustadt founded)
1730.3.23   = { owner = SWE controller = SWE courthouse = yes } # Friedrich I is nominally King in Sweden, even thogh he has little power there.
1751.4.5    = { owner = HES controller = HES citysize = 16000}
1807.7.9   = {	owner = WES
		controller = WES
		add_core = WES
		fort3 = yes
	     } # The Second Treaty of Tilsit, the kingdom of Westfalia
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1813.9.1   = { controller = RUS } # Occupied by Russian troops
1813.10.14 = {	owner = HES
		controller = HES
		remove_core = WES
	     } # Westfalia is dissolved after the Battle of Leipsig
