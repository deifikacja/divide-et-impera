#Akershus, incl. Oslo (Christiania), Sarpsborg, Tönsberg
 
owner = NOR
controller = NOR
add_core = NOR
culture = norwegian
religion = catholic
hre = no
base_tax = 5
trade_goods = fish
manpower = 2
capital = "Christiania"
citysize = 2700 # Estimated
fort1 = yes
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1400.1.1  = { citysize = 2950 }
1450.1.1  = { citysize = 3150 }
1500.1.1  = { citysize = 3340 }
1531.11.1 = { revolt_risk = 7 } # The Return of Christian II
1532.7.15 = { revolt_risk = 0 } # The Capture of Christian II
1536.1.1  = { religion = protestant } # Unknown date
1536.1.1  = { owner = DAN controller = DAN add_core = DAN } # 'Handfästningen'(Unknown date)
1540.1.1  = { marketplace = yes }
1550.1.1  = { citysize = 3625 }
1600.1.1  = { citysize = 4110 }
1630.1.1  = { trade_goods = naval_supplies } #Approximate date
1650.1.1  = { citysize = 4510 }
1700.1.1  = { citysize = 5400 }
1750.1.1  = { citysize = 7000 }
1800.1.1  = { citysize = 11900 }
1814.1.14 = {	owner = SWE
		revolt = { type = nationalist_rebels size = 1.5 }
		controller = REB
		remove_core = DAN
	    } # Norway is ceded to Sweden following the Treaty of Kiel
1814.5.17 = { revolt = {} owner = NOR controller = NOR } # Norway declares itself independent and elects Christian Frederik as king
