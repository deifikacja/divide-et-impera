#954 - Allegheny

culture = iroquis
religion = shamanism
capital = "Allegheny"
trade_goods = unknown
hre = no
base_tax = 2
manpower = 1
native_size = 5
native_ferocity = 0.5
native_hostileness = 4
discovered_by = IRO

1655.1.1 = { owner = IRO controller = IRO citysize = 100 culture = iroquis }#Defeat of the Erie in Beaver Wars
1679.1.1  = { discovered_by = FRA }
1754.4.18 = {	owner = FRA
		controller = FRA
		citysize = 400
		culture = cosmopolitan_french
	    	religion = catholic
		trade_goods = fur
	    } # Construction of Fort Duquesne
1763.2.10 = {	owner = GBR
		controller = GBR
		culture = english
		religion = episcopalism
	    } # Treaty of Paris - ceded to Britain, France gave up its claim
1763.3.1  = { revolt_risk = 6 } # Native discontent with the British rule
1764.6.1  = { revolt_risk = 0 } # Peace negotiations
1764.7.1  = {	culture = american
		revolt_risk = 6
	    } # Growing unrest
1774.1.1  = { add_core = QUE } # United with Canada under the Quebec Act of 1774
1776.7.4  = {	owner = USA
		controller = USA
		add_core = USA
		remove_core = QUE
	    } # Declaration of independence
1782.11.1 = { revolt_risk = 0 } # Preliminary articles of peace, the British recognized Amercian independence
1794.6.1  = { revolt_risk = 5 }	# Whiskey rebellion, opposition to federal taxation
1794.9.7  = { revolt_risk = 0 } # The revolt is suppressed
1800.1.1  = { citysize = 1200 }