#165 - Bern

owner = SWI
controller = SWI 
add_core = SWI 
culture = swabian
religion = catholic
capital = "Bern"
trade_goods = iron
hre = yes
base_tax = 4
manpower = 3
citysize = 4500
fort1 = yes
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1356.1.1 = { add_core = BER }
1450.1.1 = { citysize = 5000 }
1500.1.1 = { citysize = 5520 }
1513.7.3 = { revolt_risk = 7 } # Luzern Peasant War
1515.1.1 = { revolt_risk = 0 }
1528.1.1 = { religion = reformed }
1529.1.1 = { owner = BER controller = BER }#Kappel wars
1531.11.20 = { owner = SWI controller = SWI }#Kappel wars ends
1530.1.1 = { print_works = yes } #Swiss become a print center in Europe
1550.1.1 = { citysize = 6000 marketplace = yes }
1600.1.1 = { citysize = 7200 }
1648.1.1 = { hre = no } # Switzerland excluded from The Holy Roman Empire
1650.1.1 = { citysize = 8500 constable = yes }
1653.1.1 = { revolt_risk = 5 } # Peasant rebellion against overtaxation
1654.1.1 = { revolt_risk = 0 }
1655.1.1 = { owner = BER controller = BER }#Villmerger wars
1656.1.24 = { owner = SWI controller = SWI }#Villmerger wars ends
1700.1.1 = { citysize = 10870 }
1705.1.1 = { tax_assessor = yes }
1712.1.1 = { owner = BER controller = BER }#Villmerger wars
1712.7.24 = { owner = SWI controller = SWI }#Villmerger wars ends
1750.1.1 = { citysize = 12400 }
1798.3.5  = { controller = RFR } # French occupation
1798.4.12 = { controller = SWI } # The establishment of the Helvetic Republic
1798.4.15 = { revolt = { type = revolutionary_rebels size = 3 } controller = REB } # The Nidwalden Revolt
1798.9.1  = { revolt = {} controller = SWI } # The revolt is supressed
1800.1.1  = { citysize = 14038 }
1802.6.1  = { revolt = { type = revolutionary_rebels size = 1 } controller = REB } # Swiss rebellion
1802.9.18 = { revolt = {} controller = SWI }

