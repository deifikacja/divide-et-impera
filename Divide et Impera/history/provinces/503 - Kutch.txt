#503 - Kutch

owner = KTC
controller = KTC
culture = sindhi
religion = jainism
capital = "Bhuj"
trade_goods = wool
hre = no
cot = yes
base_tax = 2
manpower = 1
citysize = 5170
add_core = KTC
fort1 = yes
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = MUG

1450.1.1 = { citysize = 5690 }
1500.1.1 = { citysize = 6980 }
1526.1.1 = {	owner = MUG
		controller = MUG
		add_core = MUG
	   } # Mughal rule
1544.1.1  = {	owner = KTC
		controller = KTC
	   } # Sher Shah Suri defeats the Moghuls
1550.1.1 = { citysize = 8240 cot = no }
1600.1.1 = { citysize = 9560 marketplace = yes discovered_by = TUR }
1650.1.1 = { citysize = 10800 }
1700.1.1 = { citysize = 11400 }
1747.1.1 = {	owner = KTC
		controller = KTC
		remove_core = MUG	
	   }
1750.1.1 = { citysize = 13000 }
1800.1.1 = { citysize = 14420 }
1813.1.1 = {	owner = SND
		controller = SND
		add_core = SND
	   }
1815.1.1 = {	owner = GBR
		controller = GBR
		add_core = GBR
	   }