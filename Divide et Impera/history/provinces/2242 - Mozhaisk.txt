#1036 - Pereyaslav Zalesky

owner = MOS 
controller = MOS    
culture = russian	
discovered_by = ordu
religion = orthodox
hre = no
base_tax = 2
trade_goods = grain   
manpower = 1
capital = "Mozhaisk"
citysize = 1000
fort1 = yes
add_core = MOS
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = CRI
discovered_by = KAZ
discovered_by = GOL
discovered_by = AST
discovered_by = NOG
discovered_by = BSH
discovered_by = QAS

1382.8.26 = { citysize = 1460 }
1400.1.1  = { citysize = 2000 }
1450.1.1  = { citysize = 3070 discovered_by = SIB }
1500.1.1  = { citysize = 4029 }
1503.3.22 = {	owner = RUS
		controller = RUS
		add_core = RUS
		remove_core = MOS
	    } #capital of the Russian Empire
1550.1.1  = { citysize = 5000 }
1560.1.1  = { base_tax = 4 } # Treasury reform
1580.1.1  = { base_tax = 5 }
1598.1.1  = { revolt_risk = 5 } # "Time of troubles"
1600.1.1  = { citysize = 6000 }
1613.1.1  = { revolt_risk = 0 } # Order returned, Romanov dynasty
1638.1.1  = { citysize = 6500 }
1670.1.1  = { revolt_risk = 8 } # Stepan Razin
1671.1.1  = { revolt_risk = 0 } # Razin is captured
1710.1.1  = { citysize = 7000 }
1711.1.1  = { base_tax = 6 } # Governmental reforms and the absolutism
1725.1.1  = { citysize =7500 }
1738.1.1  = { citysize = 8000 }
1767.1.1  = { base_tax = 7 } # Legislative reform
1773.1.1  = { revolt_risk = 5 } # Emelian Pugachev, Cossack insurrection
1774.9.14 = { revolt_risk = 0 } # Pugachev is captured
1775.1.1  = { citysize = 8500 }
1785.1.1  = { citysize = 9000 }
1811.1.1  = { citysize = 9500 }
1813.1.1  = { citysize = 10000 }
1825.1.1  = { citysize = 10500 }
