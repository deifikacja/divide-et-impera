#795 - Chichas

owner = TRC
controller = TRC
add_core = TRC
culture = aimara
religion = animism
capital = "Chichas"
trade_goods = gold
hre = no
base_tax = 4
manpower = 1
citysize = 6570
fort1 = yes
discovered_by = INC
discovered_by = CHM
discovered_by = INC
discovered_by = CHM
discovered_by = HNV
discovered_by = MNO
discovered_by = PCN
discovered_by = QUT
discovered_by = CII
discovered_by = HNC
discovered_by = CAA
discovered_by = CRC
discovered_by = TRC
discovered_by = CNC
discovered_by = SCN

discovered_by = CNE
discovered_by = CNB
discovered_by = NZC
discovered_by = AYA
discovered_by = DIA
discovered_by = HUA
discovered_by = TIT
discovered_by = ATA

1499.1.1  = { owner = INC controller = INC add_core = INC }
1500.1.1  = { citysize = 7600 }
1533.8.29 = {	discovered_by = SPA
		owner = SPA
		controller = SPA
	    }# The death of Atahualpa
1545.4.1  = { capital = "Potosi" citysize = 9200 } # Mining town
1550.1.1  = { citysize = 21500 religion = catholic}
1558.8.29 = { add_core = SPA }
1600.1.1  = { citysize = 85000 }
1621.1.1  = { refinery = yes } # The Mercury amalgam process is introduced 
1650.1.1  = { citysize = 150000 marketplace = yes } # The mining flourished making Potosi the largest city in the Americas
1700.1.1  = { citysize = 106540 } # Major decline due to exhaustion of the first rich vein & the rapid decrease of the Indian pop.
1750.1.1  = { citysize = 93000 add_core = PEU }
1800.1.1  = { citysize = 75000 } # Mines almost depleted
1809.7.16 = { revolt = { type = colonial_rebels size = 3 } controller = REB } # Bolivian War of Independence