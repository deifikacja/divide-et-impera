#651 - Sulu

culture = malay-polynesian
religion = sunni
capital = "Jolo"
trade_goods = unknown
hre = no
base_tax = 2
manpower = 1
native_size = 45
native_ferocity = 4.5
native_hostileness = 1 
add_core = SUL
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM

1390.1.1 = { owner = SUL controller = SUL citysize = 4500 trade_goods = fish }
1500.1.1 = { citysize = 4975 }
1521.1.1 = { discovered_by = SPA } # Ferdinand Magellan 
1550.1.1 = { citysize = 5575 }
1600.1.1 = { citysize = 6260 }
1638.1.1 = {	owner = SPA
		controller = SPA
		fort1 = yes
	   } # Spanish occupation
1646.1.1 = {	owner = SUL
		controller = SUL
	   }
1650.1.1 = { citysize = 6810 }
1700.1.1 = { citysize = 7422 }
1750.1.1 = { citysize = 8340 }
1800.1.1 = { citysize = 8800 }