#661 - Dali

owner = YUA
controller = YUA
culture = bai
religion = animism
capital = "Dali"
trade_goods = wool
hre = no
base_tax = 4
manpower = 1
citysize = 12400
add_core = YUA	
discovered_by = ordu
add_core = MNG
add_core = SNG
add_core = QIN
add_core = DAL
fort1 = yes
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = YUA

1368.1.22 = { revolt = { type = nationalist_rebels size = 4 } owner = MNG controller = REB remove_core = YUA }#The Ming didn't send army until 1381
1381.1.1 = { revolt = { } controller = MNG }
1450.1.1 = { citysize = 13190 }
1500.1.1 = { citysize = 14570 }
1550.1.1 = { citysize = 16100 }
1600.1.1 = { citysize = 17124 }
1650.1.1 = { citysize = 19543 }
1662.1.1 = {	owner = MCH
		controller = MCH
		add_core = MCH
		remove_core = MNG
	   } # The Manchu conquest
1683.1.1 = {	owner = QNG
		controller = QNG
		add_core = QNG
		remove_core = MCH
	   } # The Qing Dynasty
1700.1.1 = { citysize = 20870 }
1750.1.1 = { citysize = 22100 }
1800.1.1 = { citysize = 24687 }