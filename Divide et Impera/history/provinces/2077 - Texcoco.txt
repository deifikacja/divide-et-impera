#2077 - Texcoco - one of the original members of the Aztec Triple Alliance

owner = TEX
controller = TEX
culture = nahuatl
religion = animism 
capital = "Texcoco" 
trade_goods = grain
hre = no 
base_tax = 4 
manpower = 3 
citysize = 14500
add_core = TEX
discovered_by = TEX
discovered_by = TLC
discovered_by = HST
discovered_by = TEP
discovered_by = MAY
discovered_by = MMA
discovered_by = QUI
discovered_by = TAY
discovered_by = ACA
discovered_by = CHT
discovered_by = CPE
discovered_by = TUX
discovered_by = UAY
discovered_by = CUP
discovered_by = ECA
discovered_by = AKC

discovered_by = ZAP
discovered_by = AZT
discovered_by = MIX
discovered_by = TLA
discovered_by = YPT
discovered_by = MTT
discovered_by = TAR
discovered_by = TEO
discovered_by = TLC

1430.1.1  = { owner = AZT controller = AZT add_core = AZT religion = teotl } # The Aztec Triple Alliance
1450.1.1  = { citysize = 17000 }
1500.1.1  = { citysize = 19740 }
1519.1.1  = { discovered_by = SPA }
1521.8.13 = {	owner = SPA 
		controller = SPA 
		citysize = 3000
		religion = catholic
	   } # The last Aztec emperor surrendered, Mexico city is founded on top of the ruins 
1546.1.1 = { add_core = SPA } 
1550.1.1 = { citysize = 4000 }
1600.1.1 = { citysize = 5000 } 
1624.1.1 = { revolt_risk = 5 } # Riots
1626.1.1 = { revolt_risk = 0 culture = castillian } 
1650.1.1 = { citysize = 6500 } 
1700.1.1 = { citysize = 7000 } 
1720.1.1 = { citysize = 8000 } # Volcano eruption 
1750.1.1 = { citysize = 9000 add_core = MEX } 
1800.1.1 = { citysize = 10000 } 
1810.9.16 = { owner = MEX controller = MEX } # Mexican War of Independence
