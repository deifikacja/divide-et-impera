#1280 - Ambohidratima

owner = AMB
controller = AMB
add_core = AMB
add_core = MER
culture = madagasque
religion = animism
capital = "Ambohidratima"
citysize = 1000
manpower = 1
trade_goods = slaves
hre = no
base_tax = 2
discovered_by = MEN
discovered_by = AMB
discovered_by = ANT
discovered_by = MER

1400.1.1 = { citysize = 1120 }
1500.1.1 = { citysize = 1300 }
1500.8.20 = { discovered_by = POR } #Diego Dias
1600.1.1 = { citysize = 1600 }
1690.1.1 = { discovered_by = BOI }
1691.2.11 = {  }
1700.1.1 = { citysize = 2700 }
1712.1.1 = { discovered_by = TAM }
1797.1.1 = { owner = MER controller = MER add_core = MER remove_core = AMB } #Annexed by Imernia
1817.8.23 = { citysize = 5000 owner = MDG controller = MDG add_core = MDG remove_core = MER } 