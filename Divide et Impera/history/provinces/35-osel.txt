#�sel and Dag�, incl. Sonnenburg and Arensburg.

owner = LIV
controller = LIV
add_core = LIV
add_core = TEU
culture = estonian
religion = catholic
hre = no
base_tax = 2
trade_goods = fish
manpower = 1
capital = "Arensburg"
citysize = 1000 # Estimated
fort1 = yes
fort2 = yes
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ

1450.1.1   = { citysize = 1100 }
1466.10.19 = { remove_core = TEU } # Peace treaty, "Peace of Torun"
1500.1.1   = { citysize = 1250 }
1542.1.1   = { religion = protestant} #Unknown date
1550.1.1   = { citysize = 1600 }
1560.1.1   = {	owner = DAN
		controller = DAN
		add_core = DAN
	     	remove_core = LIV
	     } #Unknown date
1563.1.1   = { capital = "Arensburg" } #Unknown date
1568.7.25  = { controller = SWE } #Nordic Seven-year War
1570.12.13 = { owner = DAN controller = DAN } #The Peace of Stettin
1600.1.1   = { citysize = 1880 }
1645.8.13  = {	owner = SWE
		controller = SWE
		add_core = SWE
		remove_core = DAN
	     } #The Peace of Br�msebro
1650.1.1   = { citysize = 1960 }
1700.1.1   = { citysize = 2240 }
1710.9.20  = { controller = RUS } #Estimated date
1721.8.30  = {	owner = RUS
		add_core = RUS 
		remove_core = SWE
	     } #The Peace of Nystad
1750.1.1   = { citysize = 2500 }
1800.1.1   = { citysize = 2870 }