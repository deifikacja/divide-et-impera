#1246 - Aorangi

culture = maori
religion = animism
capital = "Aorangi"
trade_goods = unknown
hre = no
base_tax = 2
manpower = 1
native_size = 10
native_ferocity = 4
native_hostileness = 9

1642.1.1 = { discovered_by = NED } # Abel Tasman
