#806 - Nazca

owner = NZC
controller = NZC
culture = aimara
religion = animism
capital = "Nazca"
trade_goods = fish
hre = no
base_tax = 2
manpower = 1
citysize = 1850
add_core = NZC
discovered_by = INC
discovered_by = CHM
discovered_by = INC
discovered_by = CHM
discovered_by = HNV
discovered_by = MNO
discovered_by = PCN
discovered_by = QUT
discovered_by = CII
discovered_by = HNC
discovered_by = CAA
discovered_by = CRC
discovered_by = TRC
discovered_by = CNC
discovered_by = SCN

discovered_by = CNE
discovered_by = CNB
discovered_by = NZC
discovered_by = AYA
discovered_by = DIA
discovered_by = HUA
discovered_by = TIT
discovered_by = ATA

1450.1.1  = { citysize = 2030 }
1499.1.1  = { owner = INC controller = INC }
1500.1.1  = { citysize = 2650 }
1533.8.29 = {	discovered_by = SPA
		owner = SPA
		controller = SPA
		culture = castillian
		religion = catholic
	    }# The death of Atahualpa
1535.1.1  = { revolt_risk = 8 } # Fighting broke out when Almagro seized Cuzco
1538.1.1  = { revolt_risk = 5 } # Almagro is defeated & executed
1541.1.1  = { revolt_risk = 6 } # Pizzaro is assassinated by supporters of Almagro, his brother assumed control
1548.1.1  = { revolt_risk = 0 } # Gonzalo Pizzaro is also executed & Spain succeeds in reasserting its authority
1550.1.1  = { citysize = 3090 }
1558.8.29 = { add_core = SPA }
1600.1.1  = { citysize = 3830 }
1650.1.1  = { citysize = 4176 }
1700.1.1  = { citysize = 5787 }
1750.1.1  = { citysize = 6300 add_core = PEU }
1800.1.1  = { citysize = 7640 }