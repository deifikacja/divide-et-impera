#594 - Pattani

owner = PAT
controller = PAT
culture = malayan
religion = buddhism
capital = "Pattani"
trade_goods = fish
hre = no
base_tax = 3
manpower = 1
citysize = 6100
add_core = PAT
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1375.1.1  = {	owner = AYU
		controller = AYU
		add_core = AYU
	    }
1450.1.1  = { citysize = 8900 }
1451.1.1  = {	owner = PAT
		controller = PAT
	    }
1457.1.1  = { religion = sunni }
1500.1.1  = { citysize = 26890 }
1500.1.1  = {	owner = MLC
		controller = MLC
		add_core = MLC
	    }
1511.11.1 = {	owner = PAT
		controller = PAT
		remove_core = MLC
	    }
1516.1.1  = { discovered_by = POR } # Godinho de Eredia
1550.1.1  = { citysize = 31000 }
1600.1.1  = { citysize = 35022 }
1650.1.1  = { citysize = 40100 }
1700.1.1  = { citysize = 41574 }
1750.1.1  = { citysize = 40678 }
1760.1.1  = { revolt_risk = 4 } # The Pattani kingdom began to decline
1782.1.1  = { revolt_risk = 2 } # The Chakri Dynasty is established by Rama I
1786.1.1  = {	owner = AYU
		controller = AYU
		revolt_risk = 5
	    }
1787.1.1 = {	owner = PAT
		controller = PAT
		revolt_risk = 0
	    }
1800.1.1  = { citysize = 23000 }
1816.1.1  = {	owner = AYU
		controller = AYU
	    } # The end of the Pattani kingdom