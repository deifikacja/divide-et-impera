#1024 - Echigo - Ueasugi possesion

owner = UES
controller = UES
culture = japanese
discovered_by = ordu
religion = shinto
capital = "Echigo"
trade_goods = grain
hre = no
base_tax = 3
manpower = 1
citysize = 15345
add_core = UES
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM

1450.1.1   = { citysize = 15802 }
1500.1.1   = { citysize = 17665 }
1542.1.1   = { discovered_by = POR }
1550.1.1   = { citysize = 19200 }
1586.1.1 = { owner = TOY controller = TOY add_core = TOY }
1599.1.1 = { owner = TKG controller = TKG add_core = TKG }#Eastern Alliance
1600.10.22 = { citysize = 15678 owner = JAP controller = JAP add_core = JAP }#Sekahigara
1650.1.1   = { citysize = 23300 }
1700.1.1   = { citysize = 25534 }
1750.1.1   = { citysize = 27570 }
1800.1.1   = { citysize = 29800 }