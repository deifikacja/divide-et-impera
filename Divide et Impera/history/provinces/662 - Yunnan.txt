#662 - Yunnan

owner = YUA
controller = YUA
culture = bai
religion = buddhism
capital = "Kunming"
trade_goods = tea
hre = no
base_tax = 5
manpower = 2
citysize = 43210
add_core = YUA	
discovered_by = ordu
add_core = MNG
add_core = SNG
add_core = QIN
add_core = DAL
fort1 = yes
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = YUA

1368.1.22 = { revolt = { type = nationalist_rebels size = 4 } owner = MNG controller = REB remove_core = YUA }#The Ming didn't send army until 1381
1381.1.1 = { revolt = { } controller = MNG }
1450.1.1 = { citysize = 43210 }
1500.1.1 = { citysize = 45100 }
1505.1.1 = { marketplace = yes }
1550.1.1 = { citysize = 47120 }
1600.1.1 = { citysize = 48785 }
1650.1.1 = { citysize = 50105 }
1662.1.1 = {	owner = MCH
		controller = MCH
		add_core = MCH
		remove_core = MNG
	   } # The Manchu conquest
1674.1.1 = {	owner = DAL
		controller = DAL
	   } # The Yunnan rebellion
1678.1.1 = {	owner = MCH
		controller = MCH
	   } # End of the rebellion
1683.1.1 = {	owner = QNG
		controller = QNG
		add_core = QNG
		remove_core = MCH
	   } # The Qing Dynasty
1700.1.1 = { citysize = 52000 }
1726.1.1 = {	controller = REB
	   } # The Yunnan rebellion
1729.1.1 = {	controller = QNG
	   } # The Qing power restored
1750.1.1 = { citysize = 54280 }
1800.1.1 = { citysize = 58470 }