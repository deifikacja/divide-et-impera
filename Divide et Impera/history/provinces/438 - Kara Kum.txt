#438 - Kara Kum

owner = GOL
controller = GOL
culture = turkmeni
religion = sunni
capital = "Kara Kum"
trade_goods = wool
hre = no
base_tax = 1
manpower = 1
citysize = 1130
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = TUR
discovered_by = ordu

1356.1.1 ={add_core = KHO}
1387.1.1 = { owner = TIM controller = TIM add_core = TIM remove_core = GOL }
1405.2.18 = { add_core = SAM }
1447.3.12  = {   owner = SAM
		controller = SAM
		remove_core = TIM
	    }#Shahrokh's death - partition of the Empire
1450.1.1 = { citysize = 1910 }
1500.1.1 = { citysize = 2885 }
1506.1.1 = {	owner = SHY
		controller = SHY 
		add_core = SHY
		remove_core = SAM
	   }
1511.1.1 = {	owner = KHI 
	   	controller = KHI
	   	add_core = KHI
	   } # Conquered by nomadic Uzbeks
1550.1.1 = { citysize = 3450 discovered_by = TUR }
1600.1.1 = { citysize = 4510 }
1650.1.1 = { citysize = 5240 }
1700.1.1 = { citysize = 5990 }
1740.1.1 = {	owner = PER
		controller = PER
		add_core = PER
	   } # Captured by Nadir Shah
1747.1.1 = {	owner = KHI
		controller = KHI
		remove_core = PER
	   } # The death of Nadir Shah ends the domination
1750.1.1 = { citysize = 6400 }
1800.1.1 = { citysize = 7210 }