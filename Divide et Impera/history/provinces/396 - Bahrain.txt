#396 - Bahrain

owner = HOR
controller = HOR
culture = najdi_arabic
religion = shiite
capital = "Manama"
trade_goods = wool
hre = no
base_tax = 1
manpower = 1
citysize = 1200
add_core = BHR
add_core = ALH
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1440.1.1 = { owner = ALH controller = ALH }
1450.1.1 = { citysize = 1540 }
1480.1.1 = { discovered_by = TUR }
1500.1.1 = { citysize = 1657 }
1521.1.1 = {	discovered_by = POR 
		owner = POR
		controller = POR
		add_core = POR
	   } # Invaded by the Portuguese
1550.1.1 = { citysize = 2340 }
1600.1.1 = { citysize = 3100 }
1602.1.1 = {	owner = PER
		controller = PER
		add_core = PER
		remove_core = POR
	   } # Invaded by the Persians under Shah Abbas I
1650.1.1 = { citysize = 4205 }
1700.1.1 = { citysize = 4888 }
1750.1.1 = { citysize = 5340 }
1783.1.1 = {	owner = BHR
		controller = BHR
		remove_core = PER
	   } # Reconquered by the Bani Utubs
1800.1.1 = { citysize = 6230 owner = OMA controller = OMA }
1801.1.1 = { owner = BHR controller = BHR }