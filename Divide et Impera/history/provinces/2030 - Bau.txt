#2030 - Bau

add_core = BUU
culture = malay-polynesian
religion = animism
capital = "Bau"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 5
native_ferocity = 0.5
native_hostileness = 2
discovered_by = BUU
discovered_by = BUA

1643.1.1 = { discovered_by = NED } # Abel Tasman
1700.1.1 = { owner = BUU controller = BUU citysize = 1000 trade_goods = fish }