#855 - Huichol

owner = HIC
controller = HIC
add_core = HIC
culture = corachol
religion = animism 
capital = "Huichol" 
trade_goods = gold
hre = no 
base_tax = 2 
manpower = 1 
citysize = 1200
discovered_by = HIC
discovered_by = MAY
discovered_by = MMA
discovered_by = QUI
discovered_by = TAY
discovered_by = ACA
discovered_by = CHT
discovered_by = CPE
discovered_by = TUX
discovered_by = UAY
discovered_by = CUP
discovered_by = ECA
discovered_by = AKC

discovered_by = ZAP
discovered_by = AZT
discovered_by = MIX
discovered_by = TLA
discovered_by = YPT
discovered_by = MTT
discovered_by = TAR
discovered_by = HST

1532.1.1   = {	discovered_by = SPA 
		owner = SPA 
		controller = SPA
		citysize = 3555
		capital = "Tepic"
	     } 
1550.1.1   = { citysize = 4100 } 
1554.1.1   = { add_core = SPA }
1578.1.1   = { discovered_by = ENG } # Drake 
1592.1.1 = { revolt = { type = nationalist_rebels size = 0 } }
1593.1.1 = { revolt = {}}
1600.1.1   = { citysize = 5780 } 
1650.1.1   = { citysize = 6552 } 
1700.1.1   = { citysize = 7231 } 
1702.1.1 = { revolt = { type = nationalist_rebels size = 0 } }
1703.1.1 = { revolt = { } culture = castillian religion = catholic remove_core = HIC }#Last rebellion crushed
1750.1.1   = { citysize = 8800 add_core = MEX } 
1800.1.1   = { citysize = 11420 }
1810.9.16  = { owner = MEX controller = MEX } # Mexican War of Independence