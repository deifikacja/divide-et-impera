# 1331 - Pelym

owner = PEL
controller = PEL
add_core = PEL
culture = mansi
religion = shamanism
capital = "Pelym"
trade_goods = grain
hre = no
base_tax = 1
manpower = 1
citysize = 1100
discovered_by = TYU
discovered_by = SIB
discovered_by = PRM
discovered_by = PEL
discovered_by = NOV

1478.1.1 = {	discovered_by = MOS
		add_core = MOS
	   }
1500.1.1 = { citysize = 520 }
1503.3.22 = { 	add_core = RUS remove_core = MOS }
1599.1.1 = {	discovered_by = RUS 
		owner = RUS
		controller = RUS
		citysize = 565
	   }
1600.1.1 = { citysize = 700 }
1606.1.1 = { revolt_risk = 3 } # Rebellions against Russian rule
1608.1.1 = { revolt_risk = 5 }
1610.1.1 = { revolt_risk = 2 }
1616.1.1 = { revolt_risk = 6 }
1620.1.1 = { revolt_risk = 0 }
1650.1.1 = { citysize = 800 }
1715.1.1 = { citysize = 900 religion = orthodox }
1750.1.1 = { citysize = 1000 }
1800.1.1 = { citysize = 1100 }