#1188 - Masapa

owner = GZI
controller = GZI
add_core = GZI
culture = shona
religion = animism
capital = "Masapa"
citysize = 4000
manpower = 1
trade_goods = gold
hre = no
base_tax = 2
discovered_by = GZI
discovered_by = MAQ
discovered_by = ZZA

1400.1.1 = { citysize = 4680 } 
1490.1.1 = { revolt_risk = 7 } #Uprising by the Changamire clan
1494.1.1 = { revolt_risk = 0 } #Changamire expelled from region
1495.1.1 = { owner = ZIM controller = ZIM add_core = ZIM discovered_by = ZIM remove_core = GZI revolt_risk = 2 }
1500.1.1 = { citysize = 6000 } #Recentering of Mutapa state following civil war, capital moves to Masapa
1513.1.1 = { discovered_by = POR } #Travels of Antonio Fernandes in hinterland of Sofala
1550.1.1 = { citysize = 6500 }
#1541.1.1 Portugal has tradepost at Masapa
1561.1.1 = { revolt_risk = 3 } #Proselytization efforts of Goncalo Da Silveira cause disturbances
1596.1.1 = { revolt_risk = 8 } #Maravi invasions disrupt region
1597.1.1 = { revolt_risk = 0 } #Mutapa allow Maravi to settle among them
1598.1.1 = { revolt_risk = 4 } #Civil War after Gatse executes uncle
1599.1.1 = { revolt_risk = 8 } #Maravi rebel against the Mutapa
1600.1.1 = { citysize = 5400 }
1602.1.1 = { revolt_risk = 0 } #Maravi expelled from the region & order restored with Portuguese help
1605.1.1 = { revolt_risk = 7 } #Matuzvianye rebels against Gatsi Rusere, sparking civil war
1623.1.1 = { revolt_risk = 6 } #Maravi invasion
1624.1.1 = { revolt_risk = 0 } #Maravi expelled from the region
1625.1.1 = { revolt_risk = 6 } #Succession war in wake of death of Gatsi Rusere
1628.1.1 = { revolt_risk = 0 } #Nyambu Kapararidze wins civil war
1629.1.1 = { revolt_risk = 3 } #Kapararidze overthrown by Mavura, long civil war begins
#1629.1.1 = Mavura signs treaty with Portuguese - makes ZIM a POR vassal
1631.1.1 = { revolt_risk = 8 } #Kapararidze launches revolt to overthrow Mavura and end POR domination
1632.1.1 = { revolt_risk = 0 } #Kapararidze revolt defeated
1650.1.1 = { trade_goods = grain } # Overworking of gold mines leads to collapse of gold production
1683.1.1 = { owner = XXX controller = XXX discovered_by = ROZ remove_core = ZIM trade_goods = unknown }
1720.1.1 = { capital = "Tsokoto" } #Recentering of the Mutapa state in wake of Rozvi invasions