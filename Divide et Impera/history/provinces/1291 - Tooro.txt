#1291 - Tooro

owner = BUN
controller = BUN
add_core = BUN
add_core = TOR
culture = bantu
religion = animism
capital = "Tooro"
citysize = 1000
manpower = 1
trade_goods = ivory
hre = no
base_tax = 1
discovered_by = BRN
discovered_by = BUG
discovered_by = BUN
discovered_by = RWA


1400.1.1 = { citysize = 1040 }
1430.1.1 = { discovered_by = NKO }
1500.1.1 = { citysize = 1100 }
1600.1.1 = { citysize = 1190 }
1700.1.1 = { citysize = 1320 }
1800.1.1 = { citysize = 1500 }
1822.1.1 = { owner = TOR controller = TOR discovered_by = TOR }