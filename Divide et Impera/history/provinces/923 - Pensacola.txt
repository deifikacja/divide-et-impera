#923 - Pensacola

add_core = APL
culture = mississippian
religion = animism
capital = "Pensacola"
trade_goods = unknown
hre = no
base_tax = 2
manpower = 1
native_size = 10
native_ferocity = 2
native_hostileness = 7
discovered_by = PLA

discovered_by = QUA
discovered_by = ONE
discovered_by = CAD
discovered_by = CAH
discovered_by = ETO
discovered_by = APL
discovered_by = ALM

1519.1.1  = { discovered_by = SPA } # Alvarez de Pi�eda explored the Gulf Coast
1550.1.1  = { owner = APL controller = APL citysize = 3240 trade_goods = grain culture = creek
religion = shamanism }
1600.1.1  = { citysize = 3410 }
1650.1.1  = { citysize = 4500 }
1698.1.1  = {	owner = SPA
		controller = SPA
		culture = castillian
		religion = catholic
		trade_goods = cotton
	    }
1723.1.1  = { add_core = SPA }
1750.1.1  = { citysize = 4857 }
1763.2.10 = {	owner = GBR
		controller = GBR
		remove_core = SPA
		culture = english
		religion = episcopalism
	    } # Part of British West Florida after the Treaty of Paris
1783.9.3  = { owner = SPA controller = SPA } # Part of Spanish West Florida
1784.1.1  = { revolt_risk = 7 } # Controversy over the Treaty of Versaille & the Treaty of Paris, Spanish or American?
1800.1.1  = { citysize = 5600 }
1808.1.1   = { add_core = SPA }
1819.2.22  = {	owner = USA
		controller = USA
		add_core = USA
		remove_core = SPA
	   } # The Adams-On�s Treaty