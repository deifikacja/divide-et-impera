#2024 - Marrapikurrinya

culture = aboriginal
religion = animism
capital = "Yawuru"
hre = no
trade_goods = unknown
manpower = 1
base_tax = 2
native_size = 5
native_ferocity = 0.5
native_hostileness = 9
