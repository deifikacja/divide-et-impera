#1229 - Berber

owner = MKR
controller = MKR
add_core = MKR
culture = nubian 
religion = eastern_churches
capital = "Berber"
citysize = 2750
manpower = 1
trade_goods = cattle
hre = no
base_tax = 2
discovered_by = ALO
discovered_by = ETH
discovered_by = MAM
discovered_by = MKR

1356.1.1 = { revolt_risk = 8 } #civil war between christians and muslims
1397.1.1 = { revolt_risk = 0
		owner = NUB
	   	controller = NUB
	   	add_core = NUB
		discovered_by = NUB
		citysize = 3400
		trade_goods = wool
 	   } #decline of ancient kingdom of Makuria
1412.1.1 = { owner = MKR controller = MKR }#Banu Kanz
1517.1.1 = { add_core = TUR  } # Mamluks fall to Ottomans, Ottomans do not advance up Nile
1530.1.1 = { religion = sunni } #spread of Islam leads to shift in religion affiliation of region
1540.1.1 = {	owner = TUR
		controller = TUR
		remove_core = MAM
		remove_core = MKR
		citysize = 3000
	   } #Ottomans occupy Lower Nubia
1650.1.1 = { citysize = 4500 }
1700.1.1 = { citysize = 5200 }
1706.1.1 = { revolt_risk = 3 } #Badi III faces revolt over development of slave army at aristocrats expense
1709.1.1 = { revolt_risk = 0 } #Badi III faces revolt over development of slave army at aristocrats expense
1718.1.1 = { revolt_risk = 6 } #Unsa III comes into conflict with aristocrats
1720.1.1 = { revolt_risk = 0 } #Unsa III deposed, new Funj dynasty set up by aristocrats
1750.1.1 = { citysize = 6500 }
1800.1.1 = { citysize = 7000 }