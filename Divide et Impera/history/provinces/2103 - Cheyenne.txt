#2103 - Cheyenne

add_core = CHY
religion = shamanism
capital = "Cheyenne"
trade_goods = unknown
hre = no
base_tax = 2
manpower = 1
native_size = 25
native_ferocity = 2
native_hostileness = 8
discovered_by = PAW
discovered_by = CHY
discovered_by = NAK
discovered_by = DAK
discovered_by = LAK

1650.1.1 = { culture = cheyenne citysize = 1200 trade_goods = cattle owner = CHY controller = CHY }#Cheyennes arrive from Great Lakes area
1803.4.3 = {
		add_core = USA
	    } # The Louisiana purchase