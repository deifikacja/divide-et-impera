#925 - Tuskegee

owner = PLA
controller = PLA
culture = mississippian
religion = animism
capital = "Tuskegee"
trade_goods = grain
hre = no
base_tax = 4
manpower = 1
citysize = 2500
add_core = CHE
add_core = PLA
discovered_by = PLA

discovered_by = QUA
discovered_by = ONE
discovered_by = CAD
discovered_by = CAH
discovered_by = ETO
discovered_by = CHE
discovered_by = CRE
discovered_by = SHA
discovered_by = CHW
discovered_by = ALM

1500.1.1  = { citysize = 3225 }
1540.1.1  = { discovered_by = SPA } # Hernando De Soto
1550.1.1  = { citysize = 3900 owner = CHE controller = CHE remove_core = PLA culture = creek
religion = shamanism }
1600.1.1  = { citysize = 4500 }
1650.1.1  = { citysize = 5340 }
1700.1.1  = { citysize = 6200 }
1750.1.1  = { citysize = 6980 add_core = LOU }
1755.1.1  = { fort1 = yes } # Fort Tombeche
1763.2.10 = {	owner = GBR
		controller = GBR
		religion = episcopalism
		culture = english
		trade_goods = cotton
	    } # Treaty of Paris - ceded to Britain, France gave up its claim
1783.9.3  = {	owner = USA
		controller = USA
		add_core = USA
	    }
1800.1.1  = { citysize  = 7200 }