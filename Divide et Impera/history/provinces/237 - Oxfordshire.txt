#237 - Oxfordshire

owner = ENG
controller = ENG
culture = english
religion = catholic
hre = no
base_tax = 7
trade_goods = grain
manpower = 2
capital = "Oxford"
citysize = 6000 #Includes Reading
add_core = ENG
fort1 = yes
university = yes #Oxford University
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ

1381.1.1  = { revolt_risk = 6 }
1381.5.1  = { revolt = { type = revolutionary_rebels size = 0 } controller = REB } # Wat Tyler's rebellion
1381.6.15 = { revolt = {} controller = ENG } #Wat Tyler killed by Mayor of London
1381.9.1  = { revolt_risk = 0 }
1450.1.1   = { citysize = 6500 }
1453.1.1   = { revolt_risk = 5 } #Start of the War of the Roses
1461.6.1   = { revolt_risk = 2 } #Coronation of Edward IV
1467.1.1   = { revolt_risk = 5 } #Rivalry between Edward IV & Warwick
1471.1.1   = { revolt_risk = 8 } #Unpopularity of Warwick & War with Burgundy
1471.5.4   = { revolt_risk = 2 } #Murder of Henry VI & Restoration of Edward IV
1483.6.26  = { revolt_risk = 8 } #Revulsion at Supposed Murder of the Princes in the Tower
1485.8.23  = { revolt_risk = 0 } #Battle of Bosworth Field & the End of the War of the Roses
1500.1.1   = { marketplace = yes } #Estimated
1520.1.1   = { citysize = 7000 }
1540.1.1   = { religion = episcopalism }
1572.1.1   = { religion = reformed }
1600.1.1   = { citysize = 8000 fort2 = yes }
1642.9.10  = { revolt = { type = revolutionary_rebels size = 0 } controller = REB }
1642.10.24 = { revolt = {} controller = ENG }
1650.1.1   = { citysize = 10000 }
1700.1.1   = { citysize = 12000 }
1707.5.12  = {	owner = GBR
		controller = GBR
		add_core = GBR
	    	remove_core = ENG
	     }
1750.1.1   = { citysize = 15000 tax_assessor = yes } #Tax Assessor Estimated
1800.1.1   = { citysize = 22000 }