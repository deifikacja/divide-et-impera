#348 - Sus

owner = MOR
controller = MOR
culture = berber
religion = sunni
capital = "Tagmadert"
trade_goods = wool
hre = no
base_tax = 2
manpower = 1
citysize = 1017
fort1 = yes
add_core = MOR
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = FZZ

1450.1.1 = { citysize = 1180 }
1500.1.1 = { citysize = 1400 }
1550.1.1 = { citysize = 1910 }
1600.1.1 = { citysize = 2330 }
1604.1.1 = { revolt_risk = 0 }
1650.1.1 = { citysize = 2678 }
1659.1.1 = { revolt_risk = 7 } # The last ruler of Saadi is overthrown
1660.1.1 = { revolt_risk = 3 }
1672.1.1 = { revolt_risk = 4 } # Oppositions against Ismail, & the idea of a unified state
1700.1.1 = { citysize = 3022 }
1727.1.1 = { revolt_risk = 0 }
1750.1.1 = { citysize = 3545 }
1800.1.1 = { citysize = 3800 }