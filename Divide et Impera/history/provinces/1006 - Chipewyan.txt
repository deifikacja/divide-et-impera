#1006 - Chipewyan

culture = northern_athabaskan
religion = shamanism
capital = "Chipewyan"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 5
native_ferocity = 2
native_hostileness = 8

1612.1.1  = { discovered_by = ENG } # Thomas Button
1717.1.1  = {	discovered_by = GBR
		owner = GBR
		controller = GBR
		citysize = 150
		culture = english
	    	religion = episcopalism
	    } # Founding of Churchill
1742.1.1  = { add_core = GBR }
1750.1.1  = { citysize = 550 trade_goods = fur }
1800.1.1  = { citysize = 1400 }
