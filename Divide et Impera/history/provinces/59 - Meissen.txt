#59 - Meissen

owner = MEI
controller = MEI
culture = saxon
religion = catholic
capital = "Meissen"
trade_goods = iron
hre = yes
base_tax = 4
manpower = 1
citysize = 3700
fort1 = yes
add_core = MEI
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1400.1.1  = { citysize = 4100 }
1428.1.4  = { owner = SAX controller = SAX add_core = SAX } # Incorporated into Saxony
1450.1.1  = { citysize = 4500 }
1500.1.1 = { citysize = 4900 }
1531.1.1 = { religion = protestant }
1550.1.1 = { citysize = 5300 }
1589.1.1 = { fort2 = yes } # Current structure of Festung Königstein is built
1600.1.1 = { citysize = 5500 marketplace = yes }
1650.1.1 = { citysize = 6220 }
1700.1.1 = { citysize = 6800 }
1710.1.1 = { trade_goods = luxury_goods } # First European porcelain is manufactured
1750.1.1 = { citysize = 7460 }
1790.8.1 = { revolt_risk = 5 } # Peasant revolt
1791.1.1 = { revolt_risk = 0 }
1800.1.1 = { citysize = 8000 }
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
1815.6.9  = {	owner = PRU
		controller = PRU
		add_core = PRU
		remove_core = SAX
	    } # Congress of Vienna