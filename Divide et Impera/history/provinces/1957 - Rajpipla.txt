#1957 - Rajpipla

owner = DLH
controller = DLH
culture = gujarati
religion = jainism
capital = "Rajpipla"
trade_goods = grain
hre = no
base_tax = 3
manpower = 2
citysize = 13000
add_core = RJP
add_core = DLH
fort1 = yes
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1388.9.20 = { revolt_risk = 4 } #death of Firuz Shah, dynasty crisis began
1389.2.19 = { revolt_risk = 8 } #Tughluq Shah II is murdered in Delhi
1390.8.31 = { revolt_risk = 3 } #Muhammad Shah III enters Delhi
1392.6.1  = { revolt_risk = 9 } #chaos in sultanate
1393.1.1 = { revolt = { type = nationalist_rebels size = 3 } controller = REB }
1396.1.1 = { revolt = {} owner = GUJ controller = GUJ remove_core = DLH revolt_risk = 0 }
1417.1.1 = { controller = MLW }#Malwa invades
1418.1.1 = { controller = GUJ }#Malwa expelled
1429.1.1 = { controller = BAH }#Invasion by the Bahmani sultan
1430.1.1 = { controller = GUJ }#Malwa expelled
1450.1.1 = { citysize = 14000 }
1460.1.1 = {	owner = RJP
		controller = RJP
	   } # Rajpipla founded
1498.1.1 = { discovered_by = POR }
1500.1.1 = { citysize = 15000 }
1550.1.1 = { citysize = 16000 }
1573.6.1 = {	owner = MUG
		controller = MUG
		add_core = MUG
	   } # Conquered by Akbar
1600.1.1 = { citysize = 17000 }
1650.1.1 = { citysize = 18000 }
1680.1.1 = {	owner = RJP
		controller = RJP
	   }#Rajpipla restored
1700.1.1 = { religion = hinduism citysize = 18500 }
1750.1.1 = { citysize = 19000 }
1800.1.1 = { citysize = 20000 }
1815.1.1 = { owner = GAK controller = GAK }#Occupied by Baroda
1820.1.1 = { owner = RJP controller = RJP }
1821.10.1 = { add_core = GBR }#Protectorate