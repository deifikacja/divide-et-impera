#522 - Delhi

owner = DLH
controller = DLH
culture = kanauji
religion = sunni
capital = "Delhi"
trade_goods = tea
hre = no
base_tax = 12
manpower = 5
citysize = 130870
add_core = DLH
fort1 = yes
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = ordu
discovered_by = MUG

1388.9.20 = { revolt_risk = 4 } #death of Firuz Shah, dynasty crisis began
1389.2.19 = { revolt_risk = 8 } #Tughluq Shah II is murdered in Delhi
1390.8.31 = { revolt_risk = 3 } #Muhammad Shah III enters Delhi
1392.6.1  = { revolt_risk = 9 } #chaos in sultanate
1398.6.1  = { owner = TIM controller = TIM add_core = TIM citysize = 97100 }
1413.1.1  = { owner = DLH controller = DLH  remove_core = TIM citysize = 107200 revolt_risk = 0 } #peace is restored
1450.1.1  = { citysize = 129985 }
1500.1.1  = { citysize = 134200 }
1526.1.1  = {	owner = MUG
		controller = MUG
		add_core = MUG
		remove_core = DLH
	    } # Battle of Panipat
1540.1.1  = { owner = SRI controller = SRI add_core = SRI } # Sher Shah takes over
1550.1.1  = { citysize = 137210 }
1556.11.5  = {	owner = MUG
		controller = MUG
		remove_core = SRI
	   } # Sher Shah Suri's death, the Bengali empire crumbeled
1555.1.1  = { religion = sufism } # Akbar's eclectic religion policy
1600.1.1  = { citysize = 42128 }
1638.1.1  = { fort2 = yes } # Delhi became the capital & grew in importance
1640.1.1  = { marketplace = yes }
1650.1.1  = { citysize = 72585 }
1660.1.1  = { courthouse = yes religion = sunni } # Aurangzeb's nontoleration policy
1700.1.1  = { citysize = 80706 }
1722.1.1  = { religion = hinduism } # Mughal Empire colapse, hindusim return to Delhi
1750.1.1  = { citysize = 85760 }
1770.1.1  = {	owner = MAR
		controller = MAR
		add_core = MAR
		remove_core = MUG
	    } # The Marathan Empire
1800.1.1  = { citysize = 90540 }
1803.11.11  = {	owner = MUG
		controller = MUG
		add_core = MUG
	    } # British forces defeat the Marathas and install puppet ruler