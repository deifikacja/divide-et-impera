#130 - Istria

owner = VEN
controller = VEN
culture = croatian
religion = catholic
capital = "Pula"
trade_goods = wool
hre = no
base_tax = 5
manpower = 1
citysize = 5000
fort1 = yes
add_core = VEN
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1420.1.1  = { remove_core = AQU } # Whole peninsula to Venice
1450.1.1  = { citysize = 5400 }
1500.1.1 = { citysize = 6000 }
1550.1.1 = { citysize = 6988 fort2 = yes } # The fort is rebuilt
1600.1.1 = { citysize = 7560 }
1650.1.1 = { citysize = 8900 marketplace = yes }
1700.1.1 = { citysize = 9200 }
1750.1.1 = { citysize = 10200 }
1797.10.17 = {	owner = HAB
		controller = HAB
		add_core = HAB
	     } # Treaty of Campo Formio
1800.1.1   = { citysize = 11700 }
1805.12.26 = {	owner = RFR
		controller = RFR
		add_core = RFR
		remove_core = HAB
	     } # Treaty of Pressburg
1813.9.20 = { controller = HAB } # Occupied by Austrian forces
1814.4.6  = {	owner = HAB
		add_core = HAB
		remove_core = RFR
	    } # Napoleon abdicates
