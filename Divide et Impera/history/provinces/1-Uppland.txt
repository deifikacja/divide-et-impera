#Uppland, contains Stockholm, Uppsala & Nyk�ping.

add_core = SWE
owner = SWE
controller = SWE
culture = swedish
religion = catholic
hre = no
base_tax = 6
trade_goods = grain
manpower = 3
fort1 = yes
capital = "Stockholm"
citysize = 4500
temple = yes #"Domkyrkan i Uppsala"
workshop = yes #"Stockholms borgarskap"
marketplace = yes
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 


1364.2.18 = { revolt = { type = pretender_rebels size = 2 leader = "Magnus IV Eriksson" } }
1371.1.1 = { revolt = {} controller = SWE }#Civil war settled
1436.4.27  = { revolt = { type = nationalist_rebels size = 2 } controller = REB } # Engelbrekt and Karl marsh on Stockholm
1448.6.20  = { revolt = {} controller = SWE } # Karl VIII is elected king of Sweden
# 1457.2.24  = { owner = DAN controller = DAN } # Karl VIII exiled and Kristian I is elected king
1460.1.1   = { citysize = 6000 }
# 1464.9.1   = { owner = SWE controller = SWE } # Kristian is driven out of Sweden and Karl is reinstated
1477.1.1   = { university = yes } #Uppsala University
1500.1.1   = { citysize = 7000 }
1501.8.1   = { controller = DAN } #Danish loyalists at Swedish DoW and breaking of vassalage
1502.5.9   = { controller = SWE } #Retaken by Sweden
# 1520.9.2   = { owner = DAN controller = DAN } #Christian II conquers Stockholm
1523.1.1   = { citysize = 8000 }
1523.6.7   = { regimental_camp = yes } #Kgl.Drabantk�ren/Svea Livgarde
# 1523.6.21  = { owner = SWE controller = SWE } #Gustav Eriksson Wasa enters on midsummer day
1527.6.1   = { religion = protestant}
1537.1.1   = { fort2 = yes } #Gripsholms Castle
1550.1.1   = { base_tax = 7 manpower = 4 }
1582.1.1   = { citysize = 9000 }
1598.8.12  = { controller = POL } #Sigismund tries to reconquer his crown
1598.12.15 = { controller = SWE } #Duke Karl get it back
1600.1.1   = { manpower = 5 }
1608.1.1   = { constable = yes } #The men around Axel Oxenstierna
1614.1.1   = { courthouse = yes } #Svea Hovr�tt
1617.1.1   = { cot = yes } #Stockholm commanded the Baltic Sea trade
1617.1.1   = { base_tax = 8 } #Stockholm become Baltic Metropol
1621.1.1   = { fort3 = yes } # Key forts defending roads to Stockholm
1630.1.1   = { citysize = 20000 }
1634.1.1   = { shipyard = yes } #Skeppsholmen issued as naval base by GIIA
1650.1.1   = { citysize = 30000 }
1654.1.1   = { tax_assessor = yes } #Result of "The Great Reduction"
1680.1.1   = { fort4 = yes } # Added forts giving coastal defense of region
1685.1.1   = { citysize = 60000 }
1700.1.1   = { citysize = 40000 }
1704.1.1   = { war_college = yes } #estimated date
1710.1.1   = { customs_house = yes } #Baron v. G�rtz new institution
1730.1.1   = { fort5 = yes } # estimated date
1750.1.1   = { fort6 = yes citysize = 58400 } # estimated date
1800.1.1   = { citysize = 75800 }
