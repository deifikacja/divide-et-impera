#1333 - Abu Arish

owner = ADE
controller = ADE
culture = yemeni_arabic
religion = sunni
capital = "Najran"
trade_goods = grain
hre = no
base_tax = 1
manpower = 1
citysize = 2420
add_core = ADE
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = TUR

1450.1.1 = { citysize = 2600 }
1500.1.1 = { citysize = 2980 }
1550.1.1 = { citysize = 3410 }
1600.1.1 = { citysize = 3870 }
1627.1.1 = {	owner = TUR
		controller = TUR
	   }
1650.1.1 = { citysize = 5320 }
1700.1.1 = { citysize = 6015 }
1750.1.1 = { citysize = 6600 }
1752.1.1 = { religion = wahhabism }
1800.1.1 = { citysize = 7210 }
1803.1.1 = {	owner = NAJ
		controller = NAJ
		add_core = NAJ
	   }
1830.1.1 = {	owner = EGY
		controller = EGY
	   }