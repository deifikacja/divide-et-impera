#140 - Bosnia

owner = BOS # Part of the Bosnian kingdom
controller = BOS
culture = bosnian
religion = greek_orthodox
capital = "Sarajevo"
trade_goods = cattle
hre = no
base_tax = 4
manpower = 4
citysize = 4800
fort1 = yes
add_core = BOS
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1366.1.1  = { revolt_risk = 8 } #aristocracy rebel
1367.1.1  = { revolt_risk = 0 }
1389.6.15 = { add_core = TUR } #Battle of Kosovo
1391.1.1  = { revolt_risk = 4 } #weak rulers
1421.1.1  = { revolt_risk = 0 }
1432.1.1  = { revolt_risk = 8 } #aristocracy rebel
1433.1.1  = { revolt_risk = 0 }
1450.1.1  = { citysize = 5300 }
1463.1.1  = {	owner = TUR
		controller = TUR
	    } # The Ottoman province of Bosnia
1500.1.1  = { citysize = 7095 }
1550.1.1  = { citysize = 9009 }
1593.1.1  = { revolt_risk = 3 } # Fighting began in northwestern Bosnia, sparked Habsburg-Ottoman conflict
1600.1.1  = { citysize = 12061 constable = yes }
1606.1.1  = { revolt_risk = 0 } # Temporarty peace
1650.1.1  = { citysize = 16270 }
1670.1.1  = { marketplace = yes }
1683.1.1  = { revolt_risk = 6 } # Heavy fighting & destruction in western Bosnia
1699.1.1  = { revolt_risk = 0 citysize = 18900 } # Flood of Muslim refugees from Slavonia & Ottoman Hungary 
1700.1.1  = { citysize = 19660 }
1716.1.1  = { controller = HAB } # Occupied by Habsburg
1718.1.1  = { controller = TUR } # Estimated
1730.1.1  = { tax_assessor = yes }
1737.7.1  = { controller = HAB } # Occupied by Habsburg again
1738.1.1  = { revolt_risk = 5 } # The constant fighting, increased taxation caused tax revolts
1739.9.18 = { controller = TUR } # Treaty of Belgrade, Habsburg gave up its claim to the territory
1740.1.1  = { revolt_risk = 8 }
1750.1.1  = { citysize = 21325 revolt_risk = 0 }
1788.12.6 = { controller = HAB } # Habsburg invasion
1791.8.4  = { controller = TUR } # Treaty of Sistova
1800.1.1  = { citysize = 30098 }
1804.1.1  = { revolt_risk = 4 } # Serb rebellion
1817.1.1  = { revolt_risk = 0 }