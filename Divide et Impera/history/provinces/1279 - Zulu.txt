#1279 - Zulu

culture = bantu #Tsonga
religion = animism
capital = "Eshowe"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 70
native_ferocity = 4.5
native_hostileness = 9 

1498.1.6 = { discovered_by = POR } #Vasco Da Gama
1700.1.1 = { owner = ZUL controller = ZUL citysize = 1200 }#Mthethwa Alliance
1818.1.1 = { revolt = { type = pretender_rebels size = 1 leader = "Zwide" } capital = "KwaBulawayo" citysize = 2200 trade_goods = cattle }#Zulu is dominant
1820.1.1 = { revolt = { } }