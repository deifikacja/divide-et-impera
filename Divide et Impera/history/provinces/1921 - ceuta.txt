#1921 - Ceuta

owner = MOR
controller = MOR  
culture = berber
religion = sunni
capital = "Cebta"
trade_goods = fish
hre = no
base_tax = 4
manpower = 1
citysize = 2000
fort1 = yes
add_core = MOR
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ

1384.1.1 = { owner = GRA controller = GRA }
1387.1.1 = { owner = MOR controller = MOR }
1415.8.21   = { controller = POR add_core = POR }
1415.9.1 = { owner = POR capital = Ceuta }
1500.1.1   = { citysize = 3500 religion = catholic }
1640.1.1   = {	owner = SPA
		controller = SPA
		add_core = SPA
		remove_core = POR
	     } # Ceuta decides to become Spanish and not Portuguese
1810.1.1   = { citysize = 4000 }
