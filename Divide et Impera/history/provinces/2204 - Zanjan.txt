#2204 - Zanjan

owner = JAL
controller = JAL
culture = persian
religion = shiite
capital = "Qazvin"
trade_goods = luxury_goods
hre = no
base_tax = 6
manpower = 2
citysize = 17788
add_core = JAL
add_core = TAB
fort1 = yes
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = BYZ
discovered_by = TIM
discovered_by = SHY
discovered_by = MUG

1356.1.1 = { add_core = QAR }
1357.1.1 = { controller = GOL } #Captured by the Golden Horde
1357.5.1 = {  controller = JAL }
1380.1.1 = { owner = TIM controller = TIM add_core = TIM remove_core = JAL }
1408.1.1  = { controller = QAR owner = QAR remove_core = TIM }
1450.1.1   = { citysize = 18535 }
1458.9.1   = { revolt = { type = revolutionary_rebels size = 4 } controller = REB } # Civil war in Qara Quyunlu
1458.12.1  = { revolt = {} controller = QAR }
1469.1.1   = {	owner = AKK
		controller = AKK
		add_core = AKK
		remove_core = QAR
	     } # The Ak Koyunlu expands their territory
1500.1.1   = { citysize = 20760 }
1501.1.1   = {	owner = PER
		controller = PER
		add_core = PER
		remove_core = AKK
                 religion = shiite
	     } # The Safavids took over
1550.1.1   = { citysize = 34280 }
1600.1.1   = { citysize = 31555 }
1650.1.1   = { citysize = 29520 constable = yes }
1700.1.1   = { citysize = 32341 }
1720.1.1   = { tax_assessor = yes }
1721.1.1   = { citysize = 25234 }#Earthquake
1724.1.1  = { controller = TUR }
1725.1.1  = { controller = PER }
1746.1.1   = { revolt_risk = 4 } # Shah Nadir is killed, local khanates emerged
1747.1.1   = { owner = TAB controller = TAB } # The empire began to decline
1750.1.1   = { citysize = 27100 }
1779.1.1   = { revolt_risk = 0 } # With the Qajar dynasty the situation stabilized
1780.1.1   = { citysize = 22100 }#Another earthquake
1796.6.15  = { controller = RUS } # Overran by Russian troops
1796.11.1  = { controller = TAB }
1800.1.1   = { citysize = 24630 }
1816.1.1 = {	owner = PER
		controller = PER
	     } 
1826.1.1  = { controller = RUS }