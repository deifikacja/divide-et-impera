#1321 - Mari

owner = GOL
controller = GOL 
capital = "Yoshkar-Ola"
culture = volgaic
religion = shamanism
trade_goods = fish
hre = no
base_tax = 2
manpower = 1
citysize = 1450
add_core = KAZ
add_core = GOL	
discovered_by = ordu
discovered_by = KAZ
discovered_by = CRI
discovered_by = GOL
discovered_by = SIB
discovered_by = NOG
discovered_by = eastern 
discovered_by = QAS 
discovered_by = AST
discovered_by = PEL

1438.1.1 = { owner = KAZ controller = KAZ remove_core = GOL fort1 = yes }
1450.1.1 = { citysize = 1620 }
1500.1.1 = { citysize = 2250 }
1550.1.1 = { citysize = 3160 }
1552.1.1 = {	owner = RUS
		controller = RUS
		add_core = RUS
		remove_core = KAZ
	   } # Conquered by Muscovy
1553.1.1 = { religion = orthodox base_tax = 1 manpower = 1 } # Most Tatars were christianized or killed
1600.1.1 = { citysize = 2623 }
1650.1.1 = { citysize = 3210 }
1700.1.1 = { citysize = 3501 }
1750.1.1 = { citysize = 4120 }
1773.1.1 = { revolt_risk = 5 } # Peasant uprising, Pugachev
1774.1.1 = { revolt_risk = 0 }
1800.1.1 = { citysize = 4320 }
