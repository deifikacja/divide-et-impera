#206 - Galicia

owner = CAS		#Juan II of Castille
controller = CAS
add_core = CAS
add_core = LEO
culture = galician
religion = catholic
hre = no
base_tax = 4
trade_goods = fish
manpower = 2
fort1 = yes
capital = "A Corunha" 
citysize = 4000
temple = yes # Santiago de Compostela pilgrimage
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1356.1.1   = { revolt_risk = 3 } # civil war between Pedro the Cruel and Enrique II de Trast�mara
1367.4.3   = { revolt_risk = 5 } #battle of N�jera
1369.3.23  = { owner = POR controller = POR } # Pedro the Cruel assasinated, Enrique II de Trast�mara now is the one king of the Castille
1371.1.1   = { owner = CAS controller = CAS }
1375.1.1   = { revolt_risk = 0 } # peace is fully restored, England & Portugal recognized Enrique's rights
1386.1.1   = { controller = ENG } # Occupied by English forces
1388.1.1   = { controller = CAS }
1450.1.1  = { citysize = 5000 }
1467.1.1  = { revolt_risk = 4 } # Second war of the "irmandi�os"
1470.1.1  = { revolt_risk = 0 } # End of the Second war of the "irmandi�os"
1475.6.2  = { controller = POR }
1476.3.2  = { controller = CAS }
1500.1.1  = { citysize = 6800 }
1516.1.23 = {	controller = SPA
		owner = SPA
		add_core = SPA
	    } # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castilla
1550.1.1  = { citysize = 7105 marketplace = yes }
1600.1.1  = { citysize = 8560 textile = yes }
1650.1.1  = { citysize = 10776 }
1700.1.1  = { citysize = 14350 }
1713.4.11 = { remove_core = CAS }
1750.1.1  = { citysize = 19870 }
1800.1.1  = { citysize = 22700 }
1808.6.6   = { revolt = { type = nationalist_rebels size = 2 } controller = REB }
1809.1.1   = { revolt = {} controller = SPA }
1812.10.1  = { revolt = { type = nationalist_rebels size = 2 } controller = REB }
1813.12.11 = { revolt = {} controller = SPA }