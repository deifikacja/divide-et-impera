# 75 Elsass - Principal cities: Strasbourg

owner = ALS
controller = ALS 
capital = "Stra�burg"
citysize = 14000
culture = swabian
religion = catholic
hre = yes
base_tax = 4
trade_goods = grain
manpower = 1
add_core = ALS
fort1 = yes
marketplace = yes # Trade Center
temple = yes # La Notre Dame de Strasbourg (finished 1439)
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1450.1.1   = { citysize = 16000 }
1467.6.15  = { add_core = BUR } # Charles the Bold ascends and lays claims on the Alsace
1469.1.1   = { owner = BUR controller = BUR } # Upper Alsace given to Charles the Bold by Archduke Sigismund
1477.1.5   = { remove_core = BUR owner = ALS controller = ALS  } # Charles the Bold dies, Alsace re-established
1500.1.1   = { citysize = 20000 fort2 = yes }
1525.1.1   = { religion = protestant revolt_risk = 5 } # Unrest in the Alsace
1525.5.1   = { revolt_risk = 15 } # Heavy unrests now: Peasant's War about to start
1525.5.14  = { revolt_risk = 0 revolt = { type = anti_tax_rebels size = 2 } controller = REB } # Rebels take control of Strasbourg in the Peasant's War
1525.6.15  = { revolt = {} controller = ALS revolt_risk = 10 } # Duke Antoine of Lorraine takes back the land and punishes the peasant's brutally
1525.6.23  = { revolt_risk = 15 } # Duke Antoine executes a few leaders of the rebellion: heavy unrest again
1525.9.18  = { revolt_risk = 5 } # The Offenburg Agreement: Peace is being restored
1526.1.1   = { revolt_risk = 0 } # Peace restored everywhere
1550.1.1   = { citysize = 24000 }
1580.1.1   = { constable = yes }
1600.1.1   = { citysize = 28000 courthouse = yes }
1621.1.1   = { university = yes } # Acad�mie de Strasbourg elevated to University
1625.1.1   = { fort3 = yes }
1650.1.1   = { citysize = 23000 }
1660.1.1   = { fort4 = yes }
1670.1.1   = { add_core = FRA } # Louis XIV lays claims through the Chambres de R�union
1674.1.1   = { fort5 = yes } # Vauban's forts in Strasbourg & Haguenau
1681.1.1   = { owner = FRA controller = FRA hre = no } # Louis XIV annexes Strasbourg 
1685.10.18 = { revolt_risk = 8 } # Edict of Nantes revoked by Louis XIV
1686.1.17  = { religion = catholic revolt_risk = 0 } # Dragonnard campaign succesful: region reverts back to Catholicism
1700.1.1   = { citysize = 30000 customs_house = yes tax_assessor = yes }
1730.1.1   = { fort6 = yes } # Cormontaigne's forts
1749.1.1   = { base_tax = 8 } # Machault & the 5% tax
1750.1.1   = { citysize = 40000 regimental_camp = yes }
1760.1.1   = { workshop = yes }
1789.7.14  = {	owner = RFR
		controller = RFR
		add_core = RFR
		remove_core = FRA
	     } # The storming of the Bastille
1800.1.1   = { citysize = 48000 }
1814.4.11  = {	owner = FRA
		controller = FRA
		add_core = FRA
		remove_core = RFR
	     } # Treaty of Fontainebleu, Napoleon abdicates unconditionally
1815.3.20  = {	owner = RFR
		controller = RFR
		add_core = RFR
		remove_core = FRA
	     } # Napoleon enters Paris
1815.7.8   = {	owner = FRA
		controller = FRA
		add_core = FRA
		remove_core = RFR
	     } # The French monarchy is restored
