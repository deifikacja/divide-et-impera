#554 - Gorka

owner = NPL
controller = NPL
culture = nepali
religion = hinduism
capital = "Gorka"
trade_goods = grain
hre = no
base_tax = 3
manpower = 1
citysize = 5387
add_core = NPL
add_core = GUR
fort1 = yes
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = NPL
discovered_by = TIB
discovered_by = MUG

1450.1.1 = { citysize = 6100 }
1484.1.1 = { owner = GUR controller = GUR }
1500.1.1 = { citysize = 7620 }
1550.1.1 = { citysize = 8544 }
1600.1.1 = { citysize = 10733 }
1650.1.1 = { citysize = 12087 }
1700.1.1 = { citysize = 15335 }
1750.1.1 = { citysize = 17800 }
1768.8.25 = { owner = NPL controller = NPL }#Reunited
1800.1.1 = { citysize = 21400 }