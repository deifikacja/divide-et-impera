#397 - Beni Yas

owner = SUH
controller = SUH
culture = omani_arabic
religion = shiite
capital = "Abu Dhabi"
trade_goods = wool
hre = no
base_tax = 1
manpower = 1
citysize = 1322
add_core = SUH
add_core = OMA
add_core = SHR
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1450.1.1  = { citysize = 1750 }
1480.1.1  = { discovered_by = TUR }
1488.1.1  = { discovered_by = POR } # P�ro da Covilh�
1488.1.1  = {	owner = NZW
		controller = NZW
	    } # Captured by Nizwa
1500.1.1  = { citysize = 1750 discovered_by = TUR }
1550.1.1  = { citysize = 2008 }
1560.1.1  = {	owner = SUH
		controller = SUH
	    } # Re-established
1600.1.1  = { citysize = 2247 }
1624.1.1  = {	owner = OMA
		controller = OMA
		remove_core = POR
	    } # Rule restored
1650.1.1  = { citysize = 2867 }
1679.1.1  = { revolt_risk = 5 } # Internal unrest upon Imam's death
1700.1.1  = { citysize = 3410 religion = wahhabism }
1750.1.1 = { citysize = 2788 }
1761.1.1 = {	revolt_risk = 0 
		citysize = 5218
		owner = ABU
		controller = ABU
	   } # Abu Dhabi founded
1800.1.1 = { citysize = 6300 }