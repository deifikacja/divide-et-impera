#875 - Canyon

owner = ANA
controller = ANA
add_core = ANA
culture = pueblo
religion = animism 
capital = "Canyon" 
trade_goods = unknown
hre = no 
base_tax = 1 
manpower = 1 
citysize = 1000

discovered_by = ANA


discovered_by = APA
discovered_by = PUB
discovered_by = NAO

1500.1.1 = { owner = XXX controller = XXX remove_core = ANA culture = numic
religion = shamanism }
1540.1.1   = { discovered_by = SPA } # Francisco V�squez de Coronado
1731.1.1   = {	owner = SPA
		controller = SPA
		citysize = 240
		religion = catholic
		culture = castillian
		trade_goods = salt 
	     } # First permanent Spanish settlers
1750.1.1   = { citysize = 500 }
1756.1.1   = { add_core = SPA add_core = MEX citysize = 1144 }
1800.1.1   = { citysize = 2000 }
1810.9.16  = {	owner = MEX
		controller = SPA
	     } # Mexican War of Independence
