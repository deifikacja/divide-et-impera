#2097 - Shoshoni

culture = numic
religion = shamanism
capital = "Shoshoni"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 25
native_ferocity = 2
native_hostileness = 8
discovered_by = ARP
discovered_by = CHY

1805.8.12 = { discovered_by = USA }#Lewis and Clark. Not settled until 1867