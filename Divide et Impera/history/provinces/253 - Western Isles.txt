#253 - Western Isles

owner = SCO
controller = SCO
culture = highland_scottish
religion = catholic
hre = no
base_tax = 1
trade_goods = fish
manpower = 2
capital = "Stornoway"
citysize = 1000 #Estimated
add_core = SCO
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
fort1 = yes

1450.1.1  = { citysize = 1400 }
1500.1.1  = { citysize = 1500 }
1580.8.1  = { religion = reformed }
1600.1.1  = { citysize = 2000 }
1700.1.1  = { citysize = 3000 }
1707.5.12 = {	owner = GBR
		controller = GBR
		add_core = GBR
	    }
1750.1.1  = { fort2 = yes citysize = 3500 }
1800.1.1  = { citysize = 4200 }