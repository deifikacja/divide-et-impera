#1061 - Yeniseysk

culture = tunguz
religion = shamanism
capital = "Yeniseysk"
trade_goods = unknown
hre = no
base_tax = 2
manpower = 1
native_size = 10
native_ferocity = 1
native_hostileness = 3

1619.1.1 = {	discovered_by = RUS
		owner = RUS
		controller = RUS
	   	culture = russian
	   	religion = orthodox
		citysize = 250
		trade_goods = gold
	   }
1643.1.1 = {	add_core = RUS
	   	citysize = 1520
	   }
1650.1.1 = { citysize = 2520 }
1700.1.1 = { citysize = 3522 }
1750.1.1 = { citysize = 4070 }
1800.1.1 = { citysize = 4800 }