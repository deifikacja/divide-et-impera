#931 - Chiaha

owner = ETO
controller = ETO
culture = mississippian
religion = animism
capital = "Coosa"
trade_goods = fur
hre = no
base_tax = 1
manpower = 1
citysize = 1000
add_core = ETO
add_core = CHE
discovered_by = PLA

discovered_by = QUA
discovered_by = ONE
discovered_by = CAD
discovered_by = CAH
discovered_by = ETO
discovered_by = CHE
discovered_by = CRE
discovered_by = SHA
discovered_by = ALM
discovered_by = CTW
discovered_by = CRW

1450.1.1  = { citysize = 1200 }
1500.1.1  = { citysize = 1550 }
1540.1.1  = { discovered_by = SPA } # Hernando de Soto
1550.1.1  = { citysize = 2000 owner = CHE controller = CHE remove_core = ETO culture = cherokee
religion = shamanism }
1600.1.1  = { citysize = 2564 }
1650.1.1  = { citysize = 2980 }
1700.1.1  = { citysize = 3500 }
1763.2.10 = {	owner = GBR
		controller = GBR
		culture = english
		religion = episcopalism
	    }
1750.1.1  = { citysize = 4100 }
1764.7.1  = { culture = american revolt_risk = 6 } # Growing unrest
1776.7.4  = {	owner = USA
		controller = USA
		add_core = USA
	    } # Declaration of independence
1782.11.1 = { revolt_risk = 0 } # Preliminary articles of peace, the British recognized Amercian independence
1800.1.1  = { citysize = 4800 }