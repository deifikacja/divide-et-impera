#2001 - Gyeonggi

owner = KOR
controller = KOR
culture = korean
religion = buddhism
capital = "Seoul"
trade_goods = grain
hre = no
base_tax = 5
manpower = 4
citysize = 22100
add_core = KOR
fort1 = yes
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = ordu

1392.1.1 = { religion = confucianism }
1500.1.1  = { citysize = 36570 }
1540.1.1  = { marketplace = yes }
1550.1.1  = { citysize = 38400 }
1592.4.28 = { controller = TOY } # Japanese invasion
1593.5.18 = { controller = KOR } # With Chinese help the Japanese are driven back
1600.1.1  = { citysize = 31083 } # Great devastation as a result of the Japanese invasion
1637.1.1  = { add_core = MNG } # Tributary of Qing China
1640.1.1  = { constable = yes }
1644.1.1  = { add_core = MCH remove_core = MNG } # Part of the Manchu empire
1650.1.1  = { citysize = 33600 }
1653.1.1  = { discovered_by = NED } # Hendrick Hamel
1683.1.1 = { add_core = QNG remove_core = MCH } # The Qing Dynasty
1700.1.1  = { citysize = 38599 }
1750.1.1  = { citysize = 44580 }
1755.1.1  = { tax_assessor = yes }
1800.1.1  = { citysize = 52450 }
