#632 - Sumbawa

owner = TMB
controller = TMB
add_core = TMB
culture = malay-polynesian
religion = animism
capital = "Tambora"
trade_goods = coffee
hre = no
base_tax = 2
manpower = 1
citysize = 1230
discovered_by = TMB

1400.1.1 = { citysize = 1610 }
1500.1.1 = { citysize = 2450 }
1522.1.1 = { discovered_by = POR }
1600.1.1 = { citysize = 3800 }
1620.1.1 = { religion = sunni }
1668.1.1 = { add_core = NED } #Dutch sphere of influence
1815.1.1 = { citysize = 130 owner = NED controller = NED remove_core = TMB } #Destroyed by the volcano