#H�lsingland-G�strikland-Medelpad, from G�vle to N�s

add_core = SWE
owner = SWE
controller = SWE
culture = swedish
religion = catholic
hre = no
base_tax = 3
trade_goods = fish
manpower = 1
fort1 = yes
capital = "G�vle"
citysize = 1100 # Estimated
marketplace = yes # controller of trade in Norrland
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western

1356.1.1 = { revolt = { type = pretender_rebels size = 0 } controller = REB }#Erik Magnusson
1359.6.21 = { revolt = {} controller = SWE }#Death of Erik Magnusson
1400.1.1  = { citysize = 1200 }
1450.1.1  = { citysize = 1300 }
# 1520.3.6  = { owner = DAN controller = DAN } #The Council accept Christian II as King
# 1521.4.30 = { owner = SWE controller = SWE } #The population joins the the uprising
1527.6.1  = { religion = protestant}
1560.1.1  = { fort2 = yes } #
1570.1.1  = { citysize = 1524 }
1610.1.1  = { citysize = 1930 }
1634.1.1  = { regimental_camp = yes } #H�lsinge regemente
1635.1.1  = { courthouse = yes } #minor court belonging to Svea Hovr�tt
1640.1.1  = { fort3 = yes } #
1650.1.1  = { citysize = 2508 }
1670.1.1  = { tax_assessor = yes } #Due to the "Staplepolicy act"
1690.1.1  = { citysize = 2803 }
1730.1.1  = { citysize = 3741 }
1730.1.1  = { constable = yes } #Due to the support of manufactories
1740.1.1  = { fort4 = yes } #
1770.1.1  = { citysize = 4014 }
1792.1.1  = { fort5 = yes } #
1800.1.1  = { citysize = 5558 }

