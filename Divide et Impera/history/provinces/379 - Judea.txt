#379 - Judea

owner = MAM
controller = MAM
culture = al_suryah_arabic
religion = sunni
capital = "Jerusalem"
trade_goods = grain
hre = no
base_tax = 3
manpower = 2
citysize = 4880
fort1 = yes
temple = yes
add_core = MAM
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = ordu

1450.1.1 = { citysize = 5100 }
1500.1.1 = { citysize = 5500 }
1517.1.1 = {	owner = TUR
		controller = TUR
		add_core = TUR
		fort2 = yes
	   } # Part of the Ottoman empire
1550.1.1 = { citysize = 6342 }
1600.1.1 = { citysize = 8642 }
1650.1.1 = { citysize = 10431 }
1700.1.1 = { citysize = 12999 }
1750.1.1 = {	citysize = 14383
		tax_assessor = yes
	   	add_core = SYR
	   }
1800.1.1 = { citysize = 17420 }
