#530 - Konkan

owner = BAH
controller = BAH
culture = marathi
religion = hinduism
capital = "Kolhapur"
trade_goods = spices
hre = no
base_tax = 4
manpower = 1
citysize = 10150
add_core = BAH
add_core = BIJ 
fort1 = yes
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = MUG

1450.1.1 = { citysize = 11630 }
1498.1.1 = { discovered_by = POR }
1500.1.1 = { citysize = 12900 }
1518.1.1 = {	owner = BIJ
		controller = BIJ
		remove_core = BAH
	   } # The Breakup of the Bahmani sultanate
1550.1.1 = { citysize = 14300 }
1600.1.1 = { citysize = 15348 }
1650.1.1 = { citysize = 17100 }
1674.1.1 = { add_core = MAR } # Maratha Empire
1686.1.1 = {	owner = MUG
		controller = MUG
		add_core = MUG
	   } # Conquered by the Mughal emperor Aurangzeb
1700.1.1 = { citysize = 18700 }
1710.1.1 = {	owner = KLH
		controller = KLH
		add_core = KLH
		remove_core = MUG
	   } # Kolhapur Bhonsle state is founded
1750.1.1 = { citysize = 22700 }
1800.1.1 = { citysize = 24100 }
1818.6.3 = {	owner = GBR
		controller = GBR
		add_core = GBR
	   }