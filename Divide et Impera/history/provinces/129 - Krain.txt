# 129 - Krain
owner = AQU
controller = AQU
add_core = AQU
culture = slovenian
religion = catholic
base_tax = 4
trade_goods = naval_supplies
manpower = 2
fort1 = yes
capital = "Laibach"
citysize = 3000
hre = yes
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1420.1.1   = {	owner = STY
		controller = STY
		add_core = STY
		remove_core = AQU
	     } # To the Habsburgs
1450.1.1   = { citysize = 3500 }
1457.11.24 = {	owner = HAB
		controller = HAB
		add_core = HAB
		remove_core = STY
	     }
1525.5.1   = { revolt_risk = 5 } # Farmer insurrections 
1526.1.1   = { revolt_risk = 0 }
1550.1.1   = { citysize = 4000 }
1578.1.1   = { fort2 = yes }
1600.1.1   = { citysize = 6000 }
1650.1.1   = { citysize = 10000 }
1660.1.1   = { textile = yes }
1679.1.1   = { citysize = 8000 } # great plague
1700.1.1   = { citysize = 9000 marketplace = yes }
1750.1.1   = { citysize = 13000 workshop = yes }
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1809.10.14 = {	owner = RFR
		controller = RFR
		add_core = RFR
		remove_core = HAB
	     } # Treaty of Schönbrunn
1813.9.20 = { controller = HAB } # Occupied by Austrian forces
1814.4.6  = {	owner = HAB
		add_core = HAB
		remove_core = RFR
	    } # Napoleon abdicates