#230 - Algarve

owner = POR
controller = POR
culture = portugese
religion = catholic
capital = "Faro"
trade_goods = naval_supplies
hre = no
base_tax = 4
manpower = 1
citysize = 3500 
add_core = POR
fort1 = yes
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ


1450.1.1  = { citysize = 4500 }
1500.1.1  = { citysize = 5477 marketplace = yes }
1550.1.1  = { citysize = 6000 }
1600.1.1  = { citysize = 6850 }
1640.1.1  = { revolt_risk = 8 } # Revolt headed by John of Bragan�a
1640.12.1 = { revolt_risk = 0 }
1650.1.1  = { citysize = 7900 }
1700.1.1  = { citysize = 7000 }
1750.1.1  = { citysize = 8100 }
1800.1.1  = { citysize = 9440 }
1807.11.30 = { controller = SPA } # Occupied by Spanish troops led by Manuel Godoy
1808.6.18  = { controller = POR } # Revolt in Olh�o