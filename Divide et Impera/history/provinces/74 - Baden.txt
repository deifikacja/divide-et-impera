#74 - Baden

owner = BAD
controller  = BAD
add_core = BAD
capital = "Karlsruhe"
trade_goods = wine
religion = catholic
culture = swabian
base_tax = 3
manpower = 1
citysize = 4000
hre = yes
fort1 = yes
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1400.1.1  = { citysize = 4500 }
1450.1.1  = { citysize = 5000 }
1500.1.1  = { marketplace = yes citysize = 5600 }
1538.1.1  = { religion = protestant } # Protestant majority
1550.1.1  = { citysize = 600 }
1600.1.1  = { citysize = 7000 constable = yes }
1650.1.1  = { religion = catholic citysize = 6500 }
1700.1.1  = { citysize = 7000 customs_house = yes }
1750.1.1  = { regimental_camp = yes tax_assessor = yes citysize = 8700 }
1771.1.1  = { base_tax = 4 manpower = 2 citysize = 12000 } # Baden-Baden Line dies without heirs, thus the possessions of the Baden family are reunited in one hand (Baden-Durlach).
1792.10.3 = { controller = RFR } # Occupied by French troops
1796.8.7  = { controller = BAD }
1800.1.1  = { citysize = 15000 }
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
