#134 - Wien

owner = HAB
controller = HAB
add_core = HAB
culture = austrian
religion = catholic
base_tax = 6
trade_goods = wine 
manpower = 2
fort1 = yes
fort2 = yes
capital = "Wien" 
citysize = 15000
marketplace = yes
hre = yes
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = ordu

1400.1.1 = { citysize = 17000 }
1450.1.1 = { citysize = 20000 }
1469.1.1 = { temple = yes } # Stephansdom becomes Cathedral
1477.1.1 = {	owner = HUN
		controller = HUN
		add_core = HUN
	   } #Matthias Corvinus
1490.4.26 = {	owner = HAB
		controller = HAB
		remove_core = HUN
	   } # Matthias Corvinus dies in Vienna
1500.1.1 = { regimental_camp = yes }
1500.1.1 = { citysize = 26000 }
1502.1.1 = { university = yes }
1526.1.1 = { courthouse = yes base_tax = 8 }
1535.1.1 = { fort3 = yes }
1540.1.1 = { citysize = 40000 constable = yes }
1545.1.1 = { manpower = 3 }
1550.1.1 = { marketplace = yes }
1599.1.1 = { cot = yes }
1600.1.1 = { citysize = 58000 }
1620.1.1 = { tax_assessor = yes }
1650.1.1 = { fort4 = yes }
1650.1.1 = { base_tax = 10 }
1650.1.1 = { citysize = 70000 }
1700.1.1 = { citysize = 130000 }
1745.1.1 = { war_college = yes }
1750.1.1 = { citysize = 175400 base_tax = 12 }
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
