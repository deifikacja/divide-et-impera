#510 - Chandigarh

owner = BIL
controller = BIL
culture = panjabi
religion = hinduism
capital = "Kahlur"
trade_goods = cloth
hre = no
base_tax = 3
manpower = 2
citysize = 5250
add_core = BIL
fort1 = yes
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = MUG

1450.1.1 = { citysize = 5630 }
1529.1.1 = { citysize = 7000 religion = sikhism }
1550.1.1 = { citysize = 7300 }
1600.1.1 = { citysize = 8500 }
1650.1.1 = { citysize = 9000 }
1663.1.1 = { capital = "Bilaspur" }
1700.1.1 = { citysize = 10310 }
1750.1.1 = { citysize = 11770 }
1800.1.1 = { citysize = 14200 }