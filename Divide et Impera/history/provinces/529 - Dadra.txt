#529 - Dadra

owner = DLH
controller = DLH
culture = marathi
religion = hinduism
capital = "Bombay"
trade_goods = fish
hre = no
base_tax = 6
manpower = 2
citysize = 3700
add_core = DLH
fort1 = yes
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = MUG

1388.9.20 = { revolt_risk = 4 } #death of Firuz Shah, dynasty crisis began
1389.2.19 = { revolt_risk = 8 } #Tughluq Shah II is murdered in Delhi
1390.8.31 = { revolt_risk = 3 } #Muhammad Shah III enters Delhi
1392.6.1  = { revolt_risk = 9 } #chaos in sultanate
1393.1.1 = { revolt = { type = nationalist_rebels size = 3 } controller = REB }
1396.1.1 = { revolt = {} owner = GUJ controller = GUJ remove_core = DLH revolt_risk = 0 }
1450.1.1  = { citysize = 3900 }
1429.1.1  = { add_core = BAH }#Bahmanids conquer Mahim
1491.1.1 = { owner = BAH controller = BAH }#Islands are captured by the Bahmanids
1498.1.1  = { discovered_by = POR }
1500.1.1  = { citysize = 4100 }
1518.1.1  = { owner = AHM controller = AHM add_core = AHM remove_core = BAH }#Bahmanids conquer Mahim
1527.1.1  = { owner = GUJ controller = GUJ remove_core = AHM }#Bahmanids conquer Mahim
1535.10.25  = {	owner = POR
		controller = POR
		add_core = POR
	    } # Treaty with the Portuguese
1550.1.1  = { citysize = 6300 cot = yes }
1570.1.1  = { marketplace = yes }
1600.1.1  = { citysize = 7580 }
1650.1.1  = { citysize = 10000 }
1660.1.1  = {	owner = ENG
		controller = ENG
		add_core = ENG
	    } # Treaty of Surat, ceded to Britain
1670.1.1  = { courthouse = yes }
1700.1.1  = { citysize = 60000 }
1707.5.12  = {	owner = GBR
		controller = GBR
		add_core = GBR
	    	remove_core = ENG
	     }
1750.1.1  = {	citysize = 12500
		tax_assessor = yes
	    }