#905 - Iowa

owner = ONE
controller = ONE
add_core = ONE
add_core = IOW
culture = mississippian
religion = animism
capital = "Iowa"
trade_goods = grain
hre = no
base_tax = 2
manpower = 1
citysize = 1500
discovered_by = PLA

discovered_by = QUA
discovered_by = ONE
discovered_by = CAD
discovered_by = CAH
discovered_by = ETO
discovered_by = ILL
discovered_by = IOW
discovered_by = FOX
discovered_by = NAK
discovered_by = DAK
discovered_by = LAK

1600.1.1 = { culture = dakota
religion = shamanism owner = IOW controller = IOW remove_core = ONE trade_goods = unknown }
1684.1.1  = { discovered_by = FRA } # Nicolas Perrot
1727.1.1  = {	owner = FRA
		controller = FRA
		citysize = 522
		culture = cosmopolitan_french
		religion = catholic
		trade_goods = grain
	    } # Several smaller settlements
1752.1.1  = { add_core = FRA }
1763.2.10 = {	owner = GBR
		controller = GBR
		remove_core = FRA
		culture = english
		religion = protestant
	   } # Treaty of Paris - ceded to Britain, France gave up its claim
1783.9.3 = {	owner = USA
		controller = USA
		add_core = USA
	   }
1800.1.1 = { citysize = 2870 }