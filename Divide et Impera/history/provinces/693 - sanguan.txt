#693 - Sanguan

owner = YUA
controller = YUA
culture = chihan
religion = confucianism
capital = "Sanguan"
trade_goods = grain
hre = no
base_tax = 3
manpower = 1
citysize = 7800
add_core = YUA	
discovered_by = ordu
add_core = MNG
add_core = SNG
add_core = QIN
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = YUA

1368.1.1 = { revolt = { type = nationalist_rebels size = 2 } controller = REB }#The Ming didn't send army until 1381
1368.1.22 = { revolt = { } owner = MNG controller = MNG remove_core = YUA }
1450.1.1  = { citysize = 7800 }
1500.1.1  = { citysize = 8500 marketplace = yes }
1550.1.1  = { citysize = 9880 }
1600.1.1  = { citysize = 10454 }
1605.1.1  = { constable = yes }
1630.1.1  = { revolt_risk = 6 } # Li Zicheng rebellion
1644.1.1  = { controller = MCH }
1645.5.27 = { revolt_risk = 0 } # The rebellion is defeated
1646.1.1  = {	owner = MCH
		add_core = MCH
		remove_core = MNG
	    } # The Qing Dynasty
1650.1.1  = { citysize = 11548 }
1683.1.1 = {	owner = QNG
		controller = QNG
		add_core = QNG
		remove_core = MCH
	   } # The Qing Dynasty
1700.1.1  = { citysize = 12800 }
1745.1.1  = { tax_assessor = yes }
1750.1.1  = { citysize = 13487 }
1800.1.1  = { citysize = 15225 }