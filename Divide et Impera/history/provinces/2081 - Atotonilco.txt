#2081 - Atotonilco

owner = TLA
controller = TLA
culture = nahuatl
religion = animism 
capital = "Tollantzinco" 
trade_goods = cattle
hre = no 
base_tax = 3 
manpower = 1 
citysize = 3200
add_core = TLA
add_core = AZT
discovered_by = TLA
discovered_by = TEX
discovered_by = TLC
discovered_by = HST
discovered_by = TEP
discovered_by = MAY
discovered_by = MMA
discovered_by = QUI
discovered_by = TAY
discovered_by = ACA
discovered_by = CHT
discovered_by = CPE
discovered_by = TUX
discovered_by = UAY
discovered_by = CUP
discovered_by = ECA
discovered_by = AKC

discovered_by = ZAP
discovered_by = AZT
discovered_by = MIX
discovered_by = TLA
discovered_by = YPT
discovered_by = MTT
discovered_by = TAR
discovered_by = TEO

1450.1.1  = { owner = AZT controller = AZT } # Estimated, conquered by Moctezuma I
1450.1.1  = { citysize = 3500 }
1465.1.1  = { religion = teotl }
1500.1.1  = { citysize = 3740 }
1519.1.1  = { discovered_by = SPA }
1521.8.13 = {	owner = SPA 
		controller = SPA 
		citysize = 3580
		religion = catholic
		culture = castillian
	   } # The last Aztec emperor surrendered, Mexico city is founded on top of the ruins 
1546.1.1 = { add_core = SPA } 
1550.1.1 = { citysize = 4088 }
1600.1.1 = { citysize = 5670 } 
1624.1.1 = { revolt_risk = 5 } # Riots
1626.1.1 = { revolt_risk = 0 } 
1650.1.1 = { citysize = 6500 } 
1700.1.1 = { citysize = 7400 } 
1720.1.1 = { citysize = 8000 } # Volcano eruption 
1750.1.1 = { citysize = 14000 add_core = MEX } 
1800.1.1 = { citysize = 30000 } 
1810.9.16 = { owner = MEX controller = MEX } # Mexican War of Independence
