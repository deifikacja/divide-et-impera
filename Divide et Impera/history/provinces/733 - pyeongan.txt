#733 - Pyeongan

owner = KOR
controller = KOR
culture = korean
religion = buddhism
capital = "Pyongyang"
trade_goods = iron
hre = no
base_tax = 5
manpower = 4
citysize = 18560
add_core = KOR
fort1 = yes
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = ordu

1400.1.1 = { citysize = 23540 }
1450.1.1 = { citysize = 27540 religion = confucianism }
1500.1.1 = { citysize = 31540 }
1550.1.1 = { citysize = 33587 }
1600.1.1 = { citysize = 26580 } # Great devastation as a result of the Japanese invasion
1637.1.1 = { add_core = MNG } # Tributary of Qing China
1644.1.1 = { add_core = MCH remove_core = MNG } # Part of the Manchu empire
1650.1.1 = { citysize = 28844 }
1653.1.1 = { discovered_by = NED } # Hendrick Hamel
1683.1.1 = { add_core = QNG remove_core = MCH } # The Qing Dynasty
1700.1.1 = { citysize = 31405 }
1750.1.1 = { citysize = 33259 }
1800.1.1 = { citysize = 35222 }
