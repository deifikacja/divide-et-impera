#725 - Hinggan

owner = YUA
controller = YUA
culture = manchu
religion = confucianism
capital = "Hinggan"
trade_goods = cloth
hre = no
base_tax = 7
manpower = 3
fort1 = yes
citysize = 9600
add_core = YUA
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = ordu

1370.1.1 = { owner = KHA controller = KHA add_core = KHA remove_core = YUA }
1450.1.1 = { citysize = 10720 }
1500.1.1 = { citysize = 12800 }
1550.1.1 = { citysize = 13888 }
1600.1.1 = { citysize = 15500 }
1644.1.1 = {	owner = MCH
		controller = MCH
		add_core = MCH
		remove_core = KHA
	   } # Assimilation of Eastern Mongols
1650.1.1 = { citysize = 16980 }
1683.1.1 = {	owner = QNG
		controller = QNG
		add_core = QNG
		remove_core = MCH
	   } # The Qing Dynasty
1700.1.1 = { citysize = 18200 }
1750.1.1 = { citysize = 19532 }
1800.1.1 = { citysize = 21220 }