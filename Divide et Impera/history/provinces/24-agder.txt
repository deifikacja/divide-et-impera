#Agder, incl. Stavanger, Kristiansand

owner = NOR
controller = NOR
add_core = NOR
culture = norwegian
religion = catholic
hre = no
base_tax = 2
trade_goods = fish
manpower = 1
fort1 = yes
capital = "Stavanger"
citysize = 1000 # Estimated
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ

1450.1.1  = { citysize = 1350 }
1500.1.1  = { citysize = 1500 }
1531.11.5 = { revolt = { type = revolutionary_rebels size = 0 } controller = REB } # The Return of Christian II
1532.7.15 = { revolt = {} controller = NOR } # The Capture of Christian II
1536.1.1  = { religion = protestant} #Unknown date
1536.1.1  = { owner = DAN controller = DAN add_core = DAN } #'Handfästningen'(Unknown date)
1550.1.1  = { citysize = 1790 marketplace = yes }
1600.1.1  = { citysize = 2200 }
1650.1.1  = { citysize = 2545 }
1700.1.1  = { citysize = 2800 }
1750.1.1  = { citysize = 2924 }
1800.1.1  = { citysize = 3200 }
1814.1.14 = {	owner = SWE
		revolt = { type = revolutionary_rebels size = 0 }
		controller = REB
		remove_core = DAN
	    } # Norway is ceded to Sweden following the Treaty of Kiel
1814.5.17 = { revolt = {} owner = NOR controller = NOR } # Norway declares itself independent and elects Christian Frederik as king
