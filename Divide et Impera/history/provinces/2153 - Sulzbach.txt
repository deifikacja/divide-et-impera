# 67 - Franken

owner = PAL
controller = PAL
add_core = PAL
culture = bavarian
religion = catholic
base_tax = 3
trade_goods = wine
manpower = 1
fort1 = yes
capital = "Amberg"
citysize = 14000
hre = yes
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
marketplace = yes
courthouse = yes 
temple = yes #Aahlen collegiate	

1450.1.1  = { citysize = 17000 }
1520.1.1 = { constable = yes }
1533.1.1 = { religion = protestant } # Brandenburg-Nürnbergische Kirchenordnung
1550.1.1 = { citysize = 22000 }
1580.1.1 = { customs_house = yes }
1599.1.1 = { regimental_camp = yes citysize = 50000 }
1618.5.6 = { owner = BAV controller = BAV add_core = BAV } # Battle of Prague
1650.1.1 = { citysize = 20000 }
1700.1.1 = { citysize = 24100 }
1742.1.1 = { university = yes}
1750.1.1 = { citysize = 32000 }
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved