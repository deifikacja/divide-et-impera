#480 - Kulunda

culture = siberian
religion = shamanism
capital = "Barnaul"
trade_goods = unknown
hre = no
base_tax = 3
manpower = 1
native_size = 5
native_ferocity = 2
native_hostileness = 3
discovered_by = GOL
discovered_by = ordu

1450.1.1 = { discovered_by = SIB }
1604.1.1 = { discovered_by = RUS }
1608.1.1 = { citysize = 1000 owner = KLM controller = KLM add_core = KLM trade_goods = wool }#The Torghuts
1630.1.1 = { citysize = 0 owner = XXX controller = XXX remove_core = KLM trade_goods = unknown }#The Torghuts move to Volga Basin
1738.1.1 = {	owner = RUS
		controller = RUS
	   	religion = orthodox
	   	culture = russian
		citysize = 200
		fort1 = yes
	   }
1800.1.1 = { citysize = 800 trade_goods = wool }