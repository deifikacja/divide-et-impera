#535 - Malabar

owner = MAD
controller = MAD
culture = malayalam
religion = hinduism
capital = "Kannur"
trade_goods = spices
hre = no
base_tax = 4
manpower = 1
citysize = 4380
add_core = KNN
add_core = MAD
add_core = VIJ
fort1 = yes
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1378.1.1 = { owner = VIJ controller = VIJ remove_core = MAD }#The Vijayangar Empire
1498.1.1 = { discovered_by = POR }
1500.1.1 = { citysize = 5344 }
1502.1.1 = { owner = POR controller = POR add_core = POR }
1545.1.1 = {	owner = KNN
		controller = KNN
		remove_core = VIJ
		revolt_risk = 4
	   } # The Vijayanagar empire collapses
1550.1.1 = { revolt_risk = 0 citysize = 6130 }
1600.1.1 = { citysize = 7000 }
1650.1.1 = { citysize = 8699 }
1663.2.15 = { add_core = NED }
1700.1.1 = { citysize = 10988 }
1750.1.1 = { citysize = 12708 }
1783.1.1 = { add_core = GBR } # British protecttorate
1800.1.1 = { citysize = 14200 }
