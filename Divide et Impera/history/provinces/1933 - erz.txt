#1771 - Erz

owner = BOH
controller = BOH
add_core = BOH
capital = "Liberec"
culture = czech
religion = catholic
trade_goods = cloth  
hre = yes
base_tax = 3
manpower = 1
citysize = 8250
fort1 = yes
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1419.7.30  = { add_core = HUS }
1419.12.1  = { revolt_risk = 5 } # Hussite Wars, Battle of Nekmir
1420.7.14  = { owner = HUS controller = HUS }
1436.5.30  = { owner = BOH controller = BOH remove_core = HUS }
1436.7.5   = { revolt_risk = 0 } # Peace is signed in Jihlava
1450.1.1   = { citysize = 9750 }
1457.1.1   = { revolt_risk = 5 regimental_camp = yes } # George of Podiebrand had to secure recognition from the German and Catholic towns
1464.1.1   = { revolt_risk = 1 } # The Catholic nobility still undermines the crown.
1471.1.1   = { revolt_risk = 0 }
1526.8.30  = {	owner = HAB
		controller = HAB
		add_core = HAB
		remove_core = BOH
	     } # Battle of Mohac where Lajos II dies -> Habsburg succession
1550.1.1   = { citysize = 11250 customs_house = yes }
1576.1.1   = { religion = reformed }
1600.1.1   = { citysize = 13500 constable = yes }
1618.4.23  = { revolt = { type = heretic_rebels size = 2 } controller = REB } # Defenstration of Prague
1619.3.1   = {	revolt = {}
		owner = PAL
		controller = PAL
		add_core = PAL
	     }
1620.11.8  = {	owner = HAB
		controller = HAB
		remove_core = PAL
		culture = saxon
	     } # Tilly beats the Winterking. Deus Vult!
1621.1.1  = { courthouse = yes } # ... and let us start this session by executing the most inconvenient nobles....
1627.1.1  = { citysize = 12000 religion = catholic } # Order from Ferdinand II to reconvert to Catholicism, many Protestant leave the country  
1700.1.1  = { citysize = 14500 tax_assessor = yes }
1750.1.1  = { citysize = 16500 }
1800.1.1  = { citysize = 18100 }
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
