#280 - Kiev

owner = KIE
controller = KIE     
culture = ruthenian	
discovered_by = ordu
religion = orthodox
hre = no
base_tax = 2	
trade_goods = grain  
manpower = 2	
capital = "Zhytomir"
citysize = 1980
fort1 = yes
add_core = KIE
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1356.1.1 = { add_core = LIT }
1362.12.25 = { owner = LIT controller = LIT } #Battle of Blue Waters
1450.1.1  = { citysize = 2205 }
1500.1.1  = { citysize = 2405 remove_core = KIE }
1550.1.1  = { citysize = 4000 }
1569.7.4   = {	owner = RZP
		controller = RZP
		add_core = RZP
		remove_core = LIT
	     }
1595.1.1  = { revolt_risk = 4 culture = ukrainian add_core = UKR } # Union of Brest, religious struggles
1596.1.1  = { revolt_risk = 0 } 
1600.1.1  = { citysize = 6120 manpower = 3 base_tax = 3 }
1648.5.30  = {	owner = UKR
		controller = UKR
	    } # Chmielnicki uprising, civil war
1650.1.1  = { citysize = 7620 }
1651.8.4  = { owner = RZP controller = RZP }
1655.6.1   = { controller = RUS }
1660.11.4  = { controller = RZP }
1700.1.1  = { citysize = 9550 }
1715.1.1  = { tax_assessor = yes }
1750.1.1  = { citysize = 11900 }
1768.1.1  = { revolt_risk = 8 } # Kolivshchyna rebellion, peasant uprising
1769.1.1  = { revolt_risk = 0 } # Suppressed by Polish and Russian troops
1793.2.17 = { owner = RUS controller = RUS add_core = RUS } # Second partition of Poland
1800.1.1  = { citysize = 14000 }
1807.7.9  = { remove_core = RZP } # The Duchy of Warsaw is established instead of Poland
