# 310 - Novgorod

owner = NOV
controller = NOV
culture = russian	
discovered_by = ordu
religion = orthodox
hre = no
cot = yes
base_tax = 8
trade_goods = fur
manpower = 3
capital = "Novgorod"
citysize = 15500
fort1 = yes
add_core = NOV
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western




discovered_by = PEL


1438.1.1  = { discovered_by = KAZ }
1450.1.1  = { citysize = 18000 }
1478.1.14 = {	owner = MOS
		controller = MOS
		add_core = MOS
		remove_core = NOV
	    }
1484.1.1  = { revolt_risk = 6 citysize = 14000 base_tax = 6 } # Massacres and deportation of leading citizens to inland Russia
1489.1.1  = { revolt_risk = 0 }
1500.1.1  = { citysize = 17500 marketplace = yes }
1503.1.1  = { temple = yes } # St Sophia Cathedral
1503.3.22 = {	owner = RUS
		controller = RUS
		add_core = RUS
		remove_core = MOS
	    }
1550.1.1  = { citysize = 20800 fort2 = yes }
1570.1.1  = { citysize = 15200 } # Thousands slaughtered by Ivan the Terrible
1572.1.1  = { cot = no base_tax = 5 }
1598.1.1  = { revolt_risk = 5 } # "Time of troubles"
1600.1.1  = { citysize = 14500 }
1611.1.1  = { controller = SWE } # Occupied by the Swedes
1615.1.1  = { citysize = 9000 } # Ingrian war
1617.2.27 = { controller = RUS revolt_risk = 0 } 
1650.1.1  = { citysize = 7000 fort3 = yes } # Fortifications were restored and reconstructed
1670.1.1  = { revolt_risk = 8 } # Stepan Razin
1671.1.1  = { revolt_risk = 0 } # Razin is captured
1672.1.1  = { base_tax = 8 } # Tax revenues increased
1700.1.1  = { citysize = 7300 }
1707.1.1  = { revolt_risk = 3 } # The Kondraty Bulavin Rebellion
1708.7.7  = { revolt_risk = 0 } # Bulains was shot  
1711.1.1  = { base_tax = 7 } # Governmental reforms and the absolutism
1750.1.1  = { citysize = 9500 }
1800.1.1  = { citysize = 11000 }