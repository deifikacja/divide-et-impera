#569 - Manipur

owner = MLB
controller = MLB
culture = bengali
religion = hinduism
capital = "Imphal"
trade_goods = grain
hre = no
base_tax = 1
manpower = 1
citysize = 5320
fort1 = yes
add_core = MLB
discovered_by = indian
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM

1450.1.1 = { citysize = 6100 }
1500.1.1 = { citysize = 7240 }
1550.1.1 = { citysize = 8135 }
1600.1.1 = { citysize = 9867 }
1650.1.1 = { citysize = 11007 }
1700.1.1 = { citysize = 12450 }
1732.1.1 = { revolt_risk = 5 } # King Pamheiba expelled everyone who opposed vaisnavism
1735.1.1 = { revolt_risk = 0 }
1750.1.1 = { citysize = 14890 }
1769.1.1 = { revolt_risk = 6 } # Burmese attack
1773.1.1 = { revolt_risk = 0 }
1800.1.1 = { citysize = 17000 }