#2119 - Cawahie

culture = amazonian
religion = animism
capital = "Cawahie"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 5
native_ferocity = 1
native_hostileness = 3



1542.4.1  = { discovered_by = SPA discovered_by = POR } # da Orellana
1759.1.1 = {	owner = POR
		controller = POR
		add_core = BRZ
		capital = "Tef�"
		citysize = 160
		culture = portugese
		religion = catholic
	    } # Jesuit mission
1800.1.1  = { citysize = 200 }