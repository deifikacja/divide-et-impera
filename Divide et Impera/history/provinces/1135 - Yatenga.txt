#1135 - Yatenga

owner = YAT
controller = YAT
add_core = YAT
culture = mossi
religion = animism
capital = "Gursi"
trade_goods = grain
hre = no
base_tax = 3
manpower = 2
citysize = 1500
discovered_by = YAT
discovered_by = FUT
discovered_by = OUG
discovered_by = NUN
discovered_by = BUS

1450.1.1  = { citysize = 2000 }
1460.1.1  = { revolt_risk = 3 } #Songhai raids devastate region
1468.1.1  = { revolt_risk = 3 } #Songhai raids devastate region
1484.1.1  = { discovered_by = SON owner = SON controller = SON add_core = SON citysize = 4000 manpower = 1 } #Conquered by Sunni Ali Ber of Songhai
1540.1.1  = { revolt_risk = 6 } #Mossi invasion led by Yadega established Mossi state
1556.1.1  = { revolt_risk = 3 } #Unrest among the Mossi
1563.1.1  = { revolt_risk = 3 } #Unrest among the Mossi
1569.1.1  = { revolt_risk = 3 } #Unrest among the Mossi
1578.1.1  = { revolt_risk = 3 } #Unrest among the Mossi
1586.1.1  = { revolt = { type = pretender_rebels size = 0 } controller = REB } #Civil war between Al-Sadduk and Ishak
1587.1.1  = { controller = SON revolt_risk = 6 } #Ishak reconquers lands in revolt, uneasy on throne
1591.3.15 = { owner = YAT controller = YAT } #Collapse of Songhai, Mossi move to consolidate power
1642.1.1  = { remove_core = SON } #Collapse of last vestiges of unity among Songhai
1650.1.1  = { capital = "Biisaga" citysize = 6000 } #Mossi expansion shifts capital northward
1756.1.1  = { revolt_risk = 8 } #Civil war between Kango and Wobgo for throne of Yatenga
1758.1.1  = { revolt_risk = 1 } #Kango wins civil war
1781.1.1  = { capital = "Wahiguya" native_size = 7000 } #New capital built by Kango
1788.1.1  = { revolt_risk = 6 } #Civil War on death of Kango as daughter tries to inherit power
1797.1.1  = { revolt_risk = 0 } #Naaba Saagha restores political stability in Yatenga