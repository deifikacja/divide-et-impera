#332 - Mus

owner = RAM
controller = RAM
culture = turkish
religion = sunni
capital = "Elbistan"
trade_goods = wool
hre = no
base_tax = 3
manpower = 3
citysize = 1546
fort1 = yes
add_core = RAM #Historical claim
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = ordu

1356.1.1 = { add_core = TUR add_core = AMI }
1402.4.1 = { controller = TIM }
1403.1.1 = { controller = RAM }
1441.1.1 = { remove_core = AMI }
1450.1.1 = { citysize = 2260 }
1500.1.1 = { citysize = 2966 marketplace = yes }
1550.1.1 = { citysize = 4008 }
1600.1.1 = { citysize = 5100 }
1608.1.1 = {	owner = TUR
		controller = TUR
		add_core = TUR
	   }
1650.1.1 = { citysize = 5655 }
1700.1.1 = { citysize = 6005 }
1750.1.1 = { citysize = 6733 }
1800.1.1 = { citysize = 7290 }