#352 - Gafsa

owner = TUN
controller = TUN
culture = maghreb_arabic
religion = ibadi
capital = "Touggourt"
trade_goods = grain
hre = no
base_tax = 3
manpower = 2
citysize = 2745
add_core = TUG
add_core = TUN
discovered_by = MOR
discovered_by = TUN
discovered_by = TUG
discovered_by = FZZ

1414.1.1  = { owner = TUG controller = TUG remove_core = TUN religion = sunni }
1450.1.1  = { citysize = 2929 }
1500.1.1  = { citysize = 3077 }
1550.1.1  = { citysize = 3297 }
1600.1.1  = { citysize = 3600 }
1650.1.1  = { citysize = 3650 }
1700.1.1  = { citysize = 4716 marketplace = yes }
1750.1.1  = { citysize = 4968 }
1800.1.1  = { citysize = 5210 }