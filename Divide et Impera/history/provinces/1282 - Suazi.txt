#1282 - Swaziland

culture = bantu #Tsonga
religion = animism
capital = "Elangeni"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 70
native_ferocity = 4.5
native_hostileness = 9 