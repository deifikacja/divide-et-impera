#619 - Minangkabau

owner = MIG
controller = MIG
culture = malayan
religion = buddhism
capital = "Padang"
trade_goods = naval_supplies
hre = no
base_tax = 1
manpower = 1
citysize = 100
native_size = 30
native_ferocity = 4
native_hostileness = 6
add_core = MIG
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1406.1.1 = { add_core = ATJ }
1500.1.1 = { citysize = 270 }
1509.1.1 = { discovered_by = POR }
1550.1.1 = { citysize = 340 }
1550.1.1 = { owner = ATJ controller = ATJ add_core = ATJ religion = sunni }
1600.1.1 = { citysize = 389 }
1650.1.1 = { citysize = 425 }
1663.1.1 = { owner = NED controller = NED }
1683.1.1 = { add_core = NED }
1700.1.1 = { citysize = 487 }
1750.1.1 = { citysize = 530 }
1800.1.1 = { citysize = 676 }