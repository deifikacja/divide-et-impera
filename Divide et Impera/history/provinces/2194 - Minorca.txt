#333 - The Baleares

owner = ARA		#Alfons V of Aragon
controller = ARA
add_core = ARA
culture = catalan
religion = catholic
hre = no
base_tax = 2 
trade_goods = fish
manpower = 1 
fort1 = yes
capital = "Mahon" 
citysize = 1000
marketplace = yes # Mediterranean trade
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1450.1.1  = { citysize = 1500 }
1462.1.1  = { revolt_risk = 2 } # Remen�a peasant revolt, in parallel with the Catalan civil war.
1472.1.1  = { revolt_risk = 0 } # End of the First Remen�a revolt
1500.1.1  = { citysize = 1604 }
1516.1.23 = {	controller = SPA
		owner = SPA
		add_core = SPA
	    } # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castille
1521.3.16 = { revolt = { type = revolutionary_rebels size = 0 } controller = REB } # The Germanies movement reaches the archipelago, the viceroy is deposed by the revolters
1523.3.8  = { revolt = {} controller = SPA } # The royal army retakes the city of Palma
1550.1.1  = { citysize = 1740 }
1600.1.1  = { citysize = 2300 }
1650.1.1  = { citysize = 2400 }
1700.1.1  = { citysize = 2537 }
1705.10.9 = { controller = HAB } # Balearic isles side with the Austrian cause
1708.1.1 = { controller = GBR } # Minorca captured by the British Navy
1713.4.11 = { owner = GBR controller = GBR } # Treaty of Utrecht.
1713.7.13 = { remove_core = ARA }
1756.6.29 = { controller = FRA } # Seven years war
1750.1.1  = { citysize = 2780 }
1763.2.10 = { controller = GBR } # Treaty of Paris
1782.2.5 = { owner = FRA controller = FRA } #US revolution
1789.1.1  = { citysize = 2900 }
1798.1.1 = { owner = GBR controller = GBR } # Treaty of Paris
1802.3.25 = { owner = SPA controller = SPA } # Treaty of Amiens