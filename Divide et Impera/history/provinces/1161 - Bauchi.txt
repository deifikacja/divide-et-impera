#1161 - Bauchi

culture = hausa
religion = sunni
capital = "Bauchi"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 60
native_ferocity = 4.5
native_hostileness = 9
discovered_by = KBO
discovered_by = FZZ
discovered_by = TUG
discovered_by = KTS
discovered_by = ZAR
discovered_by = GOB
discovered_by = ZAM
discovered_by = HAD
discovered_by = KAN


1568.1.1 = {	owner = KBO
		controller = KBO
		add_core = KBO
		citysize = 3500
		trade_goods = grain
	   }
1681.1.1 = { revolt_risk = 8 } #Jukun Invasion
1682.1.1 = { controller = WUK } #Jukun take over
1684.1.1 = { controller = KBO revolt_risk = 0 } #Jukun Expelled
1700.1.1 = { citysize = 4500 }
1783.1.1 = { revolt = { type = nationalist_rebels size = 1 } controller = REB } #Borno overlords expelled
1784.1.1 = { revolt = {} controller = KBO }
1800.1.1 = { citysize = 5500 }
1805.1.1 = { owner = BAU controller = BAU add_core = BAU discovered_by = BAU }#Bauchi djihad state founded