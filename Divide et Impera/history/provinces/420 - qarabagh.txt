#420 - Qarabagh

owner = TAB
controller = TAB
culture = azerbadjani
religion = shiite
capital = "Stepanekert"
trade_goods = cattle
hre = no
base_tax = 2
manpower = 1
citysize = 1011
add_core = TAB
add_core = QBH
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = BYZ
discovered_by = TIM
discovered_by = SHY
discovered_by = MUG
discovered_by = ordu

1356.1.1 = { add_core = QAR }
1357.1.1 = { controller = GOL }
1358.1.1 = { owner = JAL controller = JAL }
1380.1.1 = { owner = QAR controller = QAR }
1382.1.1 = { owner = TIM controller = TIM }
1408.1.1  = { controller = QAR owner = QAR remove_core = TIM }
1450.1.1  = { citysize = 1245 }
1458.9.1  = { revolt = { type = revolutionary_rebels size = 0 } controller = REB } # Civil war in Qara Quyunlu
1458.12.1 = { revolt = {} controller = QAR }
1468.1.1  = {	owner = AKK
		controller = AKK
		add_core = AKK
		remove_core = QAR
	    } # The Ak Koyunlu expands their territory
1500.1.1  = { citysize = 1870 }
1501.1.1  = {	owner = PER
		controller = PER
		add_core = PER
		remove_core = AKK
	     } # The Safavids take over
1550.1.1   = { citysize = 3053 }
1554.1.1   = { controller = TUR } # Wartime occupation
1555.5.29  = { controller = PER } # Peace of Amasya
1588.1.1   = { controller = TUR } # Ottoman conquest
1590.3.21  = { owner = TUR add_core = TUR } # Peace of Istanbul
1600.1.1   = { citysize = 2775 }
1607.1.1   = { controller = PER } # Persian reconquest
1612.11.20 = { owner = PER remove_core = TUR } # Part of Persia
1650.1.1   = { citysize = 3623 }
1700.1.1   = { citysize = 4708 }
1730.1.1   = { tax_assessor = yes }
1750.1.1   = { citysize = 5534 }
1722.1.1   = { owner = QBH controller = QBH } # Persian vassals
1800.1.1   = { citysize = 6852 }
1806.1.1   = { owner = RUS controller = RUS add_core = RUS } # Conquered by Russia