#2028 - Lau

culture = malay-polynesian
religion = animism
capital = "Lomaloma"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 5
native_ferocity = 0.5
native_hostileness = 2
discovered_by = BUU
discovered_by = BUA
