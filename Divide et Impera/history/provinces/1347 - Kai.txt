#1347 - Kai - home of the Takeda clan

owner = TAK
controller = TAK
culture = japanese
religion = shinto
capital = "Takaoka"
trade_goods = iron
hre = no
base_tax = 3
manpower = 2
citysize = 11050
add_core = TAK
discovered_by = ordu
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM

1500.1.1 = { citysize = 13148 }
1542.1.1 = { discovered_by = POR }
1550.1.1 = { citysize = 14333 }
1582.1.1 = { owner = ODA controller = ODA add_core = ODA } #Takeda are destroyed in the decisive battle of Nagashima
1582.6.21 = { owner = TOY controller = TOY add_core = TOY } #Oda Nobunanga's succesor
1599.1.1 = { owner = TKG controller = TKG add_core = TKG } #Eastern Alliance
1600.10.22 = { citysize = 15678 owner = JAP controller = JAP add_core = JAP } #Sekahigara
1650.1.1 = { citysize = 17543 }
1700.1.1 = { citysize = 18150 }
1750.1.1 = { citysize = 19877 }
1800.1.1 = { citysize = 22140 }