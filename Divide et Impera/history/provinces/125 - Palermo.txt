#125 - Palermo

owner = SIC
controller = SIC
culture = sicilian 
religion = catholic 
hre = no 
base_tax = 7
trade_goods = wine
manpower = 2
fort1 = yes 
capital = "Palermo"
citysize = 61000	# Estimated
add_core = SIC
add_core = NAP
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
 
1409.7.25 = {	owner = ARA
		controller = ARA
		add_core = ARA
	    } # Martin I inherits Sicily from his son
1450.1.1  = { citysize =  65000 } 
1500.1.1  = { citysize =  77000 marketplace = yes } 
1516.1.23 = {	owner = SPA
		controller = SPA
		add_core = SPA
	    	remove_core = ARA
	    } # Unification of Spain
1550.1.1  = { citysize =  99000 }
1600.1.1  = { citysize = 132000 }
1650.1.1  = { citysize = 157000 }
1700.1.1  = { citysize = 128000 }
1706.7.1  = { controller = SAV }
1713.4.11 = {	owner = SIC
		controller = SIC
		remove_core = SPA
	    }
1718.8.2  = {	owner = HAB
		controller = HAB
		add_core = HAB
	    }
1734.6.2  = {	owner = SIC
		controller = SIC
		remove_core = HAB
	    }
1750.1.1  = { citysize = 153000 }
1800.1.1  = { citysize = 182000 }