#1287 - Bukerewe

culture = bantu
religion = animism
capital = "Kivumbiro"
native_size = 60
native_hostileness = 6
native_ferocity = 5
manpower = 1
trade_goods = unknown
hre = no
base_tax = 1
discovered_by = BRN
discovered_by = BUG
discovered_by = BUN
discovered_by = RWA

1430.1.1 = { discovered_by = NKO }
1822.1.1 = { discovered_by = TOR }