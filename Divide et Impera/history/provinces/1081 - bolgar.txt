#1081 - Bolgar

owner = GOL
controller = GOL
capital = "Bolgar"
culture = volgaic
religion = sunni
trade_goods = iron
hre = no
base_tax = 4
manpower = 4
citysize = 2950
fort1 = yes
add_core = KAZ
add_core = GOL	
discovered_by = ordu
discovered_by = CRI
discovered_by = GOL
discovered_by = KAZ
discovered_by = SIB
discovered_by = NOG
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western

1438.1.1 = { owner = KAZ controller = KAZ remove_core = GOL }
1450.1.1 = { citysize = 3264 }
1500.1.1 = { citysize = 3955 }
1550.1.1 = { citysize = 4822 }
1552.1.1 = {	owner = RUS
		controller = RUS
		add_core = RUS
		remove_core = KAZ
		fort2 = yes
	   } # Conquered by Russia
1553.1.1 = { discovered_by = ordu religion = orthodox } # Most Tatars were christianized or killed
1600.1.1 = { citysize = 4165 }
1650.1.1 = { citysize = 3800 }
1700.1.1 = { citysize = 4670 }
1750.1.1 = { citysize = 5800 }
1773.1.1 = { revolt_risk = 5 } # Peasant uprising, Pugachev
1774.1.1 = { revolt_risk = 0 }
1800.1.1 = { citysize = 6780 }