#727 - Heilongjiang

owner = YUA
controller = YUA
culture = manchu
religion = confucianism
capital = "Harbin"
trade_goods = cloth
hre = no
base_tax = 6
manpower = 5
fort1 = yes
citysize = 16200
add_core = HXJ
add_core = YUA
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = ordu

1368.1.22 = { owner = HXJ controller = HXJ remove_core = YUA }
1450.1.1 = { citysize = 16200 }
1500.1.1 = { citysize = 18500 }
1550.1.1 = { citysize = 20549 }
1600.1.1 = { owner = MCH controller = MCH add_core = MCH remove_core = HXJ }#Nurhaci unites Haixi Jurchens
1600.1.1 = { citysize = 22500 }
1650.1.1 = { citysize = 24648 }
1683.1.1 = {	owner = QNG
		controller = QNG
		add_core = QNG
		remove_core = MCH
	   } # The Qing Dynasty
1700.1.1 = { citysize = 26870 }
1750.1.1 = { citysize = 28870 }
1800.1.1 = { citysize = 30950 }