#1999 - Angkor

owner = KHM
controller = KHM
culture = khmer
religion = buddhism
capital = "Angkor"
trade_goods = luxury_goods
hre = no
base_tax = 6
manpower = 2
citysize = 30200
fort1 = yes
add_core = KHM
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = indian
discovered_by = YUA

1352.1.1 = { controller = AYU }#Siamese invasion
1357.1.1 = { controller = KHM }#Siamese invasion
1369.1.1 = { owner = AYU controller = AYU citysize = 25000 }#Conquered by Thais
1375.1.1 = { owner = KHM controller = KHM }
1380.1.1 = { owner = AYU controller = AYU citysize = 20000 }#Conquered by Thais
1389.1.1 = { owner = KHM controller = KHM }
1431.1.1 = { citysize = 3110 base_tax = 2 manpower = 1 trade_goods = grain } #Angkor Wat is destroyed & abandoned
1450.1.1 = { citysize = 3930 }
1500.1.1 = { citysize = 4800 }
1550.1.1 = { citysize = 5600 }
1600.1.1 = { citysize = 7226 }
1650.1.1 = { citysize = 8540 }
1700.1.1 = { citysize = 9980 }
1750.1.1 = { citysize = 10240 }
1800.1.1 = { citysize = 11800 }
1811.1.1 = { revolt_risk = 6 } # The Siamese-Cambodian Rebellion
1812.1.1 = { revolt_risk = 0 }