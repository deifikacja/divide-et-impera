#373 - Meath

owner = ENG
controller = ENG
add_core = ENG
culture = irish
religion = catholic
hre = no
base_tax = 4
trade_goods = grain
manpower = 3
capital = "Dublin"
citysize = 4000
fort1 = yes
temple = yes #St. Patrick's Cathedral, Dublin
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ

1450.1.1  = { citysize = 4000 }
1500.1.1  = { marketplace = yes } #Estimated
1550.1.1  = { citysize = 5000 }
1592.1.1  = { university = yes } #Trinity College, Dublin
1600.1.1  = { citysize = 10000 }
1625.1.1  = { courthouse = yes fort2 = yes } #Estimated
1641.12.3 = { revolt = { type = nationalist_rebels size = 2 } controller = REB }
1642.4.15 = { revolt = {} controller = ENG }
1650.1.1  = { citysize = 17000 }
1689.3.12 = { revolt = { type = nationalist_rebels size = 2 } controller = REB } # James II Lands in Ireland
1690.1.1  = { revolt = {} controller = ENG } # Battle of the Boyne
1700.1.1  = { citysize = 60000 customs_house = yes } #Customs House Estimated
1707.5.12 = {	owner = GBR
		controller = GBR
		add_core = GBR
	    	remove_core = ENG
	    }
1750.1.1  = { citysize = 90000 regimental_camp = yes } #Estimated
1798.8.27 = { revolt = { type = nationalist_rebels size = 2 } controller = REB } # Republic of Connaught
1798.9.8  = { revolt = {} controller = GBR }
1800.1.1  = { citysize = 168000 }
1803.7.23 = { revolt = { type = nationalist_rebels size = 2 name = "Emmet's Rebellion" } controller = REB } # Emmet's rebellion
1803.8.25 = { revolt = {} controller = GBR }
