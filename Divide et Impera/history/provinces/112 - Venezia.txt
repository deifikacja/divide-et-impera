#112 - Venezia

owner = VEN
controller = VEN
culture = venetian
religion = catholic 
hre = no 
base_tax = 10   
trade_goods = luxury_goods
manpower = 1        
fort1 = yes 
fort2 = yes
capital = "Venezia" 
citysize = 118000	# Estimated 
marketplace = yes 
workshop = yes
regimental_camp = yes
shipyard = yes
add_core = VEN
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
cot = yes

1400.1.1 = { citysize = 121000 }
1450.1.1 = { citysize = 124000 }
1495.1.1 = { print_works = yes }
1500.1.1 = { citysize = 127000 } 
1550.1.1 = { citysize = 190000 } 
1600.1.1 = { citysize = 175000 } 
1650.1.1 = { citysize = 145000 } 
1700.1.1 = { citysize = 168000 } 
1750.1.1 = { citysize = 180000 } 
1797.10.17 = {	owner = HAB
		controller = HAB
		add_core = HAB
	     } # Treaty of Campo Formio
1800.1.1 = { citysize = 170000 }
1805.12.26 = {	owner = ITA
		controller = ITA
		add_core = ITA
	     	remove_core = HAB
	     } # Treaty of Pressburg
1814.4.11  = {	owner = HAB
		controller = HAB
		add_core = HAB
		remove_core = ITA
	     } # Treaty of Fontainebleau, Napoleon abdicates unconditionally