# 193 Franche-Comt� - Principal cities: Besan�on

owner = BUR
controller = BUR 
capital = "Besan�on"
citysize = 5500
culture = burgundian
religion = catholic
hre = yes
base_tax = 5
trade_goods = salt
manpower = 3
add_core = BUR
fort1 = yes
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1382.5.9   = { owner = FLA controller = FLA add_core = FLA } #Louis III the Male is a heir of part of the Burgundy
1384.1.20  = { owner = BUR controller = BUR remove_core = FLA }
1450.1.1   = { citysize = 6500 }
1477.1.5   = { owner = FRA controller = FRA add_core = FRA } # Charles the Bold dies and Louis XII of France takes Franche-Comt�
1493.8.19  = { owner = HAB controller = HAB add_core = HAB remove_core = FRA } # Frederick III dies and Charles VII cedes Franch-Comt� to Maximilian I von Habsburg
1500.1.1   = { citysize = 8000 }
1540.1.1   = { fort2 = yes }
1550.1.1   = { citysize = 10000 }
1519.1.14  = { owner = SPA controller = SPA add_core = SPA remove_core = HAB } # Karl V gets total control over his Burgundian inheritance
1580.1.1   = { marketplace = yes }
1600.1.1   = { citysize = 11000 }
1640.1.1   = { fort3 = yes }
1650.1.1   = { citysize = 13000 constable = yes }
1668.2.20  = { controller = FRA } # The Prince de Cond� swiftly takes Franche-Comt� in the War of Devolution
1668.5.2   = { controller = SPA } # Treaty of Aachen: Franche-Comt� returned to Spain
1670.1.1   = { add_core = FRA } # Louis XIV lays claims through the Chambres de R�union
1674.9.1   = { controller = FRA } # France captures Franche-Comt� 
1678.9.19  = { owner = FRA remove_core = SPA hre = no } # Treaty of Nijmegen (FRA-SPA)
1685.1.1   = { fort4 = yes }
1691.1.1   = { university = yes } # University of Dole transferred to Besan�on by Louis XIV
1700.1.1   = { citysize = 17000 }
1710.1.1   = { courthouse = yes }
1740.1.1   = { fort5 = yes tax_assessor = yes }
1750.1.1   = { citysize = 21000 }
1789.7.14  = {	owner = RFR
		controller = RFR
		add_core = RFR
		remove_core = FRA
	     } # The storming of the Bastille
1800.1.1   = { citysize = 24000 }
1814.4.11  = {	owner = FRA
		controller = FRA
		add_core = FRA
		remove_core = RFR
	     } # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1815.3.20  = {	owner = RFR
		controller = RFR
		add_core = RFR
		remove_core = FRA
	     } # Napoleon enters Paris
1815.7.8   = {	owner = FRA
		controller = FRA
		add_core = FRA
		remove_core = RFR
	     } # The French monarchy is restored
