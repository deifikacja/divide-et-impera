#1026 - Mutsu

owner = DAT
controller = DAT
culture = japanese
discovered_by = ordu
religion = shinto
capital = "Mutsu"
trade_goods = fish
hre = no
base_tax = 4
manpower = 2
citysize = 4200
add_core = DAT
add_core = NAN
fort1 = yes
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM

1450.1.1 = { citysize = 4910 }
1500.1.1 = { citysize = 5680 }
1542.1.1 = { discovered_by = POR }
1550.1.1 = { citysize = 7100 }
1590.1.1 = { owner = TOY controller = TOY add_core = TOY }
1599.1.1 = { owner = TKG controller = TKG add_core = TKG }#Eastern Alliance
1600.10.22 = { citysize = 15678 owner = JAP controller = JAP add_core = JAP }#Sekahigara
1650.1.1 = { citysize = 9680 capital = "Sendai" }
1700.1.1 = { citysize = 10200 }
1750.1.1 = { citysize = 12348 }
1800.1.1 = { citysize = 14680 }