#657 - Samtoy

culture = filipino
religion = animism
capital = "Samtoy"
trade_goods = unknown
hre = no
base_tax = 4
manpower = 1
native_size = 40
native_ferocity = 1
native_hostileness = 3

1521.1.1  = { discovered_by = SPA } # Ferdinand Magellan
1570.1.1  = {	owner = SPA
		controller = SPA
	   	culture = castillian
	   	religion = catholic
	   	citysize = 330
		trade_goods = grain
	    }
1595.1.1  = { add_core = SPA }
1600.1.1  = { citysize = 850 }
1650.1.1  = { citysize = 1800 }
1700.1.1  = { citysize = 2487 }
1750.1.1  = { citysize = 3000 }
1800.1.1  = { citysize = 4345 }
1807.9.16 = { revolt = { type = nationalist_rebels size = 1 } controller = REB } # Basi Revolt
1807.9.26 = { revolt = {} controller = SPA }