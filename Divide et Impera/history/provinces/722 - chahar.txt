#722 - Chahar

owner = YUA
controller = YUA
culture = mongol
religion = tibetan_buddhism
capital = "Chahar"
trade_goods = grain
hre = no
base_tax = 2
manpower = 3
citysize = 6400
add_core = YUA
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = ordu

1370.1.1 = { owner = KHA controller = KHA add_core = KHA remove_core = YUA }
1450.1.1 = { citysize = 6950 }
1500.1.1 = { citysize = 7124 }
1543.1.1 = { revolt = { type = revolutionary_rebels size = 2 } controller = REB } # Fighting broke out between the Mongols
1550.1.1 = { citysize = 10580 }
1552.1.1 = {	revolt = {}
		controller = KHA
	   } # The Oirads are defeated & Mongolia is reunited under Altan Khan
1586.1.1 = { religion = tibetan_buddhism } # State religion
1600.1.1 = { citysize = 10986 }
1634.1.1 = {	owner = MCH
		controller = MCH
		add_core = MCH
		remove_core = KHA
	   } # Part of the Manchu empire
1650.1.1 = { citysize = 12487 }
1683.1.1 = {	owner = QNG
		controller = QNG
		add_core = QNG
		remove_core = MCH
	   } # The Qing Dynasty
1700.1.1 = { citysize = 14855 }
1750.1.1 = { citysize = 15388 }
1800.1.1 = { citysize = 17040 }