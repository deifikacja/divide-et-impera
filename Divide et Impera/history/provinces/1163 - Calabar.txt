#1163 - Calabar

culture = jukun
religion = animism
capital = "Calabar"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 50
native_ferocity = 4.5
native_hostileness = 9
discovered_by = SON
discovered_by = KBO
discovered_by = BEN

1472.1.1 = { discovered_by = POR } # Fren�o do P�
1500.1.1 = { native_size = 55 }
1600.1.1 = { native_size = 65 }
1700.1.1 = { native_size = 80 }
1800.1.1 = { native_size = 90 }
1804.1.1 = { discovered_by = SOK }