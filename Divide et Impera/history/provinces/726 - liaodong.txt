#726 - Liaodong

owner = YUA
controller = YUA
culture = manchu
religion = confucianism
capital = "Shenyang"
trade_goods = grain
hre = no
base_tax = 8
manpower = 4
citysize = 128500
add_core = YUA
fort1 = yes
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = ordu

1370.1.1 = { owner = MNG controller = MNG add_core = MNG remove_core = YUA }
1450.1.1 = { citysize = 134431 }
1500.1.1 = { citysize = 138999 }
1505.1.1 = { marketplace = yes }
1550.1.1 = { citysize = 140877 }
1600.1.1 = { citysize = 142254 }
1618.1.1 = { controller = MCH }
1625.1.1 = { capital = "Mukden" } # Renamed
1626.1.1 = { controller = MNG } # Thruce
1634.1.1 = { controller = MCH }
1641.1.1 = {	owner = MCH
		add_core = MCH
	   } # The Qing Dynasty
1650.1.1 = { citysize = 143200 constable = yes }
1683.1.1 = {	owner = QNG
		controller = QNG
		add_core = QNG
		remove_core = MCH
	   } # The Qing Dynasty
1657.1.1 = { capital = "Fengtianfu" } # Renamed
1700.1.1 = { citysize = 147655 }
1750.1.1 = { citysize = 150105 tax_assessor = yes }
1800.1.1 = { citysize = 152400 }