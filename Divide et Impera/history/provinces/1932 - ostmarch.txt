#1770 - Ostmarch

owner = HAB
controller = HAB
add_core = HAB
culture = austrian
religion = catholic
base_tax = 4
trade_goods = wine 
manpower = 1
fort1 = yes
capital = "Krems" 
citysize = 1800
hre = yes
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = ordu

1450.1.1  = { citysize = 2000 }
1477.1.1 = {	owner = HUN
		controller = HUN
		add_core = HUN	discovered_by = ordu
	   } #Matthias Corvinus
1490.4.26 = {	owner = HAB
		controller = HAB
		remove_core = HUN
	   } # Matthias Corvinus dies in Vienna
1500.1.1  = { regimental_camp = yes }
1500.1.1  = { citysize = 2600 }
1526.1.1  = { courthouse = yes }
1535.1.1  = { fort2 = yes }
1540.1.1  = { citysize = 4000 constable = yes }
1545.1.1  = { manpower = 2 }
1550.1.1  = { marketplace = yes }
1600.1.1  = { citysize = 5800 base_tax = 4 }
1620.1.1  = { tax_assessor = yes }
1650.1.1  = { manpower = 3 }
1650.1.1  = { citysize = 7000 }
1700.1.1  = { citysize = 9000 }
1700.1.1  = { manpower = 5 }
1750.1.1  = { citysize = 12000 }
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
