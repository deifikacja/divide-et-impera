
#428 - Ajam

owner = JAL
controller = JAL
culture = persian	
religion = shiite
capital = "Teheran"
trade_goods = cloth
hre = no
base_tax = 8
manpower = 4
citysize = 29530
fort1 = yes
add_core = JAL
add_core = QAR
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = TUR
discovered_by = TIM
discovered_by = SHY
discovered_by = MUG
discovered_by = ordu

1380.1.1 = { owner = TIM controller = TIM add_core = TIM remove_core = JAL }
1405.2.18 = { add_core = HER }
1447.3.12  = {   owner = HER
		controller = HER
		remove_core = TIM
	    }#Shahrokh's death - partition of the Empire
1449.1.1  = { controller = QAR owner = QAR remove_core = HER }
1450.1.1  = { citysize = 31945 }
1458.9.1  = { revolt = { type = revolutionary_rebels size = 1 } controller = REB } # Civil war in Qara Quyunlu
1458.12.1 = { revolt = {} controller = QAR }
1468.2.1  = {	owner = TIM
		controller = TIM
		add_core = TIM
		remove_core = QAR
	    } # Taken over by the Timurids
1469.1.1  = {	owner = AKK
		controller = AKK
		add_core = AKK
	    }
1500.1.1  = { citysize = 34350 marketplace = yes }
1504.1.1  = {	owner = PER
		controller = PER
		add_core = PER
		remove_core = AKK
		remove_core = TIM
	    } # The Safavids took over
1550.1.1  = { citysize = 42099 discovered_by = TUR }
1600.1.1  = { citysize = 54750 }
1650.1.1  = { citysize = 87335 }
1675.12.1 = { base_tax = 6 } # Political fragmentation
1700.1.1  = { citysize = 118660 }
1747.1.1  = { revolt_risk = 3 } # Shah Nadir is killed, local khanates emerged
1748.1.1  = { revolt_risk = 4 } # The empire began to decline
1750.1.1  = { citysize = 122780 }
1779.1.1  = { revolt_risk = 0 } # With the Qajar dynasty the situation stabilized
1800.1.1  = { citysize = 141200 }