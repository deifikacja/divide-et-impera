#305 - Simbirsk
       
culture = volgaic
religion = shamanism
hre = no
base_tax = 2
trade_goods= iron
manpower = 1
native_size = 15
native_ferocity = 1
native_hostileness = 1
capital = "Kurmysh"
add_core = GOL
discovered_by = ordu
discovered_by = CRI
discovered_by = KAZ
discovered_by = GOL
discovered_by = AST
discovered_by = NOG
discovered_by = BSH
discovered_by = QAS
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western

1356.1.1  = { add_core = MOS }
1425.1.1 = { citysize = 2700  owner = MOS controller = MOS }
1438.1.1 = { owner = KAZ controller = KAZ add_core = KAZ remove_core = GOL fort1 = yes }
1450.1.1  = { citysize = 3600 }
1500.1.1  = { citysize = 3949 }
1503.3.22  = {	owner = RUS
		controller = RUS
		add_core = RUS
		remove_core = MOS
	     }
1550.1.1  = { citysize = 4281 } 
1600.1.1  = { citysize = 4542 }
1648.1.1  = { remove_core = KAZ
		culture = russian	
		religion = orthodox	
	    } # Founded as a strongpoint to defend Russia's southern front
1650.1.1  = { citysize = 4855 marketplace = yes } # Developed as a trade center
1652.1.1  = { fort1 = yes } # Designed to protect the eastern edge of the Russian empire
1670.1.1  = { revolt = { type = nationalist_rebels size = 2 leader = "Stenka Razin" } controller = REB } # Taken by Stepan Razin
1671.1.1  = { revolt = {} controller = RUS } # Razin is defeated in Simbirsk
1700.1.1  = { citysize = 7948 constable = yes }
1750.1.1  = { citysize = 9350 }
1773.1.1  = { revolt = { type = nationalist_rebels size = 0 } controller = REB } # Emelian Pugachev, Cossack insurrection
1774.9.14 = { revolt = {} controller = RUS } # Pugachev is captured
1800.1.1  = { citysize = 11000 }