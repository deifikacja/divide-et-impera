#709 - Ningxia

owner = YUA
controller = YUA
culture = mongol
religion = confucianism
capital = "Yinchuan"
trade_goods = wool
hre = no
base_tax = 3
manpower = 1
citysize = 10570
add_core = YUA	discovered_by = ordu
add_core = MNG
add_core = SNG
add_core = QIN
fort1 = yes
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = ordu


1370.1.1 = { owner = MNG controller = MNG add_core = MNG remove_core = YUA }
1450.1.1 = { citysize = 10570 }
1500.1.1 = { citysize = 12260 base_tax = 4 } # Repairs of the Great Wall
1550.1.1 = { citysize = 14115 }
1600.1.1 = { citysize = 16188 }
1644.1.1 = { controller = MCH }
1646.1.1 = {	owner = MCH
		add_core = MCH
		remove_core = MNG
	   } # The Qing Dynasty
1650.1.1 = { citysize = 17850 }
1683.1.1 = {	owner = QNG
		controller = QNG
		add_core = QNG
		remove_core = MCH
	   } # The Qing Dynasty
1700.1.1 = { citysize = 19120 }
1750.1.1 = { citysize = 21555 }
1800.1.1 = { citysize = 23800 }