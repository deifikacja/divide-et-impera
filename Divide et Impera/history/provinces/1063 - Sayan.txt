#1063 - Sayan

owner = CHG
controller = CHG
add_core = KHK
culture = khakas
religion = animism
capital = "Sayan"
trade_goods = fur
hre = no
base_tax = 1
manpower = 1
citysize = 1000
discovered_by = CHG
discovered_by = KHK
discovered_by = ordu

1368.1.22 = { owner = KHK controller = KHK }
1623.1.1 = { discovered_by = RUS }
1727.8.20 = {	owner = RUS
		controller = RUS
		citysize = 400
		add_core = RUS
		capital = "Abakan"
	   } # Part of Russia
1800.1.1 = { citysize = 850 }