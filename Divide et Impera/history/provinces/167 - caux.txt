# 167 Caux - Principal cities: Rouen

owner = FRA
controller = FRA
capital = "Rouen"
citysize = 29000
culture = normand
religion = catholic
hre = no
base_tax = 5
trade_goods = cloth
manpower = 1
add_core = FRA
add_core = ENG
add_core = NRM
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
fort1 = yes
marketplace = yes
temple = yes # La Notre Dame de Rouen

1356.1.1   = { controller = ENG }
1356.1.1   = { revolt_risk = 5 }
1358.2.22  = { revolt_risk = 7 } #Etienne Marcel in Paris
1358.7.31  = { revolt_risk = 3 } #death of Etienne Marcel
1360.1.1   = { revolt_risk = 5 }
1364.1.1   = { revolt_risk = 0 }
1360.5.8 = { owner = FRA controller = FRA }
1419.1.19  = { controller = ENG } # Incorporated into England by Henry V
1420.5.12  = { owner = ENG add_core = ENG } # Treaty of Troyes
1449.1.1   = { owner = FRA controller = FRA } # Retaken by France
1450.1.1   = { citysize = 31000 }
1465.8.15  = { owner = NRM controller = NRM } # Treaty of Conflans
1466.3.6   = { owner = FRA controller = FRA } # Louis XI reincorporates Normandy into the French crown
1467.6.15  = { add_core = BUR } # Charles the Bold ascends to the throne and lays claims
1471.1.1   = { controller = BUR } # Charles the Bold invades France and ravages the country as far as Rouen
1471.8.1   = { controller = FRA } # Charles the Bold has to retire
1475.8.29  = { remove_core = ENG } # Treaty of Picquigny
1477.1.5   = { remove_core = BUR } # Charles the Bold dies at Nancy
1500.1.1   = { citysize = 42000 }
1525.1.1   = { fort2 = yes }
1550.1.1   = { citysize = 69000 courthouse = yes shipyard = yes }
1557.1.1   = { religion = reformed }
1565.1.1   = { revolt_risk = 8 } # France is restless once again as ultra-catholic intentions become clear
1568.9.1   = { revolt_risk = 15 } # Catherine de Medici and Charles IX side with the Guise faction, religious intolerance peaks
1570.1.1   = { constable = yes }
1570.8.8   = { revolt_risk = 10 } # Edict of Saint-Germain: temporary pacification
1573.9.1   = { revolt_risk = 15 } # Saint Barthelew's Day Massacre: the consequences in the land
1574.5.1   = { revolt_risk = 7 } # Charles IX dies, situation cools a bit	
1584.1.1   = { revolt_risk = 12 } # Situation heats up again
1588.12.1  = { revolt_risk = 15 } # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1590.1.1   = { customs_house = yes } # Henri IV's quest to eliminate corruption and establish state control
1594.1.1   = { revolt_risk = 5 } # 'Paris vaut bien une messe!', Henri converts to Catholicism
1598.4.13  = { revolt_risk = 3 } # Edict of Nantes, alot more freedom to the protestants
1598.5.2   = { revolt_risk = 0 } # Peace of Vervins, formal end to the Wars of Religion
1600.1.1   = { citysize = 66000 regimental_camp = yes }
1625.1.1   = { fort3 = yes }
1631.1.1   = { revolt_risk = 5 } # Region is rebellious until about 1639
1641.1.1   = { revolt_risk = 0 }
1650.1.1   = { citysize = 90000 }
1660.1.1   = { fort4 = yes }
1670.1.1   = { tax_assessor = yes }
1680.1.1   = { fort5 = yes } # Vauban's 'pointed' fort in Dieppe
1700.1.1   = { citysize = 73000 workshop = yes decision = state_controlled_guild }
1750.1.1   = { citysize = 81000 }
1789.7.14  = {	owner = RFR
		controller = RFR
		add_core = RFR
		remove_core = FRA
	     } # The storming of the Bastille
1800.1.1   = { citysize = 90000 }
1814.4.11  = {	owner = FRA
		controller = FRA
		add_core = FRA
		remove_core = RFR
	     } # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1815.3.20  = {	owner = RFR
		controller = RFR
		add_core = RFR
		remove_core = FRA
	     } # Napoleon enters Paris
1815.7.8   = {	owner = FRA
		controller = FRA
		add_core = FRA
		remove_core = RFR
	     } # The French monarchy is restored
