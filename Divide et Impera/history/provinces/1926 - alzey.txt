#1761 - Alzey

capital = "Worms"
culture = rheinlaender
religion = catholic
trade_goods = wine
owner = WRM
base_tax = 4
manpower = 1
fort1 = yes
citysize = 10000
controller = WRM
add_core = WRM
hre = yes
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1450.1.1   = { citysize = 12000 }
1495.1.1   = { courthouse = yes }
1500.1.1   = { constable = yes } # The Reichskammergericht (1495-1806) is the highest court in the HRE situated in Worms and after 1527 Speyer
1546.4.19  = { religion = protestant } # #Friedrich II converts the coutnry to protestant
1590.1.1   = { regimental_camp = yes }
1600.1.1   = { citysize = 22000 }
1620.1.1   = { fort2 = yes customs_house = yes }
1630.1.1   = { fort3 = yes }
1648.1.1   = { citysize = 15000 }
1689.1.1   = { fort3 = no }
1689.8.1   = { base_tax = 5 fort2 = no } # French troops burn, pillage and destroy in the succession wars.
1700.1.1   = { citysize = 20000 fort2 = yes }
1750.1.1   = { citysize = 24000 fort3 = yes }
1801.1.1   = { owner = RFR controller = RFR add_core = RFR }
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1814.4.11  = { owner = HES controller = HES remove_core = RFR }
