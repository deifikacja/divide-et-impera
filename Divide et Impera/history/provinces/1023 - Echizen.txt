#1023 - Echizen - home of the Shiba clan

owner = SHB
controller = SHB
culture = japanese
discovered_by = ordu
religion = shinto
capital = "Takaoka"
trade_goods = chinaware
hre = no
base_tax = 5
manpower = 1
citysize = 11050
add_core = SHB
add_core = ASA
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM

1450.1.1 = { citysize = 12700 }
1470.1.1 = { owner = ASA controller = ASA } #The Asakura replace Shiba as lords of Echizen
1477.1.1 = { revolt_risk = 5 }#The Ikko-Ikki
1488.1.1 = { revolt = { type = religious_rebels leader = "Rennyo" size = 1} controller = REB }
1500.1.1 = { citysize = 13148 }
1542.1.1 = { discovered_by = POR }
1550.1.1 = { citysize = 14333 }
1573.1.1 = { revolt = {} owner = ODA controller = ODA add_core = ODA revolt_risk = 0 }#Oda destroys Ikko-Ikki
1582.6.21 = { owner = TOY controller = TOY add_core = TOY }#Oda Nobunanga's succesor
1600.10.22 = { citysize = 15678 owner = JAP controller = JAP add_core = JAP }#Sekahigara
1600.12.1 = { citysize = 15678 }
1650.1.1 = { citysize = 17543 }
1700.1.1 = { citysize = 18150 }
1750.1.1 = { citysize = 19877 }
1800.1.1 = { citysize = 22140 }