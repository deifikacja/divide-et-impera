owner = KOL
controller = KOL
add_core = KOL
culture = rheinlaender
religion = catholic
trade_goods = grain
capital = "K�ln"
base_tax = 6
manpower = 1
citysize = 40000
marketplace = yes
fort1 = yes
customs_house = yes
hre = yes
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1450.1.1  = { citysize = 45500 }
1466.1.1  = { workshop = yes } # First Printer opens in K�ln
1500.1.1  = { citysize = 50000 regimental_camp = yes }
1520.1.1  = { fort2 = yes constable = yes }
1553.1.1  = { tax_assessor = yes } # Stock Exchange Founded
1600.1.1  = { citysize = 55000 }
1638.1.1  = { weapons = yes base_tax = 10 fort3 = yes } # K�ln manages to stay neutral in the 30 years war and prospers through weapon sales. 
1700.1.1  = { citysize = 56000 }
1716.1.1  = { refinery = yes weapons = no base_tax = 12 } # Farnia begins exporting "Eau de Cologne" 
1750.1.1  = { war_college = yes }
1798.1.1  = { controller = RFR }
1800.1.1  = { citysize = 58000 }
1801.2.9  = {	war_college = no
		owner = RFR
		add_core = RFR
		hre = no
	    } # Treaty of Lun�ville
1814.4.11 = {	owner = FRA
		controller = FRA
		add_core = FRA
		remove_core = RFR
	    } # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1815.3.20 = {	owner = RFR
		controller = RFR
		add_core = RFR
		remove_core = FRA
	    } # Napoleon enters Paris
1815.6.9  = {	owner = PRU
		controller = PRU
		add_core = PRU
	   	remove_core = RFR
	    } # Congress of Vienna