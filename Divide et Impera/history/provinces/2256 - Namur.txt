# 1866 -- Namur

owner = NMR
controller = NMR
capital = "Namur"
citysize = 17000
culture = wallonian
religion = catholic
hre = yes
base_tax = 7
trade_goods = iron
manpower = 2
add_core = NMR
marketplace = yes
courthouse = yes
fort1 = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = ottoman

1421.1.1 = { owner = BUR controller = BUR add_core = BUR }#Sold to Burgundy
1450.1.1   = { citysize = 20000 }
1465.1.1   = { revolt_risk = 4 } # Revolt imminent
1465.4.22  = { revolt_risk = 8 } # Citizens revolt
1465.10.19 = { revolt_risk = 0 } # Peace is restored
1477.1.5   = { owner = HAB controller = HAB add_core = HAB remove_core = BUR } # Charles the Bold dies, Lowlands to Austria
1500.1.1   = { citysize = 24000 constable = yes }
1518.1.1   = { fort2 = yes }
1530.1.1   = { temple = yes } # Saint Paul's Cathedral finished
1550.1.1   = { citysize = 31000 }
1600.1.1   = { citysize = 38000 }
1630.1.1   = { workshop = yes }
1650.1.1   = { fort3 = yes citysize = 46000 }
#1690.1.1   = { customs_house = yes }
1700.1.1   = { citysize = 57000 }
1715.1.1   = { fort4 = yes }
1750.1.1   = { tax_assessor = yes citysize = 70500 }
1797.12.26 = {	owner = RFR
		controller = RFR
		add_core = RFR
	     } # Treaty of Campo Formio
1800.1.1   = { citysize = 71000 }
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1814.4.11  = {	owner = FRA
		controller = FRA
		add_core = FRA
		remove_core = RFR
	     } # Treaty of Fontainebleu, Napoleon abdicates unconditionally
1815.3.16  = {	owner = NED
		controller = NED
		add_core = NED
		remove_core = FRA
	     } # The United Kingdom of the Netherlands
