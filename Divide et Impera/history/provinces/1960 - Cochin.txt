#534 - Calicut

owner = COC #Cochin
controller = COC
culture = malayalam
religion = hinduism
capital = "Mahodayapuram"
trade_goods = tea
hre = no
cot = yes
base_tax = 5
manpower = 1
citysize = 4760
add_core = COC
fort1 = yes
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1405.1.1 = { capital = "Cochin" }
1450.1.1 = { citysize = 4100 }
1498.1.1 = { discovered_by = POR }
1500.1.1 = { citysize = 5600 }
1503.9.27 = { add_core = POR }#Portuguese influence
1550.1.1 = { citysize = 6380 }
1570.1.1 = { revolt_risk = 0 }
1600.1.1 = { citysize = 6975 }
1650.1.1 = { citysize = 7850 }
1663.1.7 = { add_core = NED remove_core = POR }#Dutch influence
1700.1.1 = { citysize = 9630 }
1750.1.1 = { citysize = 11758 }
1795.1.1 = { add_core = GBR remove_core = NED }#British influence
1800.1.1 = { citysize = 14205 }