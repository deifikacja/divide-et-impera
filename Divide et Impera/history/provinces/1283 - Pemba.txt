#1283 - Pemba

add_core = PEM
culture = swahili
religion = sunni
capital = "Chake Chake"
native_size = 50
native_ferocity = 4.5
native_hostileness = 9
manpower = 1
trade_goods = unknown
hre = no
base_tax = 2

discovered_by = ADA
discovered_by = ZZA
discovered_by = MAQ
discovered_by = ZIM
discovered_by = KIL
discovered_by = PTE
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1499.1.1 = { discovered_by = POR } 
1550.1.1 = { citysize = 3000 owner = PEM controller = PEM discovered_by = PEM trade_goods = grain } #Sultanate etablished
1606.1.1 = { owner = MLI controller = MLI } #Conquered by Malindi
1650.1.1 = { owner = POR controller = POR } #Conquered by Potrugal
1698.1.1 = { owner = OMA controller = OMA } #Conquered by Oman
1700.1.1 = { citysize = 4000 }
1750.1.1 = { owner = PEM controller = PEM } #Re-created as Omani vassal
1800.1.1 = { citysize = 4500 }