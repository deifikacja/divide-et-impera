# 66 - Bamberg
owner       = BMB
controller  = BMB
add_core    = BMB
culture     = bavarian
religion    = catholic
base_tax    = 3
trade_goods = wine
manpower    = 1
fort1       = yes
capital     = "Bamberg"
citysize    = 7000
hre           	= 	yes
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1450.1.1  = { citysize = 7000 }
1525.1.1    = { customs_house = yes }
1540.1.1    = { marketplace = yes }
1550.1.1    = { citysize = 12000 }
1580.1.1    = { constable = yes }
1590.1.1    = { temple = yes }
1600.1.1    = { citysize = 14000 }
1620.1.1    = { constable = yes }
1647.1.1    = { university = yes } # (existed until 1803)
1650.1.1    = { citysize = 8000 }
1700.1.1    = { citysize = 12000 }
1720.1.1    = { courthouse = yes }
1750.1.1    = { citysize = 14000 }
1800.1.1  = { citysize = 15000 }
1803.2.25  = {	owner = BAV
		controller = BAV
		add_core = BAV
		remove_core = BMB
	    } # Annexed by Bavaria
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved