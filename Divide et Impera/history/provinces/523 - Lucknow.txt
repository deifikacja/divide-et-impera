#523 - Lucknow

owner = DLH
controller = DLH
culture = kanauji
religion = hinduism
capital = "Lucknow"
trade_goods = cloth
hre = no
base_tax = 7
manpower = 2
citysize = 52000
add_core = DLH
fort1 = yes
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = MUG

1388.9.20 = { revolt_risk = 4 } #death of Firuz Shah, dynasty crisis began
1389.2.19 = { revolt_risk = 8 } #Tughluq Shah II is murdered in Delhi
1390.8.31 = { revolt_risk = 3 } #Muhammad Shah III enters Delhi
1392.6.1  = { revolt_risk = 9 } #chaos in sultanate
1394.1.1 = { owner = AHM controller = AHM add_core = AHM revolt_risk = 0 }
1450.1.1 = { citysize = 57600 }
1451.6.1 = { owner = DLH controller = DLH }
1483.1.1 = { remove_core = AHM }
1500.1.1 = { citysize = 53250 }
1526.1.1 = {	owner = MUG
		controller = MUG
		add_core = MUG
		remove_core = DLH
	   } # Battle of Panipat
1540.1.1  = { owner = SRI controller = SRI add_core = SRI } # Sher Shah takes over
1550.1.1 = { citysize = 62340 }
1556.11.5  = {	owner = MUG
		controller = MUG
		remove_core = SRI
	   } # Sher Shah Suri's death, the Bengali empire crumbeled
1600.1.1 = { citysize = 95000 }
1650.1.1 = { citysize = 78570 }
1700.1.1 = { citysize = 86780 }
1722.1.1 = {	owner = ODH
		controller = ODH
		add_core = ODH
		remove_core = MUG
	   } # Foundation of the Oudh dynasty
1750.1.1 = { citysize = 90805 }
1800.1.1 = { citysize = 105000 }