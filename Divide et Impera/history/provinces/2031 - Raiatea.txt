#2031 - Raiatea

owner = RAI
controller = RAI
add_core = RAI
culture = tahitan
religion = animism
capital = "Uturoa"
trade_goods = fish
hre = no
base_tax = 1
manpower = 1
citysize = 1000
discovered_by = TAH
discovered_by = BBR
discovered_by = RAI
discovered_by = HUI

1606.1.1 = { discovered_by = SPA }