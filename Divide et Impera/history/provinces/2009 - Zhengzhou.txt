#688 - Henan

owner = YUA
controller = YUA
culture = chihan
religion = confucianism
capital = "Zhengzhou"
trade_goods = cattle
hre = no
base_tax = 4
manpower = 2
citysize = 29350
add_core = YUA	
discovered_by = ordu
add_core = MNG
add_core = SNG
add_core = QIN
fort1 = yes
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = YUA

1365.1.1 = { revolt = { type = nationalist_rebels size = 2 } controller = REB }#The Ming didn't send army until 1381
1368.1.22 = { revolt = { } owner = MNG controller = MNG remove_core = YUA }
1450.1.1  = { citysize = 31978 }
1500.1.1  = { citysize = 34122 }
1530.1.1  = { marketplace = yes }
1550.1.1  = { citysize = 36808 }
1600.1.1  = { citysize = 38245 }
1630.1.1  = { revolt_risk = 6 } # The Li Zicheng rebellion
1644.1.1  = { controller = MCH }
1646.1.1  = {	owner = MCH
		add_core = MCH
		remove_core = MNG
	   } # The Qing Dynasty
1645.5.27 = { revolt_risk = 0 } # The rebellion is defeated
1648.1.1  = { constable = yes }
1650.1.1  = { citysize = 40253 }
1683.1.1 = {	owner = QNG
		controller = QNG
		add_core = QNG
		remove_core = MCH
	   } # The Qing Dynasty
1700.1.1  = { citysize = 42300 }
1745.1.1  = { tax_assessor = yes }
1750.1.1  = { citysize = 44115 }
1800.1.1  = { citysize = 46388 }