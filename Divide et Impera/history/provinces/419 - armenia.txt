#419 - Armenia

owner = TAB
controller = TAB
culture = armenian
religion = eastern_churches
capital = "Yerevan"
trade_goods = cattle
hre = no
base_tax = 4
manpower = 4
citysize = 2730
add_core = TAB
add_core = ARM 
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = TIM
discovered_by = SHY
discovered_by = MUG
discovered_by = ordu

1357.1.1 = { controller = GOL remove_core = TAB }
1358.1.1 = { owner = JAL controller = JAL }
1380.1.1 = { owner = TIM controller = TIM add_core = QAR }
1408.1.1   = { owner = QAR controller = QAR add_core = QAR }
1469.1.1 = {	owner = AKK
		controller = AKK
		add_core = AKK
		remove_core = QAR
	   }
1500.1.1   = { citysize = 3180 }
1501.1.1   = { owner = PER
	       controller = PER
	       add_core = PER
	       remove_core = AKK
	     } # The Safavids take over
1550.1.1   = { citysize = 4680 }
1554.1.1   = { controller = TUR } # Wartime occupation
1555.5.29  = { controller = PER } # Peace of Amasya
1583.8.15  = { controller = TUR } # Ottoman conquest
1590.3.21  = { owner = TUR add_core = TUR } # Peace of Istanbul
1600.1.1   = { citysize = 5711 }
1604.6.1   = { controller = PER } # Persian reconquest
1612.11.20 = { owner = PER } # Part of Persia
1639.5.17  = { remove_core = TUR } # Final division between Persia & Ottoman
1650.1.1   = { citysize = 7230 }
1700.1.1   = { citysize = 9680 constable = yes }
1722.1.1   = { owner = ARM
	     controller = ARM
	     add_core = ARM } # Rebellion against both the Persians & Ottomans
1725.1.1   = { tax_assessor = yes }
1730.1.1   = { owner = TUR
	       controller = TUR
	       add_core = TUR }
1735.1.1   = { owner = PER
	       controller = PER
	       add_core = PER
	     }
1747.1.1   = { owner = ARM
	       controller = ARM
	     }#Yerevan khanate
1750.1.1   = { citysize = 12447 }
1800.1.1   = { citysize = 15040 }