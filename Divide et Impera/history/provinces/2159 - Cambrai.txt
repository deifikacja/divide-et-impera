# 2159 Cambrésis - Principal cities: Cambrai

owner = CMB
controller = CMB
capital = "Cambrai"
citysize = 6000
culture = wallonian
religion = catholic
hre = yes
base_tax = 3
trade_goods = cloth
manpower = 1
add_core = CMB
marketplace = yes
temple = yes #The cathedral
fort1 = yes
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1450.1.1   = { citysize = 7000 }
1465.1.1   = { revolt_risk = 4 } # Revolt imminent
1465.4.22  = { revolt_risk = 8 } # Citizens revolt
1465.10.19 = { revolt_risk = 0 } # Peace is restored
1500.1.1   = { citysize = 8000 constable = yes }
1518.1.1   = { fort2 = yes }
1543.1.1 = { owner = SPA controller = SPA } # Taken by the Spanish
1550.1.1   = { citysize = 9000 }
1600.1.1   = { citysize = 10000 }
1650.1.1   = { fort3 = yes citysize = 12000 }
1677.4.19 = { controller = FRA } # Taken by the French
1678.9.19 = { owner = FRA remove_core = SPA hre = no } # Peace of the Nijmegen
1660.1.1   = { fort4 = yes }
1670.10.15 = { fort5 = yes } # Vauban's forts in the North finished, earlier than generic fort4, troops aimed at the Lowlands based there
1700.1.1   = { citysize = 13000 }
1750.1.1   = { fort6 = yes citysize = 14000 } # Expansion of the forts
1789.5.5   = { base_tax = 10 } # The General Estates
1789.7.14  = {	owner = RFR
		controller = RFR
		add_core = RFR
		remove_core = FRA
	     } # The storming of the Bastille
1800.1.1   = { citysize = 15200 }
1814.4.11  = {	owner = FRA
		controller = FRA
		add_core = FRA
		remove_core = RFR
	     } # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1815.3.20  = {	owner = RFR
		controller = RFR
		add_core = RFR
		remove_core = FRA
	     } # Napoleon enters Paris
1815.7.8   = {	owner = FRA
		controller = FRA
		add_core = FRA
		remove_core = RFR
	     } # The French monarchy is restored
