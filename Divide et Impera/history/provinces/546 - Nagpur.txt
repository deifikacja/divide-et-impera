#546 - Nagpur

owner = BAH
controller = BAH
culture = marathi
religion = hinduism
capital = "Nagpur"
trade_goods = copper
hre = no
base_tax = 4
manpower = 2
citysize = 23850
add_core = BAH
add_core = BRR
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = MUG

1450.1.1 = { citysize = 24703 }
1500.1.1 = { citysize = 27563 }
1518.1.1 = {	owner = BRR
		controller = BRR
	   } # The Breakup of the Bahmani sultanate
1550.1.1 = { citysize = 29780 }
1572.1.1 = {	owner = BAH
		controller = BAH
	   } # Invaded by Murtaza Nizam Shah	   
1600.1.1 = { citysize = 32300 }
1626.1.1 = {	owner = MUG
		controller = MUG 
		add_core = MUG
		remove_core = BAH
	   } # The Mughal Empire
1650.1.1 = { citysize = 35510 }
1700.1.1 = { citysize = 37670 }
1710.1.1 = { add_core = MAR }
1718.1.1 = {	owner = MAR
		controller = MAR
		remove_core = MUG
	   } # The territory is formally given to the Marathas
1734.1.1 = {	owner = BHO
		controller = BHO
	   	add_core = BHO
	   }
1750.1.1 = { citysize = 39000 }
1800.1.1 = { citysize = 42570 }