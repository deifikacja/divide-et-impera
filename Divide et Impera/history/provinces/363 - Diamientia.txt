#363 - Diamientia

owner = MAM
controller = MAM
culture = al_misr_arabic
religion = eastern_churches
capital = "Dumyat"
trade_goods = sugar
hre = no
base_tax = 5
manpower = 3
citysize = 5467
fort1 = yes
add_core = MAM
discovered_by = MKR
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ

1450.1.1   = { citysize = 5800 }
1450.1.1   = { citysize = 6210 }
1500.1.1   = { citysize = 6610 }
1517.1.1   = {	owner = TUR
		controller = TUR
		add_core = TUR
	     } # Conquered by Ottoman troops
1520.11.10 = { revolt = { type = nationalist_rebels size = 0 } controller = REB } # Mameluk Uprising under Canbirdi Ghazeli
1521.1.1   = { revolt = {} controller = TUR }
1550.1.1   = { citysize = 7542 }
1586.1.1   = { revolt_risk = 2 } # Beginning of oppositions
1589.1.1   = { revolt_risk = 4 }
1598.1.1   = { revolt_risk = 6 }
1600.1.1   = { citysize = 8716 }
1601.1.1   = { revolt_risk = 8 }
1604.1.1   = { revolt_risk = 10 }
1623.1.1   = { revolt_risk = 5 }
1624.1.1   = { revolt_risk = 2 }
1631.1.1   = { revolt_risk = 4 }
1632.1.1   = { revolt_risk = 0 }
1650.1.1   = { citysize = 7700 }
1695.1.1   = { revolt_risk = 3 } # Demonstrations against the Mamelukes
1700.1.1   = { citysize = 9241 marketplace = yes }
1724.1.1   = { revolt_risk = 3 } # Power struggle
1750.1.1   = { citysize = 9400 }
1769.1.1   = { owner = MAM controller = MAM } # Ali Bey's Rebellion
1772.1.1   = { owner = TUR controller = TUR }
1789.10.22 = { revolt_risk = 6 } # Introduction of house tax, strained relations 
1796.1.1   = { revolt = { type = nationalist_rebels size = 0 } controller = REB } # Revolts against the Ottomans
1798.8.3   = { revolt = {} controller = RFR } # Occupied by the French
1800.1.1   = { citysize = 9782 }
1802.5.13  = { controller = TUR revolt_risk = 8 } # Turkish rule is restored but a few troublesome years follow
1805.5.12  = { owner = MAM controller = MAM } # Egyptian rebellion
1811.6.1   = { owner = EGY controller = EGY add_core = EGY remove_core = MAM } # Citadel Massacre
