#1972 - Rajkot

owner = DLH
controller = DLH
culture = gujarati
religion = sufism
capital = "Rajkot"
trade_goods = cotton
hre = no
base_tax = 4
manpower = 3
citysize = 14311
add_core = GUJ
add_core = DLH
add_core = RJK
fort1 = yes
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1388.9.20 = { revolt_risk = 4 } #death of Firuz Shah, dynasty crisis began
1389.2.19 = { revolt_risk = 8 } #Tughluq Shah II is murdered in Delhi
1390.8.31 = { revolt_risk = 3 } #Muhammad Shah III enters Delhi
1392.6.1  = { revolt_risk = 9 } #chaos in sultanate
1393.1.1 = { revolt = { type = nationalist_rebels size = 0 } controller = REB }
1396.1.1 = { revolt = {} owner = GUJ controller = GUJ remove_core = DLH add_core = NAW }
1450.1.1 = { citysize = 15800 }
1498.1.1 = { discovered_by = POR }
1500.1.1 = { citysize = 16080 }
1540.1.1 = { owner = NAW controller = NAW }#Nawanagar state founded
1550.1.1 = { citysize = 17535 }
1600.1.1 = { citysize = 19540 discovered_by = TUR }
1620.1.1 = {	owner = RJK
		controller = RJK
		remove_core = MUG
	   } # Rajkot state founded
1650.1.1 = { citysize = 20354 }
1700.1.1 = { citysize = 21900 }
1720.1.1 = { owner = MUG controller = MUG capital = "Masumabad" }#conquered by nawab of Junagadh
1732.1.1 = { owner = RJK controller = RJK capital = "Rajkot" }#conquered by Thakore Sahib Ranmalji Mehramanji, Rajkot restored
1750.1.1 = { citysize = 23870 } 
1800.1.1 = { citysize = 25250 }
1818.1.1 = { add_core = GBR }#Protection