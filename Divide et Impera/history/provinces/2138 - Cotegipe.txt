#2138 - Cotegipe

culture = amazonian
religion = animism
capital = "Cotegipe"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 10
native_ferocity = 1
native_hostileness = 7

1820.1.1 = {	discovered_by = POR
		owner = POR
		controller = POR
		add_core = BRZ
		citysize = 460
		religion = catholic
		culture = portugese
		trade_goods = gold
	    } # Cotegipe is founded