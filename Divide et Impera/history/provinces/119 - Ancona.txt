#119 - Ancona

owner = ANC
controller = ANC
add_core = PAP
culture = umbrian 
religion = catholic 
hre = no 
base_tax = 5        
trade_goods = wool
manpower = 2        
fort1 = yes 
capital = "Ancona" 
citysize = 28000 # Estimated 
temple = yes
add_core = ANC
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
 
1450.1.1  = { citysize = 30000 }
1500.1.1 = { citysize = 35000 } 
1532.1.1 = { owner = PAP controller = PAP }
1550.1.1 = { citysize = 40000 } 
1600.1.1 = { citysize = 40000 } 
1650.1.1 = { citysize = 35000 } 
1700.1.1 = { citysize = 35000 marketplace = yes } 
1750.1.1 = { citysize = 35000 add_core = ITA } 
1797.11.19 = { owner = ANC controller = ANC }#Short-Lived Republic
1798.3.7 = { owner = PAP controller = PAP remove_core = ANC }#To Roman Republic??
1799.1.1 = { owner = HAB controller = HAB }
1800.1.1 = { citysize = 45000 }
1801.1.1 = {	
		controller = RFR
	     }
1802.1.1 = {	owner = PAP
		controller = PAP
	     }
1805.12.26 = {	owner = ITA
		controller = ITA
		add_core = ITA
	     	remove_core = PAP
	     } # Treaty of Pressburg
1814.4.11   = {	owner = PAP
		controller = PAP
	     } # Treaty of Fontainebleau, Napoleon abdicates unconditionally