#308 - Yaroslavl

owner = YAR
controller = YAR
culture = russian	
discovered_by = ordu
religion = orthodox
hre = no 
base_tax = 3
trade_goods = iron  
manpower = 2
capital = "Yaroslavl"
citysize = 5880
fort1 = yes
add_core = YAR
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western




discovered_by = PEL

discovered_by = CRI
discovered_by = KAZ
discovered_by = GOL
discovered_by = AST
discovered_by = NOG
discovered_by = BSH
discovered_by = QAS

1438.1.1  = { discovered_by = KAZ }
1450.1.1  = { citysize = 6400 }
1463.1.1  = {	owner = MOS
		controller = MOS
		add_core = MOS
		remove_core = YAR
		marketplace = yes
	    } # Incorporated into Muscovy
1500.1.1  = { citysize = 7543 }
1503.3.22 = {	owner = RUS
		controller = RUS
		add_core = RUS
		remove_core = MOS
	    }
1516.1.1  = { temple = yes } # Spasskiy Monastery
1550.1.1  = { citysize = 8874 }
1560.1.1  = { base_tax = 4 } # Treasury reforms
1598.1.1  = { revolt_risk = 5 manpower = 3 } # "Time of troubles", peasantry brought into serfdom
1600.1.1  = { citysize = 10440 }
1613.1.1  = { revolt_risk = 0 } # Order returned, Romanov dynasty
1650.1.1  = { citysize = 12283 }
1667.1.1  = { revolt = { type = nationalist_rebels size = 1 leader = "Stenka Razin" } controller = REB } # Peasant uprising, Stenka Razin
1670.1.1  = { revolt = {} controller = RUS } # Crushed by the Tsar's army
1682.1.1  = { constable = yes }
1700.1.1  = { textile = yes citysize = 14450 } # First large factories appeared in the Yaroslavl region, linen, silk
1711.1.1  = { base_tax = 5 tax_assessor = yes } # Governmental reforms and the absolutism
1750.1.1  = { citysize = 17000 }
1767.1.1  = { base_tax = 6 } # Legislative reforms 
1800.1.1  = { citysize = 20000 }