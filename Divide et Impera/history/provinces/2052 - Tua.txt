#2052 - Tua

add_core = TUA
culture = malay-polynesian
religion = animism
capital = "Malae"
trade_goods = fish
hre = no
base_tax = 1
manpower = 1
native_size = 50
native_ferocity = 2
native_hostileness = 9
discovered_by = UVE
discovered_by = SIG
discovered_by = TUA

1565.1.1 = { owner = TUA controller = TUA add_core = TUA citysize = 1000 trade_goods = fish }
1767.8.16 = { discovered_by = GBR }
