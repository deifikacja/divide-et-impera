#1150 - Zamfara

owner = ZAM
controller = ZAM
add_core = ZAM
culture = hausa
citysize = 8000
manpower = 2
religion = animism
capital = "Dutsi"
trade_goods = grain
hre = no
base_tax = 2
discovered_by = ZAM
discovered_by = SON
discovered_by = HAD
discovered_by = KAN
discovered_by = GOB
discovered_by = KTS
discovered_by = ZAR
discovered_by = FUT
discovered_by = OKO
discovered_by = OYO
discovered_by = BEN
discovered_by = NUP
discovered_by = IFE
discovered_by = BUS

1500.1.1 = { capital = "Birnin Zamfara" } #establishment of new capital
1600.1.1 = { citysize = 15000 }
1657.1.1 = { revolt_risk = 6 } #Katsina raids into Zamfara territory
1670.1.1 = { religion = sunni } #First Muslim ruler of Zamfara comes to power
1687.1.1 = { revolt_risk = 6 } #Tuareg Azbin raid Zamfara region
1700.1.1 = { citysize = 30000 }
1764.1.1 = { controller = GOB } #Gobir launches invasion of Zamfara
1790.1.1 = { revolt_risk = 3 } #Islamic revival led by Usman dan Fodio begins to challenge Hausa ruler legitimacy
1800.1.1 = { citysize = 32000 }
1804.1.1 = { controller = ZAM culture = fulani}
1804.1.1 = { discovered_by = SOK }