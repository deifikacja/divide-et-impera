#1022 - Kii

owner = HAK
controller = HAK
culture = japanese
discovered_by = ordu
religion = shinto
capital = "Wakayama"
trade_goods = grain
hre = no
base_tax = 3
manpower = 1
citysize = 8900
add_core = HAK
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM

1450.1.1 = { citysize = 9400 }
1467.1.1 = { revolt = {type = pretender_rebels size = 1 leader = "Hatakeyama Yoshinari" } controller = REB }
1467.5.1 = { revolt = {} controller = HAK }
1500.1.1 = { citysize = 10050 }
1504.1.1 = {owner = HOS controller = HOS }#Ruled by Hosokawa
1507.1.1 = {owner = HAK controller = HAK }
1542.1.1 = { discovered_by = POR }
1550.1.1 = { citysize = 11980 }
1568.1.1 = {owner = MIY controller = MIY }#Ruled by Miyoshi
1573.1.1 = {owner = HAK controller = HAK }
1586.1.1 = { owner = TOY controller = TOY add_core = TOY }
1600.10.22 = { citysize = 15678 owner = JAP controller = JAP add_core = JAP }#Sekahigara
1650.1.1 = { citysize = 14300 }
1700.1.1 = { citysize = 15405 }
1750.1.1 = { citysize = 17645 }
1800.1.1 = { citysize = 19880 }