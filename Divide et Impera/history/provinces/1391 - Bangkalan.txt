#628 - Surabaya

owner = BNN
controller = BNN
culture = sulawesi
religion = hinduism
capital = "Bangkalan"
trade_goods = cotton
hre = no
base_tax = 2
manpower = 1
citysize = 3450
add_core = BNN
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM

1450.1.1 = { citysize = 3620 }
1466.1.1 = { religion = sunni }
1531.1.1 = {
		citysize = 3900
	   }
1613.1.1 = { discovered_by = NED } # The Dutch arrived
1650.1.1 = { citysize = 4520 }
1680.1.1 = {	discovered_by = MTR
		add_core = MTR
		citysize = 5200
	   } # Mataram vassal states
1700.1.1 = { citysize = 4787 }
1705.1.1 = { remove_core = MTR }
1743.1.1 = { add_core = NED citysize = 5200 } # East India Companies
1750.1.1 = { citysize = 4840 }
1800.1.1 = { citysize = 5124 }