#1131 - Massina

owner = MAL
controller = MAL
culture = mali
citysize = 8000
manpower = 1
religion = animism
capital = "Walata"
trade_goods = grain
hre = no
base_tax = 5
add_core = MAL
discovered_by = MAL
discovered_by = SON
discovered_by = SOF
discovered_by = ASH
discovered_by = FUT
discovered_by = OUG
discovered_by = YAT
discovered_by = NUN

1453.1.1   = { revolt = { type = revolutionary_rebels size = 0 } controller = REB }
1460.1.1   = { revolt_risk = 3 citysize = 5000 } #Songhai raids devastate region
1466.1.1   = { revolt_risk = 3 } #Songhai raids devastate region
1471.1.1   = { owner = SON controller = SON add_core = SON } #Conquered by Sunni Ali Ber of Songhai
1475.1.1   = { citysize = 8000 } #Recovery under Songhai control
1481.1.1   = { revolt_risk = 3 citysize = 5000 } #Mossi Raid led by Nasere I devastates region
1525.1.1   = { citysize = 8000 } #Revival under Askiya Muhammad
1550.1.1   = { citysize = 9000 } #Era of Prosperity under Askiya Dawud
1575.1.1   = { citysize = 11000 } #Era of Prosperity under Askiya Dawud
1586.1.1   = { revolt = { type = pretender_rebels size = 0 } controller = REB } #Civil war between Al-Sadduk and Ishak
1588.1.1   = { controller = SON revolt_risk = 6 } #Ishak reconquers lands in revolt, uneasy on throne
1591.3.15 = { revolt = { type = revolutionary_rebels size = 0 } controller = REB } #Collapse of Songhai in wake of Tondibi
1593.1.1   = { discovered_by = MOR owner = MOR controller = MOR add_core = MOR citysize = 7000  } #Moroccans seize Macina
1600.1.1   = { citysize = 4000 capital = "Goundam" } #Shift in center of Masina to Niger Valley in wake of Moroccan invasions
1600.1.1   = { remove_core = MAL } #Collapse of Mali State
1642.1.1   = { remove_core = SON } #Collapse of last vestiges of unity among Songhai
1650.1.1   = { citysize = 5000 } 
1660.1.1   = { owner = SOF controller = SOF add_core = SOF } #Bambara conquers region
1685.1.1   = { revolt = { type = revolutionary_rebels size = 0 } controller = REB } #Bambara decline after death of Kaniadan Kulibali
1700.1.1   = { citysize = 6000 }  
1720.1.1   = { controller = SOF } #Mamia Kulibali restores Bambara authority in region
1750.1.1   = { citysize = 8000 }  
1756.1.1   = { revolt_risk = 9 } # Denkoro seizes power in wake of father Mamali's death, civil war
1766.1.1   = { revolt_risk = 0 } # Ngolo Diarra restores authortiy of Segu state, ends civil war
1819.1.1   = {	citysize = 0
		base_tax = 3
		native_ferocity = 4
		native_hostileness = 9
		trade_goods = unknown
		owner = XXX
		controller = XXX
		remove_core = SOF
	     } # The Massina Empire