#2163 - Ustyug

owner = ROS
controller = ROS
culture = russian	
discovered_by = ordu
religion = orthodox
hre = no 
base_tax = 2
trade_goods = fur
manpower = 1
capital = "Veliky Ustyug"
citysize = 1880
fort1 = yes
add_core = ROS
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ




discovered_by = PEL

discovered_by = CRI
discovered_by = KAZ
discovered_by = GOL
discovered_by = AST
discovered_by = NOG
discovered_by = BSH
discovered_by = QAS

1399.1.1  = {	owner = MOS
		controller = MOS
		add_core = MOS
		remove_core = ROS
	    } # Est
1438.1.1  = { discovered_by = KAZ }
1450.1.1  = { citysize = 2400 }
1500.1.1  = { citysize = 3543 }
1503.3.22 = {	owner = RUS
		controller = RUS
		add_core = RUS
		remove_core = MOS
	    }
1550.1.1  = { citysize = 2874 }
1560.1.1  = { base_tax = 3 } # Treasury reforms
1598.1.1  = { revolt_risk = 5 manpower = 1 } # "Time of troubles", peasantry brought into serfdom
1600.1.1  = { citysize = 3440 }
1613.1.1  = { revolt_risk = 0 } # Order returned, Romanov dynasty
1650.1.1  = { citysize = 4283 }
1667.1.1  = { revolt = { type = nationalist_rebels size = 1 } controller = REB } # Peasant uprising, Stenka Razin
1670.1.1  = { revolt = {} controller = RUS } # Crushed by the Tsar's army
1682.1.1  = { constable = yes }
1700.1.1  = { citysize = 6450 trade_goods = naval_supplies } # First large factories appeared in the Yaroslavl region, linen, silk
1711.1.1  = { tax_assessor = yes } # Governmental reforms and the absolutism
1750.1.1  = { citysize = 8500 }
1767.1.1  = { base_tax = 4 } # Legislative reforms 
1800.1.1  = { citysize = 11000 }