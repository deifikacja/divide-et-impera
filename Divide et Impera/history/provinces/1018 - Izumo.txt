#1018 - Izumo - Yamana possesion - Including Izumo and Hoki

owner = KYO
controller = KYO
culture = japanese
discovered_by = ordu
religion = shinto
capital = "Yonago"
trade_goods = gold
hre = no
base_tax = 5
manpower = 1
citysize = 17234
add_core = KYO
add_core = OUC
add_core = MRI
add_core = AMA
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
temple = yes # Izumo Shrine

1450.1.1   = { citysize = 5980 }
1450.1.1 = { owner = AMA controller = AMA }
1500.1.1 = { citysize = 18350 }
1542.1.1 = { discovered_by = POR }
1550.1.1 = { citysize = 19200 }
1566.1.1 = { owner = MRI controller = MRI }
1600.10.22 = { citysize = 20950 owner = JAP controller = JAP add_core = JAP }
1650.1.1 = { citysize = 21756 }
1693.1.1 = { trade_goods = fish } # Estimate, Japan stops silver export
1700.1.1 = { citysize = 22834 }
1750.1.1 = { citysize = 23348 }
1800.1.1 = { citysize = 24870 }
