#2039 - Nauru

culture = malay-polynesian
religion = animism
capital = "Yaren"
hre = no
trade_goods = unknown
manpower = 1
base_tax = 2
native_size = 5
native_ferocity = 0.5
native_hostileness = 9

1798.11.8 = { discovered_by = GBR } # John Fearn
