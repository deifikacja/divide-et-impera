#398 - Qawasim

owner = SUH
controller = SUH
culture = omani_arabic
religion = shiite
capital = "Qawasim"
trade_goods = wool
hre = no
base_tax = 2
manpower = 1
citysize = 480
add_core = SUH
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1450.1.1  = { citysize = 650 }
1480.1.1  = { discovered_by = TUR }
1488.1.1  = { discovered_by = POR } # P�ro da Covilh�
1488.1.1  = {	owner = NZW
		controller = NZW
	    } # Captured by Nizwa
1500.1.1 = { citysize = 590 discovered_by = TUR }
1514.1.1 = {	owner = POR
		controller = POR
		add_core = POR
	   } # Captured by the Portuguese
1520.1.1 = { fort1 = yes }
1550.1.1 = { citysize = 1150 }
1600.1.1 = { citysize = 1569 }
1624.1.1 = {	owner = OMA
		controller = OMA
		remove_core = POR
	   }
1650.1.1 = { citysize = 1980 }
1700.1.1 = { citysize = 2500 }
1750.1.1 = { citysize = 2788 }
1800.1.1 = { citysize = 3100 }