#812 - Chanchan

owner = CHM
controller = CHM
culture = chimuan
religion = animism
capital = "Chanchan"
trade_goods = luxury_goods # 12000 artisans alone, great center of art and craft
hre = no
base_tax = 5
manpower = 3
citysize = 15000
fort1 = yes
fort2 = yes # Powerful fortress
improved_cultivation = yes
add_core = CHM
discovered_by = INC
discovered_by = CHM
discovered_by = INC
discovered_by = CHM
discovered_by = HNV
discovered_by = MNO
discovered_by = PCN
discovered_by = QUT
discovered_by = CII
discovered_by = HNC
discovered_by = CAA
discovered_by = CRC
discovered_by = TRC
discovered_by = CNC
discovered_by = SCN

discovered_by = CNE
discovered_by = CNB
discovered_by = NZC
discovered_by = AYA
discovered_by = DIA
discovered_by = HUA
discovered_by = TIT
discovered_by = ATA

1440.1.1  = { citysize = 17100 }
1471.9.1  = {	owner = INC
		controller = INC
		add_core = INC
		citysize = 12000
                base_tax = 4
                fort2 = no
                improved_cultivation = no
	    }
1491.1.1  = { religion = inti }
1500.1.1  = { citysize = 12580 }
1533.8.29 = {	discovered_by = SPA
		owner = SPA
		controller = SPA
		religion = catholic
	    }# The death of Atahualpa
1534.1.1  = { capital = "Trujillo" } # Founded by Diego de Almagro
1537.1.1  = { revolt_risk = 8 } # Fighting broke out when Almagro seized Cuzco
1538.1.1  = { revolt_risk = 5 } # Almagro is defeated & executed
1541.1.1  = { revolt_risk = 6 } # Pizzaro is assassinated by supporters of Almagro, his brother assumed control
1548.1.1  = { revolt_risk = 0 } # Gonzalo Pizzaro is also executed & Spain succeeds in reasserting its authority
1550.1.1  = { citysize = 6960 culture = castillian }
1558.8.29 = { add_core = SPA }
1600.1.1  = { citysize = 7570 }
1650.1.1  = { citysize = 8700 }
1700.1.1  = { citysize = 9444 }
1750.1.1  = { citysize = 10322 add_core = PEU }
1800.1.1  = { citysize = 11810 }