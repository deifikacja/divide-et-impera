#1317 - Samoyeds

owner = NOV
controller = NOV
capital = "Pustozersk"
culture = samoyed
religion = shamanism
trade_goods = unknown
hre = no
base_tax = 1
manpower = 0
native_size = 5
native_ferocity = 2
native_hostileness = 3
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ

1499.1.1  = {	owner = MOS
		controller = MOS
		citysize = 120
		culture = russian
		religion = orthodox
		trade_goods = fur
	    }
1503.3.22 = {	owner = RUS
		controller = RUS
		add_core = RUS
		remove_core = MOS
	    }
1550.1.1 = { citysize = 150 }
1700.1.1 = { citysize = 275 }
1800.1.1 = { citysize = 450 }