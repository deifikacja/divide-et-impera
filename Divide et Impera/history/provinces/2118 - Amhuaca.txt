#2118 - Amhuaca

culture = panoa
religion = animism
capital = "Amhuaca"
trade_goods = unknown
hre = no
base_tax = 2
manpower = 1
native_size = 5
native_ferocity = 1
native_hostileness = 3

1542.1.1  = { discovered_by = SPA } # da Orellana
1750.1.1 = {	owner = SPA
		controller = SPA
		add_core = PEU
		capital = "Iquitos"
		citysize = 160
		culture = castillian
		religion = catholic
		trade_goods = naval_supplies
	    } # Jesuit missions
1800.1.1  = { citysize = 200 }