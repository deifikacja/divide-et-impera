#117 - Siena

owner = SIE
controller = SIE
culture = tuscan
religion = catholic 
hre = yes 
base_tax = 5        
trade_goods = cloth
manpower = 1        
fort1 = yes 
capital = "Siena" 
citysize = 15000 # Estimated 
university = yes # Founded 1240
add_core = SIE
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1450.1.1 = { citysize = 13000 }
1500.1.1 = { citysize = 15000 } 
1531.1.1 = { controller = SPA owner = SPA add_core = SPA }
1550.1.1 = { citysize = 10000 } 
1552.1.1 = { controller = FRA owner = FRA add_core = FRA }
1555.1.1 = { controller = SPA owner = SPA remove_core = FRA }
1557.1.1 = {	controller = TUS
		owner = TUS
		add_core = TUS
		remove_core = SPA
	   }
1600.1.1 = { citysize = 19000 marketplace = yes }
1648.5.15 = { hre = no } #Peace of Westphalia 
1650.1.1 = { citysize = 19000 } 
1700.1.1 = { citysize = 19000 } 
1750.1.1 = { citysize = 15000 add_core = ITA } 
1800.1.1 = { citysize = 16000 } 
1801.2.9   = {	owner = RFR
		controller = RFR
		add_core = RFR
	     } # The Treaty of LunÚville
1801.3.21  = {	owner = ETR
		controller = ETR
		add_core = ETR
	     } # The Kingdom of Etruria
1807.12.10 = {	owner = RFR
		controller = RFR
		remove_core = ETR
	     } # Etruria is annexed to France
1814.4.11   = {	owner = TUS
		controller = TUS
		remove_core = RFR
	     } # Treaty of Fontainebleu, Napoleon abdicates and Tuscany is restored
