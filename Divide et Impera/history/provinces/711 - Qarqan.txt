#711 - Qarqan

owner = TRF
controller = TRF
culture = mongol
religion = buddhism
capital = "Qarqan"
trade_goods = grain
hre = no
base_tax = 2
manpower = 1
citysize = 5670
add_core = TRF
discovered_by = TRF
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = ordu

1356.1.1 = { add_core = CHG }
1369.1.1 = { owner = CHG controller = CHG }
1450.1.1 = { citysize = 6795 }
1455.1.1 = { owner = TRF controller = TRF }
1500.1.1 = { citysize = 7124 religion = tibetan_buddhism }
1550.1.1 = { citysize = 8955 }
1600.1.1 = { citysize = 11257 }
1650.1.1 = { citysize = 13400 }
1700.1.1 = { citysize = 14815 }
1750.1.1 = { citysize = 16154 }
1682.1.1 = { owner = OIR controller = OIR add_core = OIR }
1759.1.1 = { owner = TRF controller = TRF add_core = QNG
	   } # Protectorate of China
1800.1.1 = { citysize = 17977 }