#1363 - Awaji - Hosokawa possesion

owner = HOS
controller = HOS
culture = japanese
religion = shinto
capital = "Mihara"
trade_goods = fish
hre = no
base_tax = 3
manpower = 1
citysize = 5000
fort1 = yes
add_core = HOS
add_core = MIY
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM

1450.1.1   = { citysize = 4980 }
1470.1.1   = { owner = MIY controller = MIY }
1500.1.1   = { citysize = 9870 }
1542.1.1   = { discovered_by = POR }
1550.1.1   = { citysize = 1240 }
1550.1.1   = { owner = CHO controller = CHO }
1582.6.22  = { owner = TOY controller = TOY add_core = TOY }
1600.10.22 = { owner = JAP controller = JAP }
1600.1.1   = { citysize = 3200 }
1650.1.1   = { citysize = 5250 }
1700.1.1   = { citysize = 7600 }
1750.1.1   = { citysize = 9250 }
1800.1.1   = { citysize = 12000 }