#427 - Gurgan

owner = MZN
controller = MZN
culture = persian	
religion = sunni
capital = "Gorgan"
trade_goods = cattle
hre = no
base_tax = 3
manpower = 2
citysize = 1950
add_core = TBR
add_core = MZN
fort1 = yes
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = TUR
discovered_by = TIM
discovered_by = SHY
discovered_by = MUG
discovered_by = ordu

1357.1.1 = {	owner = TBR
		controller = TBR
		remove_core = JAL
	    }
1380.1.1 = { owner = TIM controller = TIM add_core = TIM }
1405.1.1 = { owner = TBR controller = TBR remove_core = TIM }
1450.1.1  = { citysize = 2540 }
1500.1.1  = { citysize = 2870 }
1525.1.1  = {	owner = PER
		controller = PER
		add_core = PER
		remove_core = AKK
		religion = shiite
	    } # The Safavids took over, Shi'ism became the state religion
1550.1.1  = { citysize = 3258 discovered_by = TUR }
1600.1.1  = { citysize = 4100 }
1650.1.1  = { citysize = 5200 }
1700.1.1  = { citysize = 6400 }
1715.12.1 = { base_tax = 4 } # Political fragmentation
1722.1.1 = {   controller = RUS
	     } # Russia invades Persia
1723.9.12 = {   owner = RUS
	     } # Treaty of St.Petersburg
1732.1.1 = {	owner = PER
		controller = PER
	     } # Anna Ivanovna returns persian lands
1747.1.1  = { revolt_risk = 3 } # Shah Nadir is killed, local khanates emerged
1748.1.1  = { revolt_risk = 4 } # The empire began to decline
1750.1.1  = { citysize = 9855 }
1779.1.1  = { revolt_risk = 0 } # With the Qajar dynasty the situation stabilized
1800.1.1  = { citysize = 11200 }