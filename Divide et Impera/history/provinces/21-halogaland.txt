#H�logaland, incl. Steig, V�gan, Trondenes, Andenes, Alstahaug

owner = NOR
controller = NOR
add_core = NOR
culture = norwegian
religion = catholic
hre = no
base_tax = 2
trade_goods = fish
manpower = 1
capital = "Steig"
citysize = 1050 # Estimated
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ

1400.1.1   = { citysize = 1150 }
1450.1.1   = { citysize = 1250 }
1500.1.1   = { citysize = 1350 }
1531.11.15 = { revolt_risk = 10 } # The Return of Christian II
1532.7.15  = { revolt_risk = 0 } # The Capture of Christian II
1536.1.1   = { religion = protestant} #Unknown date
1536.1.1   = { owner = DAN controller = DAN add_core = DAN } #'Handf�stningen'(Unknown date)
1550.1.1   = { citysize = 1580 }
1560.1.1   = { marketplace = yes }
1600.1.1   = { citysize = 1850 }
1650.1.1   = { citysize = 1980 }
1700.1.1   = { citysize = 2578 }
1750.1.1   = { citysize = 2900 }
1800.1.1   = { citysize = 4500 }
1814.1.14  = {	owner = SWE
		revolt = { type = nationalist_rebels size = 1 }
		controller = REB
		remove_core = DAN
	     } # Norway is ceded to Sweden following the Treaty of Kiel
1814.5.17 = { revolt = {} owner = NOR controller = NOR } # Norway declares itself independent and elects Christian Frederik as king