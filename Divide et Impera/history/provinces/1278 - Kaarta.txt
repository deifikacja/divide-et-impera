#1278 - Kaarta

owner = MAL
controller = MAL
culture = tuareg
citysize = 15000
manpower = 2
religion = sunni
capital = "Walata"
trade_goods = ivory
hre = no
base_tax = 5
add_core = MAL
add_core = KAA
discovered_by = MAL
discovered_by = SOF
discovered_by = ASH
discovered_by = SON
discovered_by = FZZ
discovered_by = TUG
discovered_by = FUT

1490.1.1 = { revolt_risk = 3 } #Fulani uprisings under Koly Tengella wreak havoc before moving down Senegal River
1500.1.1 = { citysize = 13500 }
1504.1.1 = { revolt_risk = 8 } #Koly Tengella launches new wave of Fulani incursions from base in Toro
1512.1.1 = { revolt_risk = 0 } #Koly Tengella killed, Fulani settle down among Jallonke under Malian leadership  
1590.1.1 = { citysize = 11000 base_tax = 4 manpower = 1 } #desert is growing
1600.1.1 = { owner = KAA controller = KAA discovered_by = KAA remove_core = MAL culture = bambara } #Collapse of the Mali state, power devolves to the Jallonke communities in the region
1650.1.1 = { citysize = 9160 }
1700.1.1 = { citysize = 7700 }
1800.1.1 = { citysize = 5600 }