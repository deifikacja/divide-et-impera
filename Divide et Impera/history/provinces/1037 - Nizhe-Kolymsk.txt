#1037 - Nizhe-Kolymsk

culture = chukchi
religion = shamanism
capital = "Nizhe-Kolymsk"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 5
native_ferocity = 1
native_hostileness = 3

1641.1.1 = { discovered_by = RUS owner = RUS controller = RUS add_core = RUS citysize = 100 trade_goods = fur } # Mikhail Stadukhin