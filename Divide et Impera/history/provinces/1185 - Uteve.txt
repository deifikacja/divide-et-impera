#1185 - Uteve

owner = GZI
controller = GZI
add_core = GZI
culture = shona
religion = animism
capital = "Massekesa"
citysize = 7000
manpower = 1
trade_goods = gold
hre = no
base_tax = 3
discovered_by = GZI
discovered_by = MAQ
discovered_by = ZZA

1495.1.1 = { owner = ZIM controller = ZIM add_core = ZIM discovered_by = ZIM revolt_risk = 2 }
1498.1.1 = { revolt_risk = 7 } #Changamire clans attempt to establish new base in region
1500.1.1 = { citysize = 7500 }
1503.1.1 = { revolt_risk = 0 } #Changamire expelled from region
1513.1.1 = { discovered_by = POR } #Travels of Antonio Fernandes in hinterland of Sofala
1573.1.1 = { revolt_risk = 8 } #Portuguese invasion of Mutapa via this region
1574.1.1 = { revolt_risk = 0 } #Rulers of Manyika grant Portuguese trade concessions
#1575.1.1 Portuguese establish trade fair at Massekesa
1600.1.1 = { citysize = 8200 }
1631.1.1 = { revolt_risk = 8 } #Kapararidze launches revolt to overthrow Mavura and end POR domination
1632.1.1 = { revolt_risk = 0 } #Kapararidze revolt defeated
1683.1.1 = { owner = XXX controller = XXX citysize = 0 trade_goods = unknown }