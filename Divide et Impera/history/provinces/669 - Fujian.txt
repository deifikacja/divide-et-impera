#669 - Fujian

owner = YUA
controller = YUA
culture = min
religion = confucianism
capital = "Fuzhon"
trade_goods = spices
hre = no
base_tax = 6
manpower = 2
citysize = 112240
add_core = YUA	
discovered_by = ordu
add_core = MNG
add_core = SNG
add_core = QIN
add_core = WUU
fort1 = yes
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = YUA

1356.1.1 = { revolt = { type = nationalist_rebels size = 3 leader = "Chen Youding" } controller = REB }#The Ming didn't send army until 1381
1368.1.22 = { revolt = { } owner = MNG controller = MNG remove_core = YUA }
1450.1.1 = { citysize = 115400 }
1500.1.1 = { citysize = 118700 }
1550.1.1 = { citysize = 120487 }
1600.1.1 = { citysize = 124580 }
1650.1.1 = { citysize = 130741 }
1662.1.1 = {	owner = MCH
		controller = MCH
		add_core = MCH
		remove_core = MNG
	   } # The Qing Dynasty
1673.1.1 = { revolt = { type = noble_rebels leader = "Jingzhong" size = 1 } controller = REB } # Rebellion of the Three Feudatories
1681.1.1 = { revolt = {} controller = MCH }
1683.1.1 = {	owner = QNG
		controller = QNG
		add_core = QNG
		remove_core = MCH
	   } # The Qing Dynasty
1700.1.1 = { citysize = 136488 }
1750.1.1 = { citysize = 140205 }
1800.1.1 = { citysize = 145800 }