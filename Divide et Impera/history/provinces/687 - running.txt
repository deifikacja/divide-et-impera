#687 - Running

owner = YUA
controller = YUA
culture = gan
religion = confucianism
capital = "Running"
trade_goods = cattle
hre = no
base_tax = 6
manpower = 1
citysize = 31500
add_core = YUA	
discovered_by = ordu
add_core = MNG
add_core = SNG
add_core = QIN
fort1 = yes
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = YUA

1365.1.1 = { revolt = { type = nationalist_rebels size = 2 } controller = REB }#The Ming didn't send army until 1381
1368.1.22 = { revolt = { } owner = MNG controller = MNG remove_core = YUA }
1450.1.1 = { citysize = 31500 }
1500.1.1 = { citysize = 33157 }
1520.1.1 = { marketplace = yes }
1550.1.1 = { citysize = 35000 }
1600.1.1 = { citysize = 36540 }
1640.1.1 = { revolt = { type = anti_tax_rebels size = 2 } controller = REB } # Financhial crisis
1641.1.1 = { revolt = {} controller = MNG } 
1644.1.1 = { controller = MCH }
1646.1.1 = {	owner = MCH
		add_core = MCH
		remove_core = MNG
	   } # The Qing Dynasty
1650.1.1 = { citysize = 38570 }
1683.1.1 = {	owner = QNG
		controller = QNG
		add_core = QNG
		remove_core = MCH
	   } # The Qing Dynasty
1700.1.1 = { citysize = 40100 }
1750.1.1 = { citysize = 41988 }
1800.1.1 = { citysize = 43250 }