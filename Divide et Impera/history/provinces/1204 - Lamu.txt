#1204 - Lamu

owner = MAQ
controller = MAQ
add_core = MAQ
culture = swahili
religion = sunni
capital = "Lamu"
citysize = 5000
manpower = 1
trade_goods = ivory
hre = no
base_tax = 2
discovered_by = KIL
discovered_by = ADA
discovered_by = ZZA
discovered_by = MAQ
discovered_by = ZIM
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1450.1.1 = { citysize = 5600 }
1499.1.1 = { discovered_by = POR } 
1543.1.1 = { revolt_risk = 4 } #Turkish Corsairs raid ports
1544.1.1 = { revolt_risk = 0 }
1550.1.1 = { citysize = 6500 discovered_by = TUR }
1585.1.1 = { revolt_risk = 4 } #Mir Ali Bey's Raids destabilize region
1586.1.1 = {	owner = TUR
		controller = TUR
	   } #Ottomans establish control over Lamu as base for further actions in East Africa
1588.1.1 = {	owner = MAQ
		controller = MAQ
	   } #Portuguese reprisals in wake of support shown to Mir Ali Bey
1600.1.1 = { citysize = 7000 }
1603.1.1 = { revolt_risk = 7 } #Portuguese reprisals in wake of Anti-Portuguese activity
1604.1.1 = { revolt_risk = 0 }
#1633.1.1 - Portuguese establish tradepost, highly resented by Lamu
1635.1.1 = { revolt_risk = 7 } #Yusuf Ibn Hasan attempts new rebellion based in Lamu
1637.1.1 = { owner = POR controller = POR add_core = POR revolt_risk = 3 } #Portuguese crush Yusuf's rebellion
1638.1.1 = { revolt_risk = 0 }
1650.1.1 = { citysize = 7500 }
1689.1.1 = { owner = MAQ controller = MAQ remove_core = POR } #Omanis drive Portuguese out of Lamu, establish pro-Omani leaders
1700.1.1 = { discovered_by = OMA citysize = 8000 }
1728.1.1 = {	owner = POR
		controller = POR
	   } #Lamu rulers rebel against Oman, gain Portuguese protection
1729.7.1 = { owner = OMA controller = OMA add_core = OMA } #Omani forces restore control of region, impose governor
1750.1.1 = { revolt_risk = 4 } #Somali migrations begin to destablilize region
1759.1.1 = { revolt_risk = 8 } #Pate sultans try to expel Omani overlords
1781.1.1 = { revolt_risk = 6 } #Raids by Mazrui clans in Mombasa destabilize region
1800.1.1 = { citysize = 9500 }