#545 - Maharastra

owner = PHA
controller = PHA
culture = marathi
religion = hinduism
capital = "Phaltan"
trade_goods = grain
hre = no
base_tax = 5
manpower = 1
citysize = 3500
add_core = PHA
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1450.1.1  = { citysize = 3000 }
1498.1.1  = { discovered_by = POR }
1500.1.1  = { citysize = 4000 }
1550.1.1  = { citysize = 5000 }
1600.1.1  = { citysize = 6000 }
1650.1.1  = { citysize = 7000 }
1700.1.1  = { citysize = 8000 }
1750.1.1  = { citysize = 9000 }
1800.1.1  = { citysize = 10500 }
