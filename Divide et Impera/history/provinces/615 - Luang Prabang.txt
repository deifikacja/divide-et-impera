#615 - Luang Prabang

owner = LXA
controller = LXA
culture = lao
religion = buddhism
capital = "Luang Prabang"
trade_goods = wool
hre = no
base_tax = 5
manpower = 3
fort1 = yes
temple = yes
citysize = 14900
add_core = LXA
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = indian
discovered_by = ordu

1500.1.1 = { citysize = 15680 }
1550.1.1 = { citysize = 16900 }
1600.1.1 = { citysize = 17750 }
1650.1.1 = { citysize = 18670 }
1694.1.1 = { owner = DAI controller = DAI add_core = DAI } # After Soulingna Vongsa's death, Lan Xang came under vietnamese rule
1700.1.1 = { citysize = 19680 }
1707.1.1 = {	owner = LUA
		controller = LUA
		add_core = LUA
		remove_core = LXA
		remove_core = DAI
	   } # Declared independent, Lan Xang was partitioned into three kingdoms; Vientiane, Champasak & Luang Prabang
1710.1.1 = { marketplace = yes }
1750.1.1 = { citysize = 21255 }
1771.1.1 = { add_core = TAU } # Vassal state of Burma
1778.1.1 = { remove_core = TAU } # Vassalhood of Burma ends
1779.1.1 = { add_core = AYU } # Vassal state
1800.1.1 = { citysize = 23480 }