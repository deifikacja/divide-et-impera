#1077 - Solikamsk

owner = PRM
controller = PRM
capital = "Cherdyn"
culture = komi
religion = shamanism
trade_goods = fur
hre = no
base_tax = 1
manpower = 0
citysize = 500
add_core = PRM
add_core = NOV
discovered_by = NOV
discovered_by = KAZ
discovered_by = PEL
discovered_by = PRM
discovered_by = ordu

1430.1.1  = { capital = "Solikamsk" citysize = 1001 religion = orthodox } # Founded by Stroganov family
1472.1.1  = { revolt = { type = pretender_rebels size = 0 leader = "Mikhail Velikopermsky" } controller = REB add_core = MOS } # Conquest of Novgorod
1472.1.1 = { revolt = { } controller = PRM }
1478.1.1  = { owner = MOS controller = MOS add_core = MOS remove_core = NOV culture = russian } # Annexed to Muscovy by Ivan III
1500.1.1  = { citysize = 1690 }
1505.1.1 = {	owner = RUS
		controller = RUS
		add_core = RUS
		remove_core = MOS
	    }
1550.1.1  = { citysize = 2433 }
1600.1.1  = { citysize = 2920 }
1650.1.1  = { citysize = 3558 }
1700.1.1  = { citysize = 3911 }
1723.5.15 = { capital = "Perm" }
1750.1.1  = { citysize = 4860 }
1800.1.1  = { citysize = 5850 }