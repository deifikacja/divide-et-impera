#1050 - Deren

culture = ainu
religion = shamanism
capital = "Deren"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 10
native_ferocity = 1
native_hostileness = 3

1643.1.1  = { discovered_by = RUS } # Vasily Poyarkov
1689.8.27 = {	discovered_by = QNG
		owner = QNG
		controller = QNG
		add_core = QNG
		citysize = 1150
		culture = manchu
		religion = confucianism
		trade_goods = fish
	    } # Treaty of Nerchinsk
1700.1.1  = { citysize = 2040 }
1750.1.1  = { citysize = 2879 }
1800.1.1  = { citysize = 3534 }