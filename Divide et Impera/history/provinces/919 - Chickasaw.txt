#919 - Chickasaw

owner = PLA
controller = PLA
culture = mississippian
religion = animism
capital = "Chickasaw"
trade_goods = grain
hre = no
base_tax = 2
manpower = 1
citysize = 1300
add_core = PLA
add_core = CHS
discovered_by = PLA

discovered_by = QUA
discovered_by = ONE
discovered_by = CAD
discovered_by = CAH
discovered_by = ETO
discovered_by = CHE
discovered_by = CRE
discovered_by = SHA
discovered_by = CHW

1450.1.1  = { citysize = 1500 }
1500.1.1  = { citysize = 1730 }
1540.1.1  = { discovered_by = SPA } # Hernando De Soto
1540.1.1  = { citysize = 2088 religion = shamanism culture = creek owner = CHS controller = CHS remove_core = PLA }
1600.1.1  = { citysize = 2540 }
1650.1.1  = { citysize = 3400 }
1670.1.1  = { discovered_by = FRA } # Robert Cavelier de La Salle
1716.1.1  = {	owner = FRA
		controller = FRA
		fort1 = yes
		culture = cosmopolitan_french
		religion = catholic
	    } # Ren� Robert Cavelier, Fort Prudhomme
1736.5.26 = { revolt_risk = 5 } # Battle of Ackia
1737.1.1  = { revolt_risk = 0 }
1741.1.1  = { add_core = FRA }
1750.1.1  = { citysize = 4200 add_core = LOU }
1763.2.10 = {	owner = GBR
		controller = GBR
		remove_core = FRA
		culture = english
	    } # Treaty of Paris, ceded to the British
1783.9.3  = {	owner = USA
		controller = USA
		add_core = USA
		religion = episcopalism
	    }