#643 - Pose

culture = sulawesi
religion = animism
capital = "Pose"
trade_goods = unknown
hre = no
base_tax = 5
manpower = 2
native_size = 60
native_ferocity = 2
native_hostileness = 9

1511.1.1 = { discovered_by = POR }
1550.1.1 = { owner = TER controller = TER add_core = TER citysize = 100 trade_goods = coffee }
1608.1.1 = { religion = sunni }