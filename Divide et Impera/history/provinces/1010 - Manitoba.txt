#1010 - Manitoba

culture = ojibwe
religion = shamanism
capital = "Manitoba"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 5
native_ferocity = 2
native_hostileness = 6

1612.1.1  = { discovered_by = ENG } # Thomas Button
1707.5.12 = { discovered_by = GBR } 
1740.1.1 = { discovered_by = FRA }#L.Verendrye
1741.1.1  = {	owner = FRA
		controller = FRA
		culture = cosmopolitan_french
		religion = catholic
		capital = "Fort Dauphin"
		citysize = 100
	    }
1763.2.10 = {	owner = GBR
		controller = GBR
		citysize = 300
		culture = english
		trade_goods = fur
	    } # Treaty of Paris
1788.2.10 = { add_core = GBR }
