#268 - Bessarabia

owner = MOL	
discovered_by = ordu
controller = MOL
culture = moldavian
religion = greek_orthodox
hre = no
base_tax = 5
capital = "Iasi"
citysize = 8000
trade_goods = wine
manpower = 4
fort1 = yes
add_core = MOL
discovered_by = western
discovered_by = eastern 
discovered_by = ordu
discovered_by  = muslim
discovered_by = ottoman

1450.1.1  = { citysize = 9000 }
1475.1.1  = { citysize = 12000 manpower = 5 base_tax = 6 } #Stefan the Great
1484.1.1  = { controller = TUR citysize = 10500 } #Turkish invasion
1489.1.1  = { controller = MOL } # Treaty with Sultan Bezayd II.
1500.1.1  = { citysize = 11000 }
1540.1.1  = { constable = yes }
1550.1.1  = { citysize = 13000 }
1570.1.1  = { marketplace = yes }
1593.1.1  = { add_core = WAL } # Ruled by Michael the brave
1600.1.1  = { citysize = 14000 }
1601.1.1  = { remove_core = WAL }
1625.1.1  = { citysize = 9800 } # Raid by the Crimean Tatars
1650.1.1  = { citysize = 12580 }
1653.1.1  = { revolt = { type = noble_rebels size = 2 } controller = REB } # Revolt of the Boyars
1654.1.1  = { revolt = {} controller = MOL } # Estimated
1660.1.1  = { courthouse = yes }
1700.1.1  = { citysize = 16000 }
1739.1.1  = { controller = RUS }
1739.9.18 = { controller = MOL }
1750.1.1  = { citysize = 12000 tax_assessor = yes }
1769.1.1  = { controller = RUS }
1774.1.1  = { controller = MOL }
1788.1.1  = { controller = RUS }
1789.1.1  = { controller = MOL }
1789.6.1  = { controller = HAB } # Occupied by Austria
1791.8.4  = { controller = MOL } # Treaty of Sistova
1800.1.1  = { citysize = 15000 }
1806.1.1  = { controller = RUS } # Occupied by Russia
1812.5.28 = { owner = MOL	discovered_by = ordu } # Treaty of Bucharest, Budziak to Russia
