#452 - Badakhshan

owner = BDK
controller = BDK
culture = pashtun
religion = sunni
capital = "Fayzabad"
trade_goods = wool
hre = no
base_tax = 2
manpower = 1
citysize = 2540
add_core = BDK
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = indian
discovered_by = ordu

1380.1.1 = { owner = TIM controller = TIM add_core = TIM }
1405.2.18 = { add_core = SAM }
1447.3.12  = {   owner = SAM
		controller = SAM
		remove_core = TIM
	    }#Shahrokh's death - partition of the Empire
1450.1.1 = { owner = BDK controller = BDK remove_core = SAM citysize = 2800 }
1500.1.1 = { citysize = 3057 owner = SHY controller = SHY add_core = SHY }
1505.1.1 = { owner = BDK controller = BDK remove_core = TIM }
1520.1.1 = {	owner = MUG
		controller = MUG
		add_core = MUG
		remove_core = TIM
	   } # Shifted to Mughal rule as the Timurid's power wained
1550.1.1 = { citysize = 3897 }
1600.1.1 = { citysize = 4378 discovered_by = TUR }
1650.1.1 = { citysize = 5047 }
1700.1.1 = { citysize = 6324 }
1747.6.1 = {	owner = DUR
	   	controller = DUR
	   	add_core = DUR
	   	remove_core = MUG
	   } # Ahmad Shah established the Durrani empire
1750.1.1 = { citysize = 8240 }
1800.1.1 = { citysize = 10540 }