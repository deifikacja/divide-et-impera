#259 - Sandomir

owner = POL	
discovered_by = ordu
controller = POL
capital = "Sandomierz"
culture = polish
religion = catholic
trade_goods = cattle
hre = no
base_tax = 4
manpower = 2
citysize = 5250
fort1 = yes
add_core = POL
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1400.1.1 = { citysize = 5750 }
1450.1.1 = { citysize = 6100 }
1500.1.1 = { citysize = 7188 }
1520.1.1 = { marketplace = yes }
1550.1.1 = { citysize = 8457 }
1569.7.4 = {	owner = RZP
		controller = RZP
		add_core = RZP
		remove_core = POL
	     } #Union of Lublin
1580.1.1 = { tax_assessor = yes }
1588.1.1 = { revolt = { type = revolutionary_rebels size = 0 } controller = REB } # Civil war
1589.1.1 = { revolt = {} controller = RZP } # Coronation of Sigismund III
1600.1.1 = { citysize = 9396 }
1606.1.1 = { revolt = { type = revolutionary_rebels size = 2 } controller = REB } # Civil war
1608.1.1 = { revolt = {} controller = RZP } # Minor victory of Sigismund
1650.1.1 = { citysize = 11054 }
1655.9.23 = { controller = SWE } # The Deluge
1656.9.1 = { controller = RZP }
1700.1.1 = { citysize = 13005 }
1733.1.1 = { revolt = { type = revolutionary_rebels size = 0 } controller = REB } # The war of Polish succession
1735.1.1 = { revolt = {} controller = RZP }
1750.1.1 = { citysize = 15300 }
1795.10.24 = {	controller = HAB
		owner = HAB
		add_core = HAB
	   } # Third partition
1800.1.1 = { citysize = 18000 }
1809.4.14   = {	owner = DOW
		controller = DOW
		add_core = DOW
		remove_core = RZP
	     	remove_core = HAB
	     } # The Duchy of Warsaw is established after the treaty of Tilsit, ruled by Frederick Augustus I of Saxony
1812.12.12 = { controller = RUS }
1815.6.9   = {	owner = RUS
		add_core = RUS
		remove_core = DOW
		add_core = POL
	     } # Congress Poland, under Russian control
