#844 - Zapotec

owner = ZAP
controller = ZAP
culture = zapotek
religion = animism
capital = "Oaxaca"
trade_goods = sugar
hre = no
base_tax = 4
manpower = 3
citysize = 5100
add_core = ZAP
discovered_by = MAY
discovered_by = MMA
discovered_by = QUI
discovered_by = TAY
discovered_by = ACA
discovered_by = CHT
discovered_by = CPE
discovered_by = TUX
discovered_by = UAY
discovered_by = CUP
discovered_by = ECA
discovered_by = AKC

discovered_by = ZAP
discovered_by = AZT
discovered_by = MIX
discovered_by = TLA
discovered_by = YPT
discovered_by = MTT
discovered_by = TAR
discovered_by = HST

1400.1.1   = { citysize = 5460 }
1450.1.1   = { citysize = 6460 }
1500.1.1   = { citysize = 6870 }
1542.1.1   = {	discovered_by = SPA
		owner = SPA 
		controller = SPA
		citysize = 3266
		religion = catholic
	     }
1567.1.1   = { add_core = SPA }
1600.1.1   = { citysize = 3978 }
1650.1.1   = { citysize = 4244 }
1700.1.1   = { citysize = 4810 }
1750.1.1   = { citysize = 5564 add_core = MEX }
1800.1.1   = { citysize = 6200 }
1810.9.16  = { owner = MEX controller = SPA } # Mexican War of Independence