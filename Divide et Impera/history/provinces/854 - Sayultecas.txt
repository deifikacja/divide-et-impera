#854 - Sayultecas

owner = TAR 
controller = TAR 
culture = purepecha
religion = animism 
capital = "Sayultecas" 
trade_goods = gold 
hre = no 
base_tax = 4 
manpower = 2 
citysize = 2800
add_core = TAR
discovered_by = MAY
discovered_by = MMA
discovered_by = QUI
discovered_by = TAY
discovered_by = ACA
discovered_by = CHT
discovered_by = CPE
discovered_by = TUX
discovered_by = UAY
discovered_by = CUP
discovered_by = ECA
discovered_by = AKC

discovered_by = ZAP
discovered_by = AZT
discovered_by = MIX
discovered_by = TLA
discovered_by = YPT
discovered_by = MTT
discovered_by = TAR
discovered_by = HST
discovered_by = HIC
discovered_by = CHC

1500.1.1  = { citysize = 3643 }
1522.1.1  = { discovered_by = SPA } # Saavedra 
1542.1.1  = {	owner = SPA 
		controller = SPA
		capital = "Guadalajara"
		religion = catholic
		culture = castillian
	    } # Founded by Crist�bal de O�ate 
1550.1.1  = { citysize = 4870 } 
1567.1.1  = { add_core = SPA }
1578.1.1  = { discovered_by = ENG } # Drake 
1600.1.1  = { citysize = 5200 } 
1650.1.1  = { citysize = 8611 } 
1700.1.1  = { citysize = 10862 } 
1750.1.1  = { citysize = 15200 add_core = MEX } 
1800.1.1  = { citysize = 20000 }
1810.9.16 = { owner = MEX controller = MEX } # Mexican War of Independence