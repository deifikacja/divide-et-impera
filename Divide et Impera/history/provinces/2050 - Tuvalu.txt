#2050 - Tuvalu

add_core = FFI
culture = malay-polynesian
religion = animism
capital = "Fongalafe"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 1
native_ferocity = 1
native_hostileness = 1
discovered_by = FFI

1568.1.16 = { discovered_by = SPA } #�lvaro de Menda?a
1720.1.1 = { owner = FFI controller = FFI citysize = 1000 trade_goods = fish }#Estimated date of foundation
1764.1.1 = { discovered_by = GBR } #John Byron