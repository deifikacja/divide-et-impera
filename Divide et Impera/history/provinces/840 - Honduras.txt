#840 - Honduras


culture = arawak
religion = animism 
capital = "Honduras" 
trade_goods = unknown
hre = no 
base_tax = 2 
manpower = 1
native_size = 16 
native_ferocity = 4 
native_hostileness = 4
discovered_by = MAY
discovered_by = MMA
discovered_by = QUI
discovered_by = TAY
discovered_by = ACA
discovered_by = CHT
discovered_by = CPE
discovered_by = TUX
discovered_by = UAY
discovered_by = CUP
discovered_by = ECA
discovered_by = AKC

discovered_by = ZAP
discovered_by = AZT
discovered_by = MIX
discovered_by = TLA
discovered_by = YPT
discovered_by = MTT
discovered_by = TAR
discovered_by = HST

1502.1.1  = { discovered_by = CAS } # Christopher Columbus 
1516.1.23 = { discovered_by = SPA }
1538.1.1  = {	owner = SPA
		controller = SPA
		capital = "Comayagua"
		culture = castillian
		religion = catholic
		citysize = 2250
		trade_goods = coffee 
	    } # The last resistance is crushed 
1548.1.1  = { revolt_risk = 5 } # Slave rebellion 
1549.1.1  = { revolt_risk = 0 } 
1550.1.1  = { citysize = 3250 }
1562.1.1  = { add_core = SPA }
1600.1.1  = { citysize = 3570 }
1650.1.1  = { citysize = 4935 }
1700.1.1  = { citysize = 5180 }
1750.1.1  = { citysize = 5470 add_core = MEX }
1800.1.1  = { citysize = 6500 }
1808.2.1  = { add_core = CAM } # France invades Spain
1810.9.16 = { owner = MEX controller = SPA } # Mexican War of Independence
1823.1.1 = { owner = CAM controller = CAM }