#564 - Vanga

owner = BNG
controller = BNG
culture = bengali
religion = hinduism
capital = "Dhaka"
trade_goods = tea
hre = no
base_tax = 3
manpower = 3
citysize = 26500
add_core = BNG
add_core = NIZ
fort1 = yes
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = MUG

1450.1.1 = { citysize = 27270 }
1500.1.1 = { citysize = 29000 discovered_by = POR }
1538.1.1  = { owner = SRI controller = SRI add_core = SRI } # Sher Shah takes over Bengal
1550.1.1 = { citysize = 32400 }
1556.11.5  = {	owner = BNG
		controller = BNG
		remove_core = SRI
	   } # Sher Shah Suri's death, the Bengali empire crumbeled
1576.1.1 = {	owner = MUG
		controller = MUG
		add_core = MUG
	   } # Mughal rule
1600.1.1 = { citysize = 36500 }
1650.1.1 = { citysize = 38700 religion = sunni }
1678.1.1 = { fort2 = yes } # Lal Bagh fort
1680.1.1 = { textile = yes }
1700.1.1 = { remove_core = NIZ citysize = 43280 }
1750.1.1 = { citysize = 47860 }
1765.1.1 = {	owner = GBR
		controller = GBR
		remove_core = MUG
	   } # British rule
1790.1.1 = { add_core = GBR }
1800.1.1 = { citysize = 52400 }
