#560 - Jharkand

owner = BNG
controller = BNG
culture = bihari
religion = sunni
capital = "Ranchi"
trade_goods = tea
hre = no
base_tax = 5
manpower = 4
citysize = 24870
add_core = BNG
fort1 = yes
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = MUG

1450.1.1  = { citysize = 25100 }
1500.1.1  = { citysize = 25200 }
1530.12.26  = { owner = SRI controller = SRI add_core = SRI } # Sher Shah takes over Bengal
1550.1.1  = { citysize = 25789 }
1556.11.5  = {	owner = MUG
		controller = MUG
		remove_core = SRI
	   } # Sher Shah Suri's death, the Bengali empire crumbeled
1600.1.1  = { citysize = 26534 }
1650.1.1  = { citysize = 27558 }
1690.1.1  = { discovered_by = ENG }
1700.1.1  = { citysize = 28700 }
1707.5.12 = { discovered_by = GBR }
1750.1.1  = { citysize = 30343 }
1765.1.1  = {	owner = GBR
		controller = GBR
		remove_core = MUG
	    } # British authority
1772.1.1  = { revolt_risk = 5 } # Paharia revolt
1780.1.1  = { revolt_risk = 7 } # Revolt led by Tilka Manjhi
1785.1.1  = { revolt_risk = 2 }
1790.1.1  = { add_core = GBR }
1795.1.1  = { revolt_risk = 6 } # Tamar revolt, Munda revolt
1800.1.1  = { citysize = 32400 }
1802.1.1  = { revolt_risk = 0 }
1819.1.1  = { revolt_risk = 7 } # Munda revolt
1820.1.1  = { revolt_risk = 0 }

