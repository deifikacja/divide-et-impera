#1058 - Nerchinsky

culture = mongol
religion = shamanism
capital = "Nerchinsk"
trade_goods = unknown
hre = no
base_tax = 4
manpower = 1
native_size = 10
native_ferocity = 1
native_hostileness = 3
discovered_by = ordu

1654.1.1 = {	discovered_by = RUS 
		owner = RUS
		controller = RUS
	   	religion = orthodox
	   	culture = russian
		citysize = 500
		trade_goods = wool
	   } # Pyotr Beketov
1678.1.1 = {	add_core = RUS
	   	citysize = 1040
	   }
1750.1.1 = { citysize = 1270 }
1800.1.1 = { citysize = 2644 }