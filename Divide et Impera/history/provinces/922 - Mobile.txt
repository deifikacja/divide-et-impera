#922 - Mobile

culture = creek
religion = shamanism
capital = "Mobile"
trade_goods = unknown
hre = no
base_tax = 5
manpower = 1
native_size = 10
native_ferocity = 2
native_hostileness = 7
discovered_by = CRE
discovered_by = CHE
discovered_by = CHW
discovered_by = APL
discovered_by = ALM

1519.1.1  = { discovered_by = SPA } # Alonzo Alvarez de Pineda
1702.1.20 = {	owner = FRA
		controller = FRA
		citysize = 355
		culture = cosmopolitan_french
		religion = catholic
		trade_goods = cotton
	    } # Founding of Mobile
1727.1.1  = { add_core = FRA }
1717.1.1  = { fort2 = yes } # Fort Toulouse on the Coosa River constructed
1750.1.1  = { citysize = 2140 }
1756.5.16 = { controller = GBR  } # Occupied by the British
1763.2.10 = {	owner = GBR
		remove_core = FRA
		culture = english
		capital = "St. Francisville"
	    } # Treaty of Paris - ceded to Britain, France gave up its claim
1783.9.3  = { owner = SPA controller = SPA culture = american } # Spanish occupation
1800.1.1  = { citysize = 3350 }
1808.9.3  = { add_core = SPA }
1810.9.24 = {	owner = USA
		controller = USA
		add_core = USA
		remove_core = SPA
		religion = episcopalism
	    } # Captured by the Americans
