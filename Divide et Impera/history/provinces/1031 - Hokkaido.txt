#1031 - Hokkaido

culture = ainu
religion = animism
capital = "Hakodate"
trade_goods = fur
hre = no
base_tax = 1
manpower = 1
native_size = 5
native_ferocity = 1
native_hostileness = 1
discovered_by = TAK
discovered_by = MAT
discovered_by = ASK

1590.1.1  = {	
	owner = MTS
	controller = MTS
	culture = japanese
	religion = shinto
	citysize = 320
	trade_goods = fish
	} # Russian & Japanese attemps at colonization
1600.10.22 = { owner = JAP controller = JAP add_core = JAP }#Japan is unified
1640.1.1  = { discovered_by = RUS } # Ivan Moskvitin
1650.1.1 = { citysize = 400 }
1669.1.1 = { revolt_risk = 1 } # Ainu uprising
1670.1.1 = { revolt_risk = 0 } # End of uprising
1700.1.1 = { citysize = 500 }
1750.1.1 = { citysize = 600 }
1789.1.1 = { revolt_risk = 1 } # Ainu uprising
1790.1.1 = { revolt_risk = 0 culture = japanese religion = shinto } # End of uprising
1800.1.1 = { citysize = 700 }