#284 - Crimea

owner = GOL
controller = GOL 
culture = tartar
religion = sunni
hre = no
base_tax = 6
trade_goods = salt
manpower = 5
capital = "Salaciq"
citysize = 3371
fort1 = yes
add_core = CRI
regimental_camp = yes
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = CRI
discovered_by = KAZ
discovered_by = GOL
discovered_by = AST
discovered_by = NOG
discovered_by = BSH
discovered_by = QAS

1425.1.1  = { revolt = { type = nationalist_rebels size = 6 leader = "Haji I Giray" } controller = REB }
1427.1.1  = { revolt = {} owner = CRI controller = CRI }
1450.1.1 = { citysize = 3745 }
1460.1.1 = { marketplace = yes }
1475.6.1 = { add_core = TUR } # Invasion, Crimea becomes a vassal of the Ottoman Empire
1500.1.1 = { citysize = 4318 }
1532.1.1 = { capital = "Bakhchisaray" fort2 = yes citysize = 6318 } # New capital is founded by Sahib Giray I
1550.1.1 = { citysize = 7080 }
1600.1.1 = { citysize = 7977 }
1650.1.1 = { citysize = 8321 } 
1700.1.1 = { citysize = 8731 }
1738.1.1 = { citysize = 7520 } # Devastating raid by the Russians
1750.1.1 = { citysize = 8733 }
1774.1.1 = { remove_core = TUR add_core = RUS } # Treaty of Kuchuk-Kainarji, independent but under Russian influence
1783.1.1 = { owner = RUS controller = RUS } # Annexed by Catherine II, in violation with the treaty
1800.4.8 = { citysize = 10450 }