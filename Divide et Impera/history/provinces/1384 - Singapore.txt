#1384 - Singapore

owner = TEM
controller = TEM
culture = malayan
religion = hinduism
capital = "Temasek"
trade_goods = naval_supplies
hre = no
base_tax = 4
manpower = 1
citysize = 5234
add_core = TEM
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1402.1.1  = { 	owner = MLC 
		controller = MLC 
		add_core = MLC 
		add_core = JOH 
		citysize = 4120 
		base_tax = 3 
	    } #Malacca became a new capital
1416.1.1  = { religion = sunni }
1500.1.1  = { citysize = 3765 } 
1511.9.10 = { owner = JOH controller = JOH remove_core = MLC } #Malacca captured by the Portuguese
1550.1.1  = { citysize = 4385 }
1600.1.1  = { citysize = 5700 }
1613.1.1  = { citysize = 1200 base_tax = 2 manpower = 1 } #Destroyed by the Portuguese
1650.1.1  = { citysize = 1021 }
1700.1.1  = { citysize = 1183 }
1750.1.1  = { citysize = 1250 }
1800.1.1  = { citysize = 1345 }
1819.1.29 = { 	owner = GBR 
		controller = GBR 
		add_core = GBR 
		capital = "Singapore" 
		citysize = 1500 
		base_tax = 3 
		manpower = 1
	     } # Modern Singapore founded