#223 - Granada

owner = GRA		#Mustapha Sa'd King of Granada
controller = GRA
add_core = GRA
culture = andalucian
religion = sunni
hre = no
base_tax = 6
trade_goods = luxury_goods
manpower = 5 
fort1 = yes
capital = "Granada" 
citysize = 30000
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ

1356.1.1   = { add_core = CAS }
1450.1.1   = { citysize = 33000 }
1482.7.1   = { revolt = { type = revolutionary_rebels size = 2 } controller = REB } # While his father is absent becuase of the war, prince Boabdil stages a coup and the Kingdom of Granada is split in two halves, both at war with Castilla
1484.5.1   = { revolt = {} controller = GRA } # Boabdil is captured by the christians and forced to a truce, Muhammad el Zagal returns to Granada and retakes power, extending the war with Castilla.
1492.1.2   = { owner = CAS controller = CAS } # Boabdil gives the keys of the Alhambra to King Fernando, the Reconquista ends.
1499.12.1  = { revolt_risk = 2 } # Increased pressure from the Inquisition to convert Spanish muslims
1500.1.1   = { citysize = 27500 }
1502.2.1   = { revolt_risk = 0 religion = catholic citysize = 17500 base_tax = 4 manpower = 3 } # All the subjects of Castilla are forced to convert or emigrate
1516.1.23  = {	controller = SPA
		owner = SPA
		add_core = SPA
	     } # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castilla
1550.1.1   = { citysize = 20820 }
1568.12.25 = { controller = REB revolt_risk = 5 } # Sublevation of the morisques in the kingdom of Granada.
1570.10.28 = {	controller = SPA
		culture = castillian
		revolt_risk = 0
	     } # To quell the revolt, the morisques in Granada are forcefully deported to other Spanish territories
1600.1.1   = { citysize = 23000 }
1650.1.1   = { courthouse = yes citysize = 25641 }
1700.1.1   = { citysize = 26570 }
1713.4.11  = { remove_core = CAS }
1750.1.1   = { citysize = 27870 }
1800.1.1   = { citysize = 29000 }
1808.6.6   = { revolt = { type = nationalist_rebels size = 0 } controller = REB }
1811.1.1   = { revolt = {} controller = SPA }
1812.10.1  = { revolt = { type = nationalist_rebels size = 1 } controller = REB }
1813.12.11 = { revolt = {} controller = SPA }