#51 - Higher Lusatia

owner = MEI
controller = MEI
add_core = MEI
culture = lusatian
religion = catholic
hre = yes
base_tax = 2
trade_goods = grain
manpower = 2
fort1 = yes
capital = "Chosebuz"
citysize = 3400
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1364.11.1 = { owner = SVJ controller = SVJ add_core = SVJ }#Bolko II of �widnica-Jawor purchase
1368.7.28 = { owner = BOH controller = BOH add_core = BOH remove_core = SVJ }#Death of Bolko II
1400.1.1  = { citysize = 3600 culture = saxon }
1450.1.1  = { citysize = 3800 }
1500.1.1  = { citysize = 4200 }
1501.6.1 = {	owner = SAX
		controller = SAX
		add_core = SAX
		remove_core = BOH
	    }
1525.1.1  = { citysize = 4800 }
1536.1.1  = { religion = protestant }
1550.1.1  = { citysize = 5700 }
1575.1.1  = { citysize = 6200 marketplace = yes }
1585.1.1  = { fort2 = yes }
1600.1.1  = { citysize = 7500 }
1625.1.1  = { citysize = 8300 }
1631.1.1  = { citysize = 7200 } #Devastating population losses in Thirty Years War
1650.1.1  = { citysize = 8600 constable = yes }
1675.1.1  = { citysize = 8900 }
1700.1.1  = { citysize = 9600 }
1725.1.1  = { citysize = 10400 }
1730.1.1  = { fort3 = yes }
1750.1.1  = { citysize = 11100 }
1775.1.1  = { citysize = 12700 }
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
1815.1.18 = {	owner = PRU
		controller = PRU
		add_core = PRU
		remove_core = SAX
	    }