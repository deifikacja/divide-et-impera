#436 - Khurasan

owner = KHO
controller = KHO
culture = persian
religion = sunni
capital = "Birjand"
trade_goods = wool
hre = no
base_tax = 3
manpower = 3
citysize = 2945
add_core = KHO
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = TUR
discovered_by = TIM
discovered_by = SHY
discovered_by = MUG
discovered_by = ordu

1386.1.1 = { owner = TIM controller = TIM add_core = TIM }#Timur the Lenk
1405.2.18 = { add_core = HER }
1447.3.12  = {   owner = HER
		controller = HER
		remove_core = TIM
	    }#Shahrokh's death - partition of the Empire
1450.1.1  = { citysize = 3190 }
1464.1.1  = { revolt_risk = 3 } # Pillaged by Timurid rebels
1465.1.1  = { revolt_risk = 0 }
1500.1.1  = { citysize = 3870 }
1506.1.1  = {	owner = SHY
		controller = SHY
		add_core = SHY
	    }
1510.1.1  = {	owner = PER
		controller = PER
		add_core = PER
		remove_core = SHY
		religion = shiite
	     } # The Safavids took over, Shi'ism became the state religion
1550.1.1  = { citysize = 4857 fort1 = yes }
1597.12.1 = { base_tax = 4 } # The Reforms of Abbas the Great
1600.1.1  = { citysize = 6060 }
1650.1.1  = { citysize = 7080 fort2 = yes }
1700.1.1  = { citysize = 10405 }
1747.1.1  = { revolt_risk = 3 } # Shah Nadir is killed, local khanates emerged
1748.1.1  = { revolt_risk = 4 } # The empire began to decline
1749.1.2  = {	owner = KHO
		controller = KHO
		remove_core = PER
		revolt_risk = 0
	    } # Shahrokh is allowed to remain as ruler of Khurasan
1750.1.1  = { citysize = 9358 }
1800.1.1  = { citysize = 9500 }
1803.4.1  = {	owner = PER
		controller = PER
	    	add_core = PER
	    } # Conquered by Agha Muhammad Khan