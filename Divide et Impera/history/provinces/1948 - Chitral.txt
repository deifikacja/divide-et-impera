#578 - Kohistan

owner = CHI
controller = CHI
culture = pashtun
religion = sunni
capital = "Chitral"
trade_goods = luxury_goods
hre = no
base_tax = 4
manpower = 1
citysize = 4250
fort1 = yes
marketplace = yes #Important trade place
add_core = CHI
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = ordu

1450.1.1 = { citysize = 5005 }
1500.1.1 = { citysize = 6800 }
1550.1.1 = { citysize = 8780 }
1600.1.1 = { citysize = 10350 discovered_by = TUR }
1650.1.1 = { citysize = 12500 }
1700.1.1 = { citysize = 14790 }
1725.1.1 = { tax_assessor = yes }
1750.1.1 = { citysize = 19170 }
1800.1.1 = { citysize = 21640 }