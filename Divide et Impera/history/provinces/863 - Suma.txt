#863 - Suma

culture = taracahitic
religion = animism
capital = "Suma"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 5
native_ferocity = 2
native_hostileness = 7

discovered_by = ANA


discovered_by = APA
discovered_by = PUB
discovered_by = NAO

1540.1.1   = { discovered_by = SPA } # Francisco Vasquez de Coronado
1709.10.12 = {	owner = SPA
		controller = SPA
		capital = "Chicuahua"
		culture = castillian
		religion = catholic
		citysize = 1100
		trade_goods = wool
	     } # Founded by Antonio Deza y Ulloa
1734.1.1   = { add_core = SPA }
1750.1.1   = { citysize = 2060 add_core = MEX }
1800.1.1   = { citysize = 3780 }
1810.9.16  = { owner = MEX controller = SPA } # Mexican War of Independence
1821.8.24 = { controller = MEX }#Treaty of C�rdoba