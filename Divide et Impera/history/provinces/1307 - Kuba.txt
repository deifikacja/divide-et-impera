#1307 - Kuba

owner = KUB
controller = KUB
add_core = KUB
culture = bantu
religion = shamanism
capital = "Mushenge"
trade_goods = copper
hre = no
base_tax = 3
citysize = 2000
manpower = 2
discovered_by = KUB

1500.1.1 = { discovered_by = LND }
1550.1.1 = { citysize = 2500 }
1585.1.1 = { discovered_by = LUB }
1600.1.1 = { citysize = 4000 } #Kingdom consolidated
1750.1.1 = { discovered_by = KAT }
1800.1.1 = { citysize = 4750 }