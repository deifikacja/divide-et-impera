#962 - Unami


owner = DEL
controller = DEL
add_core = DEL
culture = delaware
religion = shamanism
capital = "Unami"
trade_goods = fur
hre = no
base_tax = 5
manpower = 1
citysize = 1000
discovered_by = DEL

1609.1.1   = { discovered_by = NED } # Henry Hudson
1629.1.1   = {	owner = NED
		controller = NED
		culture = dutch
		religion = reformed
		citysize = 410
	     } # Founding of Pavonia
1654.1.1   = { add_core = NED }
1665.6.1   = { controller = ENG } # English seize New Netherlands at outset of Second Anglo-Dutch war
1667.7.31  = {	owner = ENG
		culture = english
		religion = episcopalism
		citysize = 1200
		remove_core = NED
		capital = "Trenton"
	     } # Treaty of Breda
1692.7.31  = { add_core = ENG }
1700.1.1   = { citysize = 3500 }
1707.5.12 = {	discovered_by = GBR
		owner = GBR
		controller = GBR
		add_core = GBR
		remove_core = ENG
	    } 
1750.1.1   = { citysize = 12000 add_core = USA }
1764.7.1   = {	culture = american
		revolt_risk = 6
	     } # Growing unrest
1775.1.1   = { tax_assessor = yes }
1776.7.4   = {	owner = USA
		controller = USA
	     } # Declaration of independence
1782.11.1  = { revolt_risk = 0 remove_core = GBR } # Preliminary articles of peace, the British recognized Amercian independence
1800.1.1   = { citysize = 41220 }
