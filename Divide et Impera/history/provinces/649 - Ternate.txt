#649 - Ternate

owner = TER
controller = TER
add_core = TER
culture = papuan
religion = animism
capital = "Ternate"
trade_goods = spices
hre = no
base_tax = 2
manpower = 1
citysize = 1000
discovered_by = JAO
discovered_by = TER
discovered_by = TID


1500.1.1 = { religion = sunni }
1512.1.1 = { discovered_by = POR } # Francisco Serr�o
1522.1.1 = { add_core = POR } #Within Portuguese sphere of influence
1606.3.28 = { add_core = SPA } #Within Spanish sphere of influence
1676.9.12 = { add_core = NED } #Within Dutch sphere of influence
1700.1.1 = { citysize = 1050 }
1750.1.1 = { citysize = 1150 }
1800.1.1 = { citysize = 1500 }