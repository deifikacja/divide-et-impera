#1965 - Karauli

owner = KRU
controller = KRU
culture = rajput
religion = hinduism
capital = "Karauli"
trade_goods = grain
hre = no
base_tax = 3
manpower = 2
citysize = 4800
add_core = KRU
fort1 = yes
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1500.1.1 = { citysize = 5340 }
1527.1.1 = {	owner = MUG
		controller = MUG
		add_core = MUG
	   } # The Mughal Empire
1540.1.1  = { owner = SRI controller = SRI add_core = SRI } # Sher Shah takes over
1550.1.1 = { citysize = 6458 }
1556.1.1  = {	owner = MUG
		controller = MUG
		remove_core = SRI
	   } # Sher Shah Suri's death, the Bengali empire crumbeled
1600.1.1 = { citysize = 7570 }
1650.1.1 = { citysize = 8540 }
1669.1.1 = { controller = KRU } # The Jats revolted against Aurangzeb's rule
1670.1.1 = { controller = MUG }
1685.1.1 = { controller = KRU } # Jat revolt lead by Raja Ram
1691.1.1 = { controller = MUG } # The Jats were defeated
1700.1.1 = { citysize = 9086 }
1707.1.1 = { controller = KRU } # With Aurangzeb's death the resistance grew
1708.1.1 = { controller = MUG }
1750.1.1 = { citysize = 10563 }
1753.1.1 = {	owner = KRU
		controller = KRU
		remove_core = MUG
	   } # The Marathas
1780.1.1 = { revolt_risk = 5 } # Internal chaos, parts of Jaipur were annexed to Alwar & Bharatpur
1800.1.1 = { citysize = 11000 }
1817.11.15 = {	
		add_core = GBR
	   }#Protectorate
