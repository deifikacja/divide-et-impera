#143 - Albania

owner = SER
controller = SER
culture = albanian
religion = greek_orthodox
capital = "Kruj�"
trade_goods = wool
hre = no
base_tax = 2
manpower = 4
citysize = 3600
fort1 = yes
add_core = ALB
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western

1356.1.1  = { revolt = { type = nationalist_rebels size = 1 leader = "Andrea II Muzaka" } controller = REB }#Muzakaj principality of Berat
1371.10.26 = { owner = ALB controller = ALB revolt = {} }
1393.1.1 = { owner = TUR controller = TUR }
1402.1.1  = { revolt = { type = pretender_rebels size = 0 } controller = REB } # Interregnum
1410.1.1  = { revolt = {} }
1410.1.2  = { revolt = { type = pretender_rebels size = 0 } }
1413.1.1  = { revolt = {} controller = TUR }
1430.1.1  = { revolt = { type = nationalist_rebels size = 4 leader = "Gjon Kastrioti" } }
1444.3.4  = { revolt = { } owner = ALB controller = ALB } # Skanderbeg's rebellion. The League of Lezhe
1450.1.1  = { citysize = 4300 }
1453.5.29 = { remove_core = BYZ }
1478.6.16 = { owner = TUR controller = TUR }
1481.1.1  = { remove_core = NAP }
1500.1.1  = { citysize = 5254 }
1520.1.1  = { religion = sunni } # Predominant religion under Turkish rule
1550.1.1  = { citysize = 5780 }
1555.1.1  = { revolt_risk = 5 } # General discontent against the Janissaries' dominance
1556.1.1  = { revolt_risk = 0 marketplace = yes }
1600.1.1  = { citysize = 6150 }
1614.1.1  = { capital = "Tirana" } # Established by Sulejman Pasha
1650.1.1  = { citysize = 6870 }
1687.1.1  = { revolt_risk = 6 } # Christian counteroffensive against the Ottomans
1690.1.1  = { revolt_risk = 0 }
1700.1.1  = { citysize = 7140 }
1750.1.1  = { citysize = 7890 }
1788.1.1  = { fort2 = yes } # Ali Pasha made it a stronghold
1797.1.1  = { revolt = { type = revolutionary_rebels size = 1 } controller = REB } # Suliot uprising
1800.1.1  = { citysize = 9000 }
1803.1.1  = { revolt = {} controller = TUR }