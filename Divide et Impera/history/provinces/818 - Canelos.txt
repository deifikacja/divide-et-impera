#818 - Canelos

culture = jivaro
religion = animism
capital = "Canelos"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 10
native_ferocity = 0.5
native_hostileness = 2
discovered_by = INC
discovered_by = CHM
discovered_by = INC
discovered_by = CHM
discovered_by = HNV
discovered_by = MNO
discovered_by = PCN
discovered_by = QUT
discovered_by = CII
discovered_by = HNC
discovered_by = CAA
discovered_by = CRC
discovered_by = TRC
discovered_by = CNC
discovered_by = SCN

discovered_by = CNE
discovered_by = CNB
discovered_by = NZC
discovered_by = AYA
discovered_by = DIA
discovered_by = HUA
discovered_by = TIT
discovered_by = ATA


#1495.1.1  = { owner = INC controller = INC add_core = INC  }
1533.8.29 = {	discovered_by = SPA
		owner = SPA
		controller = SPA
		religion = catholic
citysize = 1000 trade_goods = grain
	    }# The death of Atahualpa
1550.1.1  = { citysize = 2433 }
1559.8.29 = { add_core = SPA }
1600.1.1  = { citysize = 3540 }
1650.1.1  = { citysize = 4120 }
1700.1.1  = { citysize = 5782 }
1750.1.1  = { citysize = 6550 add_core = PEU }
1800.1.1  = { citysize = 7210 }