#Lapland, the far north wastelands.

capital = "Kien Kasjarkka"
culture = sapmi
religion = shamanism
hre = no
base_tax = 1
trade_goods = unknown
manpower = 1
native_hostileness = 1
native_size = 3
native_ferocity = 2
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western

1634.1.1 = { capital = "Kengis" }
1640.1.1 = {	owner = SWE
		controller = SWE 
		add_core = SWE
		citysize = 400
		trade_goods = fur
	   } #The border vs Norway was set earlier but at this point colonialism had also started
1640.1.2 = { culture = swedish }
1640.1.2 = { religion = protestant }
1650.1.1 = { trade_goods = iron citysize = 620 base_tax = 2 }
1652.1.1 = { capital = "Kiruna" base_tax = 3 }
1700.1.1 = { citysize = 830 }
1740.1.1 = { fort1 = yes }
1750.1.1 = { citysize = 1070 }
1800.1.1 = { fort2 = yes citysize = 1200 }
