#575 - Baluchistan

owner = BAL
controller = BAL
culture = baluchi
religion = sunni
capital = "Kalat"
trade_goods = wool
hre = no
base_tax = 1
manpower = 3
citysize = 6840
fort1 = yes
add_core = BAL
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = ordu

1393.1.1 = { owner = TIM controller = TIM add_core = TIM }
1419.1.1 = { owner = BAL controller = BAL }
1450.1.1 = { citysize = 7300 }
1500.1.1 = { citysize = 8910 discovered_by = POR }
1526.1.1 = {	owner = MUG
		controller = MUG
		add_core = MUG
		remove_core = TIM
	   } # Shifted to Mughal rule as the Timurid's power wained
1550.1.1 = { citysize = 10100 }
1558.1.1 = {	owner = PER
		controller = PER
		add_core = PER
	   	remove_core = MUG
	   }
1595.1.1 = {	owner = MUG
		controller = MUG
		add_core = MUG
		remove_core = PER
	   } # Returned to Mughal rule by Akbar
1600.1.1 = { citysize = 12370 discovered_by = TUR }
1638.1.1 = { owner = BAL controller = BAL }#Khanate of Kalat
1650.1.1 = { citysize = 14223 }
1700.1.1 = { citysize = 15899 }
1747.6.1 = {	owner = DUR
	   	controller = DUR
	   	add_core = DUR
	   	remove_core = MUG
	   } # The Durrani Empire
1750.1.1 = { citysize = 17205 }
1773.1.1 = { owner = BAL controller = BAL }#Death of Ahmad Shah Durrani
1800.1.1 = { citysize = 19200 }