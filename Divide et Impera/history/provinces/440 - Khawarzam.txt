#440 - Khawarzam

owner = GOL
controller = GOL
culture = uzbehk
religion = sufism
capital = "Urgench"
trade_goods = wool
hre = no
base_tax = 6
manpower = 2
citysize = 12680
marketplace = yes
add_core = CHZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = ordu

1359.1.1 = { owner = CHZ controller = CHZ }
1380.1.1 = { owner = TIM controller = TIM add_core = TIM }
1385.1.1 = { owner = CHZ controller = CHZ }
1388.1.1 = { owner = TIM controller = TIM }
1428.1.1 = { owner = CHZ controller = CHZ }
1500.1.1 = { citysize = 13210 }
1506.1.1 = { owner = SHY controller = SHY add_core = SHY }
1510.1.1 = { owner = PER controller = PER remove_core = SHY }
1511.1.1 = {	owner = KHI 
	   	controller = KHI
	   	add_core = KHI
	   } # Conquered by nomadic Uzbeks
1538.1.1 = {	owner = SHY
	   	controller = SHY
	   } # Conquered by Uzbeks from Bokhara
1540.1.1 = {	owner = KHI 
	   	controller = KHI
	   } # Conquered by nomadic Uzbeks
1550.1.1 = { citysize = 13750 discovered_by = TUR fort1 = yes }
1595.1.1 = {	owner = SHY
	   	controller = SHY
	   } # Conquered by Uzbeks from Bokhara
1598.1.1 = {	owner = KHI 
	   	controller = KHI
	   } # Conquered by nomadic Uzbeks
1600.1.1 = { citysize = 14020 }
1650.1.1 = { citysize = 14741 }
1700.1.1 = { citysize = 15010 }
1740.1.1 = {	owner = PER
		controller = PER
		add_core = PER
	   } # Captured by Nadir Shah
1747.1.1 = {	owner = KHI
		controller = KHI
		remove_core = PER
	   } # The death of Nadir Shah ended the domination
1750.1.1 = { citysize = 15630 }
1800.1.1 = { citysize = 16320 }