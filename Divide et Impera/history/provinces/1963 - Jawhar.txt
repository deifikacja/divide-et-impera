#529 - Dadra

owner = JAW
controller = JAW
culture = marathi
religion = hinduism
capital = "Jawhar"
trade_goods = cattle
hre = no
base_tax = 2
manpower = 1
citysize = 1000
add_core = JAW
fort1 = yes
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1450.1.1  = { citysize = 1500 }
1498.1.1  = { discovered_by = POR }
1500.1.1  = { citysize = 2000 }
1550.1.1  = { citysize = 2500  }
1570.1.1  = { marketplace = yes }
1600.1.1  = { citysize = 3000 }
1650.1.1  = { citysize = 3500 }
1670.1.1  = { courthouse = yes }
1700.1.1  = { citysize = 4000 }
1750.1.1  = {	citysize = 4500
		tax_assessor = yes }
1800.1.1  = { citysize = 5000 }