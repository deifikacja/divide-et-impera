#586 - Pegu

owner = PEG
controller = PEG
culture = mon
religion = buddhism
capital = "Pegu"
trade_goods = spices
hre = no
base_tax = 6
manpower = 3
citysize = 68254
add_core = PEG
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1500.1.1 = { citysize = 72400 discovered_by = POR }
1539.1.1 = {	owner = TAU
		controller = TAU
		add_core = TAU
	   } # Annexed to the Kingdom of Taungoo
1550.1.1 = { citysize = 74548 owner = PEG controller = PEG }
1551.1.1 = {	owner = TAU
		controller = TAU
	   } # Annexed to the Kingdom of Taungoo
1574.1.1 = { owner = AYU controller = AYU } # Siamese occupation
1575.1.1 = { owner = TAU controller = TAU }
1600.1.1 = { citysize = 78150 }
1650.1.1 = { citysize = 81500 }
1700.1.1 = { citysize = 84607 }
1740.1.1 = { revolt = { type = nationalist_rebels size = 0 leader = "Smin Htaw" } controller = REB } # Pegu rebellion
1750.1.1 = { citysize = 87120 }
1757.1.1 = { revolt = {} controller = TAU citysize = 72000 } # Pegu is sacked & returned to Burmese control
1800.1.1 = { citysize = 74400 }