#578 - Kohistan

owner = HER
controller = HER
culture = pashtun
religion = hinduism
capital = "Peshawar"
trade_goods = grain
hre = no
base_tax = 3
manpower = 3
citysize = 8250
fort1 = yes
add_core = HER
add_core = PES
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = ordu

1389.1.1 = { owner = TIM controller = TIM add_core = TIM }
1419.1.1 = {	owner = KAB
		controller = KAB
		remove_core = TIM
	   } # Timurids collapse
1450.1.1 = { citysize = 9005 }
1489.1.1 = {   
		religion = sunni 
	   }
1500.1.1 = { citysize = 10800 }
1506.1.1 = {	owner = MUG
		controller = MUG
		add_core = MUG
		fort2 = yes
		remove_core = TIM 
	   } # Battle of Panipat
1550.1.1 = { citysize = 12780 }
1600.1.1 = { citysize = 14350 discovered_by = TUR }
1625.1.1 = { marketplace = yes }
1650.1.1 = { citysize = 16500 }
1700.1.1 = { citysize = 18790 }
1725.1.1 = { tax_assessor = yes }
1740.1.1 = {	owner = PER
		controller = PER
		add_core = PER
		remove_core = MUG
	   } # The Mughal empire began to decline
1747.6.1 = {	owner = DUR
		controller = DUR
		add_core = DUR
		remove_core = PER
	   } # Annexed by Ahmad Shah Durrani
1750.1.1 = { citysize = 23170 }
1800.1.1 = { citysize = 25640 }
1817.1.1 = {	owner = PES
		controller = PES
	   } # Very shortly independent
1829.1.1 = {	owner = KAB
		controller = KAB
	   }