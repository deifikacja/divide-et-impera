#720 - Tshiyet Noyan

owner = YUA
controller = YUA
culture = mongol
religion = shamanism
capital = "Tshiyet Noyan"
trade_goods = copper
hre = no
base_tax = 3
manpower = 7
citysize = 2850
add_core = YUA
fort1 = yes
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = ordu

1368.1.22 = { owner = KHA controller = KHA add_core = KHA remove_core = YUA }
1411.1.1 = { owner = OIR controller = OIR add_core = OIR } #Estimated
1500.1.1 = { citysize = 3150 }
1543.1.1 = { revolt = { type = revolutionary_rebels size = 2 } controller = REB } # Fighting broke out between the Mongols
1550.1.1 = { citysize = 4158 }
1552.1.1 = {	revolt = {}
		owner = KHA
		controller = KHA
		add_core = KHA
	   } # The Oirads are defeated & Mongolia is reunited under Altan Khan
1566.1.1 = { religion = tibetan_buddhism } # State religion
1600.1.1 = { citysize = 5999 }
1634.1.1 = {	owner = MCH
		controller = MCH
		add_core = MCH
		remove_core = KHA
	   } # Part of the Manchu empire
1639.1.1 = { capital = "Urga" }
1650.1.1 = { citysize = 9348 }
1683.1.1 = {	owner = QNG
		controller = QNG
		add_core = QNG
		remove_core = MCH
	   } # The Qing Dynasty
1700.1.1 = { citysize = 11555 }
1706.1.1 = { capital = "Khuree" }
1750.1.1 = { citysize = 13710 }
1800.1.1 = { citysize = 15897 }