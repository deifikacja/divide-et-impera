#477 - Nadym

owner = GOL
controller = GOL
culture = khazak
religion = sunni
capital = "Balqash"
trade_goods = wool
hre = no
base_tax = 2
manpower = 1
citysize = 580
add_core = SHY
add_core = KZH
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = ordu

1446.1.1 = { owner = SHY controller = SHY }
1450.1.1 = { citysize = 1270 }
1460.1.1 = { owner = KZH controller = KZH }
1500.1.1 = { citysize = 1270 }
1550.1.1 = { citysize = 1888 }
1600.1.1 = { citysize = 2674 }
1622.1.1 = { discovered_by = RUS }
1650.1.1 = { citysize = 3564 }
1652.1.1 = { owner = OIR controller = OIR }#The Kalmyk conquest of Junior Juz
1680.1.1 = { owner = KZH controller = KZH }#Recaptured
1700.1.1 = { citysize = 4216 }
1718.1.1 = { owner = ULZ controller = ULZ }
1731.1.1 = { owner = ORZ controller = ORZ }
1750.1.1 = { citysize = 5313 }
1798.1.1 = {	owner = RUS
		controller = RUS
		add_core = RUS
	   } # The Middle horde is conquered
1800.1.1 = { citysize = 6240 }