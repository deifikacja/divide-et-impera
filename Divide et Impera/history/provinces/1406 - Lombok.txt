#1406 - Lombok

owner = MPH
controller = MPH
culture = sulawesi
religion = hinduism
capital = "Mataram"
trade_goods = coffee
hre = no
base_tax = 3
manpower = 2
citysize = 2970
add_core = MPH
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM

1356.1.1 = { add_core = BLI }
1390.1.1 = { revolt_risk = 2 } #Decline of the Majapahit after death of Hayam Wuruk
1406.1.1 = { revolt_risk = 3 }
1450.1.1 = { citysize = 3250 }
1453.1.1 = { revolt_risk = 7 } #interregnum
1456.1.1 = { revolt_risk = 3 }
1500.1.1 = { citysize = 3797 }
1520.1.1 = { owner = BLI controller = BLI remove_core = MPH }
1550.1.1 = { citysize = 4150 }
1597.1.1 = { discovered_by = NED } # Cornelis de Houtman
1600.1.1 = { citysize = 4387 }
1650.1.1 = { citysize = 4635 }
1674.1.1 = { owner = NED controller = NED add_core = NED }
1700.1.1 = { citysize = 5199 }
1750.1.1 = { citysize = 5353 }
1800.1.1 = { citysize = 5520 }