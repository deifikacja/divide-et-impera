#589 - Chiang Mai

owner = LNA
controller = LNA
culture = northern_thai
religion = buddhism
capital = "Lampung"
trade_goods = wool 
hre = no
base_tax = 2
manpower = 2
citysize = 6700
add_core = LNA
add_core = LAM
fort1 = yes
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = indian

1450.1.1  = { citysize = 8300 }
1500.1.1  = { citysize = 10900 }
1526.1.1  = { revolt_risk = 4 } # Political instability after King Phraya's death
1550.1.1  = { citysize = 12400 }
1558.1.1  = { add_core = TAU revolt_risk = 0 } # Burmese vassal
1578.1.1  = { owner = TAU controller = TAU } # Direct Burmese rule
1600.1.1  = { citysize = 16750 }
1650.1.1  = { citysize = 21280 }
1662.1.1  = { owner = AYU controller = AYU } # Occupied by the Siamese
1663.1.1  = { owner = LNA controller = LNA remove_core = TAU }
1700.1.1  = { citysize = 25220 }
1727.1.1  = { revolt_risk = 4 } # Rebellion
1728.1.1  = { revolt_risk = 0 }
1729.1.1 = {	owner = LAM
		controller = LAM
	    	remove_core = LNA
	    } # Lampung founded
1750.1.1  = { citysize = 28700 }
1800.1.1  = { citysize = 32000 }
