#906 - Assinboin

culture = dakota
religion = shamanism
capital = "Assinboin"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 25
native_ferocity = 2
native_hostileness = 8
discovered_by = NAK
discovered_by = DAK
discovered_by = LAK

1738.1.1 = { discovered_by = FRA }# Pierre Gaultier de Varennes, no real settlements until the 1800's
1803.4.3 = {
		add_core = USA
	    } # The Louisiana purchase
1805.4.25 = { discovered_by = USA }#Lewis and Clark. Not settled until 1867