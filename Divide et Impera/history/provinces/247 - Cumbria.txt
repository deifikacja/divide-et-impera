#247 - Cumbria

owner = ENG
controller = ENG
culture = northumbrian
religion = catholic
hre = no
base_tax = 4
trade_goods = fish
manpower = 1
capital = "Carlisle"
citysize = 2500
add_core = ENG
fort1 = yes
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ

1400.1.1  = { citysize = 2800 }
1450.1.1  = { citysize = 3500 }
1453.1.1  = { revolt_risk = 5 } #Start of the War of the Roses
1461.6.1  = { revolt_risk = 2 } #Coronation of Edward IV
1467.1.1  = { revolt_risk = 5 } #Rivalry between Edward IV & Warwick
1471.1.1  = { revolt_risk = 8 } #Unpopularity of Warwick & War with Burgundy
1471.5.4  = { revolt_risk = 2 } #Murder of Henry VI & Restoration of Edward IV
1483.6.26 = { revolt_risk = 8 } #Revulsion at Supposed Murder of the Princes in the Tower
1485.8.23 = { revolt_risk = 0 } #Battle of Bosworth Field & the End of the War of the Roses
1500.1.1  = { citysize = 4500 }
1550.1.1  = { citysize = 4800 }
1575.1.1  = { constable = yes } #Estimated
1576.1.1  = { religion = episcopalism }
1600.1.1  = { citysize = 5500 fort2 = yes }
1645.6.28 = { controller = SCO }
1646.5.5  = { controller = ENG } #End of First English Civil War
1648.4.1  = { revolt = { type = revolutionary_rebels size = 3 } controller = REB } # Estimated
1648.8.25 = { revolt = {} controller = ENG }
1650.1.1  = { citysize = 6500 }
1700.1.1  = { citysize = 8000 marketplace = yes } #Marketplace Estimated
1707.5.12 = {	owner = GBR
		controller = GBR
		add_core = GBR
	    	remove_core = ENG
	    }
1800.1.1  = { citysize = 10000 }