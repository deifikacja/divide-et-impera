#630 - Sunda

owner = SUN
controller = SUN
add_core = SUN
add_core = CIR
culture = sulawesi
religion = hinduism
capital = "Cirebon"
trade_goods = tea
hre = no
base_tax = 6
manpower = 1
citysize = 5920
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM

1400.1.1  = { citysize = 6430 }
1450.1.1  = { citysize = 8200 }
1478.1.1  = {	religion = sunni
		owner = CIR
		controller = CIR
		citysize = 10775
		add_core = CIR
	    }  # Cirebon founded
1600.1.1  = { citysize = 13800 }
1613.1.1  = { discovered_by = NED } # The Dutch arrived
1619.1.1  = { citysize = 15150 } 
1644.1.1  = { add_core = NED }
1650.1.1  = { citysize = 18670 }
1700.1.1  = { citysize = 20111 }
1750.1.1  = { citysize = 22346 }
1800.1.1  = { citysize = 24880 }
1819.1.1  = {	owner = NED
		controller = NED
	    }  # Sultanate suspended