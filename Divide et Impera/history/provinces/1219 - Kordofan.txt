#1219 - Kordofan

add_core = KRD
culture = nilotic 
religion = animism
capital = "Al Ubayd"
native_size = 50
native_ferocity = 4.5
native_hostileness = 9 
trade_goods = unknown
hre = no
base_tax = 3
manpower = 2
discovered_by = ALO
discovered_by = MKR
discovered_by = DAR

1520.1.1 = { discovered_by = TUR owner = TUR controller = TUR add_core = TUR citysize = 5000 trade_goods = slaves add_core = KRD }
1650.1.1 = { discovered_by = KRD owner = KRD controller = KRD }
1700.1.1 = { citysize = 4000 }
1748.1.1 = { controller = NUB }#Conquered by the Funj
1748.1.2 = { controller = KRD }#Funj places governor
1750.1.1 = { citysize = 4700 }
1800.1.1 = { citysize = 5000 }
1800.1.1 = { controller = DAR }#Darfur conquest
1800.1.2 = { controller = KRD }#Darfur governor placed
#1821.1.1 = { controller = EGY }#Egyptian conquest
#1821.1.2 = { controller = KRD }#Egyptian governor placed