#1194 - Boina

add_core = MER
culture = madagasque
religion = animism
capital = "Majanga"
manpower = 1
trade_goods = unknown
hre = no
base_tax = 2
native_size = 25
native_ferocity = 6
native_hostileness = 8

1500.9.1  = { discovered_by = POR } # Diego Dias
1675.9.1  = { discovered_by = MER }
1685.1.1  = { discovered_by = MEN discovered_by = ANT }
1690.1.1  = { citysize = 2000 trade_goods = slaves owner = BOI controller = BOI add_core = BOI discovered_by = BOI } #Kingdom established
1691.2.11 = {  }
1712.9.1  = { discovered_by = TAM }
1750.1.1  = { citysize = 2600 }