#951 - Kanawha

culture = iroquis
religion = shamanism
capital = "Kanawha"
trade_goods = unknown
hre = no
base_tax = 2
manpower = 1
native_size = 5
native_ferocity = 0.5
native_hostileness = 4
discovered_by = SHA

1671.1.1  = { discovered_by = ENG } # Abraham Wood
1707.5.12 = { discovered_by = GBR } 
1750.1.1  = {	owner = GBR
		controller = GBR
		culture = english
		religion = episcopalism
		citysize = 200 
		trade_goods = iron
	    } # British settlers cross the Appalachians
1764.7.1  = {	culture = american
		revolt_risk = 6
	    } # Growing unrest
1776.7.4  = {	owner = USA
		controller = USA
		add_core = USA
	    } # Declaration of independence
1782.11.1 = { revolt_risk = 0 } # Preliminary articles of peace, the British recognized Amercian independence
1800.1.1  = { citysize = 1940 }