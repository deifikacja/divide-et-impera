#2058 - Rapa Nui

owner = RAP
controller = RAP
add_core = RAP
culture = rapanui
religion = animism
capital = "Hanga Roa"
trade_goods = fish
hre = no
base_tax = 1
manpower = 1
citysize = 1100
discovered_by = RAP

1722.4.5 = { discovered_by = NED }#Jacob Roggeveen
1770.11.19 = { owner = SPA controller = SPA }#Annexed but not settled