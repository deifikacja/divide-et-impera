#1269 - Uri

owner = MLO
controller = MLO
add_core = MLO
culture = lombard
religion = catholic
capital = "Bellinzona"
trade_goods = cattle
hre = yes
base_tax = 2
manpower = 1
citysize = 1900
fort1 = yes
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1403.1.1 = { add_core = SWI }#Some parts conquered by Uri
1500.1.1 = { citysize = 2650 }
1515.1.1 = { owner = SWI controller = SWI }#Fully conquered
1550.1.1 = { citysize = 3200 }
1600.1.1 = { citysize = 4400 }
1648.1.1 = { hre = no } # Switzerland excluded from The Holy Roman Empire
1650.1.1 = { citysize = 5000 }
1700.1.1 = { citysize = 6800 }
1750.1.1 = { citysize = 9000 }
1800.1.1 = { citysize = 10000 }
