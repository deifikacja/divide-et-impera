#852 - Tlacopan - one of the original members of the Aztec Triple Alliance

owner = TEP
controller = TEP
culture = nahuatl
religion = animism 
capital = "Tlacopan" 
trade_goods = grain
hre = no 
base_tax = 4 
manpower = 3 
citysize = 12500
add_core = AZT
add_core = TEP
add_core = TLC
discovered_by = TLC
discovered_by = HST
discovered_by = TEP
discovered_by = MAY
discovered_by = MMA
discovered_by = QUI
discovered_by = TAY
discovered_by = ACA
discovered_by = CHT
discovered_by = CPE
discovered_by = TUX
discovered_by = UAY
discovered_by = CUP
discovered_by = ECA
discovered_by = AKC

discovered_by = ZAP
discovered_by = AZT
discovered_by = MIX
discovered_by = TLA
discovered_by = YPT
discovered_by = MTT
discovered_by = TAR
discovered_by = TEO
discovered_by = TEX

1420.1.1  = { owner = TLC controller = TLC } # With the help of Tenochtitlan they gained independence in 1420's
1430.1.1  = { owner = AZT controller = AZT add_core = AZT remove_core = TEP religion = teotl } # The Aztec Triple Alliance
1450.1.1  = { citysize = 14500 }
1500.1.1  = { citysize = 16740 }
1519.1.1  = { discovered_by = SPA }
1521.8.13 = {	owner = SPA 
		controller = SPA 
		citysize = 6580
		religion = catholic
	   } # The last Aztec emperor surrendered, Mexico city is founded on top of the ruins 
1546.1.1 = { add_core = SPA } 
1550.1.1 = { citysize = 7088 }
1600.1.1 = { citysize = 9670 } 
1624.1.1 = { revolt_risk = 5 } # Riots
1626.1.1 = { revolt_risk = 0 } 
1650.1.1 = { citysize = 12500 } 
1700.1.1 = { citysize = 14200 } 
1720.1.1 = { citysize = 12800 } # Volcano eruption 
1750.1.1 = { citysize = 15140 add_core = MEX } 
1800.1.1 = { citysize = 19000 } 
1810.9.16 = { owner = MEX controller = MEX } # Mexican War of Independence
