#2271 - Chikuzen

owner = SHN
controller = SHN
culture = japanese
religion = shinto
capital = "Chikuzen"
trade_goods = chinaware
hre = no
base_tax = 4
manpower = 1
citysize = 7000
add_core = SHN
fort1 = yes
discovered_by = chinese

1450.1.1   = { citysize = 5910 }
1500.1.1   = { citysize = 6350 }
1542.1.1   = { discovered_by = POR }
1550.1.1   = { citysize = 7200 }
1587.1.1 = { owner = TOY controller = TOY add_core = TOY }
1600.1.1   = { citysize = 8665 }
1600.10.22 = { owner = JAP controller = JAP add_core = JAP }#Japan is unified
1637.12.17 = { revolt = { type = anti_tax_rebels size = 1 } } # Shimabara rebellion
1638.4.12  = { revolt = {} }
1650.1.1   = { citysize = 9200 }
1700.1.1   = { citysize = 10350 }
1750.1.1   = { citysize = 11500 }
1800.1.1   = { citysize = 13247 }