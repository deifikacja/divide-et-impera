#1221 - Taqali

culture = nubian
religion = animism
capital = "Shandi"
native_size = 50
native_ferocity = 4.5
native_hostileness = 9 
manpower = 1
trade_goods = unknown
hre = no
base_tax = 2
discovered_by = ALO
discovered_by = MKR

1553.1.1 = { revolt_risk = 8 } #Uprising among the Saqadi
1554.1.1 = { revolt_risk = 0 }
1600.1.1 = { discovered_by = TUR }
1620.1.1 = { religion = sunni } #spread of Islam leads to shift in religion affiliation of region
1670.1.1 = {	owner = NUB
	   	controller = NUB
	   	add_core = NUB
		discovered_by = NUB
		citysize = 5000
		discovered_by = ETH
		trade_goods = slaves
	   }
1750.1.1 = { citysize = 7000 }
1756.1.1 = { revolt_risk = 4 } #Badi IV comes into conflict with aristocrats over new slave army plan
1761.1.1 = { revolt_risk = 9 } #Badi IV persecutes aristocrats who challenge his authority
1762.1.1 = { revolt_risk = 0 } #Badi IV deposed by Muhammad Abu Likaylik
1800.1.1 = { citysize = 8000 }