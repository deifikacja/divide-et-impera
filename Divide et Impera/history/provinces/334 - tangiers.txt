#334 - Tangiers

owner = MOR
controller = MOR  
culture = berber
religion = sunni
capital = "Tangier"
trade_goods = fish
hre = no
base_tax = 5
manpower = 1
citysize = 5332
fort1 = yes
add_core = TAN
add_core = MOR
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ

1420.1.1 = { owner = TAN controller = TAN }#Emirate of Tangiers
1450.1.1   = { citysize = 5900 }
1471.8.28   = { controller = POR add_core = POR remove_core = TAN }
1471.12.30 = { owner = POR }
1500.1.1   = { citysize = 6010 }
1550.1.1   = { citysize = 6587 }
1600.1.1   = { citysize = 7885 marketplace = yes }
1650.1.1   = { citysize = 8940 }
1662.1.29   = {	owner = ENG
		controller = ENG
		add_core = ENG
		remove_core = POR
	     } # Tangier is given to Charles II of England
1679.1.1   = { revolt_risk = 4 } # Moulay Ismail attempted to seize the town
1684.2.6   = {	owner = MOR
		controller = MOR
		remove_core = ENG
		revolt_risk = 0
	     } # Turned into a pirates nest, returned to Moroccan control, British retreat
1700.1.1   = { citysize = 10300 } # The city gradually declined
1750.1.1   = { citysize = 7890 }
1800.1.1   = { citysize = 5678 }
1810.1.1   = { citysize = 5000 }
