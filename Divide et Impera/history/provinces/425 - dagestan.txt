#425 - Dagestan

owner = TAB
controller = TAB
culture = azerbadjani
religion = shiite
capital = "Derbent"
trade_goods = wool
hre = no
base_tax = 3
manpower = 2
citysize = 1010
add_core = DER
add_core = TAB
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = TIM
discovered_by = SHY
discovered_by = MUG
discovered_by = ordu

1356.1.1 = { add_core = SHV add_core = QAR }
1357.1.1 = { owner = GOL controller = GOL add_core = GOL remove_core = TAB }
1382.1.1 = { owner = TIM controller = TIM add_core = TIM }
1395.1.1 = { controller = GOL }
1395.4.1 = { controller = TIM }
1408.1.1 = { owner = QAR controller = QAR add_core = QAR }
1437.1.1  = { controller = SHV owner = SHV remove_core = TIM remove_core = GOL }
1458.9.1   = { revolt = { type = revolutionary_rebels size = 1 } controller = REB } # Civil war in Qara Quyunlu
1458.12.1  = { revolt = {} controller = SHV }
1468.1.1   = {	owner = AKK
		controller = AKK
		add_core = AKK
	     } # The Ak Koyunlu expands their territory
1500.1.1   = { citysize = 1280 }
1501.1.1   = {	owner = PER
		controller = PER
		add_core = PER
		remove_core = AKK
	     } # The Safavids take over
1516.1.1 = {	owner = TUR
		controller = TUR
	   } # Part of the Ottoman Empire
1539.1.1 = {	owner = PER
		controller = PER
	   } # Part of the Ottoman Empire
1578.1.1 = {	owner = TUR
		controller = TUR
	   } # Part of the Ottoman Empire
1579.1.1 = {	owner = PER
		controller = PER
	   } # Part of the Ottoman Empire
1550.1.1   = { citysize = 1700 }
1578.9.9   = { controller = TUR } 
1590.3.21  = { owner = TUR add_core = TUR } # Peace of Istanbul
1600.1.1   = { citysize = 2157 }
1607.1.1   = { controller = PER } # Persian reconquest
1612.11.20 = { owner = PER remove_core = TUR } # Part of Persia
1650.1.1   = { citysize = 3643 }
1700.1.1   = { citysize = 4171 }
1722.1.1 = {   controller = RUS
	     } # Russia invades Persia
1723.9.12 = {   owner = RUS
	     } # Treaty of St.Petersburg
1732.1.1 = {	owner = PER
		controller = PER
	     } # Anna Ivanovna returns persian lands
1747.1.1   = { owner = DER controller = DER } # Shah Nadir is killed, local khanates emerged
1748.1.1   = { revolt_risk = 4 } # The empire began to decline
1750.1.1   = { citysize = 4868 }
1779.1.1   = { revolt_risk = 0 } # With the Qajar dynasty the situation stabilized
1800.1.1   = { citysize = 5502 }
1803.1.1   = {	owner = RUS
		controller = RUS
		add_core = RUS
		remove_core = PER
	     } # Incorporated into Russia
