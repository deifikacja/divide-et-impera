#303 - Saratow

owner = GOL
controller = GOL       
culture = volgaic
religion = shamanism
hre = no
base_tax = 4
trade_goods = grain     
manpower = 3
capital = "Saratow"
citysize = 4337
add_core = KAZ
add_core = GOL	
discovered_by = ordu
fort1 = yes
discovered_by = SIB
discovered_by = KAZ
discovered_by = CRI
discovered_by = GOL
discovered_by = NOG
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western

1400.1.1 = { religion = sunni }
#1438.1.1 = { owner = KAZ controller = KAZ remove_core = GOL }
1450.1.1  = { citysize = 4900 }
1460.1.1  = { owner = CRI controller = CRI remove_core = KAZ add_core = RUS }
1500.1.1  = { citysize = 5103 }
1550.1.1  = { citysize = 6003 } 
1571.1.1  = { fort2 = yes owner = RUS controller = RUS } # Construction of the Saratow fortress 
1598.1.1  = { revolt_risk = 5 } # "Time of troubles"
1600.1.1  = { citysize = 7062  religion = orthodox }
1613.1.1  = { revolt_risk = 0 } # Order returned, Romanov dynasty
1650.1.1  = { citysize = 8309 culture = ukrainian }
1670.1.1  = { revolt_risk = 8 } # Stepan Razin
1671.1.1  = { revolt_risk = 0 } # Razin is captured
1700.1.1  = { citysize = 9775 marketplace = yes }
1750.1.1  = { citysize = 11500 }
1773.1.1  = { revolt = { type = nationalist_rebels size = 6 leader = "Yemelyan Pugachev" } controller = REB } # Emelian Pugachev, Cossack insurrection
1774.9.14 = { revolt = {} controller = RUS } # Pugachev is captured
1780.1.1  = { fine_arts_academy = yes }
1800.1.1  = { citysize = 24700 }