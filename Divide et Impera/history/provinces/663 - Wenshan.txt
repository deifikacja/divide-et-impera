#663 - Wenshan

owner = YUA
controller = YUA
culture = zhuang
religion = animism
capital = "Gejiu"
trade_goods = tea
hre = no
base_tax = 2
manpower = 4
citysize = 10400
add_core = YUA	
discovered_by = ordu
add_core = MNG
add_core = SNG
add_core = QIN
add_core = ZHU
fort1 = yes
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = YUA

1368.1.22 = { revolt = { type = nationalist_rebels size = 2 } owner = MNG controller = REB remove_core = YUA }#The Ming didn't send army until 1381
1381.1.1 = { revolt = { } controller = MNG }
1450.1.1 = { citysize = 11200 }
1500.1.1 = { citysize = 12088 }
1505.1.1 = { marketplace = yes }
1550.1.1 = { citysize = 13150 }
1600.1.1 = { citysize = 14805 }
1650.1.1 = { citysize = 15999 }
1662.1.1 = {	owner = MCH
		controller = MCH
		add_core = MCH
		remove_core = MNG
	   } # The Qing Dynasty
1680.1.1 = { trade_goods = iron } # Mining became the main focus during the Qing Dynasty.
1683.1.1 = {	owner = QNG
		controller = QNG
		add_core = QNG
		remove_core = MCH
	   } # The Qing Dynasty
1700.1.1 = { citysize = 17120 }
1740.1.1 = { tax_assessor = yes }
1750.1.1 = { citysize = 18564 }
1800.1.1 = { citysize = 19870 }