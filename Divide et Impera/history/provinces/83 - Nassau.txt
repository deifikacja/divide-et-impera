owner = NAS
controller = NAS
add_core = NAS
capital = "Siegen"
trade_goods = copper
hre = yes
fort1 = yes
culture = hessian
religion = catholic
base_tax = 3
manpower = 2
citysize = 2000
marketplace = yes
courthouse  = yes
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1400.1.1   = { citysize = 2500 }
1450.1.1   = { citysize = 3000 }
1500.1.1   = { fort2 = yes citysize = 3500 temple = yes }
1536.1.1   = { religion = protestant }
1547.1.1   = { constable = yes }
1550.1.1   = { citysize = 2600 regimental_camp = yes }
1555.1.1   = { base_tax = 4 } # Full minting rights granted by the Emperor
1585.1.1   = { workshop = yes citysize = 16500 } 
1620.1.1   = { regimental_camp = yes citysize = 18000 fort3 = yes }
1650.1.1   = { citysize = 2800 }
1685.1.1   = { citysize = 3000 customs_house = yes tax_assessor = yes } # Reformed refugees find shelter in Kassel (-> Oberneustadt founded)
1720.1.1   = { citysize = 3200 fort4 = yes }
1800.1.1   = { citysize = 3700 }
1806.7.12  = { hre = no owner = BRG controller = BRG } # The Holy Roman Empire is dissolved
1813.10.14 = {	owner = NAS
		controller = NAS
	     } # Regained by the Nassau dynasty
1815.1.1 = {	owner = PRU
		controller = PRU
	     } # Ceded to Prussia for Luxembourg