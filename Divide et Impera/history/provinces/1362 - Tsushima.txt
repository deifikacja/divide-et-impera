# 1362 - Tsushima

owner = IMA
controller = IMA
culture = japanese
religion = shinto
capital = "Kaneda"
trade_goods = fish
hre = no
base_tax = 3
manpower = 1
citysize = 4700
add_core = SOC
fort1 = yes #Kaneda castle
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = ordu

1356.1.1 = { add_core = KOR }#Tsushima Islands were base of Japanese pirates and were invaded by Koreans trying to wipe them out
1395.1.1   = { owner = SOC controller = SOC }
1419.9.29  = { remove_core = KOR } #Peace treaty between So and Korea
1450.1.1   = { citysize = 5350 }
1500.1.1   = { citysize = 6400 }
1542.1.1   = { discovered_by = POR }
1550.1.1   = { citysize = 7408 }
1587.1.1   = { owner = TOY controller = TOY add_core = TOY }
1600.10.22 = { citysize = 8100 owner = JAP controller = JAP add_core = JAP }
1700.1.1   = { citysize = 12485 }
1750.1.1   = { citysize = 15150 }
1800.1.1   = { citysize = 17500 }