#1979 - Jafrabad
owner = BAH
controller = BAH
culture = marathi
religion = sufism
capital = "Jafrabad"
trade_goods = cattle
hre = no
base_tax = 3
manpower = 1
citysize = 6287
add_core = BAS
add_core = BAH
add_core = JFR
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1450.1.1  = { citysize = 6380 }
1498.1.1  = { discovered_by = POR }
1500.1.1  = { citysize = 6700 }
1518.1.1  = {	owner = BAS
		controller = BAS
		remove_core = BAH
	    } # The Breakup of the Bahmani sultanate
1550.1.1  = { citysize = 7340 }
1600.1.1  = { citysize = 7575 }
1633.7.27 = {	owner = MUG
		controller = MUG
		add_core = MUG
	    } # Conquered by Shah Jahan
1650.1.1  = { citysize = 8000 }
1650.1.1  = {	owner = JFR
		controller = JFR
		remove_core = MUG
	    } # Jafrabad state
1700.1.1  = { citysize = 8340 }
1750.1.1  = { citysize = 9346 }
1800.1.1  = { citysize = 9400 }
