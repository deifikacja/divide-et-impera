#1275 - Toliary

culture = madagasque
religion = animism
capital = "Fort Dauphin"
native_size = 90
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_ferocity = 4.5
native_hostileness = 9
discovered_by = AMB

1500.8.20 = { discovered_by = POR } #Diego Dias
1642.9.24 = { discovered_by = FRA citysize = 1000 owner = FRA controller = FRA trade_goods = slaves }
1674.7.27 = { citysize = 0 owner = XXX controller = XXX trade_goods = unknown }
1675.1.1 = { discovered_by = MER }
1685.1.1 = { discovered_by = ANT discovered_by = MEN }
1690.1.1 = { discovered_by = BOI }
1691.2.11 = {  }
1712.1.1 = { discovered_by = TAM }
1768.1.1 = { citysize = 1000 owner = FRA controller = FRA trade_goods = slaves }
1771.1.1 = { citysize = 0 owner = XXX controller = XXX trade_goods = unknown }
1817.10.23 = { citysize = 1000 owner = MDG controller = MDG add_core = MDG discovered_by = MDG trade_goods = slaves }