#842 - Pet�n

owner = TAY
controller = TAY
culture = mayan
religion = animism
capital = "Tayasal"
trade_goods = coffee
hre = no
base_tax = 2
manpower = 1
citysize = 1000
add_core = TAY
discovered_by = MAY
discovered_by = MMA
discovered_by = QUI
discovered_by = TAY
discovered_by = ACA
discovered_by = CHT
discovered_by = CPE
discovered_by = TUX
discovered_by = UAY
discovered_by = CUP
discovered_by = ECA
discovered_by = AKC

discovered_by = ZAP
discovered_by = AZT
discovered_by = MIX
discovered_by = TLA
discovered_by = YPT
discovered_by = MTT
discovered_by = TAR
discovered_by = HST

1450.1.1  = { citysize = 1250 }
1500.1.1  = { citysize = 1570 }
1522.1.1  = { discovered_by = SPA }
1550.1.1  = { citysize = 2700 }
1600.1.1  = { citysize = 3240 }
1629.1.1 = { citysize = 2000 }#Spanish attack
1650.1.1  = { citysize = 2100 }
1697.3.13 = {	capital = "Flores"
		owner = SPA
		controller = SPA
		culture = castillian
		religion = catholic
		citysize = 1850
	    } # The remaining pop. was forced to resettle at Ciudad de Flores
1725.1.1  = { add_core = SPA }
1750.1.1  = { citysize = 4870 }
1800.1.1  = { citysize = 5320 }
1808.2.1   = { add_core = CAM } # France invades Spain
1810.9.16  = { owner = MEX controller = SPA } # Mexican War of Independence
1823.1.1 = { owner = CAM controller = CAM }