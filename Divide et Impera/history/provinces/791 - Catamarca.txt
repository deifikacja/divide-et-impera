#791 - Catamarca

owner = DIA
controller = DIA
culture = diaguita
religion = animism
capital = "Catamarca"
trade_goods = gold
hre = no
base_tax = 2
manpower = 1
citysize = 700
add_core = DIA
discovered_by = INC
discovered_by = CHM
discovered_by = INC
discovered_by = CHM
discovered_by = HNV
discovered_by = MNO
discovered_by = PCN
discovered_by = QUT
discovered_by = CII
discovered_by = HNC
discovered_by = CAA
discovered_by = CRC
discovered_by = TRC
discovered_by = CNC
discovered_by = SCN

discovered_by = CNE
discovered_by = CNB
discovered_by = NZC
discovered_by = AYA
discovered_by = DIA
discovered_by = HUA
discovered_by = TIT
discovered_by = ATA

1499.1.1  = { owner = INC controller = INC add_core = INC }
1533.8.29 = {   owner = XXX
		controller = XXX
		native_size = 30
		native_ferocity = 1
		native_hostileness = 7	
		trade_goods = unknown
	    }
1558.1.1  = { discovered_by = SPA } # Juan P�rez de Zurita, the colony is under constant attack
1683.1.1  = {	owner = SPA
		controller = SPA
		citysize = 150
		culture = castillian
		religion = catholic
		trade_goods = gold
	    } # First real permanent settlement
1700.1.1  = { citysize = 400 }
1708.1.1  = { add_core = SPA }
1750.1.1  = { citysize = 890 add_core = LAP }
1800.1.1  = { citysize = 1300 }
1816.7.9  = {	owner = LAP
		controller = LAP
		remove_core = SPA
	    } # Argentina is declared independent