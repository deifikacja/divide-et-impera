#710 - Hotan

owner = TRF
controller = TRF
culture = mongol
religion = shamanism
capital = "Hotan"
trade_goods = wool
hre = no
base_tax = 2
manpower = 1
citysize = 8540
add_core = TRF
add_core = HOT
fort1 = yes
discovered_by = TRF
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = ordu

1356.1.1 = { add_core = CHG }
1369.1.1 = { owner = CHG controller = CHG }
1455.1.1 = {	owner = TRF
		controller = TRF
	   }
1500.1.1 = { citysize = 9354 religion = tibetan_buddhism }
1520.1.1 = { marketplace = yes }
1533.1.1 = {	owner = HOT
		controller = HOT
	   }
1550.1.1 = { citysize = 10200 }
1600.1.1 = { citysize = 11248 }
1650.1.1 = { citysize = 13548 }
1666.1.1 = {	owner = KSG
		controller = KSG
		add_core = KSG
	   } 
1700.1.1 = { citysize = 15202 }
1750.1.1 = { citysize = 16877 }
1758.1.1 = {	owner = HOT
		controller = HOT
		add_core = QNG
	   } # The Qing Dynasty
1800.1.1 = { citysize = 17584 }