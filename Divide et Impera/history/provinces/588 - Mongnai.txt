#588 - Mongnai

owner = YAW
controller = YAW
culture = shan
religion = buddhism
capital = "Nyaung Shwe"
trade_goods = wool
hre = no
base_tax = 2
manpower = 2
citysize = 3075
add_core = YAW
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = indian

1450.1.1 = { citysize = 3710 }
1500.1.1 = { citysize = 4670 }
1555.1.1 = {	owner = TAU
		controller = TAU
		add_core = TAU
	   } # The Shan dynasty is overthrown
1581.1.1 = {	owner = YAW
		controller = YAW
	   	remove_core = YAW
	   } # Very loosely controlled
1600.1.1 = { citysize = 5200 }
1650.1.1 = { citysize = 6540 }
1700.1.1 = { citysize = 7600 }
1750.1.1 = { citysize = 8240 }
1753.1.1 = {	owner = TAU
		controller = TAU
	   	add_core = TAU
	   } # Burmese campaigns
1762.1.1 = {	owner = YAW
		controller = YAW
	   	remove_core = TAU
	   } # Burmese campaigns
1800.1.1 = { citysize = 9800 }