#881 - Piro

owner = ANA
controller = ANA
add_core = ANA
culture = pueblo 
religion = animism 
capital = "Acoma" 
trade_goods = salt
temple = yes
hre = no
base_tax = 2 
manpower = 1 
citysize = 2000

discovered_by = ANA


discovered_by = APA
discovered_by = PUB
discovered_by = NAO
discovered_by = COM

1500.1.1 = { temple = no native_size = 8 native_ferocity = 4 native_hostileness = 4 owner = XXX controller = XXX religion = shamanism remove_core = ANA }
1541.1.1  = { discovered_by = SPA } # Francisco V�squez de Coronado
1598.1.1  = {	owner = SPA
		controller = SPA
		citysize = 300
		culture = castillian
		religion = catholic 
	    } # First Spanish colonists
1623.1.1  = { add_core = SPA citysize = 1000 }
1650.1.1  = { citysize = 1800 }
1660.1.1  = { revolt_risk = 4 }
1670.1.1  = { revolt_risk = 5 }
1672.1.1  = { revolt_risk = 0 }
1680.1.1  = { revolt = { type = nationalist_rebels size = 1 } controller = REB culture = pueblo } # Pueblo revolt
1692.1.1  = { revolt = { } controller = SPA } # The Spanish reestablished control & the colonists returned
1700.1.1  = { citysize = 1990 }
1750.1.1  = { citysize = 2420 add_core = MEX }
1800.1.1  = { citysize = 2820 }
1810.9.16 = { owner = MEX controller = SPA } # Mexican War of Independence
1821.8.24 = { controller = MEX }#Treaty of C�rdoba