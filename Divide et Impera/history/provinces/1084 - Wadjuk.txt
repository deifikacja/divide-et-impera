#1084 - Wadjuk

culture = aboriginal
religion = animism
capital = "Perth"
hre = no
trade_goods = unknown
manpower = 1
base_tax = 2
native_size = 5
native_ferocity = 0.5
native_hostileness = 9

1697.1.10 = { discovered_by = NED } # Willem de Vlamingh
1829.6.11 = { owner = GBR controller = GBR add_core = GBR citysize = 100 capital = "Perth" trade_goods = grain } # James Stirling
