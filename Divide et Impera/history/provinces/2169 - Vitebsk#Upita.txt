#2169 Upita

owner = LIT
controller = LIT
culture = lithuanian
religion = baltic_pagan
capital = "Upyta"
trade_goods = grain
hre = no
base_tax = 2
manpower = 3
citysize = 2195
fort1 = yes
add_core = LIT
discovered_by = ordu
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1381.1.1   = { revolt = { type = pretender_rebels size = 0 } } #Struggle with K�stutis
1382.1.1   = { revolt = {} }
1398.1.1   = { owner = TEU controller = TEU add_core = TEU }
1408.1.1   = { revolt_risk = 5 } # Lihtuanian uprising
1411.2.1   = { revolt_risk = 0  owner = LIT controller = LIT }
1417.1.1   = { religion = catholic } # Samogitia is the last lithuanian land to be christianized
1422.1.1   = { remove_core = TEU }
1500.1.1   = { citysize = 2212 }
1550.1.1   = { citysize = 2385 }
1556.1.1 = { capital = "Paneve�ys" }
1569.7.4   = {	owner = RZP
		controller = RZP
		add_core = RZP
		remove_core = POL
		capital = "Poniewie�"
	     }
1600.1.1   = { citysize = 2923 }
1650.1.1   = { citysize = 3139 }
1655.10.20 = { controller = SWE } # The Deluge, betrayal of Janusz Radziwill
1656.1.1   = { controller = RZP } # death of Janusz Radziwill
1700.1.1   = { citysize = 3246 }
1701.1.1   = { controller = SWE } # Swedish occupation
1709.1.1   = { controller = RZP } # Occupation ended
1710.1.1   = { citysize = 3348 } # Great plague, lost almost two thirds of its population
1750.1.1   = { citysize = 3460 } 
1794.3.24  = { revolt_risk = 6 } # Kosciuszko uprising
1794.11.16 = { revolt_risk = 0 }
1795.10.1 = { owner = RUS controller = RUS add_core = RUS } # Annexed by Tsarist Russia, third partition
1800.1.1   = { citysize = 3500 }
1807.7.9  = {	
		add_core = DOW
		remove_core = RZP
	    } # The Duchy of Warsaw is established instead of Poland
1812.6.28  = { controller = RFR } # Occupied by French troops
1812.12.10 = { controller = RUS }
1815.6.9  = { remove_core = DOW }