#1162 - Wukari

culture = jukun 
religion = animism
capital = "Kwararafa"
trade_goods = unknown
hre = no
base_tax = 2
manpower = 1
native_size = 60
native_ferocity = 4.5
native_hostileness = 9

1545.1.1 = { citysize = 2000 trade_goods = grain owner = WUK controller = WUK add_core = WUK discovered_by = WUK }
1685.1.1 = { revolt_risk = 3 } #Displaced Jukun fleeing invasion from Borno disturb region
1690.1.1 = { revolt_risk = 0 }
1700.1.1 = { citysize = 2600 }
1800.1.1 = { citysize = 3000 }