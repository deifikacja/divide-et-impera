#585 - Toungoo

owner = TAU
controller = TAU
culture = burmese
religion = buddhism
capital = "Toungoo"
trade_goods = grain
hre = no
base_tax = 6
manpower = 1
citysize = 17487
add_core = TAU
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = indian

1377.1.1 = { revolt = { type = pretender_rebels size = 0 } controller = REB }#Period of disorder
1421.1.1 = { revolt = { } controller = TAU }#Period of disorder ended
1450.1.1 = { citysize = 18100 }
1482.1.1 = { revolt = { type = pretender_rebels size = 0 } controller = REB }#Period of disorder
1486.1.1 = { revolt = { } controller = TAU }#Period of disorder ended
1500.1.1 = { citysize = 19800 }
1550.1.1 = { citysize = 21440 }
1560.1.1 = { marketplace = yes }
1600.1.1 = { citysize = 24048 }
1650.1.1 = { citysize = 26557 }
1700.1.1 = { citysize = 29403 }
1750.1.1 = { citysize = 31570 }
1800.1.1 = { citysize = 35405 }