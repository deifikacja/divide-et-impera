#356 - Cyrenaica

owner = CYR
controller = CYR
culture = tuareg
religion = sunni
capital = "Benghazi"
trade_goods = fish
hre = no
base_tax = 3
manpower = 1
citysize = 5800
add_core = CYR
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = TUG
discovered_by = FZZ

1359.1.1 = { owner = MAM controller = MAM add_core = MAM }
1450.1.1 = { citysize = 6400 }
1500.1.1 = { citysize = 7010 }
1517.1.1 = {	owner = TUR
		controller = TUR
		add_core = TUR
		remove_core = CYR
	   } # Conquered by Ottoman troops
1550.1.1 = { citysize = 7664 }
1600.1.1 = { citysize = 8021 }
1650.1.1 = { citysize = 8877 }
1660.1.1 = { marketplace = yes }
1700.1.1 = { citysize = 9434 }
1711.1.1 = { revolt = { type = nationalist_rebels size = 0 } controller = REB }
1714.1.1 = { owner = CYR controller = CYR }
1750.1.1 = { citysize = 10100 }
1800.1.1 = { citysize = 11954 }