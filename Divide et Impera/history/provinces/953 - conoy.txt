#953 - Conoy

culture = nanticoke
religion = shamanism
capital = "Conoy"
trade_goods = unknown
base_tax = 5
manpower = 1
native_size = 15
native_ferocity = 2
native_hostileness = 7
discovered_by = DEL
discovered_by = POW

1607.1.1  = { discovered_by = ENG } # John Smith
1615.1.1  = { discovered_by = FRA } # �tienne Br�l�
1634.3.24 = {	owner = ENG
		controller = ENG
		culture = english
	    	religion = episcopalism
	    	capital = "St. Mary's city"
		citysize = 358	
		trade_goods = tobacco
	    } # Founding of St. Mary's city
1707.5.12 = { owner = GBR controller = GBR } 
1721.1.1  = { add_core = GBR }
1729.7.30 = { capital = "Baltimore" }
1750.1.1  = { citysize = 1522 }
1764.7.1  = {	culture = american
		revolt_risk = 6
	    } # Growing unrest
1776.7.4  = {	owner = USA
		controller = USA
		add_core = USA
	    } # Declaration of independence
1782.11.1 = {	remove_core = GBR 
		revolt_risk = 0
	    } # Preliminary articles of peace, the British recognized Amercian independence
1790.1.1  = { capital = "Washington" }
1800.1.1  = { citysize = 4180 }