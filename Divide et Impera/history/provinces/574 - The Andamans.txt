#574 - The Andamans

culture = madagascan
religion = animism
capital = "The Andamans"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 5
native_ferocity = 1
native_hostileness = 7
discovered_by = indian

1502.1.1  = { discovered_by = POR }
1789.11.1 = {	owner = BNG
		controller = BNG
		add_core = BNG
		citysize = 150
		trade_goods = fish
	    } # Bengalian colony
1796.5.1  = {	citysize = 0
		manpower = 1
		native_size = 5
		native_ferocity = 1
		owner = XXX
		controller = XXX
		remove_core = BNG
	    } # The colony is abandoned