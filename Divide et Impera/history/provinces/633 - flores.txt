#633 - Flores

culture = malay-polynesian
religion = animism
capital = "Flores"
trade_goods = unknown
hre = no
base_tax = 4
manpower = 2
native_size = 10
native_ferocity = 1
native_hostileness = 3

1515.1.1 = {	discovered_by = POR
		owner = POR
		controller = POR
	   	capital = "Solor"
		religion = catholic
		culture = portugese
	   	citysize = 400
		trade_goods = spices
	   } # Solor became the center of the Portuguese trade
1540.1.1 = { citysize = 1080 add_core = POR }
1561.1.1 = { fort1 = yes }
1605.1.1 = {	citysize = 0
		native_size = 5
		native_ferocity = 1
		native_hostileness = 2
		owner = XXX
		controller = XXX
		remove_core = POR 
		culture = papuan
		religion = animism
		trade_goods = unknown
	   } # The Portuguese are driven out by the natives
1616.1.1 = {	owner = POR
		controller = POR
		add_core = POR
		religion = catholic
		culture = portugese
		revolt_risk = 4
		trade_goods = spices
	   } # Continous struggles between the Portuguese & the Dutch
1618.1.1 = {	owner = NED
		controller = NED
		add_core = NED
		culture = dutch
		religion = reformed
		citysize = 1540
	   } # The Dutch managed to take control
1650.1.1 = { citysize = 2300 }
1657.1.1 = { remove_core = POR } # The Portuguese began to lose control
1700.1.1 = { citysize = 2980 }
1750.1.1 = { citysize = 3500 }
1800.1.1 = { citysize = 4500 }