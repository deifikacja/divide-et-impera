#1099 - Socotra

owner = HDR
controller = HDR
culture = somali
religion = eastern_churches
capital = "Socotra"
trade_goods = fish
hre = no
base_tax = 2
manpower = 1
citysize = 1000
add_core = HDR
add_core = MAH
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = sahel

1450.1.1 = { citysize = 1200 }
1488.1.1 = { discovered_by = POR }
1507.1.1 = { owner = POR controller = POR } # Conquered by the Portuguese
1511.1.1 = { owner = MAH controller = MAH } # Mahri and Sucutra sultanate
1550.1.1 = { discovered_by = TUR religion = sunni }
1567.1.1 = { citysize = 1080 }
1700.1.1 = { citysize = 1240 }
1750.1.1 = { citysize = 1875 }
1800.1.1 = { citysize = 2340 }