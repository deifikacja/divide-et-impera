#1198 - Tabora

culture = bantu
religion = animism
capital = "Tabora"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 5
native_ferocity = 0.5
native_hostileness = 1