#42 - Warmia

owner = TEU
controller = TEU
capital  = "Wurmedite" #Orneta - Seat of the bishops of Warmia
culture = east_german
religion = catholic
trade_goods = naval_supplies
hre = no
base_tax = 4
manpower = 1
citysize = 5000
fort1 = yes
fort2 = yes
add_core = TEU
add_core = WRI
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1350.1.1 = { capital = "Heilsberg" } #Lidzbark
1450.1.1   = { citysize = 5000 }
1454.3.6   = { add_core = POL } # Beginning of the "thirteen years war"
1466.10.19 = {	owner = POL	
		discovered_by = ordu
		controller = POL
		remove_core = TEU
	     } # "Peace of Torun"
1500.1.1   = { marketplace = yes citysize = 5977 }
1550.1.1   = { citysize = 6209 }
1569.7.4   = {	capital = "Lidzbark" #Polish name
		owner = RZP
		controller = RZP
		add_core = RZP
		remove_core = POL
	     } #Union of Lublin
1588.1.1   = { revolt = { type = revolutionary_rebels size = 0 } controller = REB } # Civil war
1589.1.1   = { revolt = {} controller = RZP revolt_risk = 0 } # Coronation of Sigismund III
1600.1.1   = { citysize = 6657 }
1618.1.1   = { university = yes } # Catholic university, Braunsberg, 1568
1650.1.1   = { citysize = 7361 }
1655.8.1   = { controller = SWE } # The Deluge
1660.1.1   = { controller = RZP }
1700.1.1   = { citysize = 8366 } # Allenstein is burnt down, epidemic
1703.1.1   = { controller = SWE } # Occupied by Sweden
1709.1.1   = { controller = POL }
1750.1.1   = { citysize = 8725 }
1772.8.5   = {	capital = "Heilsberg"
		owner = PRU
		controller = PRU
		add_core = PRU
	     } 
1795.1.1   = { religion = protestant } # Due to the incorporation into the Kingdom of Prussia
1800.1.1   = { citysize = 9500 }
1807.7.9   = { remove_core = RZP }