#702 - Ulaanchab

owner = YUA
controller = YUA
culture = mongol
religion = tibetan_buddhism
capital = "Hohhot"
trade_goods = grain
hre = no
base_tax = 2
manpower = 3
citysize = 8245
add_core = YUA
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = ordu

1370.1.1 = { owner = MNG controller = MNG add_core = MNG remove_core = YUA add_core = KHA }
1450.1.1 = { citysize = 8600 }
1500.1.1 = { citysize = 9345 }
1543.1.1 = { revolt_risk = 8  } # Fighting broke out between the Mongols
1550.1.1 = { citysize = 11900 }
1552.1.1 = { revolt_risk = 0 } # The Oirads are defeated & Mongolia is reunited under Altan Khan
1586.1.1 = { religion = tibetan_buddhism } # State religion
1600.1.1 = { citysize = 13422 }
1634.1.1 = {	owner = MCH
		controller = MCH
		add_core = MCH
		remove_core = KHA
	   } # Part of the Manchu empire
1650.1.1 = { citysize = 14986 }
1683.1.1 = {	owner = QNG
		controller = QNG
		add_core = QNG
		remove_core = MCH
	   } # The Qing Dynasty
1700.1.1 = { citysize = 16420 }
1750.1.1 = { citysize = 17985 }
1800.1.1 = { citysize = 18457 }