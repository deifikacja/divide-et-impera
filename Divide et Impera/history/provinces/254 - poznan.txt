#254 - Poznan

owner = POL	
discovered_by = ordu
controller = POL
capital = "Pozna�"
culture = polish
religion = catholic
trade_goods = cloth
hre = no
base_tax = 6
manpower = 4
citysize = 9000
fort1 = yes
add_core = POL
temple = yes		# Cathedral Basilica of St. Peter and St. Paul
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1352.9.2 = { revolt = { type = noble_rebels size = 0.3 leader = "Ma�ko Borkowic"} controller = REB }
1358.2.16 = { revolt = {} controller = POL }
1368.1.1  = { marketplace = yes }# King Jagie��o's privileges
1400.1.1   = { citysize = 10500 }
1450.1.1   = { citysize = 12000 }
1500.1.1   = { citysize = 14450 }
1550.1.1   = { citysize = 17000 }
1569.7.4   = {	owner = RZP
		controller = RZP
		add_core = RZP
		remove_core = POL
	     } #Union of Lublin
1575.1.1   = { regimental_camp = yes }
1588.1.1   = { revolt = { type = revolutionary_rebels size = 0 } controller = REB } # Civil war, Polish succession
1589.1.1   = { controller = RZP } # Coronation of Sigismund III
1600.1.1   = { citysize = 20000 }
1650.1.1   = { citysize = 23000 constable = yes }
1655.7.31  = { controller = SWE } # Swedish invasion
1657.8.28  = { controller = RZP citysize = 14000 } # Polish troops liberate city, but the population is decimated
1700.1.1   = { citysize = 5000 } # Due to epidemics, catastophes & the Swedish invasion
1733.1.1   = { revolt = { type = revolutionary_rebels size = 3 } controller = REB citysize = 6000 } # The war of Polish succession
1735.1.1   = { controller = RZP tax_assessor = yes }
1750.1.1   = { citysize = 6600 courthouse = yes } # Several waves of Dutch & rural settlers
1772.2.17  = { add_core = PRU citysize = 15000 } # First partition of Poland, only lost the northern part
1793.1.23  = { owner = PRU controller = PRU } # Second partition of Poland, the remaining part came under Prussian control
1794.3.24  = { revolt = { type = nationalist_rebels size = 2 } controller = REB  fort2 = yes } # Kosciuszko uprising, Prussia lost control briefly
1794.11.16 = { controller = PRU citysize = 12538 } # The end of the uprising
1800.1.1   = { citysize = 18779 }
1806.11.3  = { revolt = { type = nationalist_rebels leader = "Wincenty Aksamitowski" size = 2 } controller = REB } # Polish uprising instigated by Napoleon
1807.7.9   = {	revolt = {}
		owner = DOW
		controller = DOW
		add_core = DOW
		remove_core = RZP
	     	remove_core = PRU
	     } # The Duchy of Warsaw is established after the treaty of Tilsit, ruled by Frederick Augustus I of Saxony
1812.12.12 = { controller = RUS }
1815.6.9   = {	owner = PRU
		controller = PRU
		add_core = PRU
		remove_core = DOW
		add_core = POL
	     } # Returned to Prussia after the Congress of Vienna
