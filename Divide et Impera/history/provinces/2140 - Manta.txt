#819 - Guayaquil

owner = MNO
controller = MNO
add_core = MNO
culture = arawak
religion = animism
capital = "Manta"
trade_goods = cloth#Los Mantenos were perfect weavers
hre = no
base_tax = 3
manpower = 1
citysize = 1150 
discovered_by = INC
discovered_by = CHM
discovered_by = INC
discovered_by = CHM
discovered_by = HNV
discovered_by = MNO
discovered_by = PCN
discovered_by = QUT
discovered_by = CII
discovered_by = HNC
discovered_by = CAA
discovered_by = CRC
discovered_by = TRC
discovered_by = CNC
discovered_by = SCN

discovered_by = CNE
discovered_by = CNB
discovered_by = NZC
discovered_by = AYA
discovered_by = DIA
discovered_by = HUA
discovered_by = TIT
discovered_by = ATA
1463.1.1  = { owner = INC controller = INC add_core = INC }
1533.1.1  = { discovered_by = SPA } # Sebasti�n de Benalc�zar
1533.8.29  = {	owner = SPA
		controller = SPA
		remove_core = INC
		citysize = 280
		culture = castillian
		religion = catholic
	    } # Founded by Sebasti�n de Benalc�zar
1550.1.1  = {	add_core = SPA
		citysize = 1260		
	    }
1600.1.1  = { citysize = 2210 }
1650.1.1  = { citysize = 2600 }
1700.1.1  = { citysize = 2863 }
1750.1.1  = { citysize = 3900 add_core = COL }
1800.1.1  = { citysize = 5400 }
1819.12.17 = {	owner = COL
		controller = COL
		remove_core = SPA
	     } # The establishment of Gran Colombia
