#1192 - Quelimane

owner = KIL
controller = KIL
add_core = KIL
culture = bantu
religion = animism
capital = "Quelimane"
citysize = 3000
manpower = 1
trade_goods = ivory
hre = no
base_tax = 2
discovered_by = KIL
discovered_by = MAQ
discovered_by = ZZA
discovered_by = ZIM
discovered_by = ADA
discovered_by = GZI
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1450.1.1  = { citysize = 3450 }
1498.2.15 = { discovered_by = POR } #Vasco Da Gama
1500.1.1  = { citysize = 4200 }
#1544 Portuguese establish tradepost
1571.1.1  = {	owner = POR
		controller = POR
		citysize = 2000
		revolt_risk = 4
	    } #Portuguese take control of Quelimane as their base for invasion of Mutapa
1596.1.1  = { add_core = POR }
1631.1.1  = { revolt_risk = 7 } #Maravi invade region
1632.1.1  = { revolt_risk = 0 } #Maravi expelled from the region
1700.1.1  = { citysize = 3000 }  
1750.1.1  = { trade_goods = grain citysize = 4000 } #becomes major center of cereal production to feed slaves
1800.1.1  = { citysize = 4500 }