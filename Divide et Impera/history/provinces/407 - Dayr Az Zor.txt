#407 - Dayr Az Zor

owner = JAL
controller = JAL
culture = al_suryah_arabic
religion = sunni
capital = "Dayr Az Zor"
trade_goods = grain
hre = no
base_tax = 2
manpower = 1
citysize = 2570
add_core = JAL
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = western
discovered_by = TUR
discovered_by = BYZ
discovered_by = TIM
discovered_by = SHY
discovered_by = MUG
discovered_by = ordu

1383.1.1 = { owner = QAR controller = QAR }
1401.3.1 = { owner = TIM controller = TIM }
1408.1.1 = { owner = QAR controller = QAR }
1432.1.1 = { remove_core = JAL }
1468.1.1 = {	owner = AKK
		controller = AKK
		add_core = AKK
		remove_core = QAR
	   }
1500.1.1 = { citysize = 3150 }
1516.1.1 = {	owner = TUR
		controller = TUR
		add_core = TUR
	   } # Part of the Ottoman Empire
1550.1.1 = { citysize = 4200 }
1600.1.1 = { citysize = 6321 }
1650.1.1 = { citysize = 7005 }
1700.1.1 = { citysize = 8635 }
1750.1.1 = { citysize = 10700 add_core = SYR }
1771.1.1 = { controller = MAM } # Ali Bey gained control of Syria, reconstituting the Mamluk state
1772.1.1 = { controller = TUR }
1800.1.1 = { citysize = 14250 }