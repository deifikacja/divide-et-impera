#909 - Mandan

culture = mandan
religion = shamanism
capital = "Mandan"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 35
native_ferocity = 2
native_hostileness = 9
discovered_by = NAK
discovered_by = DAK
discovered_by = LAK

1732.1.1 = { discovered_by = FRA } # La Vérendrye
1803.3.4 = { add_core = USA }#The Louisiana purchase
1804.12.24 = { 	owner = USA
		controller = USA
		citysize = 100
		trade_goods = grain
		capital = "Fort Mandan" 
}#Lewis and Clark
