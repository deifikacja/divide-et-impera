#483 - Turks Islands

culture = carib
religion = animism
capital = "Grand Turk"
trade_goods = unknown
hre = no
base_tax = 3
manpower = 1
native_size = 10
native_ferocity = 1
native_hostileness = 3

1512.1.1  = {	discovered_by = CAS
		owner = CAS
		controller = CAS
		culture = castillian
		religion = catholic
		citysize = 500
		trade_goods = cotton
	    } # Juan Ponce de Le�n
1516.1.23 = {	discovered_by = SPA
		owner = SPA
		controller = SPA
	    }
1530.1.1  = { citysize = 1022 }
1537.1.1  = { add_core = SPA  }
1550.1.1  = { citysize = 1400 }
1600.1.1  = { citysize = 1664 }
1650.1.1  = { citysize = 1987 }
1678.1.1  = {	citysize = 2500 
		owner = ENG
		controller = ENG
		remove_core = SPA
		culture = english
		religion = episcopalism
	    } # Settlers from Bermuda
1681.1.1  = { capital = "Cockburn Town" }
1707.5.12 = { owner = GBR controller = GBR }
1725.1.1  = { add_core = GBR }
1750.1.1  = { citysize = 2560 }
1753.1.1  = { revolt_risk = 6 } # France claimed the island
1754.1.1  = { revolt_risk = 0  } # ...but were repelled just a year later
1800.1.1  = { citysize = 3100 }
1813.1.1  = { citysize = 2000 } # The first of two hurricanes
1815.1.1  = { citysize = 2400 }