#Fyn-Langeland-Lolland-Falster, incl. Odense, Nyborg and Nyk�bing

add_core = DAN
owner = DAN
controller = DAN
culture = danish
religion = catholic
hre = no
base_tax = 3
trade_goods = fish
manpower = 3
capital = "Odense"
fort1 = yes
citysize = 2150 # Estimated
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1450.1.1  = { citysize = 2450 }
1500.1.1  = { marketplace = yes }
1536.1.1  = { religion = protestant }
1550.1.1  = { citysize = 2800 }
1600.1.1  = { citysize = 3500 }
1650.1.1  = { citysize = 4508 }
1658.1.31 = { controller = SWE } #Karl X Gustavs First Danish War-Captured by Karl X Gustav
1658.2.26 = { controller = DAN } #The Peace of Roskilde
1672.1.1  = { citysize = 5808 }
1750.1.1  = { citysize = 6209 }
1769.1.1  = { citysize = 7609 }
1800.1.1  = { citysize = 8000 }