#642 - Bone


owner = BNE
controller = BNE
culture = sulawesi
religion = animism
capital = "Bone"
trade_goods = spices
hre = no
base_tax = 5
manpower = 2
citysize = 4200
add_core = BNE
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM

1450.1.1 = { citysize = 4940 }
1511.1.1 = { discovered_by = POR }
1550.1.1 = { citysize = 5679 }
1608.1.1 = { religion = sunni }
1643.1.1 = {	owner = MKS
		controller = MKS
		add_core = MKS
		citysize = 6800
	   }
1667.1.1 = {	owner = NED
		controller = NED
		citysize = 7100
	    }
1691.1.1 = { add_core = NED }
1700.1.1 = { citysize = 7980 }
1750.1.1 = { citysize = 8440 }
1800.1.1 = { citysize = 9500 }
1814.1.1 = {	owner = GBR
		controller = GBR
	   	culture = english
	   	religion = protestant
	   } # Under British control
1816.1.1 = { owner = NED controller = NED } # Returned to the Dutch