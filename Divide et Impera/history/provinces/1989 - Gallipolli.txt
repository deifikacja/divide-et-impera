#1989 - Gallipoli

owner = TUR
controller = TUR
add_core = TUR
culture = turkish #The city was abandoned when Turks arrived in 1355
religion = sunni #The city was abandoned when Turks arrived in 1355
capital = "Gelibolu"
trade_goods = fish
hre = no
base_tax = 2 #The city was abandoned when Turks arrived in 1355
manpower = 2 #The city was abandoned when Turks arrived in 1355
citysize = 1000 #The city was abandoned when Turks arrived in 1355
fort1 = yes
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1402.1.1  = { revolt_risk = 5 } # Interregnum
1410.1.1  = { revolt_risk = 3 }
1413.1.1  = { revolt_risk = 0 }
1450.1.1  = { citysize = 1100 }
1462.1.1  = { remove_core = BYZ }
1481.6.1  = { revolt = { type = pretender_rebels size = 0 } controller = REB } # Civil war, Bayezid & Jem
1482.7.26 = { revolt = {} controller = TUR } # Jem escapes to Rhodes
1500.1.1  = { citysize = 5200 }
1550.1.1  = { citysize = 5300 }
1555.1.1  = { revolt_risk = 5 } # General discontent with the Janissaries' dominance
1556.1.1  = { revolt_risk = 0 }
1600.1.1  = { citysize = 5400 }
1615.1.1  = { base_tax = 4 } #The Decentralizing Effect of the Provincial System
1621.1.1  = { revolt_risk = 6 } # Osman II's reforms against the Janissaries
1622.5.20 = { revolt_risk = 7 } # Osman II is murdered
1622.6.1  = { controller = TUR revolt_risk = 0 } # Mustafa I, estimated
1623.1.1  = { revolt_risk = 5 } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { revolt_risk = 0 } # Murad tries to quell the corruption
1650.1.1  = { citysize = 5500 } # Situation began to stabilize, Greek pop. returned
1700.1.1  = { citysize = 5600 }
1718.1.1  = { revolt_risk = 3 base_tax = 3 } # Lale Devri (the tulip age), not appreciated by everyone  
1720.1.1  = { revolt_risk = 0 }
1750.1.1  = { citysize = 5700 add_core = GRE } # Great fire (1745), earthquake in 1751, city declined
1795.1.1  = { revolt_risk = 6 } # Reforms by Sultan Selim III, tried to replace th Janissary corps
1800.1.1  = { citysize = 5800 }
1821.1.26 = { revolt_risk = 2 }
1821.3.17 = { revolt_risk = 4 } #revolt began in Peloponese