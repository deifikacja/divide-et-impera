#462 - Georgia

owner = GEO
controller = GEO
culture = circassian
religion = eastern_churches
capital = "Maikop"
trade_goods = cattle
hre = no
base_tax = 3
manpower = 2
citysize = 3600
add_core = ADG 
fort1 = yes
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = CRI
discovered_by = KAZ
discovered_by = GOL
discovered_by = AST
discovered_by = NOG
discovered_by = BSH
discovered_by = QAS
discovered_by = TIM
discovered_by = ordu

1387.1.1 = {	owner = ADG
		controller = ADG
		add_core = TIM
	   } #Timur the Lenk
1405.1.1  = {   owner = GEO
		controller = GEO
	    }
1466.1.1  = {	owner = ADG
		controller = ADG
		remove_core = GEO
	    } # Contested zone between Russia and Ottomans
1500.1.1  = { citysize = 4800 }
1550.1.1  = { citysize = 5389 }
1600.1.1  = { citysize = 6320 religion = sunni }
1614.1.1  = { revolt_risk = 6 } # Overrun several times by Persian troops
1617.1.1  = { revolt_risk = 2 citysize = 4000 } # Thousands were killed or resettled in Iran
1640.1.1  = { constable = yes }
1650.1.1  = { citysize = 5200 marketplace = yes }
1659.1.1  = { revolt_risk = 4 } # Revolts, the garrisons in Kakheti were defeated by the Kakhetians
1665.1.1  = { revolt_risk = 0 }
1700.1.1  = { citysize = 6188 }
1735.1.1  = { tax_assessor = yes }
1750.1.1  = { citysize = 6800 }
1795.1.1  = { citysize = 5700 add_core = RUS } #but totally conquered in 1864
1800.1.1  = { citysize = 6790 }