#841 - Guatemala

owner = MMA #Mam Maya
controller = MMA
culture = mayan
religion = animism 
capital = "Zaculeu" 
trade_goods = cattle
hre = no 
base_tax = 3 
manpower = 1 
citysize = 2500
add_core = MMA
temple = yes
discovered_by = MAY
discovered_by = MMA
discovered_by = QUI
discovered_by = TAY
discovered_by = ACA
discovered_by = CHT
discovered_by = CPE
discovered_by = TUX
discovered_by = UAY
discovered_by = CUP
discovered_by = ECA
discovered_by = AKC

discovered_by = ZAP
discovered_by = AZT
discovered_by = MIX
discovered_by = TLA
discovered_by = YPT
discovered_by = MTT
discovered_by = TAR
discovered_by = HST

1450.1.1  = { citysize = 2600 }
1500.1.1  = { citysize = 2820 }
1525.10.1  = {	discovered_by = SPA 
		owner = SPA 
		controller = SPA
		culture = castillian
		religion = catholic
		citysize = 3290
	    } # Conquered by Pedro de Alvarado
1526.1.1  = { capital = "Huehuetenango" citysize = 3450 }#Zaculeu is abandoned after the conquest
1541.1.1  = { citysize = 1340 }
1547.1.1  = { add_core = SPA }
1550.1.1  = { citysize = 1530 } 
1578.1.1  = { discovered_by = ENG } # Drake 
1600.1.1  = { citysize = 1750 } 
1650.1.1  = { citysize = 2135 } 
1700.1.1  = { citysize = 2400 }
1750.1.1  = { citysize = 4008 add_core = MEX } 
1773.1.1  = { citysize = 3100 } # Earthquakes 
1800.1.1  = { citysize = 6540 }
1808.2.1  = { add_core = CAM } # France invades Spain
1810.9.16 = { owner = MEX controller = SPA } # Mexican War of Independence
1823.1.1 = { owner = CAM controller = CAM }