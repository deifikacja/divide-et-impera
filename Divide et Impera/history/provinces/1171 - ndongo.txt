#1171 - Ndongo

owner = NDO
controller = NDO
add_core = KON
add_core = NDO
culture = kongolese
religion = shamanism
capital = "Mbanza Kabassa"
trade_goods = slaves
hre = no
base_tax = 4
manpower = 4
citysize = 1050
discovered_by = NDO
discovered_by = KON

1450.1.1 = { citysize = 1260 } 
1483.1.1 = { discovered_by = POR } # Diogo C�o
1500.1.1 = { citysize = 1780 discovered_by = MAT }
1550.1.1 = { citysize = 2170 }
1600.1.1 = { citysize = 3500 discovered_by = LOA }
1650.1.1 = { citysize = 4220 }
1683.1.1 = {	add_core = POR
		owner = POR
		controller = POR
	    } # Conquered by the Portuguese
1700.1.1 = { citysize = 5900 }
1750.1.1 = { citysize = 6500 }
1800.1.1 = { citysize = 6900 }