#2029 - Vanua Levu

add_core = BUA
culture = malay-polynesian
religion = animism
capital = "Bua"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 5
native_ferocity = 0.5
native_hostileness = 2
discovered_by = BUU
discovered_by = BUA

1800.1.1 = { owner = BUA controller = BUA citysize = 1000 trade_goods = fish }