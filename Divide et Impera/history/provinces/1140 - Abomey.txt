#1140 - Abomey

culture = yorumba
religion = animism
capital = "Allada"
trade_goods = unknown
hre = no
base_tax = 2
native_size = 60
native_ferocity = 4.5
native_hostileness = 9
discovered_by = OUG
discovered_by = YAT
discovered_by = NUN
discovered_by = OKO
discovered_by = OYO
discovered_by = BEN
discovered_by = NUP
discovered_by = IFE
discovered_by = BUS

1550.1.1 = { citysize = 2500 trade_goods = grain owner = ALA controller = ALA add_core = ALA discovered_by = ALA } #Foundation of Alada Kingdom
1625.1.1 = { revolt_risk = 5 add_core = DAH discovered_by = DAH } #Split of Alada Kingdom, rise of Dahomey
1700.1.1 = { capital = "Abomey" citysize = 4700 trade_goods = slaves } #Dahomey establishes dominance in area, becomes major supplier of slaves
1724.1.1 = { owner = DAH controller = DAH } #Agaja moves to eliminate all rivals to Dahomey power
1750.1.1 = { citysize = 10000 }
1800.1.1 = { citysize = 11000 }
1804.1.1 = { discovered_by = SOK }