#913 - Fox

add_core = FOX
culture = fox
religion = shamanism
capital = "Fox"
trade_goods = unknown
hre = no
base_tax = 2
manpower = 1
native_size = 10
native_ferocity = 2
native_hostileness = 5
discovered_by = ILL
discovered_by = FOX
discovered_by = IOW

1600.1.1 = { owner = FOX controller = FOX citysize = 1300 trade_goods = iron }
1684.1.1  = { discovered_by = FRA } # Nicolas Perrot
1685.1.1  = {	owner = FRA
		controller = FRA
		citysize = 250
		culture = cosmopolitan_french
		religion = catholic
	    } # Construction of Fort Antoine
1700.1.1  = { citysize = 700 }
1710.1.1  = { add_core = FRA }
1712.1.1  = { revolt_risk = 5 } # Fox war
1714.1.1  = { revolt_risk = 0 }
1728.1.1  = { revolt_risk = 5 } # Second Fox war
1729.1.1  = { revolt_risk = 0 }
1750.1.1  = { citysize = 1390 }
1763.2.10 = {	discovered_by = GBR
		owner = GBR
		controller = GBR
		remove_core = FRA
		culture = english
	    } # Treaty of Paris - ceded to Britain, France gave up its claim
1774.1.1  = { add_core = QUE } # United with Canada under the Quebec Act of 1774
1783.9.3  = {	owner = USA
		add_core = USA
		remove_core = QUE
	    } # Treaty of Paris, passed to the U.S but still under British control
1800.1.1  = { citysize = 3700 }