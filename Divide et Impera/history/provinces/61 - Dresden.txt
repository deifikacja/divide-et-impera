#61 - Dresden

owner = THU
controller = THU
culture = saxon
religion = catholic
capital = "Dresden"
trade_goods = iron
hre = yes
base_tax = 4
manpower = 1
citysize = 6000
fort1 = yes
marketplace = yes 
add_core = THU
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1400.1.1 = { citysize = 6500 }
1450.1.1 = { citysize = 7000 }
1500.1.1 = { citysize = 8000 }
1531.1.1 = { religion = protestant }
1546.1.1 = { fort2 = yes }
1550.1.1 = { citysize = 10000 }
1547.1.1 = {	owner = SAX
		controller = SAX
		add_core = SAX
		remove_core = THU
	   }
1600.1.1 = { citysize = 12000 courthouse = yes }
1650.1.1 = { citysize = 15000 }
1660.1.1 = { fort3 = yes }
1700.1.1 = { citysize = 40000 }
1750.1.1 = { citysize = 52000 }
1763.1.1 = { citysize = 33350 } # Heavy destruction during the Seven Year's war
1790.8.1 = { revolt_risk = 5 } # Peasant revolt
1791.1.1 = { revolt_risk = 0 }
1800.1.1 = { citysize = 45000 }
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved