#677 - Qamdo

owner = TIB
controller = TIB
culture = tibetan
religion = tibetan_buddhism
capital = "Lhasa"
trade_goods = cattle
hre = no
base_tax = 4
manpower = 3
fort1 = yes
temple = yes
citysize = 12500
add_core = TIB
discovered_by = TIB
discovered_by = ordu

1450.1.1  = { citysize = 13209 }
1500.1.1  = { citysize = 15348 }
1550.1.1  = { citysize = 17410 }
1600.1.1  = { citysize = 19600 }
1650.1.1  = { citysize = 21540 }
1700.1.1  = { citysize = 22570 }
1719.1.1  = { controller = QNG } 
1720.1.1  = { controller = TIB add_core = QNG } # Qing Empire
1750.1.1  = { citysize = 24150 }
1755.10.4 = { revolt_risk = 7 } # Rebellion aginst the Chinese
1757.10.4 = { revolt_risk = 0 }
1800.1.1  = { citysize = 26800 }