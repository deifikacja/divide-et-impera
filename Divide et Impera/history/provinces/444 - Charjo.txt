#444 - Charjo

owner = HER
controller = HER
culture = turkmeni
religion = sunni
capital = "Charjou"
trade_goods = wool
hre = no
base_tax = 2
manpower = 3
citysize = 1432
add_core = HER
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = ordu

1370.1.1 = { owner = TIM controller = TIM add_core = TIM remove_core = HER }#Timur the Lenk
1405.2.18 = { add_core = SAM }
1447.3.12  = {   owner = SAM
		controller = SAM
		remove_core = TIM
	    }#Shahrokh's death - partition of the Empire
1450.1.1 = { citysize = 1800 }
1500.1.1 = { citysize = 1967 }
1506.1.1 = {	owner = SHY
		controller = SHY
		add_core = SHY
		remove_core = SAM
	   } # The Uzbek Shaybanids brings an end to the Timurid dynasty
1510.1.1 = {	owner = SAM
		controller = SAM
	   } # The Samarkand Shaybanids takeover
1531.1.1 = {	owner = SHY
		controller = SHY
	   } # The Bokhara Shaybanids takeover
1550.1.1 = { citysize = 2398 }
1552.1.1 = {	owner = SAM
		controller = SAM
	   } # The Samarkand Shaybanids takeover
1557.1.1 = {	owner = SHY
		controller = SHY
	   } # The Bokhara Shaybanids takeover
1600.1.1 = { citysize = 3433 discovered_by = TUR }
1650.1.1 = { citysize = 4758 }
1700.1.1 = { citysize = 5210 }
1750.1.1 = { citysize = 5754 }
1800.1.1 = { citysize = 6250 }