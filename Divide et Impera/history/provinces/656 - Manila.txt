#656 - Manila

owner = LUZ
controller = LUZ
add_core = LUZ
culture = filipino
religion = animism
capital = "Tondo"
trade_goods = sugar
hre = no
base_tax = 4
manpower = 1
citysize = 1000
discovered_by = CEB
discovered_by = MAC
discovered_by = LUZ
discovered_by = MGD
discovered_by = MNG

1500.1.1 = { capital = "Manila" discovered_by = BEI }#Established by Bruneian invaders
1521.1.1  = { discovered_by = SPA } # Ferdinand Magellan 
1571.1.1  = {	owner = SPA
		controller = SPA
	   	citysize = 200
	   	culture = castillian
	   	religion = catholic
		remove_core = LUZ
	    }
1595.1.1  = { add_core = SPA }
1600.1.1  = { citysize = 1500 }
1650.1.1  = { citysize = 3600 }
1700.1.1  = { citysize = 6258 }
1750.1.1  = { citysize = 11650 }
1762.10.6 = {	controller = GBR
		revolt_risk = 6
	    } # Captured by the British East India Company. Diego Silang rebellion
1763.2.10 = { controller = SPA } # Spain regained control
1763.9.10 = { revolt_risk = 0 } # The revolts are defeated
1800.1.1  = { citysize = 13540 }