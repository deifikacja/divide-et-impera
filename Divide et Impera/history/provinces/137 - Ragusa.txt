#137 - Ragusa

owner = VEN
controller = VEN
culture = croatian
religion = catholic
capital = "Dubrovnik"
trade_goods = naval_supplies
hre = no
base_tax = 5
manpower = 1
citysize = 5500
fort1 = yes
marketplace = yes 
add_core = VEN
add_core = RAG
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1358.2.17 = { owner = RAG controller = RAG } # Treaty of Zadar
1400.1.1  = { citysize = 6000 }
1450.1.1  = { citysize = 6500 }
1500.1.1 = { citysize = 9230 }
1550.1.1 = { citysize = 12990 shipyard = yes } # Ragusa reached its peak
1600.1.1 = { citysize = 13790 fort2 = yes }
1610.1.1 = { fine_arts_academy = yes } # Became the center of the "Dalmatian renaissance"
1630.1.1 = { customs_house = yes }
1650.1.1 = { citysize = 14005 fort3 = yes }
1667.1.1 = { citysize = 9102 } # Earthquake, the city gradually declined, -5000
1690.1.1 = { tax_assessor = yes }
1700.1.1 = { citysize = 8763 courthouse = yes }
1750.1.1 = { citysize = 9012 }
1800.1.1 = { citysize = 9400 }
1806.5.26 = { controller = RFR } # Occupied by French troops
1806.6.17 = { controller = RAG } # The French are defeated
1806.7.12 = { controller = RFR }
1807.7.9  = { owner = RFR add_core = RFR } # Treaty of Tilsit
1813.9.20 = { controller = HAB } # Occupied by Austrian forces
1814.4.6  = {	owner = HAB
		add_core = HAB
		remove_core = RFR
	    } # Napoleon abdicates
