#626 - Mataram

owner = MPH
controller = MPH
add_core = MPH
culture = sulawesi
religion = hinduism
capital = "Karta"
trade_goods = cattle
hre = no
base_tax = 8
manpower = 5
citysize = 36200
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM

1356.1.1 = { add_core = DEM }
1390.1.1 = { revolt_risk = 3 } #Decline of the Majapahit after death of Hayam Wuruk
1406.1.1 = { revolt_risk = 4 }
1450.1.1 = { citysize = 38900 }
1453.1.1 = { revolt_risk = 9 } #interregnum
1456.1.1 = { revolt_risk = 4 }
1527.1.1 = {	owner = DEM
		controller = DEM
		remove_core = MPH
		citysize = 45090
		revolt_risk = 0
	   }
1548.1.1 = {	owner = PAJ
		controller = PAJ
		add_core = PAJ
		remove_core = DEM
		citysize = 45090
	   }
1570.1.1 = {	discovered_by = MTR
		owner = MTR
		controller = MTR
		add_core = MTR
		remove_core = PAJ
		religion = sunni
		citysize = 48570
	   } # The Sultanate of Mataram
1600.1.1 = { citysize = 52000 }
1613.1.1 = { discovered_by = NED } # The Dutch arrived
1617.1.1 = { revolt_risk = 5 } # Rebellion against Mataram rule
1620.1.1 = { revolt_risk = 2 }
1627.1.1 = { revolt_risk = 7 } # Rebellion
1630.1.1 = { revolt_risk = 0 }
1647.1.1 = { capital = "Plered" }
1650.1.1 = { citysize = 50910 }
1680.1.1 = { capital = "Kartosuro" }
1700.1.1 = { citysize = 52340 }
1750.1.1 = { citysize = 53887 }
1755.1.1 = {	owner = JOG
		controller = JOG
		add_core = JOG
		add_core = NED
	   } # Mataram succesion war
1800.1.1 = { citysize = 55630 }
1811.1.1 = {
		add_core = GBR
	    } # Conquered by the Brtitish
1816.1.1 = {
		remove_core = GBR
	    } # Conquered by the Brtitish