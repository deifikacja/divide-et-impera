#2279 - Kazusa

owner = CIB
controller = CIB
culture = japanese
religion = shinto
capital = "Kazusa"
trade_goods = grain
hre = no
base_tax = 4
manpower = 2
citysize = 7950
add_core = CIB
fort1 = yes
discovered_by = chinese

1590.1.1 = { owner = TOY controller = TOY add_core = TOY }
1600.10.22 = { owner = JAP controller = JAP add_core = JAP }#Japan is unified