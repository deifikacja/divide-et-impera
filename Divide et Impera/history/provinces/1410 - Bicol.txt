#1410 - Bicol

culture = filipino
religion = animism
capital = "Albay"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 70
native_ferocity = 2
native_hostileness = 9
discovered_by = CEB
discovered_by = MAC
discovered_by = LUZ

1573.1.1  = { discovered_by = SPA } # Juan de Salcedo
1595.1.1  = { add_core = SPA }
1616.1.1  = {	owner = SPA
		controller = SPA
	   	citysize = 100
		trade_goods = grain
	   	culture = castillian
	   	religion = catholic
	    }
1650.1.1  = { citysize = 360 }
1700.1.1  = { citysize = 625 }
1750.1.1  = { citysize = 1650 }
1800.1.1  = { citysize = 1745 }