# 180 Poitou - Principal cities: Poitiers

owner = FRA
controller = FRA
capital = "Poitiers"
citysize = 13000
culture = cosmopolitan_french
religion = catholic 
hre = no
base_tax = 5
trade_goods = cattle
manpower = 2
add_core = FRA
add_core = ENG
fort1 = yes
temple = yes
#university = yes # L'Universit� de Poitiers
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1356.1.1   = { revolt_risk = 5 }
1358.2.22  = { revolt_risk = 7 } #Etienne Marcel in Paris
1358.7.31  = { revolt_risk = 3 } #death of Etienne Marcel
1360.1.1   = { revolt_risk = 5 }
1360.5.8   = { owner = ENG controller = ENG }# Treaty of Bretigny
1364.1.1   = { revolt_risk = 0 }
1372.1.1   = { owner = FRA controller = FRA }
1450.1.1   = { citysize = 15000 }
1453.7.18  = { remove_core = ENG }
1500.1.1   = { citysize = 16000 marketplace = yes }
1525.1.1   = { fort2 = yes }
1550.1.1   = { citysize = 19000 }
1565.1.1   = { revolt_risk = 8 } # France is restless once again as ultra-catholic intentions become clear
1568.9.1   = { revolt_risk = 15 } # Catherine de Medici and Charles IX side with the Guise faction, religious intolerance peaks
1570.8.8   = { revolt_risk = 10 } # Edict of Saint-Germain: temporary pacification
1573.9.1   = { revolt_risk = 15 } # Saint Barthelew's Day Massacre: the consequences in the land
1574.5.1   = { revolt_risk = 7 } # Charles IX dies, situation cools a bit	
1584.1.1   = { revolt_risk = 12 } # Situation heats up again
1588.12.1  = { revolt_risk = 15 } # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1594.1.1   = { revolt_risk = 10 } # 'Paris vaut bien une messe!', Henri converts to Catholicism
1598.4.13  = { revolt_risk = 3 } # Edict of Nantes, alot more freedom to the protestants
1598.5.2   = { revolt_risk = 0 } # Peace of Vervins, formal end to the Wars of Religion
1600.1.1   = { citysize = 22000 constable = yes }
1631.1.1   = { revolt_risk = 3 }
1634.1.1   = { revolt_risk = 0 }
1635.1.1   = { fort3 = yes }
1639.1.1   = { revolt_risk = 3 }
1640.1.1   = { courthouse = yes }
1643.1.1   = { revolt_risk = 0 }
1650.1.1   = { citysize = 25000 customs_house = yes }
1650.1.14  = { revolt_risk = 7 } # Mazarin arrests the Princes Cond�, Conti & Longueville, the beginning of the Second Fronde
1651.4.1   = { controller = FRA revolt_risk = 4 } # An unstable peace is concluded
1651.12.1  = { revolt_risk = 7 } # Mazarin returns from exile, Cond� sides with Spain, situation heats up again
1652.2.15  = { revolt = { type = revolutionary_rebels size = 1 } controller = REB } # Fronde rebels make a foothold in the Guyenne area, under Cond�
1652.10.21 = { revolt = {} controller = FRA revolt_risk = 0 } # The King is allowed to enter Paris again, Mazarin leaves France for good. Second Fronde over.
1685.10.18 = { revolt_risk = 8 } # Edict of Nantes revoked by Louis XIV
1686.1.17  = { religion = catholic } # Dragonnard campaign succesful: region reverts back to catholicism
1689.1.1   = { revolt_risk = 0 } # War of the Grand Alliance erupts: Louis XIV can't persue his religious policy anymore
1700.1.1   = { citysize = 28000 fort4 = yes }
1749.1.1   = { base_tax = 6 } # Machault and the 5% tax
1750.1.1   = { citysize = 30000 tax_assessor = yes }
1760.1.1   = { workshop = yes }
1770.1.1   = { regimental_camp = yes }
1789.7.14  = {	owner = RFR
		controller = RFR
		add_core = RFR
		remove_core = FRA
	     } # The storming of the Bastille
1800.1.1   = { citysize = 34000 }
1814.4.11  = {	owner = FRA
		controller = FRA
		add_core = FRA
		remove_core = RFR
	     } # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1815.3.20  = {	owner = RFR
		controller = RFR
		add_core = RFR
		remove_core = FRA
	     } # Napoleon enters Paris
1815.7.8   = {	owner = FRA
		controller = FRA
		add_core = FRA
		remove_core = RFR
	     } # The French monarchy is restored
