#1828 - Bingo

owner = HOS
controller = HOS
culture = japanese
religion = shinto
capital = "Bingo"
trade_goods = fish
hre = no
base_tax = 2
manpower = 1
citysize = 6750
add_core = HOS
add_core = KIK
fort1 = yes
discovered_by = chinese

1450.1.1 = { owner = KIK controller = KIK }
1554.1.1 = { owner = MRI controller = MRI }#Mori Motohari adopted as heir of Kikkawa
1600.10.22 = { owner = JAP controller = JAP add_core = JAP }#Japan is unified