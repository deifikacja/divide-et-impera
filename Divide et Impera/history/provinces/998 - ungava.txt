#998 - Ungava

culture = inuit
religion = shamanism
capital = "Ungava"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 5
native_ferocity = 1
native_hostileness = 1

1610.1.1  = { discovered_by = ENG }
1713.4.11 = {	owner = FRA
		controller = FRA
		culture = cosmopolitan_french
		religion = catholic
		citysize = 100
	    } # Borders determined by Treaty of Utrecht
1738.7.13 = { add_core = FRA }
1750.1.1  = { citysize = 2480 trade_goods = fur }
1763.2.10 = {	owner = GBR
		controller = GBR
		remove_core = FRA
		culture = english
		religion = episcopalism
		citysize = 500
	    }
1788.2.10 = { add_core = GBR }
1800.1.1  = { citysize = 2950 }
