#V�sterg�tland, contains G�teborg, Skara, Sk�vde etc.

add_core = SWE
owner = SWE
controller = SWE
culture = swedish
religion = catholic
hre = no
base_tax = 3
trade_goods = cattle
manpower = 1
fort1 = yes #Old �lvsborg Castle
capital = "Skara"
citysize = 1400 # Estimated
temple = yes #"Domkyrkan i Skara"
workshop = yes #Settlement of Dutch and Germans near �lsvborg
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1450.1.1   = { citysize = 1500 }
1500.1.1   = { citysize = 1950 } # Estimated
# 1520.3.6   = { owner = DAN controller = DAN add_core = DAN } #The Council accept Christian II as King
# 1521.8.30  = { owner = SWE controller = SWE } #Liberated after the "Noble Day at Vadstena"
1527.6.1   = { religion = protestant}
1570.1.1   = { citysize = 2394 }
1580.1.1   = { fort2 = yes } #Skaraborg Castle
1598.8.15  = { controller = POL } #Sigismund tries to reconquer his crown
1598.12.15 = { controller = SWE } #Duke Karl get it back
1601.1.1   = { fort3 = yes } #New �lvsborg Castle
1610.1.1   = { citysize = 2703 }
1612.5.24  = { controller = DAN } #The War of Kalmar-Captured by Christian IV
1613.1.20  = { controller = SWE }#The Peace of Kn�red
1621.1.1   = { capital = "G�teborg" }
1621.1.1   = { marketplace = yes }
1621.1.1   = { citysize = 3200 } #Interpolated
1624.1.1   = { regimental_camp = yes } #V�stg�tsdals regemente/V�stg�ta regemente till H�st
1640.1.1   = { courthouse = yes } #minor court belonging to G�ta Hovr�tt
1650.1.1   = { citysize = 3653 }
1680.1.1   = { fort4 = yes }
1690.1.1   = { citysize = 5071 }
1704.1.1   = { shipyard = yes } #G�teborg issued as naval base by KXII
1730.1.1   = { citysize = 8000 }
1740.1.1   = { fort5 = yes } 
1770.1.1   = { citysize = 11143 }
1780.1.1   = { fort6 = yes } 
1810.1.1   = { citysize = 14346 }
1820.1.1   = { citysize = 18000 } #Interpolated

