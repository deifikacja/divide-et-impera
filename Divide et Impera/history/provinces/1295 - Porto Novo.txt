#1295 - Porto Novo

add_core = ALA
add_core = AJI
culture = yorumba
religion = animism
capital = "Ajache"
trade_goods = unknown
hre = no
base_tax = 2
native_size = 60
native_ferocity = 4.5
native_hostileness = 9
discovered_by = OKO
discovered_by = OYO
discovered_by = BEN
discovered_by = NUP
discovered_by = IFE

1550.1.1 = { native_size = 0 citysize = 5000 trade_goods = grain owner = ALA controller = ALA  discovered_by = ALA } #Foundation of Alada Kingdom
1600.1.1 = { citysize = 5600 }
1625.1.1 = { revolt_risk = 5 discovered_by = AJI } #Split of Alada Kingdom, rise of Ajache Ipo
1700.1.1 = { citysize = 7300 }
1724.1.1 = { revolt_risk = 0 owner = AJI controller = AJI } #Ajache Ipo founded
1750.1.1 = { citysize = 10000 }
1800.1.1 = { citysize = 11000 }