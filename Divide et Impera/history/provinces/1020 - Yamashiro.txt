#1020 - Yamashiro

owner = JAP
controller = JAP
culture = japanese
discovered_by = ordu
religion = shinto
capital = "Kyoto"
trade_goods = chinaware
hre = no
base_tax = 9
manpower = 1
citysize = 50500
add_core = JAP
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
temple = yes

1450.1.1   = { citysize = 53015 }
1466.11.1  = { revolt_risk = 5 } #Yamana and Hosokawa position troops outside Kyoto
1467.1.14  = { revolt_risk = 10 } #Yamana and Hosokawa move troops inside Kyoto
1467.5.20  = {	revolt_risk = 0
		base_tax = 5
		temple = no
		controller = YAM
	     } # Onin War starts, Kyoto is where fighting occurs
1477.12.16 = { controller = JAP } #Ouchi lays down his arms
1480.1.1   = { base_tax = 6 revolt_risk = 1 } # Barriers reconstructed around Kyoto and opposition to them
1481.1.1   = { revolt_risk = 0 }
1489.1.1   = { temple = yes } # Jishoji Temple
1500.1.1   = { citysize = 55800 }
1542.1.1   = { discovered_by = POR }
1550.1.1   = { citysize = 59700 }
1587.1.1   = { fort1 = yes } # Jurakudai Palace
1595.1.1   = { fort1 = no } # Dismamtling Jurakudai Palace
1600.10.22 = { citysize = 15678 }#Sekahigara
1603.1.1   = { fort1 = yes } # Nijo Castle
1650.1.1   = { citysize = 64234 }
1700.1.1   = { citysize = 66320 }
1750.1.1   = { citysize = 68750 }
1800.1.1   = { citysize = 70005 }