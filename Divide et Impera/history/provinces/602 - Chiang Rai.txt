#602 - Chiang Rai

owner = LNA
controller = LNA
culture = northern_thai
religion = buddhism
capital = "Chiang Rai"
trade_goods = wool 
hre = no
base_tax = 3
manpower = 1
citysize = 23500
add_core = LNA
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = YUA
discovered_by = indian

1450.1.1 = { citysize = 25200 }
1500.1.1 = { citysize = 27800 }
1526.1.1 = { revolt_risk = 4 } # Political instability after King Phraya's death
1550.1.1 = { citysize = 29654 }
1558.1.1 = {	add_core = TAU
		revolt_risk = 0
	   } # Burmese vassal
1578.1.1 = { owner = TAU controller = TAU } # Direct Burmese rule
1600.1.1 = { citysize = 31240 }
1650.1.1 = { citysize = 33000 }
1700.1.1 = { citysize = 35487 }
1750.1.1 = { citysize = 37720 }
1786.1.1 = {	owner = AYU
		controller = AYU
		add_core = AYU
		remove_core = TAU
	   } # Conquered by the Siamese
1800.1.1 = { citysize = 397150 }