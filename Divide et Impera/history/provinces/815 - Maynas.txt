#815 - Maynas

owner = CNB
controller = CNB
culture = panoa
religion = animism
capital = "Maynas"
trade_goods = copper
hre = no
base_tax = 2
manpower = 1
citysize = 1366
add_core = CNB
discovered_by = INC
discovered_by = CHM
discovered_by = INC
discovered_by = CHM
discovered_by = HNV
discovered_by = MNO
discovered_by = PCN
discovered_by = QUT
discovered_by = CII
discovered_by = HNC
discovered_by = CAA
discovered_by = CRC
discovered_by = TRC
discovered_by = CNC
discovered_by = SCN

discovered_by = CNE
discovered_by = CNB
discovered_by = NZC
discovered_by = AYA
discovered_by = DIA
discovered_by = HUA
discovered_by = TIT
discovered_by = ATA

1495.1.1  = { owner = INC controller = INC }
1500.1.1  = { citysize = 1560 }
1520.1.1  = { religion = inti }
1533.8.29 = {	discovered_by = SPA
		owner = SPA
		controller = SPA
	    }# The death of Atahualpa
1537.1.1  = { revolt_risk = 8 } # Fighting broke out when Almagro seized Cuzco
1538.1.1  = { revolt_risk = 5 } # Almagro is defeated & executed
1550.1.1  = { citysize = 2100 religion = catholic }
1558.8.29 = { add_core = SPA }
1600.1.1  = { citysize = 2586 }
1650.1.1  = { citysize = 2710 }
1700.1.1  = { citysize = 3040 }
1750.1.1  = { citysize = 3764 add_core = PEU }
1800.1.1  = { citysize = 4350 }