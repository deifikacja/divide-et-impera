#215 - Castilla La Vieja

owner = CAS		#Juan II of Castille
controller = CAS
add_core = CAS
culture = castillian
religion = catholic
hre = no
base_tax = 5
trade_goods = wool 
manpower = 1
fort1 = yes
capital = "Valladolid"
marketplace = yes	# Medina del Campo fair 
citysize = 8000
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1356.1.1   = { revolt_risk = 4 } # civil war between Pedro the Cruel and Enrique II de Trast�mara
1366.1.1   = { revolt = { type = pretender_rebels size = 0 } controller = REB } #Enrique II de Trast�mara captured Burgos & was crowned as the Castille king
1367.4.3   = { revolt = {} controller = CAS revolt_risk = 7 } #battle of N�jera
1369.3.23  = { revolt_risk = 2 } # Pedro the Cruel assasinated, Enrique II de Trast�mara now is the one king of the Castille
1375.1.1   = { revolt_risk = 0 } # peace is fully restored, England & Portugal recognized Enrique's rights
1450.1.1   = { citysize = 11000 }
1464.5.1   = { revolt_risk = 3 } # Nobiliary uprising against King Enrique, Castilla goes into anarchy
1468.9.18  = { revolt_risk = 0 } # Pactos de los Toros de Guisando. Isabel of Castille becomes heir to the throne and a temporary peace is achieved
1470.1.1   = { revolt_risk = 3 } # Isabel marries with Fernando of Aragon, breaking the Pacts of Guisando. King Enrique choses his daughter Juana ("La Beltraneja") as new heiress and a succession War erupts.
1479.9.4   = { revolt_risk = 0 } # Peace of Alca�ovas, between Queen Isabel and King Alfonso of Portugal who had entered the war supporting her wife Juana
1500.1.1   = { citysize = 23000 }
1516.1.23  = {	controller = SPA
		owner = SPA
		add_core = SPA
	     } # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
1520.5.1   = { revolt_risk = 5 } # War of the Comunidades
1521.4.1   = { revolt_risk = 0 } # The army of the "Comuneros" is defeated at Villalar. Its leaders are promptly beheaded.
1550.1.1   = { citysize = 45000 }
1600.1.1   = { citysize = 40000 }
1650.1.1   = { citysize = 15000 }
1700.1.1   = { citysize = 18000 courthouse = yes }
1713.4.11  = { remove_core = CAS }
1750.1.1   = { citysize = 19000 }
1789.1.1   = { citysize = 21000 }
1808.6.6   = { revolt = { type = nationalist_rebels size = 0 } controller = REB }
1808.12.1  = { revolt = {} controller = SPA }
1812.10.1  = { revolt = { type = nationalist_rebels size = 0 } controller = REB }
1813.12.11 = { revolt = {} controller = SPA }