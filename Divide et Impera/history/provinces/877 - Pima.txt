#877 - Pima

culture = pueblo
religion = animism 
capital = "Pima" 
trade_goods = unknown
hre = no 
base_tax = 1 
manpower = 1 
native_size = 10
native_ferocity = 3
native_hostileness = 6 


discovered_by = ANA

discovered_by = APA
discovered_by = PUB
discovered_by = NAO

1500.1.1 = { temple = no owner = XXX controller = XXX culture = pimic religion = shamanism remove_core = ANA }
1541.1.1  = { discovered_by = SPA } # Francisco V�squez de Coronado
1731.1.1  = {	owner = SPA
		controller = SPA
		citysize = 250
		religion = catholic
		culture = castillian
		trade_goods = salt 
	    } # First permanent Spanish settlers
1756.1.1  = { add_core = SPA add_core = MEX citysize = 1450 }
1751.1.1  = { revolt_risk = 5 }
1752.1.1  = { revolt_risk = 0 }
1800.1.1  = { citysize = 1680 }
1810.9.16 = {	owner = MEX
		controller = SPA
	    } # Mexican War of Independence