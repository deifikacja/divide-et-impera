#2170 - Kaffa

owner = GEN
controller = GEN
culture = pontic
religion = greek_orthodox
hre = no
base_tax = 4
trade_goods = wine
manpower = 1
capital = "Kaffa"
citysize = 3722
fort1 = yes
marketplace = yes
add_core = GEN
add_core = THD
discovered_by = ordu
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = CRI
discovered_by = KAZ
discovered_by = GOL
discovered_by = AST
discovered_by = NOG
discovered_by = BSH
discovered_by = QAS


1475.1.1 = {	owner = TUR
		controller = TUR
		add_core = TUR
		remove_core = THD
		culture = tartar
                trade_goods = slaves
                base_tax = 5
	   } # Seized by Gedik Ahmet Pasha, turned into slavemarket
1479.1.1 = { religion = sunni }
1500.1.1 = { citysize = 4379 }
1550.1.1 = { citysize = 5151 }
1600.1.1 = { citysize = 6060 }
1650.1.1 = { citysize = 7130 }
1700.1.1 = { citysize = 8388 }
1750.1.1 = { citysize = 9869 }
1783.1.1 = {	add_core = RUS
		owner = RUS
		controller = RUS
		remove_core = TUR
	   } # Conquered by Russia
1800.1.1 = { citysize = 11610 }
1802.1.1 = { capital = "Feodosiya" } # The city is renamed