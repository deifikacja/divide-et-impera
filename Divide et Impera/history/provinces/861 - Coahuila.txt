#861 - Coahuila

culture = coahuiltecan
religion = animism 
capital = "Coahuila" 
trade_goods = unknown
hre = no 
base_tax = 1 
manpower = 1 
native_size = 5 
native_ferocity = 1 
native_hostileness = 5
discovered_by = MAY
discovered_by = MMA
discovered_by = QUI
discovered_by = TAY
discovered_by = ACA
discovered_by = CHT
discovered_by = CPE
discovered_by = TUX
discovered_by = UAY
discovered_by = CUP
discovered_by = ECA
discovered_by = AKC

discovered_by = ZAP
discovered_by = AZT
discovered_by = MIX
discovered_by = TLA
discovered_by = YPT
discovered_by = MTT
discovered_by = TAR
discovered_by = HST
discovered_by = CHC

1577.1.1 = {	discovered_by = SPA 
		capital = "Saltillo" 
		owner = SPA 
		controller = SPA 
		citysize = 1260
		culture = castillian
		religion = catholic
		trade_goods = wool 
	   } # Colonized, Francisco Cano 
1600.1.1 = { citysize = 1570 } 
1602.1.1 = { add_core = SPA }
1650.1.1 = { citysize = 2587 } 
1700.1.1 = { citysize = 3460 } 
1750.1.1 = { citysize = 6700 add_core = MEX } 
1800.1.1 = { citysize = 13800 }
1810.9.16  = { owner = MEX controller = SPA } # Mexican War of Independence
