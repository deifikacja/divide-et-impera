#2133 - Chancay

owner = CAA
controller = CAA
culture = chimuan
religion = animism
capital = "Chancay"
trade_goods = fish
hre = no
base_tax = 4
manpower = 2
citysize = 8244
fort1 = yes
add_core = CAA
discovered_by = INC
discovered_by = CHM
discovered_by = INC
discovered_by = CHM
discovered_by = HNV
discovered_by = MNO
discovered_by = PCN
discovered_by = QUT
discovered_by = CII
discovered_by = HNC
discovered_by = CAA
discovered_by = CRC
discovered_by = TRC
discovered_by = CNC
discovered_by = SCN

discovered_by = CNE
discovered_by = CNB
discovered_by = NZC
discovered_by = AYA
discovered_by = DIA
discovered_by = HUA
discovered_by = TIT
discovered_by = ATA

1450.1.1  = { citysize = 9560 }
1470.1.1  = { owner = INC controller = INC add_core = INC }
1478.1.1  = { religion = inti }
1500.1.1  = { citysize = 11322 }
1533.8.29 = {	discovered_by = SPA
		owner = SPA
		controller = SPA
		religion = catholic
	    }# The death of Atahualpa
1535.6.1  = { revolt_risk = 4 } # Manco Inca Yupanqui rebellion, attacked the fort of Lima 
1537.1.1  = { revolt_risk = 8 } # Fighting broke out when Almagro seized Cuzco
1538.1.1  = { revolt_risk = 5 } # Almagro is defeated & executed
1541.1.1  = { revolt_risk = 6 } # Pizzaro is assassinated by supporters of Almagro, his brother assumed control
1548.1.1  = { revolt_risk = 0 } # Gonzalo Pizzaro is also executed & Spain succeeds in reasserting its authority
1550.1.1  = { citysize = 10015 culture = castillian }
1558.8.29 = { add_core = SPA }
1578.1.1  = { revolt_risk = 4 } # Slave rebellion
1579.1.1  = { revolt_risk = 0 }
1600.1.1  = { citysize = 25760 } # Developed rapidly because of the harbour at Callao
1630.1.1  = { marketplace = yes } # The centre of the Spanish trade monopoly
1650.1.1  = { citysize = 32280 }
1700.1.1  = { citysize = 37000 }
1746.1.1  = { citysize = 28500 } # Most of the city was destroyed in an earthquake
1750.1.1  = { citysize = 42600 add_core = PEU }
1800.1.1  = { citysize = 53000 }