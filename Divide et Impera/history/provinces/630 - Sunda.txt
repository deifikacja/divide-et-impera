#630 - Sunda

owner = SUN
controller = SUN
culture = sulawesi
religion = hinduism
capital = "Sunda Kalapa"
trade_goods = spices
hre = no
fort1 = yes
base_tax = 5
manpower = 2
citysize = 35920
add_core = SUN
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = ordu

1450.1.1  = { citysize = 38200 }
1527.1.1 = {controller = DEM}
1527.6.22 = { capital = "Jayakarta" } # The city's name is changed
1528.1.1 = { owner = BAN controller = BAN add_core = BAN}
1570.1.1  = {	religion = sunni
		citysize = 40775
	    }  # Banten became an independent sultanate
1600.1.1  = { citysize = 43800 }
1613.1.1  = { discovered_by = NED } # The Dutch arrived
1619.1.1  = {	owner = NED
		controller = NED
		capital = "Batavia"
		citysize = 45150
	    } # Conquered by the Dutch
1644.1.1  = { add_core = NED }
1650.1.1  = { citysize = 48670 }
1700.1.1  = { citysize = 50111 }
1750.1.1  = { citysize = 52346 cot = yes }
1800.1.1  = { citysize = 54880 }