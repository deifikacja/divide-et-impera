#47 - Vorpommern

owner = WOL
controller = WOL
add_core = WOL
culture = pommeranian
religion = catholic
hre = yes
base_tax = 2
trade_goods = naval_supplies
manpower = 1
fort1 = yes
capital = "Stralsund"
citysize = 2900
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1478.12.17 = { owner = POM controller = POM add_core = POM }
1490.1.1   = { marketplace = yes }
1500.1.1   = { citysize = 3150 }
1525.1.1   = { citysize = 3450 }
1529.1.1   = { add_core = BRA }
1532.1.1   = { owner = WOL controller = WOL }#Philip I
1534.1.1   = { religion = protestant fort2 = yes }
1550.1.1   = { citysize = 4400 }
1575.1.1   = { citysize = 5200 }
1600.1.1   = { citysize = 5640 }
1625.1.1   = { citysize = 6160 }
1625.1.1   = { owner = POM controller = POM add_core = POM }
1630.1.1   = { citysize = 3400 } #Devastating population losses in Thirty Years War
1648.10.24 = {	owner = SWE
		controller = SWE
	     	add_core = SWE
	     }
1650.1.1   = { citysize = 3700 }
1675.1.1   = { citysize = 4400 }
1700.1.1   = { citysize = 4700 fort3 = yes }
1720.1.1   = {	owner = PRU
		controller = PRU
	     	add_core = PRU
	     	remove_core = BRA
	     	remove_core = SWE
	     } #Sweden cedes most of Vorpommern to Prussia in the Treaty of Nystad
1725.1.1   = { citysize = 5200 }
1750.1.1   = { citysize = 5700 }
1800.1.1   = { citysize = 6500 }
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1814.1.14  = {	owner = DAN
		controller = DAN
		add_core = DAN
		remove_core = SWE
	     } # Treaty of Kiel
1815.6.7   = {	owner = PRU
		controller = PRU
		add_core = PRU
		remove_core = DAN
	     }