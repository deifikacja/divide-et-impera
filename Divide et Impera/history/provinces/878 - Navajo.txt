#878 - Navajo

owner = ANA
controller = ANA
add_core = ANA
culture = pueblo
religion = animism 
capital = "Navajo" 
trade_goods = unknown
hre = no 
base_tax = 1 
manpower = 1 
native_size = 20 
native_ferocity = 3 
native_hostileness = 5 
citysize = 2000

discovered_by = ANA


discovered_by = APA
discovered_by = PUB
discovered_by = NAO

1500.1.1 = { owner = NAO controller = NAO remove_core = ANA culture = navajo 
religion = shamanism  }
1541.1.1   = { discovered_by = SPA } # Francisco V�squez de Coronado
1756.1.1   = { add_core = SPA add_core = MEX }
1800.1.1   = { citysize = 1955 }