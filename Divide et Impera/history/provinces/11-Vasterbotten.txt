#V�sterbotten-�ngermanland, from Sundsvall/H�rn�sand over Ume� to Torne�

add_core = SWE
owner = SWE
controller = SWE
culture = swedish
religion = catholic
hre = no
base_tax = 1
trade_goods = fur
manpower = 1
capital = "Ume�"
citysize = 1050 # Estimated
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western

1356.1.1 = { revolt = { type = pretender_rebels size = 0 } controller = REB }#Erik Magnusson
1359.6.21 = { revolt = {} controller = SWE }#Death of Erik Magnusson
1400.1.1  = { citysize = 1087 }
1450.1.1  = { citysize = 1125 }
1500.1.1 = { citysize = 1210 } # Estimated
1527.6.1 = { religion = protestant}
1550.1.1 = { citysize = 1550 } # Estimated
1600.1.1 = { citysize = 1910 } # Estimated
1623.1.1 = { regimental_camp = yes } #V�sterbottens regemente
1623.1.1 = { fort1 = yes }
1650.1.1 = { citysize = 2365 }
1650.1.1 = { trade_goods = naval_supplies } #Estimated Date
1700.1.1 = { citysize = 2610 }
1704.1.1 = { fort2 = yes }
1750.1.1 = { citysize = 2738 }
1800.1.1 = { citysize = 3105 }
1809.3.24 = { controller = RUS } # Conquered by Barclay de Tolly
1809.9.17 = { controller = SWE } # Treaty of Fredrikshamn
