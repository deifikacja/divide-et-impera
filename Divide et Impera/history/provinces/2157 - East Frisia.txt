# 100 Friesland - Principal cities: Groningen & Leeuwarden

owner = EFR
controller = EFR
capital = "Norden"
citysize = 1100
culture = frisian
religion = catholic
hre = yes
base_tax = 3
trade_goods = fish 
manpower = 1
add_core = EFR
fort1 = yes
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1450.1.1   = { citysize = 1100 }
1500.1.1   = { citysize = 1800 }
1550.1.1   = { citysize = 2600 }
1568.1.1   = { religion = reformed }
1600.1.1   = { citysize = 3000 }
1650.1.1   = { citysize = 3500 }
1700.1.1   = { citysize = 3500 }
1744.1.1  = {	owner = PRU
		controller = PRU
		add_core = PRU
	     } # Annexed by Prussia
1750.1.1   = { citysize = 3500 }
1800.1.1   = { citysize = 4000 }
1801.1.1  = {	owner = RFR
		controller = RFR
		add_core = RFR
	     } # Annexed by France
1806.1.1  = {	owner = NED
		controller = NED
		add_core = NED
	     } # Annexed by France
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
1810.7.10  = {	owner = RFR
		controller = RFR
		remove_core = NED
	     } # Annexed by France
1813.11.30 = {	owner = HAN
		controller = HAN
		remove_core = RFR
	     } # William returns to the Netherlands