#616 - Viet Bac

owner = DAI
controller = DAI
culture = vietnamese
religion = buddhism
capital = "Tuyen Quang"
trade_goods = grain
hre = no
base_tax = 5
manpower = 4
citysize = 14000
add_core = DAI
fort1 = yes
discovered_by = chinese discovered_by = TRF discovered_by = AQS discovered_by = CHG discovered_by = MCH discovered_by = YUA discovered_by = QNG discovered_by = BRY discovered_by = KHA discovered_by = KSG discovered_by = TUV discovered_by = JZJ discovered_by = YNJ discovered_by = HXJ discovered_by = KLM
discovered_by = YUA
discovered_by = ordu

1407.1.1 = { add_core = MNG }
1413.1.1 = {	owner = MNG
		revolt = { type = nationalist_rebels size = 8 }
		controller = REB
		add_core = MNG
	   } # Vietnam is invaded by Chinese troops after the Tran dynasty is overthrowned
1427.1.1 = {	revolt = {}
		owner = DAI
		controller = DAI
		remove_core = MNG
	   } # The L� Dynasty
1450.1.1 = { citysize = 14280 }
1500.1.1 = { citysize = 15050 }
1533.1.1 = {	owner = VBC
		controller = VBC
		add_core = VBC
		remove_core = DAI
	   } # The kingdom is divided between the Nguyens & the Trinh line
1550.1.1 = { citysize = 15877 }
1600.1.1 = { citysize = 16570 }
1644.1.1 = { remove_core = MNG } # Independent
1650.1.1 = { citysize = 18100 }
1677.1.1 = {	owner = TOK
		controller = TOK
		add_core = TOK
		remove_core = VBC
	   } # TO Tonkin
1700.1.1 = { citysize = 19870 }
1730.1.1 = { revolt_risk = 5 } # Peasant revolt
1731.1.1 = { revolt_risk = 0 }
1750.1.1 = { citysize = 20400 }
1786.1.1 = { revolt_risk = 6 } # The Trinh dynasty is overthrown by Tay Son rebells
1788.1.1 = { revolt_risk = 0 } # The Tay-Son take over
1800.1.1 = { citysize = 21800 }
1802.1.1 = {	owner = DAI
		controller = DAI
		add_core = DAI
		remove_core = ANN
	   } # The Nguyen dynasty defeats the Tay Sons
