#873 - Chinook

culture = chinook
religion = animism 
capital = "Chinook" 
trade_goods = unknown
hre = no 
base_tax = 3 
manpower = 1 
native_size = 10 
native_ferocity = 1 
native_hostileness = 6  

1543.1.1   = { discovered_by = SPA } # Sighted by Spanish explorers
1814.12.24 = {	owner = GBR
		controller = GBR
		citysize = 200
	     	culture = english
	     	religion = protestant
		trade_goods = fur 
	     } # British control after the War of 1812