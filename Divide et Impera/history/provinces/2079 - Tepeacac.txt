#2079 - Tepeacac

owner = TEO
controller = TEO
culture = nahuatl
religion = animism
capital = "Tepeaca"
trade_goods = copper
hre = no 
base_tax = 2
manpower = 1
citysize = 2100
add_core = TEO
add_core = AZT
discovered_by = HST
discovered_by = TEP
discovered_by = MAY
discovered_by = MMA
discovered_by = QUI
discovered_by = TAY
discovered_by = ACA
discovered_by = CHT
discovered_by = CPE
discovered_by = TUX
discovered_by = UAY
discovered_by = CUP
discovered_by = ECA
discovered_by = AKC

discovered_by = ZAP
discovered_by = AZT
discovered_by = MIX
discovered_by = TLA
discovered_by = YPT
discovered_by = MTT
discovered_by = TAR
discovered_by = TEO
discovered_by = TEX
discovered_by = TLC

1450.1.1  = { citysize = 2350 }
1471.1.1  = { owner = AZT controller = AZT }
1487.1.1  = { religion = teotl }
1500.1.1  = { citysize = 2564 }
1519.1.1  = { discovered_by = SPA } # Hern�n Cort�s
1520.1.1  = {	owner = SPA
		controller = SPA
		citysize = 1215
		culture = castillian
		religion = catholic
	    } # Founded by Hern�n Cort�s
1545.1.1  = { add_core = SPA }
1550.1.1  = { citysize = 2940 }
1600.1.1  = { citysize = 3555 }
1650.1.1  = { citysize = 3780 }
1700.1.1  = { citysize = 4470 }
1750.1.1  = { citysize = 5008 add_core = MEX }
1800.1.1  = { citysize = 6500 }
1810.9.16  = { owner = MEX controller = SPA } # Mexican War of Independence
1812.8.10  = { controller = MEX } # Conquered by Jos� Mar�a Morelos
