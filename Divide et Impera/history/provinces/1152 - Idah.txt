#1152 - Idah

culture = yorumba
religion = animism
trade_goods = unknown
capital = "Idah"
hre = no
base_tax = 1
native_size = 50
native_ferocity = 4.5
native_hostileness = 9 
discovered_by = OKO
discovered_by = OYO
discovered_by = BEN
discovered_by = NUP
discovered_by = IFE
discovered_by = KBO

1510.1.1 = {	owner = BEN
		controller = BEN
		add_core = BEN
		add_core = IGA
		revolt_risk = 6
		citysize = 3500
		trade_goods = ivory
		manpower = 1
	   } #Benin expands up the Niger towards the Benue
1600.1.1 = { citysize = 5000 base_tax = 2 }
1620.1.1 = { revolt_risk = 6 } #Civil war in Benin
1625.1.1 = {	revolt_risk = 0  
		owner = IGA
		controller = IGA 	
	   } #Civil war in Benin ends Ode domination of Igala
1700.1.1 = { trade_goods = slaves } #Source of Slaves for Trade In Calabar-Bonny region
1804.1.1 = { discovered_by = SOK }