#537 - Tiruchchirapalli 

owner = MAD
controller = MAD
culture = tamil
religion = hinduism
capital = "Thanjavur"
trade_goods = spices
hre = no
base_tax = 5
manpower = 4
citysize = 14370
add_core = TNJ
add_core = MAD
fort1 = yes
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = MUG

1378.1.1  = { owner = VIJ controller = VIJ add_core = VIJ }
1450.1.1  = { citysize = 15287 }
1498.1.1  = { discovered_by = POR }
1500.1.1  = { citysize = 17630 }
1549.1.1  = {	owner = TNJ
		controller = TNJ
		revolt_risk = 9
	    } # The Vijayanagar empire collapses
1550.1.1  = { citysize = 21709 }
1570.1.1  = { revolt_risk = 0 }
1600.1.1  = { citysize = 24003 }
1650.1.1  = { citysize = 27808 }
1659.1.1  = { revolt_risk = 4 } # The kingdom began to break up.
1660.1.1  = { revolt_risk = 8 } # Madurai was invaded by Maratha & Mysorne resulting in chaos
1700.1.1  = { citysize = 31800 }
1750.1.1  = { citysize = 34770 }
1800.1.1  = { citysize = 37500 }
