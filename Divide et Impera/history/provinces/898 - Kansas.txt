#898 - Kansas

culture = dakota
religion = shamanism
capital = "Kansas"
trade_goods = unknown
hre = no
base_tax = 1
manpower = 1
native_size = 30
native_ferocity = 3
native_hostileness = 9
discovered_by = KIO
discovered_by = PAW
discovered_by = CHY

1541.1.1 = { discovered_by = SPA } # Francisco V�squez de Coronado
1803.4.3 = {	owner = USA
		controller = USA
		add_core = USA
		citysize = 350
		trade_goods = grain
	   } # The Louisiana purchase