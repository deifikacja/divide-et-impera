#2120 - Manaos

culture = tupinamba
religion = animism
capital = "Manaus"
trade_goods = unknown
hre = no
base_tax = 2
manpower = 1
native_size = 5
native_ferocity = 1
native_hostileness = 3



1542.6.3  = { discovered_by = SPA discovered_by = POR } # da Orellana
1669.10.29 = {	owner = POR
		controller = POR
		capital = "Forte de Sao Jos� do Rio Negro"#Manaus
		citysize = 360
		culture = portugese
		religion = catholic
		fort1 = yes
		trade_goods = naval_supplies
	    } # Conquered by Portugal, Forte do Pres�pio
1650.1.1  = { citysize = 530 }
1700.1.1  = { citysize = 630 add_core = POR  }
1750.1.1  = { citysize = 700 add_core = BRZ }
1800.1.1  = { citysize = 900 }