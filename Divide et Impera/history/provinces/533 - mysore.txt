#533 - Mysore

owner = MYS
controller = MYS
culture = kannada
religion = hinduism
capital = "Mysore"
trade_goods = cattle
hre = no
base_tax = 6
manpower = 2
citysize = 15354
add_core = MYS
add_core = VIJ
discovered_by = indian
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = MUG

1450.1.1 = { citysize = 15350 }
1498.1.1 = { discovered_by = POR }
1500.1.1 = { citysize = 17004 }
1550.1.1 = { citysize = 18763 }
1559.1.1 = { revolt_risk = 7 } # Mysore revolt
1565.7.1 = {	remove_core = VIJ
		revolt_risk = 4
	   } # The Vijayanagar empire collapses
1566.1.1 = { revolt_risk = 0 }
1600.1.1 = { citysize = 20865 }
1650.1.1 = { citysize = 23044 }
1700.1.1 = { citysize = 26457 }
1750.1.1 = { citysize = 28760 }
1800.1.1 = { citysize = 32500 }
