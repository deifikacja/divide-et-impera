#418 - Van

owner = QAR
controller = QAR
culture = armenian
religion = eastern_churches
capital = "Van"
trade_goods = cloth
hre = no
base_tax = 5
manpower = 2
citysize = 8907
add_core = QAR
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = TIM
discovered_by = SHY
discovered_by = MUG
discovered_by = ordu

1380.1.1  = { owner = TIM controller = TIM }
1405.1.1  = { owner = AKK controller = AKK remove_core = QAR }
1500.1.1  = { citysize = 9480 }
1508.1.1  = {	owner = PER
		controller = PER
		add_core = PER
		remove_core = AKK
	    } # The Safavids took over
1516.7.1  = { owner = TUR controller = TUR add_core = TUR } # Wartime occupation
1535.1.1  = { controller = PER } # Persians recapture Van
1548.8.25 = { controller = TUR }
1549.12.1 = {	owner = TUR
		remove_core = PER
	    } # Part of the Ottoman empire
1550.1.1  = { citysize = 12621 }
1600.1.1  = { citysize = 13024 }
1650.1.1  = { citysize = 13560 }
1700.1.1  = { citysize = 13780 }
1722.1.1  = { revolt_risk = 5 } # Rebellion against the Ottomans
1730.1.1  = { revolt_risk = 0 }
1750.1.1  = { citysize = 14258 }
1800.1.1  = { citysize = 14870 }
