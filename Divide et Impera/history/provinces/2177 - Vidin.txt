#2177 - Vidin

owner = VID
controller = VID
culture = bulgarian
religion = greek_orthodox
capital = "Vidin"
trade_goods = cattle
hre = no
base_tax = 3
manpower = 1
citysize = 5100
fort1 = yes
add_core = BUL
add_core = VID
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 

1356.1.1 = { add_core = TUR }
1365.1.1 = { controller = HUN }#Hungarian crusaders
1369.1.1 = { controller = VID }#Hungarian crusaders drove back
1422.9.17  = { owner = TUR controller = TUR remove_core = VID }#Fall of Vidin
1450.1.1  = { citysize = 7070 }
1500.1.1  = { citysize = 8100 }
1550.1.1  = { citysize = 8870 }
1555.1.1  = { revolt_risk = 5 } # General discontent against the Janissaries' dominance
1556.1.1  = { revolt_risk = 2 }
1560.1.1  = { marketplace = yes }
1600.1.1  = { citysize = 8440 }
1620.1.1  = { fort2 = yes }
1650.1.1  = { citysize = 9780 }
1688.1.1  = { revolt_risk = 6 } # Rebellion against Ottoman rule, centered on Chiprovtzi
1689.1.1  = { revolt_risk = 0 } # Brutally suppressed by Janissaries
1700.1.1  = { citysize = 10773 }
1750.1.1  = { citysize = 11980 }
1780.1.1  = { tax_assessor = yes }
1793.1.1  = { revolt_risk = 5 } # Pasvanoglu  Rebellion, centered at Vidin
1798.1.1  = { revolt_risk = 0 }
1800.1.1  = { citysize = 17467 }