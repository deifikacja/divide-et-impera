#1367 - Diyarbakir

owner = DIY
controller = DIY
culture = kurdish
religion = sunni
capital = "Diyarbakir"
trade_goods = cattle
hre = no
base_tax = 3
manpower = 5
citysize = 4135
fort1 = yes
add_core = DIY
discovered_by = western
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = ordu

1356.1.1 = { add_core = DUL }
1402.4.1 = { controller = TIM citysize = 3215 }
1405.1.1 = { controller = DIY }
1468.1.1 = { owner = AKK controller = AKK add_core = AKK }
1504.1.1 = { citysize = 3966 marketplace = yes owner = PER controller = PER add_core = PER remove_core = AKK }
1516.1.1 = {	owner = DIY
		controller = DIY
		remove_core = PER
	   }
1535.1.1 = {	owner = TUR
		controller = TUR
		add_core = TUR
		remove_core = DIY
	   }
1550.1.1 = { citysize = 5008 }
1600.1.1 = { citysize = 6100 }
1650.1.1 = { citysize = 6655 }
1700.1.1 = { citysize = 7005 }
1750.1.1 = { citysize = 7533 }
1800.1.1 = { citysize = 8620 }