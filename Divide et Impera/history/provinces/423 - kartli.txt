#423 - Kartli

owner = GEO
controller = GEO
culture = georgian
religion = eastern_churches
capital = "T'bilisi"
trade_goods = copper
hre = no
base_tax = 6
manpower = 3
citysize = 12350
add_core = GEO
add_core = KRT
fort1 = yes
discovered_by = muslim discovered_by = ottoman discovered_by = KZH discovered_by = ORZ discovered_by = KZI discovered_by = SHY discovered_by = KHI discovered_by = KOK discovered_by = TIM discovered_by = TYU discovered_by = SIB discovered_by = NOG discovered_by = ULZ discovered_by = KLM discovered_by = BUQ 
discovered_by = eastern discovered_by = GOL discovered_by = QAS discovered_by = CRI discovered_by = AST discovered_by = KAZ
discovered_by = western
discovered_by = TIM
discovered_by = SHY
discovered_by = MUG
discovered_by = ordu

1387.1.1   = { citysize = 8200 } # Tamerlan's invasion devastated region
1450.1.1   = { citysize = 11200 }
1458.1.1   = { revolt_risk = 5 } # Safavid campaign against Georgia
1460.1.1   = { revolt_risk = 0 } # Defeated by Shirwan Shah
1466.1.1   = { owner = KRT controller = KRT } # Georgia disintegrated
1500.1.1   = { citysize = 13340 }
1550.1.1   = { citysize = 15500 }
1600.1.1   = { citysize = 20100 fort2 = yes }
1625.1.1   = { revolt_risk = 8 } # Insurrection, headed by Giorgi Saakadze
1630.1.1   = { revolt_risk = 0 constable = yes }
1650.1.1   = { citysize = 23630 marketplace = yes }
1700.1.1   = { citysize = 27150 }
1730.1.1   = { tax_assessor = yes }
1750.1.1   = { citysize = 29500 }
1762.1.19   = { owner = GEO controller = GEO } #Kartli-Kakheti personal union restores "Georgia"
1795.1.1   = { citysize = 21570 } # Kartli-Kakheti is invaded by Persian troops, Tbilisi is destroyed
1800.1.1   = { citysize = 21970 }
1801.1.18 = {	owner = RUS
		controller = RUS
		add_core = RUS
	     } # Annexed by Russia
