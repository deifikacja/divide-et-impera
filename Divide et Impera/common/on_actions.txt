#country
on_battle_won = {
}

#country
on_battle_lost = {
}

#country
on_siege_won = {
	events = {
		9205		#tribes can sack stuff.
	}
}

#country 
on_new_monarch = {
	events = {
		8301	#end of ruler's influences
		9492 	#remove some event modifiers
		22003	#remove act of supremacy
		22004	#2-nd chance for episcopalism
	}
}

#country 
on_regent = {
	events = {
		8301	#end of ruler's influences
		22003	#remove act of supremacy
		22004	#2-nd chance for episcopalism
	}
}

#country 
on_new_term_election = {
	random_events = {
		90	= 700		# Election event
		4	= 702		# Election event
		2	= 703		# Election event
		4	= 704		# Election event
	}
}

#country 
on_death_election = {
	random_events = {
		97	= 701		# Election event
		3	= 703		# Election event
	}
}

#country  (country annexing, this = target)
on_diplomatic_annex = {
	events = {
		610
	}
}

#country 
on_weak_heir_claim = {
	events = {
		611		#Pretenders rise up
	}
}

# Emperor from same dynasty
on_successive_emperor = {
	events = {
		900
	}
}

# HRE Prince released by the emperor
on_released_hre_member = {
	events = {
		901
	}
}

# HRE Prince converts to non-Emperor religion
on_hre_member_false_religion = {
	events = {
		902
	}
}

# HRE Prince converts to non-Emperor religion
on_hre_member_true_religion = {
	events = {
		903
	}
}

# Emperor wins HRE defensive war
on_hre_wins_defensive_war = {
	events = {
		904
	}
}

# HRE member annexed by non-HRE country
on_hre_member_annexed = {
	events = {
		905
	}
}

# HRE member removed from non-HRE country SoI
on_hre_member_released_soi = {
	events = {
		906
	}
}

# HRE member released from non-HRE vassalage
on_hre_released_vassal = {
	events = {
		907
	}
}

# Emperor coming to the defense of a HRE member
on_hre_defense = {
	events = {
		908
	}
}

# Emperor NOT coming to the defense of a HRE member
on_hre_non_defense = {
	events = {
		909
	}
}

# HRE Member takes a HRE province from an outside country in a peace (province scope)
on_hre_province_reconquest = {
	events = {
		950
	}
}


# A new daimyo becomes kampaku all allied to him gets to decide what side they're on
on_allied_kampaku = {
	events = {
		10022
	}
}

# A peace treaty to revoke the most recent imperial eform takes effect
on_reform_revoked = {
	events = {
		766
		767
		768
		769
		770
		771
		772
	}
}

# Nation is integrated after being in union
on_integrate = {
	events = {
		610
	}
}

# country random events
on_bi_yearly_pulse = 
{
	random_events = {

	#governmentevents.txt		
		
		
	}
}