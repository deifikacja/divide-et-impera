# Irish Charge

type = infantry
unit_type = western

maneuver = 1
offensive_morale = 7
defensive_morale = 3
offensive_fire = 1
defensive_fire = 3
offensive_shock = 5
defensive_shock = 2
