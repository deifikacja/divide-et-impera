# tirailleurr

type = infantry
unit_type = western

maneuver = 1
offensive_morale = 8
defensive_morale = 5
offensive_fire = 10
defensive_fire = 9
offensive_shock = 6
defensive_shock = 6
