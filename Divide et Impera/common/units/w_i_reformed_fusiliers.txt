# reformed fusiliers

type = infantry
unit_type = western

maneuver = 1
offensive_morale = 8
defensive_morale = 8
offensive_fire = 9
defensive_fire = 9
offensive_shock = 9
defensive_shock = 9
