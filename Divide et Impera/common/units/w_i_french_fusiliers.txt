# french fusiliers

type = infantry
unit_type = western

maneuver = 1
offensive_morale = 5
defensive_morale = 5
offensive_fire = 6
defensive_fire = 7
offensive_shock = 8
defensive_shock = 7
