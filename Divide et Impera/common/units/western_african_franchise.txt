# African Western Franchise Warfare

unit_type = sahel
type = infantry

maneuver = 1
offensive_morale = 6
defensive_morale = 3
offensive_fire = 4
defensive_fire = 4
offensive_shock = 3
defensive_shock = 3
