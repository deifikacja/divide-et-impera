# reformed grenadiers

type = infantry
unit_type = western

maneuver = 1
offensive_morale = 6
defensive_morale = 7
offensive_fire = 8
defensive_fire = 8
offensive_shock = 10
defensive_shock = 7
