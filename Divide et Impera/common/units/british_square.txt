# British Square

type = infantry
unit_type = western

maneuver = 1
offensive_morale = 10
defensive_morale = 12
offensive_fire = 8
defensive_fire = 10
offensive_shock = 7
defensive_shock = 8
