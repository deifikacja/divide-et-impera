#Kiev

graphical_culture = easterngfx

color = { 113  120  200 }

historical_ideas = { bellum_iustum
	national_conscripts
        high_technical_culture
        humanist_tolerance
	merchant_adventures
	military_drill
        bureaucracy
	battlefield_commisions
	national_trade_policy
	engineer_corps
	grand_army
	espionage	
}

historical_units = {
	eastern_medieval_infantry
	eastern_knights
	slavic_stradioti
	eastern_militia
	muscovite_musketeer
	muscovite_caracolle
	muscovite_soldaty
	muscovite_cossack
	russian_petrine
	russian_lancer
	russian_green_coat
	russian_cuirassier
	russian_mass w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

leader_names = {
	Alekseev Chodkiewicz Chreptowicz Czaplic Doroszenko
	Gosiewski Gaszto�d
	Hlebowicz Kuncewicz
	Korsunovas Kiszka Kossakowski Konaszewicz Chmielnicki Korecki
	Mielzynski Massalski
	Niemirowicz
	Ostrogski Orszewski Ogi�ski Or�yk
	Pac Pociej Zbaraski
	Radziwi�� Razumowski
	Sapieha Sanguszko S�dziw�j S�uszka Sosnowski Samoj�owicz Szczuka Smotrycki
	Tyszkiewicz Plater Kryski Dembi�ski Chodkiewicz Sapieha Pac Mielecki Kmita
	Wyhowski  Czartoryski
	Wi�niowiecki Wo��owicz
	Zabie��o Z�kiewski Haraburda
        Dorohobu�ski Kaszy�ski Mikuli�ski Teliatewski 
        Cho�mski Czerniaty�ski Bie�osielski Dobry�ski
        Kargo�omski Sugorski Uchtomski  
	Aleksiejew Sapieha 
        Kamie�ski Kolcow M�cis�awski Rajewski
	Szuwa�ow Szeremietiew Wasilewicz Wo�kow
        Olelkowicz Koriatowicz Korybutowicz Holsza�ski
        Gli�ski Ostrogski Radziwi�� Hlebowicz
        Chodkiewicz Wasy�kowicz �wiatos�awicz
        Olgierdowicz Borysowski Gorodne�ski
        Hercygski Izjas�awlski Drucki Podbereski
        Kukiejnowski Lagowski Lugowski Mi�ski
        Stre�ebski Witebski Ro�cis�awowicz
        Jaros�awski M�cis�awowicz Iwanowicz
        Toropiecki Aleksandrowicz Juriewicz
        Konstantynowicz Dawidowicz Dymitrowicz
        Bria�ski Putywlski Kurski Lubecki G�uchowski
        Czerkieski Homelski Wirski Olegowicz Jarope�koicz
        Wsiewo�odowicz Jaros�awowicz Romanowicz
        Maskiewicz Gaszto�d Chreptowicz
	Apraksin
	Bagration Bibikoff Bodisco Boruffsin
	Golitzin
	Kaibula Kamenski Kolzoff
	Mensjikov Mustislavski
	Tolstoy
	Paletskij
	Rajevski
	Schuvalov Sjeremetjev Sjtjenjatvev
	Sheremetev
	Vasiljevitj Volkov
}

monarch_names = {
	"Danilo #2" = 40
	"Lev #2" = 40
	"Ivan #2" = 40
	"Vasiliy #1" = 40
	"Dmitriy #5" = 20
	"Andrei #3" = 5
	"Yuriy #2" = 5
	"Semen #1" = 5
	"Boris #0" = 5
	"Fyodor #1" = 1
	"Afanasiy #0" = 3
	"Akim #0" = 3
	"Aleksandr #0" = 3
	"Alexei #0" = 3
	"Anatoliy #0" = 3
	"Arkadiy #0" = 3
	"Boris #0" = 3
	"Daniil #0" = 3
	"Dorofey #0" = 3
	"Feodosiy #0" = 3
	"Feofilakt #0" = 3
	"Gavriil #0" = 3
	"Innokentiy #0" = 3
	"Iosif #0" = 3
	"Lavrentiy #0" = 3
	"Makariy #0" = 3
	"Matveiy #0" = 3
	"Mikhail #0" = 3
	"Nazariy #0" = 3
	"Nikifor #0" = 3
	"Oleg #0" = 3
	"Onisim #0" = 3
	"Pavel #0" = 3
	"Prokopiy #0" = 3
	"Pyotr #0" = 3
	"Radomir #0" = 3
	"Saveliy #0" = 3
	"Sergei #0" = 3
	"Terentiy #0" = 3
	"Valeriy #0" = 3
	"Yakim #0" = 3
	"Yevgeniy #0" = 3
}

ship_names = {
	"Dmitri Donskoi" "Vasili I" "D. Aleksandrovich"
	Rurik "Ivan Kalita" Moscha Volga Schorna Coluna
	Cortiza Mosnisk Olescho Piersek Troitzkoy Wolok
	Klin Iausa Chorny Dnipro Dnestr
}
