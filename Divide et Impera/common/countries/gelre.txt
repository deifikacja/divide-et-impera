#Country Name: Please see filename.

graphical_culture = latingfx

color = { 139  66  97 }

historical_ideas = { liberal_arts
	merchant_adventures
	national_conscripts
	military_drill
	patron_of_art
	shrewd_commerce_practise 
	national_trade_policy
	cabinet
	national_bank 
	smithian_economics
	scientific_revolution
	bureaucracy
	espionage
}

historical_units = {
	#Infantry Netherlands
	w_i_armed_peasantry
	w_i_municipal_militia
	w_i_mercenary
	w_i_battle_formation
	w_i_condottiere
	w_i_halberdiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_british_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Reinoud #3" = 20
	"Willem #3" = 20
	"Arnold #1" = 15
	"Adolf #0" = 15
	"Karel #0" = 15
	"Gerard #4" = 5
	"Diederik #1" = 5
	"Hendrik #1" = 5
	"Jan #1" = 5
	"Alfons #0" = 3
	"Andries #0" = 3
	"Bartholomeus #0" = 3
	"Constantijn #0" = 3
	"Ernst #0" = 3
	"Erwin #0" = 3
	"Ewoud #0" = 3
	"Floris #0" = 3
	"Godfried #0" = 3
	"Jacobus #0" = 3
	"Joost #0" = 3
	"Joris #0" = 3
	"Kasper #0" = 3
	"Koenraad #0" = 3
	"Ludger #0" = 3
	"Luuk #0" = 3
	"Marinus #0" = 3
	"Maurits #0" = 3
	"Micha�l #0" = 3
	"Nikolaas #0" = 3
	"Rutger #0" = 3
	"Reinier #0" = 3
	"Rien #0" = 3
	"Rudolf #0" = 3
	"Sebastiaan #0" = 3
	"Siemen #0" = 3
	"Sieuwerd #0" = 3
	"Sjoerd #0" = 3
	"Vincent #0" = 3
	"Werner #0" = 3
	"Wilfred #0" = 3
}

leader_names = {
	Bachman Bakker Bentinck Bosch Bulterman
	"De Haas" Derksen "De Waal"
	Elgershuisen Entink
	Gelkom
	Holst Honekes Hoornaert Huethorst
	Jansen
	Kempermans Kiperije Kleinluking Korthust Kreijhoeft
	Oldenhaven
	Polkamp
	Remmelink
	Saelckink Sluiter Sweerts
	Teerink "Ter Horst"
	"Van Bentheim" "Van Bommel" "Van Bronckhorst" "Van den Bergh"
	"Van Gulik" "Van Wassenberg" "Van Zutfen" V�gelink
	Wagevoord Wassink Wissink
}

ship_names = {
	"Twee Gebroeders" "Jonge Dani�l" "Gelderse Worst" 
	"Wapen van Kampen" "Wapen van Zutphen" "Wapen van Hattem"
	"Wapen van Zwolle" "Wapen van Deventer" "Wapen van Harderwijk"
	"Hertog Gerard" "Hertog Reinoud" "De Zwarte Hertog" Wassenberg
	"Hertog Arnold" "Hertog Eduard" Veluwe Betuwe Flipje Bronckhorst
	"Hertogin Maria" "Graaf Godschalk" "Bommelerwaard"
	"Wapen van Nijmegen" Lichtenvoorde "Maas en Waal"
}

army_names = {
	"Leger van $PROVINCE$"  }

fleet_names = {  "Vloot van $PROVINCE$" 
	 }