#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 130  100  150 }

historical_ideas = { naval_transport
	merchant_adventures
	excellent_shipwrights
	shrewd_commerce_practise
	superior_seamanship
	national_conscripts
	national_trade_policy
	sea_hawks
	naval_fighting_instruction
	military_drill
	naval_glory
}

historical_units = {
	east_asian_spearmen
	eastern_bow
	asian_arquebusier
	asian_musketeer
	reformed_asian_musketeer w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Jaka #0" = 20
	"Tingir #0" = 20
	"Pangeran #0" = 10
	"Adivijaya #1" = 30
	"Surya #0" = 20
	"Benawa #0" = 20
	"Thado #0" = 10
}

leader_names = {
	Alam
	Abdulfatah Ahmad Amir Anwar Azmi 
	Batuta 
	Chairul 
	Hamzah Hasanuddin 
	Ibnu 
	Kyai 
	Mahat Mukhtar 
	Najamuddin Natzar Nazmizan 
	Roem 
	Syarifuddin 
	Zulkifli 
}

ship_names = {
	"Sungai Belait" "Sungai Tutong" "Sungai Tembureng"
	Batang Trusan "Sungai Pandaruan" "Lungai Limbang"
	"Batang Baram" "Sungai Bakong" "Sungai Tutoh"
	"Sungai Tinjar" "Kuala Belait" "Batu Danau"
	"Kuala Medamit" Bangar Penanjong Lumut Telingan
	Labi Sukang "Kuala Baram"
}
