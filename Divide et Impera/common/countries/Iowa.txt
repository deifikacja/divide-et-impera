#Country Name: Please see filename.

graphical_culture = northamericagfx

color = { 115  64  1 } 

historical_ideas = { bellum_iustum
        military_drill
	battlefield_commisions
	humanist_tolerance
	national_conscripts
	grand_army
	national_trade_policy
	cabinet
	engineer_corps
	bill_of_rights
	patron_of_art
}

historical_units = {
	native_indian_archer
	native_indian_tribal_warfare
	algonkin_tomahawk_charge
	commanche_swarm
	native_indian_mountain_warfare
	sioux_dragoon
	american_western_franchise_warfare
}

monarch_names = {
	#Omaha	
"Abey" = 10
	"Abeytu" = 10
	"Abeytzi" = 10
#Sioux	
"Akacheta" = 10
	"Angpetu" = 10
	"Chankoowashtay" = 10
	"Chapa" = 10
	"Chapawee" = 10
	"Chatan" = 10
	"Chayton" = 10
	"Chumani" = 10
	"Chas chunk a" = 10
	"Donoma" = 10
	"Dakota" = 10
	"Dakotah" = 10
	"Dowanhowee" = 10
	"Ehawee" = 10
	"Enapay" = 10
	"Hinto" = 10
	"Hanska" = 10
	"Hantaywee" = 10
	"He lush ka" = 10
	"Hotah" = 10
	"Howahkan" = 10
	"Kangee" = 10
	"Kimimela" = 10
	"Kohana" = 10
	"Lootah" = 10
	"Migina" = 10
	"Mikasi" = 10
	"Mimiteh" = 10
	"Mitena" = 10
	"Macha" = 10
	"Magaskawee" = 10
	"Mahkah" = 10
	"Mahpee" = 10
	"Maka" = 10
	"Makawee" = 10
	"Mapiya" = 10
	"Mato" = 10
	"Matoskah" = 10
	"Mika" = 10
	"Napiyana" = 10
	"Napayshni" = 10
	"Nawkaw" = 10
	"Odakotah" = 10
	"Ogaleesha" = 10
	"Ogleesha" = 10
	"Ohanzee" = 10
	"Ohitekah" = 10
	"Ohiyesa" = 10
	"Ojinjintka" = 10
	"Otaktay" = 10
	"Paytah" = 10
	"Pezi" = 10
	"Ptaysanwee" = 10
	"Shappa" = 10
	"Skah" = 10
	"Snana" = 10
#Dakota	
"Tasunke" = 10
	"Tatanka ptecila" = 10
	"Tokala" = 10
#Omaha  
"Tadewi" = 10
	"Tadi" = 10
	"Taini" = 10
#Sioux  
"Takoda" = 10
	"Tashunka" = 10
	"Tatonga" = 10
	"Teetonka" = 10
#Omaha  
"Urika" = 10
#Dakota 
"Wambli waste" = 10
#Sioux	
"Wicapi wakan" = 10
	"Wicasa" = 10
	"Wachiwi" = 10
	"Wahchinksapa" = 10
	"Wahchintonka" = 10
	"Wahkan" = 10
	"Wahkoowah" = 10
	"Wamblee" = 10
	"Wambleesha" = 10
	"Wanageeska" = 10
	"Wanahton" = 10
	"Wanikiy" = 10
	"Wapi" = 10
	"Weayaya" = 10
	"Wichahpi" = 10
	"Winona" = -10
	"Yahto" = 10
	"Zonta" = 10
}

leader_names = {
}

ship_names = {
	"Ani-kutani" "Ah-ni-ga-to-ge-wi" "Ah-ni-gi-lo-hi" "Ah-ni-ka-wi" 
	"Ah-ni-tsi-sk-wa" "Ah-ni-sa-ho-ni" "Ah-ni-wo-di" "Ah-ni-wa-ya" 
	Kana'ti
	Selu
	"Tsul Kalu" "Oonawieh Unggi" "Ani Yuntikwalaski" "Asgaya Gigagei" 
	"I'nage-utasvhi" "Anisga'ya Tsunsdi" "Nv-da u-no-le" "Nv-da ko-la"
	"Nv-da ka-na-wo-ga" "Nv-da gu-ti-ha"
}