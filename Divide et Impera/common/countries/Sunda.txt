#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 130  125  74 }

historical_ideas = { naval_transport
	merchant_adventures  
	excellent_shipwrights 
	shrewd_commerce_practise 
	superior_seamanship
	national_conscripts
	national_trade_policy
	sea_hawks
	naval_fighting_instruction
	military_drill
	naval_glory
	battlefield_commisions
	espionage
}

historical_units = {
	east_asian_spearmen
	eastern_bow
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	asian_musketeer
	reformed_asian_musketeer w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

leader_names = {
	Maharaja Jamri Barmawijaya Hulukujang Gilingwesi Darmeswara Wuwus Dewageng Pucukwesi Wanayasa Haringwasa Kancana Ganawirya "Wulung Gadung" Brajawisesa Sanghyang Ageng Dharmasiksa Jayadarma Ragasuci Citraganda Dewata Ajigunawisesa Buana Bunisora Suradipati Wastu Tunggal
}

monarch_names = {
	"Tarusbawa #1" = 10
	"Sanjaya #1" = 10
	"Harisdarma #1" = 10
	"Rakeyan #1" = 10
	"Tamperan #1" = 10
	"Prabu #1" = 10
	"Pucukbumi #1" = 10
	"Windusakti #1" = 10
	"Limbur #1" = 10
	"Jayabupati #1" = 10
	"Prabu Guru #1" = 10
	"Prabu Lingga #1" = 10
	"Prabu Maharaja Lingga #1" = 10
	"Prabu Raja #1" = 10
	"Prabu Sasuk #1" = 10
	"Sri Baduga #1" = 10
}

ship_names = {
	Surakarta Yogyakarta Semarang Salatiga
	"Gunung Merbabu" Sragen Sukoharjo Wonosari
	"Gunung Lawu" Delanggu Cepu Purwodarli
	Ngawi Grobogan Boja Amgarawa
}