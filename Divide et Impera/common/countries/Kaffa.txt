#Country Name: Please seefilename.

graphical_culture = africangfx

color = { 56 206 255 }

historical_ideas = { bellum_iustum
	national_conscripts
	shrewd_commerce_practise
	divine_supremacy
	national_trade_policy
	military_drill
	engineer_corps
	battlefield_commisions
	merchant_adventures
	grand_army
	espionage
	cabinet
	bureaucracy
}

historical_units = {
mali_spearman
songhai_spearman
ethiopian_mountain_warfare
ethiopian_gunpowder_warfare
niger_gunpowder_warfare
westernized_ethiopian
ethiopian_guerilla_warfare
mali_cavalry
songhai_cavalry
sudan_cavalry
nubian_cavalry
fulani_cavalry
}

monarch_names = {
	"Galli Ginocho #1" = 10
	"Gaki Gaocho #1" = 10
	"Galli Gaocho #1" = 10
	"Shagi Sherocho #1" = 10
	"Beshi Ginocho #1"= 10
	"Hoti Gaocho #1" = 10
	"Gaha Nechocho #1" = 10
	"Gawi Nechocho #1" = 10
	"Kaye Sherocho #1" = 10
	"Galli Sherocho #1" = 10
	"Gaki Sherocho #1" = 10
}

leader_names = {
	Bayisa
	Dereje
	Ambo
	Asres
	Wami
	Eshete
	Ezana
	Eyob
	Asfaw
	Degife
}

ship_names = {
	Yodit Shewa "Mara Takla" Lalibela
	"Yekuno Amlak" Tigray Tatadum Seyum
	"Kedus Harbe" "Na'akueto La'ab" Yetbarak
}
