#Country Name: Please see filename.

graphical_culture = northamericagfx

color = { 105  255  100 } 

historical_ideas = { bellum_iustum
	humanist_tolerance
	national_conscripts
	battlefield_commisions
	grand_army
	national_trade_policy
	military_drill
	cabinet
	engineer_corps
	bill_of_rights
	patron_of_art
}

historical_units = {
	native_indian_archer
	native_indian_tribal_warfare
	algonkin_tomahawk_charge
	commanche_swarm
	native_indian_mountain_warfare
	sioux_dragoon
	american_western_franchise_warfare
}

monarch_names = {
	"Canajoharie #0" = 20
        "Canastigaone #0" = 20
        "Canienga #0" = 20
        "Caughnawaga #0" = 20
        "Cahunghage #0" = 20
        "Canowaroghere #0" = 20
        "Deseroken #0" = 20
        "Dayoitgao #0" = 20
        "Deonundagae #0" = 20
        "Deyodeshot #0" = 20
        "Deyohnegano #0" = 20
        "Deyonongdadagana #0" = 20
        "Gweugwehono #0" = 20
        "Gandasetaigon #0" = 20
        "Ganogeh #0" = 20
        "Gayagaanhe #0" = 20
        "Gewauga #0" = 20
        "Goiogouen #0" = 20
        "Gadoquat #0" = 20
        "Gannentaha #0" = 20
        "Hostayuntwa #0" = 20
        "Kawauka #0" = 20
        "Kente #0" = 20
        "Kanagaro #0" = 20
        "Kowogoconnughariegugharie #0" = 20
        "Kanadaseagea #0" = 20
        "Kanatakowa #0" = 20
        "Neodakheat #0" = 20
        "Nowadaga #0" = 20
        "Nundawaono #0" = 20
        "Onekagoncka #0" = 20	
}

leader_names = {
	Mohe
	Diwali
	Waya
	Tsiyi
	Onacona
	Atagulkalu
	Degataga
	Cheasequah
	Adahy
	Atagulkalu
}

ship_names = {
	"Ani-kutani" "Ah-ni-ga-to-ge-wi" "Ah-ni-gi-lo-hi" "Ah-ni-ka-wi" 
	"Ah-ni-tsi-sk-wa" "Ah-ni-sa-ho-ni" "Ah-ni-wo-di" "Ah-ni-wa-ya" 
	Kana'ti
	Selu
	"Tsul Kalu" "Oonawieh Unggi" "Ani Yuntikwalaski" "Asgaya Gigagei" 
	"I'nage-utasvhi" "Anisga'ya Tsunsdi" "Nv-da u-no-le" "Nv-da ko-la"
	"Nv-da ka-na-wo-ga" "Nv-da gu-ti-ha"
}