#Poland

graphical_culture = easterngfx

color = { 199  78  112 }

historical_ideas = {
        glorious_arms
        bill_of_rights
        military_drill
        humanist_tolerance
        national_conscripts
	patron_of_art
        engineer_corps 
	national_bank
        scientific_revolution
        cabinet
}

historical_units = {
	eastern_medieval_infantry
	eastern_knights
	slavic_stradioti
	eastern_militia
        germanized_pike
	hungarian_hussar
	polish_musketeer
	polish_hussar
	polish_tercio
	polish_winged_hussar
	russian_lancer
	russian_cuirassier
        eastern_skirmisher
	eastern_uhlan
	eastern_carabinier
	russian_mass w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"August #1" = 40
	"Stanislaw #0" = 40
	"Kazimierz #3" = 20
	"Wladyslaw #3" = 20
	"Aleksander #0" = 20
	"Henryk #0" = 20
	"Jan #0" = 20
	"Jan #0 Albert" = 20
	"Jan #0 Kazimierz" = 20
	"Michal #0 Korybut" = 20
	"Stefan #0" = 20
	"Zygmunt #0" = 20
	"Zygmunt #0 August" = 20
	"Aleksander #0 Karol" = 10
	"Zygmunt #0 Kazimierz" = 10
	"Krzysztof #0" = 5
	"Fryderyk #0" = 5
	"Jan #0 Olbracht" = 5
	"Jan #0 Zygmunt" = 5
	"Karol #0 Ferdynand" = 5
	"Olbracht #0" = 5
	"Albert #0" = 3
	"Aleksy #0" = 3
	"Andrzej #0" = 3
	"Bartlomiej #0" = 3
	"Bogumil #0" = 3
	"Jakub #0" = 30
	"Dawid #0" = 3
	"Dominik #0" = 3
	"Emil #0" = 3
	"Eugeniusz #0" = 5
	"Ferdynand #0" = 5
	"Gustaw #0" = 5
	"Igor #0" = 3
	"Iwan #0" = 3
	"Krystian #0" = 3
	"Lech #0" = 3
	"Lucjusz #0" = 3
	"Ludwik #1" = 10
	"Lukasz #0" = 3
	"Maksym #0" = 3
	"Mariusz #0" = 3
	"Mieszko #0" = 3
	"Miroslaw #0" = 3
	"Oskar #0" = 3
	"Patryk #0" = 1
	"Rafal #0" = 3
	"Sylwester #0" = 3
	"Szymon #0" = 3
	"Zygfryd #0" = 3
}

leader_names = {
	Arciszewski
	Benyowski Bidzi�ski Branicki
	Czarniecki Chometowski
	Dunin Denhoff Drzewicki
	Grodzicki G�rka
	Houwaldt Arciszewski
	Benyowski Bidzi�ski Branicki Bielski
	Czarniecki Chometowski Ciolek
	Dunin Denhoff Drzewicki Firlej Jaxa Gryfita Puchala Paluk
	Grodzicki G�rka Lis Sreniawita Labedz Borkowicz Zareba Bogoria         Skotnicki Gozdawa Wilczek Dzwonowski Grzyma�a
	Houwaldt Nalecz Grzymalita Awdaniec Starza Topor Jastrzebiec Odrowaz 
	Jablonowski Jazlowiecki Karnkowski Czarnkowski Kozlowski
	Kurozwecki  Koniecpolski  Krasinski Kalinowski Korycki Kazanowski Kmita
	Lubomirski Lisowski Lanckoro�ski Leszczy�ski Laski Leliwa Leszczyc         Lig�za Malski Mniszech Matczy�ski Muskata My�l�ci�ski
	Ostror�g Olesnicki Opacki Ozarowski Ossolinski Orzelski
	Paniewski Patkul Poniatowski Przyjemski Piaseczy�ski Potocki Pa�uk
	Rytwia�ski Rzewuski Rynarzewski
        Sulkowski Stadnicki Sieniawski Szydlowiecki �wi�ca
	Tarnowski Teczynski Wapowski Toporczyk
	Zamoyski Zebrzydowski Zborowski
	Jablonowski Jazlowiecki
	Kurozwecki Kurbski Koniecpolski Kosciuszko Krasinski Kalinowski Korycki Kazanowski         Kmita Sarnowski
	Lubomirski Lisowski Lanckoro�ski Leszczy�ski Lacki
	Malski Mniszech Matczy�ski Mlodziejowski
	Ostror�g Olesnicki Opacki Ozarowski Ossolinski
	Paniewski Patkul Poniatowski Pulaski Przyjemski Piaseczy�ski Potocki
	Rytwia�ski Rzewuski
	Sobieski Sahajdaczny Siemienowicz Sulkowski Stadnicki Sieniawski
	Tarnowski
	Wyhowski Wielopolski
	Zamoyski Zebrzydowski Za�wilichowski Zborowski Zbaraski
}

ship_names = {
	"Arka Noego"
        "Bialy Orzel" Burza "Bialy Lew" "Brzesko-Kujawskie" Belzkie Braclawskie
        Charitas "Czarny Orzel" "Czarny Kruk" Chelmin'skie Czernichowskie
        Fortuna
        Gryf Gwiazda Gnieznien'skie
	Inoworclawskie
	"Kr�l Dawid" Kaliskie Krakowskie Kijowskie Krak�w
        Lublin "Latajacy Jelen'" Lw�w Leczyckie Lubelskie
        "Maly Bialy Orzel" Malborskie Mazowieckie
        Niewrazliwy "Nowy Czarny Orzel"
        "Prorok Samuel" "Panna Wodna" Plomien' Poznanskie Plockie Podlaskie Podolskie
	"Rycerz Swiety Jerzy" Rawskie Ruskie
        Strzelec Suwerenny "Swiety Piotr" Sieradzkie Sandomierskie "Swiety Kazimierz"
        "Wielkie Slonce" Wodnik Warmin'skie Wolyn'skie Warszawa
        Zwyciestwo "Z�lty Lew"
}
army_names = {
	"Armia Koronna" "Armia Litewska" "Armia Kozacka"
}