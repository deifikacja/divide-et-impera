#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 109  168  127 }

historical_ideas = { bellum_iustum
	national_conscripts
	glorious_arms
	military_drill
	battlefield_commisions
	shrewd_commerce_practise
	grand_army
	merchant_adventures
	national_trade_policy
	engineer_corps
	cabinet
	bureaucracy
	espionage
}

historical_units = {
	east_asian_spearmen
	eastern_bow
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	asian_musketeer
	reformed_asian_musketeer
	reformed_asian_cavalry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Razadarit #0" = 20
	"Binnya Dammayaza #0" = 20
	"Binnya Ran #0" = 20
	"Binnya Waru #0" = 20
	"Binnya Kyan #0" = 20
	"Mawdaw #0" = 20
	"Shin Sawbu #0" = 20
	"Dimmazedi #0" = 20
	"Takayutpi #0" = 10
	"Smim Sawhtut #0" = 20
	"Smim Htaw #0" = 20
	"Binnya Dala #0" = 10
}

leader_names = {
	Min
	Myo
	Than
	Bourey
	Rangsey
	Nardu
	Cobar
	Leura
	Mani
	Than
}

ship_names = {
	Ceylon 
	"Khu Bua"
	Lavo
	Ramanadesa Rajadhirat
	Menam
	"Nakhon Pathom"
	Pegu
	Suwannaphum
	"U Thong"
}
