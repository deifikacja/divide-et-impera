#Country Name: Please seefilename.

graphical_culture = africangfx

color = { 84  80  152 }

historical_ideas = {
	national_conscripts
	shrewd_commerce_practise
	divine_supremacy
	national_trade_policy
	military_drill
	engineer_corps
	battlefield_commisions
	merchant_adventures
	grand_army
	espionage
	glorious_arms
}

historical_units = {
mali_trooper
songhai_trooper
sahel_trooper
sudan_trooper
niger_gunpowder_warfare
westernized_niger
african_western_franchise_warfare
mali_cavalry
songhai_cavalry
sudan_cavalry
nubian_cavalry
fulani_cavalry
}

monarch_names = {
	"'Am�ra #0" = 40
	"'Abd al-Q�dir #0" = 40
	"N�'l #0" = 20
	"Dak�n al-'Adil #0" = 20
	"D�ra #0" = 20
	"Taiy�b #0" = 40
	"Unsa #0" = 60
	"Adl�n #0" = 40
	"B�d� #0" = 140
	"Rub�t #0" = 20
	"N�l #0" = 20
	"N�sir #0" = 20
	"Ranfi #0" = 20
	"Eltecit #0" = 20
	"Joel #0" = 20
	"Koudlaniel #0" = 20
	"Agban #0" = 20
	"Ranfi #0" = 20
	"Isawi #0" = 3
	"Hashim #0" = 3
	"Maqdum #0" = 3
	"Awkal #0" = 3
	"Hassan #0" = 3
	"Agban #0" = 3
	"Abu Likayik #0" = 3
	"Rajab #0" = 3
	"Ahmad #0" = 3
	"Rifaya #0" = 3
	"Ibrahim #0" = 3
	"Idris #0" = 3
	"Tindhim #0" = 3
	"'Abd al-Rahman #0" = 3
	"Sulayman #0" = 3
	"Yusuf #0" = 3
	"Ya'qub #0" = 3
	"Ism�'�l #0" = 3
	"Musa #0" = 3
	"Muhammad #0" = 3
	"Ishaq #0" = 3
	"Halim #0" = 3
	"Mustafa #0" = 3
	"Abu Bakr #0" = 3
}

leader_names = {
	Abboud
	Fung
	Garang
	Kayra
	Keira
	Kelueljang
	Lugor
	Nuer
	Shol
	Taha
}

ship_names = {
	Apedemak Bes Dedwen Arensnuphis Amon
	Mandulis Amenirdis Taweret Mut Tefnut Khnum
}
