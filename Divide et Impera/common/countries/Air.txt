#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 220  20  96 } 

historical_ideas = { early_mining
	military_drill
	national_conscripts
	shrewd_commerce_practise
	national_trade_policy
	battlefield_commisions	
}

historical_units = {
mali_archer
songhai_archer
sahel_archer
sudan_archer
niger_gunpowder_warfare
westernized_adal
adal_guerilla_warfare
mali_nomads
songhai_nomads
sudan_nomads
nubian_nomads
fulani_nomads
}

monarch_names = {
	"Muhammad #0" = 25
	"Ahmad #0" = 10
	"Ibrahim #0" = 10
	"al-Wali #0" = 10
	"'Abd al-Kadir #0" = 10
	"'Uthm�n #0" = 10
	"'Umaru #0" = 10
	"'Abd al-Karim #0" = 10
}

leader_names = {
	"ibn Muhammad"
	"al Mubarak"
	"al Mu'min"
	"al 'Adil"
	"'Uthman"
	Humad
	al-Dani
	al-Baqiri
	ar-Raffa'
	"'Umaru"
}

ship_names = {
	Barr Muta'al Wali Batin Zahir Akhir Awwal
	Mu'akhkhir Muqaddim Muqtadir Qadir Samad
	Wahid Majid Wajed Qayyum Hayy Mumit Muhyi
	Mu'id Mubdi Muhsi Hamid Waliyy Matin Qawiyy
	Wakil Haqq Shahid Ba'ith
}
