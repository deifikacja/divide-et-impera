#Country Name: Please see filename.

graphical_culture = latingfx

color = { 231  230  138 }

historical_ideas = {
	shrewd_commerce_practise
	national_conscripts
	merchant_adventures
	humanist_tolerance
	patron_of_art
	national_bank
	national_trade_policy
	cabinet
	ecumenism
	smithian_economics
	scientific_revolution
	battlefield_commisions
}

historical_units = {
	#Infantry German
	w_i_armed_peasantry
	w_i_longbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_hungarian_formation
	w_i_spanish_formation
	w_i_chosen_formation
	w_i_swedish_formation
	w_i_quarter_army
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_prussian_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon 
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Hieronymus #0" = 20
	"Napoleon #0" = 15
	"Karl #0" = 10
	"Ludwig #0" = 10
	"Viktor #0" = 10
	"Adolf #0" = 3
	"Adrian #0" = 3
	"Albrecht #0" = 3
        "Andreas #0" = 3
	"August #0" = 3
	"Baldur #0" = 3
        "Bernhard #0" = 3
	"Cornelius #0" = 3
	"Dietrich #0" = 3
	"Ferdinand #0" = 3
	"Georg #0" = 3
	"Gerhard #0" = 3
	"G�nther #0" = 3
        "Heinrich #0" = 3
	"Ignatz #0" = 3
	"Isidor #0" = 3
	"Joseph #0" = 3
	"Konstantin #0" = 3
        "Lorenz #0" = 3
	"Ludwig #0" = 3
	"Lukas #0" = 3
        "Manfred #0" = 3
	"Marius #0" = 3
	"Markus #0" = 3
	"Matthias #0" = 3
	"Michael #0" = 3
        "Nicolaus #0" = 3
	"Paul #0" = 3
	"Raphael #0" = 3
	"Rudolf #0" = 3
	"Sigmund #0" = 3
	"Stephan #0" = 3
	"Thomas #0" = 3
	"Wenzel #0" = 3
	"Xaver #0" = 3
}

leader_names = {
	Aller
	Detmold
	Hamelm Hildesheim
	Kapp
	Minden
	Oldenburg
	Salzgitter
	Werden Wernigerode
}

ship_names = {
	"Georg von Calenberg" Leine Weser Deister
	S�ntel Wedemark Wunstorf Langenhagen
	Stadthagen Lehrte Burgdorf Sarstedt
}

army_names = {
	"K�nigliche Garde" "Armee von $PROVINCE$" 
}
