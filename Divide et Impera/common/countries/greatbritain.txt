#Country Name: Please see filename.

graphical_culture = latingfx

color = { 153  3 3 }

historical_ideas = {
	bill_of_rights
	superior_seamanship
	quest_for_the_new_world
	excellent_shipwrights
	merchant_adventures
	colonial_ventures
	land_of_opportunity
	press_gangs
	national_bank
	ecumenism
	smithian_economics
	napoleonic_warfare
}

historical_units = {
#Infantry Britain
	w_i_armed_peasantry
	w_i_longbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_british_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
"George #0" = 100
"Frederick #0" = 15
"William #3" = 10
"Edward	 #6" = 5
"Sophia	 #0" = -5	
"Charlotte  #0" = -2	
"Augusta  #0" = -2		
"Anne #1" = -1	
"Henry #8" = 1	
"Richard #3" = 1
"Charles #2" = 1
"James #2" = 1
"Adam #0" = 0
"Alexander #0" = 0
"Arthur #0" = 0
"Benjamin #0" = 0
"David #0" = 0
"Edmund #0" = 0
"Francis #0" = 0
"Gilbert #0" = 0
"Humphrey #0" = 0
"John #1" = 0
"Joseph #0" = 0
"Matthew #0" = 0
"Patrick #0" = 0
"Philip #0" = 0
"Robert #0" = 0
"Samuel #0" = 0
"Spencer #0" = 0
"Thomas #0" = 0
}

leader_names = {
Arkwright Augustus 
Bathurst Bell Burns Byng Byron
Campbell Cartwright Cavendish Chambers Clive Coleridge Compton 
Darby Davy
Field Fitzwilliam Fox 
Gainsborough Gibbon Gordon Grenville Grey
Harrison Home Hood Howard Howe Hume Hunter 
Jenkinson 
Keats Keith 
Law 
Mackenzie Moore Murray 
Nelson Newcomen
Paine Pelham Petty Playfair Pratt 
Robinson Ross Russell 
Scott Smith Stanhope Stewart Stuart 
Telford Townshend Trevithick
Walpole Watt Wedgewood Wellesley Wesley Wilberforce Wolfe
}

ship_names = {
        Achille Adventure Ajax Albemarle Alexander "Anne Gallant" Antelope "Ark Royal" Assurance Audacious
        Bear Bellerophon Bonaventure Boyne Britannia Buckingham
        Charles Christ Constant Convertine Culloden Cumberland
        Defence Defiance Destiny Dragon Dreadnought
        "Edward Howard" Elizabeth Eltham Essex Expedition
        Foresight Foudroyant
        "Gabriel Royal" Garland George Gibraltar Gloucester Goliath "Grace Dieu" Grafton "Grand Mistress" "Great Barbara" "Great Bark" "Great Elizabeth" "Great Harry" "Great Nicholas" Guernsey
        Haarlem "Happy Entrance" Hector "Henrietta Maria" Hope
        James "John Baptist"
        "Katherine Forteleza" Kent
        Leander Leopard Lion
        Majestic Margaret Mary "Mary Rose" Mercury Merhonour Michael Minion Minotaur 
        Nonpareil Nonsuch Northumberland Nottingham
        Orion
        Peter Providence "Prince Frederick"
        "Queen Charlotte"
        Rainbow "Red Lion" Regent Renown Repulse Revenge "Royal Anne" "Royal Charles" "Royal Prince" 
        "Scourge of Malice" Sovereign "Sovereign o the Seas" "St Andrew" "St Esprit" "St George" "St Matthew" Suffolk Superb Swallow Swiftsure 
        Theseus Tigre Tramontana Triumph
        Unicorn Union
        Vanguard Victory
        Warspite Worcester
}
