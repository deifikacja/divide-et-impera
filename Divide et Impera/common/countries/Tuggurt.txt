#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 90  110  76 } 

historical_ideas = {
	excellent_shipwrights
	merchant_adventures
	military_drill
	national_conscripts
	sea_hawks
	superior_seamanship
	shrewd_commerce_practise
	naval_fighting_instruction
	national_trade_policy
	battlefield_commisions	
}

historical_units = {
persian_footsoldier
persian_shamshir
persian_duel
persian_infantry
afsharid_reformed_infantry
durrani_rifled_musketeer
ali_bey_reformed_infantry
persian_rifle
musellem
shaybani
new_shaybani
abbasid
new_muslim_clan
ways
new_ways w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Muhammad #0" = 30
	"Ahmad #2" = 30
	"'Ali #0" = 40
	"Mustafa #0" = 10
	"Sulayman #0" = 40
	"'Abd er-Rahm�n #0" = 10
	"'Abd al-Qadir #0" = 20
	"Farhat #0" = 20
	"'Umar #0" = 20
	"al-Khazan #0" = 10
	"'Amar #0" = 20
	"'Aisha #0" = 10
	"Ibrahim #0" = 30
}

leader_names = {
	al-`Akhal
	"bin Bu-Kumertin"
	"bin Muhammad"
	"bin al-Kabir"
	ar-Rahman
}

ship_names = {
	Barr Muta'al Wali Batin Zahir Akhir _Awwal
	Mu'akhkhir Muqaddim Muqtadir Qadir Samad
	Wahid Majid Wajed Qayyum Hayy Mumit Muhyi
	Mu'id Mubdi Muhsi Hamid Waliyy Matin Qawiyy
	Wakil Haqq Shahid Ba'ith
}
