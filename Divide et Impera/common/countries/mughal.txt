#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 154  133  153 }

historical_ideas = { bellum_iustum
	glorious_arms  
        bureaucracy
        humanist_tolerance
	grand_army
	military_drill
	patron_of_art
	battlefield_commisions
	shrewd_commerce_practise
	engineer_corps
	national_trade_policy
	cabinet
        national_conscripts
}

historical_units = {
        yuan_2nd_category
        mawarannahr_cavalry
        babur_cavalry
        crimean_swarm
        westernized_raiders
        mongol_arch
        tartar_militia
        mongol_gunpowder_warfare
        mongol_reformed_infantry
        persian_footsoldier
	persian_cavalry_charge
	persian_shamshir
	qizilbash_cavalry
	tofongchis_musketeer
	topchis_artillery
	afsharid_reformed
	afsharid_reformed_infantry
	muslim_mass_infantry
	muslim_dragoon
	persian_rifle
	indian_footsoldier
	rajput_hill_fighters
	indian_arquebusier
	mughal_musketeer
	mughal_mansabdar
	indian_elephant
	indian_shock_cavalry
	rajput_musketeer
	reformed_mughal_musketeer
	reformed_mughal_mansabdar
	indian_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Sh�h Jah�n #0" = 60
	"'�lamg�r #0" = 40
	"Sh�h 'Alam #0" = 40
	"Muhammad #4" = 20
	"Ibr�h�m #2 Kh�n" = 20
	"Sikandar #2 Sh�h" = 20	
	"Akbar #0" = 20
	"B�bur #0" = 20
	"Bah�dur Sh�h #0" = 20
	"D�war Bakhsh #0" = 20
	"Farrukh-siyar #0" = 20
	"Hum�y�n #0" = 20
	"Isl�m Sh�h #0" = 20
	"Jah�nd�r Sh�h #0" = 20
	"Jah�ng�r #0" = 20
	"Muhammad Sh�h #0" = 20
	"N�k�-siyar #0" = 20
	"R�fi' #0" = 20	
	"Sh�r Sh�h #0" = 20
	"Ibr�him #2 Sh�h" = 10
	"'Azim-ash Sha'n #0" = 10
	"A'zam Sh�h #0" = 10
	"D�r� Shik�h #0" = 15
	"K�m Bakhsh #0" = 10
	"Mur�d Bakhsh #0" = 10
	"Sh�h Suj�h #0" = 10
	"'Abu Nasr #0" = 5
	"'Alaman #0" = 5
	"Ahd Bah�dur #0" = 5
	"Ahmad Sh�h #0" = 5
	"B�dar Bakht #0" = 5
	"Daulat Sh�h #0" = 5
	"Farrukh Sh�h #0" = 5
	"Garshasp #0" = 5
	"Hakim Sh�h #0" = 5
	"Hasan Sh�h #0" = 5
	"Husain Sh�h #0" = 5
	"Jawan Bakht #0" = 5
	"K�mr�n #0" = 5
	"Kushruh #0" = 5
	"Mahmud Sh�h #0" = 5
	"Murad Sh�h #0" = 5
	"Shahariyar Sh�h #0" = 5
	"Adel #0" = 3
	"Fakhri #0" = 3
	"Harun #0" = 3
	"Jabir #0" = 3
	"Mazin #0" = 3
	"Rashid #0" = 3
	"Samad #0" = 3
}

leader_names = {
	Ameerun
	Beg
	Bhutto
	Chughtai
	Jahan
	Kamaluddin
	Khan
	Mirza
	Musharif
	Nagar
	Nasseruddin
	Nazirun
	Radhidun
	Ra'ana
	Shahabuddin
	Sharif
	Sinan
	Singh
	Wazirun
	Zahiruddin
}

ship_names = {
	Alia Arusa Chambal Fatimah Jalsena
	Ganges "Kala Jahazi" Khan "Lal Jahazi"
	Nausena "Nav ka Yudh" Niknaz "Nila Jahazi"
	Niloufa Noor Sagar Salima Shameena Spiti
	Suhalia Yamuna Yasmine Zahira
}
