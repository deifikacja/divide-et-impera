#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 120  202  96 } 

historical_ideas = { bellum_iustum
	excellent_shipwrights
	merchant_adventures
	military_drill
	national_conscripts
	sea_hawks
	superior_seamanship
	shrewd_commerce_practise
	naval_fighting_instruction
	national_trade_policy
	battlefield_commisions
	naval_glory
	grand_navy
}

historical_units = {
	mamluk_archer
	mamluk_cavalry_charge
	mamluk_duel
	mamluk_musket_charge
	muslim_mass_infantry
	muslim_dragoon
	ali_bey_reformed_infantry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Muhammad #4" = 200
	"'Ali #0" = 100
	"Ahmad #0" = 90
	"Hasan #0" = 60
	"Husayn #0" = 60
	"Ibrahim #0" = 60
	"Mustafa #0" = 60
	"M�s� #2" = 20
	"'Abdall�h #1" = 20
	"Ab� T�shuf�n #0" = 20
	"al-Hasan #0" = 20
	"Sha'ban #0" = 20
	"'Umar #0" = 20
	"'Abd ar-Rahm�n #3" = 10
	"'Abd al-W�hid #1" = 10
	"Sa'�d #1" = 10
	"Y�suf #1" = 10
	"Baba Aruj #0" = 10
	"Khidr Khair ad-Din #0" = 10
	"Selim #0" = 10
	"'Uthm�n #2" = 5
	"Yaghmuras�n #1" = 5
	"'Abd al-Karim #0" = 3
	"Barakat #0" = 3
	"Basir #0" = 3
	"Dawud #0" = 3
	"Faris #0" = 3
	"Haidar #0" = 3
	"Hashim #0" = 3
	"Jabbar #0" = 3
	"Jamal #0" = 3
	"Junayd #0" = 3
	"Najib #0" = 3
	"Qasim #0" = 3
	"Rahim #0" = 3
	"Rash�d #0" = 3
	"Safi #0" = 3
	"Sakhr #0" = 3
	"Wahid #0" = 3
	"Zahir #0" = 3
}

leader_names = {
	Selmi
	Amara
	Behar
	Ilaes
	Benzine
	Essaid
	Hacini
	Benzai
	Kelkal
	Merah
}

ship_names = {
	Barr Muta'al Wali Batin Zahir Akhir _Awwal
	Mu'akhkhir Muqaddim Muqtadir Qadir Samad
	Wahid Majid Wajed Qayyum Hayy Mumit Muhyi
	Mu'id Mubdi Muhsi Hamid Waliyy Matin Qawiyy
	Wakil Haqq Shahid Ba'ith
}
