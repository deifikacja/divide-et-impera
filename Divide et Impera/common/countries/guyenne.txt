#Country Name: Please see filename.

graphical_culture = latingfx

color = { 32  144  204 }

historical_ideas = { liberal_arts
	national_conscripts
	merchant_adventures
	humanist_tolerance
	patron_of_art
	colonial_ventures
	military_drill
	quest_for_the_new_world
	cabinet
	bureaucracy
	scientific_revolution
	liberty_egalite_fraternity
	napoleonic_warfare
}

historical_units = {
	#Infantry France
	w_i_armed_peasantry
	w_i_crossbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_chosen_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_french_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Henri #2" = 60
	"Edouard #4" = 10
	"Thomas #0" = 10
	"Guillaume #10" = 5
	"Richard #2" = 5
	"Jean #1" = 5
	"Edmond #0" = 5
	"Humphrey #0" = 5
	"Armand #0" = 3
	"Arnaud #0" = 3
	"Aymeric #0" = 3
	"Bartomiu #0" = 3
	"Benedit #0" = 3
	"Bernat #0" = 3
	"Bertran #0" = 3
	"Brun #0" = 3
	"Carles #0" = 3	
	"Eneco #0" = 3
	"Enric #0" = 3
	"Est�ve #0" = 3
	"Folquet #0" = 3
	"Frances #0" = 3
	"Fr�dol #0" = 3
	"Gast� #0" = 3
	"Gaufr� #0" = 3
	"Guilhem #0" = 3
	"Hug #0" = 3
	"Jacme #0" = 3
	"Jordan #0" = 3
	"Loys #0" = 3
	"Matheu #0" = 3
	"Miqueu #0" = 3
	"Nicolau #0" = 3
	"Per #0" = 3
	"Pol #0" = 3
	"Pons #0" = 3
	"Thomas #0" = 3
	"Vicentz #0" = 3
	"Vivian #0" = 3
	"Xavier #0" = 3
}

leader_names = {
	Aller
	Detmold
	Hamelm Hildesheim
	Kapp
	Minden
	Oldenburg
	Salzgitter
	Werden Wernigerode
}

ship_names = {
        Adour Africain Aigle "Aigle d'Or" "Aimable Nanon"
        "Aimable Marthe" "Aimable Rose" Alexandre "Ste Anne de Bordeaux" Amiti�
        Ang�lique Am�ricain Amphitrite "Arc en Ciel" Bayonnaise 
         "Belle Poule" Bienfaisant Biscayenne Bizarre "Bonnes Amies" 
        Bordelais Brillant Caroline Castor Catiche
        Charente "Charmante Manon" "Charmante Nancy" "Charmante Rachel" "Ch�teau de Bayonne"
        Ch�zine "Commerce de Bordeaux" "Comte de Toulouse" Content D�bonnaire
        D�daigneuse "Deux Fr�res" Diamant Diligente "Duc d'Aquitaine"
        Elisabeth Entreprenante Esp�rance Extravagante Favori
        Fendant Flamand Gaillard Galant Godichon
        "Grand Coureur" "Grand Sirius" "Gros Ventre" Hercule Hermine 
        Imp�rieuse Indien Intr�pide Jolie Joseph
        Labourt Laurence L�g�re Licorne Lion
        Machault Marchand Mars Mascarin Maure
        M�g�re M�lampe Mignon Neptune "Nouvelle Victoire"
        Oiseau Opale P�lican Ph�nix Pollux
        "Quatre Fr�res" Rameau "Reine Marie" Renomm�e Rostan
        "Royal Daim" "Saint Jacques" "Saint Pierre" Sir�ne "Soleil Royal"
        Swinton Tamponne "Toison d'Or" Tourterelle Trinit�
        Triton Utile Valeur V�nus "Ville de Bayonne"
}

army_names = {
         "Arm�e d'Aquitaine"     "Arm�e du Bordelais"  "Arm�e du Blayais"
         "Arm�e du Pays de Buch" "Arm�e du Bazadais"   "Arm�e du Queyran"
         "Arm�e du Fronsadais"   "Arm�e du Libournais" "Arm�e de Castillon" 
         "Arm�e d'Albret"        "Arm�e de la Garonne" "Arm�e de l'Agenais"
	 "Arm�e de $PROVINCE$" 
}