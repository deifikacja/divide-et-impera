#Polotsk

graphical_culture = easterngfx

color = { 112  157  56 }

historical_ideas = { feudal_castles
	merchant_adventures
	national_conscripts
	shrewd_commerce_practise
	national_trade_policy
	quest_for_the_new_world
	military_drill
	battlefield_commisions
	engineer_corps
	grand_army
	espionage
	glorious_arms
	cabinet
}

historical_units = {
	bardiche_infantry 
	eastern_bowman
	eastern_knights
	slavic_stradioti
	eastern_militia
	muscovite_musketeer
	muscovite_caracolle
	muscovite_soldaty
	muscovite_cossack
	russian_petrine
	russian_lancer
	russian_green_coat
	russian_cuirassier
	eastern_skirmisher
	eastern_uhlan
	eastern_carabinier
	russian_mass w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Andrei #1" = 20
	"Lubko #1" = 20
	"Olgierd #1" = 20
	"Bryachislav #2" = 15
	"Vasiliy #2" = 15
	"Svyatoslav #1" = 15
	"Vladimir #1" = 15
	"Izyaslav #2" = 10
	"Vseslav #2" = 10
	"Boris #1" = 10
	"Aleksandr #0" = 3
	"Adrian #0" = 3
	"Afanasiy #0" = 3
	"Daniil #0" = 3
	"Dmitriy #0" = 3
	"Dorofey #0" = 3
	"Faddey #0" = 3
	"Feodosiy #0" = 3
	"Feofil #0" = 3
	"Foma #0" = 3
	"Fyodor #0" = 3
	"Gavriil #0" = 3
	"Grigoriy #0" = 3
	"Igor #0" = 3
	"Ivan #0" = 3
	"Mefodiy #0" = 3
	"Mikhail #0" = 3
	"Naum #0" = 3
	"Nikita #0" = 3
	"Nikolai #0" = 3
	"Pankrati #0" = 3
	"Prokhor #0" = 3
	"Pyotr #0" = 3
	"Radoslav #0" = 3
	"Ruslan #0" = 3
	"Semen #0" = 3
	"Serafim #0" = 3
	"Sevastian #0" = 3
	"Stanislav #0" = 3
	"Timofeiy #0" = 3
	"Varfolomeiy #0" = 3
	"Vikentiy #0" = 3
	"Yaroslav #0" = 3
}

leader_names = { 
	Vitebsky Izyaslavsky Dursky Galisky Borovsky Vereisky Volotsky Galitsky
	Mozhaisky Uglisky Shemyakin Satin Aladyin Beznosov Bokeev Burukhin Vnukov Korobyin
	Zabolotsky Dalmatov Kisleevsky Travin Tsyplyatev Shukalovsky
}

ship_names = {
	Velikaya Chudskoye Pskovskoye Polotsk
	Rakovor Bolotovo Kulikovo Ostrov Izborsk
	Pechory Porhov "Velikiye Luki" Sebezh
}
