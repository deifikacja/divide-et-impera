#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 176 3 3 } 

historical_ideas = { liberal_arts
	national_conscripts
	grand_army
	glorious_arms
	espionage
	military_drill
	merchant_adventures
	divine_supremacy
	battlefield_commisions
	engineer_corps
	national_trade_policy
	bureaucracy
	cabinet
}

historical_units = {
	persian_footsoldier
	persian_cavalry_charge
	persian_shamshir
	qizilbash_cavalry
	tofongchis_musketeer
	topchis_artillery
	afsharid_reformed
	afsharid_reformed_infantry
	muslim_mass_infantry
	muslim_dragoon
	persian_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Menteshe Beg #1" = 10
	"Shuja ud-Din #1" = 10
	"Ibrahim #1" = 10
	"Muhammad #1" = 10
	"Taj ud-Din #1" = 10
	"Muzzafar ud-Din #1" = 10
	"Laith #1" = 10
	"Uwais #1" = 10
	"Ahmed #1" = 10
}

leader_names = {
	Mas'ud
	Orkhan
	Ghazi
	Ilyas
}

ship_names = {
	Menteshe Mentese
}
