#Country Name: Please seefilename.

graphical_culture = africangfx

color = { 165  250  240 }

historical_ideas = { bellum_iustum
	national_conscripts
	shrewd_commerce_practise
	divine_supremacy
	national_trade_policy
	military_drill
	engineer_corps
	battlefield_commisions
	merchant_adventures
	grand_army
	espionage
}

historical_units = {
	african_spearmen
	bantu_tribal_warfare
	bantu_plains_warfare
	bantu_gunpowder_warfare
	westernized_bantu
	african_western_franchise_warfare
}


monarch_names = {
	"Ntare #3" = 60
	"Macwa #0" = 10
	"Rwabirere #0" = 10
	"Karara #0" = 10
	"Karaiga #0" = 10
	"Kahaya #0" = 10
	"Nyakashaija #0" = 10
	"Bwarenga #0" = 10
	"Rwebishengye #0" = 10
	"Kayungu #0" = 10
	"Rwanga #0" = 10
	"Gasyonga #0" = 20
	"Mutambuka #0" = 10
	"Mukwenda #0" = 10
	"Kahitsi #0" = 10
	"Macwa #0" = 10
}

leader_names = {
Kitabanyoro Rugingiza Rutahaba Rutashijuuka Kahigiriza
}

ship_names = {
	Ankole Ankore Nkole Nkore Unyamwezi
}
