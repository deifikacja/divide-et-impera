#Country Name: Please seefilename.

graphical_culture = africangfx

color = { 130  231  162 }

historical_ideas = {
	shrewd_commerce_practise
	national_conscripts
	merchant_adventures
	military_drill
	national_trade_policy
	battlefield_commisions
	engineer_corps
	grand_army
	espionage
	cabinet
}

historical_units = {
mali_spearman
songhai_spearman
sahel_spearman
sudan_spearman
niger_gunpowder_warfare
westernized_niger
african_western_franchise_warfare
mali_nomads
songhai_nomads
sudan_nomads
nubian_nomads
fulani_nomads
}


monarch_names = {
	"Dunama #3" = 100
	"Muhammad #1" = 140
	"Amir #0" = 20
	"Ghaji #0" = 20
	"'Uthman #0" = 20
	"'Umar #0" = 20
	"'Ali #0" = 80
	"Idris #1" = 60
	"'Abd Allah #3" = 20
	"Ibrahim #2" = 20
	"Hamdan #0" = 20
	"'Abd al-Karim #0" = 10
	"Kharut #0" = 10
	"Kharif #0" = 10
	"Ya'qub #0" = 10
	"Salih #0" = 10
	"Biri #0" = 10
	"Birni Besse #0" = 10
	"Dalai #0" = 10
	"Burkomanda #0" = 10
	"Dalo #0" = 10
	"'Abd al-Qadir #0" = 10
	"Wanja #0" = 10
	"Abu Sekkin #0" = 10
	"Rifaya #0" = 10
	"Sharud #0" = 10
	"Tindhim #0" = 10
	"Musa #0" = 10
	"Sulbuta #0" = 10
	"Ishaq #0" = 10
	"Kade Alunu #0" = 10
	"Abu Bakr #0" = 10
	"Da'ud #0" = 10
	"Kure #0" = 10
	"Bikoru #0" = 10
	"Humayy #0" = 10
	"Sa'id #0" = 10
	"Nasr #0" = 10
	"Yahya #0" = 10
	"Yusuf #0" = 10
}

leader_names = {
	Oueddai
	Ahmat
	Ardoum
	Terap
	Kabadi
	Yorassem
	Nahor
	Deby
	Djammous
	Malloum
}

ship_names = {
	Dugu Fune Aritso Kature Ayoma Bulu
	Arki Shu "Bad al-Djel" Hume Dunama
	Biri Birkoru "Abd al-Djel Selma"
	"Dunama Dibbalem" Kade "Kachim Biri"
	Djil Dari "Kure Kura" "Kure Gana"
}
