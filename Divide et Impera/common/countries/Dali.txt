#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 115  151  180 }

historical_ideas = { bellum_iustum
	national_conscripts
	glorious_arms
	military_drill
	battlefield_commisions
	shrewd_commerce_practise
	grand_army
	merchant_adventures
	national_trade_policy
	engineer_corps
	cabinet
	bureaucracy	
	national_bank
}

historical_units = {
	east_asian_spearmen
	eastern_bow
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	asian_musketeer
	reformed_asian_musketeer
	reformed_asian_cavalry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

leader_names = {
	Aung-Hla Aung-San 
	Hla-Thein 
	Khin-Nyunt Khun-Sa Kwan-Kywa 
	Maung-Gyi Maung-Win Min-Tun 
	Ohn-Myint 
	Sein-Win 
	Than-Htay Thet-Naung 
	U-Nu 
}

monarch_names = {
	"Zalahtinyan #0" = 10
	"Sithukyawhtin #0" = 10
	"Minkyinyo #0" = 10
	"Tabinshwehti #0" = 10
	"Thamindwut #0" = 10
	"Thaminhatau #0" = 10
	"Bayinnaug #0" = 10
	"Handabayin #0" = 10
	"Anaukpetlun #0" = 10
	"Minradeippa #0" = 10
	"Thalun #0" = 10
	"Pindale #0" = 10
	"Pye #0" = 10
	"Narawara #0" = 10
	"Minredyawdin #0" = 10
	"Sane #0" = 10
	"Taninganwe #0" = 10
	"Hamdammayaza #0" = 10
}

ship_names = {
	Ava Ayeyarwady Anawrahta Alaungsithu Ananda
	Bagan Htilominlo Kyanzittha Kyaswa Kyawswa
	Lokananda "Mali Hka" Narathu Naratheinkha
	Narapatisithu Narathihapati Nyima "N'Mai Hka"
	Pyinbya Pegu Sawlu Sawhnit Sawmunnit "Sula-mani"
	"Sein-nyet Ama" "Shwe-gu-gyi" "Shwe-hsan-daw"
	"Shwe-zigon" Toungoo Tambadipa Thamudarit Tharaba
	"Tan-chi-daung Paya" "That-byin-nyu" Uzana
}
