#Country Name: Please seefilename.

graphical_culture = africangfx

color = { 165  60  216 }

historical_ideas = { bellum_iustum
	national_conscripts
	shrewd_commerce_practise
	divine_supremacy
	national_trade_policy
	military_drill
	engineer_corps
	battlefield_commisions
	merchant_adventures
	grand_army
	espionage
}

historical_units = {
	african_spearmen
	bantu_tribal_warfare
	bantu_plains_warfare
	bantu_gunpowder_warfare
	westernized_bantu
	african_western_franchise_warfare
}


monarch_names = {
	"Seogola #0" = 10
	"Madirana #0" = 10
	"Ketsitihoe #0" = 10
	"Makgasama #0" = 10
	"Molete #0" = 10
	"Mokgadi #0" = 10
	"Mathiba #0" = 10
	"Kgama #0" = 30
	"Kgari #0" = 10
	"Sedimo #0" = 10
	"Khama #0" = 20
	"Sekgoma #0" = 20
	"Gorewang #0" = 10
	"Tshekedi #0" = 10
	"Serogola #0" = 10
	"Seretse #0" = 10
	"Keab�ka #0" = 10
	"Rrasebolai #0" = 10
	"Leeapeetswe #0" = 10
	"Mokgatsha #0" = 10
	"Ian #0" = 10
}

leader_names = {
"a Moleta" "a Mathiba" "a Kgama" "a Molosiwa" "a Kgari" "Boikanyo a Segkoma" Boikanyo "a Segkoma" "a Kgamane" "a Gagoitsege" "a Sekgoma a Khama" "a Khama" Kgamane "a Gorewang" "a Tshekedi" "a Seretse"
}

ship_names = {
	Kyoga Unyamwezi
}
