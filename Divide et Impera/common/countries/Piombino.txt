#Country Name: Please see filename.

graphical_culture = latingfx

color = { 253 3 3 }

historical_ideas = { naval_transport
	patron_of_art 
	shrewd_commerce_practise
	excellent_shipwrights
	merchant_adventures
	national_trade_policy
	superior_seamanship
	national_conscripts
	engineer_corps
	national_bank
	smithian_economics
	espionage
	bureaucracy
}

historical_units = {
	#Infantry Italy
	w_i_armed_peasantry
	w_i_crossbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_condottiere
	w_i_halberdiers
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_french_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Gabriele Maria #0" = 20
	"Gherardo #0 Leonardo" = 20
	"Jacopo #1" = 15
	"Emanuele #0" = 15
	"Alessandro #0" = 5
	"Belisario #0" = 5
	"Adriano #0" = 3
	"Alessio #0" = 3
	"Baldovino #0" = 3
	"Beniamino #0" = 3
	"Biagio #0" = 3
	"Bonaventura #0" = 3
	"Celio #0" = 3
	"Constantino #0" = 3
	"Damiano #0" = 3
	"Danilo #0" = 3
	"Eros #0" = 3
	"Eustachio #0" = 3
	"Evangelista #0" = 3
	"Fortunato #0" = 3
	"Gennaro #0" = 3
	"Giancarlo #0" = 3
	"Giovanni #0" = 20
	"Giustino #0" = 3
	"Innocenzo #0" = 3
	"Leonardo #0" = 3
	"Leone #0" = 3
	"Marco #0" = 3
	"Mauro #0" = 3
	"Orfeo #0" = 3
	"Pasquale #0" = 3
	"Pietro #0" = 3
	"Roberto #0" = 3
	"Sergio #0" = 3
	"Severino #0" = 3
	"Ugo #0" = 3
	"Valerio #0" = 3
	"Vincenzo #0" = 3
	"Vitale #0" = 3
	"Vittorio #0" = 3
}

leader_names = {
        "dell Agnello" Amelia Appiani "d'Appiani d'Aragona" Barone Bartoli Barzagli Basile Birindelli Brunforte Buonarroti Butteri
	Capitani Chiellini Conti Dini Fibonacci Galilei Gambacorta Ganza Gentileschi "della Gherardesca" Gori Greco Grosso Guidi
	Letta Lombardi Mancini Marchetti Marianelli Notaro Oddo Pacinotti Pesce "del Piero" Pini Pisano Pontecorvo Pozzi
	"de Rossi" Ruffo Sonnino Tabucchi Tosetti Tumicelli "della Ubaldini" Ulivelli Ventura Zaccardo
}

ship_names = {
	"Andreotto Saraceno"
	"Bartolo da Sassoferrato" "Bernardo dei Paganelli"
	"Ugolino della Gherardesca"
	"Daiberto Pisano"
	"Fazio della Gherardesca" "Fiumi Arno" "Francesco da Buti"
	"Giovanni di Simone" "Giuseppe Setaioli"
	"Leonardo Fibonacci"
	"Monti Pisani" "Morosini da Venezia"
	Rustichello
	"San Frediano" "San Giovanni Battista" "San Michele" "San Nicola" "Sant'Agata" "Sant'Antonio" "Santa Maria Assunta" "Santa Maria della Spina" "San Paolo a Ripa d'Arno" "S.Piero a Grado" S.Rossore "San Sisto"
	"Ugolino Caccini"
}

army_names = {
	"Armata de $PROVINCE$"
}