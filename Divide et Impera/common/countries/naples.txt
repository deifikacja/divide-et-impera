#Country Name: Please see filename.

graphical_culture = latingfx

color = { 160  30  154 } 

historical_ideas = {
	feudal_castles
	naval_transport
    sea_hawks
    engineer_corps
	shrewd_commerce_practise
	patron_of_art
	merchant_adventures
	national_trade_policy
	national_conscripts
	national_bank
	cabinet
	scientific_revolution
}

historical_units = {
	#Infantry Italy
	w_i_armed_peasantry
	w_i_crossbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_condottiere
	w_i_halberdiers
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_french_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Carlo #3" = 50
	"Ferrante #0" = 50
	"Filippo #0" = 40
	"Alfonso #0" = 30
	"Federigo #0" = 20
	"Ladislao #0" = 20
	"Gioachino #0" = 20
	"Giuseppe #0" = 20
	"Giovanna #1" = -15
	"Achille #0" = 15
	"Gabriele #0" = 15
	"Alberto #0" = 10
	"Antonio #0" = 10
	"Cesare #0" = 10
	"Francesco #0" = 10
	"Giovanni #0" = 10
	"Giuseppe #0" = 10
	"Leopoldo #0" = 10
	"Adriano #0" = 3
	"Alessandro #0" = 3
	"Alfredo #0" = 3
	"Bartolomeo #0" = 3
	"Benedetto #0" = 3
	"Claudio #0" = 3
	"Enzo #0" = 3
	"Fabio #0" = 3
        "Fabrizio #0" = 3
	"Gaspare #0" = 3
	"Leandro #0" = 3
	"Marco #0" = 3
        "Martino #0" = 3
	"Nestore #0" = 3
	"Paolo #0" = 3
	"Pietro #0" = 3
	"Raniero #0" = 3
	"Roberto #0" = 3
	"Salvatore #0" = 3
	"Vicenzo #0" = 3
}

leader_names = {
        "degli Albizzi" Allori "d'Appiani d'Aragona" "d'Asburgo"
	Barberini Bartolini Benciveni Bizzelli Boncompagni
	Botta-Adorno Buontalenti Buti Butteri "di Campofregoso"
	Cercignani Chiastavelli Chigi Corsini Cybo-Malaspina
	"di Farnese" Gori Grimaldi "Grimaldi di Busca" Lanzi Lessi
	Ludovisi Malaspina Mancinetti "di Medici" "del Moro" Nasini
	Orsini Piccolomini Pico Pierallini Pieroni Piombante Pontelli
	Rivani Rospigliosi "del Rosso" Savaranola Soderini Tempesta
	Terreni Tonelli Traballesi Ulivelli Vasari Zucchi
}

ship_names = {
	Albizi  Arcetri Arezzo
	Bellosguardo Bisenzio
	Careggi "Corso Donati" "Cosimo de' Medici"
	"Dante Alighieri" "Dino Compagni"
	Fiesole "Filippo Brunelleschi" Firenze "fiume Arno"
	"fiume Greve" "Francesco Petrarca"
	Galluzzo "Giano della Bella" Giotto
	Impruneta
	Medici Mugnone
	Orsanmichele
	Pistoia Prato
	Ripoli
	"San Frediano" "San Giovanni Battista" "San Lorenzo"
	"San Marco" "Santa Maria del Fiore" "Santa Trinita"
	Scandicci Settignano "Sesto Fiorentino"
	Terzolle
	"Vieri de Cerchi"
}

army_names = {
	"Armata de $PROVINCE$"
}