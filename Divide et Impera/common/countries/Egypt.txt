#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 188  166  193 } 

historical_ideas = { bellum_iustum
	glorious_arms
        national_trade_policy
	bureaucracy
        national_conscripts
	cabinet
	military_drill 
	battlefield_commisions
	espionage
	grand_army
	shrewd_commerce_practise
	engineer_corps
	improved_foraging
}

historical_units = {	
muslim_footsoldier
muslim_shamshir
mamluk_duel
tofongchis_musketeer
muslim_sturm
durrani_rifled_musketeer
ali_bey_reformed_infantry
persian_rifle
cavalry_archers
swarm
new_swarm
steppe_cav
new_steppe
durrani_swivel
durrani_dragoon w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Q�nsawh #0" = 40
	"T�m�n Bay #0" = 40
	"Muhammad #3" = 20
	"Ahmad #2" = 20
	"In�l #0" = 20
	"Khusqadam #1" = 20
	"Q�yit Bay #0" = 20
	"Timurbugh� #0" = 20
	"Yalbay #0" = 20
	"J�nbul�t #0" = 20
	"'Abd al-'Aziz #1" = 10
	"al-Musta'in #1" = 10
	"Barq�q #1" = 10
	"Barsbay #1" = 10
	"Faraj #1" = 10
	"Jaqmaq #1" = 10
	"'Uthman #1" = 10
	"Y�suf #1" = 10
	"'Al� #2" = 5
	"Baybars #2" = 5
	"H�jj� #2" = 5
	"Sha'b�n #2" = 5
	"Ab� Bakr #1" = 5
	"al-Hasan #1" = 5
	"Isma'�l #1" = 5
	"K�j�k #1" = 5
	"S�lih #1" = 5
	"'Abd al-Malik #0" = 3
	"Basir #0" = 3
	"Fadl #0" = 3
	"Ghayth #0" = 3
	"Hatim #0" = 3
	"Ilyas #0" = 3
	"Jabir #0" = 3
	"Nadim #0" = 3
	"Nizar #0" = 3
	"Qasim #0" = 3
	"Sa'di #0" = 3
	"Sayyid #0" = 3
	"Yunus #0" = 3
}

leader_names = {
	Ali
	Aybak
	Baibars
	Bay
	Bey
	Bugha
	In�l
	Kansur
	Sayfuddin
	Zangi
}

ship_names = {
	"Zaher Barqooq" "Farag Ben Barqooq"
	"Ben Barqooq" "Muyaid Sheikh" "Ahmen Muyaid" 
	"Zaher Tatar" "Nasser Muhamed" "Ashraf Barsbay"
	"Aziz Gamal" "Nahr an-Nil" "Al-Farafirah"
	"Ad-D�khilah" "Al-Kharijah" "Al-Ali" "Al-Bahriyah"
	Rahman Rahim Malik Quddus Salam Mu'min Muhaymin
	Aziz Jabbar Mutakabbir Khaliq Bari Musawwir 
	Ghaffar Qahhar Wahhab Razzaq Fattah Alim Qabid
	Basit Khafid Rafi Mu'izz Mudhill Sami Basir Hakam Adl
}
