#Country Name: Please see filename.

graphical_culture = northamericagfx

color = { 158  118  120 }

historical_ideas = {
	bellum_iustum
	deus_vult
	divine_supremacy
	patron_of_art
	national_conscripts
	military_drill
	glorious_arms
	battlefield_commisions
	superior_seamanship
	merchant_adventures
	excellent_shipwrights
	espionage
	sea_hawks
}

historical_units = {
	south_american_spearmen
	zapotec_tribal_warfare
	zapotec_plains_warfare
	zapotec_gunpowder_warfare
	westernized_zapotec
	mexican_guerilla_warfare
}

monarch_names = {
	"Cocijoeza #0" = 20
	"Cocijopi Xolo #0" = 20
	"Nucano #0" = 20
	"Cocijocza #1" = 15
	"Cocijopi #1" = 15
	"Zaachilla #1" = 15
	"Cocijouza #0" = 10
	"Cocijopi Jzi� #0" = 10
	"Pwuix Dao #0" = 10
	"Beljjw #0" = 3
	"Bibejw #0" = 3
	"Binech #0" = 3
	"Bzojj Chs�axcejj #0" = 3
	"Ch�alaxa #0" = 3
	"Chonna Bicu #0" = 3
	"Coqui #0" = 3
	"Djoraxa #0" = 3
	"Gw�ib�on� #0" = 3
	"Gyazob #0" = 3
	"Gyejj #0" = 3
	"Ipalnemoani #0" = 3
	"Ji Jzi� #0" = 3
	"Jituh #0" = 3
	"Jzi� #0" = 3
	"L�xda�xo #0" = 3
	"Lyebe #0" = 3
	"Mbalaz #0" = 3
	"Na Yel #0" = 3
	"Naquichinisa #0" = 3
	"Naxi�anguiiu #0" = 3
	"Ngolbex An #0" = 3
	"Nguiiu Rire #0" = 3
	"Nisa Ro #0" = 3
	"Pijje Jzi� #0" = 3
	"Tobi Rusiguunda #0" = 3
	"V�eni #0" = 3
	"Vezi� #0" = 3
	"Zne Yel #0" = 3
	"Zni� #0" = 3
	"Zyrx #0" = 3
}

leader_names = {
	Beljjw Bibejw Binech "Bzojj Chs�axcejj"
	Ch�alaxa Coqui
	Djoraxa
	Gwpeib�on� Gyazob Gyejj
	"Ji Jzi�" Jituh Jzi�
	L�xda�xo Lyebe
	Mbalaz
	"Na Yel" "Ngolbex An"
	"Pijje Jzi�" "Pwuix Dao"
	V�eni Vzi�
	"Zne Yel" Zni� Zyrx
}

ship_names = {
	Atoyac Atzompa
	Chalchiuhtlicue Chico Cocijo
	Ipalnemoani
	"Monte Alban"
	Necocyautl Nahuatl
	Oaxaca Omacatl
	Quetzalcoatl
	Tlaloc Tezcatlipoca Tecciztecatl
	Xochiquetzal
}
