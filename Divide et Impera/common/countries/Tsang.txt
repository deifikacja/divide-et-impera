#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 177  107  205 }

historical_ideas = { feudal_castles
	church_attendance_duty
	divine_supremacy
	battlefield_commisions
	national_trade_policy
	military_drill
	patron_of_art
	national_conscripts
	bureaucracy
	merchant_adventures
	engineer_corps
	humanist_tolerance
	glorious_arms
}

historical_units = {
	east_asian_spearmen
	eastern_bow
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	asian_musketeer
	reformed_asian_musketeer
	reformed_asian_cavalry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Norzang #0" = 20
	"Kunzang #0" = 20
	"Donyo Dorje #0" = 20
	"Ngawang Namgyal #0" = 20
	"Dondup Tseten Dorje #0" = 20
	"Ngawang Jigme Drakpa #0" = 20
	"Karma Tseten #0" = 20
	"Karma Thutob Namgyal #0" = 20
	"Khunpang Lhawang Dorje #0" = 20
	"Karma Tensung #0" = 20
	"Karma Phuntso Namgye #0" = 20
	"Karma Tenkyong #0" = 20
}

leader_names = {
	Gampo
	Srong
	Sregs
	Thekchen
	Sonam
	Tashi
	Tsewang
	Wangdue
	Wangdak
	Phuntsok
}

ship_names = {
	Barkam
	Dartsendo
	Gyantse Gartse
	Lhasa Lhotse
	Makalu 
	Nagqu Nyingchi Nedong
	Pelbar
	Qamdo
	Shigatse Sakya
	Tinggri
}
