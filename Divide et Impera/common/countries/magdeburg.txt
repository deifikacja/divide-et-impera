#Country Name: Please see filename.

graphical_culture = latingfx

color = { 189  115  99 }

historical_ideas = { feudal_castles
	church_attendance_duty
	merchant_adventures
	patron_of_art
	military_drill
	national_bank
	national_conscripts
	national_trade_policy
	cabinet
	smithian_economics
	scientific_revolution
	engineer_corps
	grand_army
}

historical_units = {
	#Infantry German Cities
	w_i_armed_peasantry
	w_i_municipal_militia
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_hungarian_formation
	w_i_dutch_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_british_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Albrecht #2" = 40
	"Friedrich #2" = 40
	"G�nther #1" = 20
	"August #0" = 20
	"Christian Wilhelm #0" = 20
	"Ernst #0" = 20
	"Joachim Friedrich #0" = 20
	"Johann #0" = 20
	"Johann Albrecht #0" = 20
	"Leopold Wilhelm #0" = 20
	"Siegmund #0" = 20
	"Dietrich #1" = 15
	"Ludwig #1" = 15
	"Otto #1" = 15
	"Peter #1" = 15
	"Burkhard #3" = 10
	"Heinrich #2" = 10
	"Konrad #2" = 10
	"Bernhard #1" = 10
	"Erich #1" = 10
	"Rudolf #1" = 10
	"Ruprecht #1" = 10
	"Adolf #0" = 3
	"Clemens #0" = 3
	"Diederick #0" = 3
	"Eberhard #0" = 3
	"Fabian #0" = 3
	"Gottfried #0" = 3
	"Helmut #0" = 3
	"Herbert #0" = 3
	"Hermann #0" = 3
	"Jakob #0" = 3
	"Luitpold #0" = 3
	"Manfred #0" = 3
	"Markus #0" = 3
	"Rainer #0" = 3
	"Siegfried #0" = 3
	"Stefan #0" = 3
	"Viktor #0" = 3
	"Wolfgang #0" = 3
}

leader_names = { 
	Ahrendt Ahrenkiel B�rwald Bastel Bause Bollers
	Burghoff Herms Meyer Sch�nebeck Schlehenstein Schulz 
	Zimmerh�ckel Winter Wahrenholz W�ger 
}

ship_names = {
	"Barleber See" Friedrichstadt Buckau
	Diesdorf Silberberg Kreuzhorst Lemsdorf
	Olvenstedt Pechau Prester Randau Rothensee
	Salbke Sudenburg Westerh�sen
}

army_names = {
	"Armee von $PROVINCE$" 
}