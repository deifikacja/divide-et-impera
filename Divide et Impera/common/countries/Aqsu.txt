#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 10  10  150 }

historical_ideas = {
	national_conscripts
	glorious_arms
	grand_army
	military_drill
	battlefield_commisions
	shrewd_commerce_practise
	engineer_corps
	merchant_adventures
	national_trade_policy
	espionage
	humanist_tolerance
	cabinet
}

historical_units = {
	yuan_2nd_category
kazan_swarm
kalmyk_invaders
westernized_horde
yuan_3rd_category
tartar_militia
mongol_gunpowder_warfare
mongol_reformed_infantry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Babdagan #1" = 10
	"Urtu Baraq #1" = 10
	"Puladchi #1" = 10
	"Hudaidad #1" = 10
	"Seyyed Ahmad #0" = 10
	"Seyyed 'Ali #0" = 10
	"Sansiz #0" = 10
	"Muhammad Haidar #0" = 10
	"Mirza Abu Bakr #0" = 10
	"Mansur Khan #0" = 10
	"Imal Khodja #0" = 10
	"Abd ar-Rashid Khan #0" = 10
	"Muhammad Khan #0" = 10
	"Abd al-Karim Khan #0" = 10
	"Muhammad Baki Sultan #0" = 10
	"Shah Khodja #0" = 10
	"Timur Sultan #0" = 10
	"Hashin Mirza Bairin #0" = 10
	"Iskandar Sultan #0" = 10
	"Sultan Ahmad #0" = 10
	"Abdallah Khan #0" = 10
	"Gazi Shah Khodja #0" = 10
	"Shah Beg #0" = 10
	"Shahid Mirza Churas #0" = 10
	"Nur ad-Din Khan #0" = 10
	"Ismail Khan #0" = 10
	"Ayyub Khodja #0" = 10
	"Avdai Beg #0" = 10
	"Sadiq Beg #0" = 10
	"Hakim Khan Tura #0" = 10
}

leader_names = {
	"ibn Ahmad"
	"ibn ar-Rashid"
	Hattori
	Olzvoi
	Bira
	Batmunkh
	Ganbold
	Saihan
	Ochirbal
	Mirza
}

ship_names = {
	Bargut Buzav Kerait Naiman "D�rvn ��rd"
	Khoshut Olo Dzungar Torgut Dorbot
	Aqsu "Altan Khan" Khoshot Ol�ts G�shi
}
