#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 248  134  153 }

historical_ideas = {
	national_conscripts
	superior_seamanship
	military_drill
	naval_fighting_instruction
	divine_supremacy
	battlefield_commisions
	shrewd_commerce_practise
	excellent_shipwrights
	merchant_adventures
	national_bank
	bureaucracy
	grand_navy
}

historical_units = {
	persian_footsoldier
persian_shamshir
persian_duel
persian_infantry
afsharid_reformed_infantry
durrani_rifled_musketeer
ali_bey_reformed_infantry
persian_rifle
persian_cavalry_charge
qizilbash_cavalry
new_qizilbash
abbasid
new_abbasid
nadir
new_nadir w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Abu Muhammad #5" = 50
	"Abu Maali #1" = 10
	"Ali #1" = 10
	"Abu'l-Jaysh #1" = 10
	"'Umar #1" = 10
	"Hilal #1" = 10
	"Khahtan #1" = 10
	"Mahzum #1" = 20
	"Sulayman #0" = 10
	"Umayr #0" = 10
       "'Abd al-Karim #0" = 3
	"Ayman #0" = 3
	"Barakat #0" = 3
	"Basir #0" = 3
	"Dawud #0" = 3
	"Fahd #0" = 3
	"Faris #0" = 3
	"Faruq #0" = 3
	"Firdaus #0" = 3
	"Fuad #0" = 3
	"Haidar #0" = 3
	"Hashim #0" = 3
	"Husam #0" = 3
	"Iqbal #0" = 3
	"Jabbar #0" = 3
	"Ja'far #0" = 3
	"Jamal #0" = 3
	"Junayd #0" = 3
	"Khaliq #0" = 3
	"Malik #0" = 3
	"Mansur #0" = 3
	"Mustafa #0" = 3
	"Najib #0" = 3
	"Qasim #0" = 3
	"Qusay #0" = 3
	"Rahim #0" = 3
	"Rash�d #0" = 3
	"Safi #0" = 3
	"Sakhr #0" = 3
	"Samir #0" = 3
	"Tariq #0" = 3
	"Wahid #0" = 3
	"Zahir #0" = 3
}

leader_names = {
	al-Husayn "ibn Mukram" "ibn al-Husayn" al-Din Nasir Quasim "ibn Ali Nasir al-Din" "ibn Ali" al-Fallah Arar al-Muzzafar Kahlan "ibn Habhan" "ibn Sulayman" "ibn Himyar"
}

ship_names = {
	Masqat Matrah "Al-Hajar" "Ash-Sharqi" S�r
	"Ra's Al-Hadd" Masirah "Al-Akhdar" "Al-Jabal"
	"Al-Batinah" Nizw� "Al-Gharbi" "Wadi Aswad"
	"Wadi Andam" "Wadi al-Bahta" "Ra's Abu Daud"
	"Jabal ash-Sh�m" "Ramlat al Wahibah" "Al-Hadd"
	Tiwi Fins Ibr� Quraqq�t "As-Sib" Sar�r Maskin
	Shinas Suhar "Al-Khaburah" Tan'am Dank
	"Umm As-Samim" "Tur at Masirah" "Al-Huqf"
}
