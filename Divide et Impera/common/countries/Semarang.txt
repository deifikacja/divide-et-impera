#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 25 100 100 }

historical_ideas = { naval_transport
	merchant_adventures  
	excellent_shipwrights 
	shrewd_commerce_practise 
	superior_seamanship
	national_conscripts
	national_trade_policy
	sea_hawks
	naval_fighting_instruction
	military_drill
	naval_glory
	smithian_economics
	cabinet
}

historical_units = {
	east_asian_spearmen
	eastern_bow
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	asian_musketeer
	reformed_asian_musketeer
	reformed_asian_cavalry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

leader_names = {
	Swarga Bagus Sugih "Bangun Tapa" Purbaya Angabehi "Bangun Kadaton" Wicaksana Ngabehi Tedjawulan
}

monarch_names = {
	"Sura Adimenggala #5" = 50
}

ship_names = {
	Sumedang
	"Gunung Merbabu" Sragen Sukoharjo Wonosari
	"Gunung Lawu" Delanggu Cepu Purwodarli
	Ngawi Grobogan Boja Amgarawa
}
