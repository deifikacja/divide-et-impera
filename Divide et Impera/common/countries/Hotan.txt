#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 10  100  100 }

historical_ideas = {
        shrewd_commerce_practise
	national_conscripts
	glorious_arms
	grand_army
	military_drill
	battlefield_commisions
	engineer_corps
	merchant_adventures
	national_trade_policy
	espionage
	humanist_tolerance
	cabinet
}

historical_units = {
kipchak_cavalry
kazan_swarm
crimean_swarm
westernized_horde
mongol_arch
tartar_arch
mongol_gunpowder_warfare
mongol_reformed_infantry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Yusuf #1" = 10
	"Kuraysh #0" = 10
	"Abu Said Sultan #0" = 10
	"Shah Khaydar #0" = 10
	"Abd al-Karim #0" = 10
	"Ibrahim #0" = 10
	"Ubaydallah #0" = 10
	"Hushang #0" = 10
	"Ibrahim #0" = 10
	"Abd al-Mumin #0" = 10
	"Khabib'ullah Khodja #0" = 10
	"Niyaz Beg #0" = 10
	"Muhammad 'Ali #0" = 10
	"Musa #0" = 10
	"Mahushan #0" = 10
}

leader_names = {
	"ibn Ahmad"
	"ibn ar-Rashid"
	Hattori
	Olzvoi
	Bira
	Batmunkh
	Ganbold
	Saihan
	Ochirbal
	Mirza 
        Bargut 
        Buzav 
        Kerait 
        Naiman 
	Khoshut 
        Olo 
        Dzungar 
        Torgut 
        Dorbot
	Khotan  
        Khoshot 
        Ol�ts 
        G�shi
}

ship_names = {
	Bargut Buzav Kerait Naiman "D�rvn ��rd"
	Khoshut Olo Dzungar Torgut Dorbot
	Khotan "Altan Khan" Khoshot Ol�ts G�shi
}
