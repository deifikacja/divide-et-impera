#Novgorod

graphical_culture = easterngfx

color = { 200  200  207 }

historical_ideas = { bellum_iustum
	merchant_adventures
	national_conscripts
	shrewd_commerce_practise
	national_trade_policy
	quest_for_the_new_world
	military_drill
	battlefield_commisions
	engineer_corps
	grand_army
	espionage
	glorious_arms
	smithian_economics
}

historical_units = {
	eastern_medieval_infantry
	eastern_knights
	slavic_stradioti
	eastern_militia
	muscovite_musketeer
	muscovite_caracolle
	muscovite_soldaty
	muscovite_cossack
	russian_petrine
	russian_lancer
	russian_green_coat
	russian_cuirassier
	eastern_skirmisher
	eastern_uhlan
	eastern_carabinier
	russian_mass w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Svyatoslav #7" = 20
	"Yaroslav #6" = 20
	"Rotislav #4" = 20
	"Vsevolod #3" = 20
	"Vasiliy #2" = 20
	"Yuriy #2" = 20
	"Aleksandr #1" = 20
	"Andrei #1" = 20
	"Dmitriy #1" = 20
	"Fyodor #1" = 20
	"Mikhail #1" = 20
	"Mstislav #6" = 10
	"Yaropolk #3" = 10
	"Roman #2" = 10
	"Rurik #2" = 10
	"Konstantin #1" = 10
	"David #2" = 5
	"Afanasiy #0" = 3
	"Akim #0" = 3
	"Alexei #0" = 3
	"Anatoliy #0" = 3
	"Arkadiy #0" = 3
	"Boris #0" = 3
	"Daniil #0" = 3
	"Dorofey #0" = 3
	"Feodosiy #0" = 3
	"Gavriil #0" = 3
	"Iosif #0" = 3
	"Ivan #0" = 3
	"Matveiy #0" = 3
	"Nazariy #0" = 3
	"Nikifor #0" = 3
	"Oleg #0" = 3
	"Prokopiy #0" = 3
	"Radomir #0" = 3
	"Saveliy #0" = 3
	"Semen #0" = 3
	"Sergei #0" = 3
	"Valeriy #0" = 3
	"Yevgeniy #0" = 3
}

leader_names = { 
	Velikopermsky Vymsky
}

ship_names = {
	Cherdyn Permia Vychegda Dvina
}
