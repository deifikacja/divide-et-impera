#Country Name: Please see filename.

graphical_culture = easterngfx

color = { 36  232  47 }

historical_ideas = { early_mining
	national_conscripts
	glorious_arms
	military_drill
	merchant_adventures
	battlefield_commisions
	grand_army
	engineer_corps
	espionage
	grand_navy
	national_trade_policy	
}

historical_units = {
	eastern_medieval_infantry
	eastern_knights
	slavic_stradioti
	eastern_militia
	muscovite_musketeer
	muscovite_caracolle
	muscovite_soldaty
	muscovite_cossack
	russian_petrine
	russian_lancer
	russian_green_coat
	russian_cuirassier
	russian_mass w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}
monarch_names = {
	"Giorgi #7" = 80
	"David #9" = 40
	"Bagrat #5" = 40
	"Vakhtang #4" = 40
	"Erekle #0" = 40
	"Luarsab #0" = 40
	"Simon #0" = 40
	"Teimuraz #0" = 40
	"Konstantine #1" = 20
	"Archil #0" = 20
	"Bakar #0" = 20
	"Iesse #0" = 20
	"Kaihosro #0" = 20
        "Rabia #0" = 20
        "Solomon #0" = 20
	"Rostom-Mirza #0" = 20
	"Alexander #1" = 10
	"Levan #0" = 10
	"Demetre #2" = 5
	"Astamur #0" = 5
        "Malkhaz #0" = 5
        "Guram #0" = 5
        "Daur #0" = 5
        "Nugzar #0" = 5
        "Sokrat #0" = 5
	"Thamar #1" = -5
	"Anna #0" = -5
	"Stefanoz #0" = 5
	"Ars #0" = 5
        "Zegnak #0" = 5
        "Sustar #0" = 5
        "Putu #0" = 5
        "Manuchar #0" = 5
        "Keilash #0" = 5
        "Seteman #0" = 5
        "Zurab #0" = 5
	"Anzori #0" = 3
	"Bebur #0" = 3
	"Birtveli #0" = 3
	"Djurkha #0" = 3
	"Gulikho #0" = 3
	"Kartlos #0" = 3
	"Koki #0" = 3
	"Kviria #0" = 3
	"Lukhum #0" = 3
	"Malkhazi #0" = 3
	"Mushni #0" = 3
	"Okhropir #0" = 3
	"Orbeli #0" = 3
	"Revaz #0" = 3
	"Takha #0" = 3
	"Torgva #0" = 3
	"Tornike #0" = 3
	"Tsotne #0" = 3
	"Xareba #0" = 3
	"Zviadi #0" = 3
}

leader_names = { 
	Andronikashvili
	Asrenidze Avashvili 
	Bakradze Chkheidze 
	Chkhenkeli Devdariani
	Gabashvili Gardapkhadze
	Gedevanishvili Gegechkori
	Gulisashvili Gvazava
	Javakhishvili Karalashvili
	Kereselidze Kvinitadze 
	Lomtatitdze Maglakelidze
	Mazniashvili Natadze
	Nikoladze Robakidze
	Sharashidze Surguladze
	Takaishvili Tsereteli
	Tsulukidze Vachnadze
	Zaldastanishvili Zhordania
        Abaza Akhba
        Ardzinba Chachkhalia
        Shinkuba Hutuni
        Shervashidze Bagratuni
        Chachba Anchabadze
        Shavliani Eshba
        Aiba Bagapsh
        Adleiba Ankvab
        Akishbaia Beria
        Tarba Djergenia
        Arshba Ashuba
        Djindjolia Gabiskiria
        Lakerbaia Lakoba
        Mikanba Jugelia
        Khadjimba Kharazia
        Khashba Ozgan
        Otyrba Shamba
        Saakian Mzhavia
        Gagulia Zarandia
        Tsugba Shartava
        Tabagua Labakhua
        Achba Jergenia
        
}

ship_names = {
	Aragveti Anacopia Artaani Abkhazeti
	Bichvinta Batomi
	Dmanisi Demetre
	Ereti
	Geguti Gelati
	Kutatsi Kharnabuji Kura
	Lasharisjvari Lore
	Mtkvari
	Narikala
	Ruisi Racha
	Seti Svaneti
	Tamar Tbilisi Telavi Tbilisi Tashiri
	Urbnisi
}
