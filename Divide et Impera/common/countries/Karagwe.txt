#Country Name: Please seefilename.

graphical_culture = africangfx

color = { 215  250  240 }

historical_ideas = {
	national_conscripts
	shrewd_commerce_practise
	divine_supremacy
	national_trade_policy
	military_drill
	engineer_corps
	battlefield_commisions
	merchant_adventures
	grand_army
	espionage
}

historical_units = {
	african_spearmen
	bantu_tribal_warfare
	bantu_plains_warfare
	bantu_gunpowder_warfare
	westernized_bantu
	african_western_franchise_warfare
}


monarch_names = {
	"Ntare #3" = 60
	"Macwa #1" = 10
	"Rwabirere #1" = 10
	"Karara #1" = 10
	"Karaiga #1" = 10
	"Kahaya #1" = 10
	"Ruhinda #3" = 10
	"Bwarenga #1" = 10
	"Rwebishengye #1" = 10
	"Kayungu #1" = 10
	"Rwanga #1" = 10
	"Gasyonga #1" = 10
	"Mutambuka #1" = 10
	"Mukwenda #1" = 10
	"Kahitsi #1" = 10
	"Macwa #1" = 10
}

leader_names = {
Kitabanyoro Rugingiza Rutahaba Rutashijuuka Kahigiriza
}

ship_names = {
	Karawge Unyamwezi
}
