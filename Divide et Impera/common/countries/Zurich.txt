#Country Name: Please see filename.

graphical_culture = latingfx

color = { 3 115 229 }

historical_ideas = { liberal_arts
	national_conscripts
	grand_army
	glorious_arms
	military_drill
	battlefield_commisions
	espionage
	engineer_corps
	national_bank
	national_trade_policy
	cabinet
}

historical_units = {
	#Infantry Switzerland
	w_i_armed_peasantry
	w_i_crossbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_condottiere
	w_i_halberdiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_hungarian_formation
	w_i_spanish_formation
	w_i_swedish_formation
	w_i_quarter_army
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_british_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon 
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Eitel Friedrich #0" = 60
	"Jost Nikolaus #0" = 40
	"Christoph #0 Friedrich" = 20
	"Friedrich Ludwig #0" = 20
	"Friedrich Wilhelm #0" = 20
	"Joachim #0" = 20
	"Josef Friedrich #0" = 20
	"Karl #0" = 20
	"Philipp #0" = 20
	"Ernst #0" = 10
	"Franz Xaver #0" = 10
	"Georg Friedrich #0" = 10
	"Hermann Friedrich #0" = 10
	"Heinrich #0" = 10
	"Leopold Franz #0" = 10
	"Meinrad Josef #0" = 10
	"Albert Friedrich #0" = 5
	"Amadeus #0" = 5
	"Christoph #0" = 5
	"Eberhard #0" = 5
	"Felix Friedrich #0" = 5
	"Ferfried #0" = 5
	"Franz Anton #0" = 5
	"Franz Leopold #0" = 5
	"Friedrich Eitel #0" = 5
	"Friedrich Hans #0" = 5
	"Friedrich Karl #0" = 5
	"Hyeronimus Josef #0" = 5
	"Jakob #0" = 5
	"Johann Friedrich #0" = 5
	"Johann Nepomuk #0" = 5
	"Josef Wilhelm #0" = 5
	"Karl Ferdinand #0" = 5
	"Leopold Karl #0" = 5
	"Philipp #0 Friedrich" = 5
	"Clemens #0" = 3
	"Maximilian #0" = 3
	"Oskar #0" = 3
	"Siegfried #0" = 3
	"Wilhelm #0" = 3
}

leader_names = {
	Aebi Amman Bischofberger Blank Blies Brugger Combe
	Eugster Frei Frisch Gasenzer Gr�neisen Heberlin
	Helberingen Hertzler Hiltbrand Honegger Luz Miller
	Munzinger Oberholzer Schwendimann Sp�rri Vogt
	"von Baltharsar" W�chter "von Hasseln" Zumbach
}

ship_names = {
	Uri Schwyz Unterwalden Glarus Zug
	Lucerne Z�rich Berne "Wilhelm Tell"
	Dufour Rhein Rh�ne Inn Ticino
}
