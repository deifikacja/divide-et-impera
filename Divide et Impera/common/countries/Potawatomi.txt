#Country Name: Please see filename.

graphical_culture = northamericagfx

color = { 110 121 61 } 

historical_ideas = { bellum_iustum
	humanist_tolerance
	national_conscripts
	battlefield_commisions
	grand_army
	national_trade_policy
	military_drill
	cabinet
	engineer_corps
	bill_of_rights
	patron_of_art
	espionage
	smithian_economics
}

historical_units = {
	native_indian_archer
	native_indian_tribal_warfare
	algonkin_tomahawk_charge
	native_indian_horsemen
	commanche_swarm
	native_indian_mountain_warfare
	sioux_dragoon
	american_western_franchise_warfare
}

monarch_names = {
"Madouche" = 10
"Millouisillyny" = 10
"Onanghisse" = 10
"Otchik" = 10
"Nanaquiba" = 10
"Ninivois" = 10
"Peshibon" = 10
"Washee" = 10
"Manamol" = 10
"Siggenauk" = 10
"Mukatapenaise" = 10
"Waubansee" = 10
"Waweachsetoh" = 10
"Mucktypoke" = 10
"Senachewine" = 10
"Shabbona" = 10
"Main Poc" = 10
"Micsawbee" = 10
"Notawkah" = 10
"Nuscotomeg" = 10
"Mesasa" = 10
"Chebass" = 10
"Onaska" = 10
"Topinbee" = 10
"Aubenaubee" = 10
"Askum" = 10
"Keesass" = 10
"Kewanna" = 10
"Kinkash" = 10
"Magaago" = 10
"Monoquet" = 10
"Tiosa" = 10
"Metea" = 10
"Winamac" = 10
"Winm�g" = 10
"Wabnaneme" = 10
"Maumksuck" = 10
"Wabash" = 10
"Menominee" = 10
"Pamtipee" = 10
"Mackahtamoah" = 10
"Pashpoho" = 10
"Pepinawah" = 10
"Pokagon" = 10
"Sauganash" = 10
"Shupshewahno" = 10
"Topinbee" = 10
"Wabanim" = 10
"Michicaba" = 10
"Wanatah" = 10
"Weesionas" = 10
"Wewesh" = 10
}

leader_names = {
}

ship_names = {
	Chillicothe Cheketecaca
	Hathawekela
	Kispokotha
	Mequachake
	Pekuwe
	"weewa'kanakithita" "Wiipekwa Weshe" "Mshkwaawi Peshikthe" "Papapanawe Meshewa" 
	"Unemake Meshewa" "Wa'kanakya Conee" "Mshkwaawi Sepe"
}
