#Country Name: Please see filename.

graphical_culture = africangfx

color = { 250 250 255 }

historical_ideas = { naval_transport
	merchant_adventures
	excellent_shipwrights
	shrewd_commerce_practise
	superior_seamanship
	national_conscripts
	national_trade_policy
	sea_hawks
	naval_fighting_instruction
	military_drill
	naval_glory
	battlefield_commisions
	engineer_corps
}

historical_units = {
	polynesian_daggermen
	polynesian_spearmen
	polynesian_clubmen
	polynesian_javelinmen
	polynesian_long_clubmen
	polynesian_shark_tooth
	polynesian_two_blade
	polynesian_throwing_axe
	polynesian_lua
	polynesian_slingermen
	polynesian_rifles
}

monarch_names = {                         
	"Tu'ipulatu #2" = 10
	"Fatafehi #1" = 10
	"Mapa #1" = 10
	"Fuatakifolaha #1" = 10
	"Maealiuaki #1" = 10               
	"Mulikiha'amea #1" = 10
	"Vuna #1" = 10  
	"Ma'afu'otu'itonga #1" = 10
	"Tupoulahi #1" = 10
	"Maealiuaki #1" = 10
	"Tupoulahisi'i  #1" = 10               
	"Tu'i #1" = 10
	"Tupoumahe'ofo #0" = -10 
	"Tupou #0" = 10
	"Talai #0" = 10 
	"Tupouto'a #0" = 10 
	"'Alea Motu'a #0" = 10 
	"Siaosi Taufa'ahau #0" = 10 
	"Salote #0" = -10 
	"Taufa'ahau #0" = 10 
}

leader_names = {
		Paulaho Ma'ulupekotofa Fununaiva Laufilitong Tu'i'oetau Halafatai Mumui Tupou Malohi Tupouifaletuipapai "Tupou Maeakafa"
}

ship_names = {
		Madolenihmw Nahmwarki Isipanu Sangoro Soukiseleng Kitti U Sokehs
}

fleet_names = {
	"King's Canoes"
}