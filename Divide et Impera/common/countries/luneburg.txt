#Country Name: Please see filename.

graphical_culture = latingfx

color = { 135  109  147 }

historical_ideas = { feudal_castles
	shrewd_commerce_practise
	national_conscripts
	merchant_adventures
	humanist_tolerance
	patron_of_art
	national_bank
	national_trade_policy
	ecumenism
	cabinet
	smithian_economics
	military_drill
	engineer_corps
}

historical_units = {
	#Infantry German
	w_i_armed_peasantry
	w_i_longbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_hungarian_formation
	w_i_spanish_formation
	w_i_chosen_formation
	w_i_swedish_formation
	w_i_quarter_army
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_prussian_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Otto #0" = 60
	"Heinrich #5" = 40
	"Ernst #0" = 40
	"Friedrich #2" = 20
	"Bernhard #1" = 20
	"August #0" = 20
	"Christian #0" = 20
	"Christian Ludwig #0" = 20
	"Ernst August #0" = 20
	"Franz #0" = 20
	"Georg #0 Wilhelm" = 20
	"Georg #0" = 20
	"Johann Friedrich #0" = 20
	"Magnus #2" = 10
	"Franz Otto #0" = 10
	"Johann #0" = 10
	"Alexander #0" = 3
	"Alois #0" = 3
	"Anton #0" = 3
	"Bruno #0" = 3
	"Clemens #0" = 3
	"Denis #0" = 3
	"Gerfried #0" = 3
	"Gottfried #0" = 3
	"G�nther #0" = 3
	"Helmut #0" = 3
	"Herbert #0" = 3
	"Ignatz #0" = 3
	"Isidor #0" = 3
	"Jakob #0" = 3
	"Konstantin #0" = 3
	"Kuno #0" = 3
	"Markus #0" = 3
	"Martin #0" = 3
	"Oskar #0" = 3
	"Reinhard #0" = 3
	"Silvester #0" = 3
	"Stefan #0" = 3
	"Viktor #0" = 3
	"Waldemar #0" = 3
}

leader_names = {
	Ahlers Martin Pielken Reinbeck S�lter "von Hodenberg"
	Westermann Basedow Beu� Bostelmann Breden Burmeister
	Dietz D�pner Flathmann Garbade Gefken Gehrken 
}

ship_names = {
	"Markgraf Billung" "Bischof Amelung"
	"St. Michaelis" "Luniburcs" "Herzog Otto"
	Kalkberg Illmenau "Herzog Wilhelm"
	"Magnus Torquatus" "Herzog Ernst"
}

army_names = {
	"Armee von $PROVINCE$" 
}