#Country Name: Please see filename.

graphical_culture = latingfx

color = { 199  175  12 }

historical_ideas = { 
	feudal_castles
	national_conscripts
	bill_of_rights
	superior_seamanship
	merchant_adventures
	excellent_shipwrights
	cabinet
	national_bank
	press_gangs
	smithian_economics
	scientific_revolution
	glorious_arms
	napoleonic_warfare
}

historical_units = {
	#Infantry Britain
	w_i_armed_peasantry
	w_i_longbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_british_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon 
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
"James #1" = 120
"Alexander #3" = 10
"Mary #0" = -8
"Charles #0" = 7
"William #1" = 4
"Robert #2" = 20
"David #2" = 4
"John #1" = 10
"Henry #0" = 3
"Arthur #0" = 3
"Murdoch #0" = 3
"Walter #0" = 3
"Malise #0" = 3 
"Margaret #1" = -1
"Isabella #0" = -1
"Andrew #0"  = 0
"Archibald #0"  = 0
"Colin #0"  = 0
"Esm� #0"  = 0
"Francis #0"  = 0
"Gavin #0"  = 0
"George #0"  = 0
"Gilbert #0"  = 0
"Lucius #0"  = 0
"Matthew #0"  = 0
"Patrick #0"  = 0
"Richard #0"  = 0
"Thomas #0"  = 0
}

leader_names = {
Baillie Beaton Boyd Buchanan Burnet
Cameron Campbell Carr Carstares Cary Cleland Cunningham
Dalrymple Douglas Drummond Dunbar
Erskine
Fletcher
Gordon Graham Gray
Hamilton Hay Henryson Hepburn Heriot Hume
Johnston
Kennedy Ker Kirkcaldy Knox
Leslie Lindsay Lockhart
MacDonald Mackenzie Maitland Melville Middleton Moray Murray
Napier
Ruthven
Sharp Stewart Stuart
Watson Wishart	
}

ship_names = {
	Advantage "Ann of Anstruther"
	Bonaventure Bruce
	Charles "Christian of Boness" Christopher Cuckoo
	Flower Fortune
	"Good Fortune" Green-tree
	"Great Michael"
	James Janet "Jenny Pirwin"
	"King's Caravel"
	"Lamb of Leith" Lesley Lion
	Margaret "Margaret of Peterhead" "Mary Willoughby" "Mary Walsingham" Mercury Moon Morton
	Pelican "Prince Rupert" "Providence of Dundee"
	"Rainbow of Dundee" Rothes
	Salamander "St. Salvator"
	Thistle
	Unicorn
	Venture
	Wemyss
	"Yellow Caravel"
}
