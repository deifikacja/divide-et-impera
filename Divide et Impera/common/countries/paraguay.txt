#Country Name: Please see filename.

graphical_culture = latingfx

is_colonial = yes

color = { 75  188  60 }

historical_ideas = {
	national_conscripts
	church_attendance_duty
	military_drill
	divine_supremacy
	battlefield_commisions
	deus_vult
	shrewd_commerce_practise
	engineer_corps
	merchant_adventures
	grand_army
	national_trade_policy
	revolution_and_counter
}

historical_units = {
	#Infantry Iberia
	w_i_armed_peasantry
	w_i_longbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_chosen_formation
	w_i_quarter_army
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_french_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Fulgencio #0" = 20
	"Jos� #0 Gaspar" = 20
	"Juan #0 Jos�" = 15
	"Manuel #0 Antonio" = 15
	"Carlos #0 Antonio" = 10
	"Mariano #0" = 10
	"Abel #0" = 3
	"Agust�n #0" = 3
	"Alberto #0" = 3
	"�lvaro #0" = 3
	"Antonio #0" = 3
	"Asdr�bal #0" = 3
	"Bartolom� #0" = 3
	"Carlos #0" = 3
	"Daniel #0" = 3
        "David #0" = 3
	"Domingo #0" = 3
	"Emiliano #0" = 3
	"Enrique #0" = 3
        "Evaristo #0" = 3
	"Fabricio #0" = 3
	"Fernando #0" = 3
        "Fidel #0" = 3
	"Gabriel #0" = 3
        "Jos� #0" = 3
	"Juan #0" = 3
	"Leonardo #0" = 3
	"Manuel #0" = 3
	"Marcos #0" = 3
        "Miguel #0" = 3
	"Narciso #0" = 3
	"Octavio #0" = 3
	"Pablo #0" = 3
	"Patricio #0" = 3
	"Pedro #0" = 3
	"Roberto #0" = 3
        "Rufino #0" = 3
	"Santiago #0" = 3
	"Tadeo #0" = 3
	"Ul�ses #0" = 3
}

leader_names = {
	Alberti �lvarez "de Alvear" Artigas Azcuenaga Balcarce Baz�n Belgrano Brown Bustos
	Cabral Castro Chiclana Costa Derqui Dorrego Gonz�lez Goyeneche Guazurary "de G�emes"
	Heredia Jonte "Kaunitz de Holmberg" "de Laprida" Larrea "de las Heras" L�pez
	Mansilla Matheu Monteagudo Moreno Paso Paz Pe�a P�rez Planes Posadas "de Pueyrred�n"
	Ram�rez Riglos Robinson Rodr�guez Rondeau "de Rosas"
	Saavedra S�enz "de San Mart�n" Sarratea Urquiza Warnes
}

ship_names = {
	"Asunci�n"
	"Gaspar"
	"El Plata"
	"Fulgencio"
	Independencia
	Libertad "Yegros"
	Moreno 
	"Gran Chaco"
	Rivalda
}
