#Country Name: Please see filename.

graphical_culture = latingfx

color = { 165  71  80 }

historical_ideas = { liberal_arts
	patron_of_art
	merchant_adventures
	shrewd_commerce_practise
	national_trade_policy
	national_bank
	national_conscripts
	military_drill
	engineer_corps 
	smithian_economics
	scientific_revolution
	espionage
	battlefield_commisions
}

historical_units = {
#Infantry Italy
	w_i_armed_peasantry
	w_i_crossbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_condottiere
	w_i_halberdiers
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_french_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon 
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Cesare #0" = 20
	"Pandolfo #0" = 20
	"Rafaello #0" = 20
	"Francesco #0" = 20
	"Fabio #0" = 20
	"Borghese #0" = 20
	"Gaspare #0" = 10
	"Adriano #0" = 3
	"Mathilda #1" = -5
	"Bernardo #0" = 3
	"Beniamino #0" = 3
	"Biagio #0" = 3
	"Boniface #0" = 3
	"Carlo #0" = 3
	"Constantino #0" = 3
	"Damiano #0" = 3
	"Ezzolino #0" = 3
	"Eros #0" = 3
	"Eustachio #0" = 3
	"Potho #0" = 3
	"Fortunato #0" = 3
	"Gennaro #0" = 3
	"Giancarlo #0" = 3
	"Marquard #1" = 10
	"Giovanni #0" = 3
	"Giustino #0" = 3
	"Gaidoald #1" = 10
	"Leone #0" = 3
	"Marco #0" = 3
	"Mateo #0" = 3
	"Rothari #1" = 10
	"Pasquale #0" = 3
	"Rinaldo #0" = 3
	"Roberto #0" = 3
	"Sergio #0" = 3
	"Tedaldo #0" = 3
	"Alachi #1" = 10
	"Valerio #0" = 3
	"Vincenzo #0" = 3
	"Vitale #0" = 3
}

leader_names = {
        Accarigi Alighieri Amelia Barone Barzagli Borghese Borgia
        Castro Cecchereni Compagnoni Damiani "Di Stefano"
	Fossombroni Galilei Gallo Gentile "Golia di Siena" Griccioli
	Grosso Guerra Idoni Labriola Nardi Neri Oddo Orlando Palumbo
	Pannocchieschi Petrucci Pianella Piccolomini "del Piero" Pirlo "da Porcari"
	Quercegrossa Riario "de Rossi" Salimbeni "da Sangallo" Segni "da Staggia"
	Testa Torricelli Uccello Vagliagli Visconti Vitali Viviani Zaccardo  Zeti
}

ship_names = {
	"Ambrogio Lorenzetti" Asciano
	"Bartolomeo Guidi" "Bonaguida Lucari"
	"Carlo IV" "Castelnuovo Berardenga" Chianti  Costalpino "Crete Senesi"
	Donatello "Duccio di Buoninsegna" 
	Elsa
	"fiumi Arbia" "Francesco di Rinaldo"
	"Ghibellini" "Giovanni di Balduccio"
	"Isola d'Arbia" 
	Merse "Minuccio di Rinaldo" "Misser santo Micchele arcangelo" Montagnola
	Montaperti Monteriggioni "Monteroni d'Arbia"
	"Orlando Bonsignori"
	"San Bernardino" "San Domenico" "San Francesco" "San Galgano"
	"San Giovanni" "San Miniato" "Sant'Ansano" "Santa Caterina"
	"Santa Maria dei Servi" "Santissima Annunziata" Senius Sovicille 
	"Taverne d'Arbia" Lamborghini
}
