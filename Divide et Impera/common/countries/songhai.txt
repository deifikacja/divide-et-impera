#Country Name: Please seefilename.

graphical_culture = africangfx

color = { 62  175  151 }

historical_ideas = { bellum_iustum
	national_conscripts
	shrewd_commerce_practise
	divine_supremacy
	national_trade_policy
	military_drill
	engineer_corps
	battlefield_commisions
	merchant_adventures
	grand_army
	espionage
	glorious_arms
	cabinet
}

historical_units = {
mali_trooper
songhai_trooper
sahel_trooper
sudan_trooper
niger_gunpowder_warfare
westernized_ethiopian
adal_guerilla_warfare
mali_cavalry
songhai_cavalry
sudan_cavalry
nubian_cavalry
fulani_cavalry
}

monarch_names = {
	"Muhammad #0" = 140
	"Karbifo #0" = 20
	"Mar Fai #0" = 20
	"Mar-Arkana #0" = 20
	"Mar Arandan #0" = 20
	"Sulaym�n #0" = 20
	"`Al� Ber #0" = 20
	"Bakar� #0" = 20
	"M�s� #0" = 20
	"Ism�`�l #0" = 60
	"Ish�q #0" = 60
	"D�`�d #0" = 100
	"N�h #0" = 40
	"H�r�n #0" = 20
	"al-Am�n #0" = 20
	"Hanga #0" = 20
	"Samsu #0" = 20
	"Harangi #0" = 20
	"Tomo #0" = 20
	"Silman #0" = 3
	"Abu Bakr #0" = 3
	"Fodi Mayrumfa #0" = 3	
	"Al-Mustafa #0" = 3
	"Harun #0" = 3
	"Tomo #0" = 3
	"Bassaru Missi #0" = 3
	"Bumi #0" = 3
	"Koyze Baba #0" = 3
	"Wankoy #0" = 3
	"Mar Kiray #0" = 3
	"Bakr Zanku #0" = 3
	"Salman #0" = 3
	"Birahim #0" = 3
	"Hammadi #0" = 3
	"Al-Mansur #0" = 3
	"`Ammar #0" = 3
	"Sa`id #0" = 3
	"`Abd Allah #0" = 3
	"Nasr #0" = 3
	"Yahya #0" = 3
	"Yusuf #0" = 3	
}

leader_names = {
	Zerbo
	Ouedraogo
	Keita
	Lamizana
	Toure
	Diarra
	Diallo
	Modibo
	Amadou
	Compaore
}

ship_names = {
	Alayaman Zakoi Takoi Akoi Ku "Ali-Fay"
	"Biyo-Kumoy" "Biyu" "Za Kuroy" "Yama-Karaway"
	Kukuray Kinkin Nintasanay
}
