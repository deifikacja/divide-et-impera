#Country Name: Please see filename.

graphical_culture = easterngfx

color = { 139  38  60 }

historical_ideas = { bellum_iustum
	national_conscripts
	glorious_arms 
	grand_army
	military_drill
	battlefield_commisions 
	shrewd_commerce_practise
	engineer_corps
	espionage
	merchant_adventures
	national_trade_policy
	bureaucracy
	cabinet
}

historical_units = {
	east_asian_spearmen
	eastern_bow
	east_mongolian_steppe
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	asian_musketeer
	reformed_asian_musketeer
	reformed_asian_cavalry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Nahratch #0" = 10
	"Agai #0" = 10
	"Ablegrim #0" = 10
	"Baim #0" = 10
	"Kulak #1" = 10
	"Kazak #0" = 10
	"Kyntsha #0" = 10
	"Satyga #0" = 10
	"Obaitko #0" = 10
        "Utshyut #0" = 10
        "Taustei #0" = 10
}

leader_names = { 
	Balna Salmiian Salmin Ravna Taiborey Vylka
	Kani Maimbava Arin Aven Tsoy
}

ship_names = {
	Koda Belgorod Ob Tabary
}
