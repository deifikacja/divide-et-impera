#Country Name: Please see filename.

graphical_culture = easterngfx

color = { 128  170  63 }

historical_ideas = { early_mining
	national_conscripts
	glorious_arms
	military_drill
	merchant_adventures
	battlefield_commisions
	grand_army
	engineer_corps
	espionage
	grand_navy
	national_trade_policy
	national_bank
	cabinet
}

historical_units = {
	bardiche_infantry 
	eastern_bowman
	eastern_knights
	slavic_stradioti
	eastern_militia
	muscovite_musketeer
	muscovite_caracolle
	muscovite_soldaty
	muscovite_cossack
	russian_petrine
	russian_lancer
	russian_green_coat
	russian_cuirassier
	eastern_skirmisher
	eastern_uhlan
	eastern_carabinier
	russian_mass w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"David #0" = 20
	"Mkhitar #0" = 20
	"Ashot #4" = 15
	"Gagik #2" = 15
	"Smbat #3" = 15
	"Abbas #1" = 15
	"Hovhannes Smbat #1" = 15
	"Vagram #1" = 10
	"Musegh #0" = 10
	"Sahak #0" = 10
	"Tachar #0" = 10
	"Arakel #0" = 3
	"Ari #0" = 3
	"Armen #0" = 3
	"Arsen #0" = 3
	"Arshavir #0" = 3
	"Avedis #0" = 3
	"Badouagan #0" = 3
	"Bedros #0" = 3
	"Berjouhi #0" = 3
	"Boghos #0" = 3
	"Ghoukas #0" = 3
	"Hagop #0" = 3
	"Haig #0" = 3
	"Hovhannes #0" = 3
	"Hovsep #0" = 3
	"Jirair #0" = 3
	"Kapriel #0" = 3
	"Karayan #0" = 3
	"Kevork #0" = 3
	"Khajag #0" = 3
	"Krikor #0" = 3
	"Levon #0" = 3
	"Movses #0" = 3
	"Nairi #0" = 3
	"Sevan #0" = 3
	"Taniel #0" = 3
	"Vahan #0" = 3
	"Zadig #0" = 3
	"Zoravan #0" = 3
}

leader_names = { 
	Adamian Adomian
	Aghajanian Altounyan 
	Apkarian Bagian
	Babaian Babikian
	Chobanian Bhobanyan
	Damadian Eskandarian
	Kapikian Kazanjian Kebabian
	Kevorkian Khachikyan
	Khatchaturian Kirlian
	Mergelyan Orbeli
	Pogosyan Sadian Simjian 
	Sevanian Meymarian Shahinian
}

ship_names = {
	Aramu Argishtis Ararat Ani Artsakh
	Dvin
	Erimena
	Haik Haldi
	Ishpuinis
	Kars
	Lutipri
	Musasir Menuas Manzikert
	Parisos
	"Rusas II"
	Siwini Selardi "Sarduri I" "Sarduris II"
	Vaspurakan
	Yerevan
}
