#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 100  200  10} 

historical_ideas = { liberal_arts
	national_conscripts
	grand_army
	glorious_arms
	espionage
	military_drill
	merchant_adventures
	divine_supremacy
	battlefield_commisions
	engineer_corps
	national_trade_policy
}

historical_units = {
	muslim_footsoldier
muslim_shamshir
mamluk_duel
tofongchis_musketeer
muslim_sturm
durrani_rifled_musketeer
ali_bey_reformed_infantry
persian_rifle
cavalry_archers
swarm
new_swarm
new_steppe
durrani_swivel
durrani_dragoon w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Karaca #1" = 20
	"Muhammad #1" = 20
	"Sulaym�n #1" = 20
	"Malik Arsl�n Beg #0" = 10
	"B�d�q Beg #0" = 20
	"Sh�hsuw�r #0" = 20
	"Al� Beg #1" = 20	
}

leader_names = {
	Balian
	Erez
	Arif
	Yucel
	Tansel
	Sezer
	Arikan
	Aygun
	Rahim
	Saglam
}

ship_names = {
	Zulkadriyye Maras Afsin Elbistan Goksun Pazarcik 
	Ceyhan Firat Akcadag Saimbeyli Kadirli Besni Tohma Darende
}
