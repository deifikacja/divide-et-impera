#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 50  57  152 }

historical_ideas = {
	national_conscripts
	military_drill
	divine_supremacy
	engineer_corps
	grand_army
	merchant_adventures
	shrewd_commerce_practise
	patron_of_art
	national_trade_policy
	espionage
	glorious_arms
	national_bank
}

historical_units = {
	muslim_footsoldier
muslim_shamshir
mamluk_duel
muslim_infantry
muslim_sturm
muslim_mass_infantry
ali_bey_reformed_infantry
persian_rifle
musellem
shaybani
new_shaybani
abbasid
new_muslim_clan
ways
new_ways w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"'Ali #0" = 10
	"Mahdi #0" = 10
	"Zulfigar #0" = 10
	"Pir Budaq #0" = 10
	"Murtaza #0" = 10
	"Pir Bulaq #0" = 10
	"Qalbali #0" = 10
	"Hudaidad #0" = 10
	"Ibrahim #0" = 10
	"Azad #0" = 10
	"Fath 'Ali #0" = 10
	"Muhammad #0" = 40
	"Reza #0" = 10
	"Najaf #0" = 10
	"Khudadad #0" = 10
	"Husain #0" = 10
	"Ahmad #0" = 10
	"Abbas #0" = 10
	"Faridun #0" = 10
	"Nasr #0" = 10
	"Muzzafar #0" = 10
	"Najafqulu #0" = 20
}

leader_names = {
	Khan Quli "Quli Khan" Karamanlu "Khan Karamanlu" Pirnaq Qajar Sultan Beg "Beg Qasimlu" Qasimlu Afshar "Khan Afshar" Husein "Husein Khan" "Khan Marandi" Marandi "Khan Mukaddam" Mukaddam Mirza ad-Din "ad-Din Mirza" Tagi "Tagi Mirza" Ali "Ali Mirza"
}

ship_names = {
	Abathur "Adamn Kasia" "Aesma Daeva" Agas
	Ahriman Ahura "Ahura Mazda" Ahurani Airyaman
	"Aka Manah" Aladdin Allatum Amashaspan Ameratat
	"Amesha Spentas" Anahita "Angra Mainyu" Anjuman
	"Apam-natat" Apaosa Aredvi Arishtat Armaiti Arsaces
	"Asha vahista" Asman "Asto Vidatu" "Astvat-Ereta"
	Atar "Azi Dahaka" Baga Bahram Burijas Bushyasta Buyasta
	Camros Daena Daevas Ahaka Dahhak Dena Dev Diwe Drug
	Drvaspa Frashegird Fravashis Gandarewa "Gao-kerena"
	Gayomard Gayomart "Geus-Tasan" "Geus-Urvan" Haoma
	Haurvatat Humay Hvar Hvarekhshaeta Indar Indra Izha
	Jamshid Jeh Karshipta Kavi Khara "Khshathra vairya"
	Kundrav Mah Mahre Mahrianag Manu Manuchihir Mao 
	Mashyane Mashye Menog Mithra Nairyosangha Nanghaithya
	Neriosang Ormazd Peris Peshdadians Rapithwin Rashnu
	Rustam Saurva Simurgh "Spenta Mainyu" Sraosa Srosh
	Tawrich Thunaupa Tistrya Tushnamatay Vanant Vata
	Verethragna "Vohu Manah" Vouruskasha Yam Yasht Yazata
	Yezidi Yima Zal Zam "Zam-Armatay" Zarathustra Zarich
	"Zend-Avesta" Zurvan
}
