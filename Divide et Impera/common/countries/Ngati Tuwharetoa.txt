#Country Name: Please see filename.

graphical_culture = africangfx

color = { 3 204 3 }

historical_ideas = { naval_transport
	merchant_adventures
	excellent_shipwrights
	shrewd_commerce_practise
	superior_seamanship
	national_conscripts
	national_trade_policy
	sea_hawks
	naval_fighting_instruction
	military_drill
	naval_glory
	battlefield_commisions
	engineer_corps
}

historical_units = {
	polynesian_daggermen
	polynesian_spearmen
	polynesian_clubmen
	polynesian_javelinmen
	polynesian_long_clubmen
	polynesian_shark_tooth
	polynesian_two_blade
	polynesian_throwing_axe
	polynesian_lua
	polynesian_rifles
}

monarch_names = {                         
	"Te #1" = 10
	"Herea Te #1" = 10
	"Mananui Te #0" = 10
	"Iwikau Te #0" = 10
	"Horonuku Te #0" = 10
	"Tureiti Te #0" = 10
	"Hoani Te #0" = 10
	"Hepi Te #0" = 10
	"Tumu Te #0" = 10
}

leader_names = {
		 Rangituamatotoru Wakaiti Rangimaheuheu "Heuheu Tukino"
}

ship_names = {
		"Ariki Nui" Waikato "Ngati Tuwharetoa"
}

fleet_names = {
	"King's Canoes"
}