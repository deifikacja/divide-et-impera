#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 155  31  31 } 

historical_ideas = { liberal_arts
	national_conscripts
	grand_army
	glorious_arms
	espionage
	military_drill
	merchant_adventures
	divine_supremacy
	battlefield_commisions
	engineer_corps
	national_trade_policy
	bureaucracy
	cabinet
}

historical_units = {
	muslim_footsoldier
muslim_shamshir
mamluk_duel
muslim_infantry
muslim_sturm
muslim_mass_infantry
ali_bey_reformed_infantry
persian_rifle
musellem
shaybani
new_shaybani
abbasid
new_muslim_clan
ways
new_ways w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Sarukhan #1" = 10
	"Ilyas #1" = 10
	"Ishaq #1" = 10
	"Khidr #1" = 10
	"Orkhan #1" = 10
        "'Abd al-Karim #0" = 3
	"Ayman #0" = 3
	"Barakat #0" = 3
	"Basir #0" = 3
	"Dawud #0" = 3
	"Fahd #0" = 3
	"Faris #0" = 3
	"Faruq #0" = 3
	"Firdaus #0" = 3
	"Fuad #0" = 3
	"Haidar #0" = 3
	"Hashim #0" = 3
	"Husam #0" = 3
	"Iqbal #0" = 3
	"Jabbar #0" = 3
	"Ja'far #0" = 3
	"Jamal #0" = 3
	"Junayd #0" = 3
	"Khaliq #0" = 3
	"Malik #0" = 3
	"Mansur #0" = 3
	"Mustafa #0" = 3
	"Najib #0" = 3
	"Qasim #0" = 3
	"Qusay #0" = 3
	"Rahim #0" = 3
	"Rash�d #0" = 3
	"Safi #0" = 3
	"Sakhr #0" = 3
	"Samir #0" = 3
	"Tariq #0" = 3
	"Umar #0" = 3
	"Wahid #0" = 3
	"Zahir #0" = 3
}

leader_names = {
	Balian
	Erez
	Arif
	Yucel
	Tansel
	Sezer
	Arikan
	Aygun
	Rahim
	Saglam
}

ship_names = {
	Saruhanoglu Manisa
}
