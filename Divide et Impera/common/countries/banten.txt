#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 130  25  74 }

historical_ideas = { naval_transport
	merchant_adventures  
	excellent_shipwrights 
	shrewd_commerce_practise 
	superior_seamanship
	national_conscripts
	national_trade_policy
	sea_hawks
	naval_fighting_instruction
	military_drill
	naval_glory
	battlefield_commisions
	espionage
}

historical_units = {
	east_asian_spearmen
	eastern_bow
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	asian_musketeer
	reformed_asian_musketeer w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Maulana Hasanudin #0" = 20
	"Abdul Mufakir #0" = 20
	"Nur ud-Din Ibrahim #0" = 20
	"Yusuf Pasaran #0" = 20
	"Muhammad Sedo Ning Rana #0" = 20
	"Fatima #0" = 10
	"Fath Muhammad #0" = 20
}

leader_names = {
	Adi
	Purnomo
	Raharjo
	Santoso
	Aman
	Damar
	Bimo
	Harjuno
	Kresno
	Puntodewo
}

ship_names = {
	Lebak Pandeglang Serang Tangerang Cilegon
	Kujang "Selat Sunda" Panaitan Krakatau
	Sebuku Sabesi Sangiang Tundo Panjang Tinjal Deli
}
