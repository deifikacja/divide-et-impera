#Country Name: Please see filename.

graphical_culture = latingfx

color = { 84  124  48 }

historical_ideas = { 
	liberal_arts
	church_attendance_duty
	merchant_adventures
	patron_of_art
	military_drill
	national_bank
	national_conscripts
	national_trade_policy
	cabinet
	smithian_economics
	scientific_revolution
	bureaucracy
	engineer_corps
}

historical_units = {
	#Infantry German Cities
	w_i_armed_peasantry
	w_i_municipal_militia
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_hungarian_formation
	w_i_dutch_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_british_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon 
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Hermann #3" = 40
	"Friedrich #2" = 40
	"Gebhard #0" = 40
	"Adolf #2" = 20
	"Dietrich #1" = 20
	"Philipp #1" = 20
	"Anton #0" = 20
	"Anton Viktor #0" = 20
	"Clemens August #0" = 20
	"Ernst #0" = 20
	"Ferdinand #0" = 20
	"Maximilian Franz #0" = 20
	"Maximilian Friedrich #0" = 20
	"Maximilian Heinrich #0" = 20
	"Joseph Clemens #0" = 20
	"Rupprecht #0" = 20
	"Salentin #0" = 20
	"Engelbert #3" = 15
	"Heinrich #2" = 15
	"Siegfried #2" = 15
	"Konrad #1" = 15
	"Kuno #1" = 15
	"Walram #1" = 15
	"Wikbold #1" = 15
	"Wilhelm #1" = 15
	"Bruno #4" = 10
	"Adam #0" = 3
	"Alfred #0" = 3
	"Christian #0" = 3
	"Dominik #0" = 3
	"Gottlob #0" = 3
	"G�nther #0" = 3
	"Hans #0" = 3
	"Hyeronimus #0" = 3
	"J�rgen #0" = 3
	"Karl #0" = 3
	"Paul #0" = 3
	"Rudolf #0" = 3
	"Theodor #0" = 3
	"Tobias #0" = 3
}

leader_names = {
	Baten B�hmer Bombeck Breucker Buschmann Diekmann
	Eckrath Fischedick Fortkamp Boymann Bremer
	Lohmann Perdekamp Gr�ter Hegemann Kappenberg
	Hegemann Kn�mann Leggewe Liesenfeld Limberg 
	Glaser Lochtkemper Lohe Nathrath Ortmann 
	Pickenbrock Sank�hler Springmann Stratmann
	Stenkhoff "von Wedich" Wessels "von Westerburg"
}

ship_names = {
	"Erzbischof Brun" "Erbischof Anno" "Bischof Hildebold"
	"Erzbischof Gunthar" "Erzbischof Gero" "Rainald von Dassel"
	"Engelbert II" "Konrad Hochstaden" Rhein Erft Agger
}

army_names = {
	"Erzbischofliche Garde" "Armee von $PROVINCE$" 
}