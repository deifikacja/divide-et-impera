#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 127  202  135 }

historical_ideas = { bellum_iustum
	national_conscripts
	glorious_arms
	military_drill
	merchant_adventures
	battlefield_commisions
	grand_army
	engineer_corps
	espionage
	national_trade_policy
	cabinet
	bureaucracy
}

historical_units = {
yuan_2nd_category
kazan_swarm
kalmyk_invaders
westernized_horde
yuan_3rd_category
tartar_militia
mongol_gunpowder_warfare
mongol_reformed_infantry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Ughyth #1" = 10
	"Kara #1" = 10
	"Bajgrid #1" = 10
	"Masem #1" = 10
	"Muytiyen #1" = 10
	"Maikyi #1" = 10
	"Seit #0" = 10
	"Aldar #0" = 10
	"Kfisyom #0" = 10
}

leader_names = { 
	Khan Bey
}

ship_names = {
	Qazgan Qazan S�yembik� Qazansu Ghiasetdin 
	"Tas Ayaq" Alat Arca G�rec C�ri Nugay
	S�yet S�yex Qazi
}
