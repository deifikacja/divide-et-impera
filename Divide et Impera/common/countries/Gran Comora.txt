#Country Name: Please seefilename.

graphical_culture = africangfx

color = { 231  200  200 }

historical_ideas = { naval_transport
	national_conscripts
	shrewd_commerce_practise
	divine_supremacy
	national_trade_policy
	military_drill
	engineer_corps
	battlefield_commisions
	merchant_adventures
	grand_army
	espionage
}

historical_units = {
	african_spearmen
	bantu_tribal_warfare
	bantu_plains_warfare
	bantu_gunpowder_warfare
	westernized_bantu
	african_western_franchise_warfare
}


monarch_names = {
	"Muhammad #1" = 100
	"Sulaym�n #6" = 20
	"Ahmad #0" = 60
	"Hasan #5" = 80
	"Sa'�d #0" = 40
	"'Abd All�h #0" = 80
	"'Al� #5" = 40
	"Ibr�h�m #0" = 40
	"al-Fudail #0" = 20
	"Ab� Bakr #0" = 40
	 "Malindi #0" = 20
	"Sabo #0" = 20
	"al-Hasan #0" = 20
	"Munganaye #0" = 20
	"Y�suf #0" = 20
	"Bwana Mkuu #0" = 20
	"Mas'�d #0" = 20
	"Bwana Tamu #0" = 5  
	"Fumo Madi #0" = 5  
	"Makame #0" = 1  
	"Mwinyi Ahmad #0" = 1  
	"Sa'if #0" = 3  
	"Nasr #0" = 3  
	"Salih #0" = 3  
	"Fumo Luti #0" = 3
	"Bwana Waziri #0" = 3
	"Bwana Shaykh #0" = 3
	"Fumo Bakari #0" = 3
	"Fumo Umar #0" = 3
	"Khamis #0" = 3
	"Rashid #0" = 3
	"Mwinyi Hasan #0" = 3
	"Kombo #0" = 3
	"Ngwale #0" = 3
	"Ngwacani #0" = 3
	"Athmani #0" = 3
	"'Alawi #0" = 3
	"Twakali #0" = 3
	"Janfar #0" = 3
	"Muksedi #0" = 3
}

leader_names = {
	Wakil
	Yomba
	Salim
	Setia
	Jumbe
	Semid
	Kawawe
	Dato
	Sokoine
	Suja
}

ship_names = {
	Mlima moto moshi homu ukungu nyota
	jua maji mwezi mnyama samaki ndege
	mbwa nyoka ua damu
}
