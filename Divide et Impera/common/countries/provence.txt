#Country Name: Please see filename.

graphical_culture = latingfx

color = { 93  160  163 }

historical_ideas = { liberal_arts
	national_conscripts
	merchant_adventures
	humanist_tolerance
	patron_of_art
	colonial_ventures
	military_drill
	quest_for_the_new_world
	cabinet
	bureaucracy
	scientific_revolution
	improved_foraging
	napoleonic_warfare
}

historical_units = {
	#Infantry France
	w_i_armed_peasantry
	w_i_crossbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_chosen_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_french_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon 
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Charles #2" = 40
	"Louis #1" = 40
	"Ren� #1" = 20
	"Nicolas #0" = 10
	"Jean #0" = 5
	"Raymond Berengar #3" = 1
	"Alphonse #2" = 1
	"Robert #1" = 1
	"Adh�mar #0" = 3
	"Aymeric #0" = 3
	"Bartomiu #0" = 3
	"Benedit #0" = 3
	"Bernat #0" = 3
	"Bertran #0" = 3
	"Dami�n #0" = 3
	"Emeric #0" = 3
	"Enric #0" = 3
	"Est�ve #0" = 3
	"Ferr�n #0" = 3
	"Folquet #0" = 3
	"Frances #0" = 3
	"Gast� #0" = 3
	"Guilhem #0" = 3
	"Huc #0" = 3
	"Jacme #0" = 3
	"Jaufre #0" = 3
	"Jordan #0" = 3
	"Loys #0" = 3
	"Lucas #0" = 3
	"Mart� #0" = 3
	"Matheu #0" = 3
	"Miqueu #0" = 3
	"Per #0" = 3
	"Pol #0" = 3
	"Pons #0" = 3
	"Sanc #0" = 3
	"Thibaud #0" = 3
	"Vicentz #0" = 3
	"Victor #0" = 3
	"Xavier #0" = 3
}

leader_names = {
	"d'Abeille" "d'Adh�mar" "d'Agoult" "d'Aiglun" "d'Aigui�res" "d'Allemagne" "d'Amalric" "d'Audiffredi"
	"de Bagnols" "de Barberin" "de Barcillon" "de Baschi" "des Baux" "de Bermond" "de Bionneau" "de Brunet"
	"de Cadenet" "de Cascaris-Castellet" "de Castaing" "de Castellane" "de Castillon" Coquerel "de Cordes" "de Cormis" "de Crillon"
	"d'Entrecasteaux" "d'Espagnet" Esparron "d'Espinouze"
	"de Fargis" "Fauque de Jonqui�res" "Ferr� de la Grange" 
	"de Gassendi" "de G�vaudan" "de Gombert" "de Gordes" "de Grignan" Grimaldi "de Grimaud" "de Guast" "des Isnards" 
	"de Lauris" "de Libertat" "de Lom�nie"
	"de Monteil" "de Montpezat" 
	"de Paule" "de Pena" "Pene de la Borde" "de la P�russe" "de Pontev�s"
	"de Raimondis" "de Revest" "de Riquetti"
	"de Sabran" "de Sade" "de Saint Victoret" "de S�guiran" "de Simiane"
	"de Valbelle"
}

ship_names = {
        Achille Agr�able "Aigle Volant" Alcyon Alouette
        Altier Aquilon Ardente Assur� Avenant
        Aventurier Baleine B�casse Bienvenu Bizarre
        Bor�e "Branche d'Olivier" Brillant Bucentaure Cach�
        Caille Capable Centaure C�sar "Cheval Marin"
        Coche "Commerce de Marseille" "Commerce de Montpellier" "Comte de Provence" Conqu�rant
        Constant Content Croissant Dauphine Diamant
        Dieppoise Dur Dromadaire Eclatant Eole
        Esp�rance Fantasque Ferme Fid�le Fier
        Florissant Formidable Fort Foudroyant Gaillard
        Heureux H�ros Jason Lion Mignon
        Minerve Oc�an Orph�e Parfait Ph�nix
        Poli Pompeux Portefaix Protecteur Proven�al
        Provence Prudent Puissant R�ale Redoutable
        Requin "Royal Louis" Sage Sagittaire "Royal Th�r�se"
        "Saint Esprit" "Saint Joseph" "Saint Philippe" Sardine Sceptre
        S�duisant Seine S�rieux Solide Souverain
        Suffisant T�m�raire Terrible Tigre Tonnant
        Toulon Toulouse Trident Triomphant Triton
        Union Vaillant Vigilant Volontaire Z�l�
}

army_names = {
         "Arm�e de Marseille" "Arm�e des Baux"        "Arm�e de Provence-Forcalquier" 
         "Arm�e d'Orange"     "Arm�e du Pays d'Arles" "Arm�e des Alpes"
         "Arm�e du Rh�ne"     "Arm�e de la Durance"   "Arm�e de Haute Provence"
         "Arm�e de Basse Provence" "Arm�e du Vivarais"
	 "Arm�e de $PROVINCE$" 
}