#Country Name: Please seefilename.

graphical_culture = africangfx

color = { 215  155  155 }

historical_ideas = { bellum_iustum
	shrewd_commerce_practise
	national_conscripts
	merchant_adventures
	military_drill
	national_trade_policy
	battlefield_commisions
	engineer_corps
	grand_army
	espionage
	cabinet
	glorious_arms
	bureaucracy
}

historical_units = {
	african_spearmen
	mali_tribal_warfare
	african_hill_warfare
	niger_kongolese_gunpowder_warfare
	westernized_niger_kongolese
	african_western_franchise_warfare
}

monarch_names = {
	"N'Dyadya #0" = 10
	"Sare #0" = 10
	"N'Diklam #0" = 10
	"Tyukulia #0" = 10
	"Leeyti #0" = 10
	"N'Delen Mbey #0" = 10
	"Birayma Kuran #0" = 10
	"Bukaar #0" = 20
	"Birayma #0" = 20
	"Leele Fuli #0" = 20
	"al-Buri #0" = 20
	"Lat-Semba #0" = 3
	"Gireun Buri #0" = 3
	"Bakar #0" = 3
	"Birayma #0" = 3
	"Bakar #0" = 3
	"Bakan-Tam #0" = 3
	"Birawa #0" = 3
	"Lat-Kodu #0" = 3
	"Taanor #0" = 3
	"Bakan-Tam #0" = 3
	"Amadu #0" = 3
	"'Ali Buri #0" = 3
}

leader_names = {
	N'Dyaye
	Sare
	N'Diklam
	Tyukuli
	Leeyti
	Eter
	Daagulen
	"Kuran Kan"
	Fak
	Penda
	Dyelen
	Mba
	Gan
	Keme
	Buri-Nyabu
	Yaago
	Ma-Dyigen
}

ship_names = {
	Kiringa Linguere 
	Buur-ba Djolof "Sundiata Keita" "Kankan Musa"
	"Kouroukan Fouga" "Wali Keita" "Quati Keita"
	"Khalifa Keita" Sakura
}
