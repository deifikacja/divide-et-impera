# Country name see file name.

graphical_culture = indiangfx

color = { 245  139  127 }

historical_ideas = { early_mining
	national_conscripts
	glorious_arms
	military_drill
	battlefield_commisions
	shrewd_commerce_practise
	grand_army
	merchant_adventures
	espionage
	engineer_corps
	national_trade_policy
	smithian_economics
	cabinet
}

historical_units = {
	indian_footsoldier
	rajput_hill_fighters
	indian_arquebusier
	mughal_musketeer
	mughal_mansabdar
	indian_elephant
	indian_shock_cavalry
	rajput_musketeer
	reformed_mughal_musketeer
	reformed_mughal_mansabdar
	maharathan_guerilla_warfare
	maharathan_cavalry
	bhonsle_infantry
	bhonsle_cavalry
	indian_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Pilaji Rao #0" = 10
	"Damaji Rao #0" = 10
	"Govind Rao #0" = 10
	"Sayaji Rao #0" = 10
	"Fateh Singh Rao #0" = 10
	"Manaji Rao #0" = 10
	"Govind Rao #0" = 10
	"Anand Rao #0" = 10
}

leader_names = {
	Nisheeth
	Sukarman
	Yateen
	Sitipala
	Vyapari
	Vishwa
	Tjakur
	Ramdas
	Prajna
	Lakhani
}

ship_names = {
	Agni Bhim Brahma Devi Durga
	Ganesh Gayatri Hanumaun Indra
	Jalsena "Kala Jahazi" Kali Kubera
	Krishna Lakshmi "Lal Jahazi"
	Nausena "Nav ka Yudh" "Nila Jahazi"
	Parvati Radha Ratri Sagar Sarasvati
	Shiva Sita Soma Surya Varuna Vayu
	Vishnu Yama
}
