#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 24  14  13 } 

historical_ideas = { naval_transport
	national_conscripts
	military_drill
	shrewd_commerce_practise
	divine_supremacy
	excellent_shipwrights
	superior_seamanship
	merchant_adventures
	national_trade_policy
	engineer_corps
	grand_army
	sea_hawks
	naval_fighting_instruction
}

historical_units = {
	mamluk_archer
	mamluk_cavalry_charge
	mamluk_duel
	mamluk_musket_charge
	muslim_mass_infantry
	muslim_dragoon
	ali_bey_reformed_infantry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Muhammad #0" = 200
	"Ahmad #0" = 80
	"'Abdall�h #0" = 60
	"'Abd al-Malik #0" = 40
	"'Al� #0" = 40
	"Isma'�l #1" = 20
	"Yahy� #1" = 20
	"'al-Wal�d #0" = 20
	"al-Mustad� #0" = 20
	"al-Rash�d #0" = 20
	"Hish�m #0" = 20
	"Idr�sid #0" = 20
	"Sulaym�n #0" = 20
	"Zayd�n #0" = 20
	"Zayn #0" = 20
	"'Abd ar-Rahm�n #0" = 10
	"al-Yaz�d #0" = 10
	"Mansur #0" = 10
	"Muhraz #0" = 10
	"Nasir #0" = 10
	"Ab� Faris #0" = 5
	"Abbas #0" = 5
	"Ab� Merwan #0" = 5
	"al-Talib #0" = 5
	"Hammada #0" = 5
	"Hasam #0" = 5
	"Hashim #0" = 5
	"Ibrahim #0" = 5
	"Ja'far #0" = 5
	"Maimun #0" = 5
	"Maslama #0" = 5
	"Said #0" = 5
	"Umar #0" = 5
	"Y�suf #0" = 5
	"Ataullah #0" = 3
	"Habib #0" = 3
	"Jabril #0" = 3
	"Lutfi #0" = 3
	"Rasul #0" = 3
	"Zulfiqar #0" = 3
}

leader_names = {
	Sahiri
	Haida
	Sellami
	Arazi
	Berui
	Lahsini
	Skah
	Barek
	Alami
	Behar
}

ship_names = {
	Tawwab Muntaqim Afuww Fa'uf "Malik Mulk"
	"Jalal Ikram" Muqsit Jami Ghani Mughni
	Mani Darr Nafi Nur Hadi Badi Baqi Warith
	Rashid Sabur "Fazl Azim"
}
