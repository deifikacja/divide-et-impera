#Country Name: Please see filename.

graphical_culture = africangfx

color = { 110  23  222 }

historical_ideas = {
	shrewd_commerce_practise
	national_conscripts
	merchant_adventures
	military_drill
	national_trade_policy
	battlefield_commisions
	engineer_corps
	grand_army
	espionage
	cabinet
}

historical_units = {
mali_archer
songhai_archer
sahel_archer
sudan_archer
niger_gunpowder_warfare
westernized_adal
adal_guerilla_warfare
mali_nomads
songhai_nomads
sudan_nomads
nubian_nomads
fulani_nomads
}

monarch_names = {
	"Naaba #0" = 100
	"Yatenga-Naaba #0" = 90
	"Sidiyete #0" = 10
}

leader_names = {
	Niago
	Parima
	Kumpaugum
	Nabassere
	Tusuru
	Sini
	Piiyo
	Kango
	Wabgho
	Kango
	Seega
	Kaongo
	Tuguri
	Koom
	Korogo
	Ragongo
	Wobgo
	"Nyambe Moogo"
	Nyambe
	Moogo
	Totebaldbo
	Yemde
	Sanum
	Woboga
	Baongo
	Bulli
	Wedraogo
	Ligidi
	Kobga
	Tigre
	Sigiri
	Gigma
}

ship_names = {
	Sisili Mouhoun Bougouriba Naznon
	Nakanbe Koulpelogo Pendjan
}
