#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 12  12  52 } 

historical_ideas = { feudal_castles
        glorious_arms 
	grand_army
        high_technical_culture
	bureaucracy
	cabinet
	military_drill
	battlefield_commisions 
	shrewd_commerce_practise
	engineer_corps
	merchant_adventures
	national_trade_policy
	espionage
}

historical_units = {
	muslim_footsoldier
muslim_shamshir
mamluk_duel
muslim_infantry
muslim_sturm
muslim_mass_infantry
ali_bey_reformed_infantry
persian_rifle
musellem
shaybani
new_shaybani
abbasid
new_muslim_clan
ways
new_ways w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery

}

monarch_names = {
	"Musa #1" = 10
	"Arslan #0" = 10
	"Hafiz #1" = 10
	"Shams #3" = 30
	"Fakhr #1" = 10
	"Mu'izz #1" = 10
	"Ghiyath #2" = 20
	"Shah Rukh #0" = 10
	"Abu #0" = 10
	"Shah Mahmud #0" = 10
	"Ibrahim #0" = 10
	"Hussein #0" = 10
	"Badi #0" = 10
	"Muzzafar #0" = 10
	"Muzzafar Hussein #0" = 10
	"Abdalla #0" = 10
	"Asad #0" = 10
	"Allah #0" = 10
	"Timur Shah #0" = 10
	"Mahmud Shah #0" = 20
	"Kamran #0" = 10
	"Fath #0" = 10
	"Yar Muhammad #0" = 10
	"Seyyid Muhammad #0" = 10
	"Muhammad #0" = 10
	"Ahmed #0" = 10
	"Sher #0" = 10
	"Ilyas #1" = 10
}

leader_names = {
"ibn Asad" ud-Din Muhammad "Pir Hussain" "Pir 'Ali"
	Rukh "al-Qasim Babur" al-Quasim Babur Mahmud Baiqara
	"ibn Mansur" "Khan Abdali" Abdali "Allah Khan" Khan 
	"Yar Khan" "ibn Ahmad" "ibn Ahmad Shah" "ibn Mahmud"
	"ibn Yar Muhammad" "Yar Muhammad" Jung "Khan Alkozai"
	Alkozai Yusuf Yaqub "'Ali" "Ayyub Khan" Ayyub Shah
}

ship_names = {
	Hotaki
	Taraki
	Amin
	Karmal
	Najibullah
	Akhund
	Rahman
	Ghazi
	Ahmad
	Qadir
}
