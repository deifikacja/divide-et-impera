#Country Name: Please seefilename.

graphical_culture = africangfx

color = { 255  200  200 }

historical_ideas = { bellum_iustum
	national_conscripts
	shrewd_commerce_practise
	divine_supremacy
	national_trade_policy
	military_drill
	engineer_corps
	battlefield_commisions
	merchant_adventures
	grand_army
	espionage
}

historical_units = {
	african_spearmen
	bantu_tribal_warfare
	bantu_plains_warfare
	bantu_gunpowder_warfare
	westernized_bantu
	african_western_franchise_warfare
}


monarch_names = {
	"Radama #0" = 20
	"Andriamasinavalona #0" = 10
	"Andriantsimitoviaminiandriandrazaka #0" = 10
	"Andriambelomasina #0" = 10
	"Andrianjafy #0" = 10
	"Andrianjafinandriamanitra #0" = 10
	"Andrianampoinmerina #0" = 10
	"Ramboasalama #0" = 10
	"Lehidama #0" = 10
	"Ranavalona #0" = 10
	"Rabodoandrianampoinimerina #0" = 10
	"Rakotosehenondrama #0" = 10
	"Rasoherina #0" = 10
	"Rabodozanakandriana #0" = 10
	"Ramoma #0" = 10
	"Razafindrahety #0" = 10
}

leader_names = {
}

ship_names = {
	Fiantaranasoa Antananriva Tananariva Merina Imerina Imernia Toliary Mahajanga Antsiranana
}
