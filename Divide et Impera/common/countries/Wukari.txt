#Country Name: Please see filename.

graphical_culture = africangfx

color = { 11  173  249 }

historical_ideas = { bellum_iustum
	shrewd_commerce_practise
	national_conscripts
	merchant_adventures
	military_drill
	national_trade_policy
	battlefield_commisions
	engineer_corps
	grand_army
	espionage
	cabinet
}

historical_units = {
	african_spearmen
	niger_kongolese_tribal_warfare
	niger_kongolese_forest_warfare
	niger_kongolese_gunpowder_warfare
	westernized_niger_kongolese
	niger_kongolese_guerilla_warfare
}

monarch_names = {
	"Agbu #0" = 10
	"Katakpa #0" = 10
	"Agwabi #0" = 10
	"Dawi #0" = 20
	"Agyibi #0" = 10
	"Nani #0" = 10
	"Daju #0" = 10
	"Zike #0" = 10
	"Kuvyon #0" = 20
	"Adi #0" = 10
	"Ashu Manu #0" = 40
	"Angyu #0" = 10
	"Agbu Manu #0" = 40
	"Adi #0" = 10
	"Shekarau #0" = 10
	"Masa #0" = 10
	"Ibi #0" = 10
}

leader_names = {
	To
	Manu
	Byewi
	Matswen
	Zikenyu
	Kenjo
}

ship_names = {
	Xevioso Sogbo "Nana Buluku" "Mawu-Lisa"
	Dan Gbadu Da Gu Ayaba Loko Sakpata Gleti
	Zinsu Zinsi
}
