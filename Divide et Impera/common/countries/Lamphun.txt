#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 4 154 255 }

historical_ideas = { early_mining
	national_conscripts
	glorious_arms
	military_drill 
	battlefield_commisions
	colonial_ventures
	grand_army
	espionage
	merchant_adventures
	engineer_corps
	national_trade_policy
	cabinet
	bureaucracy
}

historical_units = {
	east_asian_spearmen
	eastern_bow
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	asian_musketeer
	reformed_asian_musketeer
	reformed_asian_cavalry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Kaeo #1" = 10
	"Setthi Kham Fan #1" = 10
	"Khanan Kawila #1" = 10
	"Boon Ma #1" = 10
	"Kham Tan #0" = 10
	"Neejchajlanka #0" = 10
	"Chakrakam Kajornsakdi #0" = 10
}

leader_names = {
	Bunnag
	Chao
	Chalor
	Chuan
	Luang
	Lui
	Nut
	Prem
	Sanya
	Sarit
}

ship_names = {
	"Chiang Rai"
	"Ban Muang"
	Haripunchai
	Loethai 
	Mengrai
	Nguanamthom
	"Pu Saisongkhram"
	Ramkhamhaeng
	"Si Indrathit" "Sai Leu Thai" "Sai Luthai"
	Thammarach
}
