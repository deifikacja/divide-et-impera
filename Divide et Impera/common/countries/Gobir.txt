#Country Name: Please seefilename.

graphical_culture = africangfx

color = { 48  169  255 }

historical_ideas = { bellum_iustum
	shrewd_commerce_practise
	national_conscripts
	merchant_adventures
	military_drill
	national_trade_policy
	battlefield_commisions
	engineer_corps
	grand_army
	espionage
	cabinet
}

historical_units = {
mali_archer
songhai_archer
sahel_archer
sudan_archer
niger_gunpowder_warfare
westernized_adal
adal_guerilla_warfare
mali_nomads
songhai_nomads
sudan_nomads
nubian_nomads
fulani_nomads
}

monarch_names = {
	"Yakubu #0" = 20
	"Chiroma #0" = 60
	"'Abdullahi #1" = 20
	"Da'uda Abasama #0" = 40
	"Abu Bakr Kado #0" = 20
	"Yakufu #0" = 20
	"Kutambi #0" = 20
	"Alhaji #0" = 20
	"Shekkarau #1" = 20
	"Soyaki #0" = 20
	"Bawo #0" = 20
	"Babba Zaki #0" = 20
	"Dalla Gungum #0" = 3
	"Dalla Dawaki #0" = 3
	"Chiroma #0" = 3
	"Akal #0" = 3
	"Bachiri #0" = 3
	"Gumsara #0" = 3
	"Bardandoma #0" = 3
	"Soba #0" = 3
	"Nyakum #0" = 3
	"Yakub #0" = 3
	"Salihu #0" = 3	
	"Janhazo #0" = 3
	"Muhammad #0" = 30
	"Abd al-Karim #0" = 3
	"Tsagarana #0" = 3
	"Agwaragwi #0" = 3
	"Bawa #0" = 3
	"Magajin #0" = 3
	"Tomo #0" = 3
	"Sumail #0" = 3
	"Suleyman #0" = 3
	"Fati #0" = 3
	"Ibrahim #0" = 3
	"'Aliyu #0" = 3
	"Bako #0" = 3
	"Burema #0" = 3
	"Dudufani #0" = 3
	"Burum #0" = 3	
}

leader_names = {
	Saibou
	Bakara
	Mamami
	Kountch�
	Diore
	Balewa
	Aluko
	Bandele
	Kanu
	Kuti
}

ship_names = {
	iska sama hazo gajimare "babban dutse"
	teku rairayi rana wata tauraro ruwa
	itace saiwa fure
}
