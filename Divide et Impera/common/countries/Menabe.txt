#Country Name: Please seefilename.

graphical_culture = africangfx

color = { 200  20  202 }

historical_ideas = {
	national_conscripts
	shrewd_commerce_practise
	divine_supremacy
	national_trade_policy
	military_drill
	engineer_corps
	battlefield_commisions
	merchant_adventures
	grand_army
	espionage
}

historical_units = {
	african_spearmen
	bantu_tribal_warfare
	bantu_plains_warfare
	bantu_gunpowder_warfare
	westernized_bantu
	african_western_franchise_warfare
}


monarch_names = {
	"Andriamanetriarivo #1" = 10
	"Trimanongarivo #0" = 10
	"Andrianamboatsimarofy #0" = 10
	"Ratrimolahi #0" = 10
	"Andriantsoarivo #0" = 10
	"Miakala #0" = 10
	"Ramitraho #0" = 10
	"Rainasa #0" = 10
	"Kelisambay #0" = 10
}

leader_names = {
}

ship_names = {
	Fiantaranasoa Antananriva Tananariva Merina Imerina Imernia Toliary Mahajanga Antsiranana
}
