#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 212  211  151 }

historical_ideas = { liberal_arts
	national_conscripts
	glorious_arms
	military_drill
	battlefield_commisions
	shrewd_commerce_practise
	grand_army
	merchant_adventures
	national_trade_policy
	engineer_corps
	cabinet
	espionage
	patron_of_art
}

historical_units = {
	east_asian_spearmen
	eastern_bow
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	asian_musketeer
	reformed_asian_musketeer
	reformed_asian_cavalry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Radzathu #0" = 20
	"Thinggathu #0" = 20
	"Mengtsaumwun #0" = 20
	"'Al� Kh�n #0" = 20
	"Kalima #0" = 20
	"Dawliya #0" = 20
	"Basawnyo #0" = 20
	"Yanaung #0" = 20
	"Salingathu #0" = 20
	"Minyaza #0" = 20
	"Kasabadi #0" = 20
	"Minsaw O #0" = 20
	"Thatasa #0" = 20
	"Minbin #0" = 20
	"Dikha #0" = 20
	"Sawla #0" = 20
	"Minsetya #0" = 20
	"Minpalaung #0" = 20
	"Salim #0" = 20
	"Husayn #0" = 20
	"Narapatikyi #0" = 20
	"Thado #0" = 20
	"Sandathudamma #0" = 20
	"Thirithuriya #0" = 20
	"Waradhammaraza #0" = 20
	"Munithudhammaraza #0" = 20
	"Sandathuriyadhamma #0" = 20
	"Nawrahtazaw #0" = 20
	"Kalamandat #0" = 20
	"Sandawimala #0" = 20
	"Sandathuriya #0" = 20
	"Sandawizaya #0" = 20
	"Minsani #0" = 20
	"Naradipati #0" = 20
	"Narapawara #0" = 20
	"Katya #0" = 20
	"Madarit #0" = 20
	"Thirithu #0" = 20
	"Apaya #0" = 20
	"Sandathumana #0" = 20
	"Sandaditha #0" = 20
	"Ranoung #0" = 20
	"Tsalenggathu #0" = 20
	"Dik-Kha #0" = 20
	"Thirithudhamma #0" = 20
	"Radzathugyi #0" = 20
}

leader_names = {
	Aung-Hla Aung-San 
	Hla-Thein 
	Khin-Nyunt Khun-Sa Kwan-Kywa 
	Maung-Gyi Maung-Win Min-Tun 
	Ohn-Myint 
	Sein-Win 
	Than-Htay Thet-Naung 
	U-Nu 
}

ship_names = {
	Alaumaphru "Athinkhara Raza" 
	Candasuriya
	Datharaza
	Foonthan
	Gandakudi
	Koliya
	Lakkyamunnan
	Mahamuni "Mong Saw Mon" "Mrauk U"
	"Mong Khaki" "Mong Ba" "Mong Razagree"
	"Mahataing Candra"
	Ngahnaloon
	Suriya "Suriya Candra"
	Thiharaza Thakyawangree Thakyawannge
	Ukkabala
	Waradhamma
}
