#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 100  100  25 }

historical_ideas = {
	national_conscripts
	glorious_arms
	grand_army
	military_drill
	battlefield_commisions
	shrewd_commerce_practise
	engineer_corps
	merchant_adventures
	national_trade_policy
	espionage
	humanist_tolerance
	cabinet
}

historical_units = {
	east_asian_spearmen
	eastern_bow
	east_mongolian_steppe
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	asian_musketeer
	reformed_asian_musketeer
	reformed_asian_cavalry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Pan Tegin #1" = 10
	"Boko Tegin #1" = 10
	"Alp Arslan #1" = 10
	"Irdimin Qagan #1" = 10
	"Arslan Qagan #1" = 10
	"Bilga Tegin #1" = 10
	"Isen Tomur #1" = 10
	"Baurchuq Art Tegin #1" = 10
	"Qusmayin #1" = 10
	"Salun Tegin #1" = 10
	"Oghrunzh Tegin #1" = 10
	"Mamuraq #1" = 10
	"Qozhighar Tegin #1" = 10
	"Nolen Tegin #1" = 10
	"Tomur Buga #1" = 10
	"Sunggi Tegin #1" = 10
	"Taypan #1" = 10
	"Dost Muhammad #0" = 10
	"Kebek Sultan #0" = 10
	"Yunus #0" = 10
	"Ahmad Alaq #0" = 10
	"Mansur Khan #0" = 10
	"Shah Khan #0" = 10
	"Kuraysh #0" = 10
	"Muhammad #0" = 10
	"Hudabende #0" = 10
	"Abd ar-Rahim #0" = 10
	"Muhammad Khashim #0" = 10
	"Abdallah #0" = 10
	"Abu'l Muhammad #0" = 10
	"Sultan Sa'id Baba #0" = 10
	"Abd ar-Rashid #0" = 10
	"Amin Haliq #0" = 10
	"Sulaiman #0" = 10
	"Iskandar #0" = 10
	"Ilus #0" = 10
	"Muhammad Sa'id #0" = 10
	"Aq-Layidu #0" = 10
	"Mahmud Yamin #0" = 10
}

leader_names = {
	Jasray
	Gonbat
	Hattori
	Olzvoi
	Bira
	Batmunkh
	Ganbold
	Saihan
	Ochirbal
	Davaa
}

ship_names = {
	Bargut Buzav Kerait Naiman "D�rvn ��rd"
	Khoshut Olo Dzungar Torgut Dorbot
	Amdo "Altan Khan" Khoshot Ol�ts G�shi
}
