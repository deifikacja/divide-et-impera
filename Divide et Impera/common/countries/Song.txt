#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 179  128  14 }

historical_ideas = { liberal_arts
	national_conscripts
	espionage
	military_drill
	national_trade_policy
	patron_of_art
	grand_army
	bureaucracy
	national_bank
	cabinet
	humanist_tolerance
	glorious_arms
}

historical_units = {
	chinese_longspear
	eastern_bow
	chinese_footsoldier
	chinese_steppe
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	manchu_banner
	han_banner
	asian_musketeer
	chinese_dragoon
	reformed_asian_musketeer
	reformed_asian_cavalry
	reformed_manchu_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
"Wen" = 10
"Wu" = 10
"Cheng" = 10
"Kang" = 10
"Yuan" = 10
"Guang" = 10
"Ming" = 10
"Xuan" = 10
"De" = 10
"Ren" = 10
"Xiao" = 10
"Huan" = 10
"Wei" = 10
"Lie" = 10
"Ping" = 10
"Ding" = 10
"An" = 10
"Jing" = 10
"Shun" = 10
"He" = 10
"Mu" = 10
"Zhao" = 10
"Hui" = 10
"Jing" = 10
"Ai" = 10
"Si" = 10
"Min" = 10
"Zhang" = 10
"Xian" = 10
"Da" = 10
"Tai" = 10
"Dao" = 10
"Ying" = 10
"Yi" = 10
"Rui" = 10
"Chun" = 10
"Yi" = 10
"Zhuang" = 10
"Su" = 10
"Xiaan" = 10
"Xiang" = 10
"Gong" = 10
"Yang" = 10
"Li" = 10
"You" = 10
"Miu" = 10
"Wencheng" = 10
"Wengong" = 10
"Wende" = 10
"Wuyi" = 10
"Wusu" = 10
"Wushun" = 10
"Chengwu" = 10
"Chenglie" = 10
"Chengzhao" = 10
"Kangming" = 10
"Kangding" = 10
"Kangxian" = 10
"Yuanzhao" = 10
"Yuanxiang" = 10
"Yuanshun" = 10
"Guangwu" = 10
"Guangwen" = 10
"Guangren" = 10
"Mingde" = 10
"Mingyi" = 10
"Minghui" = 10
"Xuanwu" = 10
"Xuanmu" = 10
"Xuanding" = 10
"Dewei" = 10
"De'an" = 10
"Degong" = 10
"Renzhang" = 10
"Renying" = 10
"Renxian" = 10
"Xiaoping" = 10
"Xiaochun" = 10
"Xiaoding" = 10
"Huanwu" = 10
"Huanlie" = 10
"Huanzhao" = 10
"Weilie" = 10
"Weicheng" = 10
"Weisi" = 10
"Liejing" = 10
"Lie'an" = 10
"Lie'ai" = 10
"Ping'an" = 10
"Pingjing" = 10
"Ping'ai" = 10
"Dinghe" = 10
"Dingyi" = 10
"Dingshun" = 10
"Ansi" = 10
"Anmin" = 10
"Anren" = 10
"Jingxian" = 10
"Jinggong" = 10
"Jingxiao" = 10
"Shunde" = 10
"Shunmu" = 10
"Shunyi" = 10
"Heling" = 10
"Hexiao" = 10
"Herui" = 10
"Muchun" = 10
"Muxian" = 10
"Mujing" = 10
"Zhaoming" = 10
"Zhaoxian" = 10
"Zhaowu" = 10
"Hui'an" = 10
"Huizhuang" = 10
"Huijing" = 10
"Jingding" = 10
"Jingping" = 10
"Jingsu" = 10
"Aiping" = 10
"Aiying" = 10
"Aijing" = 10
"Sirui" = 10
"Siren" = 10
"Side" = 10
"Mincheng" = 10
"Minhuai" = 10
"Minxian" = 10
"Daren" = 10
"Dayi" = 10
"Dade" = 10
"Taiwu" = 10
"Tai'an" = 10
"Taiding" = 10
"Daowu" = 10
"Daoying" = 10
"Daohui" = 10
"Yingwen" = 10
"Yingyi" = 10
"Yingren" = 10
"Yizhao" = 10
"Yixian" = 10
"Yicheng" = 10
"Ruicheng" = 10
"Ruiwu" = 10
"Rui'an" = 10
"Chunshun" = 10
"Chunkang" = 10
"Chunmu" = 10
"Yiyi" = 10
"Yiming" = 10
"Yiming" = 10
"Zhuangsu" = 10
"Zhuangxian" = 10
"Zhuanggong" = 10
"Sujing" = 10
"Suwu" = 10
"Suxian" = 10
"Xiaanzhao" = 10
"Xiaandao" = 10
"Xiaande" = 10
"Xiang'an" = 10
"Xianghuan" = 10
"Xiangping" = 10
"Xianmu" = 10
"Xiande" = 10
"Xianwu" = 10
"Gongmin" = 10
"Gong'ai" = 10
"Gongshun" = 10
"Da'an" = -10
"Xiaoci" = -10
"Zhang'an" =-10
}

leader_names = {
Zhao
Qian
Sun
Li
Zhou
Wu
Zheng
Wang
Feng
Chen
Chu
Wei
Jiang
Shen
Han
Yang
Zhu
Qin
You
He
Lu
Shi
Zhang
Kong
Cao
Yan
Hua
Jin
Wei
Tao
Jiang
Qi
Xie
Zou
Yu
Bai
Shui
Dou
Zhang
Yun
Su
Pan
Ge
Xi
Fan
Peng
Lang
Lu
Wei
Chang
Ma
Miao
Feng
Hua
Fang
Yu
Ren
Yuan
Liu
Feng
Bao
Shi
Tang
Fei
Lian
Cen
Xue
Lei
He
Ni
Tang
Teng
Yin
Luo
Bi
Hao
Wu
An
Chang
Yue
Yu
Shi
Fu
Pi
Bian
Qi
Kang
Wu
Yu
Yuan
Bo
Qu
Meng
Ping
Huang
He
Mu
Xiao
Yin
Yao
Shao
Zhan
Wang
Qi
Mao
Yu
Di
Mi
Bei
Ming
Zang
Ji
Fu
Cheng
Dai
Tan
Song
Mao
Pang
Xiong
Ji
Shu
Qu
Xiang
Zhu
Dong
Liang
Du
Ruan
Lan
Min
Xi
Ji
Ma
Qiang
Jia
Lu
Lou
Wei
Jiang
Tong
Yan
Guo
Mei
Sheng
Lin
Diao
Zhong
Xu
Qiu
Luo
Gao
Xia
Cai
Tian
Fan
Hu
Ling
Huo
Yu
Wan
Zhi
Ke
Zan
Guan
Lu
Mo
Jing
Fang
Qiu
Miao
Gan
Xie
Ying
Zong
Ding
Xuan
Ben
Deng
Yu
Shan
Hang
Hong
Bao
Zhu
Zuo
Shi
Cui
Ji
Niu
Gong
Cheng
Ji
Xing
Hua
Pei
Lu
Rong
Weng
Xun
Yang
Yu
Hui
Zhen
Qu
Jia
Feng
Rui
Yi
Chu
Jin
Ji
Bing
Mi
Song
Jing
Duan
Fu
Wu
Wu
Jiao
Ba
Gong
Mu
Kui
Shan
Qu
Che
Hou
Mi
Peng
Quan
Xi
Ban
Yang
Qiu
Zhong
Yi
Gong
Ning
Qiu
Fu
Liu
Jing
Zhan
Shu
Long
Ye
Xing
Si
Shao
Gao
Li
Ji
Bo
Yin
Su
Bai
Huai
Pu
Tai
Cong
E
Suo
Xian
Ji
Lai
Zhuo
Lin
Tu
Meng
Chi
Qiao
Yin
Yu
Xu
Neng
Cang
Shuang
Wen
Shen
Dang
Zhai
Tan
Gong
Lao
Pang
Ji
Shen
Fu
Du
Ran
Zai
Li
Yong
Xi
Qu
Sang
Qui
Pu
Niu
Shou
Tong
Bian
Hu
Yan
Ji
Jia
Pu
Shang
Nong
Wen
Bie
Zhuang
Yan
Chai
Qu
Yan
Chong
Mu
Lian
Ru
Xi
Huan
Ai
Yu
Rong
Xiang
Qu
Yi
Shen
Ge
Liao
Yu
Zhong
Ji
Ju
Heng
Bu
Du
Geng
Man
Hong
Kuang
Guo
Wen
Kou
Guang
Lu
Que
Dong
Ou
Shu
Wo
Li
Wei
Yue
Kui
Long
Shi
Gong
She
Niue
Chao
Gou
Ao
Rong
Leng
Zi
Xin
Kan
Na
Jian
Rao
Kong
Zeng
Mu
Sha
Yang
Ju
Xu
Feng
Chao
Guan
Kuai
Xiang
Zha
Hou
Jing
Hong
You
Gai
Yi
Huan
Gong
Moqi
Sima
Shangguan
Ouyang
Xiahou
Zhuge
Wenren
Dongfang
Helian
Huangfu
Yuchi
Gongyang
Tantai
Gongye
Zongzheng
Puyang
Chunyu
Chanyu
Taishu
Shentu
Gongsun
Zhongsun
Xuanyuan
Linghu
Zhongli
Yuwen
Zhangsun
Murong
Xianyu
Luqiu
Situ
Sikong
Qiguan
Sikou
Zhangdu
Ziju
Zhuansun
Duanmu
Wuma
Gongxi
Qidiao
Yuezheng
Rangsi
Gongliang
Tuoba
Jiagu
Zaifu
Guliang
Jinchu
Yanfa
Ruyan
Tuqin
Duangan
Baili
Dongguo
Nanmen
Huyan
Guihai
Yangshe
Weisheng
Yueshuai
Gokang
Kuanghou
Youqin
Liangqiu
Zuoqiu
Dongmen
Ximen
Shangmou
Shenai
Boshang
Moha
Qiaoda
Nian'ai
Yangtong
Diwu
Yanfu
}

ship_names = {
Ningbian Ningguo Ningwei Ningguang Ningyuan Ninghe
Ningfang Ningwu Ning'an Ningbo Ninglang Ninghao
Ninghong Ningkang Ningye Ningjing Ninghui Ningding
Ningfeng Ningtian Jingjing Langjing Yuanjing Zhongjing
Yijing Weijing Yingjing Fengjing Hongjing Yongjing
Xijing Longjing Laijing Huaijing Jijing Zhijing
Fujing Guangjing Liangjing Qingjing Yujing Hai'an
Haining Haiwei Haiyu Haihang Haiyang Haijing
Haiji Haming Haiqing Haili Haixun Haiguang
Haiwu Haiwen Haiye Haiyun Haibang Haihui
Hailing Zhiyuan Zhenyuan Jingyuan Laiyuan Pingyuan
Dingyuan Jiyuan Weiyuan Ningyuan Haiyuan Huiyuan
Jiayuan Huaiyuan Xiuyuan Chiyuan Mingyuan Qingyuan
Yuyuan Longyuan Qiyuan Jieyuan Yiyuan Chengyuan
}

army_names = {
"Imperial Guardian" "$PROVINCE$ Wei"
}

fleet_names = {
"Baochuan" "$PROVINCE$ Hai'ao"
}