#Country Name: Please see filename.

graphical_culture = southamericagfx

color = { 148  111  61} 

historical_ideas = { feudal_castles
	deus_vult
	divine_supremacy
	patron_of_art
	national_conscripts
	military_drill
	glorious_arms
	battlefield_commisions
	superior_seamanship
	merchant_adventures
	excellent_shipwrights
}

historical_units = {
	south_american_spearmen
	aztec_tribal_warfare
	aztec_hill_warfare
	aztec_gunpowder_warfare
	westernized_aztec
	mexican_guerilla_warfare
}

monarch_names = {
		"Moctezuma #0" = 3
                "Auitzotl #0" = 20
                "Axayacatl #0" = 20
                "Cuauhtemoc #0" = 20
                "Cuitlahuac #0" = 3
		"Tizoc #0" = 20
		"Huanitzin #0" = 10
		"Motelchiuh #0" = 10
                "Tehuetstiquitzin #0" = 10
		"Tlacotzin #0" = 10
                "Xochiquentzin #0" = 10
		"Acamapichtli #1" = 5
		"Chimalpopoca #1" = 5
		"Huitzilihuitl #1" = 5
		"Itzcoatl #1" = 5
		"Tenoch #1" = 5
		"Acacitli #0" = 20
                "Ahcambal #0" = 3
		"Axacaya #0" = 3
		"Cacamatzin #0" = 20
		"Copil #0" = 3
		"Eyahue #0" = 3
                "Huicton #0" = 3
		"Ilhuicamina #0" = 3
		"Itzquautzin #0" = 3
		"Mazatl #0" = 3
		"Nopaltzin #0" = 3
		"Olintecke #0" = 3
		"Opochtli #0" = 3
                "Panitzin #0" = 3
		"Quetzalmantzin #0" = 3
		"Quimichetl #0" = 3
		"Texcoyo #0" = 3
		"Timas #0" = 3
		"Tlacaelel #0" = 20
		"Tlotzin #0" = 3
		"Xiuhcozcatl #0" = 3
                "Xocoyol #0" = 3
                "Yaztachimal #0" = 3
                "Zolton #0" = 3
}

leader_names = {
        Acatlotzin Ahu�tzotl Axayac�tl Axoquetzin Cacamatzin Chalchiutlatonac Cuauhtemoc Cuitlahuac Cuitlalpitoc
	Ecatzin Eyahue Ezhuahuacatl Huemac Huestzin Huitzilihuitl Itzcoatl Ixtlilxochitl Izquemitl
	Mazatl Maxixcatzin Moctezuma Motelchiuh Nauyotl Nezahualpilli Nopaltzin
	Ocelopan Olintecke Opochtli Oquitzin Panitzin Pimotl Popopoyotl
	Quahcoatl Qualpopoca Quilaztli Quimichetl Temilotecatl Tezozomoc Tizoc Totoquihuatzin Tzompantli
	Xiconocatzin Xochipanitzin Xolotl Xomimitl Yaomauhitzin Yaztachimal Zincicha Zoanacochtzin Zolton
}

ship_names = {
	Acolmiztli Acolnahuacat "Ah Hoya" "Ah Tzenul" Amimitl Apanecatl Atl Atlau Axoloa
	Camaxtli Centeot Centzonuiznaua Chalchiuhtlatonal Chalchiuhtlicue Chalchiutotolin Chalmecalt Chalmecaucihuilt
	Chantico Chichomexochtli Chicomecoatl Chiconahuai Chihalma Chualchihuitlcue Cihuacaotyl Cinteotl
	Citlalatonac Citlalicue Ciucoatl Coatlicue Cochimetl Coyolxauhqui Cuauhcoatl 
	Ehehcatl Eueucoyotl
	"Hopop Can" Huehueteotl Huitzilopochtli Huixtocihuatl
	Itzcoliuhqui Itzli Itzpapalotl Itzpzpalotl Ixtlilton Iztamixcoatzin
	Macuilxochitl Mahayuel Malinalxochi Matalcueye Metztli Mictlan Mictlancihuntl Mictlantechtle
	Mictlantechupi Mictlantecihuatl Mixcoatl
	Nanauatzin
	Ococaltzin Omecihuatl Ometecuhlti Ometeotl Omeyocan
	Patecatli Paynal
	Quetzalcoatl
	Tecciztecatl Teoyaomqui Tepeyollotl Teteoinnan Tezacoatl Tezcatlipoca Tillan-Tlapallan Tlaloc
	Tlalocan Tlaltechhtlii Tlazolteotl Tonacatecuhtli Tonatiuhican Tonatiuth Tzitmime
	Xilonen "Xipe Totec" Xiucoatl Xiuhtecuhtli Xochipilli Xochiquetzal Xolotl Xonecuilli
	Yacatechutli
}
