#Country Name: Please see filename.

graphical_culture = africangfx

color = { 255 3 3 }

historical_ideas = { naval_transport
	merchant_adventures
	excellent_shipwrights
	shrewd_commerce_practise
	superior_seamanship
	national_conscripts
	national_trade_policy
	sea_hawks
	naval_fighting_instruction
	military_drill
	naval_glory
	battlefield_commisions
	engineer_corps
}

historical_units = {
	polynesian_daggermen
	polynesian_spearmen
	polynesian_clubmen
	polynesian_javelinmen
	polynesian_long_clubmen
	polynesian_shark_tooth
	polynesian_two_blade
	polynesian_throwing_axe
	polynesian_lua
	polynesian_slingermen
	polynesian_rifles
}

monarch_names = {                         
	"Te ari`i noho ra`i Tapoa #1" = 10
	"Tapoa #2" = 10
	"Te ari`i maeva rua #0" = -20
	"Temauiari`i a Mai #0" = 10
}

leader_names = {
		Te-Ma-puteoa Eutokia Toaputeitou Te-Ika-tohara Teaoiti
}

ship_names = {
		Porapora Borabora Vavau Ari'i
}

fleet_names = {
	"King's Canoes"
}