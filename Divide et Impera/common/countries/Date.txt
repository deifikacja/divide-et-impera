#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 155  150  100 }

historical_ideas = { bellum_iustum
	national_conscripts
	excellent_shipwrights
	merchant_adventures
	superior_seamanship
	espionage
	military_drill
	glorious_arms
	national_trade_policy
	patron_of_art
	shrewd_commerce_practise
	grand_army
	cabinet
}

historical_units = {
	japanese_archer
	eastern_bow
	japanese_footsoldier
	japanese_samurai
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	asian_musketeer
	reformed_asian_musketeer
	reformed_asian_cavalry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Momozono" = 40
	"Tsuchimikado" = 20
	"Kashiwabara" = 20
	"Nara" = 20
	"Oogimachi" = 20
	"Yozei" = 20
	"Misunoo" = 20
	"Meisho" = -10
	"Komyo" = 20
	"Saiin" = 20
	"Reigen" = 20
	"Higashiyama" = 20
	"Nakamikado" = 20
	"Sakuramachi" = 20
	"Kokaku" = 20
	"Ninko" = 20
	"Yoshito" = 10
	"Yukihito" = 10
	"Tadahito" = 10
	"Orihito" = 10
	"Yukikatsu" = 10
	"Nagahito" = 10
	"Naohito" = 10
	"Sadatsune" = 10
	"Kunitaka" = 10
	"Sadaatsu" = 10
	"Kunisuke" = 10
	"Sadayasu" = 10
	"Kuninobu" = 10
	"Sadakiyo" = 10
	"Kuninari" = 10
	"Kunimichi" = 10
	"Sadayuki" = 10
	"Kuninaga" = 10
	"Sadatake" = 10
	"Kunitada" = 10
	"Sadamochi" = 10
	"Kuniyori" = 10
	"Sadamochi" = 5
	"Kiyohiko" = 5
	"Masahito" = 5
	"Yoshinobu" = 5
	"Tenshin" = 5
	"Kotobuki" = 5
	"Nobu" = 5
	"Hanazono" = 1
	"Shoko" = 1
	"Komatsu" = 1
	"Kameyama" = 1
	"Chokei" = 1
	"Murakami" = 1
	"En'yu" = 1
	"Kogon" = 1
	"Suko" = 1
	"Daigo" = 1
	"Nijo" = 1
	"Fushimi" = 1
	"Uda" = 1
	"Fukakusa" = 1
	"Saga" = 1
	"Shijo" = 1
	"Horikawa" = 1
	"Chukyo" = 1
	"Juntoku" = 1
	"Toba" = 1
	"Antoku" = 1
	"Takakura" = 1
	"Rokujo" = 1
	"Shirakawa" = 1
	"Konoe" = 1
	"Sutoku" = 1
	"Sanjo" = 1
	"Reizei" = 1
	"Suzaku" = 1
	"Ichijo" = 1
	"Kanzen" = 1
	"Koko" = 1
	"Seiwa" = 1
	"Montoku" = 1
	"Nimmyo" = 1
	"Junna" = 1
	"Heizei" = 1
	"Kammu" = 1
	"Konin" = 1
	"Shotoku" = -1
	"Junnin" = 1
	"Shomu" = 1
	"Mommu" = 1
	"Jito" = -1
	"Temmu" = 1
	"Kobun" = 1
	"Tenji" = 1
	"Kotuko" = 1
	"Jomei" = 1
	"Suiko" = -1
	"Sushun" = 1
	"Yomei" = 1
	"Bidatsu" = 1
	"Kimmei" = 1
	"Senka" = 1
	"Ankan" = 1
	"Keitai" = 1
	"Buretsu" = 1
	"Ninken" = 1
	"Kenzo" = 1
	"Seinei" = 1
	"Yuryaku" = 1
	"Anko" = 1
	"Ingyo" = 1
	"Hanzai" = 1
	"Richu" = 1
	"Nintoku" = 1
	"Ojin" = 1
	"Jingu" = -1
	"Chuai" = 1
	"Seimu" = 1
	"Keiko" = 1
	"Suinin" = 1
	"Sujin" = 1
	"Kaika" = 1
	"Kogen" = 1
	"Korei" = 1
	"Koan" = 1
	"Kosho" = 1
	"Itoku" = 1
	"Annei" = 1
	"Suizei" = 1
	"Jimmu" = 1
	"Akio" = 3
	"Akira" = 3
	"Daisuke" = 3
	"Hanzo" = 3
	"Hideaki" = 3
	"Katsuo" = 3
	"Masuyo" = 3
	"Musashi" = 3
	"Ryo" = 3
	"Ryoukan" = 3
	"Sojun" = 3
	"Sho" = 3
	"Susumu" = 3
	"Takashi" = 3
	"Tenkai" = 3
	"Yoshio" = 3
	"Yuki" = 3
}

leader_names = {
	Date
}

ship_names = {
	"Date Maru"
}