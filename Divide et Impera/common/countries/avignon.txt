#Country Name: Please see filename.

graphical_culture = latingfx

color = { 105  106  182 }

historical_ideas = { 
	feudal_castles
	national_conscripts
	merchant_adventures
	humanist_tolerance
	patron_of_art
	colonial_ventures
	military_drill
	quest_for_the_new_world
	cabinet
	bureaucracy
	scientific_revolution
	liberty_egalite_fraternity
	revolution_and_counter
}

historical_units = {
	#Infantry France
	w_i_armed_peasantry
	w_i_municipal_militia
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_chosen_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_french_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon 
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Benedictus #12" = 60
	"Clemens #7" = 20
	"Iohannes #22" = 10
	"Gregorius #11" = 10
	"Innocentius #6" = 5
	"Urbanus #5" = 5
	"Aim�e #0" = 3
        "Albert #0" = 3
        "Alexandre #0" = 3
        "Am�d�e #0" = 3
	"Andr� #0" = 3		
        "Antoine #0" = 3
	"Armand #0" = 3
        "Arnaud #0" = 3
	"Barth�l�my #0" = 3
	"Bastian #0" = 3
	"Beno�t #0" = 3
	"Bertrand #0" = 3
        "C�sar #0" = 3
	"Charles #0" = 3
        "Calude #0" = 3
	"Cl�ment #0" = 3
	"Denis #0" = 3
	"Donat #0" = 3
	"�douard #0" = 3
        "Ermenegilde #0" = 3
	"�tienne #0" = 3
	"Eudes #0" = 3
	"Fabien #0" = 3
	"F�lix #0" = 3
	"Gr�goire #0" = 3
	"Honor� #0" = 3
        "Jean #0" = 3
	"Jean-Baptiste #0" = 3
	"Jourdain #0" = 3
	"L�on #0" = 3
	"Louis #0" = 3
	"Paul #0" = 3
	"Pierre #0" = 3
	"Sylvain #0" = 3
}

leader_names = {
	"d'Agoult" "d'Allemand" "des Alrics" "d'Anc�zune" "d'Artaud" "d'Astouaud"
	"des Balbs" Balbani "de Baldoni" "de Berton de Balbes" "de Brancas"
	"de Caritat de Condorcet" "de Cavaillon" "de Cheisolme" "de Crillon" 
	"de Farges" "de Fontfroide" "de Fortia"
	"de Gall�an" "de Grignan" Grimaldi 
	Imperiali "des Isnards"
	"de Joannis" Juliani
	"de L�vis" "de Libertat" "de Lop�s"
	"de Malespine" "de Marseille-Vintimille" "de Mistral de Mondragon" "de Montauban" "de Montmorency"
	"de Novarin"
	"Odde de Bonniot" "d'Orange" "d'Orl�ans de la Motte" 
	"de Panisse" "de Pascalis" "Pelletier de Gigondas" "de P�ruzzi"
	"de Raff�lis" "Raimond de Mormoiron"  Raousset
	"de Saluces" "des Seguins"
	Tonduti
	"d'Urban"
	"de Vassadel" "de Villeneuve"
}

ship_names = {
        "Notre Dame" "Sainte Anastasie" "Saint Andiol" "Saint Andr�" "Sainte Anne"
        "Saint Antonin" "Saint Auban" "Saint Aubert" "Sainte Baume" "Saint Cannat"
        "Sainte Catherine" "Sainte C�cile" "Saint Claude" "Sainte Colombe" "Saint Cyr"
        "Saint Edh" "Saint Etienne" "Saint Gabriel" "Saint Geni�s" "Saint Genis"
        "Saint Hyppolyte" "Sainte Jalle" "Saint Jean" "Saint J�r�me" "Saint Laurent"
        "Saint Maime" "Saint Marc" "Saint Marcel" "Saint Marcellin" "Sainte Marguerite"
        "Saintes Marie" "Saint Martin" "Saint Maurice" "Saint Maximin" "Saint Michel"
        "Saint Pantal�on" "Saint Paul" "Saint Paul Tricastin" "Saint Pierre" "Saint Pierre Avez"
        "Saint R�my" "Saint Quenin" "Saint Restitut" "Saint Rimbert" "Saint Romain"
        "Saint Roman" "Saint Saturnin" "Saint Sauveur" "Saint V�ran" "Saint Vincent"
}

army_names = {
         "Arm�e de Saint Michel" "Arm�e de Saint Georges" "Arm�e de l'Archange Gabriel"
         "Arm�e de Notre Dame"   "Arm�e de Saint Jacques" "Arm�e de Saint Dominique"
         "Arm�e de Saint Jean"   "Arm�e de Saint Maurice"
	 "Arm�e de $PROVINCE$" 
}
