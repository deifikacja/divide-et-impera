#Country Name: Please see filename.

graphical_culture = northamericagfx

color = { 108  19  108 }

historical_ideas = {
	humanist_tolerance
	national_conscripts
	battlefield_commisions
	grand_army
	national_trade_policy
	military_drill
	cabinet
	engineer_corps
	bill_of_rights
	patron_of_art
	espionage
	shrewd_commerce_practise
}

historical_units = {
	native_indian_archer
	native_indian_tribal_warfare
	algonkin_tomahawk_charge
	native_indian_horsemen
	commanche_swarm
	creek_arquebusier
	sioux_dragoon
	american_western_franchise_warfare
}

monarch_names = {
	"Opothleyaholo" = 20	
	"Emistesigo" = 20	
	"Fife" = 20	
	"Lamochattee" = 20	
	"Yahatatastenake" = 20	
	"Mico Malatchi" = 20	
	"Atepa" = 20	
	"Fala" = 20	
	"Isi" = 20	
	"Kiwidinok" = 20	
	"Minco" = 20	
	"Nashoba" = 20	
	"Nito" = 20	
	"Opa" = 20	
	"Poloma" = 20	
	"Talulah" = 20	
	"Yaholo" = 20	
}

leader_names = {
}

ship_names = {
	Ibofanga
	tvpvsvnv
	"Okhvtk� Yvhv" "Okhvtk� Nokos�" "Lvst� Nokos�" "Okhvtk� Lvmhe"
	"Cat� lvmhe" "Cat� Ayo" "Ayo-rakko" "Est-akwvnayv"
	"Esaugetuh Emissee" "Nunne Chaha" "Hisagita-imisi" "Sint Holo"
}
