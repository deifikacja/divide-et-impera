#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 165  132  57 }

historical_ideas = { bellum_iustum
        grand_army
	national_conscripts
        military_drill
        battlefield_commisions
        cabinet
        patron_of_art
	engineer_corps
	glorious_arms
	espionage
	national_trade_policy
	deus_vult
}

historical_units = {
yuan_1st_category
kazan_swarm
kalmyk_invaders
westernized_horde
yuan_3rd_category
tartar_arch
mongol_gunpowder_warfare
mongol_reformed_infantry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery

}

monarch_names = {
	"Taizu #0" = 40 		#Special exception made because his name is so famous
	"Tolo #0" = 20
	"Sibeoci Fiyanggu #0" = 20
	"Fuman #0" = 20
	"Giocangga #0" = 20
	"Taksi #0" = 20
	"Huang Taiji #0" = 20
	"Shunzhi #0" = 20
	"Kangxi #0" = 20
	"Yangzheng #0" = 20
	"Qianlong #0" = 20
	"Jiaqing #0" = 20
	"Daoguang #0" = 20
	"Cungsan #0" = 20
	"Mongke Temur #0" = 20
	"Fanca #0" = 10
	"Dorgon #0" = 10
	"Hoogle #0" = 10
	"Yinzhi #0" = 10
	"Yinreng #0" = 10
	"Yinti #0" = 10
	"Daisan #0" = 5
	"Abatai #0" = 5
	"Ajige #0" = 5
	"Dodo #0" = 5
	"Yinxiang #0" = 5
	"Yinsi #0" = 5
	"Yintang #0" = 5
	"Hongshi #0" = 5
	"Hongzhou #0" = 5
	"Yongqi #0" = 5
	"Hejing #0" = -5
	"Hejia #0" = -5
	"Hexiao #0" = -5
	"Zhuangjing #0" = -5
	"Bukuri Yongson #1" = 1
	"Akjan #0" = 3
	"Ayan #0" = 3
	"Baohu #0" = 3
	"Chengjun #0" = 3
	"Dorgide #0" = 3
	"Eje #0" = 3
	"Encehen #0" = 3
	"Fisin #0" = 3
	"Fulu #0" = 3
	"Ganyong #0" = 3
	"Ganzhan #0" = 3
	"Hurong #0" = 3
	"Jabsan #0" = 3
	"Jakdan #0" = 3
	"Leli #0" = 3
	"Liyi #0" = 3
	"Lung #0" = 3
	"Mergen #0" = 3
	"Muduri #0" = 3
	"Mujin #0" = 3
	"Oori #0" = 3
	"Radi #0" = 3
	"Talman #0" = 3
	"Wehe #0" = 3
	"Yelu #0" = 3
	"Yongyi #0" = 3
	"Zhanhung #0" = 3
	"Zhenghu #0" = 3
	"Zhengrong #0" = 3
	"Zhongcheng #0" = 3
	"Zhongchun #0" = 3
	"Zhongho #0" = 3
	"Zhonghu #0" = 3
	"Zhonghung #0" = 3
	"Zongqian #0" = 3
	"Zhongxian #0" = 3
	"Zhongyong #0" = 3
	"Zhongyu #0" = 3
}

leader_names = {
	Nurhaci
	Xi
	Ji
	Zhengzhi
	Deyi
	Guozho
	Zhifang
	Alantai
	Yuanwen
	Dian
	Folun
	Tianfu
}

ship_names = {
	"Aisin Gioro" Niuhuru Yehenala Heseri
	Uya Waliuha Janggiya Guwalgiya Tunggiya
	Magiya Hata Ula Huifa Haixi
}
