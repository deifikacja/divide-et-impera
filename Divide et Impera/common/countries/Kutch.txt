#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 159  111  122 }

historical_ideas = { naval_transport
	national_conscripts
	glorious_arms 
	grand_army
	military_drill
	battlefield_commisions 
	shrewd_commerce_practise
	engineer_corps
	merchant_adventures
	national_trade_policy
	espionage
	smithian_economics
	bureaucracy
}

historical_units = {
	indian_footsoldier
	rajput_hill_fighters
	indian_arquebusier
	mughal_musketeer
	mughal_mansabdar
	indian_elephant
	indian_shock_cavalry
	rajput_musketeer
	reformed_mughal_musketeer
	reformed_mughal_mansabdar
	indian_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Jado #1" = 10
	"Lakho #1" = 10
	"Ratto #1" = 10
	"Othoji #1" = 10
	"Gaoji #1" = 10
	"Vahenji #1" = 10
	"Murvoji #1" = 10
	"Kaiyaji #1" = 10
	"Amarji #0" = 10
	"Bheemji #0" = 10
	"Hamirji #0" = 10
	"Khengarji #0" = 20
	"Tamachiji #0" = 10
	"Rayadhanji #0" = 20
	"Pragmalji #0" = 20 
	"Gohodaji #0" = 20
	"Bharmalji #1" = 20
	"Deshalji #1" = 20
	"Vijayaraja #0" = 20
	"Madan Singh #0" = 10
}

leader_names = {
	Rajput
	Babber
	Bargujars
	Jadani
	Chandelas
	Chauhan
	Jadeja
	Kachwaha
	Karmavati
	Khumba
	Paramaras
	Pratap
	Rayadhan
	Singh
	Tomaras
	Mihirbhoj
	Mahipala
	Nagabhatta
	Padmini
	Vaghela
	Vatsraja
}

ship_names = {
	Chambal Devi Durga Gayatri Ghaggar
	Godwar Jalsena "Kala Jahazi" Lakshmi
	"Lal Jahazi" Luni Nausena "Marwar ka Nav"
	"Merwar ka Nav" "Nav ka Yudh" "Nila Jahazi"
	Parvati Radha Rani Ratri Sagar Sarasvati
	Sekhawati Sita
}
