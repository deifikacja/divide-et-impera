#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 219 234 216 } 

historical_ideas = { liberal_arts
	high_technical_culture
	shrewd_commerce_practise
	battlefield_commisions
	national_trade_policy
	national_conscripts
	military_drill
	merchant_adventures
	engineer_corps
	espionage
	grand_army
	glorious_arms
}

historical_units = {
	muslim_footsoldier
muslim_shamshir
mamluk_duel
muslim_infantry
muslim_sturm
muslim_mass_infantry
ali_bey_reformed_infantry
persian_rifle
musellem
shaybani
new_shaybani
abbasid
new_muslim_clan
ways
new_ways w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Mansur #1" = 10
	"Iraq #1" = 10
	"Muhammad #1" = 10
	"'Abdallah #1" = 10
	"Abu Sa'id Ahmad #1" = 10
	"Abu 'Abdallah Muhammad #1" = 10
	"Ma'mun #2 Abu 'Ali" = 10
	"'Ali Abu'l-Hasan #1" = 10
	"Mohammed Abu'l-Harith #1" = 10
	"Altuntash Hajid #1" = 10
	"Harun #1" = 10
	"Isma'il Khandam #1" = 10
	"Abu'l-Fawaris #1" = 10
	"Anushtigin #1" = 10
	"Ekinchi #1" = 10
	"Arslan Tigin #1" = 10
	"Qizil Arslan #1" = 10
	"Il-Arslan #1" = 10
	"Tekish Abu'l-Muzzafar #1" = 10
	"Mahmud Abu'l-Qasim #1" = 10
	"Muhammad Aladdin #1" = 10
	"Mingburnu #1" = 10
	"Aq #0" = 10
	"Husain #0" = 10
	"Yusuf #0" = 10
	"Balankhi #0" = 10
	"Maing #0" = 10
	"Sulayman #0" = 10
	"Usman #0" = 10
	"Abd al-Khalik #0" = 10
	"Sharif Sufi #0" = 10
	"Abu'l Khayr #0" = 10
	"Haidar #0" = 10
	"Ilbars Khan #0" = 10
	"Sultan Hajji Khan #0" = 10
	"Hasan Quli Khan #0" = 10
	"Buchugha Khan #0" = 10
	"Sufiyan Khan #0" = 10
	"Avanesh Khan #0" = 10
	"Kal Khan #0" = 10
	"Aqatay Khan #0" = 10
	"Yunus Khan #0" = 10
	"Dost Khan" = 10
	"'Ali Sultan #0" = 10
	"Ishi Sultan #0" = 10
	"Hajji Muhammad #0" = 10
}

leader_names = { 
	"Mongke Nasan" "Bora Chinua" "Shria Gal" "Boke Arslan" "Suren Unegen"
	"Arigh Gan" "Qara Temur" "Bayan Checheg" "Yeke Burilgi" 
	"Chagan Erdene" "Altan Vachir" "Mongke Enq" "Mongke Bayar" 
}

ship_names = {
	Amudarja Syrdarja Karakul Karsi Guzar
	Gulistan Cimkent Jangijul Chudzand Kerki
	Gizduvan Chiva Bajsun Mukry
}
