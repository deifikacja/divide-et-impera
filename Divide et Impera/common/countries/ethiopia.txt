#Country Name: Please seefilename.

graphical_culture = africangfx

color = { 56  120  191 }

historical_ideas = { liberal_arts
	national_conscripts
	shrewd_commerce_practise
	divine_supremacy
	national_trade_policy
	military_drill
	engineer_corps
	battlefield_commisions
	merchant_adventures
	grand_army
	espionage
	cabinet
	bureaucracy
}

historical_units = {
mali_spearman
songhai_spearman
ethiopian_mountain_warfare
ethiopian_gunpowder_warfare
niger_gunpowder_warfare
westernized_ethiopian
ethiopian_guerilla_warfare
mali_cavalry
songhai_cavalry
sudan_cavalry
nubian_cavalry
fulani_cavalry
}

monarch_names = {
	"Iy�s� #0" = 60
	"Taqla H�ym�n�t #0" = 60
	"Sal�m�n #2" = 40
	"Y�hannes #0" = 40
	"Hezqey�s #0"= 40
	"Zar'a Y�qob #0" = 20
	"Ba'eda M�ry�m #1" = 20
	"Quastant�n�s #3" = 20
	"'Amda Sey�n #1" = 20
	"N�'�d #0" = 20
	"Lebna Dengel #0" = 20
	"Gal�wd�w�s #0" = 20
	"M�n�s #0" = 20
	"Sarsa Dengel #0" = 20
	"Y�'qob #0" = 20
	"S�seny�s #0" = 20
	"F�s�ladas #0" = 20
	"T�w�fl�s #0" = 20
	"Y�st�s #0" = 20
	"Y�nas #0" = 20
	"D�w�t #0" = 20
	"'Amsa G�y�rg�s #0" = 20
	"Tuqla G�y�rg�s #0" = 20
	"Demetros #0" = 20
	"G�y�rg�s #0" = 20
	"Fiqtor #0" = 5
	"Menas #0" = 5
	"Marqos #0" = 5
	"Hamalmal #0" = 5
	"Saga Krestos #0" = 5
	"Malta Krestos #0" = 5
	"F�s�l #0" = 5
	"'Endrey�s #1" = 1
	"Yeshaq #1" = 1
	"Mika'el #0" = 3
	"Amda Seyon #0" = 3
	"Antenatewos #0" = 3
	"Welde Giyorgis #0" = 3
	"Mika'el Sehul #0" = 3
	"Gusho #0" = 3
	"Wand Bewossen #0" = 3
	"Haile #0" = 3
	"Wolda Gabrael #0" = 3
}

leader_names = {
	Bayisa
	Dereje
	Ambo
	Asres
	Wami
	Eshete
	Ezana
	Eyob
	Asfaw
	Degife
}

ship_names = {
	Yodit Shewa "Mara Takla" Lalibela
	"Yekuno Amlak" Tigray Tatadum Seyum
	"Kedus Harbe" "Na'akueto La'ab" Yetbarak
}
