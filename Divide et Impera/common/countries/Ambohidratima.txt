#Country Name: Please seefilename.

graphical_culture = africangfx

color = { 255  3  3 }

historical_ideas = {
	colonial_ventures
	national_conscripts
	shrewd_commerce_practise
	national_trade_policy
	military_drill
	engineer_corps
	battlefield_commisions
	merchant_adventures
	grand_army
	espionage
}

historical_units = {
	african_spearmen
	bantu_tribal_warfare
	bantu_plains_warfare
	bantu_gunpowder_warfare
	westernized_bantu
	african_western_franchise_warfare
}


monarch_names = {
	"Andriantompoinimerina #0" = 20
	"Andriantrimonibemihisatra #0" = 10
	"Andrianmanananimerina #0" = 10
	"Andriambelomasina #0" = 10
	"Andriambelo #0" = 10
	"Andriambelononana #0" = 10
	"Ramanandrianjaka #0" = 10
	"Ravorambato #0" = 10
	"Rabehety #0" = 10
	"Andriantompoimrinamanadimby #0" = 10
	"Rambolamasoandro #0" = 10
}

leader_names = {
}

ship_names = {
	Fiantaranasoa Antananriva Tananariva Merina Imerina Imernia Toliary Mahajanga Antsiranana
}
