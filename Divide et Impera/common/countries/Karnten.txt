#Country Name: Please see filename.

graphical_culture = latingfx

color = { 170  43  21 }

historical_ideas = { bellum_iustum
	national_conscripts
	military_drill
	patron_of_art
	grand_army
	divine_supremacy
	national_bank
	engineer_corps
	regimental_system
	high_technical_culture
	scientific_revolution
	glorious_arms
	napoleonic_warfare
}

historical_units = {
	#Infantry German
	w_i_armed_peasantry
	w_i_longbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_hungarian_formation
	w_i_spanish_formation
	w_i_chosen_formation
	w_i_swedish_formation
	w_i_quarter_army
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_prussian_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Ferdinand #0" = 60
	"Albrecht #3" = 40
	"Franz #0" = 40
	"Joseph #0" = 40
	"Karl #0" = 40
	"Maximilian #0" = 40
	"Leopold #7" = 20
	"Friedrich #4" = 20
	"Franz #0 Stefan" = 20
	"Ladislav #0" = 20
	"Matthias #0" = 20
	"Leopold #7 Johann" = 15
	"Ernst #1" = 15
	"Ferdinand #0 Wenzel" = 15
	"Georg #0" = 15
	"Philipp #0" = 15
	"Johann Karl #0" = 15
	"Johann Leopold #0" = 15
	"Karl #0 Joseph" = 15
	"Maria Theresia #0" = -15
	"Leopold #7 Wilhelm" = 10
	"Rudolf #4" = 10
	"Alexander #0" = 10
	"Anton #0" = 10
	"Franz Karl #0" = 10
	"Johann #0" = 10
	"Ludwig #0" = 10
	"Rainer #0" = 10
	"Adolf #0" = 3
	"Alfred #0" = 3
	"Benno #0" = 3
	"Bruno #0" = 3
	"Cristoph #0" = 3
	"Clemens #0" = 3
	"Florian #0" = 3
	"Gerhard #0" = 3
	"G�nther #0" = 3
	"Heinrich #0" = 3
	"Jakob #0" = 3
	"Joachim #0" = 3
	"J�rg #0" = 3
	"Michael #0" = 3
	"Oskar #0" = 3
	"Otto #0" = 3
	"Siegfried #0" = 3
	"Stefan #0" = 3
	"Ulrich #0" = 3
	"Viktor #0" = 3
	"Wilhelm #0" = 3
	"Wolfgang #0" = 3
}

leader_names = {	
	"von Habsburg" "von Waldstein" "von Ihering" Seyssel
	Haemrich "von Esterhazy" "von Oberg" "von Sch�nburg"
	"von Quadt" Ketterer "von Stubenrauch" "von Hatzfeldt"
	"von Neippberg" Lobkowitz Mittelbach "von Laudon"
	Alvintzky "von Hohenberg" Erlmayr "von Stark" Meinl
	Bethmann Kolin Heinrich "von Riedesel" Freystadt
	"von Guttenberg" Frantsits "von Stauffenberg"
	"von Kapfenberg" Zakrzewsky Schr�der Sauerzapff
	Minucci Sulerzycki Stichsenstein Hagedorn Arenberg
	Boroewitsch Steiger Tegethoff Buseck Byszynski Zwerger
	Casper "von Sch�nbrunn" Poll "von Schwarzenberg"
}

ship_names = {
	"Viribus Unitis" Theresia Austria Habsburg Tirol "Baron Bruck" "Baron Call" Belvedre
	Pannonia Salzach Wien Pustazer Traisen "Erzherzog Ferdinand" Herkules Lacroma
	Triest "Plus Ultra" Aurora Albatros Saida Novara Imperator Trinitas Doppeladler 
	"Sankt Stephan" Felix "Austria Imperare" Lachs Leitha Goliath Samson Vindobona
	Inexhaustus Invulnerabilis Pavidarus Fortitas Emenentius "Sankt Wolfgang" 
	Vigilatia M�we "Fides et Spes" Nixe Natter Nymphe Taurus "Kaiserin Maria Theresia" Wachau Donau Inn Lindwurm Zenta Zara Fiume Arpad Babenberg Zrinyi
	Pola Tiger Panther Leopard Trabant Satellit
}

army_names = {
	"K.u.K Armee" "Habsburgische Armee" "Kaiserliche Armee" "Armee von B�hmen" "Armee von Ungarn" "Armee von Italien" "Armee von Schlesien" "Armee von $PROVINCE$"
}

fleet_names = {
	"Adriatik Flotte" "Habsburgische Flotte" "Kaiserliche Flotte"
}


