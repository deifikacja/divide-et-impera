#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 150  150  97 }

historical_ideas = { naval_transport
	merchant_adventures  
	excellent_shipwrights 
	shrewd_commerce_practise 
	superior_seamanship
	national_conscripts
	national_trade_policy
	sea_hawks
	naval_fighting_instruction
	military_drill
	naval_glory
	engineer_corps
	battlefield_commisions
}

historical_units = {
	east_asian_spearmen
	eastern_bow
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	asian_musketeer
	reformed_asian_musketeer
	reformed_asian_cavalry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

leader_names = {
	Abdulfatah Ahmad Amir Anwar Azmi 
	Batuta 
	Chairul 
	Hamzah Hasanuddin 
	Ibnu 
	Kyai 
	Mahat Mukhtar 
	Najamuddin Natzar Nazmizan 
	Roem 
	Syarifuddin 
	Zulkifli 
}

monarch_names = {
	"Mahmud #1" = 10
	"Aladdin #1" = 10
	"Muzzafar #1" = 10
	"Abdul Jalil #5" = 10
	"Abdullah #1" = 10
	"Ibrahim Shah #0" = 10
	"Mahmud Shah #3" = 10
	"SUleiman Shah #1" = 10
	"Ahmad Riayat #1" = 10
	"Abdul Rahman #1" = 10
	"Husein Muadzam #1" = 10
	"'Ali Iskandar #0" = 10
	"Tun Daeng #0" = 10
	"Tun Abu Bakr #0" = 10
	"Abu Bakr #0" = 10
	"Ibrahim #0" = 10
	"Ismail #0" = 10
	"Mahmud Iskandar #0" = 10
}

ship_names = {
	Parameswara "Sultan Iskandar" Bendahara Laksamana
	Temenggung "Penghulu bendahari" Shahbandar
	"Hukum Kanun" "Undang-Undang" "Pulau Bakung"
	"Pulau Lingga" Kepulauan "Pulau Singkep" "Kepulauan Riau"
	"Pulau Rempang" "Pulau Kundur" "Pulau Padang"
	"Pulau Bengkaus" "Pulau Tinggi" "Pulau Sibu"
	"Pulau Tioman" "Pulau Aur" "Pulau Rupat"
	"Pulau Rangsang" "Pulau Tebing Tinggi"
}
