#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 152  128  52 } 

historical_ideas = {
	national_conscripts
	glorious_arms 
	grand_army
	military_drill
	battlefield_commisions 
	shrewd_commerce_practise
	engineer_corps
	merchant_adventures
	national_trade_policy
	espionage
	bureaucracy
	cabinet
}

historical_units = {	
muslim_footsoldier
muslim_shamshir
mamluk_duel
muslim_infantry
muslim_sturm
muslim_mass_infantry
ali_bey_reformed_infantry
persian_rifle
musellem
shaybani
new_shaybani
abbasid
new_muslim_clan
ways
new_ways w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery

}

monarch_names = {
	"ar-Rabi #1" = 10
	"Abd ar-Rahman #1" = 10
	"Abdallah #1" = 10
	"Abu Muhammad Talha #1" = 10
	"Abu Muhammad al-Hadjadj #1" = 10
	"Abu Uqba al-Djarrah #1" = 10
	"Maan #1" = 10
	"Ibeahim #1" = 10
	"Dirham #1" = 10
	"Abu Yusuf Yaqub #2" = 20
	"Abu Hafs Amir #2" = 20
	"Tahir #2" = 20
	"Laith #1" = 10
	"Muhammad #1" = 10
	"al-Muaddal #1" = 10
	"Mohammad #1" = 10
	"Ahmad Niya #1" = 10
	"Qusayr #1" = 10
	"Ahmad #1" = 10
	"Abdallah #1" = 10
	"Khalaf #1" = 10
	"Husein #1" = 10
	"Tagh ad-Din #1 Nasr" = 10
	"Baha ud-Dawla Tahir #3" = 30
	"Badr ad-Dawla #1" = 10
	"Baha ud-Dawla #2" = 20
	"Tagh ad-Din #2 Nasr" = 20
	"Shams ad-Din Ahmad #1" = 10
	"'Izz ad-Din #1" = 10
	"Tagh ad-Din #3 Kharb" = 30
	"Yamin ad-Din Bahram Shah #1" = 10
	"Tagh ad-Din #4 Nasir" = 40
	"Shihab ad-Din Mahmud #1" = 10
	"Rukn ad-Din Mahmud #1" = 10
	"Abul Muzaffar 'Ali #1" = 10
	"Ala ad-Din Ahmad #1" = 10
	"Nasir ad-Din Uthman Shah #1" = 10
	"Inal Tegin #1" = 10
	"Shams ad-Din 'Ali #1" = 10
	"Nasir ad-Din Muhammad #1" = 10
	"Nusrat ad-Din Muhammad #1" = 10
	"Qutb ad-Din #1 Muhammad" = 10
	"Tagh ad-Din #1" = 10
	"Jalal ad-Din Mahmud #1" = 10
	"Izz ad-Din Karman #1" = 10
	"Shams ad-Din 'Ali #1" = 10
	"Nizam ad-Din #1" = 10
	"Shams ad-Din Mohammad #1" = 10
	"Zun-n-Nun-Beg Argun #1" = 10
	"Sultan Mahmud Sistani #1" = 10
	"Djalal ad-Din #1" = 10
	"Khamza #1" = 10
	"Nusrat #1" = 10
	"Djafar #1" = 10
	"Fath 'Ali Khan #1" = 10
	"Muhammad Hussein #1" = 10
	"Kalbali #1" = 10
	"Mahmud #1" = 10
	"Malik Asadullah #1" = 10
	"'Ali Quli Khan #1" = 10
	"Sulayman #1" = 10
	"Bahram #1" = 10
	"Muhammad Nasr #1" = 10
}

leader_names = {
	Hotaki
	Taraki
	Amin
	Karmal
	Najibullah
	Akhund
	Rahman
	Ghazi
	Ahmad
	Qadir
	Sistani
	Khwarazmi
	Mihrabani
	Keyani
}

ship_names = {
	Arghandab Khash Farah Harut Harirud
	Murghab Balkh Kowkcheh Kabol Zhob
	Kech Dasht Hingol
}
