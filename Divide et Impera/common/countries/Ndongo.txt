#Country Name: Please seefilename.

graphical_culture = africangfx

color = { 119  16  139}

historical_ideas = { bellum_iustum
	shrewd_commerce_practise
	national_conscripts
	merchant_adventures
	military_drill
	national_trade_policy
	battlefield_commisions
	engineer_corps
	grand_army
	espionage
	cabinet
}

historical_units = {
	african_spearmen
	niger_kongolese_tribal_warfare
	niger_kongolese_forest_warfare
	niger_kongolese_gunpowder_warfare
	westernized_niger_kongolese
	niger_kongolese_guerilla_warfare
}

monarch_names = {
	"Nzinga Nkuwa #0" = 20
	"Afonso #0" = 100
	"Pedro #0" = 120
	"Diogo #0" = 20
	"Bernardo #0" = 40
	"�lvaro #0" = 220
	"Garcia #0" = 40
	"Ambr�sio #0" = 20
	"Ant�nio #0" = 40
	"Daniel #0" = 20
	"Andr� #0" = 20
	"Nicolau #0" = 20
	"Sebasti�o #0" = 20
	"Aleixo #0" = 20
	"Henrique #0" = 40
	"Jos� #0" = 40
	"Ilunga #0" = 3
	"Kasongo #0" = 3
	"Sanza #0" = 3
	"Kumwimba #0" = 3
	"Kasongo #0" = 3
	"Kekenya #0" = 3
	"Kaumbo #0" = 3
	"Miketo #0" = 3
	"Nday #0" = 3
	"Mwine #0" = 3
	"Maloba #0" = 3
	"Kitamba #0" = 3
	"Cibind #0" = 3
	"Mbala #0" = 3
	"Nawej #0" = 3
	"Munying #0" = 3
	"Kateng #0" = 3
	"Cakasekene #0" = 3
	"Cikombe #0" = 3
	"Kalong #0" = 3
	"Mutand #0" = 3
	"Mbala #0" = 3
	"Mukaz #0" = 3
	"Mpemba #0" = 3
}

leader_names = {
	Lissouba
	Bongo
	Boussombo
	Youlou
	Nguesso
	Youla
	Rallum
	Raoumbe
	Tayot
	Makanga
}

ship_names = {
	"Nimi a Nzima" "Nsaku Lau" "Lukeni lua Nimi"
	Matadi "Mpemba Kasi" Mbata Mbanza Nanga Nlaza
	"Nkuwu a Ntinu"
}
