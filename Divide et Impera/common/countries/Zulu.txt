#Country Name: Please seefilename.

graphical_culture = africangfx

color = { 215  60  216 }

historical_ideas = { bellum_iustum
	national_conscripts
	shrewd_commerce_practise
	divine_supremacy
	national_trade_policy
	military_drill
	engineer_corps
	battlefield_commisions
	merchant_adventures
	grand_army
	espionage
}

historical_units = {
	african_spearmen
	bantu_tribal_warfare
	bantu_plains_warfare
	bantu_gunpowder_warfare
	westernized_bantu
	african_western_franchise_warfare
}


monarch_names = {
	"Ntu #1" = 10
	"Mnguni #1" = 10
	"Luzumana #1" = 10
	"Malandela #1" = 10
	"Zulu #1" = 10
	"Gumede #1" = 10
	"Phunga #1" = 10
	"Mageba #1" = 10
	"Ndaba #1" = 10
	"Jama #1" = 10
	"Mkabayi #1" = -10
	"Senzangakhona #1" = 10
	"Shaka #1" = 20
	"Dingane #0" = 10
	"Mpande #0" = 10
	"Cetshwayo #0" = 10
	"Dinuzulu #0" = 10
	"Bhekuzulu #0" = 10
	"Zwelithini #0" = 10
}

leader_names = {
kaMnguni kaLuzumana kaNtombela kaZulu kaGumede kaMageba kaNdaba kaJama kaSenzangakhona kaCetshwayo kaDinuzulu kaMpande
}

ship_names = {
	Kyoga Unyamwezi
}
