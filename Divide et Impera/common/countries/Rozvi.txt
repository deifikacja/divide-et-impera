#Country Name: Please seefilename.

graphical_culture = africangfx

color = { 186  164  178}

historical_ideas = { bellum_iustum
	national_conscripts
	shrewd_commerce_practise
	merchant_adventures
	military_drill
	national_trade_policy
	battlefield_commisions
	engineer_corps
	grand_army
	espionage
	cabinet
}

historical_units = {
	african_clubmen
	bantu_tribal_warfare
	bantu_plains_warfare
	bantu_gunpowder_warfare
	westernized_bantu
	african_western_franchise_warfare
}


monarch_names = {
	"Matope #0" = 20
	"Mavura #0" = 40
	"Mukombero #0" = 20
	"Changamire #0" = 20
	"Kakuyo #0" = 20
	"Neshangwe #0" = 20
	"Chivere #0" = 20
	"Chirisamhuru #0" = 20
	"Gatsi #0" = 20
	"Nyambo #0" = 20
	"Siti #0" = 20
	"Kamborapasu #0" = 20
	"Dombo Chinkura #0" = 20  
	"Nechisake #0" = 20
	"Nechagadzike #0" = 20
	"Mambo Rupanadamananga #0" = 20
	"Gumboremvura #0" = 20 
	"Toloa #0" = 5
	"Matuzvianye #0" = 5
	"Nyatsimba #0" = 3
	"Mukombwe #0" = 3
	"Nyamhandu #0" = 3
	"Basvi #0" = 3
	"Tohwechipi #0" = 3
	"Nyatsimba #0" = 3
	"Ngwato #0" = 3
	"Molwa #0" = 3
	"Tamasiga #0" = 3
	"Seogola #0" = 3
	"Mucombo #0" = 3
	"Inhamunda #0" = 3
	"Ningomoxa #0" = 3
	"Chiraramuro #0" = 3
	"Chombo #0" = 3
	"Peranhe #0" = 3
	"Chicanga #0" = 3
	"Nyacuimbiri #0" = 3
	"Samutumbu #0" = 3
	"Mupunzagutu #0" = 3
	"Gowera #0" = 3
	"Nyarumwe #0" = 3
}

leader_names = {
	Chirombo
	Njie
	Rise
	Tembo
	Birir
	Chrima
	Kagona
	Nkomo
	Mutasa
	Moyo
}

ship_names = {
	Dziva Mwari Dzivaguru Mulungu Mbonga
	Svikiro Mazendere "Mai Vedu" Musikavanhu
	Mutorwa
}
