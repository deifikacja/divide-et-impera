#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 35 145 40 }

historical_ideas = { feudal_castles
	national_conscripts
	grand_army
	glorious_arms
	military_drill
	battlefield_commisions
	shrewd_commerce_practise
	merchant_adventures
	engineer_corps
	espionage
	national_trade_policy
	cabinet
	bureaucracy
}

historical_units = {
	indian_footsoldier
	rajput_hill_fighters
	indian_arquebusier
	mughal_musketeer
	mughal_mansabdar
	indian_elephant
	indian_shock_cavalry
	rajput_musketeer
	reformed_mughal_musketeer
	reformed_mughal_mansabdar
	maharathan_guerilla_warfare
	maharathan_cavalry
	bhonsle_infantry
	bhonsle_cavalry
	indian_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Fadli 'Ali #2" = 30
	"Muhammad Beg #1" = 10
	"Hosayn 'Ali #1" = 20
	"Gholam 'Ali #1" = 30
	"Mozzafar al-Molk #0" = 20
	"Gholam Mohammad 'Ali #2" = 20
	"Fath 'Ali #0" = 10
}

leader_names = {
	Kilandar
	Ranga
	Malik
	Nayna
	Kodali
	Chandna
	Ganesh
	Buchar
	Bhoola
	Garlanka
	Mallya
}

ship_names = {
	Alia Fatimah Godvari Jalsena
	"Kala Jahazi" Khan "Lal Jahazi"
	Mahanadi Nausena "Nav ka Yudh"
	Niknaz "Nila Jahazi" Niloufa
	Sagar Salima Suhalia Shameena
	Yasmine
}
