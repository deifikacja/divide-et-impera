#Hussite Poland

graphical_culture = easterngfx

color = { 40  10  10 }

historical_ideas = { bellum_iustum
	engineer_corps 
	merchant_adventures
	shrewd_commerce_practise
	national_conscripts
	glorious_arms
	national_trade_policy
	military_drill
	cabinet
	national_bank
	grand_army
	bureaucracy
	espionage
}

historical_units = {
	bardiche_infantry 
	eastern_bowman
	eastern_knights
	slavic_stradioti
	hungarian_hussar
	polish_musketeer
	polish_hussar
	polish_tercio
	polish_winged_hussar
	russian_lancer
	russian_cuirassier
	eastern_skirmisher
	eastern_uhlan
	eastern_carabinier
	russian_mass w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"August #0" = 4
	"Wladyslaw #0" = 25
	"Stanislaw #0" = 23
	"Kazimierz #0" = 23
	"Aleksander #0" = 20
	"Henryk #0" = 23
	"Jan #0" = 20
	"Jan #0 Albert" = 20
	"Jan #0 Kazimierz" = 20
	"Michal #0 Korybut" = 20
	"Stefan #0" = 20
	"Zygmunt #0" = 20
	"Zygmunt #0 August" = 20
	"Aleksander #0 Karol" = 15
	"Zygmunt #0 Kazimierz" = 15
	"Krzysztof #0" = 10
	"Fryderyk #0" = 10
	"Jan #0 Olbracht" = 10
	"Jan #0 Zygmunt" = 10
	"Karol #0 Ferdynand" = 10
	"Olbracht #0" = 10
	"Albert #0" = 3
	"Aleksy #0" = 3
	"Andrzej #0" = 3
	"Bartlomiej #0" = 3
	"Bogumil #0" = 3
	"Boleslaw #0" = 5
	"Dawid #0" = 3
	"Dominik #0" = 3
	"Emil #0" = 3
	"Eugeniusz #0" = 3
	"Ferdynand #0" = 3
	"Gustaw #0" = 3
	"Igor #0" = 3
	"Iwan #0" = 3
	"Krystian #0" = 3
	"Lech #0" = 5
	"Lucjusz #0" = 3
	"Ludwik #0" = 3
	"Lukasz #0" = 3
	"Maksym #0" = 3
	"Mariusz #0" = 3
	"Mieszko #0" = 5
	"Miroslaw #0" = 3
	"Oskar #0" = 3
	"Patryk #0" = 3
	"Rafal #0" = 3
	"Sylwester #0" = 3
	"Szymon #0" = 3
	"Zygfryd #0" = 3
}

leader_names = {
	Arciszewski
	Benyowski Bidzi�ski Branicki
	Czarniecki Chometowski
	Dunin Denhoff Drzewicki
	Grodzicki G�rka
	Houwaldt
	Jablonowski Jazlowiecki
	Kurozwecki Kurbski Koniecpolski Kosciuszko Krasinski Kalinowski Korycki Kazanowski Kmita
	Lubomirski Lisowski Lanckoro�ski Leszczy�ski Lacki
	Malski Mniszech Matczy�ski Mlodziejowski
	Ostror�g Olesnicki Opacki Ozarowski Ossolinski
	Paniewski Patkul Poniatowski Pulaski Przyjemski Piaseczy�ski Potocki
	Rytwia�ski Rzewuski
	Sobieski Sahajdaczny Siemienowicz Sulkowski Stadnicki Sieniawski
	Tarnowski Benyowski Bidzi�ski Branicki Bielski
	Czarniecki Chometowski Ciolek Arciszewski
	Dunin Denhoff Drzewicki Firlej Jaxa Gryfita Puchala Paluk
	Grodzicki G�rka Lis Sreniawita Labedz Borkowicz Zareba Bogoria         
	Houwaldt Nalecz Grzymalita Awdaniec Starza Topor Jastrzebiec Odrowaz 
	Jablonowski Jazlowiecki Karnkowski Czarnkowski Kozlowski
	Kurozwecki  Koniecpolski  Krasinski Kalinowski Korycki Kazanowski Kmita
	Lubomirski Lisowski Lanckoro�ski Leszczy�ski Laski Leliwa Leszczyc         
	Ostror�g Olesnicki Opacki Ozarowski Ossolinski Orzelski
	Paniewski Patkul Poniatowski Przyjemski Piaseczy�ski Potocki Pa�uk
	Rytwia�ski Rzewuski Rynarzewski Arciszewski Dzwonowski Grzyma�a
	Benyowski Bidzi�ski Branicki Bielski Skotnicki Gozdawa Wilczek
	Czarniecki Chometowski Ciolek Skotnicki Gozdawa Wilczek Dzwonowski Grzyma�a
	Dunin Denhoff Drzewicki Firlej Jaxa Gryfita Puchala Paluk
	Grodzicki G�rka Lis Sreniawita Labedz Borkowicz Zareba Bogoria         
	Houwaldt Nalecz Grzymalita Awdaniec Starza Topor Jastrzebiec Odrowaz 
	Jablonowski Jazlowiecki Karnkowski Czarnkowski Kozlowski
	Kurozwecki  Koniecpolski  Krasinski Kalinowski Korycki Kazanowski Kmita
	Lubomirski Lisowski Lanckoro�ski Leszczy�ski Laski Leliwa Leszczyc         
	Ostror�g Olesnicki Opacki Ozarowski Ossolinski Orzelski
	Paniewski Patkul Poniatowski Przyjemski Piaseczy�ski Potocki Pa�uk
	Rytwia�ski Rzewuski Rynarzewski Matczy�ski Muskata My�l�ci�ski
        Sulkowski Stadnicki Sieniawski Szydlowiecki �wi�ca
	Tarnowski Teczynski Wapowski Toporczyk
	Zamoyski Zebrzydowski Zborowski Lig�za Malski Mniszech
        Sulkowski Stadnicki Sieniawski Szydlowiecki �wi�ca
	Tarnowski Teczynski Wapowski Toporczyk
	Zamoyski Zebrzydowski Zborowski
        Lig�za Malski Mniszech Matczy�ski Muskata My�l�ci�ski
	Wyhowski Wielopolski
	Zamoyski Zebrzydowski Zaswilichowski Zborowski Zbaraski
}

ship_names = {
	"Arka Noego"
        "Bialy Orzel" Burza "Bialy Lew" "Brzesko-Kujawskie" Belzkie Braclawskie
        Charitas "Czarny Orzel" "Czarny Kruk" Chelminskie Czernichowskie
        Fortuna
        Gryf Gwiazda Gnieznienskie
	Inoworclawskie
	"Kr�l Dawid" Kaliskie Krakowskie Kijowskie Krak�w
        Lublin "Latajacy Jelen" Lw�w Leczyckie Lubelskie
        "Maly Bialy Orzel" Malborskie Mazowieckie
        Niewrazliwy "Nowy Czarny Orzel"
        "Prorok Samuel" "Panna Wodna" Plomien Poznanskie Plockie Podlaskie Podolskie
	"Rycerz Swiety Jerzy" Rawskie Ruskie
        Strzelec Suwerenny "Swiety Piotr" Sieradzkie Sandomierskie "Swiety Kazimierz"
        "Wielkie Slonce" Wodnik Warminskie Wolynskie Warszawa
        Zwyciestwo "Z�lty Lew"
}

army_names = {
	"Armia z $PROVINCE$"
}