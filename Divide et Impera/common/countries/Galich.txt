#Muscovy 

graphical_culture = easterngfx

color = { 206  101  97 }

historical_ideas = { bellum_iustum
	national_conscripts
	merchant_adventures
	military_drill
	battlefield_commisions
	quest_for_the_new_world
	national_trade_policy
	engineer_corps
	grand_army
	espionage
	grand_navy
	glorious_arms
	excellent_shipwrights
}

historical_units = {
	eastern_medieval_infantry
	eastern_knights
	slavic_stradioti
	eastern_militia
	muscovite_musketeer
	muscovite_caracolle
	muscovite_soldaty
	muscovite_cossack
	russian_petrine
	russian_lancer
	russian_green_coat
	russian_cuirassier
	eastern_skirmisher
	eastern_uhlan
	eastern_carabinier
	russian_mass w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

leader_names = {
	Chodkiewicz Chreptowicz Czaplic Doroszenko
	Gosiewski Gaszto�d
	Hlebowicz Kuncewicz
	Korsunovas Kiszka Kossakowski Konaszewicz Chmielnicki Korecki
	Mielzynski Massalski
	Niemirowicz
	Ostrogski Orszewski Ogi�ski Or�yk
	Pac Pociej Zbaraski
	Radziwi�� Razumowski
	Sapieha Sanguszko S�dziw�j S�uszka Sosnowski Samoj�owicz Szczuka Smotrycki
	Tyszkiewicz Plater Kryski Dembi�ski Chodkiewicz Sapieha Pac Mielecki Kmita
	Wyhowski  Czartoryski
	Wi�niowiecki Wo��owicz
	Zabie��o Z�kiewski Haraburda
	Dorohobu�ski Kaszy�ski Mikuli�ski Teliatewski 
    Cho�mski Czerniaty�ski Bie�osielski Dobry�ski
    Kargo�omski Sugorski Uchtomski  
	Aleksiejew Sapieha 
    Kamie�ski Kolcow M�cis�awski Rajewski
	Szuwa�ow Szeremietiew Wasilewicz Wo�kow
    Olelkowicz Koriatowicz Korybutowicz Holsza�ski
    Gli�ski Ostrogski Radziwi�� Hlebowicz
    Chodkiewicz Wasy�kowicz �wiatos�awicz
    Olgierdowicz Borysowski Gorodne�ski
    Hercygski Izjas�awlski Drucki Podbereski
    Kukiejnowski Lagowski Lugowski Mi�ski
    Stre�ebski Witebski Ro�cis�awowicz
    Jaros�awski M�cis�awowicz Iwanowicz
    Toropiecki Aleksandrowicz Juriewicz
    Konstantynowicz Dawidowicz Dymitrowicz
    Bria�ski Putywlski Kurski Lubecki G�uchowski
    Czerkieski Homelski Wirski Olegowicz Jarope�koicz
    Wsiewo�odowicz Jaros�awowicz Romanowicz
    Maskiewicz Gaszto�d Chreptowicz
}

monarch_names = {
	"Konstantin #1" = 10
	"David #1" = 10
	"Theodore #1" = 10
	"Ivan #1" = 10
	"Dimitr #2" = 40
	"Georgi #1" = 10
	"Vassily #0" = 5
}

ship_names = {
	"Dmitri Donskoi" "Vasili I" "D. Aleksandrovich"
	Rurik "Ivan Kalita" Moscha Volga Schorna Coluna
	Cortiza Mosnisk Olescho Piersek Troitzkoy Wolok
	Klin Iausa
}
