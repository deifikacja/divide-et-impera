#Country Name: Please see filename.

graphical_culture = latingfx

color = { 20  20  17 }

historical_ideas = { 
	feudal_castles
	national_conscripts
	merchant_adventures
	humanist_tolerance
	patron_of_art
	colonial_ventures
	military_drill
	espionage
	cabinet
	bureaucracy
	scientific_revolution
}

historical_units = {
	#Infantry German Cities
	w_i_armed_peasantry
	w_i_municipal_militia
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_hungarian_formation
	w_i_dutch_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_british_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Charles #0" = 60
	"Philippe #2" = 100
	"Louis #0" = 30
	"Louis-Henri #0" = 20
	"Louis-Joseph #0" = 20
	"Henri #1" = 10
	"Antoine #0" = 10
	"Henri-Jules #0" = 10
	"Joseph #0" = 10
	"Marie #0" = -10
	"Fran�ois #0" = 5		
	"Hughes #5" = 1
	"Eudes #4" = 1
	"Robert #2" = 1
	"Jean #1" = 1
	"Armand #0" = 3
	"Barth�lemy #0" = 3
	"Beno�t #0" = 3
	"Bernard #0" = 3
	"Claude #0" = 3
	"Denis #0" = 3
	"�tienne #0" = 3
	"Eug�ne #0" = 3
	"Eustache #0" = 3
	"Georges #0" = 3
	"Gui #0" = 3
	"Guillaume #0" = 3
	"Isidore #0" = 3
	"Jean-Baptiste #0" = 3
	"Jules #0" = 3
	"Marc #0" = 3
	"Maurice #0" = 3
	"Michel #0" = 3
	"Nicolas #0" = 3
	"Pascal #0" = 3
	"Pierre #0" = 3
	"Raoul #0" = 3
	"Ren� #0" = 3
	"S�bastien #0" = 3
	"Simon #0" = 3
}

leader_names = {
        d'Aban d'Abonde Arbelot d'Auxy
        "de Balay" "de Bauffremont" "de Beaumont" "de Beugre" "de Boulay" "de Bourgeois" Bourgogne-Montaigu "de Brachet" "de Brimeu"
        "de Castaing" "de Chabot" "de Ch�lon" "de Ch�lon-Arlay" "de Chandieu" "de Chastellux" "de Chin" "de Cicon" "de Coligny" "de Cr�quy" "de Cuiseaux"
        "de Damas" "de Dammartin" "de Donzy" "de Dyo"
        "d'Egmont"
        "de Ferrette"
        "de Lalaing" "de Lannoy" "Le Corgne" "de Lugny"
        "de Marle" "de Melun" "de Montfaucon"
        "Nagu de Varennes" "de Nanton" "de Neufch�tel" "de Noyelles"
        "de Pontailler" "de la Porte"
        "Pot de la Roche" "de Rochebaron" "de Rochechouart" "de Roye"
        "de Scorailles" "de S�mur"
        "de Ternant" "de Tonnerre" "de Toucy" "de Toulonjon"
        "de Vaubresson" "de Vaudrey" "de Vellexon" "de Vergy" "de Vienne" "de Villaines" "de Villiers"
}

ship_names = {
	Arman�on Arroux
	Bourgogne Brugge
	Cambrai "Castrum Divionense" Childebert "Childebert II" Chilperic Citeaux Chlothar Cluny Comtesse "C�te-d'Or" 
	Dijon "Duc de Bourgogne" Duchesse 
	Gebicca Giselher Godegisel Godomar Gunderic Gundobad Gundomar "Gundomar II" Gunther Guntram
	"Jean sans Peur"
	"Marie de Bourgogne"
	"Philippe le Bon" "Philippe le Hardi"
	"Richard de Bourgogne"
	Sa�ne Sigismund
	Theudebert "Theuderic II"
	V�zelay
}
