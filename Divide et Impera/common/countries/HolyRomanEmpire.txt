graphical_culture = latingfx

color = { 150  177  161 }

historical_ideas = {
	bellum_iustum
	shrewd_commerce_practise
	humanist_tolerance
	merchant_adventures
	patron_of_art
	national_bank
	national_trade_policy
	cabinet
	smithian_economics
	ecumenism
	scientific_revolution
	military_drill
}

historical_units = {
	#Infantry German
	w_i_armed_peasantry
	w_i_longbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_hungarian_formation
	w_i_spanish_formation
	w_i_chosen_formation
	w_i_swedish_formation
	w_i_quarter_army
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_prussian_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Friedrich #2" = 60
	"Albrecht #1" = 50
	"Friedrich #2 Wilhelm" = 40
	"Friedrich #2 August" = 30
	"Ferdinand #0" = 30
	"Wilhelm #0" = 30
	"Karl #4" = 20
	"Christian #0" = 20
	"Johann #0 Georg" = 20
	"Maximilian #0" = 20
        "Franz #0" = 20
        "Joseph #0" = 20
        "Heinrich #7" = 15
	"Karl #4 Albrecht" = 15
	"Karl #4 Theodor" = 15
	"Friedrich #2 Albrecht" = 15
	"Friedrich #2 Christian" = 15
	"Albrecht #1 Achilles" = 15
	"August #0" = 15
	"Ernest #0" = 15
	"Ferdinand #0 Maria" = 15
	"Franz #0 Stefan" = 15
	"Georg #0" = 15
	"Georg #0 Wilhelm" = 15
	"Joachim #0 Hector" = 15
	"Joachim #0 Nestor" = 15
	"Johann #0" = 15
	"Johann #0 Cicero" = 15
	"Johann #0 Friedrich" = 15
	"Johann #0 Siegmund" = 15
	"Ladislav #0" = 15
	"Leopold #0" = 15
	"Matthias #0" = 15
	"Mortiz #0" = 15
	"Siegmund #0" = 15
	"Karl #4 Joseph" = 10
        "Ludwig #4" = 10
	"August #0 Wilhelm" = 10
	"Wilhelm #0 Friedrich" = 10
	"Alexander #0" = 10
	"Anton #0" = 10
	"Christian #0 Albrecht" = 10
	"Hektor #0" = 10
	"Johann #0 Karl" = 10
	"Johann #0 Leopold" = 10
	"Leopold #0 Ferdinand" = 10 
	"Leopold #0 Johann" = 10
	"Maximilian #0 Philipp" = 10
	"Ruprecht #0" = 10
	"Wenzel #0" = 10
}

leader_names = { 
	Adam Angermann Appelt Arndt Beck Becker Bening Blasius
	Brilz Br�ser B�ttner Carl Dahm Deneke Eckstein Edelmann 
	Elsner Feil Fischer Franke Giewer Haase Hafner Hertel 
	Hirsch H�hn Kammel Keller Kessler Koeppe Kohn Kr�mer
	Kuhn Lindner Mertesdorf Meyer Mick M�ller Necker Noell 
	Oelgem�ller Raabe Rainer Scharfbillig Schave Scheler 
	Schmitt Schneider Schumann Stephan Thelen Trierweiler
	Uhlig "von Franken" "von Rothenburg" "von Schwaben"
	"von Vitzgau" "von Weimar" Wachsmuth Wacht "von Werl"
	Walther Wecker
}

ship_names = {
	Ariadne Habicht Adler Albatross Amazone
	Arcona Basilisk Beowulf Biene Wespe Wolf
	Nautilus Natter Otter Pelikan Pfeil Meteor
	M�we M�cke Luchs K�nig Kaiser Eber Drache
	Falke Frauenlob Freya Fuchs Gazelle Geier
	Greif Condor Cormoran Comet Wacht Tiger
	Seeadler Jaguar Hy�ne Blitz Bremse Brummer
	Bussard Vulkan
}

army_names = {
	"Kaiserliche Armee" "Armee von $PROVINCE$" 
}

fleet_names = {
	"Kaiserliche Flotte"
}