#Country Name: Please see filename.

graphical_culture = africangfx

color = { 92 92 174 }

historical_ideas = { naval_transport
	merchant_adventures
	excellent_shipwrights
	shrewd_commerce_practise
	superior_seamanship
	national_conscripts
	national_trade_policy
	sea_hawks
	naval_fighting_instruction
	military_drill
	naval_glory
	battlefield_commisions
	engineer_corps
}

historical_units = {
	polynesian_daggermen
	polynesian_spearmen
	polynesian_clubmen
	polynesian_javelinmen
	polynesian_long_clubmen
	polynesian_shark_tooth
	polynesian_two_blade
	polynesian_throwing_axe
	polynesian_lua
	polynesian_slingermen
	polynesian_rifles
}

monarch_names = {                         
	"Makea #1" = 10
	"Rangi #1" = 10
	"Makea #0" = -10
}

leader_names = {
		"Te Rangi" Tukivao Makea "Te Patuakino" Pini Ken Tinirau Tekao Pori Karika Davida Pa "Te Vairua" Tuaivi Daniela Tavake Takau Mana "Nui Teremona" Tupou Fairoa Inanui Love-Nia
}

ship_names = {
		Avarua Rarotonga Pue Matavera Ngatangiia Muri Titikaveka Arorangi Nikao Avatiu
}

fleet_names = {
	"King's Canoes"
}