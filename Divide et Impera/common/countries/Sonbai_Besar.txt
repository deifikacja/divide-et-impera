#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 10  50  120 }

historical_ideas = {
	merchant_adventures
	excellent_shipwrights
	shrewd_commerce_practise
	superior_seamanship
	national_conscripts
	national_trade_policy
	sea_hawks
	naval_fighting_instruction
	military_drill
	naval_glory
	battlefield_commisions
	engineer_corps
}

historical_units = {
	east_asian_spearmen
	eastern_bow
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	asian_musketeer
	reformed_asian_musketeer w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Nai Dawan #1" = 10
	"Nai Natti #1" = 10
	"Nai Lele #1" = 10
	"Nai Tukulua #1" = 10
	"Ama Tuan #1" = 10
	"Nai Manas #1" = 10
	"Nai Talus #1" = 10
	"Nai Sobe #1" = 10
	"Nai Tafin #1" = 10
	"Nai Kau #1" = 10
	"Nai Bau #1" = 10
	"Nai Nasu #1" = 10
	"Noni #1" = -5
	"Bawa Leu #1" = 10
	"Corneo #1" = 10
	"Nube Bena #2" = 10
	"Gerek Baki #1" = 10
	"Isu Baki #1" = 10
	"Ote #1" = 10
	"Babkas #1" = 10
	"Meis #1" = 10
	"Isu #1" = 10
	"Said #1" = 10
}

leader_names = {
	Babkas Tuan Sonbai Molo Leu Taffij Liurai Aulase Baki Nisnojni Nube Meis
}

ship_names = {
	"Sonbai Besar"
}
