#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 131  124  107 }

historical_ideas = {
	national_conscripts
	glorious_arms
	military_drill
	battlefield_commisions
	shrewd_commerce_practise
	grand_army
	merchant_adventures
	espionage
	engineer_corps
	national_trade_policy
	cabinet
	smithian_economics
}

historical_units = {
	indian_footsoldier
	rajput_hill_fighters
	indian_arquebusier
	mughal_musketeer
	mughal_mansabdar
	indian_elephant
	indian_shock_cavalry
	rajput_musketeer
	reformed_mughal_musketeer
	reformed_mughal_mansabdar
	maharathan_guerilla_warfare
	maharathan_cavalry
	bhonsle_infantry
	bhonsle_cavalry
	indian_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Ravaji #1" = 10
	"Vibhaji #1" = 10
	"Sataji #1" = 10
	"Lakhaji #1" = 30
	"Ranmalji #0" = 10
	"Raisinhji #1" = 20
	"Tamachi #1" = 20
	"Hardholji #1" = 10
	"Jasaji #1" = 10
	"Sataji #1" = 10
	"Jashwantsihji #1" = 10
	"Ranjitsinhji #1" = 10
	"Digvijaysinghi #1" = 10
}

leader_names = {
	Desai
	Jain
	Gandhi
	Majmudar
	Munshi
	Muzzafar
	Parikh
	Pandya
	Patel
	Patidar
	Vanik
	Vyas
	Rushi
	Saraiya
	Shaw
	Shrivastav
	Tilak
}

ship_names = {
	Ambika Auranga Bhadar Damanganga
	Devki Jalsena "Kala Jahazi" Khari
	"Lal Jahazi" Luni Mahi Mithi
	Nausena Narmada "Nav ka Yudh"
	"Nila Jahazi" Purna Sabarmati
	Sagar Saraiya Saraswati Shetrunji
	Tapi Yasmin
}
