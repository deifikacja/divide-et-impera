#Country Name: Please see filename.

graphical_culture = latingfx

color = { 161  139  40 }

historical_ideas = { 
	early_mining
	battlefield_commisions
	divine_supremacy
	ecumenism
	patron_of_art
	grand_army
	national_bank
	engineer_corps
	bureaucracy
	regimental_system
	high_technical_culture
	scientific_revolution
	glorious_arms
	napoleonic_warfare
}

historical_units = { 
#Infantry German
	w_i_armed_peasantry
	w_i_longbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_hungarian_formation
	w_i_spanish_formation
	w_i_chosen_formation
	w_i_swedish_formation
	w_i_quarter_army
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_prussian_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon 
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {  
	"Vladislav #2" = 20
	"Ferdinand #0" = 20
	"Fridrich #0 Falcky" = 20
	"Jiri #0" = 20
	"Ladislav #0" = 20
	"Karel #4" = 20
	"V�clav #4" = 20
	"Ludvik #0" = 10
	"Jindrich #1 Fridrich" = 10
	"Zikmund #1" = 10
	"Karel #4 Ludvik" = 5
	"Jan #1" = 5
	"Jindrich #1" = 5
	"Eduard #0" = 5
	"Fridrich #0" = 5
	"Ruprecht #0" = 5
	"Rudolf #1" = 5
	"Premysl #2" = 1
	"Konrad #2" = 1
	"Viktorin #0" = 1
	"Alexandr #0" = 3
	"Bronislav #0" = 3
	"Ctibor #0" = 3
	"David #0" = 3
	"Drahoslav #0" = 3
	"Ignac #0" = 3
	"Jakub #0" = 3
	"Josef #0" = 3
	"Kliment #0" = 3
	"Leos #0" = 3
	"Lubomir #0" = 3
	"Lukas #0" = 3
	"Marek #0" = 3
	"Mikulas #0" = 3
	"Miroslav #0" = 3
	"Ondrej #0" = 3
	"Otakar #0" = 3
	"Pavel #0" = 3
	"Vilem #0" = 3
	"Zdislav #0" = 3
	"Albrecht #0" = 3
	"Matyas #0" = 3
	"Maxmilian #0" = 3
	"Leopold #0" = 3	
	"Frantisek #0" = 3
	"Bedrich #0" = 3
	"Divis #0" = 3
	"Prokop #0" = 3
	"Petr #0" = 3
	
}

leader_names = { 
	"Adam z Veleslavina" "Adrspach z Dube" Abrozek Augusta
	Balbin Beran Bilovsky Bily Blahoslav "Borita z Martinic" Bridel "Brikci z Cimperka" Bydzovsky
	"Cakovec z Bohusic" "Dacicky z Heslova" 
	Dobrovsky Drslav
	Holub Holy Hvezda
	Chelcicky
	Jaros "Jencik z Jezova"
	Konias "Kornel ze Vsehrd" Kounic Kratky "Krcin z Jelcan"
	Landfras 
	Nejedly Netolicky
	Pernstejn Prazsky
	Rejsek Rokycana Rozmberk
	Schwarzenberk Skala Slavata Spidlik Sternberk
	Tham "Trcka z Lipy" Tranovsky
	Vambersky Vitek Vodnansky "Vratislav z Mitrovic" Vrestovsky
	Waldhauser
	"z Brandysa" "z Cimburka" "z Husi" "z Jenstejna" "z Kolovrat" "z Lamberka" "z Lobkovic" "z Miletina" "z Mladonovic" "z Podebrad" "z Prachatic" "z Vresovic" "ze San" "ze Stribra" "ze Zerotina"
	Ziegler Zima Zelezny Zelivsky

}

ship_names = {	
	"Svaty Vaclav"
	"Svaty Mikulas"
	"Svaty Jiri"
	"Svata Anezka"
	"Svata Ludmila"
	"Svat� Gorazd"
	"Svat� Zikmund"
	"Kral Karel"
	"Kral Boleslav"
	"Kral Premysl"	
	"Kral Vaclav"
	"Panna Marie Vitezna"
	"Jan Hus"
	Cyril
	Metodej
	Sarka
	Ctirad
	Libuse
	Horymir
	Semik
	Budweiser
	Libuse
	Orel
	Sokol
	Sipka
	Hvezda
	Stribro
	Praha
	Brno
	Olomouc
	Plzen
	"Kutna Hora"
}
