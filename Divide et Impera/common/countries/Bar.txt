#Country Name: Please see filename.

graphical_culture = latingfx

color = { 168  40  110 }

historical_ideas = {
	national_conscripts
	merchant_adventures
	patron_of_art
	shrewd_commerce_practise
	military_drill
	national_bank
	cabinet
	smithian_economics
	bureaucracy
	scientific_revolution
	engineer_corps
	glorious_arms
}

historical_units = {
	#Infantry France
	w_i_armed_peasantry
	w_i_crossbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_chosen_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_french_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon 
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Robert #1" = 55
	"Edward #2" = 50
	"Ren� #0" = 35
	"Louis #0" = 30
	"Henri #4" = 20
	"Frederick #3" = 10
	"Thierry #1" = 10
	"Sophie #1" = -10
	"Theodoric #2" = 10
	"Reginald #2" = 10
	"Theobald #2" = 10
	"Nicholas #0" = 10
	"Fran�ois #0" = 10
	"Philippe #0" = 10
	"Charles #0" = 10
	"Jean #0" = 10
	"Antoine #0" = 10
	"Nicolas #0" = 10
	"Isabelle #0" = -10
	"L�opold #0" = 10
	"Claude #0" = 5
	"Ferdinand Philippe #0" = 5
	"L�opold #0 Cl�ment" = 5
	"Rodolphe #0" = 5
	"Charles #1 Ferdinand" = 5
	"Charles #1 Henri" = 5
	"Fran�ois #1 Antoine" = 5
	"Joseph #0" = 5
	"Alfred #0" = 5
	"Bernard #0" = 5
	"David #0" = 5
	"G�rard #0" = 10
	"Jacques #0" = 10
	"Gaston #0" = 10
}

leader_names = {
	d'Ambleteuse d'Aulnay
	"de Beauharnais" "de Bercy" "de B�thune" "de Boissieu" "de Bonnefoy" "de Bonneuil" "du Bosquet" "de la Bretonni�re" "de Broglie"
	"de Castaing" "de Caumartin" "de Chambly" "de Chambronne" "de Champmartin" "de Chevigny" "des Coulons" "de Cr�vecoeur" "de Crussol"
	"de Dampierre" Desgouttes Duquesne
	"des Ecures" d'Entraigues d'Estr�es
	"du Fournay"
	"de Galard"
	"des Herbiers"
	"de La Barthe" "de La Ferrandie" "de La Galissoni�re" "de La Mothe" "de La Motte d'Airan" "de La Porte" "Le Tellier" "de Lh�ry" "de Luynes"
	"de Marguerye" "de Maurepas"
	"de Neufch�tel"
	"d'Ornano"
	"de Pellefort"
	"de Richemont" "de Rochechouart" "de Rochemaure" "de Rieu"
	"de Saint Chamond" "de Saint Esprit" "de Saint Germain" "de Saint Omer" "de Siorac"
	"de Tocqueville" "de la Tour d'Auvergne" "de Tr�ville"
	"d'Usson"
	"du Valentinois" "de Vergennes" "de Vibien" "de Vigny" "de Villeneuve"
}

ship_names = {
	"Emperor Lothar" "Lotharingen"
	"Rene II Lothringen" Barrois Metz
	Toul Verdun Warding Skirmjan
	Bera Scoc Sekjan
}
