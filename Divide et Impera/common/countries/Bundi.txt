#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 159  111  122 }

historical_ideas = {
	national_conscripts
	glorious_arms 
	grand_army
	military_drill
	battlefield_commisions 
	shrewd_commerce_practise
	engineer_corps
	merchant_adventures
	national_trade_policy
	espionage
	smithian_economics
	bureaucracy
}

historical_units = {
	indian_footsoldier
	rajput_hill_fighters
	indian_arquebusier
	mughal_musketeer
	mughal_mansabdar
	indian_elephant
	indian_shock_cavalry
	rajput_musketeer
	reformed_mughal_musketeer
	reformed_mughal_mansabdar
	indian_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Deva #1" = 10
	"Napuji #1" = 10
	"Hamuji #0" = 10
	"Vir #0" = 10
	"Biru #0" = 10
	"Bandu #0" = 10
	"Narayan #0" = 10
	"Suraj #0" = 10
	"Surtan #0" = 10
	"Surjan #0" = 10
	"Bhoj #0" = 10
	"Ratan #0" = 10
	"Chatra #0" = 10
	"Bhao #0" = 10
	"Anirudh #0" = 10 
	"Budh #0" = 10
	"Dalel #0" = 10
	"Umaid #0" = 10
	"Ajit #0" = 10
	"Bishen #0" = 10
	"Ram #0" = 10
	"Raghubir #0" = 10
	"Ishwari Singhji #0" = 10
	"Bahadur #0" = 10 
}

leader_names = {
	Singh Mal Das Bahadur "Singhaji Bahadur" Singhaji
}

ship_names = {
	Chambal Devi Durga Gayatri Ghaggar
	Godwar Jalsena "Kala Jahazi" Lakshmi
	"Lal Jahazi" Luni Nausena "Marwar ka Nav"
	"Merwar ka Nav" "Nav ka Yudh" "Nila Jahazi"
	Parvati Radha Rani Ratri Sagar Sarasvati
	Sekhawati Sita
}
