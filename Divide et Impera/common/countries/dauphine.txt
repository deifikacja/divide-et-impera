#Country Name: Please see filename.

graphical_culture = latingfx

color = { 93  194  122 }

historical_ideas = { 
	feudal_castles
	national_conscripts
	merchant_adventures
	humanist_tolerance
	patron_of_art
	colonial_ventures
	military_drill
	quest_for_the_new_world
	cabinet
	bureaucracy
	scientific_revolution
	liberty_egalite_fraternity
	napoleonic_warfare
}

historical_units = {
	#Infantry France
	w_i_armed_peasantry
	w_i_crossbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_chosen_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_french_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Guigues #8" = 40
	"Humbert #2" = 40
	"Jean #2" = 40
	"Huges #2" = 10
	"Anne #1" = -10
	"Adh�mar #0" = 3
	"Arnaud #0" = 3
	"Aymeric #0" = 3
	"Bartomiu #0" = 3
	"Bernat #0" = 3
	"Bertran #0" = 3		
	"Charles #0" = 3
	"Dami�n #0" = 3
	"Enric #0" = 3
	"Est�ve #0" = 3
	"Ferr�n #0" = 3
	"Folquet #0" = 3
	"Frances #0" = 3
	"Gast� #0" = 3
	"Gaufr� #0" = 3
	"Guilhem #0" = 3
	"Ignaci #0" = 3
	"Jacme #0" = 3
	"Jordan #0" = 3
	"Loys #0" = 3
	"Mart� #0" = 3
	"Matheu #0" = 3
	"Miqueu #0" = 3
	"Nicolau #0" = 3
	"Per #0" = 3
	"Pol #0" = 3
	"Pons #0" = 3
	"Ramond #0" = 3
	"Ricard #0" = 3
	"Sanc #0" = 3
	"Simos #0" = 3
	"Vicentz #0" = 3
	"Victor #0" = 3
	"Xavier #0" = 3
	"Victor #0" = 3
	"John #0" = 3
}

leader_names = {
        d'Abon d'Agoult "Agrain des Ubaz" d'Alande-Mirabel d'Alauzon d'Albon Alleman d'Andrevet d'Anger�s d'Arbalestier d'Arcilay Artaud d'Aspremont
        "de Balathier" "de Bardonnenche" "de Beaumont" "de Belmont" "de Bernard" "de Bocsozel" "de Bompart" "de Bonlieu" "de Boulieu" "de Bressieu"
        "de Calignon" "de Chalendar" "de Chambarlhac" "de Chambly" "de la Chambre" "de Champiney" "de Cheilus" "de Cl�rieux"
        "de Di�" "de Domaine"
        "de l'Espine"
        "de Forbin"
        "de Gandil" "de Gen�ve" "de Granges"
        "de La Balme" "de La Farge" "de La Porte" "de La Tour du Pin" Lesdigui�res
        "de Marcieu" "de M�rins" "de Miribel" "de Mollin" "de Montchenu" "de Montferrat"
        "Pape de Saint Auban" "de Peccat" "de Peloux" "de P�rissol" "de Perdrix" "de Pracomtal"
        "de R�vilasc"
        "de Sassenage" "de Seyssel"
        "du Terrail"
        "de Virieu"
}

ship_names = {
        Apprieu 
        Barnave Beaurepaire Bellegarde-Poussieu Bourg-de-P�age "Bourg d'Oisans" "Bourgoin" "Brian�on"
        Cessieu Clelles Crolles
        Dieu-le-Fit
        Embrun
        Gap Grenoble
        Huez
        "La C�te Saint Andr�" "La Mure" "La Tour du Pin" "Le Poet-Laval" "Le Pont de Claix" "Le Teil" Livet-et-Gavet "Les Abrets" Loriol "Lus la Croix Haute"
        Mens Moirans Mont�limar
        Rochemaure Roussillon
        Saillans "Saint Clair de la Tour" "Saint Etienne en D�voluy" "Saint Geoir" "Saint Jean de Bournay" "Saint Laurent en Royan" "Saint Roman" Sauzet Serres
        Tullin
        Valbonnais Valence Venosc Veynes Vienne Vif Vizille Voiron Voreppe
}


army_names = {
         "Arm�e du Dauphin"     "Arm�e du Viennois"  "Arm�e du Gr�sivaudan"
         "Arm�e du Valentinois" "Arm�e du Royans"    "Arm�e du Queyras"   
         "Arm�e de l'Embrunais" "Arm�e du Gapen�ais" "Arm�e du Brian�onnais" 
         "Arm�e du Diois"       "Arm�e du D�voluy"   "Arm�e du Vercors"   
         "Arm�e des Baronnies"  "Arm�e du Tri�ves"   "Arm�e du Champsaur"
	 "Arm�e de $PROVINCE$" 
}