#Country Name: Please see filename.

graphical_culture = southamericagfx

color = { 139  193  16 } 

historical_ideas = {
	humanist_tolerance
	national_conscripts
	military_drill
	national_trade_policy
	engineer_corps
	national_bank
	battlefield_commisions
	merchant_adventures
	cabinet
	smithian_economics
	espionage
	patron_of_art
}

historical_units = {
	south_american_spearmen
	south_american_warfare
	inca_mountain_warfare
	south_american_gunpowder_warfare
	westernized_south_american
	peruvian_guerilla_warfare
}

monarch_names = {
	"Anco Coyuch #0" = 20
	"Caja Cimcim #0" = 20
	"Chumun Caur #0" = 20
	"Guaman Chumu #0" = 20
	"Michan Caman #0" = 20
	"Guari Cur #1" = 15
	"Nancen Pico #1" = 15
	"Tacaynamo #1" = 10
	"Aiyotechapun #0" = 3
	"Buvaovamu #0" = 3
	"Caja C�pac #0" = 3
	"Cama Bergu #0" = 3
	"Chumtiku #0" = 3
	"Cuyuchi #0" = 3
	"Docoshamu #0" = 3
	"Draytonamu #0" = 3
	"Ehenymacu #0" = 3
	"Felan Tamuc #0" = 3
	"Gusmango #0" = 3
	"Ilsayunamu #0" = 3
	"Jotiantikaka #0" = 3
	"Katitika #0" = 3
	"Litun Orco #0" = 3
	"Melantonan #0" = 3
	"Michan Chimor #0" = 3
	"Muhton Belan #0" = 3
	"Nartamamu #0" = 3
	"Nehuametepun #0" = 3
	"Opolcynamun #0" = 3
	"Pacatmamu #0" = 3
	"Puznaymun #0" = 3
	"Relyen Padnu #0" = 3
	"Seton Apac #0" = 3
	"Seran Tugun #0" = 3
	"Taracun #0" = 3
	"Troynuremo #0" = 3
	"Urtyantemu #0" = 3
	"Vetsalynun #0" = 3
	"Yetuniamu #0" = 3
	"Zergoetanu #0" = 3
}
leader_names = {
	Guacricaur
	Tacaynamo
	Tiso
	Nancempinco
	Chilche
	Maras
	Luyes
	Maila
	Illa
	Rimac
}

ship_names = {
	Viracocha "Apo Inti" 
	"Chiqui Illapa" Mamquilla Yakumama Mamacocha 
	Pachakama Waca "Chan Chan" Chimor Chicama Moche 
	Viru Sipan Huancaco
}
