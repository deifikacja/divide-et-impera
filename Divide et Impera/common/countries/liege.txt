#Country Name: Please see filename.

graphical_culture = latingfx

color = { 149  186  37 }

historical_ideas = { early_mining
	merchant_adventures
	national_conscripts
	military_drill
	patron_of_art
	shrewd_commerce_practise 
	national_trade_policy
	cabinet
	national_bank 
	smithian_economics
	scientific_revolution
	engineer_corps
	glorious_arms
}

historical_units = {
	#Infantry Netherlands
	w_i_armed_peasantry
	w_i_municipal_militia
	w_i_mercenary
	w_i_battle_formation
	w_i_condottiere
	w_i_halberdiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_british_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Jean #5" = 80
	"Robert #1" = 20
	"Corneille #0" = 20
	"Ferdinand #0" = 20
	"Fran�ois-Antoine #0" = 20
	"Fran�ois-Charles #0" = 20
	"G�rard #0" = 20
	"�rard #0" = 20
	"Ernest #0" = 20
	"Jean-Louis #0" = 20
	"Jean-Th�odore #0" = 20		
	"Georges #0" = 20
	"Georges-Louis #0" = 20
	"Joseph-Cl�ment #0" = 20
	"C�sar-Constantin #0" = 20
	"Charles #0" = 20
	"Louis #0" = 20
	"Maximilien-Henri #0" = 20
	"Guillaume #3" = 5
	"Alexandre #2" = 5
	"Th�oduin #1" = 5
	"Henri #1" = 5
	"Adriaan #0" = 3
	"Andries #0" = 3
	"Augustijn #0" = 3
	"Bartholomeus #0" = 3
	"Boudewijn #0" = 3
	"Christiaan #0" = 3
	"Constantijn #0" = 3
	"Dani�l #0" = 3
	"Dominicus #0" = 3
	"Fabian #0" = 3
	"Frans #0" = 3
	"Gillis #0" = 3
	"Godfried #0" = 3
	"Herman #0" = 3
	"Ignaas #0" = 3
	"Ivo #0" = 3
	"Jacob #0" = 3
	"Stefanus #0" = 3
}

leader_names = {
	Aubry Bouchet Chauchet Digneffe Fabry
	G�rard Hauzeur Henkart Josias Nannan
	Bastin Bauduinet Bitha Cloet Colson
	"da Franquinet" "de Donnea" "de Finesse"
	"de Haxhe" "de Lacu" "de Spirlet" Haeck
	Lambermont Paulet Philippens Reul Reynier
	Vanhove "la Vignette" 
}

ship_names = {
	"St. Lambert" "St. Hubert" "St. Martin"
	Meuse "Notger of Liege" "Place du Queer"
	"Saint Jaques" "Saint Bartholomew" Seraing
	"Sart-Tilman" Luik "Saint-Nicolas" Ans
}
