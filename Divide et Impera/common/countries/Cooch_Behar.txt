#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 13  75  77 }

historical_ideas = { bellum_iustum
	national_conscripts
	glorious_arms
	military_drill
	battlefield_commisions
	shrewd_commerce_practise
	grand_army
	merchant_adventures
	national_trade_policy
	engineer_corps
	cabinet
	espionage
	bureaucracy

}

historical_units = {
	east_asian_spearmen
	eastern_bow
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	asian_musketeer
	reformed_asian_musketeer
	reformed_asian_cavalry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Prithu #1" = 10
	"Sandhya #1" = 10
	"Sindhu #1" = 10
	"Rup #1" = 20
	"Singhadhwaj #1" = 10
	"Pratapdhwaj #1" = 10
	"Dharma #1" = 10
	"Durlabh #1" = 10
	"Indra #1" = 10
	"Sasanka #0" = 10
	"Gajanka #0" = 10
	"Sukranka #0" = 10
	"Mriganka #0" = 10
	"Niladhwaj #0" = 10
	"Chakradhwaj #0" = 10
	"Nilambar #0" = 10
	"Chandan #0" = 10
	"Biswa #0" = 10
	"Nara #0" = 10
	"Lakshmi #0" = -10
	"Bir #0" = 10
	"Pran #0" = 10
	"Mod #0" = 10
	"Vasudeo #0" = 10
	"Mahendra #0" = 10
	"Upendra #0" = 10
	"Debendra #0" = 10
	"Dhairjendra #0" = 10
	"Rajendra #0" = 20
	"Dharendra #0" = 10
	"Harendra #0" = 10
	"Shivendra #0" = 10
	"Narendra #0" = 10
	"Nripendra #0" = 10
	"Jitendra #0" = 10
	"Jagaddipendra #0" = 10
	"Indra Devi #0" = 10
	"Raghudev #0" = 10
	"Parikshit #0" = 10
}

leader_names = {
	Baruah
	Borphukan
	Chandra
	Das
	Deka
	Kalita
	Mahanta
	Narayan
	Prithu
	Roy
	Prithu
	Singha
	Koch
	Khen
}

ship_names = {
	Brahmaputra "Chingri Mach" Darrang
	Devi Dibang "Durga Ma" Ganga "Hilsa Mach"
	"Ilish Mach" Machli "Kali Ma" "Kali Meghana"
	"Kali Nauk" Kamrup Lakimpur "Lal Nauk"
	"Loki Devi" Luhit "Manasa Devi" Manjuli
	Meghna "Nil Nauk" "Nil Sagor" "Parvati Devi"
	"Rui Mach" Saraswoti "Sagorer Bahini" Shakti
	"Shaktir Nauk" "Shasti Ma" Sibsagar Trishool
	"Uma Devi"
}
