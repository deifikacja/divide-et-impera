#Country Name: Please see filename.

graphical_culture = southamericagfx

color = { 224  86  191 }

historical_ideas = { liberal_arts
	deus_vult
	divine_supremacy
	patron_of_art
	national_conscripts
	military_drill
	glorious_arms
	battlefield_commisions
	superior_seamanship
	merchant_adventures
	excellent_shipwrights
	engineer_corps
	shrewd_commerce_practise
}

historical_units = {
	south_american_spearmen
	maya_tribal_warfare
	maya_forest_warfare
	maya_gunpowder_warfare
	westernized_mayan
	maya_guerilla_warfare
}

leader_names = {
	Ahkinxoc Ahmetakkukul Ahmok Ahnakukpech Ahpop-Achi Arana
	Bakan Balam Bor
	Canek Chaob Chiam
	Hnuyg
	Kayom K�in Kukumatz
	Mochcouoh
	Nachancan Nohochacyum
	Pacal
	"Quitze Balam"
	Tecunuman
	Vataki
	Yumcaax
	Zotz
}

monarch_names = {
	"Can Ek #0" = 60
	"Balam Quitze #0" = 20
	"Cocom #1" = 20
	"Pacal #1" = 20
	"Vahxaqui Caam #0" = 20
	"Ah Cacao #1" = 15
	"Cham Bahlun #1" = 15
	"Cu Ix #1" = 15
	"Pacal Balam #1" = 15
	"Yaxum Balam #1" = 15
	"Butz Chan #1" = 10
	"Chaan Muan #1" = 10
	"Kan Boar #1" = 10
	"Yax Kuk Mok #1" = 10
	"Yax Pac #1" = 10
	"Ah Can #0" = 3
	"Ahkinxok #0" = 3
	"Ahmok #0" = 3
	"Ahpop-Achi #0" = 3
	"Arana #0" = 3
	"Bakan #0" = 3
	"Bor #0" = 3
	"Cocaib #0" = 3
	"Cocoja #0" = 3
	"Conache #0" = 3
	"Cotuja #0" = 3
	"Gucamatz #0" = 3
	"Hnuyg #0" = 3
	"Kan Bahlun Mo #0" = 3
	"Kayom #0" = 3
	"Kukumatz #0" = 3
	"Mochcouoh #0" = 3
	"Nohochacyum #0" = 3
	"Oxib Queh #0" = 3
	"Tecunuman #0" = 3
	"Tutul Xiu #0" = 3
	"Tziquin #0" = 3
	"Vataki #0" = 3
	"Yumcaaz #0" = 3
	"Zotz #0" = 3
}

ship_names = {
	Ak� "Ah Puch" "Altun Ha"
	Bonampak Becan Bolonchen
	Chaac Camazotz Cancu�n Caracol Cerros Chacchoben Chacmultun Chinikiha Chinkultic 
	Chunchucmil Civiltuk Calakmul Copan Coba
	"Chichen Itza"
	Dzibilchaltun Dzibilnocac Dzilam Dzibilchaltun
	"Dos Pilas" "Cahal Pech"
	Edzn� Ekab "Ek'Balam" 
	Gumarcaj Gucumatz
	Hocchob Hormiguero Hunahpu Huracan
	Ixbalanque Ixchel Ixtab Ichmac Ichmul Itzan Ixil Iximche Ixlu Izamal "Ix Chel"
	Labn� Lamanai Louisville Lubaantun LubaantunJaina Jolja "Joya de Cer�n"
	Kaminaljuyu Kabah Kiuic "Kantunil Kin"
	Lamanai 
	Mayapan Machaquila Mani Muluchtzekel Muyil 
	Nakum Nakbe Naranjo Nebaj "Nim Li Punit"
	Okop Oxcutzcab Oxkintok 
	Palenque Pomon� Pusilha "Piedras Negras"
	Quirigua
	"Rio Azul" "Rio Bec"
	Sayil Seibal Sisila
	Tonina Tikal Tulum Tancah Tazumal Tecoh Topoxt� 
	Uxmal Uaxactun Ucanal Uci Utatlan
	Wak�
	Xcalumkin Xcaret Xcorralche Xcucsuc Xculoc Xlapak Xpuhil Xultun Xunantunich
	Yaklmai Yaxchilan Yaxha Yo'okop 
	Zipacna Zaculeu 
}
