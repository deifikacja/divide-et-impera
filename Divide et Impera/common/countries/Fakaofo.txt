#Country Name: Please see filename.

graphical_culture = africangfx

color = { 233 210 22 }

historical_ideas = { naval_transport
	merchant_adventures
	excellent_shipwrights
	shrewd_commerce_practise
	superior_seamanship
	national_conscripts
	national_trade_policy
	sea_hawks
	naval_fighting_instruction
	military_drill
	naval_glory
	battlefield_commisions
	engineer_corps
}

historical_units = {
	polynesian_daggermen
	polynesian_spearmen
	polynesian_clubmen
	polynesian_javelinmen
	polynesian_long_clubmen
	polynesian_shark_tooth
	polynesian_two_blade
	polynesian_throwing_axe
	polynesian_lua
	polynesian_slingermen
	polynesian_rifles
}

monarch_names = {                         
	"Letaiolo #1" = 10
	"Poufau #1" = 10
	"Taupe #0" = 10
	"Havaiki #0" = 10
	"Lika #0" = 10
	"Longotahi #0" = 10
	"Vaopuka #0" = 10
	"Te #0" = 10
	"Tavita #0" = 10
	"Ielema #0" = 10
	"Peniuto #0" = 10
	"Falima #0" = 10
	"Foua #0" = 10
}

leader_names = {
		Taulu "Te Fuli" Havaiki Semisi Taeo Toloa
}

ship_names = {
		Aliki Fakaofo "Fenua Fala" "Te Loto" "Fenua Loa" "Te Lafu" Mulifenua
}

fleet_names = {
	"King's Canoes"
}