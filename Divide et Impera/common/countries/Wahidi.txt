#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 213  255  81 }

historical_ideas = { bellum_iustum
	national_conscripts
	superior_seamanship
	military_drill
	naval_fighting_instruction
	divine_supremacy
	battlefield_commisions
	shrewd_commerce_practise
	excellent_shipwrights
	merchant_adventures
	national_bank
	bureaucracy
	grand_navy
}

historical_units = {
	
muslim_footsoldier
muslim_shamshir
mamluk_duel
tofongchis_musketeer
muslim_sturm
durrani_rifled_musketeer
ali_bey_reformed_infantry
persian_rifle
cavalry_archers
swarm
new_swarm
new_steppe
durrani_swivel
durrani_dragoon w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Salih #0 al-Wahidi" = 30
	"al-Hadi #0 al-Wahidi" = 20
	"al-Hasan #0 al-Wahidi" = 10
	"al-Husayn #0 al-Wahidi" = 40
	"Sa'id #0 al-Wahidi" = 10
	"Ahmad #0 al-Wahidi" = 30
	"Abd Allah #0 al-Wahidi" = 40
	"Nasir #0 al-Wahidi" = 30
	"'Umar #0 al-Wahidi" = 10
	"Muhsin #0 al-Wahidi" = 20
	"'Ali #0 al-Wahidi" = 40
	"'Alawi #0 al-Wahidi" = 20
	"Talib #0 al-Wahidi" = 10
}

leader_names = {
	"ibn Nasir" "ibn Salih" "ibn al-Hadi" "ibn al-Hasan" "ibn Ahmad" "ibn al-Husayn" "ibn 'Ali" "ibn 'Umar" "ibn 'Abd Allah" "ibn Muhammad" "ibn Talib" "ibn Muhsin"
}

ship_names = {
	"Ali ibn Rasul" "Mansur Umar" Zaizz Zabid
	"Muzaffar Yusuf" "Malik al-Mujahid" 
	"Shaykh Uthman" "Al-Kawd" Whaqra "Wadi Bana"
	Ta'izz "At-Tubah" Dhubab "Jabal Kharaz" Musaymir
	Mawiyah "Jabal al-Hasha" Ibb Qa'tabah "Jabal Thamar"
}
