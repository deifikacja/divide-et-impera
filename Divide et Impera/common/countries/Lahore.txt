#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 187  59  56 }

historical_ideas = { bellum_iustum
	national_conscripts
	glorious_arms 
	grand_army
	military_drill
	battlefield_commisions 
	shrewd_commerce_practise
	engineer_corps
	merchant_adventures
	national_trade_policy
	espionage
	smithian_economics
	cabinet
}

historical_units = {
	indian_footsoldier
	rajput_hill_fighters
	indian_arquebusier
	mughal_musketeer
	mughal_mansabdar
	indian_elephant
	indian_shock_cavalry
	rajput_musketeer
	reformed_mughal_musketeer
	reformed_mughal_mansabdar
	sikh_hit_and_run
	sikh_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Jassa Ahuwalia #0" = 10
	"Lahina #0" = 20
	"Ranjit #0" = 20
	"Bhag #0" = 10
	"Kharak #0" = 10
	"Duleep #0" = 10
	"Nao Nehal #0" = 5
	"Maha #0" = 5
	"Sobha #0" = 5
	"Narnak #0" = 5
	"Angad #0" = 5
	"Chand Kaur #0" = -5
	"Ram #0" = 5
	"Sher #0" = 5
	"Gobind #0" = 5
	"Rai #0" = 5
	"Krishan #0" = 5
	"Bahadur #0" = 5
	"Sardar Badar #1" = 1
	"Balbir #0" = 3
	"Bhalla #0" = 3
	"Daya #0" = 3
	"Dharem #0" = 3
	"Fakir #0" = 3
	"Gahunia #0" = 3
	"Gian #0" = 3
	"Granth #0" = 3
	"Himmat #0" = 3
	"Hoora #0" = 3
	"Ishnar #0" = 3
	"Jhol #0" = 3
	"Jhotti #0" = 3
	"Lohaar #0" = 3
	"Mokham #0" = 3
	"Nand #0" = 3
	"Parmanand #0" = 3
	"Rajsingh #0" = 3
	"Sanlib #0" = 3
	"Sardul #0" = 3
	"Sher #0" = 3
	"Shergill #0" = 3
	"Sodhi #0" = 3
	"Sohal #0" = 3
	"Tara #0" = 3
	"Udasi #0" = 3
}

leader_names = {
	Bahadur
	Bhalla
	Das
	Dev
	Gahunia
	Gobind
	Granth
	Hoora
	Jhol
	Jhotti
	Krishan
	Lohaar
	Parmanand
	Rajsingh
	Shergill
	Singh
	Sodhi
	Sohal
	Rai
	Udasi
}

ship_names = {
	Beas Chenab Devi Gayatri Guru Indus
	Jalsena Jhelum Lakshmi Nausena
	"Nav ka Yudh" Parvati Radha Ratri
	Ravi Sagar Sutlej Sarasvati Sita
}
