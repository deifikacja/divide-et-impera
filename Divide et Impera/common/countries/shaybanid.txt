#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 89  134  166 } 

historical_ideas = { bellum_iustum
        national_conscripts
	military_drill
        grand_army
	glorious_arms
	divine_supremacy
	merchant_adventures
	shrewd_commerce_practise
	battlefield_commisions
	national_trade_policy
	engineer_corps
	espionage
}

historical_units = {
yuan_2nd_category
mawarannahr_cavalry
babur_cavalry
crimean_swarm
westernized_raiders
mongol_arch
tartar_militia
mongol_gunpowder_warfare
mongol_reformed_infantry
persian_footsoldier
persian_cavalry_charge
persian_shamshir
qizilbash_cavalry
tofongchis_musketeer
topchis_artillery
afsharid_reformed
afsharid_reformed_infantry
muslim_mass_infantry
muslim_dragoon
persian_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Toqtamish #0" = 20
	"Chekre #0" = 20
	"Daylat Shaykh #0" = 20
	"Abu'l Khayr #0" = 20
	"Haidar Sultan #0" = 40
	"Muhammad Shaybani #0" = 20
	"Kochkunju Khan #0" = 20
	"Muzaffar-ud-Din Abu-Sa'id #0" = 20
	"Ubayd'Allah Khan #0" = 40
	"Abd Allah #0" = 20
	"Abu-al Aziz Khan #0" = 20
	"Nawruz Ahmad Khan #0" = 20
	"Pir Muhammad #0" = 20
	"Iskandar Bahadur #2" = 40
	"Abd al-Mu'min #0" = 20
	"Yar Muhammad #0" = 20
	"Jani Muhammad #0" = 20
	"Baqi Muhammad #0" = 20
	"Wali Muhammad #0" = 20
	"Imam Quli Bahadur #0" = 20
	"Sa'id Nadir Muhammad #0" = 20
	"Sa'id Abd al-Aziz #0" = 20
	"Sa'id Subhan Quli Bahadur #0" = 20
	"Sa'id Ubayd'Allah #1" = 40
	"Abu'l Fa'iz #0" = 20
	"Rahim Bi #0" = 20
	"Muhammad Rahim Khan #0" = 20
	"Danial Ataliq #0" = 20
	"M�r Ma'sum Shah Murad #0" = 20
	"Haidar Tura #0" = 20
}

leader_names = { 
	"Mongke Nasan" "Bora Chinua" "Shria Gal" "Boke Arslan" "Suren Unegen"
	"Arigh Gan" "Qara Temur" "Bayan Checheg" "Yeke Burilgi" 
	"Chagan Erdene" "Altan Vachir" "Mongke Enq" "Mongke Bayar" 
}

ship_names = {
	Amudarja Syrdarja Karakul Karsi Guzar
	Gulistan Cimkent Jangijul Chudzand Kerki
	Gizduvan Chiva Bajsun Mukry
}
