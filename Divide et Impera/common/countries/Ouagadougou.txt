#Country Name: Please see filename.

graphical_culture = africangfx

color = { 110  23  22 }

historical_ideas = {
	shrewd_commerce_practise
	national_conscripts
	merchant_adventures
	military_drill
	national_trade_policy
	battlefield_commisions
	engineer_corps
	grand_army
	espionage
	cabinet
}

historical_units = {
mali_archer
songhai_archer
sahel_archer
sudan_archer
niger_gunpowder_warfare
westernized_adal
adal_guerilla_warfare
mali_nomads
songhai_nomads
sudan_nomads
nubian_nomads
fulani_nomads
}

monarch_names = {
	"Naaba #0" = 100
	"Moogo-Naaba #0" = 90
}

leader_names = {
	Zana
	Gilinga
	Ubra
	Muatiba
	Warga
	Zombre
	Koom
	Saaga
	Dulugu
	Karfo
	Baongo
	Kutu
	Sawadogo
	Sanum
	Wobgo
	Sigiri
	Kugri
}

ship_names = {
	Sisili Mouhoun Bougouriba Naznon
	Nakanbe Koulpelogo Pendjan
}
