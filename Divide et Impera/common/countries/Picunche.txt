#Country Name: Please see filename.

graphical_culture = northamericagfx

color = { 220 36 31 } 

historical_ideas = { bellum_iustum
	national_conscripts
	military_drill
        deus_vult
	church_attendance_duty
        cabinet
}

historical_units = {
	native_indian_archer
	native_indian_tribal_warfare
	algonkin_tomahawk_charge
	commanche_swarm
	native_indian_mountain_warfare
	sioux_dragoon
	american_western_franchise_warfare
}

monarch_names = {
	"Ailen" = -10
	"Ayel�n" = -10
	"Huenu" = 20
	"Lican" = -10
	"Lichuen" = 20
	"Lilen" = -10
	"Maiten" = 20
	"Malen" = -10
	"Manque" = 20
	"Nahuel" = 20
	"Nehuen" = 20
	"Neyen" = 20
	"Nulpi" = 20
	"Pehuen" = 20
	"Pichi" = 20
	"Pire" = -10
	"Raiquen" = 20
	"Rayen" = -10
	"Suyai" = -10
	"Yaco" = 20
	"Yaima" = -10
	"Yamai" = 20
	"Yenien" = 20
	"Yerimen" = 20
}

leader_names = {
}

ship_names = {
	Maule B�o-B�o
}
