#Country Name: Please seefilename.

graphical_culture = africangfx

color = { 110  255  110 }

historical_ideas = {
	shrewd_commerce_practise
	national_conscripts
	merchant_adventures
	military_drill
	national_trade_policy
	battlefield_commisions
	engineer_corps
	grand_army
	espionage
	cabinet
}

historical_units = {
mali_archer
songhai_archer
sahel_archer
sudan_archer
niger_gunpowder_warfare
westernized_adal
adal_guerilla_warfare
mali_nomads
songhai_nomads
sudan_nomads
nubian_nomads
fulani_nomads
}

monarch_names = {
	"Abdalkadir #0" = 10
	"Muxtar #0" = 10
	"Hamad #0" = 10
	"Yusuf #0" = 10
	"Bokar #0" = 10
	"Ali #0" = 10
	"Tapsir #0" = 10
	"Biran #0" = 10
	"al-Faqih #0" = 10
	"Mamadu #0" = 10
	"'Abd ar-Rahman #0" = 10
	"Amadu #0" = 10
	"Mamudu #0" = 10
	"Baabali #0" = 10
	"Sibawayhi #0" = 10
	"Cerno #0" = 10
	"Mustafa #0" = 10
	"Eliman #0" = 10
	"Raasin #0" = 10
	"Sada #0" = 10
	"Malik #0" = 10
	"Njay #0" = 10
	"Bubu #0" = 10
	"Alfa #0" = 10
}

leader_names = {
	"ibn Hamadi Torodo"
	"ibn Sire Kudeeje"
	"Lamin Baal"
	"Sire Li"
	Tambari
	"Ibraa Waan"
	"Amadu Li"
	"Mamudu 'Aan"
	"Modibo Kan"
	"Lamin Ture"
	"Jaatara 'Aan"
	"'Aan"
	"Sire Ja"
	"Baaba Li"
	"Mamudu Ja"
	Li
	"Sire Waan"
	"Mamudu Ba"
	"Demba Li"
	"Hassan Baro"
	"Sellin Talla"
	"Demba Ki"
	"Baaba Waan"
	Baal
	"Sellin Talla"
	"Mamadu Cam"
	"'Eli Baro"
	"Lamin Li"
	"Haba Li"

}

ship_names = {
	iska sama hazo gajimare "babban dutse"
	teku rairayi rana wata tauraro ruwa
	itace saiwa fure
}
