#Country Name: Please see filename.

graphical_culture = easterngfx

color = { 136  157  23 }

historical_ideas = { bellum_iustum
	national_conscripts
	grand_army
	glorious_arms
	military_drill
	espionage
	battlefield_commisions
	engineer_corps
	national_trade_policy
	high_technical_culture
	cabinet
	shrewd_commerce_practise
	national_bank
}

historical_units = {
	eastern_medieval_infantry
	eastern_knights
	slavic_stradioti
	eastern_militia
	ottoman_sekban
	ottoman_reformed_spahi
	ottoman_reformed_janissary
	ottoman_toprakli_hit_and_run
	ottoman_nizami_cedid
	ottoman_toprakli_dragoon
	eastern_skirmisher
	eastern_uhlan
	eastern_carabinier
	ottoman_new_model
	ottoman_lancer w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Alexandru #0" = 280
	"Stefan #1" = 180
	"Constantin #0" = 120
	"Mihail #0" = 80
	"Grigore #0" = 60
	"Ioan #0" = 60
	"Petru #2" = 40
	"Bogdan #1" = 40
	"Antioh #0" = 40
	"Ilias #0" = 40
	"Scarlat #0" = 40
	"Stefanito #0" = 40
	"George #0" = 40
	"Petru #2 Aron" = 20
	"Aron #0" = 20
	"Dimitrie #0" = 20
	"Dumitrascu #0" = 20
	"Emanuel #0" = 20
	"Eustratius #0" = 20
	"Gaspar #0" = 20
	"George Stefan #0" = 20
	"Iancu #0" = 20
	"Ilias Alexandru #0" = 20
	"Ioan #0 Iacob" = 20
	"Ioan Teodoru #0" = 20
	"Iuga #0" = 20
	"Jeremie #0" = 20
	"Matvei #0" = 20
	"Milos #0" = 20
	"Moise #0" = 20
	"Nicolae #0" = 20
	"Radu #0" = 20
	"Simeon #0" = 20
	"Vasile #0" = 20
	"Ioan #0 Bogdan" = 15
	"Sergei #0" = 10
	"Roman #2" = 5
	"Andrei #0" = 3
	"Iulian #0" = 3
	"Mircea #0" = 3
}

leader_names = {
	Callimachi Campineanu Cantacuzino Cantemir Caragea Craiovescu
	Duca
	Ghica
	Ilias
	Lupu
	Mavrocordat Moruzi Movila Musatin
	Norocea 
	Racovita Ruset                         
	Sturdza Sutu
	Voda
}

ship_names = {
	"Athleta Christi"
	Baia "Bogdan I"
	Iuga
	Moldova
	"R�ul Prut"
	"Stefan cel Mare" Suceava
	"T�rgul Moldovei"
	Vaslui
}

army_names = {
	"Armata de $PROVINCE$" 
}