#Country Name: Please see filename.

graphical_culture = latingfx

color = { 184  66  25 }

historical_ideas = { 
	early_mining
	church_attendance_duty
	merchant_adventures
	patron_of_art
	military_drill
	national_bank
	national_conscripts
	national_trade_policy
	cabinet
	smithian_economics
	scientific_revolution
}

historical_units = {
	#Infantry German
	w_i_armed_peasantry
	w_i_longbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_hungarian_formation
	w_i_spanish_formation
	w_i_chosen_formation
	w_i_swedish_formation
	w_i_quarter_army
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_prussian_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon 
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Henryk #2" = 20
	"Bolko #2" = 20
	"Bernard #2" = 20
	"Herman #1" = 20
	"Przemys�aw #0" = 20
	"Siemowit #1" = 10
	"Boles�aw #1" = 20
	"Wac�aw #0 cieszy�ski" = 10
	"Fryderyk #0" = 30
	"Fryderyk Kazimierz #0" = 10
	"Fryderyk Wilhelm #0" = 10
	"Adam #0" = 10
	"Adam #0 Wac�aw" = 5
	"Wac�aw #0 Adam" = 5
	"El�bieta #0" = -5
	"Agnieszka #0" = -10
	"Jadwiga #0" = -5
	"Aleksandra #0" = -5
	"Karol #0" = 1
	"J�zef #0" = 1
	"Franciszek #0" = 1
	"Maria #0" = -1
	"Albert #0" = 1
	"Albrecht #0" = 1
	"Jan #0" = 1
	"Katarzyna #0" = -1
	"Ma�gorzata #0" = -1
	"Eufemia #0" = -1
}

leader_names = { 
	Ko�czycki Zbytkowski B�kowski Kozakowski Hermanowski Grodziski Bielski 
	Jab�onkowski Skoczowski Cieszy�ski Kazimierzowic Mieszkowic Przemy�lid Boles�awowic Trzyniecki Trzy�ski Goleszewski
}

ship_names = {
	Strumie� Bogumin Bielsk Frysztad Brennica Bystra Wapienka Jasienica I�owica 
	Aleksandra Jadwiga Lomna Szaniawka Turawka Wis�a Olza Skocz�w Jab�onk�w �widnica Jawor "Ksi�na �widnicy" "Z�oty Orze�" "Silesia" "Henryk V"
}
