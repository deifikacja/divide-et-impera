#Country Name: Please see filename.

graphical_culture = indiangfx

color = { 149  191  117 }

historical_ideas = {
	national_conscripts
	grand_army
	glorious_arms
	military_drill
	battlefield_commisions
	shrewd_commerce_practise
	merchant_adventures
	engineer_corps
	espionage
	national_trade_policy
	national_bank
	cabinet
}

historical_units = {
	indian_footsoldier
	rajput_hill_fighters
	indian_arquebusier
	mughal_musketeer
	mughal_mansabdar
	indian_elephant
	indian_shock_cavalry
	rajput_musketeer
	reformed_mughal_musketeer
	reformed_mughal_mansabdar
	indian_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Pengshiba #0" = 10
	"Ningthou Khomba #0" = 10
	"Senbi Kiyaamba #0" = 10
	"Koiremba #0" = 10
	"Chingkhong Lamgai Ngamba #0" = 10
	"Nongyin Phaaba #0" = 10
	"Senbi Khomba #0" = 10
	"Taangjaamba #0" = 10
	"Chalaamba #0" = 10
	"Munghaamba #0" = 10
	"Khagemba #0" = 10
	"Khunjaoba #0" = 10
	"Paikhomba #0" = 10
	"Charairongba #0" = 10
	"Paamheiba #0" = 10
	"Chit Sai #0" = 10
	"Bhorot Sai #0" = 10
	"Maraamba #0" = 10
	"Chingthang Khomba #0" = 10
	"Labeinachandra #0" = 10
	"Maramaba #0" = 10
	"Madhuchandra #0" = 10
	"Chourjit #0" = 10
	"Marjit #0" = 10
}

leader_names = {
	Shubol
	Chandrakirti
	Ganavira
	Marjit
	Charajit
	Pitambara
	Moduchandra
	Harshachandra
	Nara
	Devendra
}

ship_names = {
	"Chingri Mach"  Devi "Durga Ma" Ganga
	"Hilsa Mach" "Ilish Mach" Imphal Iril
	Jalangi Machli "Kali Ma" "Kali Meghana"
	"Kali Nauk" Kongba Kushiari "Lal Nauk"
	"Loki Devi" "Manasa Devi" Meghna Nambul
	"Nil Nauk" "Nil Sagor" Padma "Parvati Devi"
	"Rui Mach" Saraswoti "Sagorer Bahini"
	Shakti "Shaktir Nauk" "Shasti Ma"
	Surma Trishool "Uma Devi"
}    
