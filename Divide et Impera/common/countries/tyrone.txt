# Country name see file name.

graphical_culture = latingfx

color = { 131  101  85 }

historical_ideas = { 
	feudal_castles
	excellent_shipwrights
	bill_of_rights
	superior_seamanship
	merchant_adventures
	naval_glory
	cabinet
	national_bank
	espionage  
	smithian_economics
	scientific_revolution
	sea_hawks
	grand_navy
}

historical_units = {
#Infantry Britain
	w_i_armed_peasantry
	w_i_longbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_british_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon 
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

leader_names = {
	"Mac Anna" "Mac �rdghail" "Mac Artian" "Mac Cearbhaill" "Mac Dhomhnaill"
	"Mac Lochlainn" "Mac Mathghamha" "Mac Suibhne" "Mag Aonghusa" "Mag Uidhir"
	"� Banain" "� Baoghill" "� Canannain" "� Caoinnigh" "� Caiside" "� Ceallaigh"
	"� Cleireigh" "� Corcr�in" "� Cuileann�in" "� Cuinn" "� Duibheamhna"
	"� Daimh�n" "� Dochartaigh" "� Doibhilin" "� Duach�in" "� Farach�in"
	"� fLaithbheartaigh" "� Flaithre" "� Flannag�in" "� hAodhag�in" "� hAnluain"
	"� h�ignigh" "� hInneirghe" "� h�r" "� Long�in" "� Lorc�in" "� Maolag�in"
	"� Maold�in" "� Neill" "� Raithbheartiagh"
}

monarch_names = {
	"Donal #12" = 40
	"Art #0" = 40
	"Conn #0" = 40
	"Einr� #0" = 40
	"�ed #13" = 20
	"Niall #6" = 20
	"Brian #2" = 20
	"Eoghan #1" = 20
	"S�an #0" = 20
	"Turlough #0" = 20
	"Alaois #0" = 3
	"Alastar #0" = 3
	"Aonghus #0" = 3
	"Brad�in #0" = 3
	"Brennan #0" = 3
	"Br�nach #0" = 3
	"Calbhach #0" = 3
	"Caolan #0" = 3
	"Carroll #0" = 3
	"Ceallach #0" = 3
	"Caerbhall #0" = 3
	"Cian #0" = 3
	"Conan #0" = 3
	"Connor #0" = 3
	"Diarmaid #0" = 3
	"Dubghall #0" = 3
	"�amonn #0" = 3
	"F�el�n #0" = 3
	"Fearchar #0" = 3
	"Fearghas #0" = 3
	"Fionnlagh #0" = 3
	"Flann #0" = 3
	"Gallchobhair #0" = 3
	"Gob�n #0" = 3
	"Gr�daigh #0" = 3
	"Lomm�n #0" = 3
	"Maeleachlain #0" = 3
	"Murchadha #0" = 3
	"Naomh�n #0" = 3
	"Torin #0" = 3
}

ship_names = {
	Aedh
	Baedan
	Colman Cookstown 
	Donal
	Eocha Eogan 
	Fergal Fergus
	Niall
	Omagh
	Strabane Suibne
}
