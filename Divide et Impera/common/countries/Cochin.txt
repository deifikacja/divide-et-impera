#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 255  66  132 }

historical_ideas = { naval_transport
	national_conscripts
	glorious_arms  
	grand_army
	military_drill
	patron_of_art
	merchant_adventures
	battlefield_commisions
	shrewd_commerce_practise
	engineer_corps
	national_trade_policy
	smithian_economics
	cabinet
}

historical_units = {
	indian_footsoldier
	rajput_hill_fighters
	indian_arquebusier
	mughal_musketeer
	mughal_mansabdar
	indian_elephant
	indian_shock_cavalry
	rajput_musketeer
	reformed_mughal_musketeer
	reformed_mughal_mansabdar
	indian_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Cheraman" = 10
	"Veerakerala" = 10
	"Unniraman #2" = 20
	"Veera Kerala" = 90
	"Keshava Rama" = 10
	"Ravi" = 90
	"Godavarma" = 10
	"Verarayira" = 10
	"Rama" = 90
	"Rani" = -10
	"Goda" = 10
}

leader_names = {
	Varma Perumal Koykial Gangadharalakshmi "Shakthan Thapuran"
}

ship_names = {
	Alia Arusa Jalsena
	Ganges
	Nausena "Nav ka Yudh" Niknaz
	Noor Spiti
	Suhalia Yamuna
}
