#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 140  169  183 }

historical_ideas = { feudal_castles
	national_conscripts
	merchant_adventures
	battlefield_commisions
	espionage
	military_drill
	glorious_arms
	national_trade_policy
	patron_of_art
	shrewd_commerce_practise
	grand_army
	engineer_corps
	bureaucracy
}

historical_units = {
	chinese_longspear
	eastern_bow
	chinese_footsoldier
	chinese_steppe
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	manchu_banner
	han_banner
	asian_musketeer
	chinese_dragoon
	reformed_asian_musketeer
	reformed_asian_cavalry
	reformed_manchu_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Anwang #0" = 40
	"Calwang #0" = 40
	"Congwang #0" = 40
	"Congwei #0" = 40
	"Congzhi #0" = 40
	"Dewang #0" = 40
	"Dewei #0" = 40
	"Deyu #0" = 40
	"Jiping #0" = 40
	"Jiwang #0" = 40
	"Jungwang #0" = 40
	"Jungyu #0" = 40
	"Kaiwang #0" = 40
	"Liwang #0" = 40
	"Wanggui #0" = 40
	"Wangwei #0" = 40
	"Wangyi #0" = 40
	"Xingwang #0" = 40
	"Yongwang #0" = 40
	"Zhengde #0" = 40
	"Zhengping #0" = 40
	"Zhengwang #0" = 40
	"Akjan #0" = 20
	"Encehen #0" = 20
	"Mergen #0" = 20
	"Jabsan #0" = 20
	"Yelu #0" = 20
	"Ayan #0" = 3
	"Baohu #0" = 3
	"Chengjun #0" = 3
	"Dorgide #0" = 3
	"Eje #0" = 3
	"Fisin #0" = 3
	"Fulu #0" = 3
	"Ganyong #0" = 3
	"Ganzhan #0" = 3
	"Hurong #0" = 3
	"Jakdan #0" = 3
	"Leli #0" = 3
	"Liyi #0" = 3
	"Muduri #0" = 3
	"Mujin #0" = 3
	"Oori #0" = 3
	"Radi #0" = 3
	"Talman #0" = 3
	"Wehe #0" = 3
	"Yongyi #0" = 3
	"Zhanhung #0" = 3
	"Zhenghu #0" = 3
	"Zhengrong #0" = 3
	"Zhongcheng #0" = 3
	"Zhongchun #0" = 3
	"Zhongho #0" = 3
	"Zhonghu #0" = 3
	"Zhonghung #0" = 3
	"Zongqian #0" = 3
	"Zhongxian #0" = 3
	"Zhongyong #0" = 3
	"Zhongyu #0" = 3
}

leader_names = {
	Hao
	Song
	Xuan
	Menglin
	Zongyan
	Yan
	Lin
	Ting
	Shixian
	Mingtai
}

ship_names = {
	Gaodi Huidi Wendi Zhaodi Zhangdi "Da Ming Guo"
	Beijing "Zeng He" Ji'nan Nanjing Hangzhou 
	Wuchang Fuzhou Nanchang Guilin Guangzhou
	Guiyang Yunnan Chendo Xi'an Taiyuan "Huang He"
	"Chang Jiang" "Xijiang" "Kung-fu tzu" Mencius
	"Xun Zi" "Yan Hui" "Min Sun" "Ran Geng" "Ran Yong"
	"Zi-you" "Zi-lu" "Zi-wo" "Zi-gong" "Yan Yan"
	"Zi-xia" "Zi-zhang" "Zeng Shen" "Dantai Mieming"
	"Fu Buji" "Yuan Xian" "Gungye Chang" "Nangong Guo"
	"Gongxi Ai" "Zeng Dian" "Yan Wuyao" "Shang Zhu"
	"Gao Chai" "Qidiao Kai" "Gongbo Liao" "Sima Geng"
	"Fan Xu" "You Ruo" "Gongxi Chi" "Wuma Shi" "Liang Zhan"
	"Yan Xing" "Ran Ru" "Cao Xu" "Bo Qian" "Gongsun Long"
}
