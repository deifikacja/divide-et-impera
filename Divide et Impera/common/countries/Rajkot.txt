#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 3 3 128 }

historical_ideas = { feudal_castles
	national_conscripts
	glorious_arms 
	grand_army
	military_drill
	battlefield_commisions 
	shrewd_commerce_practise
	engineer_corps
	merchant_adventures
	national_trade_policy
	espionage
	smithian_economics
	bureaucracy
}

historical_units = {
	indian_footsoldier
	indian_archers
	indian_arquebusier
	mughal_musketeer
	mughal_mansabdar
	indian_elephant
	indian_shock_cavalry
	rajput_musketeer
	reformed_mughal_musketeer
	reformed_mughal_mansabdar
	indian_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Vibhaji #1" = 10
	"Mehramanji #2" = 40
	"Sahebji #1" = 10
	"Bamaniaji #1" = 10
	"Ranmalji #2" = 20
	"Lakhaji #1" = 10
	"Surajji #0" = 10
	"Bavajirajsinhji #0" = 10
	"Lakhajirajsinhji #1" = 20
	"Dharmendrasinhji #0" = 10
	"Pradyumansinhji #0" = 10
}

leader_names = {
	"Thakur Shri Sahib"
	Babber
	Bargujars
	Bhoja
	Chandelas
	Chauhan
	Jadeja
	Kachwaha
	Karmavati
	Khumba
	Paramaras
	Pratap
	Rathore
	Singh
	Tomaras
	Mihirbhoj
	Mahipala
	Nagabhatta
	Padmini
	Vaghela
	Vatsraja
}

ship_names = {
	Chambal Devi Durga Gayatri Ghaggar
	Godwar Jalsena "Kala Jahazi" Lakshmi
	"Lal Jahazi" Luni Nausena "Marwar ka Nav"
	"Merwar ka Nav" "Nav ka Yudh" "Nila Jahazi"
	Parvati Radha Rani Ratri Sagar Sarasvati
	Sekhawati Sita
}
