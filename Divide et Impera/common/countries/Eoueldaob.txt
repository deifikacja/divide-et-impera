#Country Name: Please see filename.

graphical_culture = africangfx

color = { 3 3 128 }

historical_ideas = { naval_transport
	merchant_adventures
	excellent_shipwrights
	shrewd_commerce_practise
	superior_seamanship
	national_conscripts
	national_trade_policy
	sea_hawks
	naval_fighting_instruction
	military_drill
	naval_glory
	battlefield_commisions
	engineer_corps
}

historical_units = {
	polynesian_daggermen
	polynesian_spearmen
	polynesian_clubmen
	polynesian_javelinmen
	polynesian_long_clubmen
	polynesian_shark_tooth
	polynesian_two_blade
	polynesian_throwing_axe
	polynesian_lua
	polynesian_slingermen
	polynesian_rifles
}

monarch_names = {                         
	"Ngiraidid #1" = 10
	"Kingsos #1" = 10
	"Ngiratachadong  #1" = 10
	"Meang #0" = 10
	"Ngirachosarech #0" = 10
	"Meresou #0" = 10
	"Ngirchokebai #0" = 10
	"Louch #0" = 10
	"Tem #0" = 10
	"Ngiraked #0" = 10
	"Ngoriakl #0" = 10
	"Yutaka #0" = 10
}

leader_names = {
		 Chorot Merikl Salii
}

ship_names = {
		Ibedul Eoueldaob
}

fleet_names = {
	"King's Canoes"
}