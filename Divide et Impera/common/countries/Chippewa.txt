#Country Name: Please see filename.

graphical_culture = northamericagfx

color = { 100  3 3 }

historical_ideas = {
	humanist_tolerance
	national_conscripts
	battlefield_commisions
	grand_army
	national_trade_policy
	military_drill
	cabinet
	engineer_corps
	bill_of_rights
	patron_of_art
	espionage
	smithian_economics
}

historical_units = {
	native_indian_archer
	native_indian_tribal_warfare
	algonkin_tomahawk_charge
	native_indian_horsemen
	huron_arquebusier
	commanche_swarm
	native_indian_mountain_warfare
	american_western_franchise_warfare
}

monarch_names = {
	"Awegen" = 20
	"Abedabun" = 20
	"Abequa" = 20
	"Abeque" = 20
	"Ahmik" = 20
	"Ayasha" = 20
	"Ayashe" = 20
	"Keezheekoni" = 20
	"Kiwidinok" = 20
	"Meoquanee" = 20
	"Migisi" = 20
	"Namid" = 20
	"Nokomis" = 20
	"Odahingum" = 20
	"Ominotago" = 20
	"Sheshebens" = 20
	"Canajoharie" = 10
	"Deseroken" = 10
	"Gweugwehono" = 10
	"Hostayuntwa" = 10
	"Kawauka" = 10
	"Kente" = 10
}

leader_names = {
}

ship_names = {
	Atahensic Ataensic Adekagagwaa
	Gaol Gohone Gah-oh Gendenwitha
	Hahgwehdiyu Hahgwehdaetgan "Ha Wen Neyu"
	Jogah
	Losheka
	Onatha Oki
	Sosondowah
	Tarhuhyiawahku
	Yosheka Ya-o-gah
}
