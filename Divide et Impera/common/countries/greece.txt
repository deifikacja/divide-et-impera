#Country Name: Please see filename.

graphical_culture = easterngfx

color = { 249  206  173 }

historical_ideas = { liberal_arts
	church_attendance_duty
	national_conscripts
	espionage
	military_drill
	shrewd_commerce_practise
	engineer_corps
	merchant_adventures
	national_trade_policy	
	grand_army
	national_bank
	cabinet
	bureaucracy
}

historical_units = {
	eastern_medieval_infantry
	eastern_knights
	slavic_stradioti
	eastern_militia
	ottoman_spahi
	ottoman_sekban
	ottoman_reformed_spahi
	ottoman_reformed_janissary
	ottoman_toprakli_hit_and_run
	ottoman_nizami_cedid
	ottoman_toprakli_dragoon
	eastern_skirmisher
	eastern_uhlan
	eastern_carabinier
	ottoman_new_model
	ottoman_lancer w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Georgios #0" = 40
	"Alexandros #0" = 20
	"Konstantinos #0" = 20
	"Pavlos #0" = 15
	"Andreas #0" = 10
	"Alexandra #0" = -10
	"Nikolaos #0" = 10
	"Aineias #0" = 3
	"Alexeios #0" = 3
	"Ambrosios #0" = 3
	"Bartholomaios #0" = 3
	"Basileios #0" = 3
	"Chrysanthos #0" = 3
	"David #0" = 3
	"Demetrios #0" = 3
	"Diogenes #0" = 3
	"Dionysos #0" = 3
	"Eirenaios #0" = 3
	"Elpidios #0" = 3
	"Gregorios #0" = 3
	"Hektor #0" = 3
	"Herakles #0" = 3
	"Iason #0" = 3
	"Ioannes #0" = 3
	"Kastor #0" = 3
	"Manuel #0" = 3
	"Matthaios #0" = 3
	"Nestor #0" = 3
	"Nikomedes #0" = 3
	"Orestes #0" = 3
	"Palaemon #0" = 3
	"Pelagios #0" = 3
	"Petros #0" = 3
	"Philemon #0" = 3
	"Philippos #0" = 3
	"Prokopios #0" = 3
	"Skantarios #0" = 3
	"Stefanos #0" = 3
	"Theodoros #0" = 3
	"Thomas #0" = 3
}

leader_names = {
	Argyrus Artavasdus
	Basileus
	Diogenes Ducas
	Monomachus
	Romanus
	Stauracius Stratioticus
	Vataces
	Zimisces
}

ship_names = {
	Hydra
	Lesbos Limnos
	Mikonios
	Pezopoulos
	Salamis Samos Simitzopoulos
	Themistoklis
	Votsis
}
