#Kurland

graphical_culture = latingfx

color = { 99  137  153 }

historical_ideas = { feudal_castles
	merchant_adventures
	shrewd_commerce_practise
	national_conscripts
	glorious_arms
	national_trade_policy
	military_drill
	cabinet
	national_bank
	grand_army
	engineer_corps
	battlefield_commisions
	espionage
}

historical_units = {
	#Infantry German
	w_i_armed_peasantry
	w_i_longbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_hungarian_formation
	w_i_spanish_formation
	w_i_chosen_formation
	w_i_swedish_formation
	w_i_quarter_army
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_prussian_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Ernst Johann #0" = 20
	"Ferdinand #0" = 20
	"Friedrich #0" = 20
	"Friedrich Casimir #0" = 20
	"Friedrich Wilhelm #0" = 20
	"Gotthard #0" = 20
	"Jakob #0" = 20
	"Karl #0" = 20
	"Peter #0" = 20
	"Johann #0" = 15
	"Karl Ernst #0" = 15
	"Ladislas Friedrich #0" = 15
	"Alexander #0" = 10
	"Karl Jakob #0" = 10
	"Leopold Karl #0" = 10
	"Adolf #0" = 3
	"Alfred #0" = 3
	"Benedikt #0" = 3
	"Denis #0" = 3
	"Diederick #0" = 3
	"Erwin #0" = 3
	"Franz #0" = 3
	"Gebhard #0" = 3
	"Heinrich #0" = 3
	"Joachim #0" = 3
	"Kaspar #0" = 3
	"Kurt #0" = 3
	"Leo #0" = 3
	"Ludwig #0" = 3
	"Manfred #0" = 3
	"Moritz #0" = 3
	"Nikolaus #0" = 3
	"Oskar #0" = 3
	"Otto #0" = 3
	"Philipp #0" = 3
	"Reiner #0" = 3
	"Rupprecht #0" = 3
	"Siegmund #0" = 3
	"Urs #0" = 3
	"Werner #0" = 3
}

leader_names = {
        "von Altenbockum" Ascheberg
        "von Bach" Behr Bilderling Blomberg "von Boetticher" Borch Brincken  Br�ggen Buchholtz
        Derschau Drachenfels D�sterlohe
        Engelhardt
        Fircks F�lkersam Funck
        Grotthuss
        Haaren Hahn Heyking "von Hoerner" Howen
        Kleist Klopmann Komorowski Koskull
        Lambsdorff "von der Launitz" "von Lysander"
        Mirbach
        Nettelhorst Nolcken
        Oelsen Offenberg Osten-Sacken
        Pahlen
        Raczynski Recke Ropp Rosenberg "von Rummel"
        Schlippenbach "von Schroeders" Seefeld Stackelberg Stromberg
        Taube "von Timroth"
}

ship_names = {
	Blumentopf Dame "Das Wappen der Herzogin von Kurland" "Drei Heringe"
	Hoffnung Krokodil "Lynn kurl�ndische M�we" D�na Aa Bruno Windau Mitau
	Goldingen Seeburg Hasenpoth Tukums "von Gr�ningen" "von Stierland"
	"von Sangershausen" "von Hornhausen" "von Breithausen" "von Mandern"
	"von Lutterberg" "von Nortecken" "von Rassburg" "von Feuchtwangen"
	"von Endorp" "Herzogenstein" "von Hohembach" "von dinkelaghe" "von Rogga"
	"von Jocke" "Johannes Ungenade" "Reimar Hane" "von Monheim" "von Dreileben"
	"von Hercke" "von Vietinghof" "von Vrymersheim" "von Eltz" "von B�rggeneye"
	"Diderick Tork" "von Spanheim" "von Rutenberg" "Franco Kerskorff"
	"von Bockenvorde" "von Overbergen" "von Mengede" "von Herse" "von der Borch"
	"von Loringhofe" "von Plettenberg" "von der Recke" "von Galen" "von F�rstenberg"
}
