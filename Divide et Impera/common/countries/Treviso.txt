#Country Name: Please see filename.

graphical_culture = latingfx

color = { 255 3 5 }

historical_ideas = { bellum_iustum
	patron_of_art
	merchant_adventures
	shrewd_commerce_practise
	national_trade_policy
	national_bank
	national_conscripts
	military_drill
	engineer_corps 
	smithian_economics
	scientific_revolution
	espionage
	battlefield_commisions
}

historical_units = {
#Infantry Italy
	w_i_armed_peasantry
	w_i_crossbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_condottiere
	w_i_halberdiers
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_french_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon 
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Enrico #0" = 10
	"Guacello #1" = 10
	"Matilde #0" = -10
	"Ezzelino #3" = 30
	"Giovanni #0" = 10
	"Rizzardo #1" = 10
	"Ludovico #0" = 10
	"Francisco #1" = 10
	"Alberto #0" = 10
	"Bartolomeo #0" = 10
	"Gherardo #1" = 10
	"Leopold #3" = 30
	"Paolo #0" = 10
	"Rizzardo #1" = 10
	"Antonio #0" = 10
	"Frederico #3" = 30
	"Bruno #0" = 10
}

leader_names = {
        Accarigi Alboin Alighieri Amelia Barone Barzagli Borghese Borgia
        Castro Cecchereni Compagnoni Damiani "Di Stefano"
	Fossombroni Galilei Gallo Gentile "Golia di Siena" Griccioli
	Grosso Guerra Idoni Labriola Nardi Neri Oddo Orlando Palumbo
	Pannocchieschi Petrucci Pianella Piccolomini "del Piero" Pirlo "da Porcari"
	Quercegrossa Riario "de Rossi" Salimbeni "da Sangallo" Segni "da Staggia"
	Testa Torricelli Uccello Vagliagli Visconti Vitali Viviani Zaccardo  Zeti
}

ship_names = {
	"Ambrogio Lorenzetti" Asciano
	"Bartolomeo Guidi" "Bonaguida Lucari"
	"Carlo IV" "Castelnuovo Berardenga" Chianti  Costalpino "Crete Senesi"
	Donatello "Duccio di Buoninsegna" 
	Elsa
	"fiumi Arbia" "Francesco di Rinaldo"
	"Ghibellini" "Giovanni di Balduccio"
	"Isola d'Arbia" 
	Merse "Minuccio di Rinaldo" "Misser santo Micchele arcangelo" Montagnola
	Montaperti Monteriggioni "Monteroni d'Arbia"
	"Orlando Bonsignori"
	"San Bernardino" "San Domenico" "San Francesco" "San Galgano"
	"San Giovanni" "San Miniato" "Sant'Ansano" "Santa Caterina"
	"Santa Maria dei Servi" "Santissima Annunziata" Senius Sovicille 
	"Taverne d'Arbia" Lamborghini
}
