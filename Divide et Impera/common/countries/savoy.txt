#Country Name: Please see filename.

graphical_culture = latingfx

color = { 235  196 231 }

historical_ideas = { 
	feudal_castles
	national_conscripts
	patron_of_art
	shrewd_commerce_practise
	merchant_adventures
	national_trade_policy
	national_bank
	cabinet
	military_drill
	engineer_corps
	scientific_revolution
	espionage
	bureaucracy
}

historical_units = {
	#Infantry Italy
	w_i_armed_peasantry
	w_i_crossbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_condottiere
	w_i_halberdiers
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_french_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Carlo #0" = 60
	"Amedeo #7" = 40
	"Carlo Emanuele #0" = 40
	"Filiberto #0" = 40
	"Vittorio Amedeo #0" = 40
	"Filippo #1" = 20
	"Emanuele Filiberto #0" = 20
	"Francesco Giacinto #0" = 20
	"Ludovico #0" = 20
	"Antonio #0" = 15
	"Bernardo #0" = 15
	"Filippo Emanuel #0" = 15
	"Giacomo Ludovico #0" = 15
	"Pietro #2" = 10
	"Tomasso #2" = 10
	"Adriano #0" = 10
	"Aimone #0" = 10
	"Assolone #0" = 10
	"Emanuele #0" = 10
	"Francesco #0" = 10
	"Giacomo #0" = 10
	"Gian Claudio #0" = 10
	"Gian Maria #0" = 10
	"Giano #0" = 10
	"Giovanni Amedeo #0" = 10
	"Giovanni Ludovico #0" = 10
	"Girolamo #0" = 10
	"Janus #0" = 10
	"Maurizio #0" = 10
	"Umberto #3" = 5
	"Agostino #0" = 3
	"Bonaventura #0" = 3
	"Edmondo #0" = 3
	"Gennaro #0" = 3
	"Giulio #0" = 3
	"Lazzaro #0" = 3
	"Lorenzo #0" = 3
	"Nestore #0" = 3
	"Sebastiano #0" = 3
	"Vittorio #0" = 3
}

leader_names = {
        Alfieri "d'Appiani d'Aragona" "Arborio Mella" Asinari "d'Attalaya"
	Balbo Benso "Boncompagni Ludovisi" Borromeo Bosco Botta Broglia "di Busca"
	"di Cadoval" Cafasso "Cantatore di Breo" "di Card�" Casati Chiodo "Cisa Asinari" Cottolengo
	"Doria di Ciri�" Ferrero-Fiesco Fiesco Gaetani "Gaetani dell'Aquila" Galletti Gioberti
	Lascaris "Luserna di Bonifacio" "Luserna di Campiglione" "Luserna di Chiaberto" "di Luserna"
	"del Marco" "de Maretta" Melzi Micca "del Monte" d'Ottaiano
	"di Paesana" "Paleologi Montferrato" Pallavicini "di Saluzzo" "di Savoia" Solaro
	Taparelli "della Torre" Valdo Valfr� Vico
}

ship_names = { 
	"Adelaide di Susa" Albane Aimone "Amedeo Coda" Annecy
	Bauges Bellevaux Biancamano Bonifacio
	"Casa di Savoia" Chablais Chamb�ry Contamine-sur-Arve
	"Dora Riparia"
	Edoardo Entremont
	Faucigny Filippo
	Genevois Giacomo Jaquerio
	Hautecombe
	"Umberto Sabaudia" 
	"Lac du Bourget" Leysse  
	Maurienne  
	Oddone 
	"Pietro I" Piemonte "fiume Po"
	"Sacra Sindone" "San Domenico" "San Donato" "San Paolo" "Santa Rita" "San Salvario" "Stura di Lanzo" Sangone 
	Talloires Tami� Tommaso Tarentaise Torino
	"Val d'Aosta" 
}

army_names = {
	"Armata de $PROVINCE$"
}