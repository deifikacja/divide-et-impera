#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 210  250  210 }

historical_ideas = { bellum_iustum
	national_conscripts
	military_drill
	divine_supremacy
	engineer_corps
	grand_army
	merchant_adventures
	shrewd_commerce_practise
	patron_of_art
	national_trade_policy
	espionage
}

historical_units = {
	persian_footsoldier
persian_shamshir
persian_duel
persian_infantry
afsharid_reformed_infantry
durrani_rifled_musketeer
ali_bey_reformed_infantry
persian_rifle
musellem
shaybani
new_shaybani
abbasid
new_muslim_clan
ways
new_ways w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Sayyed Mawla #0" = 100
	"Sayyed #0" = 90
        "'Abd al-Karim #0" = 3
	"Ayman #0" = 3
	"Barakat #0" = 3
	"Basir #0" = 3
	"Dawud #0" = 3
	"Fahd #0" = 3
	"Faris #0" = 3
	"Faruq #0" = 3
	"Firdaus #0" = 3
	"Fuad #0" = 3
	"Haidar #0" = 3
	"Hashim #0" = 3
	"Husam #0" = 3
	"Iqbal #0" = 3
	"Jabbar #0" = 3
	"Ja'far #0" = 3
	"Jamal #0" = 3
	"Junayd #0" = 3
	"Khaliq #0" = 3
	"Malik #0" = 3
	"Mansur #0" = 3
	"Mustafa #0" = 3
	"Najib #0" = 3
	"Qasim #0" = 3
	"Qusay #0" = 3
	"Rahim #0" = 3
	"Rash�d #0" = 3
	"Safi #0" = 3
	"Sakhr #0" = 3
	"Samir #0" = 3
	"Tariq #0" = 3
	"Umar #0" = 3
	"Wahid #0" = 3
	"Zahir #0" = 3
}

leader_names = {
	Farajallah
	Ali
	Abdallah
	Mohammed
	Muttalib
	Judallah
	Ismail
	"Abd al-Ali"
	Nasrallah
	Khazal
}

ship_names = {
	Abathur "Adamn Kasia" "Aesma Daeva" Agas
	Ahriman Ahura "Ahura Mazda" Ahurani Airyaman
	"Aka Manah" Aladdin Allatum Amashaspan Ameratat
	"Amesha Spentas" Anahita "Angra Mainyu" Anjuman
	"Apam-natat" Apaosa Aredvi Arishtat Armaiti Arsaces
	"Asha vahista" Asman "Asto Vidatu" "Astvat-Ereta"
	Atar "Azi Dahaka" Baga Bahram Burijas Bushyasta Buyasta
	Camros Daena Daevas Ahaka Dahhak Dena Dev Diwe Drug
	Drvaspa Frashegird Fravashis Gandarewa "Gao-kerena"
	Gayomard Gayomart "Geus-Tasan" "Geus-Urvan" Haoma
	Haurvatat Humay Hvar Hvarekhshaeta Indar Indra Izha
	Jamshid Jeh Karshipta Kavi Khara "Khshathra vairya"
	Kundrav Mah Mahre Mahrianag Manu Manuchihir Mao 
	Mashyane Mashye Menog Mithra Nairyosangha Nanghaithya
	Neriosang Ormazd Peris Peshdadians Rapithwin Rashnu
	Rustam Saurva Simurgh "Spenta Mainyu" Sraosa Srosh
	Tawrich Thunaupa Tistrya Tushnamatay Vanant Vata
	Verethragna "Vohu Manah" Vouruskasha Yam Yasht Yazata
	Yezidi Yima Zal Zam "Zam-Armatay" Zarathustra Zarich
	"Zend-Avesta" Zurvan
}
