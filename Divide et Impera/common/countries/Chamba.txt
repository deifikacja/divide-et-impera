#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 3 66 255 }

historical_ideas = { bellum_iustum
	national_conscripts
	grand_army
	glorious_arms
	military_drill
	battlefield_commisions
	shrewd_commerce_practise
	merchant_adventures
	engineer_corps
	espionage
	national_trade_policy
	smithian_economics
	cabinet
}

historical_units = {
	indian_footsoldier
	rajput_hill_fighters
	indian_arquebusier
	indian_elephant
	mughal_musketeer
	mughal_mansabdar
	indian_shock_cavalry
	rajput_musketeer
	reformed_mughal_musketeer
	reformed_mughal_mansabdar
	sikh_hit_and_run
	sikh_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Udai Singh #1" = 10
	"Ugra Singh #1" = 10
	"Dalel Singh #1" = 10
	"Umed Singh #1" = 10
	"Raj Singh #1" = 10
	"Jit Singh #1" = 10
	"Charhat Singh #1" = 10
	"Rani Sarada Sahiba #1" = -10
	"Sri Singh #1" = 10
	"Mohr Singh #1" = 10
	"Gopal Singh #0" = 10
	"Sham Singh #1" = 10
	"Bhuri Singh #1" = 10
	"Ram Singh #1" = 10
	"Lakshman Singh #1" = 10
}

leader_names = {
	Paramanabhattaraka
	Bargujars
	Bhoja
	Chandelas
	Chauhan
	Jadeja
	Kachwaha
	Karmavati
	Khumba
	Paramaras
	Pratap
	Rathore
	Singh
	Tomaras
	Mihirbhoj
	Mahipala
	Nagabhatta
	Padmini
	Vaghela
	Vatsraja
}

ship_names = {
	Alia Alisha Aziza Beas Chenab
	Fatimah Guru Indus Jalsena Jhelum
	Khan Nausena "Nav ka Yudh" Niknaz
	Niloufa Ravi Sagar Sutlej Yasmine Zahira
}
