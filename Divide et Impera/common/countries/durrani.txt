#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 102  128  52 } 

historical_ideas = { feudal_castles
	national_conscripts
	glorious_arms 
	grand_army
	military_drill
	battlefield_commisions 
	shrewd_commerce_practise
	engineer_corps
	merchant_adventures
	national_trade_policy
	espionage
	bureaucracy
	cabinet
}

historical_units = {
	muslim_footsoldier
muslim_shamshir
mamluk_duel
tofongchis_musketeer
muslim_sturm
durrani_rifled_musketeer
ali_bey_reformed_infantry
persian_rifle
cavalry_archers
swarm
new_swarm
steppe_cav
new_steppe
durrani_swivel
durrani_dragoon w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Mir Ways #0" = 20
	"Humayun #0" = 20
	"Ahmad #0" = 30
	"Timur #0" = 20
	"Shoja al-Mulk #0" = 10
	"Muhammad #0" = 20
	"Shoja #0" = 20
	"Fath Jang #0" = 20
	"Shahpur #0" = 20
	"Zaman #0" = 20
	"Mahmud #0" = 20
	"Ayub #0" = 20
	"Ghulam #0" = 0
    "Sher #0" = 0
    "Abdullah #0" = 0
    "Ali #0" = 0
    "Hamid #0" = 0
    "Saad #0" = 0
        "Akbar #0" = 0
        "Juma #0" = 0
        "Yar #0" = 0
        "Sayyed #0" = 0
        "Ismail #0" = 0
        "Adil #0" = 0
        "Najib #0" = 0
        "Bisharat #0" = 0
}

leader_names = {
	Hotaki
	Taraki
	Amin
	Karmal
	Najibullah
	Akhund
	Rahman
	Ghazi
	Ahmad
	Qadir
	Khan
        Tarzi
        Muhammadzai
        Durrani
        Barakzai
        Alakozai
        Popalzai
        Achakzai
        Sadozai
        Hayat
        Afshar
        Qoli
}

ship_names = {
	Arghandab Khash Farah Harut Harirud
	Murghab Balkh Kowkcheh Kabol Zhob
	Kech Dasht Hingol
}
