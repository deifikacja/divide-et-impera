#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 100  94  125 }

historical_ideas = { bellum_iustum
	national_conscripts
	grand_army
	glorious_arms
	military_drill
	battlefield_commisions
	shrewd_commerce_practise
	merchant_adventures
	engineer_corps
	espionage
	national_trade_policy
	bureaucracy
	cabinet
}

historical_units = {
	indian_footsoldier
	rajput_hill_fighters
	indian_arquebusier
	mughal_musketeer
	mughal_mansabdar
	indian_elephant
	indian_shock_cavalry
	rajput_musketeer
	reformed_mughal_musketeer
	reformed_mughal_mansabdar
	indian_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Malik Qaranful #0" = 20
	"Mahmud #0" = 20
	"Muhammad #0" = 20
	"Husayn #0" = 20
	"Ahmad #0" = 20
	"Burhan #0" = 30
	"Husain #0" = 20
	"Murtaza #0" = 20
	"Ibrahim #0" = 10
	"Bahadur #0" = 10
}

leader_names = {
	Rajabhushan
	Naagesh
	Meherhomji
	Moidu
	Nayudu
	Naseer
	Paramartha
	Saklani
	Tamragouri
	Ghani
}

ship_names = {
	Bagmati Budhi Devi Durga Falgu
	Gandak Ganga Gayatri Jalsena
	"Kali Nauk" Kosi Lakshmi "Lal Nauk"
	"Loki Devi" "Manasa Devi" Parvati
	Radha Ratri "Sagorer Bahini" Sarasvati
	Shakti "Shaktir Nauk" "Shasti Ma" Sita
	Son Trishool Uma
}
