#Country Name: Please see filename.

graphical_culture = latingfx

color = { 146  108  146 }

historical_ideas = { liberal_arts
	national_conscripts
	patron_of_art
	shrewd_commerce_practise
	merchant_adventures
	national_trade_policy
	national_bank
	cabinet
	engineer_corps
	scientific_revolution
	military_drill
	espionage
	bureaucracy
}

historical_units = {
	#Infantry Italy
	w_i_armed_peasantry
	w_i_crossbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_condottiere
	w_i_halberdiers
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_french_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Francesco #0" = 40
	"Gian Galeazzo #0" = 40
	"Filippo Maria #0" = 20
	"Galeazzo Maria #0" = 20
	"Giancarlo #0" = 20
	"Giovanni Maria #0" = 20
	"Lodovico Maria #0" = 20
	"Massimiliano #0" = 20
	"Ermes #0" = 15
	"Azzone #1" = 10
	"Ascanio Maria #0" = 10
	"Carlo #0" = 10
	"Ottaviano #0" = 10
	"Rodolfo #0" = 10
	"Sforza Maria #0" = 10
	"Galeazzo #2" = 5
	"Matteo #2" = 5
	"Guido #1" = 5
	"Luchino #1" = 5
	"Achille #0" = 3
	"Benedetto #0" = 3
	"Bonifacio #0" = 3
	"Camillo #0" = 3
	"Claudio #0" = 3
	"Demetrio #0" = 3
	"Emanuele #0" = 3
	"Fabrizio #0" = 3
	"Felice #0" = 3
	"Gaetano #0" = 3
	"Geronimo #0" = 3
	"Jacopo #0" = 3
	"Leonardo #0" = 3
	"Marcello #0" = 3
	"Niccol� #0" = 3
	"Paolo #0" = 3
	"Raniero #0" = 3
	"Salvatore #0" = 3
	"Timoteo #0" = 3
	"Uberto #0" = 3
	"Vittorio #0" = 3
}

leader_names = {
        Adelasio Aldini Alessandri "Arborio Mella" "Barbiano Belgioioso" Bargnani "Bigliori di Lava" Birago Borromeo Brunetti
	Caetani "di Campofregoso" Caracciolo Carafa "di Card�" "del Carreto" Containi Franchi Ghislieri Gonzaga "Grimaldi di Busca" "de Guzman"
	Lamberti "de Leiva" Luosi "Luserna Bigliori" "Luserna della Torre" Mancini "Medici di Marignano" Melzi "del Monte" Moscati
	Odescalchi "Paleologi Montferrato" Paradisi Pico Ruga Sabbati "di Saluzzo" Savoldi "della Scalla" Serbelloni Sfondrati Sforza Sommariva Sopransi Spinola
	Testi Tizzone Visconti
}

ship_names = {
	Alliata "Ariberto d'Intimiano" Ansperto "Azzone Visconti"
	Celentano "come Angilberto" Como "Corrado II" Cremona
	"Filippo Maria" "Fiumi Lambro" "Fiumi l'Olona" "Fiume Seveso" "Francesco Sforza"
	"Gian Galeazzo Visconti"
	Lecco Lodi Lombardia
	Pavia
	"Re Alboino"
	"San Babila" "San Bernardino alle Ossa" "San Cristoforo sul Naviglio" "San Giovanni in Conca" "San Gottardo" "San Lorenzo" "San Marco" "San Nazaro Maggiore" "San Simpliciano" "San Satiro" "Santa Maria dei Miracoli" Sant'Agostino Sant'Ambrogio Sant'Eustorgio "Santo Stefano" Sondrio
	Terragni Teodolinda Torriani
	Valperto Varese Virgilio
}

army_names = {
	"Armata de $PROVINCE$"
}