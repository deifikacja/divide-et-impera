#Country Name: Please see filename.

graphical_culture = southamericagfx

color = { 240  120  110 } 

historical_ideas = { bellum_iustum
	deus_vult
	divine_supremacy
	patron_of_art
	national_conscripts
	military_drill
	glorious_arms
	battlefield_commisions
	superior_seamanship
	merchant_adventures
	excellent_shipwrights
	sea_hawks
	naval_provisioning
}

historical_units = {
	south_american_spearmen
	aztec_tribal_warfare
	aztec_hill_warfare
	aztec_gunpowder_warfare
	westernized_aztec
	mexican_guerilla_warfare
}

monarch_names = {
	"Oldman #1" = 20
	"Jeremy #2" = 20
	"Peter #1" = 20
	"Edward #1" = 20
	"George #2" = 20
	"George Frederic Augustus #2" = 20
	"Robert #1" = 20
	"William #1" = 20
	"Andrew #1" = 20
	"Jonathan #1" = 20
	"Norton #1" = 20
}

leader_names = {
        Taya Kiamp Ego Egaya Kiamego Kiaya Kego Kiego Tegaya Tiamp
}

ship_names = {
	        Taya Kiamp Ego Egaya Kiamego Kiaya Kego Kiego Tegaya Tiamp
}
