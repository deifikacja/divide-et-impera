#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 250  166  93 } 

historical_ideas = { bellum_iustum
	national_conscripts
	glorious_arms
	military_drill 
	battlefield_commisions
	espionage
	grand_army
	shrewd_commerce_practise
	engineer_corps
	national_trade_policy
	bureaucracy
}

historical_units = {
	mamluk_archer
	mamluk_cavalry_charge
	mamluk_duel
	mamluk_musket_charge
	ali_bey_reformed_infantry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"'Ali #1" = 10
	"Bashir #1" = 20
	"Sa'ad #1" = 20
	"Othman #0" = 10
	"Ahmed #0" = 50
	"Mulhim #0" = 20
	"Yusuf #1" = 30
	"Afandi #0" = 10
	"Haidar #1" = 20
	"Qa�dan #0" = 10
	"Husein #0" = 10
	"Salim #0" = 10
	"Sa�d #0" = 10
	"Abbas #0" = 10
	"Salman #0" = 10
	"Jaqmaq #1" = 10
	"'Uthman #1" = 10
	"Y�suf #1" = 10
	"'Al� #2" = 5
	"Baybars #2" = 5
	"H�jj� #2" = 5
	"Sha'b�n #2" = 5
	"Ab� Bakr #1" = 5
	"al-Hasan #1" = 5
	"Isma'�l #1" = 5
	"K�j�k #1" = 5
	"S�lih #1" = 5
	"'Abd al-Malik #0" = 3
	"Basir #0" = 3
	"Fadl #0" = 3
	"Ghayth #0" = 3
	"Hatim #0" = 3
	"Ilyas #0" = 3
	"Jabir #0" = 3
	"Nadim #0" = 3
	"Nizar #0" = 3
	"Qasim #0" = 3
	"Sa'di #0" = 3
	"Sayyid #0" = 3
	"Yunus #0" = 3
}

leader_names = {
	ad-Din
	Pasha
	Bey
	"Haqqi Bey"
	Kuyumjian
	"Kuyumjian Bey"
	Munif
	"Munif Bey"
	Hassan
	ud-Dawlah
	ul-Malik
	ul-Mulk
}

ship_names = {
	"Zaher Barqooq" "Farag Ben Barqooq"
	"Ben Barqooq" "Muyaid Sheikh" "Ahmen Muyaid" 
	"Zaher Tatar" "Nasser Muhamed" "Ashraf Barsbay"
	"Aziz Gamal" "Nahr an-Nil" "Al-Farafirah"
	"Ad-D�khilah" "Al-Kharijah" "Al-Ali" "Al-Bahriyah"
	Rahman Rahim Malik Quddus Salam Mu'min Muhaymin
	Aziz Jabbar Mutakabbir Khaliq Bari Musawwir 
	Ghaffar Qahhar Wahhab Razzaq Fattah Alim Qabid
	Basit Khafid Rafi Mu'izz Mudhill Sami Basir Hakam Adl
}
