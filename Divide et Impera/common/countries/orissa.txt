#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 210  106  47 }

historical_ideas = { feudal_castles
        merchant_adventures
	high_technical_culture
        military_drill
        bureaucracy
        battlefield_commisions
	superior_seamanship
	shrewd_commerce_practise
	naval_fighting_instruction
	national_trade_policy
	sea_hawks
	smithian_economics
	espionage
}

historical_units = {
	indian_footsoldier
	rajput_hill_fighters
	indian_arquebusier
	indian_elephant
	indian_shock_cavalry
	rajput_musketeer
	south_indian_musketeer
	mughal_mansabdar
	reformed_mughal_musketeer
	reformed_mughal_mansabdar
	tipu_sultan_rocket
	indian_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Narasimhadeva #3" = 30
	"Bhanudeva #3" = 30
	"Kapilesvara Gajapati #0" = 30
	"Prat�prudra #3" = 30
	"Kaluadeva #3" = 30
	"Govindaraja Vidyadhara #3" = 30
	"Chakrapret�pa #0" = 30
	"N�rashima Jana #0" = 30
	"Raghur�ma Chhotra #0" = 30
	"Mukundadeva Harishandra #0" = 30
	"R�mchandra Deva #0" = 30
	"R�mchandra #0" = 20
	"Purushottama Deva #0" = 20
	"Mukunda Deva #0" = 10
	"N�rsimha Deva #0" = 10
	"Gang�dar Deva #0" = 10
	"B�labhadra Deva #0" = 10
	"Mukunda Deva #0" = 10
	"Divya Simha Deva #0" = 10
	"Hari Krishna Deva #0" = 10
	"Gap�n�th Deva #0" = 10
	"Padmanabha #0" = 10
	"V�ra Kishor Deva #0" = 10
}

leader_names = {
	Bhowarbar
	Bidyadhar
	Dassa
	Deo
	Kalinga
	Mansingh
	Patra
	Rai
	Ratha
	Roy
	Santra
	Singhar
	Sinha
	Uria
	Vansa
}

ship_names = {
	Baitarani Balasore Balugaon Behrampur
	Bhubeneswar Brahmani Chandipur Chilka
	Cuttack Durga Gopalpur Jagannath
	Juggernaut Kalijai Konark Lakshmi
	Mahanadi Nalabana Paradeep Parvati
	Puri Radha Sarasvati Satapada
}
