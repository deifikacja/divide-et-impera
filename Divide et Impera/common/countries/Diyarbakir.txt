#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 20  120  150 }

historical_ideas = { early_mining
	national_conscripts
	grand_army
	glorious_arms
	espionage
	military_drill
	merchant_adventures
	divine_supremacy
	battlefield_commisions
	engineer_corps
	national_trade_policy
	cabinet
	patron_of_art
}

historical_units = {
	muslim_footsoldier
muslim_shamshir
mamluk_duel
tofongchis_musketeer
muslim_sturm
durrani_rifled_musketeer
ali_bey_reformed_infantry
persian_rifle
cavalry_archers
swarm
new_swarm
new_steppe
durrani_swivel
durrani_dragoon w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"al-Hasan #1" = 10
	"Sa'id #2" = 10
	"Abu #1" = 10
	"Ahmad #2" = 10
	"Nasr #1" = 10
	"Mansur #0" = 10 
	"Ibrahim #1" = 10
	"Shams #1" = 10
	"al-Salih #3" = 10
	"al-Mu'azzam #1" = 10
	"al-Muwahhid #1" = 10
	"al-Kamil #1" = 10
	"al-Adil #6" = 10
	"al-Ashraf #1" = 10
	"Sulaiman #2" = 10
	"al-Husayn #1" = 10
}

leader_names = {
	"Abu 'Ali"
	ad-Dawla
	Shoja
	ad-Din
	Mahmud
	"ibn al-Kamil"
	"ibn al-Salih"
	"ibn al-Mu'azzam"
	"ibn al-Muwahhid"
	"ibn al-Adil"
	"ibn al-Ashraf"
	"ibn Muhammad"
	"ibn Sulaiman"
	"ibn Khalil"
}

ship_names = {
	"Nure Sufi" Sadaddin Diyarbakir Ermenek
	Mut G�lnar Mer Silifke Cilicia Larande
	Bunsuz "Zeyn�l-Hac" "Semseddin Mehmet" G�ksu
}
