#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 150  96  77 }

historical_ideas = {
	merchant_adventures  
	excellent_shipwrights 
	shrewd_commerce_practise 
	superior_seamanship
	national_conscripts
	national_trade_policy
	sea_hawks
	naval_fighting_instruction
	military_drill
	naval_glory
	engineer_corps
	battlefield_commisions
}

historical_units = {
	east_asian_spearmen
	eastern_bow
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	asian_musketeer
	reformed_asian_musketeer
	reformed_asian_cavalry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

leader_names = {
	Abdulfatah Ahmad Amir Anwar Azmi 
	Batuta 
	Chairul 
	Hamzah Hasanuddin 
	Ibnu 
	Kyai 
	Mahat Mukhtar 
	Najamuddin Natzar Nazmizan 
	Roem 
	Syarifuddin 
	Zulkifli 
}

monarch_names = {
	"Melawar #1" = 10
	"Hitam #1" = 10
	"Lenggang #1" = 10
	"Kerjan #0" = 10
	"Luboh #0" = 10
	"Radin #0" = 10
	"Imam #0" = 10
	"Intan #0" = -10
	"Antah #0" = 10
	"Muhammad #0" = 10
	"Abdul Rahman #0" = 10
	"Jaafar #0" = 10
}

ship_names = {
	"Sultan Iskandar" Bendahara Laksamana
	Temenggung "Penghulu bendahari" Shahbandar
	"Hukum Kanun" "Undang-Undang" "Pulau Bakung"
	"Pulau Lingga" Kepulauan "Pulau Singkep" "Kepulauan Riau"
	"Pulau Rempang" "Pulau Kundur" "Pulau Padang"
	"Pulau Bengkaus" "Pulau Tinggi" "Pulau Sibu"
	"Pulau Tioman" "Pulau Aur" "Pulau Rupat"
	"Pulau Rangsang" "Pulau Tebing Tinggi"
}
