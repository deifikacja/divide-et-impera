#Country Name: Please see filename.

graphical_culture = latingfx

color = { 203  93  124 }

historical_ideas = { 
	early_mining
	shrewd_commerce_practise
	humanist_tolerance
	merchant_adventures
	patron_of_art
	national_bank
	national_conscripts
	national_trade_policy
	ecumenism
	smithian_economics
	scientific_revolution
	bureaucracy
	espionage
}

historical_units = {
	#Infantry German
	w_i_armed_peasantry
	w_i_longbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_hungarian_formation
	w_i_spanish_formation
	w_i_chosen_formation
	w_i_swedish_formation
	w_i_quarter_army
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_prussian_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon 
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Johann Georg #0" = 40
	"Leopold #0" = 40
	"Georg #0" = 20
	"Jakob Ernst #0" = 20
	"Joachim Ernst #0" = 20
	"Johann Kasimir #0" = 20
	"Leopold #0 Friedrich" = 20
	"Leopold #0 Maximilian" = 20
	"Ernst #0" = 20
	"Siegmund #0" = 20
	"Bernhard #6" = 15
	"Waldemar #5" = 15
	"Johann #4" = 15
	"Rudolf #3" = 15
	"Friedrich #0" = 15
	"Friedrich Kasimir #0" = 15
	"Georg Aribert #0" = 15
	"Karl #0" = 15
	"Moritz #0" = 15
	"Thomas #0" = 15
	"Wilhelm Gustav #0" = 15
	"Albrecht #5" = 15
	"Heinrich #4" = 10
	"Albrecht Friedrich #0" = 10
	"August #0" = 10
	"Christian #0" = 10
	"Dietrich #0" = 10
	"Friedrich Heinrich #0" = 10
	"Friedrich Moritz #0" = 10
	"Heinrich Waldemar #0" = 10
	"Joachim #0" = 10
	"Joachim Christoph #0" = 10
	"Johann Ernst #0" = 10
	"Lorenz #0" = 10
	"Ludwig #0" = 10
	"Paul #0" = 10
	"Wilhelm #0" = 10
	"Andreas #0" = 3
	"G�nther #0" = 3
	"Michael #0" = 3
	"Ulrich #0" = 3
}

leader_names = {
	Hoppe Herzburg Ackermann Alberti 
	Albrecht Alvensleben Amman Annecke Anthe
	Bauer Bause Ehrenberg Erxleben Feuerstack 
	Flachsbart Genthe Gerstung Held Isaack
	Janeck Jantke Kersten Kleinau M�rz Nehmiz 
	Rumpf Scharfe Schauseil Schieck "von Zerbst"
}

ship_names = {
	"Albrecht der B�r" Aschersleben "Hermann Orlam�nde"
	"Bischof Siegfried" "Dietrich Werben" "Albrecht Ballenstedt"
	"Bernhard Wittenberg" "Heinrich I" Zerbst Coswig Dessau K�then
	Bernburg Brocken Hartz Pl�tzkau Ramberg Rosslau
}

army_names = {
	"Armee von $PROVINCE$" 
}