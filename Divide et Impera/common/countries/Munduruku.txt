#Country Name: Please see filename.

graphical_culture = southamericagfx

color = { 114  184  217 }

historical_ideas = { bellum_iustum
	church_attendance_duty
	humanist_tolerance
	national_trade_policy
	bill_of_rights
	patron_of_art
	national_conscripts
	military_drill
	battlefield_commisions
	grand_army
}

historical_units = {
	south_american_spearmen
	south_american_warfare
	inca_mountain_warfare
	south_american_gunpowder_warfare
	westernized_south_american
	peruvian_guerilla_warfare
}

leader_names = {
}

monarch_names = {
	"Apip�" = 20
	"Arar�" = 20
	"Abrojo" = 20
	"Achira" = 20
	"Aguaribay" = 20
	"Arah�" = 20
	"Algarrobo" = 20
	"Anchico" = 20
	"Aliso" = 20
	"Aromo" = 20
	"An�" = 20
	"Arriador" = 20
	"Apero" = 20
	"Baquiano" = 20
	"Begu�" = 20
	"Bagual" = 20
	"Bohan" = 20
	"Boyero" = 20
	"Curupy" = 20
	"Caranday" = 20
	"Cachilo" = 20
	"Camb�" = 20
	"Cumpa" = 20
	"Carona" = 20
	"Caraguat�" = 20
	"Ceibo" = 20
	"Cincha" = 20
	"Capanga" = 20
	"Camat�" = 20
	"Cachimbo" = 20
	"Criolla" = 20
	"Creole" = 20
	"Cacique" = 20
	"Curiy�" = 20
	"Ca�" = 20
	"Cardo" = 20
	"Cana" = 20
	"Cunum�" = 20
	"Clina" = 20
	"Cabay�" = 20
	"Cunata�" = 20
	"Cabur�" = 20
	"Curund�" = 20
	"Ca�" = 20
	"Capib�" = 20
	"Curup�" = 20
	"Carac�" = 20
	"Capitanejo" = 20
	"Cachap�" = 20
	"Chicote" = 20
	"Chamam�" = 20
	"Chimango" = 20
	"Chan�" = 20
	"Chaira" = 20
	"Chirip�" = 20
	"Chusca" = 20
	"Ch�caro" = 20
	"Chajar�" = 20
	"Chicharra" = 20
	"Charr�a" = 20
	"Chaj�" = 20
	"Espinel" = 20
	"Estribo" = 20
	"Espinillo" = 20
	"Fierro" = 20
	"Fac�n" = 20
	"Faca" = 20
	"Fog�n" = 20
	"Fija" = 20
	"Gaucho" = 20
	"Gur�" = 20
	"Gringa" = 20
	"Guabiy�" = 20
	"Garza" = 20
	"Gualeguay" = 20
	"Guasca" = 20
	"Guayac�n" = 20
	"Guena" = 20
	"Guayaca" = 20
	"Gualicho" = 20
	"Guaina" = 20
	"Guar�n" = 20
	"Gurisa" = 20
	"Guaran�" = 20
	"Gueya" = 20
	"Guabiy�" = 20
	"Guaica" = 20
	"Gaucha" = 20
	"Guayac�n" = 20
	"Guayaib�" = 20
	"Guaz�" = 20
	"Guayabo" = 20
	"Guenoa" = 20
	"Ibapoy" = 20
	"Ing�" = 20
	"Itat�" = 20
	"Iguaz�" = 20
	"Ivot�" = 20
	"Jaguel" = 20
	"Jangada" = 20
	"Julepe" = 20
	"Jacarand�" = 20
	"Litoral" = 20
	"Lanza" = 20
	"Lapacho" = 20
	"Lechiguana" = 20
	"Mate" = 20
	"Malvina" = 20
	"Malvinas" = 20
	"Milico" = 20
	"Moraj�" = 20
	"Mojarra" = 20
	"Minu�n" = 20
	"Mens�" = 20
	"Mandiy�" = 20
	"Maula" = 20
	"Matungo" = 20
	"Mikich�" = 20
	"Min�" = 20
	"Mocoret�" = 20
	"Malambo" = 20
	"Mataco" = 20
	"Neike" = 20
	"Napind�" = 20
	"Naupa" = 20
	"Nand�" = 20
	"Nangapir�" = 20
	"Nandubay" = 20
	"Nanduty" = 20
	"Omb�" = 20
	"Orejano" = 20
	"Paran�" = 20
	"Piola" = 20
	"Palenque" = 20
	"Pirana" = 20
	"Patac�n" = 20
	"Picada" = 20
	"Payador" = 20
	"Prienda" = 20
	"Pira�" = 20
	"Pitangu�" = 20
	"Por�" = 20
	"Penca" = 20
	"Pingo" = 20
	"Paca�" = 20
	"Poncho" = 20
	"Pind�" = 20
	"Peric�n" = 20
	"Quebracho" = 20
	"Quincho" = 20
	"Rancho" = 20
	"Recado" = 20
	"Resero" = 20
	"Rebenque" = 20
	"Sauce" = 20
	"Sarand�" = 20
	"Solapa" = 20
	"Sapucay" = 20
	"Samoh�" = 20
	"Sotreta" = 20
	"Sauco" = 20
	"Tacuar�" = 20
	"Tapera" = 20
	"Tape" = 20
	"Tarasca" = 20
	"Tero" = 20
	"Tala" = 20
	"Talero" = 20
	"Tuca" = 20
	"Trabuco" = 20
	"Torcaza" = 20
	"Taita" = 20
	"Taba" = 20
	"Timb�" = 20
	"Tranca" = 20
	"Tiento" = 20
	"Tuna" = 20
	"Tacuaruz�" = 20
	"Taipa" = 20
	"Tacur�" = 20
	"Tacuara" = 20
	"Tuyango" = 20
	"Totora" = 20
	"Tata" = 20
	"Tucur�" = 20
	"Toba" = 20
	"Tipa" = 20
	"Yas�" = 20
	"Yas�" = 20
	"Yacar�" = 20
	"Yar�" = 20
	"Yarar�" = 20
	"Yaro" = 20
	"Yerba" = 20
	"Yatay" = 20
	"Yerra" = 20
	"Yeru�" = 20
	"Yapa" = 20
	"Yuch�n" = 20
	"Yapey�" = 20
	"Yunta" = 20
	"Yuvia" = 20
	"Ubajay" = 20
	"Urunday" = 20
	"Uruguay" = 20
	"Viracho" = 20
	"Virar�" = 20
	"Viray�" = 20
	"Zorzal" = 20
	"Zarza" = 20
	"Zamba" = 20
}

ship_names = {      
	"Guarani Raft" "Guarani Canoe" "Guarani Dingie"   
	"Guarani Floatingthing" "Waterproof" "Never Sinks"    
}

