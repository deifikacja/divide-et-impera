#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 130  168  224 }

historical_ideas = { bellum_iustum
        grand_army
	merchant_adventures
	shrewd_commerce_practise
	patron_of_art
	national_trade_policy
	espionage
	battlefield_commisions
	cabinet
	national_conscripts
	military_drill
	divine_supremacy
	engineer_corps
}

historical_units = {
	muslim_footsoldier
muslim_shamshir
mamluk_duel
tofongchis_musketeer
muslim_sturm
durrani_rifled_musketeer
ali_bey_reformed_infantry
persian_rifle
cavalry_archers
swarm
new_swarm
steppe_cav
new_steppe
durrani_swivel
durrani_dragoon w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Ab� Sa'�d #0" = 20
	"B�bur #0" = 20
	"Bad� al-Zam�n #0" = 20
	"Husayn B�yqar� #0" = 20
	"Abu al-Qasim #0" = 20
	"Mahm�d #0" = 20
	"Sh�h Rukh #1" = 10
	"Ulugh Beg #1" = 10
	"'Abbas #0" = 3
        "'Abd al-Karim #0" = 3
	"'Abd al-Rahman #0" = 3
	"'Al� #0" = 3
        "'Abdall�h #0" = 3
	"Anwar #0" = 3
	"Bakr #0" = 3
	"Fahd #0" = 3
	"Faruq #0" = 3
	"Fouad #0" = 3
	"Hakim #0" = 3
        "Harun #0" = 3
	"Hisham #0" = 3
	"Husayn #0" = 3
	"Isma'il #0" = 3
	"Jafar #0" = 3
        "Jamaal #0" = 3
	"Karim #0" = 3
	"Khalil #0" = 3
        "Mal�k #0" = 3
	"Mirza #0" = 3
	"Muhammad #0" = 3
	"Qasim #0" = 3
	"Rash�d #0" = 3
        "Sulayman #0" = 3
	"Tar�q #0" = 3
	"'Umar #0" = 3
	"Usama #0" = 3
	"Yasir #0" = 3
	"Yusuf #0" = 3
	"Zaid #0" = 3
	"Z�yad #0" = 3
	"Zulqifar #0" = 3
}

leader_names = {
	Ali
	Bali
	Karawi
	Khan
	Mirza
	Nawaz
	Quli
	Tadj
	Wali
	Zaman
}

ship_names = {
	Badam
	Bardeskan
	Dasht
	Deyhuk
	Germi
	Jandaq
	Khvor
	Kondor
	Tabas
	Torud
}
