#Country Name: Please see filename.

graphical_culture = latingfx
color = { 220  138  57 }

historical_ideas = { naval_transport
	merchant_adventures
	excellent_shipwrights
	national_conscripts
	military_drill
	superior_seamanship
	quest_for_the_new_world
	national_trade_policy
	colonial_ventures
	naval_fighting_instruction
	smithian_economics
	ecumenism
	scientific_revolution
	improved_foraging
}

historical_units = {
	#Infantry Netherlands
	w_i_armed_peasantry
	w_i_municipal_militia
	w_i_mercenary
	w_i_battle_formation
	w_i_condottiere
	w_i_halberdiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_british_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Willem #0" = 100
	"Lodewijk #0" = 40
	"Frederik Hendrik #0" = 20
	"Maurits #0" = 20
	"Filips Willem #0" = 15
	"Hendrik Lodewijk #0" = 15
	"Karel #0 Lodewijk" = 15
	"Lodewijk #0 Karel" = 15
	"Adriaan #0" = 10
	"Andries #0" = 10
	"Antonius #0" = 10
	"Frederik #0" = 10
	"Gaspar #0" = 10
	"Isaac #0" = 10
	"Jacob #0" = 10
	"Johan #0" = 10
	"Laurens #0" = 10
	"Paul #0" = 10
	"Pieter #0" = 10
	"Simon #0" = 10
	"Abraham #0" = 3
	"Antoon #0" = 3
	"Barthout #0" = 3
	"Christiaan #0" = 3
	"Christoffel #0" = 3
	"Constantijn #0" = 3
	"Cornelis #0" = 3
	"Daniel #0" = 3
	"David #0" = 3
	"Filips #0" = 3
	"Dirk #0" = 3
	"Eise #0" = 3
	"Frans #0" = 3
	"Gabriel #0" = 3
	"Gerard #0" = 3
	"Gerben #0" = 3
	"Godfried #0" = 3
	"Hans #0" = 3
	"Jeroen #0" = 3
	"Hugo #0" = 3
	"Jacob #0" = 3
	"Jan #0" = 3
	"Joost #0" = 3
	"Nicolaas #0" = 3
	"Roeland #0" = 3
	"Rogier #0" = 3
	"Rutger #0" = 3
	"Thomas #0" = 3
	"Vincent #0" = 3
	"Zacharias #0" = 3
}

leader_names = {
	"Van der Aa" "Van Assendelft" 
	"Van Bronkhorst" Batenburg Bentinck Brandt "Van Brederode" Brouwer
	Coehoorn Coen Crijnssen "Van Culemborg"
	Daendels "Van Diemen" "Van der Does" "Van Duivenvoorde"
	"Van Egmont" Evertzen
	"Van Galen" "De Grouwe"
	"Van Haren" "Van Heemskerck" "De Houtman" "Van Huchtenbroek"
	"Van Ilpendam"
	Kater "Van Keppel" Kortenaer
	"Van Marnix" Martena "De Moor"
	Nachtegaal "Van Nieuwenaar" "Van Noort"
	"Van Pallandt"
	"Van Raephorst" "Van Rechteren" Reijersen "Van Renesse" "Van Riebeeck" "De Rijk" "Van Rossum"
	Schouten Schenk Spiegel Steltman Stuyvesant "Van Swieten"
	Tasman "Van Troyen"
	Vliechop "Van Voorst"
	"Van Utenhove"
	"Van Walbeeck" "Van Wassenaer" "Van der Werff" "Van Wesembeke" "Van Wingle" "Van Wijngaarden" "De Winter"
}

ship_names = {
	Adelaar Ajax Alkmaar Amelia Amsterdam Atalanta
	Bantam Batavia Beschermer Brederode Brouwer Brutus
	Campen Cerberus Comeestar
	Daphne Delfland Delft "Den Swarten Raven" Delphin Deventer Duyfken
	Eendracht Eenhorn Embuscade Engel Erasmus
	Gaesterland "Gekroende Liefde" Gelijkheid Gouda "Gouden Arend" "Gouden Leeuw" "Groot Frisia" "Groote Sint Joris" "Grote Zon"
	Haarlem Heldin Hercules "De Halve Maen" Hollandia "Hollandsche Tuyn" "Huis te Oosterwijk" "Huis te Zwieten" "Huys van Nassau"
	"Jong Prins te Paard" Jupiter "Kleine Zon"
	"Kleene Sint Joris" Komeeetster "Kronede Arend"
	Leyden "Liefde van Hoorn"
	"Maagd van Dordrecht" Mars Mauritius Middelburg Minerva Moon Monnikendam Muiderberg
	Nassau "Nieuw Vlissingen" 
	Olifant Onrust Ooievaar Oranje
	Patientia Posthorn "Prins Friso" "Prins Willem" 
	Rotterdam Rozendaal
	Schakerloo Schermer Spiegel Staten-Generaal Statenlande Ster "St. Iago de Victoria" "St. Marten" "St. Matthuis"
	Tholen Tyger 
	"Vereenigde Provincie�n" Vlissingen Vrede Vrijheid
	Waakzaamheid "Walcheren" "Wapen van Holland" "Wapen van Medenblik" Wassenaar Winhond "Witte Leeuw" Woerden Zeerijp
	"Zeven Provincien" Zierikzee Zon Zuidholland "Zwarte Leeuw" 
}


army_names = {
	"Staatse Leger" "Prinselijk Leger" "Leger van $PROVINCE$" 
}

fleet_names = {
	"Staatse Vloot" "Noordzee Vloot" "Oostzee Vloot" "Oost-Indi� Vloot" "West-Indi� Vloot" "Middellandsezee Vloot"  "Vloot van $PROVINCE$" 
}