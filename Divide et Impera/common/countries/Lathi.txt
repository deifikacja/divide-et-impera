#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 255 3 3 }

historical_ideas = { bellum_iustum
	national_conscripts
	glorious_arms 
	grand_army
	military_drill
	battlefield_commisions 
	shrewd_commerce_practise
	engineer_corps
	merchant_adventures
	national_trade_policy
	espionage
	smithian_economics
	bureaucracy
}

historical_units = {
	indian_footsoldier
	indian_archers
	indian_arquebusier
	mughal_musketeer
	mughal_mansabdar
	indian_elephant
	indian_shock_cavalry
	rajput_musketeer
	reformed_mughal_musketeer
	reformed_mughal_mansabdar
	indian_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Lakhaji #0 Jijibawa" = 10
	"Takhtsimhji #0 Lakhaji" = 10
	"Sursimhji #0 Takhtsimhji" = 10
	"Pratapsimhji #0 Sursimhji" = 10
	"Prahladsinhji #0 Pratapsimhji" = 10
}

leader_names = {
	"Thakur Sahib"
	Bargujars
	Bhoja
	Chandelas
	Chauhan
	Jadeja
	Kachwaha
	Karmavati
	Khumba
	Paramaras
	Pratap
	Rathore
	Singh
	Tomaras
	Mihirbhoj
	Mahipala
	Nagabhatta
	Padmini
	Vaghela
	Vatsraja
}

ship_names = {
	Chambal Devi Durga Gayatri Ghaggar
	Godwar Jalsena "Kala Jahazi" Lakshmi
	"Lal Jahazi" Luni Nausena "Marwar ka Nav"
	"Merwar ka Nav" "Nav ka Yudh" "Nila Jahazi"
	Parvati Radha Rani Ratri Sagar Sarasvati
	Sekhawati Sita
}
