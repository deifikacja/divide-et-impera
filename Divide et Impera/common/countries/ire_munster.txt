# Country name see file name.

graphical_culture = latingfx

color = { 104  137  140 }

historical_ideas = { naval_transport
	excellent_shipwrights
	bill_of_rights
	superior_seamanship
	merchant_adventures
	naval_glory
	cabinet
	national_bank
	espionage  
	smithian_economics
	scientific_revolution
	revolution_and_counter
	national_trade_policy
}

historical_units = {
	#Infantry Britain
	w_i_armed_peasantry
	w_i_longbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_british_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Turlough #2" = 80
	"Conchobhar #2" = 60
	"Tadhg #2" = 40
	"Donnchad #7" = 20
	"Muireadhach #1" = 20
	"Mathghamhain #0" = 20
	"Diarmaid #3" = 1
	"Mathghamhain #0" = 20
	"�dhamh #0" = 3
	"Ailill #0" = 3
	"Br�gh #0" = 3
	"Br�nach #0" = 3
	"Cadwgwan #0" = 3
	"Caoimh�n #0" = 3
	"Cinn�idigh #0" = 3
	"Deagl�n #0" = 3
	"Dougal #0" = 3
	"Fionnlagh #0" = 3
	"Garbh�n #0" = 3
	"Lachtna #0" = 3
	"Lorcc�n #0" = 3
	"Mainch�n #0" = 3
	"Math�in #0" = 3
	"Meall�n #0" = 3
	"Naomh�n #0" = 3
	"Niall #0" = 3
	"Odhr�n #0" = 3
	"P�draig #0" = 3
	"Quinn #0" = 3
	"Riain #0" = 3
	"R�oghn�n #0" = 3
	"R�rdan #0" = 3
	"Ruaidhr� #0" = 3
	"Ruarc #0" = 3
	"S�aghdh� #0" = 3
	"Sean�n #0" = 3
	"Sechnall #0" = 3
	"Tighearnach #0" = 3
	"Tuathal #0" = 3
	"Uilliam #0" = 3
	"Uinseann #0" = 3
}

ship_names = {
	Clare Cork
	"Deas Mhuman" 
	Kerry 
	"Lar Mumhan" Limerick
	Tipperary "Tuad Mhuman"
	"Urh Mumhan"  
	Waterford
}  

leader_names = {
	Barry Boyle
	Darcy
	FitzGerald FitzThomas
	O'Brien O'Neill
	MacCarthy
	Sarsfield
	Talbot
}
