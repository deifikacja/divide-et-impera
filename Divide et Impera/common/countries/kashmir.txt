#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 63  150  30 }

historical_ideas = { early_mining
	national_conscripts
	grand_army
	glorious_arms
	military_drill
	battlefield_commisions
	shrewd_commerce_practise
	merchant_adventures
	engineer_corps
	espionage
	national_trade_policy
	smithian_economics
	cabinet
}

historical_units = {
	indian_footsoldier
	rajput_hill_fighters
	indian_arquebusier
	indian_elephant
	mughal_musketeer
	mughal_mansabdar
	indian_shock_cavalry
	rajput_musketeer
	reformed_mughal_musketeer
	reformed_mughal_mansabdar
	sikh_hit_and_run
	sikh_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Sikandar #0" = 10
	"Ali Shah #0" = 10
	"Zayn al-'Abidin #0" = 10
	"Haydar Shah #0" = 10
	"Hasan Shah #0" = 10
	"Muhammad Shah #0" = 10
	"Sayyid Hassan #0" = 10
	"Fath Shah #0" = 10
	"Ibrahim Shah #0" = 30
	"Nazuk Shah #0" = 10
	"Shams ad-Din #1" = 10
	"Ismail Shah #0" = 20
	"Ghazi Shah #0" = 10
	"Husayn Shah #0" = 10
	"Yusuf Shah #0" = 10
	"Lohir Shah #0" = 10
	"Nabib Shah #0" = 10
	"Ya'qub Shah #0" = 10
}

leader_names = {
	Bahadur
	Bhalla
	Das
	Dev
	Gahunia
	Gobind
	Granth
	Hoora
	Jhol
	Jhotti
	Krishan
	Lohaar
	Parmanand
	Rajsingh
	Shergill
	Singh
	Sodhi
	Sohal
	Rai
	Udasi
}

ship_names = {
	Alia Alisha Aziza Beas Chenab
	Fatimah Guru Indus Jalsena Jhelum
	Khan Nausena "Nav ka Yudh" Niknaz
	Niloufa Ravi Sagar Sutlej Yasmine Zahira
}
