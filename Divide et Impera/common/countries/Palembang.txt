#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 120  110  170 }

historical_ideas = { liberal_arts
	merchant_adventures  
	excellent_shipwrights 
	shrewd_commerce_practise 
	superior_seamanship
	national_trade_policy
	naval_fighting_instruction
	sea_hawks
	national_conscripts
	military_drill
	naval_glory 	
}

historical_units = {
	east_asian_spearmen
	eastern_bow
	asian_arquebusier
	asian_musketeer
	reformed_asian_musketeer w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Mahmud Badruddin #2 bin Muhammad" = 10
	"Ahmad Najmuddin #2 bin Muhammad" = 10
	"Ahmad Najmuddin #0 bin Ahmad" = 10
}

leader_names = {
	"bin Ahmad"
	"bin Muhammad"
}

ship_names = {
	Pendet Legong Baris Topeng Barong
	Kecak Batur Catur Bratan Pohen Lesong
	Sangyang Batukaru Patas Musi Mesehe
	Agong Seraya Merbuk Sanglang Kelatakan
}
