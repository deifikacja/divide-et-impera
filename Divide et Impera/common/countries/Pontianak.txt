#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 141  89  174 }

historical_ideas = { bellum_iustum
	merchant_adventures
	excellent_shipwrights
	shrewd_commerce_practise
	superior_seamanship
	national_conscripts
	national_trade_policy
	sea_hawks
	naval_fighting_instruction
	military_drill
	naval_glory
	battlefield_commisions
	engineer_corps
}

historical_units = {
	east_asian_spearmen
	eastern_bow
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	asian_musketeer
	reformed_asian_musketeer w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Syarif Abdul Rahman #1" = 10
	"Syarif Kasim #1" = 10
	"Syarif Usman #1" = 10
	"Radzathu #1" = 10
	"Sultan Abdul Hamid #1" = 20
	"Syarif Yusuf #1" = 10
	"Syarif Muhammad #1" = 10
	"Syarif Taha #1" = 10
	"Syarif Abubakr #1" = 10
}

leader_names = {
	al-Kadri "bin Husain al-Kadri" "bin Abdul Rahman al-Kadri" "bin Usman al-Kadri" "bin Abdul Hamid al-Kadri" "bin Yusuf al-Kadri" "bin Muhammad al-Kadri"
}

ship_names = {
	"Sungai Belait" "Sungai Tutong" "Sungai Tembureng"
	Batang Trusan "Sungai Pandaruan" "Lungai Limbang"
	"Batang Baram" "Sungai Bakong" "Sungai Tutoh"
	"Sungai Tinjar" "Kuala Belait" "Batu Danau"
	"Kuala Medamit" Bangar Penanjong Lumut Telingan
	Labi Sukang "Kuala Baram"
}
