#Country Name: Please seefilename.

graphical_culture = africangfx

color = { 25  245  20 }

historical_ideas = {
	national_conscripts
	shrewd_commerce_practise
	divine_supremacy
	national_trade_policy
	military_drill
	engineer_corps
	battlefield_commisions
	merchant_adventures
	grand_army
	espionage
}

historical_units = {
	african_spearmen
	bantu_tribal_warfare
	bantu_plains_warfare
	bantu_gunpowder_warfare
	westernized_bantu
	african_western_franchise_warfare
}


monarch_names = {
	"Ramaromanpo #0" = 10
	"Ratsimiaho #0" = 10
	"Beti #0" = 10
	"Zanahary #0" = 10
	"Iavy #0" = 10
	"Zakavola #0" = 10
}

leader_names = {
}

ship_names = {
	Fiantaranasoa Antananriva Tananariva Merina Imerina Imernia Toliary Mahajanga Antsiranana
}
