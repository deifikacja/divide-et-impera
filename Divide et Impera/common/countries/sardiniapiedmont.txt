#Country Name: Please see filename.

graphical_culture = latingfx

color = { 97  209  251 }

historical_ideas = {
	patron_of_art 
	superior_seamanship
	shrewd_commerce_practise
	merchant_adventures
	excellent_shipwrights
	national_bank
	engineer_corps
	national_trade_policy
	cabinet
	bureaucracy
	sea_hawks
	napoleonic_warfare	
}

historical_units = {
	#Infantry Italy
	w_i_armed_peasantry
	w_i_crossbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_condottiere
	w_i_halberdiers
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_french_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon 
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Vittorio Amedeo #1" = 40
	"Carlo Emanuele #2" = 20
	"Carlo Felice #0" = 20
	"Amedeo #9" = 10
	"Carlo #3" = 10
	"Emanuele Filiberto #1" = 10
	"Benedetto #0" = 10
	"Maurizio #0" = 10
	"Giuseppe #0" = 10
	"Agostino #0" = 3
	"Andrea #0" = 3
	"Arsenio #0" = 3
	"Benvenuto #0" = 3
	"Bonaventura #0" = 3
	"Cesare #0" = 3
	"Costanzo #0" = 3
	"Cosimo #0" = 3
	"Dario #0" = 3
	"Davide #0" = 3
	"Edmondo #0" = 3
	"Enzo #0" = 3
	"Franco #0" = 3
	"Gaspare #0" = 3
	"Gennaro #0" = 3
	"Giulio #0" = 3
	"Ilario #0" = 3
	"Jacopo #0" = 3
	"Lazzaro #0" = 3
	"Lorenzo #0" = 3
	"Luciano #0" = 3
	"Martino #0" = 3
	"Nestore #0" = 3
	"Patrizio #0" = 3
	"Pietro #0" = 3
	"Roberto #0" = 3
	"Santo #0" = 3
	"Sebastiano #0" = 3
	"Simone #0" = 3
	"Ugo #0" = 3
	"Vittorio #0" = 3
}

leader_names = {
        Alfieri "d'Appiani d'Aragona" "Arborio Mella" Asinari d'Attalaya
	Balbo Benso "de Bette" "Boncompagni Ludovisi" Borromeo Bosco Botta Broglia "di Busca"
	Cafasso "di Card�" "del Carreto" Casati "di Cavour" Chiodo "Cisa Asinari" Cottolengo
	Delitalia "Doria di Ciri�" "Facchinetti de Nuce" Ferrero-Fiesco 
	"Gaetani dell'Aquila" Galletti Gioberti "Homo dei Moura" Lascaris Ludovisi "Luserna Campiglione" "di Luserna"
	"del Marco" "de Maretta" Micca Ossorio "d'Ottaiano" "di Paesana" Pallavicini Pignatelli
	"di Savoia" Solaro Taparelli Tassoni "della Torre" Trivulzio "de Tutavila" Vico
}

ship_names = { 
	"Alfonso il Benigno" Alghero Asinara Assemini
	Campidano Capoterra Caprera "Carbonia-Iglesias" Cagliari Cedrino Coghinas
	"Dolomiti di Oliena"
	"Eleanor Arborea"
	Flumendosa
	"Giacomo II" "Giovanni il Galantuomo" "Giudice Ugone"
	"lago di Baratz"
	"Martino l'Umano" "Medio Campidano" Monserrato "Monte Albo" "Monte Rasu" "Monti di Limbara" "Monti di Al�" 
	Nuoro 
	Ogliastra "Olbia-Tempio" Oristano Orosei
	"Porto Torres" "Punta La Marmora"
	Quartucciu
	Sant'Antioco "Santa Gilla" Sant'Elena "San Michele" "San Pietro" Sassari S.Domenico S.Maria "S.Saturnino" Selargiu Sestu Sinnai Sorso
	Tavolara Tirso
}
