#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 32 58 132 }

historical_ideas = {
	national_conscripts
	glorious_arms
	military_drill
	battlefield_commisions
	shrewd_commerce_practise
	grand_army
	merchant_adventures
	national_trade_policy
	engineer_corps
	cabinet
	humanist_tolerance
	bureaucracy
}

historical_units = {
	east_asian_spearmen
	eastern_bow
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	asian_musketeer
	reformed_asian_musketeer
	reformed_asian_cavalry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Sao M�ng #0" = 20
	"Sao Maha #0" = 20
	"Sao Hseng #0" = 20
	"Sao Kawng #0" = 20
	"Sao Kawn Kham #0" = 20
	"Sao Kawn Kyaw #0" = 20
	"Sao Sailong #0" = 20
}

leader_names = {
	Daw
	Hwae
	Hsam 
	Tin
	Tchin
	U-nu
	Tun-Aung
	Min-Tun
	Kwei
	Maung-Gyi
	Hkanan 
	Tai
	Pawn
	Hpu
	Intaleng
	Mangrai
}

ship_names = {
	Salween Taunggyi Kengtong Tachileik Nam Thanlwin
	Saluen Lui Hsim Teng Myitnge Pilu Inle Shweli
}
