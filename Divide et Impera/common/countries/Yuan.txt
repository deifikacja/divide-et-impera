#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 179  178  104 }

historical_ideas = {
        bureaucracy
	national_conscripts
	merchant_adventures
	espionage
	military_drill
	glorious_arms
	national_trade_policy
	patron_of_art
	shrewd_commerce_practise
	grand_army
	engineer_corps
}

historical_units = {
yuan_1st_category
kazan_swarm
kalmyk_invaders
westernized_horde
yuan_3rd_category
tartar_arch
mongol_gunpowder_warfare
mongol_reformed_infantry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Taizu" = 10
	"Ruizong" = 10
	"Taizong" = 10
	"Dingzong" = 10
	"Xianzong" = 10
	"Shizu" = 20
	"Chengzong" = 10
	"Wuzong" = 10
	"Renzong" = 10
	"Yinzong" = 10
	"Jinzong" = 10
	"Wenzong" = 10
	"Mingzong" = 10
	"Ningzong" = 10
	"Huizong" = 20
	"Zhaozong" = 10
	"Yizong" = 10
}

leader_names = {
	Hao
	Song
	Xuan
	Menglin
	Zongyan
	Yan
	Lin
	Ting
	Shixian
	Mingtai
}

ship_names = {
	Gaodi Huidi Wendi Zhaodi Zhangdi "Da Ming Guo"
	Beijing "Zeng He" Ji'nan Nanjing Hangzhou 
	Wuchang Fuzhou Nanchang Guilin Guangzhou
	Guiyang Yunnan Chendo Xi'an Taiyuan "Huang He"
	"Chang Jiang" "Xijiang" "Kung-fu tzu" Mencius
	"Xun Zi" "Yan Hui" "Min Sun" "Ran Geng" "Ran Yong"
	"Zi-you" "Zi-lu" "Zi-wo" "Zi-gong" "Yan Yan"
	"Zi-xia" "Zi-zhang" "Zeng Shen" "Dantai Mieming"
	"Fu Buji" "Yuan Xian" "Gungye Chang" "Nangong Guo"
	"Gongxi Ai" "Zeng Dian" "Yan Wuyao" "Shang Zhu"
	"Gao Chai" "Qidiao Kai" "Gongbo Liao" "Sima Geng"
	"Fan Xu" "You Ruo" "Gongxi Chi" "Wuma Shi" "Liang Zhan"
	"Yan Xing" "Ran Ru" "Cao Xu" "Bo Qian" "Gongsun Long"
}
