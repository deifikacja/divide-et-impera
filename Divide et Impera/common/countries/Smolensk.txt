#Country Name: Please see filename.

graphical_culture = easterngfx

color = { 250  165  110 }

historical_ideas = { bellum_iustum
	merchant_adventures
	national_conscripts
	shrewd_commerce_practise
	national_trade_policy
	quest_for_the_new_world
	military_drill
	battlefield_commisions
	engineer_corps
	grand_army
	espionage
}

historical_units = {
	eastern_medieval_infantry
	eastern_knights
	slavic_stradioti
	eastern_militia
	muscovite_musketeer
	muscovite_caracolle
	muscovite_soldaty
	muscovite_cossack
	russian_petrine
	russian_lancer
	russian_green_coat
	russian_cuirassier
	russian_mass w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Mikhail #2" = 20
	"Boris #1" = 20
	"Aleksandr #2" = 10
	"Yuriy #1" = 10
	"Yaroslav #2" = 5
	"Dmitriy #1" = 5
	"Ivan #2" = 5
	"Konstantin #1" = 5
	"Svyatoslav #1" = 5
	"Akim #0" = 3
	"Anastas #0" = 3
	"Anton #0" = 3
	"Arseniy #0" = 3
	"Bogdan #0" = 3
	"Daniil #0" = 3
	"David #0" = 3
	"Ferapont #0" = 3
	"Fyodor #0" = 3
	"Isaak #0" = 3
	"Kirill #0" = 3
	"Lavrentiy #0" = 3
	"Lazar #0" = 3
	"Leonti #0" = 3
	"Luka #0" = 3
	"Makariy #0" = 3
	"Mefodiy #0" = 3
	"Milan #0" = 3
	"Mitrofan #0" = 3
	"Nazariy #0" = 3
	"Nestor #0" = 3
	"Nikolai #0" = 3
	"Nikon #0" = 3
	"Pyotr #0" = 3
	"Roman #0" = 3
	"Veniamin #0" = 3
	"Vlad #0" = 3
	"Vsevolod #0" = 3
	"Yaromir #0" = 3
	"Yevgeniy #0" = 3
	"Zinoviy #0" = 3
}

leader_names = {
	Chodkiewicz Chreptowicz Czaplic Doroszenko
	Gosiewski Gaszto�d
	Hlebowicz Kuncewicz
	Korsunovas Kiszka Kossakowski Konaszewicz Chmielnicki Korecki
	Mielzynski Massalski
	Niemirowicz
	Ostrogski Orszewski Ogi�ski Or�yk
	Pac Pociej Zbaraski
	Radziwi�� Razumowski
	Sapieha Sanguszko S�dziw�j S�uszka Sosnowski Samoj�owicz Szczuka Smotrycki
	Tyszkiewicz Plater Kryski Dembi�ski Chodkiewicz Sapieha Pac Mielecki Kmita
	Wyhowski  Czartoryski
	Wi�niowiecki Wo��owicz
	Zabie��o Z�kiewski Haraburda
        Dorohobu�ski Kaszy�ski Mikuli�ski Teliatewski 
        Cho�mski Czerniaty�ski Bie�osielski Dobry�ski
        Kargo�omski Sugorski Uchtomski  
	Aleksiejew Sapieha 
        Kamie�ski Kolcow M�cis�awski Rajewski
	Szuwa�ow Szeremietiew Wasilewicz Wo�kow
        Olelkowicz Koriatowicz Korybutowicz Holsza�ski
        Gli�ski Ostrogski Radziwi�� Hlebowicz
        Chodkiewicz Wasy�kowicz �wiatos�awicz
        Olgierdowicz Borysowski Gorodne�ski
        Hercygski Izjas�awlski Drucki Podbereski
        Kukiejnowski Lagowski Lugowski Mi�ski
        Stre�ebski Witebski Ro�cis�awowicz
        Jaros�awski M�cis�awowicz Iwanowicz
        Toropiecki Aleksandrowicz Juriewicz
        Konstantynowicz Dawidowicz Dymitrowicz
        Bria�ski Putywlski Kurski Lubecki G�uchowski
        Czerkieski Homelski Wirski Olegowicz Jarope�koicz
        Wsiewo�odowicz Jaros�awowicz Romanowicz
        Maskiewicz Gaszto�d Chreptowicz
}

ship_names = {
	Yaroslavich Volga Tvertsa Mikhail Dmitry
	Kashin Kholmsky Torzhok Kashin "Vyshniy Voloche"
	Zubtsov Toropets Rzev Startsa Valdai Brosno
}
