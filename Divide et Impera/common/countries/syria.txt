#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 203  176  128 }

historical_ideas = { liberal_arts
	national_conscripts
        merchant_adventures
	shrewd_commerce_practise
	patron_of_art
	national_trade_policy
	military_drill
	divine_supremacy
	engineer_corps
	grand_army
	espionage
	glorious_arms
	cabinet
}

historical_units = {
	muslim_footsoldier
muslim_shamshir
mamluk_duel
muslim_infantry
muslim_sturm
muslim_mass_infantry
ali_bey_reformed_infantry
persian_rifle
musellem
shaybani
new_shaybani
abbasid
new_muslim_clan
ways
new_ways w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
        "al-Nasr #2" = 20
        "al-Mu'azzam #2" = 20
	"al-Salih #2" = 20
	"al-'Adil #2" = 20
	"al-Kamil #1" = 20
	"al-Ashraf #1" = 20
	"al-Afdal #1" = 15
        "Tadj #2" = 10
	"Mujir ud-Din #1" = 10
	"Djamal ud-Din #1" = 10
	"Shihab ud-Din #1" = 10
	"Shams #1" = 10
	"Saif #1" = 10
	"'Abbas #0" = 3
	"'Abd al-Rahman #0" = 3
	"Anwar #0" = 3
	"Atuf #0" = 3
	"Bakr #0" = 3
	"Fahd #0" = 3
	"Hakim #0" = 3
        "Harun #0" = 3
	"Hikmat #0" = 3
	"Hisham #0" = 3
	"Husayn #0" = 3
	"Isma'il #0" = 3
	"Jafar #0" = 3
        "Jamaal #0" = 3
	"Karim #0" = 3
        "Mal�k #0" = 3
	"Mirza #0" = 3
	"Muhammad #0" = 3
	"Rash�d #0" = 3
	"Rusul #0" = 3
	"Shimun #0" = 3
	"Tar�q #0" = 3
	"Usama #0" = 3
	"Yasir #0" = 3
	"Yusuf #0" = 3
	"Zaid #0" = 3
	"Z�yad #0" = 3
}

leader_names = {
	Damad
	Esad
	Farhad
	Hafiz
	Fazil
	Ismail
	Mehmed
	Murad
	Nasuh
	Osman
	Salih
	Sinan
}

ship_names = {
	Arif
	Azmi
	Dervish
	Fuad
	Haci
	Haydar
	Mulish
	Nuri
	Salih
	Said
	Sefveti
	Shukri
}
army_names = {
	"$PROVINCE$ Army"
}