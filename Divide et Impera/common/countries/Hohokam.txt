#Country Name: Please see filename.

graphical_culture = northamericagfx

color = { 25  25  70 } 

historical_ideas = { bellum_iustum
        military_drill
	national_conscripts
	deus_vult
	quest_for_the_new_world
}

historical_units = {
	native_indian_archer
	native_indian_tribal_warfare
	algonkin_tomahawk_charge
	commanche_swarm
	native_indian_mountain_warfare
	sioux_dragoon
	american_western_franchise_warfare
}

monarch_names = {
	"Mohe" = 20
	"Diwali" = 20
	"Waya" = 20
	"Tsiyi" = 20
	"Onacona" = 20
	"Atagulkalu" = 20
	"Degataga" = 20
        "Cheasequah" = 20
        "Adahy" = 20
        "Atagulkalu" = 20
	"Ahiga" = 20
	"Altsoba" = 20
	"Anaba" = 20
	"Ashkii" = 20
	"Ata'halne" = 20
	"Atsidi" = 20
	"Bidzill" = 20
	"Bilagaana" = 20
	"Doba" = 20
	"Doli" = 20
	"Gaagli" = 20
	"Gad" = 20
	"Hastiin" = 20
	"Hok'ee" = 20
	"Kai" = 20
	"Kilchii" = 20
	"Klah" = 20
	"Manaba" = 20
	"Naalnish" = 20
	"Naalyehe ya sidahi" = 20
	"Nakai" = 20
	"Nascha" = 20
	"Nastas" = 20
	"Niichaad" = 20
	"Niyol" = 20
	"Sani" = 20
	"Shilah" = 20
	"Shiye" = 20
	"Shizhe'e" = 20
	"Sicheii" = 20
	"Sike" = 20
	"Sik'is" = 20
	"Sahkonteic" = 20
	"T'iis" = 20
	"Tse" = 20
	"Tsiishch'ili" = 20
	"Yanaba" = 20
	"Yanisin" = 20
	"Yas" = 20
	"Yiska" = 20
}

leader_names = {
	Mohe
	Diwali
	Waya
	Tsiyi
	Onacona
	Atagulkalu
	Degataga
	Cheasequah
	Adahy
	Atagulkalu
}

ship_names = {
	"Ani-kutani" "Ah-ni-ga-to-ge-wi" "Ah-ni-gi-lo-hi" "Ah-ni-ka-wi" 
	"Ah-ni-tsi-sk-wa" "Ah-ni-sa-ho-ni" "Ah-ni-wo-di" "Ah-ni-wa-ya" 
	Kana'ti
	Selu
	"Tsul Kalu" "Oonawieh Unggi" "Ani Yuntikwalaski" "Asgaya Gigagei" 
	"I'nage-utasvhi" "Anisga'ya Tsunsdi" "Nv-da u-no-le" "Nv-da ko-la"
	"Nv-da ka-na-wo-ga" "Nv-da gu-ti-ha"
}