#Country Name: Please see filename.

graphical_culture = latingfx

color = { 114  171  184 }

historical_ideas = { early_mining
	shrewd_commerce_practise
	national_conscripts
	merchant_adventures
	humanist_tolerance
	patron_of_art
	national_bank
	national_trade_policy
	cabinet
	ecumenism
	smithian_economics
	espionage
	bureaucracy
}

historical_units = {
	#Infantry German
	w_i_armed_peasantry
	w_i_longbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_hungarian_formation
	w_i_spanish_formation
	w_i_chosen_formation
	w_i_swedish_formation
	w_i_quarter_army
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_prussian_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Wilhelm #0" = 170
	"Ludwig #1" = 40
	"Friedrich #0" = 40
	"Hermann #1" = 20
	"Karl #0" = 20
	"Moritz #0" = 20
	"Philipp #0" = 20
	"Heinrich #1" = 15
	"Otto #1" = 15
	"Adolf #0" = 10
	"Christian #0" = 10
	"Georg #0" = 10
	"Leopold #0" = 10
	"Maximilian #0" = 10
	"Philipp Ludwig #0" = 10
	"Adelmar #0" = 3
	"Albrecht #0" = 3
	"Barnabas #0" = 3
	"Denis #0" = 3
	"Egon #0" = 3
	"Erich #0" = 3
	"Gerhard #0" = 3
	"Gottlieb #0" = 3
	"Gregor #0" = 3
	"Hagan #0" = 3
	"Helmut #0" = 3
	"Isaak #0" = 3
	"Isidor #0" = 3
	"Ivo #0" = 3
	"Joachim #0" = 3
	"Joseph #0" = 3
	"Kreszens #0" = 3
	"Lothar #0" = 3
	"Oskar #0" = 3
	"Siegfried #0" = 3
	"Stefan #0" = 3
	"Thomas #0" = 3
	"Wenzel #0" = 3
	"Werner #0" = 3
	"Wolfram #0" = 3
}

leader_names = {
	Arnsberg
	Bebra
	Eder
	Fulda
	Hamm
	Kassel Knyphausen
	Lippstadt
	M�nden
	Paderborn
	"von Scheideck"
}

ship_names = {
	"Heinrich I" Kassel Werra Lahn Fulda
	Wasserkuppe Stirnberg Hanau Marburg Wetzlar
}

army_names = {
	"Armee von $PROVINCE$" 
}