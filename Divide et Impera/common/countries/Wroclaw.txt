#Country Name: Please see filename.

graphical_culture = latingfx

color = { 248 195 3 }

historical_ideas = { 
	bellum_iustum
	shrewd_commerce_practise
	merchant_adventures
	patron_of_art
	military_drill
	national_bank
	national_conscripts
	national_trade_policy
	cabinet
	smithian_economics
	scientific_revolution
	engineer_corps
	bureaucracy
}

historical_units = {
	#Infantry German Cities
	w_i_armed_peasantry
	w_i_municipal_militia
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_hungarian_formation
	w_i_dutch_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_british_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon 
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Jan #3" = 60
	"Hieronim #1" = 10
	"Piotr #1" = 20
	"�yros�aw #2" = 20
	"Heymo #1" = 10
	"Robert #2" = 20
	"Konrad #1" = 20
	"Walter #1" = 10
	"Jaros�aw #1" = 10
	"Cyprian #1" = 10
	"Wawrzyniec #1" = 10
	"Tomasz #2" = 20
	"W�adys�aw #1" = 10
	"Henryk #1" = 10
	"Wit #1" = 10
	"Lutold #1" = 10
	"Nanker #1" = 10
	"Przec�aw #0" = 10
	"Wac�aw #0" = 10
	"Jodok #0" = 10
	"Rudolf #0" = 10
	"Jakub #0" = 10
	"Baltazar #0" = 10
	"Kaspar #0" = 10
	"Marcin #0" = 10
	"Andreas #0" = 10
	"Bonawentura #0" = 10
	"Pawe� #0" = 10
	"Karol #0" = 30
	"Leopold #0" = 10
	"Sebastian #0" = 10
	"Friedrich #0" = 10
	"Franz Ludwig #0" = 10
	"Philipp Ludwig #0" = 10
	"Philipp Gotthard #0" = 10
}

leader_names = { 
	"z Malonne" Opolczyk Wroc�awski Zaremba "de Habdank" "z Kromiery�a" Nowak "z Ro�emberka" Romka "z Legnicy" "z Pogorzeli" "z Ole�nicy" "von R�desheim" Thurzo Roth "von Salza" "von Promnitz" "von Logau" Gerstmann Jerin Hahn Albert "von Sitsch" "von Rostock" "von Hessen" "zu Neuburg" "von Sinzendorf" "von Schaffgotsch"
}

ship_names = {
	Wroc�aw Odra "Ksi���-biskup" Nysa Silesia
}
