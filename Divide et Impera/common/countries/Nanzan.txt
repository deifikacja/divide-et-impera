#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 90  138  3 }

historical_ideas = { naval_transport
	merchant_adventures  
	shrewd_commerce_practise
	excellent_shipwrights 
	superior_seamanship
	national_trade_policy
	sea_hawks
	national_conscripts
	naval_fighting_instruction
	military_drill
	naval_glory
	engineer_corps
	battlefield_commisions
}

historical_units = {
	east_asian_spearmen
	eastern_bow
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	asian_musketeer
	reformed_asian_musketeer w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Sei #0" = 40
	"Toku #0" = 20
	"En Kanamura #0" = 20
	"Sen-i #0" = 20
	"Gen #0" = 20
	"Ei #0" = 20
	"Nei #0" = 20
	"Ho #0" = 20
	"Jo-Ken #0" = 20
	"Shitsu #0" = 20
	"Tei #0" = 20
	"Eki #0" = 20
	"Kei #0" = 20
	"Boku #0" = 20
	"On #0" = 20
 	"Ko #0" = 20
	"Shoken #0" = 5
	"Taikyu #1" = 1
	"Shiatsu #1" = 1
	"Cho #1" = 1
	"Hashi #1" = 1
	"Shisho #1" = 1
	"Chokei #0" = 3
	"Chuji #0" = 3
	"Ekei #0" = 3
	"Genta #0" = 3
	"Hanzo #0" = 3
	"Inori #0" = 3
	"Kaishu #0" = 3
	"Karou #0" = 3
	"Kenji #0" = 3
	"Mugen #0" = 3
	"Nagato #0" = 3
	"Noboru #0" = 3
	"Shojiro #0" = 3
	"Soun #0" = 3
	"Takeo #0" = 3
	"Yayoi #0" = 3
}

leader_names = {
	Chin
	Feng
	Haga
	Cai
	Handa
	Ke
	Hojo
	Hou
	Hiro
	Sa
}      

ship_names = {
	Bunei
	Chuzan
	Eiso Eiji
	Gusuku Gihon
	Hokuzan
	Nanzan Naha Nakijin 
	Sho Shuri Shunten Sei Satto
	Taisei Tamagusuku
	"Sho En" "Sho Shin" "Sho Hashi" "Shun Bajunki"
	"Sho Shisho" "Sho Chu" "Sho Shitatsu"
	"Sho Kinpuku" "Sho Taikyu" "Sho Toku" "Sho Sen'i"
	"Sho Sei" "Sho Gen" "Sho Ei"
	"Sho Nei" "Sho Ho" "Sho Ken" "Sho Shitsu"
}
