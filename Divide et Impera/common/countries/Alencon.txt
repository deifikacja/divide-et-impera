#Country Name: Please see filename.

graphical_culture = latingfx

color = { 162 116 225 }

historical_ideas = {
	national_conscripts
	merchant_adventures
	humanist_tolerance
	patron_of_art
	colonial_ventures
	military_drill
	quest_for_the_new_world
	cabinet
	bureaucracy
	scientific_revolution
	improved_foraging
	napoleonic_warfare
}

historical_units = {
	#Infantry France
	w_i_armed_peasantry
	w_i_crossbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_chosen_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_french_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon 
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	

}

monarch_names = {
	"Louis #0" = 80
	"Charles #1" = 60
	"Louis Philippe #0" = 40
	"Philippe #0" = 40
	"Gaston #0" = 20
	"Henri Alexandre #0" = 20
	"Nicolas #0" = 20
	"Alexandre Louis #0" = 10
	"Claudine #0" = -10
	"Fran�ois #0" = 10
	"Jean #0" = 5		
	"Antoine Philippe #0" = 5
	"Hughes #2" = 1
	"Robert #1" = 1
	"Alexandre #0" = 0
	"Ambroise #0" = 0
	"Armand #0" = 0
	"Barth�l�my #0" = 0
	"Bernard #0" = 0
	"Claude #0" = 0
	"Denis #0" = 0
	"Eudes #0" = 0
	"Gui #0" = 0
	"Guillaume #0" = 0
	"Gustave #0" = 0
	"Henri #0" = 0
	"Jacques #0" = 0
	"Jean-Baptiste #0" = 0
	"Jean-Philippe #0" = 0
	"Jules #0" = 0
	"Marc #0" = 0
	"Maurice #0" = 0
	"Maximiliem #0" = 0
	"Michel #0" = 0
	"Pierre #0" = 0
	"Raoul #0" = 0
	"Ren� #0" = 0
	"Simon #0" = 0
	"Thibaut #0" = 0
	"Xavier #0" = 0
}

ship_names = {
        Amboise Amilly Artenay Azay-le-Rideau
        Beaune-la-Rolande Beauregard Bellegarde Blois Briare
        Chalette Chambord Chamerolles Ch�teauneuf Ch�teau-Renard Ch�tillon-Coligny Ch�cy  Chenonceau Cheverny Chevilly Cl�ry-Saint-Andr� Combleux Courtenay
        Dunois
        "Fay aux Loges" Ferri�res "Fert� Saint Aubin" Fleury-les-Aubrais
        Germigny Gien
        Jargeau
        "La Bussi�re"
        Malesherbes "Meung sur Loire" Montargis
        Neuville-Aux-Bois
        Olivet Outarville Ouzouer
        Patay Pithiviers Pontchevron Puiseaux
        "Saint Beno�t" "Saint Jean de Braye" "Saint Jean le Blanc" Sully
        Talcy
        Vend�me Villesavin
}

leader_names = {
        d'Avaray
        Badoux "de Barville" "de Beaugency" "de Beauharnais" "de Beauregard" "du Bellay" "Bernard de Montebise" "de Boigneux" 
        "de Chabot" "de Ch�tillon"
        d'Ennery
        "de Gauville" "de Geresme" "Giv�s de Creusy" Garandeau "de Garlande"
        "de Laage" "de La Brisardi�re" "de La Bussi�re" "de La Chapelle" "de La Chesnaye" "de La Valette" "de L'Estoile" "Le Ma�tre" "Le Texier"
        "de Maintenon" "de Marolles" "de Martel de R�nac" "de Montboissier" "de Montigny" "de Montlh�ry"
        "de Neufcarres"
        "P�an de Saint Gilles" "de Ph�line" "de Pontville" "de Prunel�"
        "de Rancourt" "de R�m�on d'Orval" 
        "de Sabrecois" "de Saint Cosme" "de Saint Marc" "de Saint Mars" "de Salles" "Scott de Coulanges" "de Senneville" "de S�rizy"  "Seurat de la Boullaye"
        "de Tannoys"
        "de Villiers"
}

army_names = { 
         "Arm�e du Bl�sois"   "Arm�e de la Beauce" 
         "Arm�e du Dunois"    "Arm�e du G�tinais"
         "Arm�e de Sologne"      "Arm�e du Vend�mois" "Arm�e de la Loire"
	 "Arm�e de $PROVINCE$" 
}