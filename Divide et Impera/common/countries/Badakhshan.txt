#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 102  128  152 } 

historical_ideas = {
        glorious_arms 
        merchant_adventures
	national_trade_policy
        grand_army
	national_conscripts
	military_drill
	battlefield_commisions 
	shrewd_commerce_practise
	engineer_corps
	espionage
	bureaucracy
	cabinet
}

historical_units = {
persian_footsoldier
persian_shamshir
persian_duel
persian_infantry
afsharid_reformed_infantry
durrani_rifled_musketeer
ali_bey_reformed_infantry
persian_rifle
persian_cavalry_charge
qizilbash_cavalry
new_qizilbash
abbasid
new_abbasid
nadir
new_nadir w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Humar #1" = 10
	"Fakhr ud-Din #1" = 10
	"Shams ud-Din #1" = 10
	"Baha' ud-Din #1" = 10
	"Jalal ud-Din #1" = 10
	"'Ali Shah #2" = 20
	"Dawlat Shah #1" = 10
	"Sultan Bakh #1" = 10
	"Argun Shah #1" = 10
	"Shah Baha' #0" = 10
	"Muhammad Shah #0" = 10
	"Shaykh 'Ali #0" = 10
	"Bahramshah #0" = 10
	"'Ali Shah #0" = 10
	"Dawlat Shah #0" = 10
	"Sultan Bakh #0" = 10
	"Shah Sultan #0" = 10
	"Mirza Abu Bakr #0" = 10
	"Sultan Mahmud #0" = 10
	"Masud #0" = 10
	"Baysuqur Mirza #0" = 10
	"Sultan 'Ali #0" = 10
	"Muborek Shah #0" = 10
	"Nasir Mirza #0" = 10
	"Sultan Uways Mirza #0" = 10
	"Humayun #0" = 10
	"Abu Nasr #0" = 10
	"Sulaiman Shah Mirzah #0" = 10
	"Shah Rukh #0" = 10
	"Mir Yari #0" = 10
	"Sulaiman #0" = 10
	"Yusuf #0" = 10
	"Diya' ad-Din #0" = 10
	"Mir Nabat #0" = 10
	"Mirza Kalan #0" = 40
	"Zaman ad-Din #0" = 10
	"Mir Mohammad Shah #0" = 10
	"Mirza Abd al-Ghaful #0" = 10
	"Murad Beg #0" = 10
	"Mirza Sulaiman #0" = 10
	"Sultan Shah #0" = 20
	"Mir Shah Nizam ad-Din #0" = 10
	"Jan Khan #0" = 10
	"Shah Ghahan #0" = 10
	"Saman ad-Din #0" = 10
	"Ghahandar Shah #0" = 10
	"Mizrab Shah #0" = 10
	"Mahmud Shah #0" = 10
	"Shah Zade Hasan #0" = 10
	"Alim Khan #0" = 10
}

leader_names = {
	Hotaki
	Taraki
	Amin
	Karmal
	Najibullah
	Akhund
	Rahman
	Ghazi
	Ahmad
	Qadir
	"ibn Mahmud"
	"ibn Abu Said"
	"ibn Sultan Mahmud"
	"ibn Babur"
	"ibn Sultan Uways"
	Muzaffar
}

ship_names = {
	Arghandab Khash Farah Harut Harirud
	Murghab Balkh Kowkcheh Kabol Zhob
	Kech Dasht Hingol
}
