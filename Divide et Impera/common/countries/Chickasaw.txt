#Country Name: Please see filename.

graphical_culture = northamericagfx

color = { 18  139  28 }

historical_ideas = { bellum_iustum
	humanist_tolerance
	national_conscripts
	battlefield_commisions
	grand_army
	national_trade_policy
	military_drill
	cabinet
	engineer_corps
	bill_of_rights
	patron_of_art
	espionage
	shrewd_commerce_practise
}

historical_units = {
	native_indian_archer
	native_indian_tribal_warfare
	algonkin_tomahawk_charge
	native_indian_horsemen
	commanche_swarm
	creek_arquebusier
	sioux_dragoon
	american_western_franchise_warfare
}

monarch_names = {
	"Sint Holo" = 20
	"Ibofanga" = 10
	"Lamochattee" = 20
	"Yahatatastenake" = 20
	"Nunne Chaha" = 20
	"Mico Malatchi" = 20	
	"Atepa" = 10
	"Fala" = 10
	"Isi" = 10
	"Kiwidinok" = 10
	"Minco" = 10
	"Nashoba" = 10
	"Nito" = 10
	"Opa" = 10
	"Poloma" = 10
	"Talulah" = 10
	"Yaholo" = 10
}

leader_names = {
}

ship_names = {
	Ibofanga
	tvpvsvnv
	"Okhvtk� Yvhv" "Okhvtk� Nokos�" "Lvst� Nokos�" "Okhvtk� Lvmhe"
	"Cat� lvmhe" "Cat� Ayo" "Ayo-rakko" "Est-akwvnayv"
	"Esaugetuh Emissee" "Nunne Chaha" "Hisagita-imisi" "Sint Holo"
}
