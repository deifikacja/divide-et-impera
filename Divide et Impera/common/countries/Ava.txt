#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 255 102 102 }

historical_ideas = {
	national_conscripts
	glorious_arms
	military_drill
	battlefield_commisions
	shrewd_commerce_practise
	grand_army
	merchant_adventures
	national_trade_policy
	engineer_corps
	cabinet
	bureaucracy	
	national_bank
}

historical_units = {
	east_asian_spearmen
	eastern_bow
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	asian_musketeer
	reformed_asian_musketeer
	reformed_asian_cavalry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

leader_names = {
	Aung-Hla Aung-San 
	Hla-Thein 
	Khin-Nyunt Khun-Sa Kwan-Kywa 
	Maung-Gyi Maung-Win Min-Tun 
	Ohn-Myint 
	Sein-Win 
	Than-Htay Thet-Naung 
	U-Nu 
}

monarch_names = {
	"Athinkaya #1" = 10
	"Yazathinkyan #1" = 10
	"Thihathu #1" = 10
	"Uzana #2" = 20
	"Ngashishin #1" = 10
	"Kyanswange #1" = 10
	"Narathu #2" = 20
	"Uzana Pyaung #1" = 10
	"Thadominbya #1" = 10
	"Nga Nu #1" = 10
	"Minkyiswasawke #1" = 10
	"Tarabya #1" = 10
	"Nga Nauk Han #1" = 10
	"Minhkaung #2" = 20
	"Thihathu #1" = 10
	"Minhlange #1" = 10
	"Kalekyetaungnvo #1" = 10
	"Mohnyinthado #1" = 10
	"Minrekyawswa #1" = 10
	"Narapati #1" = 10
	"Thihathura #1" = 10
	"Shwenankyanshin #1" = 10
	"Thohanbwa #1" = 10
	"Hkonmaing #1" = 10
	"Mobye Narapati #1" = 10
	"Sithkyawhtin #1" = 10
}

ship_names = {
	Ava Ayeyarwady Anawrahta Alaungsithu Ananda
	Bagan Htilominlo Kyanzittha Kyaswa Kyawswa
	Lokananda "Mali Hka" Narathu Naratheinkha
	Narapatisithu Narathihapati Nyima "N'Mai Hka"
	Pyinbya Pegu Sawlu Sawhnit Sawmunnit "Sula-mani"
	"Sein-nyet Ama" "Shwe-gu-gyi" "Shwe-hsan-daw"
	"Shwe-zigon" Toungoo Tambadipa Thamudarit Tharaba
	"Tan-chi-daung Paya" "That-byin-nyu" Uzana
}
