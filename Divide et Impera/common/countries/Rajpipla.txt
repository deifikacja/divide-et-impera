#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 102 3 102 }

historical_ideas = { feudal_castles
	national_conscripts
	glorious_arms
	military_drill
	battlefield_commisions
	shrewd_commerce_practise
	grand_army
	merchant_adventures
	espionage
	engineer_corps
	national_trade_policy
	cabinet
	smithian_economics
}

historical_units = {
	indian_footsoldier
	indian_archers
	indian_arquebusier
	mughal_musketeer
	mughal_mansabdar
	indian_elephant
	indian_shock_cavalry
	rajput_musketeer
	reformed_mughal_musketeer
	reformed_mughal_mansabdar
	maharathan_guerilla_warfare
	maharathan_cavalry
	bhonsle_infantry
	bhonsle_cavalry
	indian_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Chatrasalji #1" = 10
	"Mota Verisalji #2" = 20
	"Jitsinhji #0" = 10
	"Pratapsinhji #0" = 10
	"Ajabsinhji #0" = 10
	"Gambhirsinhji #0" = 10
	"Chhatrasinhji #0" = 10
	"Vijayasinhji #0" = 20
}

leader_names = {
	Desai
	Jain
	Gandhi
	Majmudar
	Munshi
	Parikh
	Pandya
	Patel
	Patidar
	Vanik
	Vyas
	Rushi
	Saraiya
	Shaw
	Shrivastav
	Tilak
}

ship_names = {
	Ambika Auranga Bhadar Damanganga
	Devki Jalsena "Kala Jahazi" Khari
	"Lal Jahazi" Luni Mahi Mithi
	Nausena Narmada "Nav ka Yudh"
	"Nila Jahazi" Purna Sabarmati
	Sagar Saraiya Saraswati Shetrunji
	Tapi Yasmin
}
