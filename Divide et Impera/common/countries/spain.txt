#Country Name: Please see filename.

graphical_culture = latingfx

color = { 231  181  12 }

historical_ideas = {
	feudal_castles
	bellum_iustum
	quest_for_the_new_world
	divine_supremacy
	colonial_ventures
	land_of_opportunity
	national_conscripts
	vice_roys
	superior_seamanship
	regimental_system
	national_bank
	excellent_shipwrights
	improved_foraging
	napoleonic_warfare
}

historical_units = {
#Infantry Iberia
	w_i_armed_peasantry
	w_i_longbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_chosen_formation
	w_i_quarter_army
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_french_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon 
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Carlos #0" = 80
	"Felipe #1" = 60
	"Fernando #5" = 40
	"Luis #0" = 20
	"Jos� #0" = 20
	"Diego #0" = 15
	"Baltasar Carlos #0" = 15
        "Ana #0" = -15
	"Mar�a Teresa #0" = -10
	"Alfonso #11" = 10
	"Enrique #4" = 10
	"Juan #2" = 10
        "Isabel #1" = -10
	"Francisco #0" = 10
	"Tom�s #0" = 10
        "Antonio #0" = 10
	"Francisco #0 de Paula" = 10
	"Gabriel #0" = 10
	"Sancho #4" = 5
	"Jaime #2" = 5
        "Ramiro #2" = 5
	"Pedro #1" = 5
	"Pelayo #1" = 5
	"Alonso #0" = 3
	"Baltasar #0" = 3
        "Bartolom� #0" = 3
	"Claudio #0" = 3
	"Cristobal #0" = 3
	"Domingo #0" = 3
	"Emilio #0" = 3
	"Fadrique #0" = 3
	"Fausto #0" = 3
        "Garc�a #0" = 3
	"Gaspar #0" = 3
	"Gonzalo #0" = 3
	"Guillermo #0" = 3
	"Hermenegildo #0" = 3
        "Hernando #0" = 3
	"Ignacio #0" = 3
	"Jorge #0" = 3
	"Juan Bautista #0" = 3
	"Juan Jos� #0" = 3
	"Leopoldo #0" = 3
	"Lope #0" = 3
        "Manuel #0" = 3
	"Miguel #0" = 3
	"Ram�n #0" = 3
	"Rodrigo #0" = 3
	"Sebasti�n #0" = 3
        "Vicente #0" = 3
}

leader_names = {
	"Abarca de Bolea" "de Aguirre" "de Alarc�n" "�lvarez de Toledo" "de Arag�n" "de Austria" "de Ayala"
	"de Baz�n" "de Beaumont" "de Benavides" "de Borb�n" "de Borja"
	"Cabeza de Vaca" "Cagigal de la Vega" Campomanes "de la Cerda" Col�n Coronado Cort�s D�vila
	"de Egmont" Elcano Elhuyar "Fadrique de Toledo" "Fern�ndez de Castro" "Fern�ndez de C�rdoba" "de Figueroa" Fitz-James "Folch de Cardona" Fonseca
	G�lvez G�mez Gravina "de Grijalva" Grimaldi "de Guzm�n" "de Jovellanos" "de Leiva"
	"L�pe de Haro" "Manrique de Lara" "Manrique de Velasco" "de Mendoza" "de Montcada" "Nu�ez de Balboa"
	"de Ojeda" Orellana Osorio Palafox Pati�o Pimentel Pizarro "Ponce de Le�n" Portocarrero
	"de Requesens" "de Ripperd�" "de Rojas" Sastre Tellez-Gir�n "de la Vega" "de Z��iga"
}

ship_names = {
	Andaluc�a Anunciaci�n Argonauta Aurora Aventurero Bizarra Brillante Burlando
	Cacafuego Caim�n Castilla Catalina "Conde de Tolosa" Conquistador Constante "Cuervo Marino"
	Dorada Drag�n "El Retiro" Esmeralda Esperanza Fuerte Galicia Gavil�n Gaviota Glorioso Griega
	H�rcules Hermiona Infante Intr�pido Iris Jass�n Juno J�piter
	"La Carlota" "La Cazadora" "La Tetis" Le�n Luz Mercurio Neptuno
	Pena Pen�lope Peregrina "Perla de Espa�a" Princesa "Pr�ncipe de Asturias" Puercoesp�n
	"Real Carlos" "Real Familia" "Real Felipe" "Real Jorge" "Rub�" "Saeta"
	"San Andr�s" "San Antonio" "San Carlos" "San Esteban" "San Felipe" "San Fernando" "San Francisco"
	"San Gabriel" "San Gil" "San Isidoro" "San Isidro" "San Jer�nimo" "San Joaqu�n" "San Jos�" "San Juan" "San Juan Bautista"
	"San Leandro" "San Luis" "San Marcos" "San Mart�n" "San Miguel" "San P�o"
	"Santa Ana" "Santa B�rbara" "Santa Eulalia" "Santa Irene" "Santa Isabel" "Santa Justa"
	"Santa Natalia" "Santa Rita" "Santa Rosa" "Santa Susana" "Santa Teresa"
	Santiago "Sant�sima Trinidad" Sebastiana Soberano Susana Trinidad Triunfo Unicornio
	Venganza Vichero Victoria Vigilante Volante Vulcano Zara
}

army_names = {
	"Tercio de $PROVINCE$"	"Armada Real"  "Armada de Espana"
}

fleet_names = {
	"Escadra de $PROVINCE$"
}
