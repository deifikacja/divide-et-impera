#Country Name: Please see filename.

graphical_culture = easterngfx

color = { 190  15  11 }

historical_ideas = { bellum_iustum
	merchant_adventures
	national_conscripts
	church_attendance_duty
	national_trade_policy
	quest_for_the_new_world
	military_drill
	battlefield_commisions
	engineer_corps
	grand_army
	espionage
}

historical_units = {
	eastern_medieval_infantry
	eastern_knights
	slavic_stradioti
	eastern_militia
	muscovite_musketeer
	muscovite_caracolle
	muscovite_soldaty
	muscovite_cossack
	russian_petrine
	russian_lancer
	russian_green_coat
	russian_cuirassier
	russian_mass w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Grigori #3" = 40
	"Vasili #2" = 40
	"Mikhail #2" = 20
	"Yaropolk #1" = 10
	"Svyatoslav #1" = 10
	"Andrei #2" = 10
	"Aleksandr #1" = 30
	"Konstantin #1" = 10
	"Simeon #0" = 10
	"Boris #0" = 10
	"Danilo #0" = 20
	"Ivan #0" = 20
	"Fedor #0" = 10
}

leader_names = { 
	Shuisky orogobushsky Kashinsky Mikulinsky Telyatevsky Kholmsky 
	Chernyatinsky Andomsky Beloselsky Vadbolsky Dyabrinsky 
	Kargolomsky Kemsky Sugorsky Ukhtomsky Sheleshpansky Vorotynsky
}

ship_names = {
	Yaroslavich Volga Tvertsa Mikhail Dmitry
	Kashin Kholmsky Torzhok Kashin "Vyshniy Voloche"
	Zubtsov Toropets Rzev Startsa Valdai Brosno
}
