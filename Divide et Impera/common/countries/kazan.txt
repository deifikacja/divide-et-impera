#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 97  102  135 }

historical_ideas = { bellum_iustum
	glorious_arms
        national_conscripts
	military_drill
	merchant_adventures
        bureaucracy
	battlefield_commisions
	grand_army
	engineer_corps
	espionage
	national_trade_policy
	cabinet
}

historical_units = {
	yuan_2nd_category
        kazan_swarm
        kalmyk_invaders
        westernized_horde
        yuan_3rd_category
        tartar_militia
        mongol_gunpowder_warfare
        mongol_reformed_infantry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Abdullah Pasha #1" = 40
	"Talich #1" = 40
	"'Ali Beg #1" = 40
	"Ulugh Muhammad #1" = 40
	"Mahmudak #0" = 50
	"Khalil #0" = 30
	"Ibrahim #0" = 50
	"Ilham #4" = 30
	"Muhammad #0" = 50
	"Mamuk #0" = 30
	"Abd-al-Latif #0" = 20
	"Shah Ali #0" = 10
        "Sahib #0" = 30
        "Safa #0" = 30
        "Yan Ali #0" = 15
        "Utemish #0" = 30
        "Yadigar Muhammad #0" = 15
}

leader_names = { 
	Aqcurin C�lil
	G�llameva G�b�ydullina
	Nasiri N�cmetdin
	Nigm�tin Nurg�l
	Isxaqi M�rcani
	Qabayeva Qamay "Qol-G�li"
	Qursawi S�gdiy Tuqay 
	X�bibullin Yusup
	Zemfira
}

ship_names = {
	Qazgan Qazan S�yembik� Qazansu Ghiasetdin 
	"Tas Ayaq" Alat Arca G�rec C�ri Nugay
	S�yet S�yex Qazi
}
