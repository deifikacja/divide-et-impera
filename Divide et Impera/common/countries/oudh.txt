#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 122  179  97 }

historical_ideas = { early_mining
	national_conscripts
	glorious_arms
	military_drill
	battlefield_commisions
	shrewd_commerce_practise
	grand_army
	merchant_adventures
	espionage
	engineer_corps
	national_trade_policy
	bureaucracy
	cabinet
}

historical_units = {
	indian_footsoldier
	rajput_hill_fighters
	indian_arquebusier
	mughal_musketeer
	mughal_mansabdar
	indian_elephant
	indian_shock_cavalry
	rajput_musketeer
	reformed_mughal_musketeer
	reformed_mughal_mansabdar
	indian_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Muhammad Sa'adat Khan #0" = 30
	"Muhammad Safdar Jang #0" = 30
	"Shujah ad-Daula #0" = 20
	"Asaf ad-Daula #0" = 20
	"Wazir 'Ali #0" = 20
	"Sa'adat 'Ali Khan #0" = 20
	"Ghazi ud-Din Haidar #0" = 20
        "Burhan #0" = 5
        "Ahmad #0" = 5
        "Mansoor #0" = 5
        "Najib #0" = 5
        "Nasir #0" = 3
        "Nawab #0" = 0
        "'Ali #0" = 0
        "Sayid #0" = 0
        "Sa'adat #0" = 0
        "Muhammad #0" = 0
        "Safdar #0" = 0
        "Shuja #0" = 0
        "Solayman #0" = 0
        "Moinuddin #0" = 0
        "Abul Fateh #0" = 0
        "Firdaus #0" = 0
        "Khalid #0" = 0
}

leader_names = {
	Vinata
	Satsangi
	Rishmal
	Sagar
	Ranjini
	Agarkar
	Sahgal
	Senagala
	Shailendra
	Vivatma
        Bahadur
        Shah
        Muqim
        Amani
        Manzil
        Beg
}

ship_names = {
	Alia Betwa Fatimah Gandak Ghagra
	Gomti Jalsena "Kala Jahazi" Khan
	"Lal Jahazi" Nausena "Nav ka Yudh"
	Niknaz "Nila Jahazi" Niloufa Sagar
	Satlaj Yamuna Yasmine
}
