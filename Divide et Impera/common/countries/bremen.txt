#Country Name: Please see filename.

graphical_culture = latingfx

color = { 33  101  137 }

historical_ideas = { 
	naval_transport
	church_attendance_duty
	merchant_adventures
	patron_of_art
	military_drill
	national_bank
	national_conscripts
	national_trade_policy
	ecumenism
	smithian_economics
	scientific_revolution
	battlefield_commisions
	glorious_arms
	
}

historical_units = {
	#Infantry German
	w_i_armed_peasantry
	w_i_longbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_hungarian_formation
	w_i_spanish_formation
	w_i_chosen_formation
	w_i_swedish_formation
	w_i_quarter_army
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_prussian_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon 
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Heinrich #1" = 40
	"Johann #1" = 40
	"Gerhard #2" = 20
	"Friedrich #1" = 20
	"Otto #1" = 20
	"Baldwin #0" = 20
	"Christoph #0" = 20
	"Georg #0" = 20
	"Johann Adolf #0" = 20
	"Johann Friedrich #0" = 20
	"Nicolaus #0" = 20
	"Burkhard #2" = 15
	"Albrecht #1" = 15
	"Gottfried #1" = 15
	"Bernhard #1" = 10
	"Florent #1" = 10
	"Adam #0" = 3
	"Alexander #0" = 3
	"Andreas #0" = 3
	"Baldur #0" = 3
	"Bruno #0" = 3
	"Clemens #0" = 3
	"Denis #0" = 3
	"Eckhard #0" = 3
	"Ernst #0" = 3
	"Florian #0" = 3
	"Gregor #0" = 3
	"Helmfried #0" = 3
	"Helmut #0" = 3
	"Isaak #0" = 3
	"Joachim #0" = 3
	"J�rgen #0" = 3
	"Klaus #0" = 3
	"Martin #0" = 3
	"Maximilian #0" = 3
	"Michael #0" = 3
	"Rainer #0" = 3
	"Rudolf #0" = 3
	"Siegert #0" = 3
	"Siegmund #0" = 3
}

leader_names = { 
	Adler Alers Bake Beckr�ge Bergius Berninghausen 
	Boelken Bollheim Brill Delius Drewers Engels Ficke 
	Foukhard Geffken Gildemeister Grovermann Happach
	Heimbruch Hinke Kindt K�hne Krefting Laporte L�ning 
	Meyer Oelrichs Ordemann Reiners Rohde Sanders Schulken
	Steinwachs Stucken Truernitt "von Bobert" "von Cappeln" 
	"von Holten" "von Horn" "von Meinertzhagen" 
}

ship_names = {
	Roland
	Tagenbare
	Geelbeen
	"Johann Dove"
	Honorius
	Willehad
	Jever
	"Bremer Stolz"
	"Wappen von Bremen"
	"Bremer Schl�ssel"
	Weserwelle
}

army_names = {
	"Erzbischofliche Garde" "Armee von $PROVINCE$" 
}