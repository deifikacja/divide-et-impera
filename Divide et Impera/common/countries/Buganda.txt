#Country Name: Please seefilename.

graphical_culture = africangfx

color = { 65  50  240 }

historical_ideas = {
	national_conscripts
	shrewd_commerce_practise
	divine_supremacy
	national_trade_policy
	military_drill
	engineer_corps
	battlefield_commisions
	merchant_adventures
	grand_army
	espionage
}

historical_units = {
	african_spearmen
	bantu_tribal_warfare
	bantu_plains_warfare
	bantu_gunpowder_warfare
	westernized_bantu
	african_western_franchise_warfare
}


monarch_names = {
	"Tebandeke #0" = 10
	"Ndawula #0" = 10
	"Kagulu #0" = 10
	"Kikulwe #0" = 10
	"Mawanda #0" = 10
	"Mwanga #0" = 20
	"Namugala #0" = 10
	"Kyabaggu #0" = 10
	"Jjunju #0" = 10
	"Ssemakookiro #0" = 10
	"Kamaanya #0" = 10
	"Ssuna #1" = 20
	"Mukaabya #0" = 10
	"Mutebi #0" = 10
	"Kalema #0" = 10
	"Daudi Chwa #1" = 20
	"Apolo #0" = 10
	"Badru #0" = 10
	"Matayo #0" = 10
	"Manyangenda #0" = 10
	"Tebandeke #0" = 10 
        "Mujambula #0" = 10 
	"Ndawula #0" = 10
        "Nsobya #0" = 10
	"Ntambi #0" = 10 
	"Mawuuba #0" = 10 
	"Nakiyenje #0" = 10
	"Nakikofu #0" = 10 
	"Ssebanakitta #0" = 10 
	"Kagali #0" = 10 
	"Kabinuli #0" = 10 
	"Lugoloobi #0" = 10 
	"Ssendegeya #0" = 10
	"Mayembe #0" = 10
	"Kagenda #0" = 10
	"Nabbunga #0" = 10
	"Ssekayiba #0" = 10
	"Nabembezi #0" = 10
	"Kadduwamala #0" = 10
	"Katimpa #0" = 10
	"Kafumbirwango #0" = 10
	"Kimoga #0" = 10
	"Ssebuko #0" = 10
	"Migeekyamye #0" = 10
	"Kayiira #0" = 10
	"Kisomose #0" = 10
	"Mayanja #0" = 10
	"Mulere #0" = 10
	"Mukasa #0" = 10
	"Nnyonyintono #0" = 10
	"Muguluma #0" = 10
	"Apolo #0" = 10
	"Kisosonkole #0" = 10
	"Samwiri #0" = 10

}

leader_names = {
Kabinuli Ssendegeya Nabbunga Mukasa Migeekyame Kayiira Basammula-Ekere Kiwewa Muguluma Kagwa Kavuma Mpagi Mugwanya Kakungulu Kyemwa Nsibirwa Wamala Kawalya-Kagwa Kintu Nkangi Ssemwogerere Muliika Ssendaula
}

ship_names = {
	Unyamwezi Nyamwezi
}
