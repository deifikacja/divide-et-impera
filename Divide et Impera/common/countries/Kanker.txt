#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 123 120 152 }

historical_ideas = {
	national_conscripts
	glorious_arms
	military_drill
	battlefield_commisions
	shrewd_commerce_practise
	grand_army
	merchant_adventures
	espionage
	engineer_corps
	national_trade_policy
	smithian_economics
	cabinet
}

historical_units = {
	indian_footsoldier
	rajput_hill_fighters
	indian_arquebusier
	mughal_musketeer
	mughal_mansabdar
	indian_elephant
	indian_shock_cavalry
	rajput_musketeer
	reformed_mughal_musketeer
	reformed_mughal_mansabdar
	maharathan_guerilla_warfare
	maharathan_cavalry
	bhonsle_infantry
	bhonsle_cavalry
	indian_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Ghor Sai Dev #1" = 10
	"Harpal Dev #1" = 10
	"Dhiraj Singh Dev #1" = 10
	"Ram Raj Singh Dev #1" = 10
	"Shyam Singh Dev #1" = 10
	"Bhoop Dev #0" = 10
	"Padma Dev #0" = 10
	"Narhar Dev #0" = 10
	"Kamal Dev #0" = 10
	"Bhanupratap Dev #0" = 10
}

leader_names = {
	Konda
	Kambli
	Duranjaya
	Bhatt
	Chande
	Boparai
	Charan
	Godambe
	Amra
	Joardar
}

ship_names = {
	Amitha Amman Arul Baitarani
	Balasore Balugaon Brahmani
	Chellam Devi Durga Juggernaut
	Lakshmi Mahanadi Mou Nilima
	Paradeep Parvati Radha Sarasvati
	Sharmistha Tuli
}
