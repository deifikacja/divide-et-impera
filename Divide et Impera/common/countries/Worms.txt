#Country Name: Please see filename.

graphical_culture = latingfx

color = { 254 80 43 }

historical_ideas = { liberal_arts
	shrewd_commerce_practise
	national_conscripts
	merchant_adventures
	humanist_tolerance
	patron_of_art
	national_bank
	national_trade_policy
	ecumenism
	cabinet
	smithian_economics
	scientific_revolution
	espionage
}

historical_units = {
	#Infantry German Cities
	w_i_armed_peasantry
	w_i_municipal_militia
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_hungarian_formation
	w_i_dutch_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_british_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon 
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Friedrich #0" = 100
	"Ludwig #2" = 80
	"Ruprecht #2" = 20
	"Johann Wilhelm #0" = 20
	"Karl #0" = 20
	"Karl #0 Ludwig" = 20
	"Karl #0 Philipp" = 20
	"Karl #0 Theodor" = 20
	"Otto Heinrich #0" = 20
	"Philipp #0" = 20
	"Philipp Wilhelm #0" = 20
	"Heinrich #6 Friedrich" = 15
	"Alexander Siegmund #0" = 15
	"Friedrich #0 Philipp" = 15
	"Johann Friedrich #0" = 15
	"Ludwig Anton #0" = 15
	"Ludwig Philipp #0" = 15
	"Hermann #3 Ludwig" = 10
	"Wilhelm #1" = 10
	"Albrecht #0" = 10
	"Christoph #0" = 10
	"Eduard #0" = 10
	"Franz Ludwig #0" = 10
	"Friedrich #0 Wilhelm" = 10
	"Georg #0" = 10
	"Gustav Adolf #0" = 10
	"Johann Kasimir #0" = 10
	"Ludwig Wilhelm #0" = 10
	"Moritz #0" = 10
	"Moritz Christian #0" = 10
	"Richard #0" = 10
	"Wolfgang Georg #0" = 10
	"Gottfried #1" = 5
	"Bruno #0" = 3
	"Gottschalk #0" = 3
	"Hieronymus #0" = 3
	"Ignatz #0" = 3
	"Markus #0" = 3
	"Raphael #0" = 3
	"R�diger #0" = 3
}

leader_names = {
	Blittersdorf
	"von Gelnhausen"
	"von Sch�neck"
	"von Dieblich"
	C�lln
	Bensenraede
	Adenau
	Gruithausen
	Hohenfeld
	"von Namedy"
	Falkenberg
	"von Clausbruch"
	Hillensberg
	Dammerscheidt
	Elmpt
	Naxleden
	Schaluyn
	"von Leyen"
	Ansembourg
	Zebnitsch
}

ship_names = {
	Neustadt Kaiserslautern Elmstein Landstuhl
	Rhodt Deidesheim Hochspeyer Neunkirchen
	Wolfstein Winnweiler
}


army_names = {
	"Kurf�rstliche Garde" "Armee von $PROVINCE$" 
}

fleet_names = {
	"Kurf�rstliche Flotte"
}
