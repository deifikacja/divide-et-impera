#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 126  53  77 }

historical_ideas = {
        excellent_shipwrights
	merchant_adventures
	national_conscripts
        bureaucracy
	glorious_arms
	superior_seamanship
	military_drill
	national_trade_policy
	patron_of_art
	shrewd_commerce_practise
	cabinet
	grand_army
}

historical_units = {
	east_asian_spearmen
	eastern_bow
	asian_arquebusier
	asian_charge_cavalry
	asian_mass_infantry
	asian_musketeer
	reformed_asian_musketeer
	reformed_asian_cavalry w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
        "Goeulla" = 10
	"Geon" = 10
	"Samgye" = 10
	"Ilmang" = 10
	"Doje" = 10
        "Eongyeong" = 10
        "Bomyeong" = 10
        "Haengcheon" = 10
        "Hwan" = 10
	"Wi" = 10
	"Sik" = 10
	"Uk" = 10
	"Hwang" = 10
        "Yeong" = 10
        "HU" = 10
	"Dumyeong" = 10
	"Seonju" = 10
	"Jinam" = 10
	"Seongbang" = 10
	"Munseong" = 10
	"Ik" = 10
	"Jihyo" = 10
	"Suk" = 10
	"Hyeonbang" = 10
	"Gi" = 10
	"Dam" = 5
	"Jiun" = 5
	"Seo" = 5
	"Damyeong" = 5
	"Dam" = 5
	"Jium" = 5
	"Hyejeong #0" = 5
	"Hyohye #0" = 5
	"Uihye #0" = 5
	"Hyosun #0" = 5
	"Gyeonghyeon #0" = 5
	"Insun #0" = 5
	"Jeongsin #0" = 5
	"Jeonghye #0" = 5
	"Jeongsuk #0" = 5
	"Jeong-an #0" = 5
	"Jeonghyu #0" = 5
	"Jeongmyeong #0" = 5
	"Dan #1" = 1
	"Mun #1" = 1
	"Tae #2" = 1
	"Hwan #1" = 1
	"Do #1" = 1
	"Ik #1" = 1
	"Mok #1" = 1
	"Si #1" = 1
	"I #0" = 3
	"Guem #0" = 3
	"Sun #0" = 3
	"Rip #0" = 3
	"Il #0" = 3
	"Dong #0" = 3
	"Gwai #0" = 3
	"Sang #0" = 3
	"Ji #0" = 3
	"U #0" = 3
	"Gyu #0" = 3
	"Dae #0" = 3
	"Gu #0" = 3
}

leader_names = {
	"- wang"
}

ship_names = {
	Baekdu Baekje 
	Changbai
	Duman Daedeok Dongmyeongseong
	Gayasan Geumgang Geumho Goguryeo "Gyeon Hwon"
	Jirisan Jeongjong
	Kumgangsan
	Nakdong
	Sincheon Silla Sobaek
	Taebaek Taejo 
	Hanseong Haemosu Habaek Hwaeomsa
	Worak
	Yuhwa Yeong 
	"Jeong Do-jeon" "Yi Seong-gye"
}
