#Country Name: Please see filename.

graphical_culture = latingfx

color = { 153  122  108 }

historical_ideas = { 
	feudal_castles
	national_conscripts
	grand_army
	glorious_arms
	military_drill
	battlefield_commisions
	espionage
	engineer_corps
	national_bank
	ecumenism
	national_trade_policy
	shrewd_commerce_practise
	bureaucracy
}

historical_units = {
	#Infantry German
	w_i_armed_peasantry
	w_i_longbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_hungarian_formation
	w_i_spanish_formation
	w_i_chosen_formation
	w_i_swedish_formation
	w_i_quarter_army
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_prussian_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon 
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Friedrich #0" = 20
	"Otto #0" = 20
	"Heinrich #0" = 15
	"Werner #0" = 15
	"Ferdinand #0" = 10
	"Karl #0" = 10
	"Alfred #0" = 0
	"Alois #0" = 0
	"Hans #0" = 0
	"Andreas #0" = 0
	"David #0" = 0
	"Eduard #0" = 0
	"Jonas #0" = 0
	"Josef #0" = 0
	"Benedikt #0" = 0
	"Christoph #0" = 0
	"Franz #0" = 0
	"Dietrich #0" = 0
	"Bartholom�us #0" = 0
	"Hieronymus #0" = 0
	"Engelbert #0" = 0
	"Florian #0" = 0
	"Gerhard #0" = 0
	"Immanuel #0" = 0
	"Johann #0" = 0
	"Kaspar #0" = 0
	"Leonhard #0" = 0
	"Alexander #0" = 0
	"Markus #0" = 0
	"Kastor #0" = 0
	"Nikolaus #0" = 0
	"Peter #0" = 0
	"Paul #0" = 0
	"Ruprecht #0" = 0
	"Stefan #0" = 0
	"Thomas #0" = 0
	"Ulrich #0" = 0
	"Urs #0" = 0
	"Vinzenz #0" = 0
	"Andr� #0" = 0
	"Charles #0" = 0
	"Didier #0" = 0
	"Emmanuel #0" = 0
	"Fr�d�ric #0" = 0
	"G�rard #0" = 0
	"Henri #0" = 0
	"Stephan #0" = 0
	"Pierre #0" = 0
	"Fran�ois #0" = 0
	"Jules #0" = 0
	"Matthieu #0" = 0
	"Nicolas #0" = 0
	"Pascal #0" = 0
	"Vincent #0" = 0
	"Claudio #0" = 0
	"Edmondo #0" = 0
	"Anton #0" = 0
	"Leonardo #0" = 0
	"Timoteo #0" = 0
	"Wolfgang #0" = 0
	"Markward #0" = 0
	"Felix #0" = 0
	"Konrad #0" = 0
	"Michael #0" = 0
	"Leonz #0" = 0
	"Simon #0" = 0
	"Theodor #0" = 0
	"C�lestin #0" = 0
	"Leodegar #0" = 0
	"Georg #0" = 0
	"Laurenz #0" = 0
        "Arnold #0" = 0
        "Anselm #0" = 0
        "Tobias #0" = 0
        "Balthasar #0" = 0
        "Christof #0" = 0
        "Xavier #0" = 0
        "Ignaz #0" = 0
        "Werner #0" = 0
        "Fridolin #0" = 0
}

leader_names = {
	Aebi Amman Bischofberger Blank Blies Brugger Combe "von Berau"
	Eugster Frei Frisch Gasenzer Gr�neisen Heberlin "von Meyenburg-Rausch"
	Helberingen Hertzler Hiltbrand Honegger Luz Miller Holl�nder
	Munzinger Oberholzer Schwendimann Sp�rri Vogt Siegerist Gysel W��rner
	"von Baltharsar" W�chter "von Hasseln" Zumbach "von Waldkirch"
        "de Villars" Zahringen Freiburg Orleans Legrand Thurn Ammann Pfyl
        "de La Harpe" layre Oberlin Ochs Bay Dolder Frisching Stokar Senn
        Savary Finsler Zimmermann Schmid R�ttiman Usteri Harder Stierlin
        "von Biberegg" "von Wattenwyl" "d'Affry" "von Glutz-Ruchti"
        Merian Reinhard "von Wartenfels" "von Reinhard" "von Wyss" K�chlin
        "von M�linen" "von Meyenburg-Stokar" "von Altishoven" Hess Hirzel Tavel
        Tscharner Kopp "von Muralt" Neuhaus "Siegwart-M�ller" Furrer Gilg
        Zehnder Funk Ochsenbein Schneider N�ff St�mpfli "Frey-H�ros�" K�ndig
        Kn�sel Dubs Schenk Fornerod Welti C�r�sole Scherer Heer Droz Schorno
        "von Salis-Soglio" Feer H�nerwadel Herzog Rothpletz Attenhofer
        Weissenbach Suter Fetzer Herzog H�rner Wieland Siegfried Burch
        Feld Wirz Anderhalden "von Fl�e" Stockmann Bucher "von Rudenz"
        Spichtig Britschgi Hermann Michel Imfeld Schlinder Rhyn Crivelli
        Fassbind Ackermann Morlot Schumacher "von Wattingen" B�ler
        "von Rickenbach" Paravicini Utiger Escher Kilchberger Segesser Holdener
        Epp Streiff "von Glas" G�ldli Hauser Weber Sp�ndli Hallauer
        "von Graffenried" Pfyffer Jauch Zilcher Gonzenbach Anderwert Tobler
        Mettler Kreuel Zwicky Thormann Waser Nabholz Willading Reichmuth
        Locker Lentulus Baldinger Scheuchzer Holzhalb Dulliker Ceberg Wepfer
        Abegg Blattmann Fahrl�nder Sturzenegger Tanner Gruber Zellweger Ehrler
        Alther Z�rcher R�sch Nef Nagel Oertli Geiger Speck Sciess Wecken Ceberg
        Hersche Mauser Br�lmann D�hler F�ssler Rechsteiner Broger St�helin
        Socin Burckhardt "de Bary" Hagenbach Mitz Ryhinder Wieland Zschokke
        Sarasin Wenk Frey Gutzwiller Plattner "von Sinner" Graffenried Pfister
        "von Erlach" Joneli Bellerive Zeerlander "von Effinger" Kurz B�ren Nider�st
        Roter Scholl Wildermett Juillerat Cu�nat Schwaller P�riat Staal Voirol
        S�mon "von Meyenburg" Joliat Deluce Monnin Imer "de Gl�resses" Ch�telain Philistorff
        Herbort "de L�chelles" "de Montenach" May "de Lenzburg" Ott Otth Schroeder
        Rodt Fivaz Jenner Gasser Hartmann Ratz� Diesbach "de Lanthen-Heid" Etlin
        "de Diesbac" Schmalz Steiger Gady "de Granges" "von St�rler" Techtermann
        Kaiser Betschart Scolar Bitzin Reding Russy "von Spiringen" "von Rulberg"
        Stricker Gering Kammer J�tz Kyd D�rrer Zberg Wyrsch Wolleb Gut B�schenstein
        Gamma Leu Janser Dunat Oschwald Staub P�ntener Krus Legler Blattler Hug
        B�chli D�rholz W�erner Gidler Gross Peyer Sury Turer Straumeyer "von Matt"
        Amb�el "von Roten" Burgener Kreig Grand Meschler Ritter Werra Willa Oggier
        Kalbermatten Zmillachren Rothen Raron Matter Steffen Ugspurger Knecht Weck
        Rot St�rler Jacob Ernst Haller Mutach Naegeli Fellenberg Dachselhofer Imhof
        Rodt Euster Hegglin Zurlauben Letter Kolin Roost Brandenburg "von Hochreut"
}

ship_names = {
	Uri Schwyz Unterwalden Glarus Zug
	Lucerne Z�rich Berne "Wilhelm Tell"
	Dufour Rhein Rh�ne Inn Ticino
}

army_names = {
	"Armee von $PROVINCE$" 
}