#Country Name: Please see filename.

graphical_culture = latingfx

color = { 128  202  129 }

historical_ideas = { liberal_arts
	national_conscripts
	church_attendance_duty
	divine_supremacy
	merchant_adventures
	patron_of_art
	national_bank
	engineer_corps
	national_trade_policy
	shrewd_commerce_practise
	cabinet
	espionage
	bureaucracy
}

historical_units = {
#Infantry Italy
	w_i_armed_peasantry
	w_i_crossbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_condottiere
	w_i_halberdiers
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_spanish_formation
	w_i_dutch_formation
	w_i_swedish_formation
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_french_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Ranuccio #0" = 40
	"Alessandro #0" = 20
	"Antonio #0" = 20
	"Carlo #0" = 20
	"Ferdinando #0" = 20
	"Filippo #0" = 20
	"Francesco #0" = 20
	"Odoardo #0" = 20
	"Ottavio #0" = 20
	"Pier Luigi #0" = 20
	"Lodovico #0" = 15
	"Maria Luigia #0" = -15
	"Isabella #0" = -10
	"Orazio #0" = 10
	"Pietro #0" = 10
	"Achille #0" = 3
	"Alberto #0" = 3
	"Benedetto #0" = 3
	"Bernardo #0" = 3
	"Ciro #0" = 3
	"Claudio #0" = 3
	"Daniele #0" = 3
	"Demetrio #0" = 3
	"Enrico #0" = 3
	"Fabio #0" = 3
	"Flavio #0" = 3
	"Gaetano #0" = 3
	"Graziano #0" = 3
	"Luciano #0" = 3
	"Manfredo #0" = 3
	"Mario #0" = 3
	"Modesto #0" = 3
	"Nestore #0" = 3
	"Ottaviano #0" = 3
	"Prudenzio #0" = 3
	"Roberto #0" = 3
	"Samuele #0" = 3
	"Sesto #0" = 3
	"Valerio #0" = 3
	"Vincenzo #0" = 3
}

leader_names = {
        Anguissola Anvidi "d'Appiani d'Aragona" d'Asburgo
	Bajardi Barattieri Bernini Bertorella Bianconese "di Borbone" Borghese
	Cantelli "di Castagnola" Cavagnari Cavalcab� Cella Cerati Conaccia Confallonieri
	Draghi Emanueli "di Farnese" Fumagalli Gioia Landi Lemignano Linati Lombardi
	Manfredi Marazzani Marescalchi "Meli Lupi"
	Paletti Pallavicini Pallieri Piatti Pontremoli Riva Rivalta "dalla Rosa" Rossi
	Salati Sanseverino Sanvitale Schizzati Torelli Torrechiara
	Ventura Verdi "dal Verme"
}

ship_names = { 
	Alberi 
	"Benedetto Antelami" "Bernabo' Visconti"
	Calestani Carignano Casaltone Casagnola "Case Capelli" Castalletto Cavanna Certosa Cusani
	"donna Egidia"
	Eia 
	"Fiumi Po" Fontanini 
	Gaione "Ghiaiata Nuova" 
	Montanara Molinetto
	Panocchia Pedrignano Pilastrello Ponte Porporano 
	Quercioli 
	"Rolando Taverna" "Ronco Pascolo" 
	"San Francesco del Prato" "San Lazzaro" "San Leonardo" "San Martino dei Bocci" "San Pancrazio" "San Pietro Apostolo" "San Prospero" "San Ruffino" "Santa Coce" Sant'Ulderico Sissa 
	Valera Valserena "vescovo C�dalo" "vescovo Guiberto" Viazza Vigolante
}

army_names = {
	"Armata de $PROVINCE$"
}