#Country Name: Please see filename.

graphical_culture = chinesegfx

color = { 120  190  60 }

historical_ideas = { feudal_castles
	merchant_adventures  
	excellent_shipwrights 
	shrewd_commerce_practise 
	superior_seamanship
	national_trade_policy
	naval_fighting_instruction
	sea_hawks
	national_conscripts
	military_drill
	naval_glory 	
}

historical_units = {
	east_asian_spearmen
	eastern_bow
	asian_arquebusier
	asian_musketeer
	reformed_asian_musketeer w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Jaya #1" = 10
	"Paramaswara #1" = 10
	"Sri #1" = 10
	"Ibrahim #1" = 10
	"Khoja #1" = 10
	"Jamal #1" = 10
	"Mudari #1" = 10
	"Mara #1" = 10
	"Aladdin #1" = 10
	"Sulaiman #1" = 10
	"Hassan #1" = 10
	"'Abdu'l Jalil #1" = 10
	"Mahmud 'Abd'ul Jalil #1" = 10
	"Ismail 'Abd'ul Jalil #1" = 10
	"Yahya 'Abd'ul Jalil #1" = 10
	"'Ali 'Abd'ul Jalil #1" = 10
	"Ibrahim 'Abd'ul Jalil #1" = 10
	"Ismail 'Abd'ul Jalil #0" = 10
	"Hashim 'Abd'ul Jalil #0" = 10
	"Sayyid #0" = 10
	"Tengku #0" = 10
	"Kasim #0 'Abd'ul Jalil" = 10
}

leader_names = {
	Iskander
	Ahmad
	Muhammad
	Rahmad
	"ud-Din Muzzafar"
	ud-Din
	"ud-Din Ri'ayat"
	Mu'azzam
	Muzzafar
	"Saif ud-Din"
	"Khalil ud-Din"
	"Saif ul-'Alam"
	"'Ali"
	Besar
}

ship_names = {
	Pendet Legong Baris Topeng Barong
	Kecak Batur Catur Bratan Pohen Lesong
	Sangyang Batukaru Patas Musi Mesehe
	Agong Seraya Merbuk Sanglang Kelatakan
}
