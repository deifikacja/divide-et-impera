#Country Name: Please see filename.

graphical_culture = latingfx

color = { 117  161  67 }

historical_ideas = { 
	liberal_arts
	shrewd_commerce_practise
	national_conscripts
	merchant_adventures
	humanist_tolerance
	patron_of_art
	national_bank
	national_trade_policy
	cabinet
	smithian_economics
	scientific_revolution
	military_drill
	battlefield_commisions
}

historical_units = {
	#Infantry German
	w_i_armed_peasantry
	w_i_longbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_harquebusiers
	w_i_halberdiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_hungarian_formation
	w_i_spanish_formation
	w_i_chosen_formation
	w_i_swedish_formation
	w_i_quarter_army
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_prussian_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon 
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Bernhard #0" = 70
	"Christoph #0" = 40
	"Karl #0" = 40
	"Jakob #0" = 50
	"Philipp #1" = 30
	"August Georg #0" = 20
	"Eduard Fortunat #0" = 20
	"Ernst Friedrich #0" = 20
	"Georg Friedrich #0" = 20
	"Karl Friedrich #0" = 20
	"Ludwig #0" = 20
	"Ludwig Georg #0" = 20
	"Ludwig Wilhelm #0" = 20
	"Philibert #0" = 20
	"Wilhelm #0" = 20
	"Ferdinand Maximilian #0" = 15
	"Hermann Fortunat #0" = 15
	"Karl Joseph #0" = 15
	"Karl Leopold #0" = 15
	"Karl Ludwig #0" = 15
	"Leopold Wilhelm #0" = 15
	"Wilhelm Georg #0" = 15
	"Rudolf #4" = 10
	"Albrecht #0" = 10
	"Albrecht Karl #0" = 10
	"Christoph Gustav #0" = 10
	"Ernst #0" = 10
	"Georg #0" = 10
	"Johann #0" = 10
	"Johann Karl #0" = 10
	"Markus #0" = 10
	"Philipp Siegmund #0" = 10
	"Wilhelm Ludwig #0" = 10
	"Wolfgang #0" = 10
	"Albrecht #0" = 3
	"Erich #0" = 3
	"Gregor #0" = 3
	"Isaak #0" = 3
	"Oskar #0" = 3
	"Wolfram #0" = 3
}

leader_names = { 
	Albers B�chtold Baehr Beer Benthens Breh Eisenkolb Erb
	Ferstenfeld Fischer G�hringer Grether Heitzmann Helff 
	Kempf Klee Mackensen R�mermann SCh��ler Steinmann 
	Tientjen Tuve Wachsmuth
}

ship_names = {
	"Marktgraf Hermann" "Marktgraf Bernard I" "Marktgraf Karl I"
	Linzgau L�rrach Freiburg Karlsruhe Mannheim Tauber Rhein
	Bruchsal Bretten Constance Eberbach Feldberg Heidelberg
	Kinzig Lahr Offenburg Rastatt Villingen
}

army_names = {
	"Markgr�fische Garde" "Armee von $PROVINCE$" 
}