#Country Name: Please see filename.

graphical_culture = latingfx

color = { 60  133  142 }

historical_ideas = { 
	early_mining
	national_conscripts
	church_attendance_duty
	military_drill
	patron_of_art
	grand_army
	engineer_corps
	national_bank
	high_technical_culture
	battlefield_commisions
	scientific_revolution
	glorious_arms
	cabinet
}

historical_units = {
	#Infantry Switzerland
	w_i_armed_peasantry
	w_i_crossbowmen
	w_i_mercenary
	w_i_battle_formation
	w_i_condottiere
	w_i_halberdiers
	w_i_landsknechte
	w_i_arquebusiers
	w_i_pikemen
	w_i_musketeers
	w_i_hungarian_formation
	w_i_spanish_formation
	w_i_swedish_formation
	w_i_quarter_army
	w_i_reformed_pikemen
	w_i_german_line
	w_i_english_line
	w_i_grenadiers
	w_i_carabiniers
	w_i_chasseur_a_pied
	w_i_fusiliers
	w_i_british_fusiliers
	w_i_shock_tactics
	w_i_tirailleur
	w_i_reformed_grenadiers
	w_i_voltigeurs
	w_i_reformed_fusiliers 
	w_i_refomerd_voltigeurs
#Cavalry
	w_c_feudal_knights
	w_c_lances_fournies
	w_c_mounted_crossbowmen
	w_c_harquebusiers
	w_c_dragoons
	w_c_caracole
	w_c_reiters
	w_c_carabiniers
	w_c_reformed_reiters
	w_c_mounted_musketeers
	w_c_half_cuirassiers
	w_c_hussars
	w_c_mounted_grenadiers
	w_c_cuirassiers
	w_c_lancers
	w_c_reformed_dragoons
	w_c_uhlans
	w_c_reformed_hussars
	w_c_chasseurs_a_cheval
	w_c_reformed_cuirassiers
	w_c_chevau-leger
	w_c_reformed_uhlans
	w_c_reformed_lancers
	w_c_reformed_chasseurs
#Artillery
	w_a_bombard
	w_a_fowler
	w_a_houfnice
	w_a_mortar
	w_a_bronze_mortar
	w_a_early_culverin
	w_a_swivel_cannon 
	w_a_kartouwe
	w_a_falconet
	w_a_culverin
	w_a_leather_cannon
	w_a_late_culverin
	w_a_iron_cannon
	w_a_light_artillery_3-pounder
	w_a_heavy_artillery_12-pounder
	w_a_light_artillery_6-pounder
	w_a_heavy_artillery_16-pounder
	w_a_horse_artillery
	w_a_field_artillery
	w_a_siege_artillery	
}

monarch_names = {
	"Ferdinand #0" = 40
	"Friedrich #1" = 20
	"Ernst #0" = 20
	"Karl #0" = 20
	"Wilhelm #0" = 20
	"Johann Karl #0" = 15
	"Leopold #1" = 15
	"Cristoph #0" = 15
	"Maximilian Ernst #0" = 15
	"Albrecht #3" = 10
	"Rudolf #2" = 10
	"Leopold #1 Wilhelm" = 10
	"Alexander #0" = 10
	"Johann #0" = 10
	"Adalbert #0" = 3
	"Benedikt #0" = 3
	"Benno #0" = 3
	"Bruno #0" = 3
	"Christian #0" = 3
	"Clemens #0" = 3
	"Florian #0" = 3
	"Gerhard #0" = 3
	"Gregor #0" = 3
	"G�nther #0" = 3
	"Heinrich #0" = 3
	"Hermann #0" = 3
	"Humbert #0" = 3
	"Immanuel #0" = 3
	"Isidor #0" = 3
	"Jakob #0" = 3
	"Joachim #0" = 3
	"Michael #0" = 3
	"Oskar #0" = 3
	"Otto #0" = 3
	"Siegfried #0" = 3
	"Stefan #0" = 3
	"Ulrich #0" = 3
	"Viktor #0" = 3
	"Wilfried #0" = 3
	"Wolfgang #0" = 3
}

ship_names = { 
	Graz
	Leoben
	Leibnitz
	Weiz
	Steiermark
	Eisenerz
	Bruck
	M�rztal
	Greim
	Kapfenberg 
}

leader_names = { 
	Schmidtberger Maxa Kroha Herndler Almer Wergals 
	Trenkler Zingl Absenger Allmer Teubenbacher Fuchs Hierzegger Lanz Messner
	Potetz Altmann Andreitsch Amtmann Arlitz Ruckenstuhl Steinegger
}

army_names = {
	"Armee von $PROVINCE$" 
}
