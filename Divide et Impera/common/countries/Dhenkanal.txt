#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 210  10  7 }

historical_ideas = { feudal_castles
	excellent_shipwrights
	superior_seamanship
	shrewd_commerce_practise
	merchant_adventures
	naval_fighting_instruction
	national_trade_policy
	battlefield_commisions
	sea_hawks
	military_drill
	bureaucracy
	smithian_economics
	espionage
}

historical_units = {
	indian_footsoldier
	rajput_hill_fighters
	indian_arquebusier
	indian_elephant
	indian_shock_cavalry
	rajput_musketeer
	south_indian_musketeer
	mughal_mansabdar
	reformed_mughal_musketeer
	reformed_mughal_mansabdar
	tipu_sultan_rocket
	indian_rifle w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Nru Singh #1" = 10
	"Kunja Behari #1" = 10
	"Braja Behari #1" = 10
	"Damodar #1" = 10
	"Dayanidhi #1" = 10
	"Ramchandra #1" = 10
	"Krishna Chandra #1" = 10
	"Shyam Chandra #0" = 10
	"Bhagirath Mahendra #0" = 10
	"Dinabhandu Mahendra #0" = 10
	"Shura Pratap #0" = 10
	"Shankar Pratap Singh #0" = 10
}

leader_names = {
	Bhramarbar
	Mahendra
	Bhowarbar
	Bidyadhar
	Dassa
	Deo
	Kalinga
	Mansingh
	Patra
	Rai
	Ratha
	Roy
	Santra
	Singhar
	Sinha
	Uria
	Vansa
}

ship_names = {
	Baitarani Balasore Balugaon Behrampur
	Bhubeneswar Brahmani Chandipur Chilka
	Cuttack Durga Gopalpur Jagannath
	Juggernaut Kalijai Konark Lakshmi
	Mahanadi Nalabana Paradeep Parvati
	Puri Radha Sarasvati Satapada
}
