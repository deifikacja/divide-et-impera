#Country Name: Please see filename.

graphical_culture = muslimgfx

color = { 52  118  12 } 

historical_ideas = { feudal_castles
	national_conscripts
	glorious_arms 
	grand_army
	military_drill
	battlefield_commisions 
	shrewd_commerce_practise
	engineer_corps
	merchant_adventures
	national_trade_policy
	espionage
	bureaucracy
	cabinet
}

historical_units = {
	muslim_footsoldier
muslim_shamshir
mamluk_duel
tofongchis_musketeer
muslim_sturm
durrani_rifled_musketeer
ali_bey_reformed_infantry
persian_rifle
cavalry_archers
swarm
new_swarm
new_steppe
durrani_swivel
durrani_dragoon w_a_bombard w_a_fowler w_a_houfnice w_a_mortar w_a_bronze_mortar w_a_early_culverin w_a_swivel_cannon w_a_kartouwe w_a_falconet w_a_culverin w_a_leather_cannon w_a_late_culverin w_a_iron_cannon w_a_light_artillery_3-pounder w_a_heavy_artillery_12-pounder w_a_light_artillery_6-pounder w_a_heavy_artillery_16-pounder w_a_horse_artillery w_a_field_artillery w_a_siege_artillery
}

monarch_names = {
	"Suurgatmish #0" = 10
	"Masud #0" = 10
	"Karuchar #0" = 10
	"Abu #0" = 10
	"Ulugh Beg #0" = 10
	"Zu-u-Nun Beg #0" = 10
	"Muhammad #0" = 40
	"Sultan #0" = 10
	"Ayyub #0" = 10
	"Yar Muhammad #0" = 10
	"Dost Muhammad #0" = 10
	"Sher Ali #0" = 10
	"Wali #0" = 10
         "'Abd al-Karim #0" = 3
	"Ayman #0" = 3
	"Barakat #0" = 3
	"Basir #0" = 3
	"Dawud #0" = 3
	"Fahd #0" = 3
	"Faris #0" = 3
	"Faruq #0" = 3
	"Firdaus #0" = 3
	"Fuad #0" = 3
	"Haidar #0" = 3
	"Hashim #0" = 3
	"Husam #0" = 3
	"Iqbal #0" = 3
	"Jabbar #0" = 3
	"Ja'far #0" = 3
	"Jamal #0" = 3
	"Junayd #0" = 3
	"Khaliq #0" = 3
	"Malik #0" = 3
	"Mansur #0" = 3
	"Mustafa #0" = 3
	"Najib #0" = 3
	"Qasim #0" = 3
	"Qusay #0" = 3
	"Rahim #0" = 3
	"Rash�d #0" = 3
	"Safi #0" = 3
	"Sakhr #0" = 3
	"Samir #0" = 3
	"Tariq #0" = 3
	"Umar #0" = 3
	"Wahid #0" = 3
	"Zahir #0" = 3
}

leader_names = {
"ibn Shah Rukh" "al-Quasim Babur" al-Quasim Babur Argun al-Razzak "ibn Umar Shaykh" "ibn Umar" "ibn Shaykh" ad-Din "'Ali Khan" "ibn Painda" Muhammad Afzal Azam Mohammed
}

ship_names = {
	Hotaki
	Taraki
	Amin
	Karmal
	Najibullah
	Akhund
	Rahman
	Ghazi
	Ahmad
	Qadir
}
