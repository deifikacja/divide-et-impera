country_event = {

	id = 1104

	trigger = {
		NOT = { num_of_cities = 2 }
		SWI = { NOT = { is_subject = yes } }
		SWI = { NOT = { is_lesser_in_union = yes } }
		NOT = { has_country_flag = swiss_accession_declined }
		any_neighbor_country = { tag = SWI }
		any_neighbor_country = { num_of_cities = 4 }
		relation = { who = SWI value = 100 }
		is_emperor = no
		OR = {
			culture_group = germanic
			culture_group = french
			culture_group = latin
			culture_group = netherlands
		}
	}
	
	mean_time_to_happen = {
	
		months = 10000
		modifier = { 
			factor = 0.9
			relation = { who = SWI value = 160 }
			}
		modifier = { 
			factor = 0.8
			primary_culture = rheinlaender
			}
		modifier = { 
			factor = 1.5
			culture_group = netherlands
			}
		modifier = { 
			factor = 0.4
			THIS = { vassal_of = SWI }
			}
		modifier = { 
			factor = 0.8
			junior_union_with = SWI
			}
		modifier = { 
			factor = 0.8
			alliance_with = SWI
			}
		modifier = { 
			factor = 0.9
			trade_agreement_with = SWI
			}
		modifier = { 
			factor = 0.95
			marriage_with = SWI
			}
		modifier = { 
			factor = 2.0
			elector = yes
			}
		modifier = { 
			factor = 0.9
			SWI = { religion = THIS }
			}
		modifier = { 
			factor = 2.0
			SWI = { NOT = { religion = THIS } }
			}
	}
	
	title = "EVTNAME1104"
	desc = "EVTDESC1104"
	
	option = {
		name = "EVTOPTA1104"
		ai_chance = { factor = 75 }
		SWI = {
		inherit = THIS
		}
	}

	option = {
		name = "EVTOPTB1104"
		ai_chance = { factor = 25 }
		set_country_flag = swiss_accession_declined
	}
}

country_event = {

	id = 1105

	trigger = {
		NOT = { exists = SWI }
		NOT = { has_country_flag = swiss_nation_declined }
		capital_scope = { is_core = SWI }
	}
	
	mean_time_to_happen = {
	
		months = 216
	}
	
	title = "EVTNAME1105"
	desc = "EVTDESC1105"
	
	option = {
		name = "EVTOPTA1105"
		ai_chance = { factor = 85 }
		change_tag = SWI
	}

	option = {
		name = "EVTOPTB1105"
		ai_chance = { factor = 15 }
		set_country_flag = swiss_nation_declined
	}
}