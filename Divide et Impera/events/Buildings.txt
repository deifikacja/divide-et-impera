province_event = {
	id = 9464
	title = "EVTNAME9464"
	desc = "EVTDESC9464"
	
	trigger = {
		has_building = university
	}
	
	mean_time_to_happen = {
		months = 3600
		modifier = {
			factor = 0.8
			owner = { stability = 3 }
		}
		modifier = {
			factor = 0.1
			month = 6
		}
	}
	
	option = {
		name = "EVTOPTA9464"
		owner = {
			officials = 1
		}
	}
	
	option = {
		name = "EVTOPTB9464"
		owner = {
			cultural_tradition = 0.06
		}
	}
}

province_event = {
	id = 9465
	title = "EVTNAME9465"
	desc = "EVTDESC9465"
	
	trigger = {
		has_building = fine_arts_academy
	}

	mean_time_to_happen = {
		months = 600
	}
	
	option = {
		name = "EVTOPTA9465"
		owner = {
			cultural_tradition = 0.15
		}
	}
	
	option = {
		name = "EVTOPTB9465"
		owner = {
			cultural_tradition = 0.05
			treasury = 50
		}
	}
}

province_event = {
	id = 9467
	title = "EVTNAME9467"
	desc = "EVTDESC9467"
	
	trigger = {
		has_building = university
		has_owner_culture = yes
		has_owner_religion = yes
		is_core = yes
		owner = {
			war = yes
		}
	}
	mean_time_to_happen = {
		months = 1500
	}
	option = {
		name = "EVTOPTA9467"
		owner = {
			manpower = 5
			officials = 1
		}
	}
	
	option = {
		name = "EVTOPTB9467"
		owner = {
			officials = 3
		}
	}
}
			