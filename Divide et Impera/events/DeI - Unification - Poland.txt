country_event = {

	id = 85002

	is_triggered_only = yes

	title = "EVTNAME85001"
	desc = "EVTDESC85001"

	option = {
		name = "EVTOPTA85001"
		german_region = { remove_core = THIS }
		jagellonian_empire = { remove_core = THIS }
		primary_culture = xxx
	}
}

country_event = {

	id = 8541

	trigger = {
		owns = 257
		year = 1550
		NOT = { exists = MAZ }
		war = no
		stability = 1
		NOT = { has_global_flag = maz_disappear }
	}

	mean_time_to_happen = {
		months = 120
		
		modifier = { 
			year = 1600
			factor = 0.5
		}
		modifier = { 
			NOT = { centralization_decentralization = 0 }
			factor = 0.8
		}
		modifier = { 
			centralization_decentralization = 3
			factor = 1.25
		}
		modifier = { 
			NOT = { ADM = 5 }
			factor = 1.25
		}
		modifier = { 
			ADM = 8
			factor = 0.8
		}
		modifier = { 
			stability = 3
			factor = 0.9
		}
	}

	title = "EVTNAME8501"
	desc = "EVTDESC8501"

	option = {
		name = "EVTOPTA8501"
		MAZ = { country_event = 85002 }
		set_global_flag = maz_disappear
	}
}

country_event = {

	id = 8542

	trigger = {
		owns = 256
		year = 1500
		NOT = { exists = PLO }
		war = no
		stability = 1
		NOT = { has_global_flag = plo_disappear }
	}

	mean_time_to_happen = {
		months = 120
		
		modifier = { 
			year = 1550
			factor = 0.5
		}
		modifier = { 
			NOT = { centralization_decentralization = 0 }
			factor = 0.8
		}
		modifier = { 
			centralization_decentralization = 3
			factor = 1.25
		}
		modifier = { 
			NOT = { ADM = 5 }
			factor = 1.25
		}
		modifier = { 
			ADM = 8
			factor = 0.8
		}
		modifier = { 
			stability = 3
			factor = 0.9
		}
	}

	title = "EVTNAME8501"
	desc = "EVTDESC8501"

	option = {
		name = "EVTOPTA8501"
		PLO = { country_event = 85002 }
		set_global_flag = plo_disappear
	}
}

country_event = {

	id = 8543

	trigger = {
		owns = 42
		year = 1550
		NOT = { exists = WRI }
		war = no
		stability = 1
		NOT = { has_global_flag = war_disappear }
	}

	mean_time_to_happen = {
		months = 1200
		
		modifier = { 
			year = 1600
			factor = 0.5
		}
		modifier = {
			NOT = { religion = catholic }
			factor = 0.1
		}
		modifier = { 
			NOT = { centralization_decentralization = 0 }
			factor = 0.8
		}
		modifier = { 
			centralization_decentralization = 3
			factor = 1.25
		}
		modifier = { 
			NOT = { ADM = 5 }
			factor = 1.25
		}
		modifier = { 
			ADM = 8
			factor = 0.8
		}
		modifier = { 
			stability = 3
			factor = 0.9
		}
	}

	title = "EVTNAME8501"
	desc = "EVTDESC8501"

	option = {
		name = "EVTOPTA8501"
		WRI = { country_event = 85002 }
		set_global_flag = war_disappear
	}
}

country_event = {

	id = 8545

	trigger = {
		owns = 41
		year = 1500
		NOT = { exists = TEU }
		war = no
		stability = 1
		NOT = { has_global_flag = teu_disappear }
	}

	mean_time_to_happen = {
		months = 480
		
		modifier = { 
			year = 1550
			factor = 0.5
		}
		modifier = {
			NOT = { religion = catholic }
			factor = 0.2
		}
		modifier = { 
			NOT = { centralization_decentralization = 0 }
			factor = 0.8
		}
		modifier = { 
			centralization_decentralization = 3
			factor = 1.25
		}
		modifier = { 
			NOT = { ADM = 5 }
			factor = 1.25
		}
		modifier = { 
			ADM = 8
			factor = 0.8
		}
		modifier = { 
			stability = 3
			factor = 0.9
		}
	}

	title = "EVTNAME8501"
	desc = "EVTDESC8501"

	option = {
		name = "EVTOPTA8501"
		TEU = { country_event = 85002 }
		set_global_flag = teu_disappear
	}
}

country_event = {

	id = 8546

	trigger = {
		owns = 273
		year = 1500
		NOT = { exists = LIV }
		war = no
		stability = 1
		NOT = { has_global_flag = liv_disappear }
	}

	mean_time_to_happen = {
		months = 360
		
		modifier = { 
			year = 1550
			factor = 0.5
		}
		modifier = {
			NOT = { religion = catholic }
			factor = 0.2
		}
		modifier = { 
			NOT = { centralization_decentralization = 0 }
			factor = 0.8
		}
		modifier = { 
			centralization_decentralization = 3
			factor = 1.25
		}
		modifier = { 
			NOT = { ADM = 5 }
			factor = 1.25
		}
		modifier = { 
			ADM = 8
			factor = 0.8
		}
		modifier = { 
			stability = 3
			factor = 0.9
		}
	}

	title = "EVTNAME8501"
	desc = "EVTDESC8501"

	option = {
		name = "EVTOPTA8501"
		LIV = { country_event = 85002 }
		set_global_flag = liv_disappear
	}
}

country_event = {

	id = 8547

	trigger = {
		owns = 38
		year = 1550
		NOT = { exists = RIG }
		war = no
		stability = 1
		NOT = { has_global_flag = rig_disappear }
	}

	mean_time_to_happen = {
		months = 1200
		
		modifier = { 
			year = 1600
			factor = 0.5
		}
		modifier = {
			NOT = { religion = catholic }
			factor = 0.1
		}
		modifier = { 
			NOT = { centralization_decentralization = 0 }
			factor = 0.8
		}
		modifier = { 
			centralization_decentralization = 3
			factor = 1.25
		}
		modifier = { 
			NOT = { ADM = 5 }
			factor = 1.25
		}
		modifier = { 
			ADM = 8
			factor = 0.8
		}
		modifier = { 
			stability = 3
			factor = 0.9
		}
	}

	title = "EVTNAME8501"
	desc = "EVTDESC8501"

	option = {
		name = "EVTOPTA8501"
		RIG = { country_event = 85002 }
		set_global_flag = rig_disappear
	}
}

country_event = {

	id = 8549

	trigger = {
		owns = 264
		year = 1500
		NOT = { exists = WRO }
		war = no
		stability = 1
		NOT = { has_global_flag = wro_disappear }
	}

	mean_time_to_happen = {
		months = 120
		
		modifier = { 
			year = 1550
			factor = 0.5
		}
		modifier = {
			NOT = { religion = catholic }
			factor = 0.5
		}
		modifier = { 
			NOT = { centralization_decentralization = 0 }
			factor = 0.8
		}
		modifier = { 
			centralization_decentralization = 3
			factor = 1.25
		}
		modifier = { 
			NOT = { ADM = 5 }
			factor = 1.25
		}
		modifier = { 
			ADM = 8
			factor = 0.8
		}
		modifier = { 
			stability = 3
			factor = 0.9
		}
	}

	title = "EVTNAME8501"
	desc = "EVTDESC8501"

	option = {
		name = "EVTOPTA8501"
		WRO = { country_event = 85002 }
		set_global_flag = wro_disappear
	}
}

country_event = {

	id = 8550

	trigger = {
		owns = 1251
		year = 1500
		NOT = { exists = GLO }
		war = no
		stability = 1
		NOT = { has_global_flag = glo_disappear }
	}

	mean_time_to_happen = {
		months = 120
		
		modifier = { 
			year = 1550
			factor = 0.5
		}
		modifier = { 
			NOT = { centralization_decentralization = 0 }
			factor = 0.8
		}
		modifier = { 
			centralization_decentralization = 3
			factor = 1.25
		}
		modifier = { 
			NOT = { ADM = 5 }
			factor = 1.25
		}
		modifier = { 
			ADM = 8
			factor = 0.8
		}
		modifier = { 
			stability = 3
			factor = 0.9
		}
	}

	title = "EVTNAME8501"
	desc = "EVTDESC8501"

	option = {
		name = "EVTOPTA8501"
		GLO = { country_event = 85002 }
		set_global_flag = glo_disappear
	}
}

country_event = {

	id = 8551

	trigger = {
		owns = 1257
		year = 1600
		NOT = { exists = OPP }
		war = no
		stability = 1
		NOT = { has_global_flag = opp_disappear }
	}

	mean_time_to_happen = {
		months = 120
		
		modifier = { 
			year = 1650
			factor = 0.5
		}
		modifier = { 
			NOT = { centralization_decentralization = 0 }
			factor = 0.8
		}
		modifier = { 
			centralization_decentralization = 3
			factor = 1.25
		}
		modifier = { 
			NOT = { ADM = 5 }
			factor = 1.25
		}
		modifier = { 
			ADM = 8
			factor = 0.8
		}
		modifier = { 
			stability = 3
			factor = 0.9
		}
	}

	title = "EVTNAME8501"
	desc = "EVTDESC8501"

	option = {
		name = "EVTOPTA8501"
		OPP = { country_event = 85002 }
		set_global_flag = opp_disappear
	}
}

country_event = {

	id = 8552

	trigger = {
		owns = 1945
		year = 1450
		NOT = { exists = SVJ }
		war = no
		stability = 1
		NOT = { has_global_flag = svj_disappear }
	}

	mean_time_to_happen = {
		months = 120
		
		modifier = { 
			year = 1500
			factor = 0.5
		}
		modifier = { 
			NOT = { centralization_decentralization = 0 }
			factor = 0.8
		}
		modifier = { 
			centralization_decentralization = 3
			factor = 1.25
		}
		modifier = { 
			NOT = { ADM = 5 }
			factor = 1.25
		}
		modifier = { 
			ADM = 8
			factor = 0.8
		}
		modifier = { 
			stability = 3
			factor = 0.9
		}
	}

	title = "EVTNAME8501"
	desc = "EVTDESC8501"

	option = {
		name = "EVTOPTA8501"
		SVJ = { country_event = 85002 }
		set_global_flag = svj_disappear
	}
}

country_event = {

	id = 8553

	trigger = {
		owns = 1250
		year = 1600
		NOT = { exists = TES }
		war = no
		stability = 1
		NOT = { has_global_flag = tes_disappear }
	}

	mean_time_to_happen = {
		months = 120
		
		modifier = { 
			year = 1650
			factor = 0.5
		}
		modifier = { 
			NOT = { centralization_decentralization = 0 }
			factor = 0.8
		}
		modifier = { 
			centralization_decentralization = 3
			factor = 1.25
		}
		modifier = { 
			NOT = { ADM = 5 }
			factor = 1.25
		}
		modifier = { 
			ADM = 8
			factor = 0.8
		}
		modifier = { 
			stability = 3
			factor = 0.9
		}
	}

	title = "EVTNAME8501"
	desc = "EVTDESC8501"

	option = {
		name = "EVTOPTA8501"
		TES = { country_event = 85002 }
		set_global_flag = tes_disappear
	}
}

country_event = {

	id = 8554

	trigger = {
		owns = 2156
		year = 1500
		NOT = { exists = SLU }
		war = no
		stability = 1
		NOT = { has_global_flag = slu_disappear }
	}

	mean_time_to_happen = {
		months = 120
		
		modifier = { 
			year = 1550
			factor = 0.5
		}
		modifier = { 
			NOT = { centralization_decentralization = 0 }
			factor = 0.8
		}
		modifier = { 
			centralization_decentralization = 3
			factor = 1.25
		}
		modifier = { 
			NOT = { ADM = 5 }
			factor = 1.25
		}
		modifier = { 
			ADM = 8
			factor = 0.8
		}
		modifier = { 
			stability = 3
			factor = 0.9
		}
	}

	title = "EVTNAME8501"
	desc = "EVTDESC8501"

	option = {
		name = "EVTOPTA8501"
		SLU = { country_event = 85002 }
		set_global_flag = slu_disappear
	}
}

country_event = {

	id = 8555

	trigger = {
		owns = 48
		year = 1600
		NOT = { exists = STE }
		war = no
		stability = 1
		NOT = { has_global_flag = ste_disappear }
	}

	mean_time_to_happen = {
		months = 1200
		
		modifier = { 
			year = 1650
			factor = 0.5
		}
		modifier = { 
			exists = POM
			factor = 0.1
		}
		modifier = { 
			NOT = { centralization_decentralization = 0 }
			factor = 0.8
		}
		modifier = { 
			centralization_decentralization = 3
			factor = 1.25
		}
		modifier = { 
			NOT = { ADM = 5 }
			factor = 1.25
		}
		modifier = { 
			ADM = 8
			factor = 0.8
		}
		modifier = { 
			stability = 3
			factor = 0.9
		}
	}

	title = "EVTNAME8501"
	desc = "EVTDESC8501"

	option = {
		name = "EVTOPTA8501"
		STE = { country_event = 85002 }
		set_global_flag = ste_disappear
	}
}

country_event = {

	id = 8556

	trigger = {
		owns = 47
		year = 1600
		NOT = { exists = WOL }
		war = no
		stability = 1
		NOT = { has_global_flag = wol_disappear }
	}

	mean_time_to_happen = {
		months = 120
		
		modifier = { 
			year = 1650
			factor = 0.5
		}
		modifier = { 
			NOT = { centralization_decentralization = 0 }
			factor = 0.8
		}
		modifier = { 
			centralization_decentralization = 3
			factor = 1.25
		}
		modifier = { 
			NOT = { ADM = 5 }
			factor = 1.25
		}
		modifier = { 
			ADM = 8
			factor = 0.8
		}
		modifier = { 
			stability = 3
			factor = 0.9
		}
	}

	title = "EVTNAME8501"
	desc = "EVTDESC8501"

	option = {
		name = "EVTOPTA8501"
		WOL = { country_event = 85002 }
		set_global_flag = wol_disappear
	}
}
