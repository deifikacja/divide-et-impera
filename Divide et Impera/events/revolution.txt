# Revolution
country_event = {

	id = 3011
	
	major = yes
	
	trigger = {
		
		NOT = { has_country_flag = revolution }
		OR = {
			government = feudal_monarchy
			government = despotic_monarchy
			government = noble_republic
			government = absolute_monarchy
			government = administrative_monarchy
			government = enlightened_despotism
			government = bureaucratic_despotism
			government = republican_dictatorship
			government = imperial_government
			government = major_merchant_republic
			government = merchant_republic
		}
		government_tech = 48
		num_of_cities = 5
		NOT = { centralization_decentralization = 0 }
		NOT = { serfdom_freesubjects = 0 }	
		NOT = { aristocracy_plutocracy = 0 }	
	}
	
	mean_time_to_happen = {
		months = 3600
		
		modifier = {
			factor = 0.95
			NOT = { aristocracy_plutocracy = -2 }
		}
		modifier = {
			factor = 2.0
			war = yes
		}	
		modifier = {
			factor = 0.8
			revolution_target = {
				war_with = THIS
			}
		}		 
		modifier = {
			factor = 0.9
			NOT = { aristocracy_plutocracy = -3 }
		}
		modifier = {
			factor = 0.85
			NOT = { aristocracy_plutocracy = -4 }
		}		
		modifier = {
			factor = 0.95
			NOT = { centralization_decentralization = -2 }
		}
		modifier = {
			factor = 0.9
			NOT = { centralization_decentralization = -3 }
		}
		modifier = {
			factor = 0.85
			NOT = { centralization_decentralization = -4 }
		}
		modifier = {
			factor = 0.9
			NOT = { serfdom_freesubjects = -2 }
		}
		modifier = {
			factor = 0.85
			NOT = { serfdom_freesubjects = -3 }
		}
		modifier = {
			factor = 0.8
			NOT = { serfdom_freesubjects = -4 }
		}
		modifier = {
			factor = 0.9
			revolt_percentage = 0.1
		}
		modifier = {
			factor = 0.75
			revolt_percentage = 0.25
		}
		modifier = {
			factor = 0.9
			war_exhaustion = 2
		}
		modifier = {
			factor = 0.8
			war_exhaustion = 4
		}
		modifier = {
			factor = 0.7
			war_exhaustion = 6
		}
		modifier = {
			factor = 0.6
			war_exhaustion = 8
		}
		modifier = {
			factor = 0.5
			war_exhaustion = 10
		}
		modifier = {
			factor = 0.9
			badboy = 0.5
		}
		modifier = {
			factor = 1.25
			idea = humanist_tolerance
		}
		modifier = {
			factor = 0.9
			NOT = { stability = 1 }
		}
		modifier = {
			factor = 0.85
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 0.8
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.75
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.25
			stability = 3
		}
	}
	
	title = "EVTNAME3011"
	desc = "EVTDESC3011"
	
	option = {
		name = "EVTOPTA3011"			# Dire times are ahead of us
		set_country_flag = revolution
		stability = -1
	}
}


# Religious intolerance
country_event = {

	id = 3012
	
	trigger = {
		has_country_flag = revolution
		NOT = { has_country_flag = religious_revolution }
		NOT = { protestant = -1 }
		NOT = { reformed = -1 }
	}
	
	mean_time_to_happen = {
		months = 30
		
		modifier = {
			factor = 0.9
			NOT = { protestant = -2 }
		}		
		modifier = {
			factor = 0.8
			NOT = { protestant = -3 }
		}
		modifier = {
			factor = 0.7
			NOT = { protestant = -4 }
		}
		modifier = {
			factor = 0.9
			NOT = { reformed = -2 }
		}		
		modifier = {
			factor = 0.8
			NOT = { reformed = -3 }
		}
		modifier = {
			factor = 0.7
			NOT = { reformed = -4 }
		}
		modifier = {
			factor = 0.9
			relation = { who = PAP value = 0 }
		}
		modifier = {
			factor = 0.8
			relation = { who = PAP value = 100 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 1
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.1
			stability = 3
		}
	}
	
	title = "EVTNAME3012"
	desc = "EVTDESC3012"
	
	option = {
		name = "EVTOPTA3012"			# One nation, one belief
		ai_chance = { factor = 75 }
		set_country_flag = religious_revolution
		add_country_modifier = {
			name = "religious_intolerance"
			duration = 720
		}
	}
	option = {
		name = "EVTOPTB3012"			# Tolerate all beliefs
		ai_chance = { factor = 30 }
		set_country_flag = religious_revolution
		relation = { who = PAP value = -100 }
		add_country_modifier = {
			name = "religious_tolerance"
			duration = 720
		}
		secularism_theocracy = -2
	}
}



# Serfdom
country_event = {

	id = 3013
	
	trigger = {
		has_country_flag = revolution
		NOT = { has_country_flag = serfdom }
		NOT = { serfdom_freesubjects = -2 }
	}
	
	mean_time_to_happen = {
		months = 30
		
		modifier = {
			factor = 0.90
			NOT = { serfdom_freesubjects = -4 }
		}
		modifier = {
			factor = 0.95
			revolt_percentage = 0.1
		}
		modifier = {
			factor = 0.85
			revolt_percentage = 0.3
		}
		modifier = {
			factor = 0.75
			revolt_percentage = 0.5
		}
		modifier = {
			factor = 0.9
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 1
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.1
			stability = 3
		}	
	}
	
	title = "EVTNAME3013"
	desc = "EVTDESC3013"
	
	option = {
		name = "EVTOPTA3013"			# Restrict serfdom
		ai_chance = { factor = 30 }
		set_country_flag = serfdom
		serfdom_freesubjects = 2
		add_country_modifier = {
			name = "restrict_serfdom"
			duration = 720
		}
	}
	option = {
		name = "EVTOPTB3013"			# Keep the masses in chains
		ai_chance = { factor = 70 }
		set_country_flag = serfdom
		serfdom_freesubjects = -1
		random_owned = { revolutionary_rebels = 1 }
		add_country_modifier = {
			name = "enforce_serfdom"
			duration = 720
		}
	}
}


# Social reform
country_event = {

	id = 3014
	
	trigger = {
		has_country_flag = revolution
		NOT = { has_country_flag = social_reform }
		NOT = { stability = 0 }
		OR = { 
			advisor = philosopher
			advisor = statesman
			advisor = high_judge
		}
	}
	
	mean_time_to_happen = {
		months = 40

		modifier = {
			factor = 0.9
			philosopher = 5
		}
		modifier = {
			factor = 0.8
			philosopher = 6
		}
		modifier = {
			factor = 1.2
			NOT = { advisor = philosopher }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
	}
	
	title = "EVTNAME3014"
	desc = "EVTDESC3014"
	
	option = {
		name = "EVTOPTA3014"			# Suppress the free thinkers
		ai_chance = { factor = 60 }
		innovative_narrowminded = 1
		set_country_flag = social_reform
		add_country_modifier = {
			name = "suppress_free_thinkers"
			duration = 720
		}
	}
	option = {
		name = "EVTOPTB3014"			# They hardly pose a threat
		ai_chance = { factor = 40 }
		set_country_flag = social_reform
		innovative_narrowminded = -1
		secularism_theocracy = -1
		serfdom_freesubjects = 1
		random_owned = { revolutionary_rebels = 1 }
		random_owned = { revolutionary_rebels = 1 }
		random_owned = { revolutionary_rebels = 1 }
	}
}


# Royal Bureaucracy
country_event = {

	id = 3015
	
	trigger = {
		has_country_flag = revolution
		NOT = { has_country_flag = royal_bureaucracy }
		centralization_decentralization = -2
		idea = bureaucracy
	}
	
	mean_time_to_happen = {
		months = 24
		
		modifier = {
			factor = 0.95
			NOT = { centralization_decentralization = -3 }
		}
		modifier = {
			factor = 0.85
			NOT = { centralization_decentralization = -4 }
		}
		modifier = {
			factor = 0.9
			revolt_percentage = 0.1
		}
		modifier = {
			factor = 0.8
			revolt_percentage = 0.2
		}		
		modifier = {
			factor = 0.7
			revolt_percentage = 0.3
		}
		modifier = {
			factor = 0.8
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.8
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 1
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.2
			stability = 3
		}
	}
	
	title = "EVTNAME3015"
	desc = "EVTDESC3015"
	
	option = {
		name = "EVTOPTA3015"			# Reduce the royal bureaucracy
		ai_chance = { factor = 65 }
		set_country_flag = royal_bureaucracy
		centralization_decentralization = 1
		add_country_modifier = {
			name = "bureaucratic_reduction"
			duration = 720
		}		
	}
	option = {
		name = "EVTOPTB3015"			# Expand the royal bureaucracy
		ai_chance = { factor = 35 }
		set_country_flag = royal_bureaucracy
		add_country_modifier = {
			name = "bureaucratic_expansion"
			duration = 720
		}
	}
}



# Deplorable reign
country_event = {

	id = 3016
	
	trigger = {
		has_country_flag = revolution
		NOT = { has_country_flag = deplorable_reign }
		NOT = { ADM = 4 }
		OR = {
			NOT = { MIL = 4 }
			NOT = { DIP = 4 }
		}
	}
	
	mean_time_to_happen = {
		months = 35

		modifier = {
			factor = 0.9
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.8
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 1
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.2
			stability = 3
		}
		modifier = {
			factor = 1.1
			advisor = statesman
		}
		modifier = {
			factor = 1.1
			statesman = 5
		}
		modifier = {
			factor = 1.1
			statesman = 6
		}
		modifier = {
			factor = 0.9
			NOT = { advisor = statesman }
		}
	}
	
	title = "EVTNAME3016"
	desc = "EVTDESC3016"
	
	option = {
		name = "EVTOPTA3016"			# Support
		ai_chance = { factor = 60 }
		set_country_flag = deplorable_reign
		add_country_modifier = {
			name = "support_monarch"
			duration = 720
		}
	}
	option = {
		name = "EVTOPTB3016"			# Oppose
		ai_chance = { factor = 40 }
		set_country_flag = deplorable_reign
		random_owned = { revolutionary_rebels = 1 }
		add_country_modifier = {
			name = "oppose_monarch"
			duration = 720
		}
	}
}



# The road to bankruptcy
country_event = {

	id = 3017
	
	trigger = {
		has_country_flag = revolution
		NOT = { has_country_flag = road_to_bankruptcy }
		number_of_loans = 1
		inflation = 30
	}
	
	mean_time_to_happen = {
		months = 40
		
		modifier = {
			factor = 0.9
			inflation = 40
		}
		modifier = {
			factor = 0.8
			inflation = 50
		}
		modifier = {
			factor = 0.7
			inflation = 60
		}
		modifier = {
			factor = 0.9
			number_of_loans = 2
		}
		modifier = {
			factor = 0.8
			number_of_loans = 3
		}
		modifier = {
			factor = 0.9
			NOT = { advisor = treasurer }
		}		
		modifier = {
			factor = 1.1
			advisor = treasurer
		}
		modifier = {
			factor = 1.1
			treasurer = 5
		}
		modifier = {
			factor = 1.1
			treasurer = 6
		}
		modifier = {
			factor = 0.9
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 1
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.1
			stability = 3
		}
	}
	
	title = "EVTNAME3017"
	desc = "EVTDESC3017"
	
	option = {
		name = "EVTOPTA3017"			# Mint more money
		ai_chance = { factor = 30 }
		set_country_flag = road_to_bankruptcy
		treasury = 40
		add_country_modifier = {
			name = "mint_money"
			duration = 720
		}
	}
	option = {
		name = "EVTOPTB3017"			# We must strengthen our currency
		ai_chance = { factor = 70 }
		set_country_flag = road_to_bankruptcy
		add_country_modifier = {
			name = "fight_inflation"
			duration = 720
		}
	}
}


# Financial Crisis
country_event = {

	id = 3018
	
	trigger = {
		has_country_flag = revolution
		NOT = { has_country_flag = financial_crisis }
		is_bankrupt = yes
		war = yes
	}
	
	mean_time_to_happen = {
		months = 40

		modifier = {
			factor = 0.9
			NOT = { production_efficiency = 0.5 }
		}
		modifier = {
			factor = 0.9
			NOT = { production_efficiency = 0.4 }
		}
		modifier = {
			factor = 0.9
			NOT = { trade_efficiency = 0.5 }
		}
		modifier = {
			factor = 0.9
			NOT = { trade_efficiency = 0.4 }
		}
		modifier = {
			factor = 0.9
			NOT = { advisor = treasurer }
		}
		modifier = {
			factor = 1.1
			advisor = treasurer
		}
		modifier = {
			factor = 0.9
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 1
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.1
			stability = 3
		}
	}
	
	title = "EVTNAME3018"
	desc = "EVTDESC3018"
	
	option = {
		name = "EVTOPTA3018"			# Cut back on war expenditures
		ai_chance = { factor = 70 }
		set_country_flag = financial_crisis
		add_country_modifier = {
			name = "disarmament"
			duration = 720
		}
	}
	option = {
		name = "EVTOPTB3018"			# The war has its purpose
		ai_chance = { factor = 30 }
		set_country_flag = financial_crisis
		treasury = -50
		add_country_modifier = {
			name = "financial_disaster"
			duration = 720
		}
	}

}


# Stability returns
country_event = {

	id = 3019
	
	major = yes	
	
	trigger = {
		has_country_flag = revolution
		had_country_flag = { flag = revolution days = 1000 }
		stability = 1
		NOT = { num_of_revolts = 1 }
	}
	
	mean_time_to_happen = {
		months = 84
		
		modifier = {
			factor = 0.9
			MIL = 5
		}
		modifier = {
			factor = 0.8
			MIL = 6
		}
		modifier = {
			factor = 1.1
			NOT = { MIL = 4 }
		}
		modifier = {
			factor = 0.9
			ADM = 5
		}	
		modifier = {
			factor = 0.8
			ADM = 6
		}
		modifier = {
			factor = 1.1
			NOT = { ADM = 4 }
		}	
		modifier = {
			factor = 0.9
			stability = 1
		}
		modifier = {
			factor = 0.9
			stability = 2
		}
		modifier = {
			factor = 0.9
			stability = 3
		}
		modifier = {
			factor = 1.1
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 1.1
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 1.1
			NOT = { stability = -2 }
		}
	}
	
	title = "EVTNAME3019"
	desc = "EVTDESC3019"
	
	option = {
		name = "EVTOPTA3019"			# Restore order
		clr_country_flag = revolution
		clr_country_flag = religious_revolution
		clr_country_flag = serfdom
		clr_country_flag = social_reform
		clr_country_flag = royal_bureaucracy
		clr_country_flag = deplorable_reign
		clr_country_flag = road_to_bankruptcy
		clr_country_flag = financial_crisis		
		stability = 2
	}
}
