######################################
#
# Events for Korea. Id 10500-
#
######################################


#State Council (of Joseon)

country_event = {

	id = 10500

	trigger = {
		tag = KOR
		NOT = { has_country_flag = state_council }
	}

	mean_time_to_happen = {
		months = 2000 # Random Country Event

		modifier = {
			factor = 2.0
			NOT = { ADM = 4 }
		}
		modifier = {
			factor = 0.8
			ADM = 7
		}
		modifier = {
			factor = 0.5
			ADM = 8
		}
	}

	title = "EVTNAME10500"
	desc = "EVTDESC10500"
	
	option = {
		name = "EVTOPTA10500"		#Form the Council
		ai_chance = { factor = 60 }
		prestige = 0.1
		government_tech = -200
		stability = -1
		add_country_modifier = {
			name = "state_council"
			duration = 1825
		}
		set_country_flag = state_council
	}
	option = {
		name = "EVTOPTB10500"		#Let things be as they were
		ai_chance = { factor = 40 }
	}
}

#The King's Approval
country_event = {

	id = 10501

	trigger = {
		tag = KOR
		NOT = { has_country_flag = kings_approval }
	}

	mean_time_to_happen = {
		months = 2000 # Random Country Event

		modifier = {
			factor = 2.0
			NOT = { ADM = 4 }
		}
		modifier = {
			factor = 0.8
			ADM = 7
		}
		modifier = {
			factor = 0.5
			ADM = 8
		}
	}

	title = "EVTNAME10501"
	desc = "EVTDESC10501"
	
	option = {
		name = "EVTOPTA10501"		#All decisions should be approved by the King
		ai_chance = { factor = 50 }
		legitimacy = 0.1
		centralization_decentralization = -1
		random_list = {
			50 = {
				random_owned = {
					create_revolt = 9
				}
			}
			50 = {
				any_owned = {
					revolt_risk = 5
				}
			}
		}
		set_country_flag = kings_approval
	}
	option = {
		name = "EVTOPTB10501"		#The current system works well enough
		ai_chance = { factor = 50 }
		prestige = -0.05
		set_country_flag = hidden_tooltip
		any_neighbor_country = {
			limit = {
				THIS = { has_country_flag = hidden_tooltip }
				NOT = {
					alliance_with = KOR
					vassal_of = KOR
				}
			}
			add_ai_strategy = {
				antagonize = {
					id = KOR
					value = 50
				}
			}
		}
		clr_country_flag = hidden_tooltip
	}
}

#Sinmun Office
country_event = {

	id = 10502

	trigger = {
		tag = KOR
		NOT = { has_country_flag = sinmun_office }
	}

	mean_time_to_happen = {
		months = 2000 # Random Country Event

		modifier = {
			factor = 2.0
			stability = 3
		}
		modifier = {
			factor = 0.8
			NOT = { stability = 0 }
		}
	}

	title = "EVTNAME10502"
	desc = "EVTDESC10502"
	
	option = {
		name = "EVTOPTA10502"		#Create the Sinmun Office
		ai_chance = { factor = 70 }
		treasury = -50
		add_country_modifier = {
			name = "sinmun_office"
			duration = -1
		}
		set_country_flag = sinmun_office
	}
	option = {
		name = "EVTOPTB10502"		#Dismiss this absurd request
		ai_chance = { factor = 30 }
		add_country_modifier = {
			name = "sinmun_office_rejected"
			duration = 365
		}
		set_country_flag = sinmun_office
	}
}

#Manchu Nomad Raids
province_event = {

	id = 10504

	trigger = {
		owner = { tag = KOR }
		NOT = { has_province_modifier = border_patrol }
		NOT = { has_province_modifier = nomad_raids }
		any_neighbor_province = {
			owned_by = MCH
			NOT = { has_province_modifier = suppress_nomad_raids }
			OR = {
				has_province_modifier = support_nomad_raids
				has_province_modifier = allow_nomad_raids
			}
		}
	}

	mean_time_to_happen = {
		months = 12

		modifier = {
			factor = 0.5
			any_neighbor_province = {
				owned_by = MCH
				has_province_modifier = support_nomad_raids
			}
		}
		modifier = {
			factor = 1.2
			units_in_province = 1
		}
		modifier = {
			factor = 1.5
			units_in_province = 3
		}
		modifier = {
			factor = 1.2
			has_building = fort1
		}
		modifier = {
			factor = 1.2
			has_building = fort2
		}
		modifier = {
			factor = 1.2
			has_building = fort3
		}
		modifier = {
			factor = 1.5
			has_building = fort4
		}
	}

	title = "EVTNAME10504"
	desc = "EVTDESC10504"
	
	option = {
		name = "EVTOPTA10504"		#Damn!
		ai_chance = { factor = 100 }
		set_province_flag = nomad_raids
		add_province_modifier = {
			name = "nomad_raids"
			duration = 365
		}
	}
}

#Chinese Nomad Raids
province_event = {

	id = 10505

	trigger = {
		owner = { tag = KOR }
		NOT = { has_province_modifier = border_patrol }
		NOT = { has_province_modifier = nomad_raids }
		any_neighbor_province = {
			owned_by = MNG
			NOT = { has_province_modifier = suppress_nomad_raids }
			OR = {
				has_province_modifier = support_nomad_raids
				has_province_modifier = allow_nomad_raids
			}
		}
	}

	mean_time_to_happen = {
		months = 24

		modifier = {
			factor = 0.5
			any_neighbor_province = {
				owned_by = MNG
				has_province_modifier = support_nomad_raids
			}
		}
		modifier = {
			factor = 1.2
			units_in_province = 1
		}
		modifier = {
			factor = 1.5
			units_in_province = 3
		}
		modifier = {
			factor = 1.2
			has_building = fort1
		}
		modifier = {
			factor = 1.2
			has_building = fort2
		}
		modifier = {
			factor = 1.2
			has_building = fort3
		}
		modifier = {
			factor = 1.5
			has_building = fort4
		}
	}

	title = "EVTNAME10505"
	desc = "EVTDESC10505"
	
	option = {
		name = "EVTOPTA10505"		#Damn!
		ai_chance = { factor = 100 }
		set_province_flag = nomad_raids
		add_province_modifier = {
			name = "nomad_raids"
			duration = 365
		}
	}
}

#Pirate Raid
province_event = {

	id = 10506

	trigger = {
		owner = {
			tag = KOR
			NOT = { has_country_flag = prepare_wokou_expedition }
			NOT = { has_country_flag = wokou_expedition }
			NOT = { has_country_flag = wokou_pirates_defeated }
		}
		NOT = { has_province_modifier = wokou_raids }
		port = yes
	}

	mean_time_to_happen = {
		months = 48

		modifier = {
			factor = 1.5
			has_province_modifier = coastal_defense
		}
		modifier = {
			factor = 1.2
			units_in_province = 1
		}
		modifier = {
			factor = 1.2
			units_in_province = 3
		}
	}

	title = "EVTNAME10506"
	desc = "EVTDESC10506"

	option = {
		name = "EVTOPTA10506"		#Damn!
		ai_chance = { factor = 100 }
		add_province_modifier = {
			name = "wokou_raids"
			duration = 180
		}
		#If raids on a province that has coastal defense
		set_province_flag = hidden_tooltip
		random_country = {
			limit = {
				THIS = {
					has_province_flag = hidden_tooltip
					has_province_modifier = coastal_defense
					has_province_flag = wokou_raids
					owner = { NOT = { has_country_flag = prepare_wokou_expedition } }
				}
			}
			THIS = { owner = { country_event = 10507 } }
		}
		clr_province_flag = hidden_tooltip
		set_province_flag = wokou_raids
	}
}

#Prepare Wokou Expedition
country_event = {

	id = 10507

	is_triggered_only = yes

	title = "EVTNAME10507"
	desc = "EVTDESC10507"

	option = {
		name = "EVTOPTA10507"		#Fund the preparations
		ai_chance = { factor = 80 }
		prestige = 0.05
		treasury = -50
		set_country_flag = prepare_wokou_expedition
	}
	option = {
		name = "EVTOPTB10507"		#It is not worth the trouble
		ai_chance = { factor = 20 }
		prestige = -0.1
		FROM = {
			revolt_risk = 3
		}
	}
}

#Wokou Expedition
country_event = {

	id = 10508

	trigger = {
		tag = KOR
		has_country_flag = prepare_wokou_expedition
		manpower = 2
	}

	mean_time_to_happen = {
		months = 12

		modifier = {
			factor = 0.5
			MIL = 7
		}
		modifier = {
			factor = 0.8
			ADM = 7
		}
		modifier = {
			factor = 2.0
			NOT = { MIL = 4 }
		}
		modifier = {
			factor = 1.5
			NOT = { ADM = 4 }
		}
	}

	title = "EVTNAME10508"
	desc = "EVTDESC10508"

	option = {
		name = "EVTOPTA10508"		#The time has come
		ai_chance = { factor = 80 }
		prestige = 0.05
		manpower = -1
		clr_country_flag = prepare_wokou_expedition
		set_country_flag = wokou_expedition
	}
	option = {
		name = "EVTOPTB10508"		#Our soldiers are needed elsewhere
		ai_chance = {
			factor = 20
			modifier = {
				factor = 0
				war = yes
			}
		}
		prestige = -0.1
		any_owned = {
			limit ={
				has_province_flag = wokou_raids
			}
			revolt_risk = 3
		}
		random_owned = {
			limit = {
				has_province_flag = wokou_raids
			}
			spawn_rebels = {
				type = anti_tax_rebels
				size = 2
			}
			owner = { clr_country_flag = prepare_wokou_expedition }
		}
	}
}

#Wokou Expedition Returns
country_event = {

	id = 10509

	trigger = {
		tag = KOR
		has_country_flag = wokou_expedition
	}

	mean_time_to_happen = {
		months = 12

		modifier = {
			factor = 0.7
			OR = {
				advisor = navigator
				advisor = grand_admiral
				advisor = great_naval_reformer
			}
		}
		modifier = {
			factor = 0.95
			MIL = 6
		}
		modifier = {
			factor = 0.8
			ADM = 7
		}
		modifier = {
			factor = 0.7
			MIL = 8
		}
		modifier = {
			factor = 2.0
			NOT = { MIL = 7 }
		}
	}

	title = "EVTNAME10509"
	desc = "EVTDESC10509"

	immediate = {
		clr_country_flag = wokou_expedition
		set_country_flag = wokou_pirates_defeated
	}

	option = {
		name = "EVTOPTA10509"		#This is truly good news!
		ai_chance = { factor = 100 }
		prestige = 0.1
		stability = 1
		any_owned = {
			limit = {
				has_province_flag = wokou_raids
			}
			revolt_risk = -1
		}
		random_list = {
			50 = {
				treasury = 25
				random_owned = {
					limit = {
						has_province_flag = wokou_raids
					}
					add_province_modifier = {
						name = "wokou_slaves_return"
						duration = 365
					}					
				}
			}
			25 = {
				treasury = 50
				random_owned = {
					limit = {
						has_province_flag = wokou_raids
					}
					add_province_modifier = {
						name = "wokou_slaves_return"
						duration = 365
					}					
				}
			}
			25 = {
				treasury = 100
				any_owned = {
					limit = {
						has_province_flag = wokou_raids
					}
					add_province_modifier = {
						name = "wokou_slaves_return"
						duration = 365
					}					
				}				
			}
		}
	}
}

#Wokou Expedition Returns
country_event = {

	id = 10510

	trigger = {
		tag = KOR
		has_country_flag = wokou_expedition
	}

	mean_time_to_happen = {
		months = 24

		modifier = {
			factor = 0.95
			NOT = { MIL = 6 }
		}
		modifier = {
			factor = 0.8
			NOT = { MIL = 5 }
		}
		modifier = {
			factor = 0.7
			NOT = { MIL = 4 }
		}
		modifier = {
			factor = 2.0
			MIL = 7
		}
	}

	title = "EVTNAME10510"
	desc = "EVTDESC10510"

	immediate = {
		clr_country_flag = wokou_expedition
	}

	option = {
		name = "EVTOPTA10510"		#Black news, indeed!
		ai_chance = { factor = 100 }
		prestige = -0.05
	}
}
