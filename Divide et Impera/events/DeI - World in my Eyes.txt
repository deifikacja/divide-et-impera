# AI events

country_event = {

	id = 666901
	
	trigger = {
		ai = yes
		culture_group = sinnic
		NOT = { has_country_flag = china_ai_start }
  	}

	immediate = { set_country_flag = china_ai_start }

	mean_time_to_happen = {
		months = 3
	}

	title = "One Emperor under the Sun!"
	desc = "Ich will"

	option = {
		name = "By Your Command!"
		ai_chance = { factor = 100 }
		THIS = {
			fixed_ai_strategy = yes
			add_ai_strategy = {
				befriend = {
					id = KOR
					value = 400
				}
				befriend = {
					id = DAI
					value = 200
				}
				befriend = {
					id = TIB
					value = 100
				}
				protect = {
					id = KOR
					value = 300
				}
				protect = {
					id = TIB
					value = 200
				}
				conquer_prov = {
					id = 660
					value = 400
				}
				conquer_prov = {
					id = 661
					value = 400
				}
				conquer_prov = {
					id = 662
					value = 400
				}
				conquer_prov = {
					id = 663
					value = 400
				}
				conquer_prov = {
					id = 664
					value = 400
				}
				conquer_prov = {
					id = 665
					value = 400
				}
				conquer_prov = {
					id = 666
					value = 400
				}
				conquer_prov = {
					id = 667
					value = 400
				}
				conquer_prov = {
					id = 668
					value = 400
				}
				conquer_prov = {
					id = 669
					value = 400
				}
				conquer_prov = {
					id = 670
					value = 400
				}
				conquer_prov = {
					id = 671
					value = 400
				}
				conquer_prov = {
					id = 672
					value = 400
				}
				conquer_prov = {
					id = 673
					value = 400
				}
				conquer_prov = {
					id = 674
					value = 400
				}
				conquer_prov = {
					id = 675
					value = 400
				}
				conquer_prov = {
					id = 679
					value = 400
				}
				conquer_prov = {
					id = 680
					value = 400
				}
				conquer_prov = {
					id = 681
					value = 400
				}
				conquer_prov = {
					id = 682
					value = 400
				}
				conquer_prov = {
					id = 683
					value = 400
				}
				conquer_prov = {
					id = 684
					value = 400
				}
				conquer_prov = {
					id = 685
					value = 400
				}
				conquer_prov = {
					id = 686
					value = 400
				}
				conquer_prov = {
					id = 687
					value = 400
				}
				conquer_prov = {
					id = 688
					value = 400
				}
				conquer_prov = {
					id = 689
					value = 400
				}
				conquer_prov = {
					id = 690
					value = 400
				}
				conquer_prov = {
					id = 691
					value = 400
				}
				conquer_prov = {
					id = 692
					value = 400
				}
				conquer_prov = {
					id = 693
					value = 400
				}
				conquer_prov = {
					id = 694
					value = 400
				}
				conquer_prov = {
					id = 695
					value = 400
				}
				conquer_prov = {
					id = 696
					value = 400
				}
				conquer_prov = {
					id = 697
					value = 400
				}
				conquer_prov = {
					id = 698
					value = 400
				}
				conquer_prov = {
					id = 699
					value = 400
				}
				conquer_prov = {
					id = 700
					value = 400
				}
				conquer_prov = {
					id = 703
					value = 400
				}
				conquer_prov = {
					id = 704
					value = 400
				}
				conquer_prov = {
					id = 707
					value = 400
				}
				conquer_prov = {
					id = 708
					value = 400
				}
				conquer_prov = {
					id = 709
					value = 400
				}
				conquer_prov = {
					id = 723
					value = 400
				}
				conquer_prov = {
					id = 726
					value = 400
				}
				conquer_prov = {
					id = 2003
					value = 400
				}
				conquer_prov = {
					id = 2004
					value = 400
				}
				conquer_prov = {
					id = 2005
					value = 400
				}
				conquer_prov = {
					id = 2006
					value = 400
				}
				conquer_prov = {
					id = 2007
					value = 400
				}
				conquer_prov = {
					id = 2008
					value = 400
				}
				conquer_prov = {
					id = 2009
					value = 400
				}
				conquer_prov = {
					id = 2010
					value = 400
				}
				conquer_prov = {
					id = 2011
					value = 400
				}
				conquer_prov = {
					id = 2012
					value = 400
				}
			}
		}
	}
}

country_event = {

	id = 666902

		trigger = {
			ai = yes
			culture_group = sinnic
			NOT = { has_country_flag = emperor_ai_start }
  	}

	mean_time_to_happen = {
		months = 3
		
		modifier = {
			factor = 0.5
			adm = 7
		}
		modifier = {
			factor = 0.5
			luck = yes
		}
		
	}

	title = "One Emperor under the Sun!"
	desc = "Ich will"
	
	option = {
		name = "EVTOPTA5008"		
		ai_chance = { factor = 100 }
		set_country_flag = emperor_ai_start
		government = eastern_bureaucracy
		centralization_decentralization = -1
		aristocracy_plutocracy = 1
		serfdom_freesubjects = -1
		land_naval = -1
		mercantilism_freetrade = -1
		innovative_narrowminded = 1
		secularism_theocracy = -1
		prestige = 0.05
		treasury = 50
		stability = 1
		create_general = 40
		create_general = 30
		create_general = 20
	}
	
}

country_event = {

	id = 666903
	
	trigger = {
		NOT = { has_country_flag = iberia_ai_start }
		tag = SPA
		ai = yes
  	}

	mean_time_to_happen = {
		days = 33
	}

	title = "Iberian Masters"
	desc = "..."

	option = {
		name = "Si."
		ai_chance = { factor = 100 }
		set_country_flag = iberia_ai_start
		add_ai_strategy = {
			befriend = {
				id = POR
				value = 200
			}
			protect = {
				id = POR
				value = 50
			}
			rival = {
				id = POR
				value = 400
			}
			threat = {
				id = POR
				value = 300
			}
			antagonize = {
				id = POR
				value = 50
			}
			rival = {
				id = GBR
				value = 400
			}
			threat = {
				id = GBR
				value = 400
			}
			antagonize = {
				id = GBR
				value = 100
			}
			rival = {
				id = ENG
				value = 200
			}
			threat = {
				id = GBR
				value = 200
			}
			antagonize = {
				id = GBR
				value = 50
			}
			befriend = {
				id = FRA
				value = 50
			}
			rival = {
				id = FRA
				value = 400
			}
			threat = {
				id = FRA
				value = 400
			}
			rival = {
				id = MOR
				value = 250
			}
			threat = {
				id = MOR
				value = 150
			}
			antagonize = {
				id = MOR
				value = 100
			}
			rival = {
				id = ALG
				value = 150
			}
			threat = {
				id = ALG
				value = 50
			}
			antagonize = {
				id = ALG
				value = 50
			}
			threat = {
				id = TUR
				value = 300
			}
			vassal = {
				id = GRA
				value = 400
			}
			threat = {
				id = GRA
				value = 250
			}
			rival = {
				id = GRA
				value = 50
			}
			antagonize = {
				id = GRA
				value = 200
			}
			antagonize = {
				id = AZT
				value = 400
			}
			antagonize = {
				id = CHM
				value = 400
			}
			antagonize = {
				id = HUA
				value = 400
			}
			antagonize = {
				id = INC
				value = 400
			}
			antagonize = {
				id = TLA
				value = 400
			}
			explore_prov = {
				id = 778
				value = 400
			}
			colonize_prov = {
				id = 778
				value = 350
			}
			explore_prov = {
				id = 779
				value = 400
			}
			colonize_prov = {
				id = 779
				value = 350
			}
			explore_prov = {
				id = 780
				value = 400
			}
			colonize_prov = {
				id = 780
				value = 350
			}
			explore_prov = {
				id = 781
				value = 400
			}
			colonize_prov = {
				id = 781
				value = 350
			}
			explore_prov = {
				id = 782
				value = 400
			}
			colonize_prov = {
				id = 782
				value = 100
			}
			explore_prov = {
				id = 783
				value = 400
			}
			colonize_prov = {
				id = 783
				value = 300
			}
			explore_prov = {
				id = 784
				value = 400
			}
			colonize_prov = {
				id = 784
				value = 300
			}
			explore_prov = {
				id = 785
				value = 400
			}
			colonize_prov = {
				id = 785
				value = 300
			}
			explore_prov = {
				id = 786
				value = 400
			}
			colonize_prov = {
				id = 786
				value = 300
			}
			explore_prov = {
				id = 787
				value = 400
			}
			colonize_prov = {
				id = 787
				value = 300
			}
			explore_prov = {
				id = 788
				value = 400
			}
			colonize_prov = {
				id = 788
				value = 300
			}
			explore_prov = {
				id = 1095
				value = 400
			}
			colonize_prov = {
				id = 1095
				value = 50
			}
			explore_prov = {
				id = 747
				value = 400
			}
			colonize_prov = {
				id = 747
				value = 250
			}
			explore_prov = {
				id = 746
				value = 400
			}
			colonize_prov = {
				id = 746
				value = 250
			}
			explore_prov = {
				id = 745
				value = 400
			}
			colonize_prov = {
				id = 745
				value = 250
			}
			explore_prov = {
				id = 744
				value = 400
			}
			colonize_prov = {
				id = 744
				value = 250
			}
			explore_prov = {
				id = 743
				value = 400
			}
			colonize_prov = {
				id = 743
				value = 300
			}
			explore_prov = {
				id = 741
				value = 400
			}
			colonize_prov = {
				id = 741
				value = 350
			}
			explore_prov = {
				id = 831
				value = 400
			}
			colonize_prov = {
				id = 831
				value = 350
			}
			explore_prov = {
				id = 830
				value = 400
			}
			colonize_prov = {
				id = 830
				value = 350
			}
			explore_prov = {
				id = 829
				value = 400
			}
			colonize_prov = {
				id = 829
				value = 350
			}
			explore_prov = {
				id = 828
				value = 400
			}
			colonize_prov = {
				id = 828
				value = 350
			}
			explore_prov = {
				id = 827
				value = 400
			}
			colonize_prov = {
				id = 827
				value = 350
			}
			explore_prov = {
				id = 826
				value = 400
			}
			colonize_prov = {
				id = 826
				value = 350
			}
			explore_prov = {
				id = 835
				value = 400
			}
			colonize_prov = {
				id = 835
				value = 350
			}
			explore_prov = {
				id = 836
				value = 400
			}
			colonize_prov = {
				id = 836
				value = 200
			}
			explore_prov = {
				id = 838
				value = 400
			}
			colonize_prov = {
				id = 838
				value = 200
			}
			explore_prov = {
				id = 484
				value = 400
			}
			colonize_prov = {
				id = 484
				value = 400
			}
			explore_prov = {
				id = 485
				value = 400
			}
			colonize_prov = {
				id = 485
				value = 300
			}
			explore_prov = {
				id = 486
				value = 400
			}
			colonize_prov = {
				id = 485
				value = 350
			}
			explore_prov = {
				id = 502
				value = 400
			}
			colonize_prov = {
				id = 502
				value = 300
			}
			explore_prov = {
				id = 490
				value = 400
			}
			colonize_prov = {
				id = 490
				value = 400
			}
			explore_prov = {
				id = 492
				value = 400
			}
			colonize_prov = {
				id = 492
				value = 400
			}
			explore_prov = {
				id = 926
				value = 400
			}
			colonize_prov = {
				id = 926
				value = 250
			}
			explore_prov = {
				id = 792
				value = 400
			}
			colonize_prov = {
				id = 792
				value = 200
			}
			explore_prov = {
				id = 793
				value = 400
			}
			colonize_prov = {
				id = 793
				value = 200
			}
			explore_prov = {
				id = 796
				value = 400
			}
			colonize_prov = {
				id = 796
				value = 200
			}
			explore_prov = {
				id = 791
				value = 400
			}
			colonize_prov = {
				id = 791
				value = 150
			}
			explore_prov = {
				id = 794
				value = 400
			}
			colonize_prov = {
				id = 794
				value = 150
			}
			explore_prov = {
				id = 806
				value = 400
			}
			explore_prov = {
				id = 805
				value = 400
			}
			explore_prov = {
				id = 809
				value = 400
			}
			explore_prov = {
				id = 812
				value = 400
			}
			explore_prov = {
				id = 816
				value = 400
			}
			explore_prov = {
				id = 819
				value = 400
			}
			explore_prov = {
				id = 823
				value = 400
			}
			explore_prov = {
				id = 846
				value = 400
			}
			explore_prov = {
				id = 845
				value = 400
			}
			explore_prov = {
				id = 839
				value = 400
			}
			explore_prov = {
				id = 841
				value = 400
			}
			explore_prov = {
				id = 847
				value = 400
			}
			explore_prov = {
				id = 849
				value = 400
			}
			explore_prov = {
				id = 851
				value = 400
			}
			explore_prov = {
				id = 848
				value = 400
			}
			explore_prov = {
				id = 858
				value = 400
			}
			explore_prov = {
				id = 854
				value = 400
			}
			conquer_prov = {
				id = 835
				value = 400
			}
			conquer_prov = {
				id = 836
				value = 400
			}
			conquer_prov = {
				id = 837
				value = 400
			}
			conquer_prov = {
				id = 838
				value = 400
			}
			conquer_prov = {
				id = 839
				value = 400
			}
			conquer_prov = {
				id = 840
				value = 400
			}
			conquer_prov = {
				id = 841
				value = 400
			}
			conquer_prov = {
				id = 842
				value = 400
			}
			conquer_prov = {
				id = 843
				value = 400
			}
			conquer_prov = {
				id = 844
				value = 400
			}
			conquer_prov = {
				id = 845
				value = 400
			}
			conquer_prov = {
				id = 846
				value = 400
			}
			conquer_prov = {
				id = 847
				value = 400
			}
			conquer_prov = {
				id = 848
				value = 400
			}
			conquer_prov = {
					id = 849
					value = 400
				}
			conquer_prov = {
				id = 850
				value = 400
			}
			conquer_prov = {
				id = 851
				value = 400
			}
			conquer_prov = {
				id = 852
				value = 400
			}
			conquer_prov = {
				id = 853
				value = 400
			}
			conquer_prov = {
				id = 854
				value = 400
			}
			conquer_prov = {
				id = 855
				value = 400
			}
			conquer_prov = {
				id = 856
				value = 400
			}
			conquer_prov = {
				id = 857
				value = 400
			}
			conquer_prov = {
				id = 858
				value = 400
			}
			conquer_prov = {
				id = 859
				value = 400
			}
			conquer_prov = {
				id = 860
				value = 400
			}
			conquer_prov = {
				id = 861
				value = 400
			}
			conquer_prov = {
				id = 862
				value = 400
			}
			conquer_prov = {
				id = 863
				value = 400
			}
			conquer_prov = {
				id = 864
				value = 400
			}
			conquer_prov = {
				id = 865
				value = 400
			}
			conquer_prov = {
				id = 866
				value = 400
			}
			conquer_prov = {
				id = 867
				value = 400
			}
			conquer_prov = {
				id = 868
				value = 400
			}
			conquer_prov = {
				id = 869
				value = 400
			}
			conquer_prov = {
				id = 870
				value = 400
			}
			conquer_prov = {
				id = 871
				value = 400
			}
			conquer_prov = {
				id = 872
				value = 400
			}
			conquer_prov = {
				id = 873
				value = 400
			}
			conquer_prov = {
				id = 874
				value = 400
			}
			conquer_prov = {
				id = 875
				value = 400
			}
			conquer_prov = {
				id = 876
				value = 400
			}
			conquer_prov = {
				id = 877
				value = 400
			}
			conquer_prov = {
				id = 878
				value = 400
			}
			conquer_prov = {
				id = 879
				value = 400
			}
			conquer_prov = {
				id = 880
				value = 400
			}
			conquer_prov = {
				id = 881
				value = 400
			}
			conquer_prov = {
				id = 882
				value = 400
			}
			conquer_prov = {
				id = 883
				value = 400
			}
			conquer_prov = {
				id = 884
				value = 400
			}
			conquer_prov = {
				id = 2059
				value = 400
			}
			conquer_prov = {
				id = 2060
				value = 400
			}
			conquer_prov = {
				id = 2061
				value = 400
			}
			conquer_prov = {
				id = 2062
				value = 400
			}
			conquer_prov = {
				id = 2063
				value = 400
			}
			conquer_prov = {
				id = 2064
				value = 400
			}
			conquer_prov = {
				id = 2065
				value = 400
			}
			conquer_prov = {
				id = 2066
				value = 400
			}
			conquer_prov = {
				id = 2067
				value = 400
			}
			conquer_prov = {
				id = 2068
				value = 400
			}
			conquer_prov = {
				id = 2069
				value = 400
			}
			conquer_prov = {
				id = 2070
				value = 400
			}
			conquer_prov = {
				id = 2071
				value = 400
			}
			conquer_prov = {
				id = 2072
				value = 400
			}
			conquer_prov = {
				id = 2073
				value = 400
			}
			conquer_prov = {
				id = 2074
				value = 400
			}
			conquer_prov = {
				id = 2075
				value = 400
			}
			conquer_prov = {
				id = 2076
				value = 400
			}
			conquer_prov = {
				id = 2077
				value = 400
			}
			conquer_prov = {
				id = 2078
				value = 400
			}
			conquer_prov = {
				id = 2079
				value = 400
			}
			conquer_prov = {
				id = 2080
				value = 400
			}
			conquer_prov = {
				id = 2081
				value = 400
			}
			conquer_prov = {
				id = 2082
				value = 400
			}
			conquer_prov = {
				id = 2083
				value = 400
			}
			conquer_prov = {
				id = 2084
				value = 400
			}
			conquer_prov = {
				id = 2085
				value = 400
			}
			conquer_prov = {
				id = 2086
				value = 400
			}
			conquer_prov = {
				id = 2087
				value = 400
			}
			conquer_prov = {
				id = 2088
				value = 400
			}
			conquer_prov = {
				id = 2142
				value = 400
			}
			conquer_prov = {
				id = 1942
				value = 400
			}
#TODO Andes, Philippines, Carribean, Florida
		}
	}
}

country_event = {

	id = 666904
	
	trigger = {
		NOT = { has_country_flag = ottoman_ai_start }
		tag = TUR
		ai = yes
  	}

	mean_time_to_happen = {
		days = 33
	}

	title = "Ottoman Dreams"
	desc = "..."

	option = {
		name = "Inch'allah!"
		ai_chance = { factor = 100 }
		set_country_flag = ottoman_ai_start
		land_tech = 0.33
		government_tech = 0.33
		naval_tech = 0.22
		production_tech = 0.22
		add_ai_strategy = {
			befriend = {
				id = CRI
				value = 200
			}
			protect = {
				id = CRI
				value = 250
			}
			rival = {
				id = PER
				value = 400
			}
			threat = {
				id = PER
				value = 300
			}
			antagonize = {
				id = PER
				value = 50
			}
			rival = {
				id = HUN
				value = 400
			}
			threat = {
				id = HUN
				value = 400
			}
			antagonize = {
				id = HUN
				value = 100
			}
			rival = {
				id = HAB
				value = 200
			}
			threat = {
				id = HAB
				value = 200
			}
			antagonize = {
				id = HAB
				value = 50
			}
			befriend = {
				id = WAL
				value = 50
			}
			rival = {
				id = RUS
				value = 400
			}
			threat = {
				id = RUS
				value = 400
			}
			rival = {
				id = SPA
				value = 250
			}
			threat = {
				id = SPA
				value = 150
			}
			antagonize = {
				id = MAM
				value = 100
			}
			rival = {
				id = MAM
				value = 150
			}
			threat = {
				id = MAM
				value = 50
			}
			antagonize = {
				id = RZP
				value = 50
			}
			threat = {
				id = RZP
				value = 300
			}
			vassal = {
				id = WAL
				value = 400
			}
			threat = {
				id = MOL
				value = 250
			}
			rival = {
				id = VEN
				value = 50
			}
			antagonize = {
				id = GEN
				value = 200
			}
			antagonize = {
				id = BUL
				value = 400
			}
			antagonize = {
				id = BYZ
				value = 400
			}
			antagonize = {
				id = GRE
				value = 400
			}
			antagonize = {
				id = SER
				value = 400
			}
			antagonize = {
				id = ALB
				value = 400
			}
			antagonize = {
				id = TRE
				value = 400
			}
			antagonize = {
				id = GEO
				value = 400
			}
			antagonize = {
				id = AMI
				value = 400
			}
			antagonize = {
				id = KAR
				value = 400
			}
			antagonize = {
				id = AYD
				value = 400
			}
			conquer_prov = {
				id = 142
				value = 400
			}
			conquer_prov = {
				id = 143
				value = 400
			}
			conquer_prov = {
				id = 144
				value = 400
			}
			conquer_prov = {
				id = 145
				value = 400
			}
			conquer_prov = {
				id = 146
				value = 400
			}
			conquer_prov = {
				id = 147
				value = 400
			}
			conquer_prov = {
				id = 148
				value = 400
			}
			conquer_prov = {
				id = 149
				value = 400
			}
			conquer_prov = {
				id = 151
				value = 400
			}
			conquer_prov = {
				id = 163
				value = 400
			}
			conquer_prov = {
				id = 164
				value = 400
			}
			conquer_prov = {
				id = 1316
				value = 400
			}
			conquer_prov = {
				id = 1369
				value = 400
			}
			conquer_prov = {
				id = 1935
				value = 400
			}
			conquer_prov = {
				id = 1941
				value = 400
			}
			conquer_prov = {
				id = 2179
				value = 400
			}
			conquer_prov = {
				id = 316
				value = 400
			}
			conquer_prov = {
				id = 317
				value = 400
			}
			conquer_prov = {
				id = 318
				value = 400
			}
			conquer_prov = {
				id = 319
				value = 400
			}
			conquer_prov = {
				id = 320
				value = 400
			}
			conquer_prov = {
				id = 321
				value = 400
			}
			conquer_prov = {
				id = 322
				value = 400
			}
			conquer_prov = {
				id = 323
				value = 400
			}
			conquer_prov = {
				id = 324
				value = 400
			}
			conquer_prov = {
				id = 325
				value = 400
			}
			conquer_prov = {
				id = 326
				value = 400
			}
			conquer_prov = {
				id = 327
				value = 400
			}
			conquer_prov = {
				id = 328
				value = 400
			}
			conquer_prov = {
				id = 329
				value = 400
			}
			conquer_prov = {
				id = 330
				value = 400
			}
			conquer_prov = {
				id = 331
				value = 400
			}
			conquer_prov = {
				id = 332
				value = 400
			}
			conquer_prov = {
				id = 1367
				value = 400
			}
			conquer_prov = {
				id = 1368
				value = 400
			}
			conquer_prov = {
				id = 1372
				value = 400
			}
			conquer_prov = {
				id = 1373
				value = 400
			}
			conquer_prov = {
				id = 150
				value = 300
			}
			conquer_prov = {
				id = 159
				value = 300
			}
			conquer_prov = {
				id = 1928
				value = 300
			}
			conquer_prov = {
				id = 2177
				value = 300
			}
			conquer_prov = {
				id = 137
				value = 300
			}
			conquer_prov = {
				id = 138
				value = 300
			}
			conquer_prov = {
				id = 139
				value = 300
			}
			conquer_prov = {
				id = 140
				value = 300
			}
			conquer_prov = {
				id = 141
				value = 300
			}
			conquer_prov = {
				id = 377
				value = 200
			}
			conquer_prov = {
				id = 407
				value = 200
			}
			conquer_prov = {
				id = 378
				value = 200
			}
			conquer_prov = {
				id = 382
				value = 200
			}
			conquer_prov = {
				id = 379
				value = 200
			}
			conquer_prov = {
				id = 381
				value = 200
			}
			conquer_prov = {
				id = 364
				value = 200
			}
			conquer_prov = {
				id = 129
				value = 200
			}
			conquer_prov = {
				id = 130
				value = 200
			}
			conquer_prov = {
				id = 131
				value = 200
			}
			conquer_prov = {
				id = 136
				value = 200
			}
			conquer_prov = {
				id = 152
				value = 200
			}
			conquer_prov = {
				id = 1929
				value = 200
			}
			conquer_prov = {
				id = 358
				value = 100
			}
			conquer_prov = {
				id = 359
				value = 100
			}
			conquer_prov = {
				id = 361
				value = 100
			}
			conquer_prov = {
				id = 362
				value = 100
			}
			conquer_prov = {
				id = 363
				value = 100
			}
			conquer_prov = {
				id = 364
				value = 100
			}
			conquer_prov = {
				id = 365
				value = 100
			}
			conquer_prov = {
				id = 1231
				value = 100
			}
			conquer_prov = {
				id = 1233
				value = 100
			}
			conquer_prov = {
				id = 1234
				value = 100
			}
			conquer_prov = {
				id = 135
				value = 100
			}
			conquer_prov = {
				id = 153
				value = 100
			}
			conquer_prov = {
				id = 155
				value = 100
			}
			conquer_prov = {
				id = 156
				value = 100
			}
			conquer_prov = {
				id = 157
				value = 100
			}
			conquer_prov = {
				id = 158
				value = 100
			}
			conquer_prov = {
				id = 2143
				value = 100
			}
			conquer_prov = {
				id = 2144
				value = 100
			}
			conquer_prov = {
				id = 2176
				value = 100
			}
			conquer_prov = {
				id = 1263
				value = 100
			}
			conquer_prov = {
				id = 2170
				value = 100
			}
			conquer_prov = {
				id = 282
				value = 100
			}
		}
	}
}
