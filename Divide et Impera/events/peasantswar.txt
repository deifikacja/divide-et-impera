# Peasants' War
country_event = {
	
	id = 3021
	
	major = yes

	trigger = {
		num_of_cities = 5
		NOT = { has_country_flag = peasant_war }
		OR = { 
			government = feudal_monarchy
			government = caste_monarchy
			government = despotic_monarchy
			government = administrative_monarchy
			government = noble_republic
			government = eastern_despotism
			government = oligarchical_monarchy
		}
		NOT = { serfdom_freesubjects = -2 }
		centralization_decentralization = 2
	}

	mean_time_to_happen = {
	
		months = 430
	
		modifier = {
			factor = 0.95
			NOT = { serfdom_freesubjects = -3 }
		}
		modifier = {
			factor = 0.85
			NOT = { serfdom_freesubjects = -4 }
		}
		modifier = {
			factor = 0.95
			centralization_decentralization = 3
		}
		modifier = {
			factor = 0.85
			centralization_decentralization = 4
		}
		modifier = {
			factor = 0.85
			NOT = { relation = { who = PAP value = 0 } }
		}
		modifier = {
			factor = 0.75
			NOT = { relation = { who = PAP value = -100 } }
		}
		modifier = {
			factor = 1.15
			relation = { who = PAP value = 0 }
		}
		modifier = {
			factor = 1.25
			relation = { who = PAP value = 100 }
		}
		modifier = {
			factor = 1.2
			idea = humanist_tolerance
		}
		modifier = {
			factor = 0.9
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 1
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.1
			stability = 3
		}		
	}

	title = "EVTNAME3021"
	desc = "EVTDESC3021"

	immediate = {
		set_country_flag = peasant_war
		stability = -3
	}
	option = {
		name = "EVTOPTA3021"			# Dire times are ahead of us
		prestige = -0.05
	}
}


# Peasant protests
country_event = {
	id = 3022

	trigger = {
		has_country_flag = peasant_war
		NOT = { has_country_flag = peasant_protests }
	}

	mean_time_to_happen = {
	
		months = 28
		
		modifier = {
			factor = 0.9
			NOT = { serfdom_freesubjects = -4 }
		}
		modifier = {
			factor = 0.9
			centralization_decentralization = 4
		}
		modifier = {
			factor = 0.95
			revolt_percentage = 0.1
		}
		modifier = {
			factor = 0.85
			revolt_percentage = 0.3
		}
		modifier = {
			factor = 0.75
			revolt_percentage = 0.5
		}
		modifier = {
			factor = 1.05
			stability = 1
		}
		modifier = {
			factor = 1.05
			stability = 2
		}
		modifier = {
			factor = 1.05
			stability = 3
		}
		modifier = {
			factor = 0.95
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 0.95
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.95
			NOT = { stability = -2 }
		}
	}

	title = "EVTNAME3022"
	desc = "EVTDESC3022"

	option = {
		name = "EVTOPTA3022"			# Enforce serfdom
		ai_chance = { factor = 65 }
		set_country_flag = peasant_protests
		random_owned = { anti_tax_rebels = 1 }
		serfdom_freesubjects = -1
		add_country_modifier = {
			name = "enforce_serfdom"
			duration = 720
		}
	}
	option = {
		name = "EVTOPTB3022"			# Restrict serfdom
		ai_chance = { factor = 35 }
		set_country_flag = peasant_protests
		serfdom_freesubjects = 1
		add_country_modifier = {
			name = "restrict_serfdom"
			duration = 720
		}
	}
}


# Religious revolt
country_event = {
	id = 3023

	trigger = {
		has_country_flag = peasant_war
		NOT = { has_country_flag = religious_revolt }
		NOT = { protestant = 0 }
		NOT = { reformed = 0 }
	}

	mean_time_to_happen = {
	
		months = 32
		
		modifier = {
			factor = 0.9
			NOT = { protestant = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { protestant = -2 }
		}
		modifier = {
			factor = 0.9
			NOT = { protestant = -3 }		
		}
		modifier = {
			factor = 0.9
			NOT = { reformed = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { reformed = -2 }
		}
		modifier = {
			factor = 0.9
			NOT = { reformed = -3 }		
		}
		modifier = {
			factor = 0.9
			relation = { who = PAP value = 100 }
		}
		modifier = {
			factor = 0.8
			relation = { who = PAP value = 150 }
		}
		modifier = {
			factor = 1.1
			NOT = { relation = { who = PAP value = -100 } }		
		}
		modifier = {
			factor = 1.2
			NOT = { relation = { who = PAP value = -150 } }		
		}
	}

	title = "EVTNAME3023"
	desc = "EVTDESC3023"

	option = {
		name = "EVTOPTA3023"			# Crush the rebels!
		ai_chance = { factor = 60 }		
		set_country_flag = religious_revolt
		random_owned = { anti_tax_rebels = 1 }
		add_country_modifier = {
			name = "religious_intolerance"
			duration = 720
		}
	}
	option = {
		name = "EVTOPTB3023"			# Let them chose their own faith
		ai_chance = { factor = 40 }
		set_country_flag = religious_revolt
		relation = { who = PAP value = -100 }
		add_country_modifier = {
			name = "religious_tolerance"
			duration = 720
		}
	}
}


# Obsolete forces
country_event = {
	id = 3024

	trigger = {
		has_country_flag = peasant_war
		NOT = { has_country_flag = obsolete_forces }
		revolt_percentage = 0.1
		NOT = { stability = 0 }
	}

	mean_time_to_happen = {
	
		months = 34

		modifier = {
			factor = 0.9
			revolt_percentage = 0.2
		}
		modifier = {
			factor = 0.9
			revolt_percentage = 0.3
		}
		modifier = {
			factor = 0.9
			revolt_percentage = 0.4
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			ADM = 7
		}
		modifier = {
			factor = 1.1
			ADM = 8
		}
	}

	title = "EVTNAME3024"
	desc = "EVTDESC3024"

	option = {
		name = "EVTOPTA3024"			# They have no place in our society
		ai_chance = { factor = 50 }
		set_country_flag = obsolete_forces
		add_country_modifier = {
			name = "obsolete_forces"
			duration = 720
		}
	}
	option = {
		name = "EVTOPTB3024"			# They may still be of some use
		ai_chance = { factor = 50 }
		set_country_flag = obsolete_forces
		treasury = -80
		add_country_modifier = {
			name = "support_troops"
			duration = 720
		}
	}
}



# National decentralisation
country_event = {
	id = 3025

	trigger = {
		has_country_flag = peasant_war
		NOT = { has_country_flag = national_decentralisation }
		centralization_decentralization = 3
		NOT = { ADM = 4 }
		NOT = { MIL = 4 }
	}

	mean_time_to_happen = {
	
		months = 30
		
		modifier = {
			factor = 0.9
			centralization_decentralization = 4
		}
		modifier = {
			factor = 0.9
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 1
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.1
			stability = 3
		}
	}

	title = "EVTNAME3025"
	desc = "EVTDESC3025"

	option = {
		name = "EVTOPTA3025"			# Crush the rebels
		ai_chance = { factor = 55 }
		set_country_flag = national_decentralisation
		random_owned = {
			limit = { is_core = THIS }
			anti_tax_rebels = 3
		}
		add_country_modifier = {
			name = "isolate_rebels"
			duration = 720
		}
	}
	option = {
		name = "EVTOPTB3025"			# We have to negotiate a solution
		ai_chance = { factor = 45 }	
		set_country_flag = national_decentralisation		
		treasury = -30
		add_country_modifier = {
			name = "peasant_negotiation"
			duration = 720
		}
		clr_country_flag = peasant_war
	}
}



# Order is returning
country_event = {
	id = 3026
	
	major = yes

	trigger = {
		has_country_flag = peasant_war
		stability = 1
		NOT = { num_of_revolts = 1 }
	}

	mean_time_to_happen = {
	
		months = 72
		
		modifier = {
			factor = 0.9
			MIL = 5
		}
		modifier = {
			factor = 0.8
			MIL = 6
		}
		modifier = {
			factor = 0.9
			ADM = 5
		}	
		modifier = {
			factor = 0.8
			ADM = 6
		}
		modifier = {
			factor = 0.9
			stability = 1
		}
		modifier = {
			factor = 0.9
			stability = 2
		}
		modifier = {
			factor = 0.9
			stability = 3
		}
		modifier = {
			factor = 1.1
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 1.1
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 1.1
			NOT = { stability = -2 }
		}
	}

	title = "EVTNAME3026"
	desc = "EVTDESC3026"

	option = {
		name = "EVTOPTA3026"			# Restore order
		clr_country_flag = peasant_war
		clr_country_flag = peasant_protests
		clr_country_flag = religious_revolt
		clr_country_flag = obsolete_forces
		clr_country_flag = national_decentralisation		
	}
}
