province_event = {

	id = 2550
	
	trigger = {
		religion = orthodox
		NOT = { is_religion_enabled = graecocatholic }
		owner = { 
			advisor = theologian
			religion = catholic
			NOT = { government = papal_government }
			secularism_theocracy = -1
		}
		year = 1550

	}
	
	mean_time_to_happen = {
		months = 600


		modifier = {
			factor = 0.8
			owner = { defender_of_faith = yes }
		}
		modifier = {
			factor = 2.0
			owner = { NOT = { secularism_theocracy = 0 } }
		}
		modifier = {
			factor = 1.2
			owner = { NOT = { secularism_theocracy = 1 } }
		}
		modifier = {
			factor = 0.9
			owner = { secularism_theocracy = 2 }
		}
		modifier = {
			factor = 0.9
			owner = { secularism_theocracy = 3 }
		}
		modifier = {
			factor = 0.8
			owner = { secularism_theocracy = 4 }
		}
		modifier = {
			factor = 0.6
			owner = { secularism_theocracy = 5 }
		}
		modifier = {
			factor = 0.9
			owner = { idea = deus_vult }
		}
		modifier = {
			factor = 0.9
			owner = { idea = divine_supremacy }
		}
		modifier = {
			factor = 2.0
			owner = { idea = humanist_tolerance }
		}
		modifier = {
			factor = 0.9
			owner = { relation = { who = PAP value = 50 } }
		}
		modifier = {
			factor = 0.8
			owner = { relation = { who = PAP value = 100 } }
		}
		modifier = {
			factor = 0.75
			owner = { relation = { who = PAP value = 150 } }
		}		
		modifier = {
			factor = 0.7
			owner = { government = theocratic_government }
		}		
	}
	
	title = "EVTNAME2550"
	desc = "EVTDESC2550"
	
	option = {
		name = "EVTOPTA2550"
		ai_chance = { factor = 75 }
		religion = graecocatholic
		random_neighbor_province = {
			limit = {
				religion = orthodox
				owned_by = THIS
			}
			religion = graecocatholic
		}
		owner = { 
			enable_religion = graecocatholic
			prestige = 0.1
			relation = { who = PAP value = 100 } 
			random_owned = {
				limit = { religion = orthodox }
				create_revolt = 2 
			}
		}
	}
	option = {
		name = "EVTOPTB2550"
		ai_chance = { factor = 15 }
		religion = graecocatholic
		owner = { 
			enable_religion = graecocatholic
			random_owned = {
				limit = { religion = orthodox }
				create_revolt = 1 
			}
		}
	}
	option = {
		name = "EVTOPTC2550"
		ai_chance = { factor = 10 }
		owner = { 
			prestige = -0.06
			relation = { who = PAP value = -50 }
		}
	}
		

}

province_event = {

	id = 2551
	
	trigger = {
		religion = greek_orthodox
		NOT = { is_religion_enabled = graecocatholic }
		owner = { 
			advisor = theologian
			religion = catholic
			NOT = { government = papal_government }
			secularism_theocracy = -1
		}
		year = 1550

	}
	
	mean_time_to_happen = {
		months = 600


		modifier = {
			factor = 0.8
			owner = { defender_of_faith = yes }
		}
		modifier = {
			factor = 2.0
			owner = { NOT = { secularism_theocracy = 0 } }
		}
		modifier = {
			factor = 1.2
			owner = { NOT = { secularism_theocracy = 1 } }
		}
		modifier = {
			factor = 0.9
			owner = { secularism_theocracy = 2 }
		}
		modifier = {
			factor = 0.9
			owner = { secularism_theocracy = 3 }
		}
		modifier = {
			factor = 0.8
			owner = { secularism_theocracy = 4 }
		}
		modifier = {
			factor = 0.6
			owner = { secularism_theocracy = 5 }
		}
		modifier = {
			factor = 0.9
			owner = { idea = deus_vult }
		}
		modifier = {
			factor = 0.9
			owner = { idea = divine_supremacy }
		}
		modifier = {
			factor = 2.0
			owner = { idea = humanist_tolerance }
		}
		modifier = {
			factor = 0.9
			owner = { relation = { who = PAP value = 50 } }
		}
		modifier = {
			factor = 0.8
			owner = { relation = { who = PAP value = 100 } }
		}
		modifier = {
			factor = 0.75
			owner = { relation = { who = PAP value = 150 } }
		}		
		modifier = {
			factor = 0.7
			owner = { government = theocratic_government }
		}		
	}
	
	title = "EVTNAME2551"
	desc = "EVTDESC2551"
	
	option = {
		name = "EVTOPTA2551"
		ai_chance = { factor = 75 }
		religion = graecocatholic
		random_neighbor_province = {
			limit = {
				religion = greek_orthodox
				owned_by = THIS
			}
			religion = graecocatholic
		}
		owner = { 
			enable_religion = graecocatholic
			prestige = 0.1
			relation = { who = PAP value = 100 } 
			random_owned = {
				limit = { religion = greek_orthodox }
				create_revolt = 1 
			}
		}
	}
	option = {
		name = "EVTOPTB2551"
		ai_chance = { factor = 15 }
		religion = graecocatholic
		owner = { 
			random_owned = {
				limit = { religion = greek_orthodox }
				create_revolt = 1 
			}
		}
	}
	option = {
		name = "EVTOPTC2551"
		ai_chance = { factor = 10 }
		owner = { 
			enable_religion = graecocatholic
			prestige = -0.06
			relation = { who = PAP value = -50 }
		}
	}
}

province_event = {

	id = 2552
	
	trigger = {
		religion = orthodox
		is_religion_enabled = graecocatholic
		owner = { 
			advisor = theologian
			religion = catholic
			NOT = { government = papal_government }
			secularism_theocracy = -1
			NOT = { has_country_flag = union_of_churches }
		}
		year = 1550

	}
	
	mean_time_to_happen = {
		months = 600


		modifier = {
			factor = 0.8
			owner = { defender_of_faith = yes }
		}
		modifier = {
			factor = 2.0
			owner = { NOT = { secularism_theocracy = 0 } }
		}
		modifier = {
			factor = 1.2
			owner = { NOT = { secularism_theocracy = 1 } }
		}
		modifier = {
			factor = 0.9
			owner = { secularism_theocracy = 2 }
		}
		modifier = {
			factor = 0.9
			owner = { secularism_theocracy = 3 }
		}
		modifier = {
			factor = 0.8
			owner = { secularism_theocracy = 4 }
		}
		modifier = {
			factor = 0.6
			owner = { secularism_theocracy = 5 }
		}
		modifier = {
			factor = 0.9
			owner = { idea = deus_vult }
		}
		modifier = {
			factor = 0.9
			owner = { idea = divine_supremacy }
		}
		modifier = {
			factor = 2.0
			owner = { idea = humanist_tolerance }
		}
		modifier = {
			factor = 0.9
			owner = { relation = { who = PAP value = 50 } }
		}
		modifier = {
			factor = 0.8
			owner = { relation = { who = PAP value = 100 } }
		}
		modifier = {
			factor = 0.75
			owner = { relation = { who = PAP value = 150 } }
		}		
		modifier = {
			factor = 0.7
			owner = { government = theocratic_government }
		}		
	}
	
	title = "EVTNAME2552"
	desc = "EVTDESC2552"
	
	option = {
		name = "EVTOPTA2552"
		ai_chance = { factor = 75 }
		religion = graecocatholic
		random_neighbor_province = {
			limit = {
				religion = orthodox
				owned_by = THIS
			}
			religion = graecocatholic
		}
		owner = { 
			set_country_flag = union_of_churches
			prestige = 0.1
			relation = { who = PAP value = 100 } 
			random_owned = {
				limit = { religion = orthodox }
				create_revolt = 1 
			}
		}
	}
	option = {
		name = "EVTOPTB2552"
		ai_chance = { factor = 15 }
		religion = graecocatholic
		owner = { 
			set_country_flag = union_of_churches
			random_owned = {
				limit = { religion = orthodox }
				create_revolt = 1 
			}
		}
	}
	option = {
		name = "EVTOPTC2552"
		ai_chance = { factor = 10 }
		owner = { 
			set_country_flag = union_of_churches
			prestige = -0.05
			relation = { who = PAP value = -50 }
			stability = -1
		}
	}
}

province_event = {

	id = 2553
	
	trigger = {
		religion = greek_orthodox
		is_religion_enabled = graecocatholic
		owner = { 
			NOT = { has_country_flag = union_of_churches }
			advisor = theologian
			religion = catholic
			NOT = { government = papal_government }
			secularism_theocracy = -1
		}
		year = 1550

	}
	
	mean_time_to_happen = {
		months = 600


		modifier = {
			factor = 0.8
			owner = { defender_of_faith = yes }
		}
		modifier = {
			factor = 2.0
			owner = { NOT = { secularism_theocracy = 0 } }
		}
		modifier = {
			factor = 1.2
			owner = { NOT = { secularism_theocracy = 1 } }
		}
		modifier = {
			factor = 0.9
			owner = { secularism_theocracy = 2 }
		}
		modifier = {
			factor = 0.9
			owner = { secularism_theocracy = 3 }
		}
		modifier = {
			factor = 0.8
			owner = { secularism_theocracy = 4 }
		}
		modifier = {
			factor = 0.6
			owner = { secularism_theocracy = 5 }
		}
		modifier = {
			factor = 0.9
			owner = { idea = deus_vult }
		}
		modifier = {
			factor = 0.9
			owner = { idea = divine_supremacy }
		}
		modifier = {
			factor = 2.0
			owner = { idea = humanist_tolerance }
		}
		modifier = {
			factor = 0.9
			owner = { relation = { who = PAP value = 50 } }
		}
		modifier = {
			factor = 0.8
			owner = { relation = { who = PAP value = 100 } }
		}
		modifier = {
			factor = 0.75
			owner = { relation = { who = PAP value = 150 } }
		}		
		modifier = {
			factor = 0.7
			owner = { government = theocratic_government }
		}		
	}
	
	title = "EVTNAME2553"
	desc = "EVTDESC2553"
	
	option = {
		name = "EVTOPTA2553"
		ai_chance = { factor = 75 }
		religion = graecocatholic
		random_neighbor_province = {
			limit = {
				religion = greek_orthodox
				owned_by = THIS
			}
			religion = graecocatholic
		}
		owner = { 
			set_country_flag = union_of_churches
			prestige = 0.1
			relation = { who = PAP value = 100 } 
			random_owned = {
				limit = { religion = greek_orthodox }
				create_revolt = 1 
			}
		}
	}
	option = {
		name = "EVTOPTB2553"
		ai_chance = { factor = 15 }
		religion = graecocatholic
		owner = { 
			set_country_flag = union_of_churches
			random_owned = {
				limit = { religion = greek_orthodox }
				create_revolt = 1 
			}
		}
	}
	option = {
		name = "EVTOPTC2553"
		ai_chance = { factor = 10 }
		owner = { 
			set_country_flag = union_of_churches
			prestige = -0.05
			relation = { who = PAP value = -50 }
			stability = -1
		}
	}
}

country_event = {

	id = 2554

	trigger = {
		religion = orthodox
		NOT = { num_of_cities = 5 }
		NOT = { prestige = 10 }
		DIP = 5
		NOT = { has_country_flag = unionist }
		NOT = { is_religion_enabled = graecocatholic }
		NOT = { defender_of_faith = yes }
	}

	mean_time_to_happen = {
		months = 2400

		modifier = {
			factor = 0.5
			war = yes
		}
		modifier = {
			factor = 0.8
			any_neighbor_country = {
				NOT = { religion = orthodox }
			}
		}
		modifier = {
			factor = 0.8
			NOT = { num_of_cities = 2 }
		}
		modifier = {
			factor = 1.2
			num_of_cities = 4
		}
		modifier = {
			factor = 1.2
			secularism_theocracy = 4
		}
		modifier = {
			factor = 1.5
			secularism_theocracy = 5
		}
		modifier = {
			factor = 0.9
			NOT = { secularism_theocracy = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { secularism_theocracy = -2 }
		}
		modifier = {
			factor = 0.8
			NOT = { secularism_theocracy = -4 }
		}
		modifier = {
			factor = 1.1
			theologian = 1
		}
		modifier = {
			factor = 1.2
			theologian = 3
		}
		modifier = {
			factor = 1.5
			theologian = 5
		}
		modifier = {
			factor = 2.0
			theologian = 6
		}
		modifier = {
			factor = 0.9
			relation = { who = PAP value = 0 }
		}
		modifier = {
			factor = 0.8
			relation = { who = PAP value = 120 }
		}
		modifier = {
			factor = 0.9
			DIP = 8
		}
		modifier = {
			factor = 0.9
			NOT = { MIL = 5 }
		}
		modifier = {
			factor = 1.1
			MIL = 8
		}
		modifier = {
			factor = 2.5
			has_country_flag = nonunionist
		}
	}


	title = "EVTNAME2554"
	desc = "EVTDESC2554"

	option = {
		name = "EVTOPTA2554"
		ai_chance = { factor = 10 }
		religion = graecocatholic
		capital_scope = { 
			religion = graecocatholic
			create_revolt = 2
		}
		stability = -3
		quality_quantity = -1
		set_country_flag = unionist
	}
	option = {
		name = "EVTOPTB2554"
		ai_chance = { factor = 35 }
		religion = graecocatholic
		stability = -2
		quality_quantity = -1
		set_country_flag = unionist
	}
	option = {
		name = "EVTOPTC2554"
		ai_chance = { factor = 55 }
		prestige = 0.02
		set_country_flag = nonunionist
	}
}

country_event = {

	id = 2555

	trigger = {
		religion = greek_orthodox
		NOT = { num_of_cities = 5 }
		NOT = { prestige = 10 }
		DIP = 5
		NOT = { has_country_flag = unionist }
		NOT = { is_religion_enabled = graecocatholic }
		NOT = { defender_of_faith = yes }
	}

	mean_time_to_happen = {
		months = 2000

		modifier = {
			factor = 0.5
			war = yes
		}
		modifier = {
			factor = 0.8
			any_neighbor_country = {
				NOT = { religion = greek_orthodox }
			}
		}
		modifier = {
			factor = 0.8
			NOT = { num_of_cities = 2 }
		}
		modifier = {
			factor = 1.2
			num_of_cities = 4
		}
		modifier = {
			factor = 1.2
			secularism_theocracy = 4
		}
		modifier = {
			factor = 1.5
			secularism_theocracy = 5
		}
		modifier = {
			factor = 0.9
			NOT = { secularism_theocracy = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { secularism_theocracy = -2 }
		}
		modifier = {
			factor = 0.8
			NOT = { secularism_theocracy = -4 }
		}
		modifier = {
			factor = 1.1
			theologian = 1
		}
		modifier = {
			factor = 1.2
			theologian = 3
		}
		modifier = {
			factor = 1.5
			theologian = 5
		}
		modifier = {
			factor = 2.0
			theologian = 6
		}
		modifier = {
			factor = 0.9
			relation = { who = PAP value = 0 }
		}
		modifier = {
			factor = 0.8
			relation = { who = PAP value = 120 }
		}
		modifier = {
			factor = 0.9
			DIP = 8
		}
		modifier = {
			factor = 0.9
			NOT = { MIL = 5 }
		}
		modifier = {
			factor = 1.1
			MIL = 8
		}
		modifier = {
			factor = 2.5
			has_country_flag = nonunionist
		}
	}


	title = "EVTNAME2555"
	desc = "EVTDESC2555"

	option = {
		name = "EVTOPTA2555"
		ai_chance = { factor = 10 }
		religion = graecocatholic
		capital_scope = { 
			religion = graecocatholic
			create_revolt = 2
		}
		stability = -3
		quality_quantity = -1
		set_country_flag = unionist
	}
	option = {
		name = "EVTOPTB2555"
		ai_chance = { factor = 35 }
		religion = graecocatholic
		stability = -2
		quality_quantity = -1
		set_country_flag = unionist
	}
	option = {
		name = "EVTOPTC2555"
		ai_chance = { factor = 55 }
		prestige = 0.02
		set_country_flag = nonunionist
	}
}

country_event = {

	id = 2556

	trigger = {
		religion = orthodox
		NOT = { num_of_cities = 5 }
		NOT = { prestige = 10 }
		DIP = 5
		NOT = { has_country_flag = unionist }
		is_religion_enabled = graecocatholic
		NOT = { defender_of_faith = yes }
	}

	mean_time_to_happen = {
		months = 2000

		modifier = {
			factor = 0.4
			war = yes
		}
		modifier = {
			factor = 0.8
			any_neighbor_country = {
				NOT = { religion = orthodox }
			}
		}
		modifier = {
			factor = 0.8
			NOT = { num_of_cities = 2 }
		}
		modifier = {
			factor = 1.2
			num_of_cities = 4
		}
		modifier = {
			factor = 1.2
			secularism_theocracy = 4
		}
		modifier = {
			factor = 1.5
			secularism_theocracy = 5
		}
		modifier = {
			factor = 0.9
			NOT = { secularism_theocracy = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { secularism_theocracy = -2 }
		}
		modifier = {
			factor = 0.8
			NOT = { secularism_theocracy = -4 }
		}
		modifier = {
			factor = 1.1
			theologian = 1
		}
		modifier = {
			factor = 1.2
			theologian = 3
		}
		modifier = {
			factor = 1.5
			theologian = 5
		}
		modifier = {
			factor = 2.0
			theologian = 6
		}
		modifier = {
			factor = 0.9
			relation = { who = PAP value = 0 }
		}
		modifier = {
			factor = 0.8
			relation = { who = PAP value = 120 }
		}
		modifier = {
			factor = 0.9
			DIP = 8
		}
		modifier = {
			factor = 0.9
			NOT = { MIL = 5 }
		}
		modifier = {
			factor = 1.1
			MIL = 8
		}
		modifier = {
			factor = 2.5
			has_country_flag = nonunionist
		}
	}


	title = "EVTNAME2556"
	desc = "EVTDESC2556"

	option = {
		name = "EVTOPTA2556"
		ai_chance = { factor = 10 }
		religion = graecocatholic
		capital_scope = { 
			religion = graecocatholic
			create_revolt = 2
		}
		stability = -3
		quality_quantity = -1
		set_country_flag = unionist
	}
	option = {
		name = "EVTOPTB2556"
		ai_chance = { factor = 35 }
		religion = graecocatholic
		stability = -2
		quality_quantity = -1
		set_country_flag = unionist
	}
	option = {
		name = "EVTOPTC2556"
		ai_chance = { factor = 55 }
		prestige = 0.02
		set_country_flag = nonunionist
	}
}

country_event = {

	id = 2557

	trigger = {
		religion = greek_orthodox
		NOT = { num_of_cities = 5 }
		NOT = { prestige = 10 }
		DIP = 5
		NOT = { has_country_flag = unionist }
		is_religion_enabled = graecocatholic
		NOT = { defender_of_faith = yes }
	}

	mean_time_to_happen = {
		months = 2000

		modifier = {
			factor = 0.4
			war = yes
		}
		modifier = {
			factor = 0.8
			any_neighbor_country = {
				NOT = { religion = greek_orthodox }
			}
		}
		modifier = {
			factor = 0.8
			NOT = { num_of_cities = 2 }
		}
		modifier = {
			factor = 1.2
			num_of_cities = 4
		}
		modifier = {
			factor = 1.2
			secularism_theocracy = 4
		}
		modifier = {
			factor = 1.5
			secularism_theocracy = 5
		}
		modifier = {
			factor = 0.9
			NOT = { secularism_theocracy = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { secularism_theocracy = -2 }
		}
		modifier = {
			factor = 0.8
			NOT = { secularism_theocracy = -4 }
		}
		modifier = {
			factor = 1.1
			theologian = 1
		}
		modifier = {
			factor = 1.2
			theologian = 3
		}
		modifier = {
			factor = 1.5
			theologian = 5
		}
		modifier = {
			factor = 2.0
			theologian = 6
		}
		modifier = {
			factor = 0.9
			relation = { who = PAP value = 0 }
		}
		modifier = {
			factor = 0.8
			relation = { who = PAP value = 120 }
		}
		modifier = {
			factor = 0.9
			DIP = 8
		}
		modifier = {
			factor = 0.9
			NOT = { MIL = 5 }
		}
		modifier = {
			factor = 1.1
			MIL = 8
		}
		modifier = {
			factor = 2.5
			has_country_flag = nonunionist
		}
	}


	title = "EVTNAME2557"
	desc = "EVTDESC2557"

	option = {
		name = "EVTOPTA2557"
		ai_chance = { factor = 10 }
		religion = graecocatholic
		capital_scope = { 
			religion = graecocatholic
			create_revolt = 2
		}
		stability = -3
		quality_quantity = -1
		set_country_flag = unionist
	}
	option = {
		name = "EVTOPTB2557"
		ai_chance = { factor = 35 }
		religion = graecocatholic
		stability = -2
		quality_quantity = -1
		set_country_flag = unionist
	}
	option = {
		name = "EVTOPTC2557"
		ai_chance = { factor = 55 }
		prestige = 0.02
		set_country_flag = nonunionist
	}
}

province_event = {

	id = 2558
	
	trigger = {
		OR = {
			religion = orthodox
			religion = greek_orthodox
		}
		is_religion_enabled = graecocatholic
		owner = { 
			advisor = theologian
			religion = catholic
			secularism_theocracy = -1
			has_country_flag = union_of_churches
		}
		year = 1570

	}
	
	mean_time_to_happen = {
		months = 480


		modifier = {
			factor = 0.8
			owner = { defender_of_faith = yes }
		}
		modifier = {
			factor = 2.0
			owner = { NOT = { secularism_theocracy = 0 } }
		}
		modifier = {
			factor = 1.2
			owner = { NOT = { secularism_theocracy = 1 } }
		}
		modifier = {
			factor = 0.9
			owner = { secularism_theocracy = 2 }
		}
		modifier = {
			factor = 0.9
			owner = { secularism_theocracy = 3 }
		}
		modifier = {
			factor = 0.8
			owner = { secularism_theocracy = 4 }
		}
		modifier = {
			factor = 0.6
			owner = { secularism_theocracy = 5 }
		}
		modifier = {
			factor = 0.9
			owner = { idea = deus_vult }
		}
		modifier = {
			factor = 0.9
			owner = { idea = divine_supremacy }
		}
		modifier = {
			factor = 2.0
			owner = { idea = humanist_tolerance }
		}
		modifier = {
			factor = 0.95
			owner = { relation = { who = PAP value = 50 } }
		}
		modifier = {
			factor = 0.9
			owner = { relation = { who = PAP value = 100 } }
		}
		modifier = {
			factor = 0.85
			owner = { relation = { who = PAP value = 150 } }
		}		
		modifier = {
			factor = 0.7
			owner = { government = theocratic_government }
		}		
	}
	
	title = "EVTNAME2558"
	desc = "EVTDESC2558"
	
	option = {
		name = "EVTOPTA2558"
		ai_chance = { factor = 35 }
		religion = graecocatholic
		create_revolt = 2
	}
	option = {
		name = "EVTOPTB2558"
		ai_chance = { factor = 25 }
		religion = graecocatholic
		owner = { treasury = -45 }
	}
	option = {
		name = "EVTOPTC2558"
		ai_chance = { factor = 40 }
		owner = { 
			prestige = -0.01
		}
	}
}

country_event = {

	id = 2559
	
	trigger = {
		NOT = { is_religion_enabled = wahhabism }
		advisor = theologian
		OR = { 
			idea = divine_supremacy 
		 	idea = deus_vult 
		}
		religion = sunni
		year = 1710
		secularism_theocracy = 0
	}
	
	mean_time_to_happen = {
		months = 60

		modifier = {
			factor = 0.9
			secularism_theocracy = 2
		}
		modifier = {
			factor = 0.9
			secularism_theocracy = 3
		}
		modifier = {
			factor = 0.9
			secularism_theocracy = 4
		}
		modifier = {
			factor = 2.0
			idea = humanist_tolerance
		}
		modifier = {
			factor = 0.7
			defender_of_faith = yes
		}
		modifier = {
			factor = 0.8
			government = confessional_state
		}		
	}
	
	title = "EVTNAME2559"
	desc = "EVTDESC2559"
	
	option = {
		name = "EVTOPTA2559"
		enable_religion = wahhabism
		capital_scope = { 
			religion = wahhabism 
		}
	}
}

province_event = {

	id = 2560
	
	trigger = {
		is_religion_enabled = wahhabism
		religion = sunni
		owner = { 
			OR = {
				religion = sunni 
				religion = shiite 
				religion = sufism
			}
		}
	}

	mean_time_to_happen = {
		months = 2400


		modifier = {
			factor = 0.8
			owner = { defender_of_faith = yes }
		}
		modifier = {
			factor = 2.0
			owner = { religion = sufism }
		}
		modifier = {
			factor = 0.9
			owner = { idea = deus_vult }
		}
		modifier = {
			factor = 0.9
			owner = { idea = divine_supremacy }
		}
		modifier = {
			factor = 1.6
			owner = { idea = humanist_tolerance }
		}
		modifier = {
			factor = 0.9
			owner = { secularism_theocracy = 2 }
		}
		modifier = {
			factor = 0.9
			owner = { secularism_theocracy = 3 }
		}
		modifier = {
			factor = 0.9
			owner = { secularism_theocracy = 4 }
		}
		modifier = {
			factor = 0.9
			owner = { secularism_theocracy = 5 }
		}
		modifier = {
			factor = 1.1
			owner = { NOT = { secularism_theocracy = 0 } }
		}
		modifier = {
			factor = 1.5
			owner = { NOT = { secularism_theocracy = -2 } }
		}
		modifier = {
			factor = 0.9
			owner = { government = confessional_state }
		}
		modifier = {
			factor = 0.4
			any_neighbor_province = { religion = wahhabism }
		}
		modifier = {
			factor = 0.7
			NOT = { religion_years = { wahhabism = 12 } }
		}
		modifier = {
			factor = 1.5
			religion_years = { wahhabism = 20 }
		}
		modifier = {
			factor = 3.0
			religion_years = { wahhabism = 30 }
		}
		modifier = {
			factor = 5.0
			religion_years = { wahhabism = 50 }
		}

	}

	title = "EVTNAME2560"
	desc = "EVTDESC2560"
	
	option = {
		name = "EVTOPTA2560"
		ai_chance = { factor = 20 }
		religion = wahhabism
		owner = { 
			secularism_theocracy = 1 
		}
	}
	option = {
		name = "EVTOPTB2560"
		ai_chance = { factor = 35 }
		religion = wahhabism
	}
	option = {
		name = "EVTOPTC2560"
		ai_chance = { factor = 30 }
		create_revolt = 1
		owner = { 
			stability = -2 
		}
	}
	option = {
		name = "EVTOPTD2560"
		ai_chance = { factor = 15 }
		create_revolt = 1
		owner = { 
			secularism_theocracy = -1
			stability = -2 
		}
	}
}

province_event = {

	id = 2561
	
	trigger = {
		is_religion_enabled = wahhabism
		religion = sunni
		owner = { religion = wahhabism }
	}

	mean_time_to_happen = {
		months = 1200


		modifier = {
			factor = 0.8
			owner = { defender_of_faith = yes }
		}
		modifier = {
			factor = 0.9
			owner = { idea = deus_vult }
		}
		modifier = {
			factor = 0.9
			owner = { idea = divine_supremacy }
		}
		modifier = {
			factor = 1.6
			owner = { idea = humanist_tolerance }
		}
		modifier = {
			factor = 0.9
			owner = { secularism_theocracy = 2 }
		}
		modifier = {
			factor = 0.9
			owner = { secularism_theocracy = 3 }
		}
		modifier = {
			factor = 0.9
			owner = { secularism_theocracy = 4 }
		}
		modifier = {
			factor = 0.9
			owner = { secularism_theocracy = 5 }
		}
		modifier = {
			factor = 1.1
			owner = { NOT = { secularism_theocracy = 0 } }
		}
		modifier = {
			factor = 1.5
			owner = { NOT = { secularism_theocracy = -2 } }
		}
		modifier = {
			factor = 0.9
			owner = { government = confessional_state }
		}
		modifier = {
			factor = 0.4
			any_neighbor_province = { religion = wahhabism }
		}
		modifier = {
			factor = 0.7
			NOT = { religion_years = { wahhabism = 10 } }
		}
		modifier = {
			factor = 1.5
			religion_years = { wahhabism = 15 }
		}
		modifier = {
			factor = 3.0
			religion_years = { wahhabism = 20 }
		}
		modifier = {
			factor = 5.0
			religion_years = { wahhabism = 30 }
		}
		modifier = {
			factor = 10.0
			religion_years = { wahhabism = 50 }
		}


	}

	title = "EVTNAME2561"
	desc = "EVTDESC2561"
	
	option = {
		name = "EVTOPTA2561"
		ai_chance = { factor = 30 }
		religion = wahhabism
		owner = { 
			secularism_theocracy = 1 
		}
	}
	option = {
		name = "EVTOPTB2561"
		ai_chance = { factor = 70 }
		religion = wahhabism
	}
}

province_event = {

	id = 2562
	
	trigger = {
		OR = {
			religion = shiite
			religion = sunni
		}
	}

	mean_time_to_happen = {
		months = 3600


		modifier = {
			factor = 1.25
			owner = { tag = TUR }
		}
		modifier = {
			factor = 1.5
			any_neighbor_province = { religion = sufism }
		}
		modifier = {
			factor = 2.0
			owner = { defender_of_faith = yes }
		}
		modifier = {
			factor = 0.8
			owner = { idea = humanist_tolerance }
		}
		modifier = {
			factor = 1.2
			owner = { 
				NOT = { religion = sufism }
				secularism_theocracy = 1 
			}
		}
		modifier = {
			factor = 1.5
			owner = { 
				NOT = { religion = sufism }
				secularism_theocracy = 2 
			}
		}
		modifier = {
			factor = 2.0
			owner = { 
				NOT = { religion = sufism }
				secularism_theocracy = 3 
			}
		}
		modifier = {
			factor = 2.5
			owner = { 
				NOT = { religion = sufism }
				secularism_theocracy = 4 
			}
		}
		modifier = {
			factor = 3.0
			owner = { 
				NOT = { religion = sufism }
				secularism_theocracy = 5 
			}
		}
		modifier = {
			factor = 0.9
			owner = { NOT = { secularism_theocracy = 0 } }
		}
		modifier = {
			factor = 0.8
			owner = { NOT = { secularism_theocracy = -2 } }
		}
		modifier = {
			factor = 2.0
			owner = {
				NOT = { religion = sufism }
				government = confessional_state 
			}
		}
		modifier = {
			factor = 0.8
			owner = {
				NOT = { num_of_religion = { religion = sufism value = 0.05 } }
			}
		}
		modifier = {
			factor = 2.0
			owner = {
				num_of_religion = { religion = sufism value = 0.15 }
			}
		}
		modifier = {
			factor = 3.0
			owner = {
				num_of_religion = { religion = sufism value = 0.2 }
			}
		}
		modifier = {
			factor = 5.0
			owner = {
				num_of_religion = { religion = sufism value = 0.3 }
			}
		}
		modifier = {
			factor = 2.0
			owner = {
				NOT = { num_of_cities = 2 }
			}
		}
		modifier = {
			factor = 1.5
			owner = {
				NOT = { num_of_cities = 3 }
			}
		}
		modifier = {
			factor = 1.1
			owner = {
				NOT = { num_of_cities = 5 }
			}
		}
	}

	title = "EVTNAME2562"
	desc = "EVTDESC2562"
	
	option = {
		name = "EVTOPTA2562"
		ai_chance = { factor = 15 }
		religion = sufism
		owner = { 
			random_owned = {
				limit = { 
					owner = {
						OR = { 
							religion = shiite
							religion = sunni
						}
					}
				}
				owner = {
					secularism_theocracy = -1 
					prestige = -0.02
				}
			}
		}
	}
	option = {
		name = "EVTOPTB2562"
		ai_chance = { factor = 25 }
		religion = sufism
		owner = { 
			random_owned = {
				limit = { 
					owner = {
						OR = { 
							religion = shiite
							religion = sunni
						}
					}
				}
				owner = {
					random = {
						chance = 50
						stability = -1 
					}
				}
			}
		}
	}
	option = {
		name = "EVTOPTC2562"
		ai_chance = { factor = 25 }
		religion = sufism
		owner = { 
			random_owned = {
				limit = { 
					owner = {
						OR = { 
							religion = shiite
							religion = sunni
						}
					}
				}
				owner = {
					secularism_theocracy = 1 
					stability = -1 
				}
			}
		}
	}
	option = {
		name = "EVTOPTD2562"
		ai_chance = { factor = 20 }
		create_revolt = 1
		owner = { 
			random_owned = {
				limit = { 
					owner = {
						OR = { 
							religion = shiite
							religion = sunni
						}
					}
				}
				owner = { 
					stability = -1 
				}
			}
		}
	}
	option = {
		name = "EVTOPTE2562"
		ai_chance = { factor = 15 }
		create_revolt = 1
		owner = { 
			random_owned = {
				limit = { 
					owner = {
						OR = { 
							religion = shiite
							religion = sunni
						}
					}
				}
				owner = {
					secularism_theocracy = 1 
					stability = -2 
				}
			}
		}
	}
}

country_event = {

	id = 2563
	
	trigger = {
		NOT = { is_religion_enabled = sikhism }
		OR = {
			culture_group = western_aryan
			culture_group = eastern_aryan
			culture_group = hindusthani
			culture_group = dravidian
		}
		year = 1520
	}
	
	mean_time_to_happen = {
		months = 120

		modifier = {
			factor = 0.9
			secularism_theocracy = 1
		}
		modifier = {
			factor = 0.9
			secularism_theocracy = 2
		}
		modifier = {
			factor = 0.8
			secularism_theocracy = 3
		}
		modifier = {
			factor = 0.8
			secularism_theocracy = 4
		}
		modifier = {
			factor = 0.7
			secularism_theocracy = 5
		}
		modifier = {
			factor = 1.2
			NOT = { secularism_theocracy = 0 }
		}
		modifier = {
			factor = 2.0
			NOT = { secularism_theocracy = -2 }
		}

	}


	title = "EVTNAME2563"
	desc = "EVTDESC2563"
	
	option = {
		name = "EVTOPTA2563"
		enable_religion = sikhism
		random_owned = { religion = sikhism }
	}
}

province_event = {

	id = 2564
	
	trigger = {
		any_neighbor_province = { religion = sikhism }
		OR = {
			culture_group = western_aryan
			culture_group = eastern_aryan
			culture_group = hindusthani
			culture_group = dravidian
		}
		OR = {
			religion = sunni		
			religion = shiite
			religion = sufism
			religion = jainism	
			religion = hinduism
		}
	}
	
	mean_time_to_happen = {
		months = 240

		modifier = {
			factor = 0.9
			owner = { secularism_theocracy = 1 }
		}
		modifier = {
			factor = 0.9
			owner = { secularism_theocracy = 2 }
		}
		modifier = {
			factor = 0.8
			owner = { secularism_theocracy = 3 }
		}
		modifier = {
			factor = 0.8
			owner = { secularism_theocracy = 4 }
		}
		modifier = {
			factor = 0.7
			owner = { secularism_theocracy = 5 }
		}
		modifier = {
			factor = 1.2
			owner = { NOT = { secularism_theocracy = 0 } }
		}
		modifier = {
			factor = 2.0
			owner = { NOT = { secularism_theocracy = -2 } }
		}
		modifier = {
			factor = 1.5
			religion_years = { sikhism = 5 }
		}
		modifier = {
			factor = 2.0
			religion_years = { sikhism = 10 }
		}
		modifier = {
			factor = 5.0
			religion_years = { sikhism = 20 }
		}
		modifier = {
			factor = 10.0
			religion_years = { sikhism = 30 }
		}
		modifier = {
			factor = 20.0
			religion_years = { sikhism = 40 }
		}
	}


	title = "EVTNAME2564"
	desc = "EVTDESC2564"
	
	option = {
		name = "EVTOPTA2564"
		religion = sikhism
	}
}

country_event = {

	id = 2565
	
	trigger = {
		religion = catholic
		has_global_flag = byzantine_conversion
		not = { has_country_flag = byzantium_converted }
	}
	
	mean_time_to_happen = {
		months = 6
	}
	
	title = "EVTNAME2565"
	desc = "EVTDESC2565"
	
	option = {
		name = "EVTOPTA2565"
		ai_chance = { factor = 75 }
		relation = { who = BYZ value = 100 }
		set_country_flag = byzantium_converted 

	}
	option = {
		name = "EVTOPTB2565"
		ai_chance = { factor = 25 }
		relation = { who = BYZ value = -25 }
		set_country_flag = byzantium_converted 
	}
}

country_event = {

	id = 2566

	trigger = {
		religion_group = christian
		has_country_flag = bible_translated
		NOT = { has_country_modifier = bible_translation }
		NOT = { has_country_flag = bible_translation_done }
	}
	mean_time_to_happen = {
		months = 60
		
		modifier = {
			factor = 0.75
			stability = 3
		}
		modifier = {
			factor = 2
			war = yes
		}
	}

	title = "EVTNAME2566"
	desc = "EVTDESC2566"

	option = {
		name = "EVTOPTA2566"
		stability = 1
		add_country_modifier = {
			name = "bible_translation_profits"
			duration = 7300
		}
		secularism_theocracy = 1
		prestige = 0.05
		set_country_flag = bible_translation_done
	}
}