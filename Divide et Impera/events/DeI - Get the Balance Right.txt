# Event emulating historical fenomens and curbing some ahistorical problems


province_event = {

	id = 666109
	
	trigger = {
		OR = {
			province_id = 279 
		    province_id = 280 
		    province_id = 281 
		    province_id = 290 
		    province_id = 291 
		    province_id = 261 
			province_id = 1320 
			province_id = 289
			province_id = 2166
			province_id = 2167
			province_id = 2168 
		}
	}
	
	mean_time_to_happen = {
		months = 666

                modifier = {
			factor = 10.0
			NOT = { year = 1400 }
		}
                modifier = {
			factor = 6.0
			NOT = { year = 1500 }
		}
                modifier = {
			factor = 1.25
			NOT = { culture = ukrainian } 
		}
                modifier = {
			factor = 1.1
			year = 1650
		}
                modifier = {
			factor = 1.15
			year = 1700
		}
                modifier = {
			factor = 1.2
			year = 1750
		}
                modifier = {
			factor = 1.25
			year = 1800
		}
                modifier = {
			factor = 1.05
			citysize = 5000
		}
                modifier = {
			factor = 1.1
			citysize = 10000
		}
                modifier = {
			factor = 1.2
			citysize = 20000
		}
                modifier = {
			factor = 1.5
			citysize = 50000
		}
		modifier = {
			factor = 1.5 
			NOT = { is_core = THIS }
		}

		modifier = {
			factor = 1.1 
			owner = { NOT = { ADM = 4 } }
		}						
		modifier = {
			factor = 0.95
			owner = { ADM = 4 }
		}			
		modifier = {
			factor = 0.98
		 	owner = { MIL = 4 }
		}
                modifier = {
			factor = 0.95
			owner = { ADM = 5 }
		}			
		modifier = {
			factor = 0.98
		 	owner = { MIL = 5 }
		}
                modifier = {
			factor = 0.95
			owner = { ADM = 6 }
		}			
		modifier = {
			factor = 0.98
		 	owner = { MIL = 6 }
		}
                modifier = {
			factor = 0.95
			owner = { ADM = 7 }
		}			
		modifier = {
			factor = 0.98
		 	owner = { MIL = 7 }
		}
                modifier = {
			factor = 0.95
			owner = { ADM = 8 }
		}			
		modifier = {
			factor = 0.98
		 	owner = { MIL = 8 }
		}
                modifier = {
			factor = 0.95
			owner = { ADM = 9 }
		}			
		modifier = {
			factor = 0.98
		 	owner = { MIL = 9 }
		}
		modifier = {
			factor = 0.95
			owner = { revolt_percentage = 0.1 }
		}
		modifier = {
			factor = 0.95
			owner = { revolt_percentage = 0.3 }
		}
		modifier = {
			factor = 0.95
			owner = { revolt_percentage = 0.5 }
		}
		modifier = {
			factor = 0.9
			owner = {
				num_of_cities = 15
			}
		}
		modifier = {
			factor = 0.95
			owner = { stability = 1 }
		}
		modifier = {
			factor = 0.95
			owner = { stability = 2 }
		}
		modifier = {
			factor = 0.95
			owner = { stability = 3 }
		}
             modifier = {
			factor = 0.95
			culture = ukrainian  
		}

                
	}
	
	title = "EVTNAME666109"
	desc = "EVTDESC666109"
	
	option = {
		name = "EVTOPTA666109"
		ai_chance = { factor = 20 }
                random_list = {
                45 = { citysize = +1000 }
                25 = { citysize = +1500 }
                10 = { citysize = +1500
		       change_manpower = 1 }
                10 = { citysize = +1000 noble_rebels = 1 }
                10 = { citysize = +1000 anti_tax_rebels = 1 }
                }
                owner = { production_tech = 0.01 }
		
	}
	option = {
		name = "EVTOPTB666109"
		ai_chance = { factor = 45 }
                random_list = {
		    35 = { citysize = +2000 }
                35 = { citysize = +2500 }
                15 = { citysize = +1500
		       anti_tax_rebels = 2 }
                5 = { citysize = +1500 
                      cavalry = THIS }
                }
                owner = { production_tech = 0.03
                trade_tech = -0.01
                government_tech = -0.015
	          treasury = +35 }
		
	}
        option = {
		name = "EVTOPTC666109"
		ai_chance = { factor = 35 }
                change_manpower = +1
                random_list = {
		38 = { citysize = +1000 }
                40 = { citysize = +1500 
                       change_manpower = 1 }
                15 = { citysize = +1000
		       noble_rebels = 2 }
                7 = { citysize = +1500 
                       infantry = THIS }
                }               
               owner = { 
                production_tech = 0.01
                government_tech = 0.01
                treasury = -25 }
		
	}
}

country_event = {

	id = 666115

	trigger = {
		ai = yes
		year = 1556
		government = military_order
		NOT = { religion = catholic }
		OR = { 
			religion_group = christian
			religion_group = eastern_christian
		}
	}

	mean_time_to_happen = {
		months = 666
	}

	title = "EVTNAME666115"
	desc = "EVTDESC666115"

	option = {
		name = "EVTOPTA666115"
		ai_chance = { factor = 50 }
		secularism_theocracy = 1
		quality_quantity = -1
		years_of_income = +0.15
		religion = catholic
		kill_ruler = THIS
		country_event = 8301
		stability = -2
	}
	option = {
		name = "EVTOPTB666115"
		ai_chance = { factor = 20 }
		aristocracy_plutocracy = -1
		serfdom_freesubjects = -1
		government = administrative_monarchy
		stability = -1
	}
	option = {
		name = "EVTOPTC666115"
		ai_chance = { factor = 15 }
		innovative_narrowminded = 1
		centralization_decentralization = -1
		government = despotic_monarchy
		stability = -1
	}
	option = {
		name = "EVTOPTD666115"
		ai_chance = { factor = 15 }
		aristocracy_plutocracy = 1
		mercantilism_freetrade = 1
		government = merchant_republic
		stability = -1
	}
	
}
country_event = {

	id = 666116

	trigger = {
		ai = yes
		year = 1556
		government = military_order
		NOT = { 
			religion_group = christian
			religion_group = eastern_christian
		}
	}

	mean_time_to_happen = {
		months = 66
	}

	title = "EVTNAME666116"
	desc = "EVTDESC666116"

	
	option = {
		name = "EVTOPTA666115"
		ai_chance = { factor = 55 }
		secularism_theocracy = 1
		quality_quantity = -1
		years_of_income = +0.15
		religion = catholic
		stability = -4
		kill_ruler = THIS
		country_event = 8301		
	}
	option = {
		name = "EVTOPTB666116"
		ai_chance = { factor = 15 }
		aristocracy_plutocracy = -1
		centralization_decentralization = 1
		government = oligarchical_monarchy
		stability = -1
	}
	option = {
		name = "EVTOPTC666116"
		ai_chance = { factor = 15 }
		serfdom_freesubjects = -1
		centralization_decentralization = -1
		government = eastern_despotism
		stability = -1
	}
	option = {
		name = "EVTOPTD666116"
		ai_chance = { factor = 15 }
		secularism_theocracy = 1
		innovative_narrowminded = 1
		government = confessional_state
		stability = -1
	}
	
}
country_event = {

	id = 666117

	trigger = {
		OR = {
			tag = KNI
			tag = LIV
			tag = ALC
			tag = TEU
		}
		OR = {
			NOT = { government = military_order }
			NOT = { religion = catholic }
		}
		ai = yes
		NOT = { year = 1490 }
	}

	mean_time_to_happen = {
		months = 60
	}

	title = "EVTNAME666116"
	desc = "EVTDESC666116"

	
	option = {
		name = "EVTOPTA666115"
		ai_chance = { factor = 100 }
		secularism_theocracy = 1
		quality_quantity = -1
		years_of_income = +0.75
		government = military_order
		religion = catholic
		stability = +1
		define_ruler = { ADM = 3 DIP = 3 MIL = 3 }
		country_event = 8301
		capital_scope = { cavalry = THIS infantry = this }
	}
	
}
country_event = {

	id = 666211

	trigger = {
		tag = TUR
		NOT = { year = 1556 }
		ai = yes
	}

	mean_time_to_happen = {
		months = 240
		modifier = {
			factor = 0.1
			regency = yes
		}
		modifier = {
			factor = 1.4
			ADM = 8
		}
		modifier = {
			factor = 1.4
			DIP = 8
		}
		modifier = {
			factor = 1.4
			MIL = 8
		}
		modifier = {
			factor = 1.5
			ADM = 9
		}
		modifier = {
			factor = 1.5
			DIP = 9
		}
		modifier = {
			factor = 1.5
			MIL = 9
		}
		modifier = {
			factor = 0.85
			NOT = { ADM = 6 }
		}
		modifier = {
			factor = 0.85
			NOT = { DIP = 6 }
		}
		modifier = {
			factor = 0.85
			NOT = { MIL = 6 }
		}
		modifier = {
			factor = 0.8
			NOT = { ADM = 4 }
		}
		modifier = {
			factor = 0.8
			NOT = { DIP = 4 }
		}
		modifier = {
			factor = 0.8
			NOT = { MIL = 4 }
		}
		modifier = {
			factor = 0.75
			war = yes
		}
	}

	title = "Great Sultan"
	desc = "Dummy"

	option = {
		name = "Certainly..."
		ai_chance = { factor = 100 }
		stability = +1
		manpower = +5
		prestige = 0.1
		treasury = 100
		create_general = 40
		create_general = 20
		create_general = 10
		badboy = -3
		inflation = -3.0
		production_tech = 0.15
		land_tech = 0.15
		naval_tech = 0.15
		trade_tech = 0.15
		government_tech = 0.15
		capital_scope = { 
			multiply_citysize = 1.05
			infantry = THIS
			cavalry = THIS
		}
		random_list = {
			25 = { centralization_decentralization = -1 }
			25 = { innovative_narrowminded = -1 }
			25 = { offensive_defensive = -1 }
			25 = { quality_quantity = -1 }
		}
		random_list = {
			10 = { define_ruler = { ADM = 4 DIP = 3 MIL = 2 } }
			10 = { define_ruler = { ADM = 4 MIL = 5  } }
			10 = { define_ruler = { ADM = 4 MIL = 5 } }
			10 = { define_ruler = { ADM = 3 MIL = 3 DIP = 3 } }
			10 = { define_ruler = { MIL = 5 DIP = 2 } }
			10 = { define_ruler = { MIL = 4 DIP = 4 ADM = 5 } }
			10 = { define_ruler = { MIL = 5 DIP = 1 ADM = 1 } }
			10 = { define_ruler = { MIL = 3 DIP = 1 ADM = 3 } }
			20 = { define_ruler = { MIL = 4 DIP = 2 ADM = 2 } }
		}
		country_event = 8301
	}

}
country_event = {

	id = 666212

	trigger = {
		ai = yes
		government = merchant_republic
		NOT = {
			aristocracy_plutocracy = -1
			serfdom_freesubjects = 1
		}
		num_of_cities = 4
	}

	mean_time_to_happen = {
		months = 36
		modifier = {
			factor = 50
			tag = VEN
		}
		modifier = {
			factor = 25
			tag = GEN
		}
		modifier = {
			factor = 1.5
			ADM = 8
		}
		modifier = {
			factor = 1.4
			DIP = 8
		}
		modifier = {
			factor = 1.3
			MIL = 8
		}
		modifier = {
			factor = 0.5
			NOT = { ADM = 4 }
		}
		modifier = {
			factor = 0.6
			NOT = { DIP = 4 }
		}
		modifier = {
			factor = 0.7
			NOT = { MIL = 4 }
		}
		modifier = {
			factor = 0.666
			war = yes
		}
}

	title = "Aristocratic leader opposes merchant republic"
	desc = "Dummy"

	option = {
		name = "Certainly..., one shall rule!"
		ai_chance = { factor = 70 }
		government = feudal_monarchy
		stability = -1
		prestige = +0.02
		treasury = +15
		manpower = +2
		land_tech = +0.03
		aristocracy_plutocracy = -1
		serfdom_freesubjects = -1
		random_list = {
			10 = { define_ruler = { ADM = 4 DIP = 3 MIL = 2 } }
			10 = { define_ruler = { ADM = 4 MIL = 5 } }
			10 = { define_ruler = { ADM = 4 MIL = 5 } }
			10 = { define_ruler = { ADM = 3 MIL = 3 DIP = 3 } }
			10 = { define_ruler = { MIL = 5 DIP = 2 } }
			10 = { define_ruler = { MIL = 4 DIP = 4 ADM = 5 } }
			10 = { define_ruler = { MIL = 5 DIP = 1 ADM = 1 } }
			10 = { define_ruler = { MIL = 3 DIP = 1 ADM = 3 } }
			20 = { define_ruler = { MIL = 4 DIP = 2 ADM = 2 } }
		}
		country_event = 8301
	}
	option = {
		name = "Fight him for freedom!"
		ai_chance = { factor = 30 }
		random_list = {
			10 = { stability = -4 }
			20 = { stability = -3 }
			30 = { stability = -2 }
			40 = { stability = -1 }
		}
		aristocracy_plutocracy = 1
		serfdom_freesubjects = 1
		merchants = 2
		trade_tech = 0.01
		production_tech = 0.01
		government_tech = 0.01
	}
}
country_event = {

	id = 666215

	trigger = {
		ai = yes
		government = tribal_democracy
		NOT = {
			aristocracy_plutocracy = -1
			serfdom_freesubjects = 1 
		}
		num_of_cities = 4
	}

	mean_time_to_happen = {
		months = 36
		modifier = {
			factor = 1.5
			ADM = 8
		}
		modifier = {
			factor = 1.4
			DIP = 8
		}
		modifier = {
			factor = 1.3
			MIL = 8
		}
		modifier = {
			factor = 0.5
			NOT = { ADM = 4 }
		}
		modifier = {
			factor = 0.6
			NOT = { DIP = 4 }
		}
		modifier = {
			factor = 0.7
			NOT = { MIL = 4 }
		}
		modifier = {
			factor = 0.666
			war = yes
		}
	}

	title = "Aristocratic leader opposes tribal democracy"
	desc = "Dummy"

	option = {
		name = "Certainly..."
		ai_chance = { factor = 80 }
		government = tribal_despotism
		stability = -2
		prestige = +0.02
		treasury = +15
		manpower = +2
		centralization_decentralization = -1
		aristocracy_plutocracy = -1
		serfdom_freesubjects = -1
		land_tech = 0.02
		random_list = {
			10 = { define_ruler = { ADM = 4 DIP = 3 MIL = 2  } }
			10 = { define_ruler = { ADM = 4 MIL = 5  } }
			10 = { define_ruler = { ADM = 4 MIL = 5  } }
			10 = { define_ruler = { ADM = 3 MIL = 3 DIP = 3  } }
			10 = { define_ruler = { MIL = 5 DIP = 2  } }
			10 = { define_ruler = { MIL = 4 DIP = 4 ADM = 5  } }
			10 = { define_ruler = { MIL = 5 DIP = 1 ADM = 1  } }
			10 = { define_ruler = { MIL = 3 DIP = 1 ADM = 3  } }
			20 = { define_ruler = { MIL = 4 DIP = 2 ADM = 2  } }
		}
		country_event = 8301
	}
	option = {
		name = "Fight him!"
		ai_chance = { factor = 20 }
		random_list = {
			10 = { stability = -4 }
			20 = { stability = -3 }
			30 = { stability = -2 }
			40 = { stability = -1 }
		}
		aristocracy_plutocracy = 1
		serfdom_freesubjects = 1
		merchants = 2
		trade_tech = 0.01
		production_tech = 0.01
		government_tech = 0.01
	}
}
country_event = {

	id = 666218

	trigger = {
		TUR = { ai = yes }
	    ai = yes
		overlord = { tag = TUR }
		#is_subjest = no
		primary_culture = turkish
		NOT = { tag = AKK tag = QAR }
		OR = {
			alliance_with = TUR
			marriage_with = TUR
		}
		OR = {
			AND = {
				TUR = { num_of_cities = 8 }
				NOT = { num_of_cities = 4 } 
			}
			AND = {
				TUR = { num_of_cities = 6 }
				NOT = { num_of_cities = 2 }
			}
		}
		TUR = {
			OR = { ADM = 6 MIL = 6 DIP = 6 }
		}
	}

	mean_time_to_happen = {
		months = 66
	}

	title = "Ottoman political power"
	desc = "Dummy"

	option = {
		name = "Certainly..."
		TUR = {
			inherit = THIS
			prestige = 0.01
			treasury = 10
			create_general = 20
			badboy = -1
			production_tech = 0.01
			land_tech = 0.01
			naval_tech = 0.01
			trade_tech = 0.01
			government_tech = 0.01
			capital_scope = { 
				multiply_citysize = 1.03
			}
		}
	}
}


province_event = {

	id = 666222
	
	trigger = {
	
		NOT = { is_core = THIS }
		owner = {
			NOT = { religion = THIS }
			num_of_cities = 12
		}		
	}
	
	mean_time_to_happen = {
		months = 720
		
		modifier = {
			factor = 1.25
			owner = { idea = bill_of_rights }
		}
		modifier = {
			factor = 2
			owner = { idea = ecumenism }
		}
		modifier = {
			factor = 1.25
			owner = { idea = humanist_tolerance }
		}
		modifier = {
			factor = 1.5 
			owner = { religion_group = THIS }
		}
		modifier = {
			factor = 5
			owner = { primary_culture = THIS }
		}
		modifier = {
			factor = 2
			owner = { accepted_culture = THIS }
		}
		modifier = {
			factor = 1.2
			owner = { culture_group = THIS }
		}
		modifier = {
			factor = 0.95 
			owner = { NOT = { ADM = 4 } }
		}					
		modifier = {
			factor = 0.95 
		 	owner = { NOT = { MIL = 4 } }
		}		
		modifier = {
			factor = 1.05 
			owner = { ADM = 5 }
		}			
		modifier = {
			factor = 1.05
		 	owner = { MIL = 5 }
		}
        modifier = {
			factor = 1.1
			owner = { ADM = 6 }
		}			
		modifier = {
			factor = 1.1
		 	owner = { MIL = 6 }
		}
		modifier = {
			factor = 1.2
			owner = { ADM = 7 }
		}			
		modifier = {
			factor = 1.2
		 	owner = { MIL = 7 }
		}
		modifier = {
			factor = 1.3
			owner = { ADM = 8 }
		}			
		modifier = {
			factor = 1.3
		 	owner = { MIL = 8 }
		}
		modifier = {
			factor = 1.4
			owner = { ADM = 9 }
		}			
		modifier = {
			factor = 1.4
		 	owner = { MIL = 9 }
		}
		modifier = {
			factor = 0.95
			owner = { revolt_percentage = 0.1 }
		}
		modifier = {
			factor = 0.95
			owner = { revolt_percentage = 0.3 }
		}
		modifier = {
			factor = 0.95
			owner = { revolt_percentage = 0.5 }
		}
		modifier = {
			factor = 0.7
			owner = { regency = yes }
		}
		modifier = {
			factor = 0.85
			owner = { NOT = { stability = 0 } }
		}
		modifier = {
			factor = 0.8
			owner = { NOT = { stability = -1 } }
		}
		modifier = {
			factor = 0.75
			owner = { NOT = { stability = -2 } }
		}
		modifier = {
			factor = 1.1
			owner = { stability = 1 }
		}
		modifier = {
			factor = 1.25
			owner = { stability = 2 }
		}
		modifier = {
			factor = 1.5
			owner = { stability = 3 }
		}
	}
	
	title = "EVTNAME3051"
	desc = "EVTDESC3051"


    immediate = {
		religious_rebels = 1
		multiply_citysize = 0.97
		change_controller = REB 
	}
	
	option = {
		name = "EVTOPTA3051"
		ai_chance = { factor = 100 }
		any_neighbor_province = {
			limit = { 
				religion = THIS 
                owner = { 
					num_of_cities = 12 
					NOT = { religion = THIS }
				}
			}
			religious_rebels = 1
		}
	}	
}

province_event = {

	id = 666237
	
	trigger = {
	
		NOT = { is_core = THIS }
		owner = {
			NOT = { accepted_culture = THIS }
			NOT = { primary_culture = THIS }
			num_of_cities = 12
		}		
	}
	
	mean_time_to_happen = {
		months = 780
		modifier = {
			factor = 2
			owner = { idea = bill_of_rights }
		}
		modifier = {
			factor = 1.1
			owner = { idea = ecumenism }
		}
		modifier = {
			factor = 1.5
			owner = { idea = humanist_tolerance }
		}
		modifier = {
			factor = 1.2 
			owner = { religion_group = THIS }
		}
		modifier = {
			factor = 1.5
			owner = { religion = THIS }
		}
		modifier = {
			factor = 2
			owner = { culture_group = THIS }
		}
		modifier = {
			factor = 0.95 
			owner = { NOT = { ADM = 4 } }
		}					
		modifier = {
			factor = 0.95 
		 	owner = { NOT = { MIL = 4 } }
		}		
		modifier = {
			factor = 1.05 
			owner = { ADM = 5 }
		}			
		modifier = {
			factor = 1.05
		 	owner = { MIL = 5 }
		}
        modifier = {
			factor = 1.1
			owner = { ADM = 6 }
		}			
		modifier = {
			factor = 1.1
		 	owner = { MIL = 6 }
		}
		modifier = {
			factor = 1.2
			owner = { ADM = 7 }
		}			
		modifier = {
			factor = 1.2
		 	owner = { MIL = 7 }
		}
		modifier = {
			factor = 1.3
			owner = { ADM = 8 }
		}			
		modifier = {
			factor = 1.3
		 	owner = { MIL = 8 }
		}
		modifier = {
			factor = 1.4
			owner = { ADM = 9 }
		}			
		modifier = {
			factor = 1.4
		 	owner = { MIL = 9 }
		}
		modifier = {
			factor = 0.95
			owner = { revolt_percentage = 0.1 }
		}
		modifier = {
			factor = 0.95
			owner = { revolt_percentage = 0.3 }
		}
		modifier = {
			factor = 0.95
			owner = { revolt_percentage = 0.5 }
		}
		modifier = {
			factor = 0.7
			owner = { regency = yes }
		}
		modifier = {
			factor = 0.85
			owner = { NOT = { stability = 0 } }
		}
		modifier = {
			factor = 0.8
			owner = { NOT = { stability = -1 } }
		}
		modifier = {
			factor = 0.75
			owner = { NOT = { stability = -2 } }
		}
		modifier = {
			factor = 1.1
			owner = { stability = 1 }
		}
		modifier = {
			factor = 1.25
			owner = { stability = 2 }
		}
		modifier = {
			factor = 1.5
			owner = { stability = 3 }
		}
	}
	
	title = "EVTNAME3051"
	desc = "EVTDESC3051"


    immediate = {
		nationalist_rebels = 1
		patriot_rebels = 1
		multiply_citysize = 0.97
		change_controller = REB 
	}
	
	option = {
		name = "EVTOPTA3051"
		ai_chance = { factor = 100 }
		any_neighbor_province = {
			limit = { 
				culture = THIS 
                owner = { 
					num_of_cities = 12
					NOT = { primary_culture = THIS }
				}
			}
			patriot_rebels = 1
			nationalist_rebels = 1
		}
	}	
}
country_event = {

	id = 666238

	trigger = {
		ai = yes
		war = no
		OR = {
			government = tribal_despotism
			government = tribal_federation
			government = tribal_democracy
		}
		technology_group = new_world
		NOT = { ADM = 8 }
		NOT = { MIL = 8 }
		NOT = { DIP = 8 }
		NOT = {
			AND = {
				MIL = 6
				ADM = 6
				DIP = 6
			}
		}
		num_of_cities = 4
        can_create_vassals = yes
		NOT = { prestige = 0 }
	}

	mean_time_to_happen = {
		months = 6
	}

	title = "American empire collapses"
	desc = "Dummy"

	option = {
		name = "Certainly..."
		ai_chance = { factor = 100 }
		release_vassal = random
		release_vassal = random
		release_vassal = random
		prestige = 0.05
		treasury = 25
		badboy = -2.5
	}

}
country_event = {

	id = 666221

	trigger = {
		ai = yes
		OR = {
			tag = CRI
			tag = GOL
			tag = KAZ
			tag = QAS
			tag = AST
			tag = NOG
			tag = AKK
			tag = QAR
			tag = KZH
			tag = KHA
			tag = ORZ
			tag = KZI
			tag = SIB
			tag = ULZ
			tag = KLM
			tag = BUQ
			tag = HXJ
			tag = YNJ
			tag = JZJ
			tag = TUV
			tag = KSG
			tag = BRY
			tag = MCH
			tag = CHG
			tag = AQS
			tag = TRF
		}
		NOT = { government = steppe_horde }
		NOT = { government_tech = 10 }
	}

	mean_time_to_happen = {
		months = 6
	}

	title = "Horde"
	desc = "Dummy"

	option = {
		name = "Certainly..."
		ai_chance = { factor = 100 }
		government = steppe_horde
		define_ruler = { MIL = 5 }
		country_event = 8301
		land_tech = 0.15
		naval_tech = -0.15
	}

}

country_event = {

	id = 666236

	trigger = {		
		owns = 161
		ai = yes
		NOT = { religion_group = eastern_christian }
		NOT = { exists = WAL }
		war = no
	}

	mean_time_to_happen = {
		months = 6
	}

	title = "Danubian Vassals"
	desc = "Dummy"

	option = {
		name = "Certainly..."
		ai_chance = { factor = 100 }
		WAL = { add_core = 161 add_core = 160 }
		release_vassal = WAL
		prestige = 0.03
		treasury = +25
		badboy = -1
	}

}
country_event = {

	id = 666217

	trigger = {
		tag = MAM
	    ai = yes
		NOT = { year = 1520 }
	    exists = EGY
        EGY = { government = oligarchical_monarchy ai = yes }
	}

	mean_time_to_happen = {
		months = 6
	}

	title = "Mamluk Egypt"
	desc = "Dummy"

	option = {
		name = "Certainly..."
		government = oligarchical_monarchy
		inherit = EGY
		prestige = 0.1
		treasury = 50
		diplomats = -5  
		merchants = -5
		colonists = -5 
		missionaries = -5 
		spies = -5
		production_tech = -0.25
		land_tech = -0.25
		naval_tech = -0.25
		trade_tech = -0.25
		government_tech = -0.25
		stability = -1
	}
}
country_event = {

	id = 666210

	trigger = {
		
		owns = 268
		ai = yes
		NOT = { religion_group = eastern_christian }
		NOT = { exists = MOL }
		war = no
	}

	mean_time_to_happen = {
		months = 6
	}

	title = "Danubian Vassals"
	desc = "Dummy"

	option = {
		name = "Certainly..."
		ai_chance = { factor = 100 }
		MOL = { add_core = 162 add_core = 268 }
		release_vassal = MOL
		prestige = 0.03
		treasury = +25
		badboy = -1
	}

}
country_event = {

	id = 666209

	trigger = {
	    ai = yes
        OR = {
			primary_culture = serbian
            primary_culture = greek
			primary_culture = pontic
            primary_culture = bulgarian
            culture_group = romanian
        }
        religion = catholic
		NOT = { tag = KNI }
	}

	mean_time_to_happen = {
		months = 6
	}

	title = "Orthodoxy!"
	desc = "Dummy"

	option = {
		name = "Orthodoxy!"
		ai_chance = { factor = 75 }
		religion = greek_orthodox
		capital_scope = { religion = greek_orthodox }
	}
	option = {
		name = "Union!"
		ai_chance = { factor = 25 }
		religion = graecocatholic
		capital_scope = { religion = graecocatholic }
	}
}