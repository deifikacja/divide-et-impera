country_event = {

	id = 1355

	trigger = {
		year = 1356
		NOT = { year = 1453 }
		government = military_order
		NOT = { has_country_flag = knights_of_the_christ }
	}

	mean_time_to_happen = {	
		months = 12
	}

	title = "EVTNAME1355"
	desc = "EVTDESC1355"

	option = {
		name = "EVTOPTA1355"
		ai_chance = { factor = 35 }
		secularism_theocracy = 1
		stability = 1
		prestige = 0.1
		badboy = -2
		add_country_modifier = {
			name = "knights_of_the_christ"
			duration = 5975
			}
		set_country_flag = knights_of_the_christ
	}
	option = {
		name = "EVTOPTB1355"
		ai_chance = { factor = 35 }
		random_owned = {
			limit = {
				has_building = fort1
			}
			add_building = fort2
		}
		random_owned = {
			limit = {
				has_building = fort1
			}
			add_building = fort2
		}
		add_country_modifier = {
			name = "knights_of_the_christ"
			duration = 5975
		}
		set_country_flag = knights_of_the_christ
	}
	option = {
		name = "EVTOPTC1355"
		ai_chance = { factor = 30 }
		centralization_decentralization = -1
		years_of_income = 0.5
		add_country_modifier = {
			name = "knights_of_the_christ"
			duration = 5975
			}
		set_country_flag = knights_of_the_christ
	}
}

country_event = {

	id = 1356

	trigger = {
		year = 1380
		NOT = { year = 1466 }
		government = military_order
		NOT = { has_country_flag = knights_and_taxes1 }
		NOT = { has_country_flag = knights_and_taxes2 }
	}

	mean_time_to_happen = {	
		months = 12
	}

	title = "EVTNAME1356"
	desc = "EVTDESC1356"

	option = {
		name = "EVTOPTA1356"
		ai_chance = { factor = 35 }
		years_of_income = 1
		stability = -2
		random_owned = { anti_tax_rebels = 1 }			
		set_country_flag = knights_and_taxes1
	}
	option = {
		name = "EVTOPTB1356"
		ai_chance = { factor = 40 }
		add_country_modifier = {
			name = "knights_taxes"
			duration = 3650
			}
		set_country_flag = knights_and_taxes1
	}
	option = {
		name = "EVTOPTC1356"
		ai_chance = { factor = 25 }
		centralization_decentralization = 1
		stability = 1
		set_country_flag = knights_and_taxes2
	}
}

country_event = {

	id = 1357

	trigger = {
		year = 1415
		government = military_order
		has_country_flag = knights_and_taxes1
		NOT = { has_country_flag = knights_economic_crisis }
	}
	mean_time_to_happen = {	
		months = 36
	}

	title = "EVTNAME1357"
	desc = "EVTDESC1357"

	option = {
		name = "EVTOPTA1357"
		ai_chance = { factor = 65 }
		add_country_modifier = {
			name = "military_order_economic_crisis"
			duration = 7300
		}
		innovative_narrowminded = 1
		set_country_flag = knights_economic_crisis
	}
	option = {
		name = "EVTOPTB1357"
		ai_chance = { factor = 35 }
		add_country_modifier = {
			name = "military_order_economic_crisis"
			duration = 3650
		}
		stability = -2
		secularism_theocracy = -1
		centralization_decentralization = 1
		set_country_flag = knights_economic_crisis
	}
}

country_event = {

	id = 1358

	trigger = {
		year = 1415
		government = military_order
		has_country_flag = knights_and_taxes2
		NOT = { has_country_flag = knights_economic_crisis }
	}
	mean_time_to_happen = {	
		months = 36
	}

	title = "EVTNAME1357"
	desc = "EVTDESC1357"

	option = {
		name = "EVTOPTA1357"
		ai_chance = { factor = 65 }
		add_country_modifier = {
			name = "military_order_economic_crisis"
			duration = 3650
		}
		innovative_narrowminded = 1
		set_country_flag = knights_economic_crisis
	}
	option = {
		name = "EVTOPTB1357"
		ai_chance = { factor = 35 }
		add_country_modifier = {
			name = "military_order_economic_crisis"
			duration = 1825
		}
		stability = -1
		secularism_theocracy = -1
		centralization_decentralization = 1
		set_country_flag = knights_economic_crisis
	}
}

country_event = {

	id = 1359

	trigger = {
		year = 1450
		government = military_order
		NOT = { has_country_flag = knights_political_crisis }
	}
	mean_time_to_happen = {	
		months = 36
	}

	title = "EVTNAME1359"
	desc = "EVTDESC1359"

	option = {
		name = "EVTOPTA1359"
		ai_chance = { factor = 65 }
		add_country_modifier = {
			name = "military_order_political_crisis"
			duration = 7300
		}
		innovative_narrowminded = 1
		set_country_flag = knights_political_crisis
	}
	option = {
		name = "EVTOPTB1359"
		ai_chance = { factor = 35 }
		add_country_modifier = {
			name = "military_order_political_crisis"
			duration = 3650
		}
		stability = -3
		secularism_theocracy = -1
		set_country_flag = knights_political_crisis
	}
}