country_event = {

	id = 70701

	trigger = {
		government_tech = 3
	}

	mean_time_to_happen = {
		months = 3100

		modifier = {
			factor = 0.9
			idea = bureaucracy
		}
		modifier = {
			factor = 0.9
			NOT = { centralization_decentralization = -1 }
		}
		modifier = {
			factor = 1.5
			OR = {
				technology_group = african
				technology_group = new_world
				technology_group = pacific
			}
		}
		modifier = {
			factor = 1.25
			OR = {
				technology_group = sahel
				technology_group = ordu
			}
		}
		modifier = {
			factor = 1.5
			NOT = { num_of_cities = 2 }
		}
		modifier = {
			factor = 1.2
			NOT = { num_of_cities = 4 }
		}
		modifier = {
			factor = 1.1
			NOT = { num_of_cities = 6 }
		}
		modifier = {
			factor = 1.25
			NOT = { prestige = 0 }
		}
		modifier = {
			factor = 1.2
			NOT = { year = 1400 }
		}
		modifier = {
			factor = 1.1
			NOT = { year = 1500 }
		}
		modifier = {
			factor = 0.9
			year = 1600
		}
		modifier = {
			factor = 0.9
			year = 1700
		}
		modifier = {
			factor = 5
			has_country_flag = deibureaucracy_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_army_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_naval_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_scientist
		}
		modifier = {
			factor = 1.25
			has_country_flag = deimaster_of_merchant_guild
		}
		modifier = {
			factor = 1.25
			has_country_flag = deisaint
		}
		modifier = {
			factor = 1.25
			has_country_flag = deiforeign_merchant
		}
		modifier = {
			factor = 1.25
			has_country_flag = deileader_of_bourgeoisie
		}
		modifier = {
			factor = 1.25
			has_country_flag = deirenaissance_man
		}

	}

	title = "EVTNAME70701"
	desc = "EVTDESC70701"

	option = {
		name = "EVTOPTA70701"
		ai_chance = { factor = 25 }
		centralization_decentralization = -2
		random = {
			chance = 60
			random_owned = { noble_rebels = 1 }
     		}
		random = {
			chance = 30
			random_owned = { noble_rebels = 1 }
     		}
		stability = -3
		define_advisor = { type = bureaucracy_reformer skill = 6 } 
		set_country_flag = deibureaucracy_reformer
	}
	option = {
		name = "EVTOPTB70701"
		ai_chance = { factor = 25 }
		capital_scope = { base_tax = 1 }
		random_owned = { 
			limit = { is_capital = no }
			base_tax = 1
		}
		random_owned = {
			limit = { is_capital = no }
			base_tax = 1
		}
		random = {
			chance = 75
			random_owned = { anti_tax_rebels = 1 }
     		}
		random = {
			chance = 50
			random_owned = { anti_tax_rebels = 1 }
     		}
		stability = -2
		define_advisor = { type = bureaucracy_reformer skill = 6 } 
		set_country_flag = deibureaucracy_reformer
	}
	option = {
		name = "EVTOPTC70701"
		ai_chance = { factor = 40 }
		prestige = -0.03
		define_advisor = { type = bureaucracy_reformer skill = 6 } 
		set_country_flag = deibureaucracy_reformer
	}
	option = {
		name = "EVTOPTD70701"
		ai_chance = { factor = 10 }
		prestige = 0.01
		set_country_flag = deibureaucracy_reformer
	}

}

country_event = {

	id = 70702

	trigger = {
		land_tech = 3
	}

	mean_time_to_happen = {
		months = 3100

		modifier = {
			factor = 0.95
			idea = grand_army
		}
		modifier = {
			factor = 0.95
			idea = military_drill
		}
		modifier = {
			factor = 0.95
			idea = engineer_corps
		}
		modifier = {
			factor = 0.9
			NOT = { land_naval = -1 }
		}
		modifier = {
			factor = 1.5
			OR = {
				technology_group = african
				technology_group = new_world
				technology_group = pacific
			}
		}
		modifier = {
			factor = 1.25
			OR = {
				technology_group = sahel
				technology_group = ordu
			}
		}
		modifier = {
			factor = 1.5
			NOT = { num_of_cities = 2 }
		}
		modifier = {
			factor = 1.2
			NOT = { num_of_cities = 4 }
		}
		modifier = {
			factor = 1.1
			NOT = { num_of_cities = 6 }
		}
		modifier = {
			factor = 1.25
			NOT = { prestige = 0 }
		}
		modifier = {
			factor = 1.2
			NOT = { year = 1400 }
		}
		modifier = {
			factor = 1.1
			NOT = { year = 1500 }
		}
		modifier = {
			factor = 0.9
			year = 1600
		}
		modifier = {
			factor = 0.9
			year = 1700
		}
		modifier = {
			factor = 1.25
			has_country_flag = deibureaucracy_reformer
		}
		modifier = {
			factor = 5
			has_country_flag = deigreat_army_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_naval_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_scientist
		}
		modifier = {
			factor = 1.25
			has_country_flag = deimaster_of_merchant_guild
		}
		modifier = {
			factor = 1.25
			has_country_flag = deisaint
		}
		modifier = {
			factor = 1.25
			has_country_flag = deiforeign_merchant
		}
		modifier = {
			factor = 1.25
			has_country_flag = deileader_of_bourgeoisie
		}
		modifier = {
			factor = 1.25
			has_country_flag = deirenaissance_man
		}

	}

	title = "EVTNAME70702"
	desc = "EVTDESC70702"

	option = {
		name = "EVTOPTA70702"
		ai_chance = { factor = 15 }
		land_naval = -1
		quality_quantity = -1
		years_of_income = -0.35
		define_advisor = { type = great_army_reformer skill = 6 } 
		set_country_flag = deigreat_army_reformer
	}
	option = {
		name = "EVTOPTB70702"
		ai_chance = { factor = 20 }
		offensive_defensive = -1
		quality_quantity = -1
		years_of_income = -0.35
		define_advisor = { type = great_army_reformer skill = 6 } 
		set_country_flag = deigreat_army_reformer
	}
	option = {
		name = "EVTOPTC70702"
		ai_chance = { factor = 20 }
		random_owned = {
			limit = { has_building = fort2 }
			add_building = fort3
		}
		random_owned = {
			limit = { has_building = fort1 }
			add_building = fort2
		}		
		offensive_defensive = 1
		years_of_income = -0.35
		define_advisor = { type = great_army_reformer skill = 6 } 
		set_country_flag = deigreat_army_reformer
	}
	option = {
		name = "EVTOPTD70702"
		ai_chance = { factor = 35 }
		prestige = -0.03
		define_advisor = { type = great_army_reformer skill = 6 } 
		set_country_flag = deigreat_army_reformer
	}
	option = {
		name = "EVTOPTE70702"
		ai_chance = { factor = 10 }
		prestige = 0.01
		set_country_flag = deigreat_army_reformer
	}

}

country_event = {

	id = 70703

	trigger = {
		naval_tech = 3
		num_of_ports = 3
	}

	mean_time_to_happen = {
		months = 3100

		modifier = {
			factor = 0.9
			idea = grand_navy
		}
		modifier = {
			factor = 0.9
			idea = sea_hawks
		}
		modifier = {
			factor = 0.9
			idea = superior_seamanship
		}
		modifier = {
			factor = 0.9
			land_naval = 2
		}
		modifier = {
			factor = 1.1
			NOT = { num_of_ports = 5 }
		}
		modifier = {
			factor = 1.5
			OR = {
				technology_group = african
				technology_group = new_world
				technology_group = pacific
			}
		}
		modifier = {
			factor = 1.25
			OR = {
				technology_group = sahel
				technology_group = ordu
			}
		}
		modifier = {
			factor = 1.5
			NOT = { num_of_cities = 2 }
		}
		modifier = {
			factor = 1.2
			NOT = { num_of_cities = 4 }
		}
		modifier = {
			factor = 1.1
			NOT = { num_of_cities = 6 }
		}
		modifier = {
			factor = 1.25
			NOT = { prestige = 0 }
		}
		modifier = {
			factor = 1.2
			NOT = { year = 1400 }
		}
		modifier = {
			factor = 1.1
			NOT = { year = 1500 }
		}
		modifier = {
			factor = 0.9
			year = 1600
		}
		modifier = {
			factor = 0.9
			year = 1700
		}
		modifier = {
			factor = 1.25
			has_country_flag = deibureaucracy_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_army_reformer
		}
		modifier = {
			factor = 5
			has_country_flag = deigreat_naval_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_scientist
		}
		modifier = {
			factor = 1.25
			has_country_flag = deimaster_of_merchant_guild
		}
		modifier = {
			factor = 1.25
			has_country_flag = deisaint
		}
		modifier = {
			factor = 1.25
			has_country_flag = deiforeign_merchant
		}
		modifier = {
			factor = 1.25
			has_country_flag = deileader_of_bourgeoisie
		}
		modifier = {
			factor = 1.25
			has_country_flag = deirenaissance_man
		}

	}

	title = "EVTNAME70703"
	desc = "EVTDESC70703"

	option = {
		name = "EVTOPTA70703"
		ai_chance = { factor = 15 }
		land_naval = 2
		create_admiral = 50
		years_of_income = -0.2
		define_advisor = { type = great_naval_reformer skill = 6 } 
		set_country_flag = deigreat_naval_reformer
	}
	option = {
		name = "EVTOPTB70703"
		ai_chance = { factor = 20 }
		land_naval = 1
		random_owned = {
			limit = { 
				port = yes
				NOT = { has_building = shipyard } 
			}
			add_building = shipyard
		}
		years_of_income = -0.2
		define_advisor = { type = great_naval_reformer skill = 6 } 
		set_country_flag = deigreat_naval_reformer
	}
	option = {
		name = "EVTOPTC70703"
		ai_chance = { factor = 20 }
		colonists = 2
		create_explorer = 30
		years_of_income = -0.15
		define_advisor = { type = great_naval_reformer skill = 6 } 
		set_country_flag = deigreat_naval_reformer
	}
	option = {
		name = "EVTOPTD70703"
		ai_chance = { factor = 35 }
		prestige = -0.03
		define_advisor = { type = great_naval_reformer skill = 6 } 
		set_country_flag = deigreat_naval_reformer
	}
	option = {
		name = "EVTOPTE70703"
		ai_chance = { factor = 10 }
		prestige = 0.01
		set_country_flag = deigreat_naval_reformer
	}

}

country_event = {

	id = 70704

	trigger = {
		production_tech = 3
	}

	mean_time_to_happen = {
		months = 3100

		modifier = {
			factor = 0.85
			idea = scientific_revolution
		}
		modifier = {
			factor = 0.9
			NOT = { innovative_narrowminded = -1 }
		}
		modifier = {
			factor = 1.5
			OR = {
				technology_group = african
				technology_group = new_world
				technology_group = pacific
			}
		}
		modifier = {
			factor = 1.25
			OR = {
				technology_group = sahel
				technology_group = ordu
			}
		}
		modifier = {
			factor = 1.5
			NOT = { num_of_cities = 2 }
		}
		modifier = {
			factor = 1.2
			NOT = { num_of_cities = 4 }
		}
		modifier = {
			factor = 1.1
			NOT = { num_of_cities = 6 }
		}
		modifier = {
			factor = 1.25
			NOT = { prestige = 0 }
		}
		modifier = {
			factor = 1.2
			NOT = { year = 1400 }
		}
		modifier = {
			factor = 1.1
			NOT = { year = 1500 }
		}
		modifier = {
			factor = 0.9
			year = 1600
		}
		modifier = {
			factor = 0.9
			year = 1700
		}
		modifier = {
			factor = 1.25
			has_country_flag = deibureaucracy_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_army_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_naval_reformer
		}
		modifier = {
			factor = 5
			has_country_flag = deigreat_scientist
		}
		modifier = {
			factor = 1.25
			has_country_flag = deimaster_of_merchant_guild
		}
		modifier = {
			factor = 1.25
			has_country_flag = deisaint
		}
		modifier = {
			factor = 1.25
			has_country_flag = deiforeign_merchant
		}
		modifier = {
			factor = 1.25
			has_country_flag = deileader_of_bourgeoisie
		}
		modifier = {
			factor = 1.25
			has_country_flag = deirenaissance_man
		}

	}

	title = "EVTNAME70704"
	desc = "EVTDESC70704"

	option = {
		name = "EVTOPTA70704"
		ai_chance = { factor = 0 }
		innovative_narrowminded = -2
		secularism_theocracy = -1
		stability = -2
		years_of_income = -0.3
		define_advisor = { type = great_scientist skill = 6 } 
		set_country_flag = deigreat_scientist
	}
	option = {
		name = "EVTOPTB70704"
		ai_chance = { factor = 0 }
		innovative_narrowminded = -1
		years_of_income = -0.25
		define_advisor = { type = great_scientist skill = 6 } 
		set_country_flag = deigreat_scientist
	}
	option = {
		name = "EVTOPTC70704"
		ai_chance = { factor = 0 }
		secularism_theocracy = -1
		stability = -1
		define_advisor = { type = great_scientist skill = 6 } 
		set_country_flag = deigreat_scientist
	}
	option = {
		name = "EVTOPTD70704"
		ai_chance = { factor = 0 }
		prestige = -0.03
		define_advisor = { type = great_scientist skill = 6 } 
		set_country_flag = deigreat_scientist
	}
	option = {
		name = "EVTOPTE70704"
		ai_chance = { factor = 100 }
		prestige = 0.01
		set_country_flag = deigreat_scientist
	}

}

country_event = {

	id = 70705

	trigger = {
		trade_tech = 3
		num_of_cots = 1
	}

	mean_time_to_happen = {
		months = 3100

		modifier = {
			factor = 0.9
			idea = national_trade_policy
		}
		modifier = {
			factor = 0.9
			num_of_cots = 1
		}
		modifier = {
			factor = 1.5
			OR = {
				technology_group = african
				technology_group = new_world
				technology_group = pacific
			}
		}
		modifier = {
			factor = 1.25
			OR = {
				technology_group = sahel
				technology_group = ordu
			}
		}
		modifier = {
			factor = 1.5
			NOT = { num_of_cities = 2 }
		}
		modifier = {
			factor = 1.2
			NOT = { num_of_cities = 4 }
		}
		modifier = {
			factor = 1.1
			NOT = { num_of_cities = 6 }
		}
		modifier = {
			factor = 1.25
			NOT = { prestige = 0 }
		}
		modifier = {
			factor = 1.2
			NOT = { year = 1400 }
		}
		modifier = {
			factor = 1.1
			NOT = { year = 1500 }
		}
		modifier = {
			factor = 0.9
			year = 1600
		}
		modifier = {
			factor = 0.9
			year = 1700
		}
		modifier = {
			factor = 1.25
			has_country_flag = deibureaucracy_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_army_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_naval_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_scientist
		}
		modifier = {
			factor = 5
			has_country_flag = deimaster_of_merchant_guild
		}
		modifier = {
			factor = 1.25
			has_country_flag = deisaint
		}
		modifier = {
			factor = 1.25
			has_country_flag = deiforeign_merchant
		}
		modifier = {
			factor = 1.25
			has_country_flag = deileader_of_bourgeoisie
		}
		modifier = {
			factor = 1.25
			has_country_flag = deirenaissance_man
		}

	}

	title = "EVTNAME70705"
	desc = "EVTDESC70705"

	option = {
		name = "EVTOPTA70705"
		ai_chance = { factor = 35 }
		mercantilism_freetrade = -1
		random_owned = { base_tax = 2 }
		define_advisor = { type = master_of_merchant_guild skill = 6 } 
		set_country_flag = deimaster_of_merchant_guild
	}
	option = {
		name = "EVTOPTB70705"
		ai_chance = { factor = 35 }
		mercantilism_freetrade = 1
		merchants = 3
		years_of_income = 0.12
		define_advisor = { type = master_of_merchant_guild skill = 6 } 
		set_country_flag = deimaster_of_merchant_guild
	}
	option = {
		name = "EVTOPTC70705"
		ai_chance = { factor = 20 }
		prestige = -0.03
		define_advisor = { type = master_of_merchant_guild skill = 6 } 
		set_country_flag = deimaster_of_merchant_guild
	}
	option = {
		name = "EVTOPTD70705"
		ai_chance = { factor = 10 }
		prestige = 0.01
		set_country_flag = deimaster_of_merchant_guild
	}

}

country_event = {

	id = 70706

	trigger = {
		NOT = { secularism_theocracy = -2 }
	}

	mean_time_to_happen = {
		months = 3100

		modifier = {
			factor = 0.95
			idea = church_attendance_duty
		}
		modifier = {
			factor = 0.95
			idea = divine_supremacy
		}
		modifier = {
			factor = 0.95
			idea = deus_vult
		}
		modifier = {
			factor = 0.9
			defender_of_faith = yes
		}
		modifier = {
			factor = 0.9
			secularism_theocracy = 2
		}
		modifier = {
			factor = 1.5
			OR = {
				technology_group = african
				technology_group = new_world
				technology_group = pacific
			}
		}
		modifier = {
			factor = 1.25
			OR = {
				technology_group = sahel
				technology_group = ordu
			}
		}
		modifier = {
			factor = 1.5
			NOT = { num_of_cities = 2 }
		}
		modifier = {
			factor = 1.2
			NOT = { num_of_cities = 4 }
		}
		modifier = {
			factor = 1.1
			NOT = { num_of_cities = 6 }
		}
		modifier = {
			factor = 1.25
			NOT = { prestige = 0 }
		}
		modifier = {
			factor = 1.2
			NOT = { year = 1400 }
		}
		modifier = {
			factor = 1.1
			NOT = { year = 1500 }
		}
		modifier = {
			factor = 0.9
			year = 1600
		}
		modifier = {
			factor = 0.9
			year = 1700
		}
		modifier = {
			factor = 1.25
			has_country_flag = deibureaucracy_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_army_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_naval_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_scientist
		}
		modifier = {
			factor = 1.25
			has_country_flag = deimaster_of_merchant_guild
		}
		modifier = {
			factor = 5
			has_country_flag = deisaint
		}
		modifier = {
			factor = 1.25
			has_country_flag = deiforeign_merchant
		}
		modifier = {
			factor = 1.25
			has_country_flag = deileader_of_bourgeoisie
		}
		modifier = {
			factor = 1.25
			has_country_flag = deirenaissance_man
		}

	}

	title = "EVTNAME70706"
	desc = "EVTDESC70706"

	option = {
		name = "EVTOPTA70706"
		ai_chance = { factor = 15 }
		secularism_theocracy = 2
		missionaries = 2
		random_owned = {
			limit = { NOT = { religion = catholic } owner = { religion = catholic } }
			religious_rebels = 1
			religion = catholic			
		}
		random_owned = {
			limit = { NOT = { religion = hussite } owner = { religion = hussite } }
			religious_rebels = 1
			religion = hussite			
		}
		random_owned = {
			limit = { NOT = { religion = protestant } owner = { religion = protestant } }
			religious_rebels = 1
			religion = protestant		
		}
		random_owned = {
			limit = { NOT = { religion = reformed } owner = { religion = reformed } }
			religious_rebels = 1
			religion = reformed			
		}
		random_owned = {
			limit = { NOT = { religion = graecocatholic } owner = { religion = graecocatholic } }
			religious_rebels = 1
			religion = graecocatholic			
		}
		random_owned = {
			limit = { NOT = { religion = orthodox } owner = { religion = orthodox } }
			religious_rebels = 1
			religion = orthodox			
		}
		random_owned = {
			limit = { NOT = { religion = greek_orthodox } owner = { religion = greek_orthodox } }
			religious_rebels = 1
			religion = greek_orthodox		
		}
		random_owned = {
			limit = { NOT = { religion = eastern_churches } owner = { religion = eastern_churches } }
			religious_rebels = 1
			religion = eastern_churches		
		}
		random_owned = {
			limit = { NOT = { religion = sunni } owner = { religion = sunni } }
			religious_rebels = 1
			religion = sunni		
		}
		random_owned = {
			limit = { NOT = { religion = ibadi } owner = { religion = ibadi } }
			religious_rebels = 1
			religion = ibadi		
		}
		random_owned = {
			limit = { NOT = { religion = shiite } owner = { religion = shiite } }
			religious_rebels = 1
			religion = shiite		
		}
		random_owned = {
			limit = { NOT = { religion = sufism } owner = { religion = sufism } }
			religious_rebels = 1
			religion = sufism	
		}
		random_owned = {
			limit = { NOT = { religion = wahhabism } owner = { religion = wahhabism } }
			religious_rebels = 1
			religion = wahhabism
		}
		random_owned = {
			limit = { NOT = { religion = hinduism } owner = { religion = hinduism } }
			religious_rebels = 1
			religion = hinduism	
		}
		random_owned = {
			limit = { NOT = { religion = jainism } owner = { religion = jainism } }
			religious_rebels = 1
			religion = jainism		
		}
		random_owned = {
			limit = { NOT = { religion = sikhism } owner = { religion = sikhism } }
			religious_rebels = 1
			religion = sikhism	
		}
		random_owned = {
			limit = { NOT = { religion = buddhism } owner = { religion = buddhism } }
			religious_rebels = 1
			religion = buddhism	
		}
		random_owned = {
			limit = { NOT = { religion = tibetan_buddhism } owner = { religion = tibetan_buddhism } }
			religious_rebels = 1
			religion = tibetan_buddhism
		}
		random_owned = {
			limit = { NOT = { religion = confucianism } owner = { religion = confucianism } }
			religious_rebels = 1
			religion = confucianism	
		}
		random_owned = {
			limit = { NOT = { religion = shinto } owner = { religion = shinto } }
			religious_rebels = 1
			religion = shinto
		}
		random_owned = {
			limit = { NOT = { religion = animism } owner = { religion = animism } }
			religious_rebels = 1
			religion = animism		
		}
		random_owned = {
			limit = { NOT = { religion = shamanism } owner = { religion = shamanism } }
			religious_rebels = 1
			religion = shamanism	
		}
		random_owned = {
			limit = { NOT = { religion = baltic_pagan } owner = { religion = baltic_pagan } }
			religious_rebels = 1
			religion = baltic_pagan		
		}
		random_owned = {
			limit = { NOT = { religion = teotl } owner = { religion = teotl } }
			religious_rebels = 1
			religion = teotl		
		}
		random_owned = {
			limit = { NOT = { religion = inti } owner = { religion = inti } }
			religious_rebels = 1
			religion = inti	
		}
		random_owned = {
			limit = { NOT = { religion = reformed_pagan } owner = { religion = reformed_pagan } }
			religious_rebels = 1
			religion = reformed_pagan		
		}
		define_advisor = { type = saint skill = 6 } 
		set_country_flag = deisaint
	}
	option = {
		name = "EVTOPTB70706"
		ai_chance = { factor = 20 }
		secularism_theocracy = 1
		stability = 1
		define_advisor = { type = saint skill = 6 } 
		set_country_flag = deisaint
	}
	option = {
		name = "EVTOPTC70706"
		ai_chance = { factor = 30 }
		prestige = 0.05
		badboy = -1
		years_of_income = -0.15
		define_advisor = { type = saint skill = 6 } 
		set_country_flag = deisaint
	}
	option = {
		name = "EVTOPTD70706"
		ai_chance = { factor = 25 }
		prestige = -0.03
		define_advisor = { type = saint skill = 6 } 
		set_country_flag = deisaint
	}
	option = {
		name = "EVTOPTE70706"
		ai_chance = { factor = 10 }
		secularism_theocracy = -1
		set_country_flag = deisaint
	}

}

country_event = {

	id = 70707

	trigger = {
		trade_tech = 3
	}

	mean_time_to_happen = {
		months = 3100

		modifier = {
			factor = 0.95
			idea = shrewd_commerce_practise
		}
		modifier = {
			factor = 0.95
			idea = merchant_adventures
		}
		modifier = {
			factor = 0.9
			num_of_cots = 1
		}		
		modifier = {
			factor = 0.95
			mercantilism_freetrade = 1
		}
		modifier = {
			factor = 1.5
			OR = {
				technology_group = african
				technology_group = new_world
				technology_group = pacific
			}
		}
		modifier = {
			factor = 1.25
			OR = {
				technology_group = sahel
				technology_group = ordu
			}
		}
		modifier = {
			factor = 1.5
			NOT = { num_of_cities = 2 }
		}
		modifier = {
			factor = 1.2
			NOT = { num_of_cities = 4 }
		}
		modifier = {
			factor = 1.1
			NOT = { num_of_cities = 6 }
		}
		modifier = {
			factor = 1.25
			NOT = { prestige = 0 }
		}
		modifier = {
			factor = 1.2
			NOT = { year = 1400 }
		}
		modifier = {
			factor = 1.1
			NOT = { year = 1500 }
		}
		modifier = {
			factor = 0.9
			year = 1600
		}
		modifier = {
			factor = 0.9
			year = 1700
		}
		modifier = {
			factor = 1.25
			has_country_flag = deibureaucracy_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_army_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_naval_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_scientist
		}
		modifier = {
			factor = 1.25
			has_country_flag = deimaster_of_merchant_guild
		}
		modifier = {
			factor = 1.25
			has_country_flag = deisaint
		}
		modifier = {
			factor = 5
			has_country_flag = deiforeign_merchant
		}
		modifier = {
			factor = 1.25
			has_country_flag = deileader_of_bourgeoisie
		}
		modifier = {
			factor = 1.25
			has_country_flag = deirenaissance_man
		}

	}

	title = "EVTNAME70707"
	desc = "EVTDESC70707"

	option = {
		name = "EVTOPTA70707"
		ai_chance = { factor = 20 }
		mercantilism_freetrade = 2
		random_owned = {
			limit = { 
				base_tax = 5
				cot = no 
				NOT = { any_neighbor_province = { cot = yes } }
			}
			cot = yes
		}
		random_owned = {
			limit = { cot = no }
			base_tax = -1
		}
		years_of_income = -1.05
		define_advisor = { type = foreign_merchant skill = 6 } 
		set_country_flag = deiforeign_merchant
	}
	option = {
		name = "EVTOPTB70707"
		ai_chance = { factor = 40 }
		mercantilism_freetrade = 1
		merchants = 4
		define_advisor = { type = foreign_merchant skill = 6 } 
		set_country_flag = deiforeign_merchant
	}
	option = {
		name = "EVTOPTC70707"
		ai_chance = { factor = 30 }
		prestige = -0.03
		define_advisor = { type = foreign_merchant skill = 6 } 
		set_country_flag = deiforeign_merchant
	}
	option = {
		name = "EVTOPTD70707"
		ai_chance = { factor = 10 }
		mercantilism_freetrade = -1
		prestige = 0.01
		set_country_flag = deiforeign_merchant
	}

}

country_event = {

	id = 70708

	trigger = {
		production_tech = 3
	}

	mean_time_to_happen = {
		months = 3100

		modifier = {
			factor = 0.9
			idea = national_bank
		}
		modifier = {
			factor = 0.9
			idea = smithian_economics
		}
		modifier = {
			factor = 0.8
			idea = liberty_egalite_fraternity
		}		
		modifier = {
			factor = 0.95
			aristocracy_plutocracy = 2
		}
		modifier = {
			factor = 0.95
			serfdom_freesubjects = 2
		}
		modifier = {
			factor = 1.5
			OR = {
				technology_group = african
				technology_group = new_world
				technology_group = pacific
			}
		}
		modifier = {
			factor = 1.25
			OR = {
				technology_group = sahel
				technology_group = ordu
			}
		}
		modifier = {
			factor = 1.5
			NOT = { num_of_cities = 2 }
		}
		modifier = {
			factor = 1.2
			NOT = { num_of_cities = 4 }
		}
		modifier = {
			factor = 1.1
			NOT = { num_of_cities = 6 }
		}
		modifier = {
			factor = 1.25
			NOT = { prestige = 0 }
		}
		modifier = {
			factor = 1.2
			NOT = { year = 1400 }
		}
		modifier = {
			factor = 1.1
			NOT = { year = 1500 }
		}
		modifier = {
			factor = 0.9
			year = 1600
		}
		modifier = {
			factor = 0.9
			year = 1700
		}
		modifier = {
			factor = 1.25
			has_country_flag = deibureaucracy_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_army_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_naval_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_scientist
		}
		modifier = {
			factor = 1.25
			has_country_flag = deimaster_of_merchant_guild
		}
		modifier = {
			factor = 1.25
			has_country_flag = deisaint
		}
		modifier = {
			factor = 1.25
			has_country_flag = deiforeign_merchant
		}
		modifier = {
			factor = 5
			has_country_flag = deileader_of_bourgeoisie
		}
		modifier = {
			factor = 1.25
			has_country_flag = deirenaissance_man
		}

	}

	title = "EVTNAME70708"
	desc = "EVTDESC70708"

	option = {
		name = "EVTOPTA70708"
		ai_chance = { factor = 25 }
		centralization_decentralization = -1
		aristocracy_plutocracy = 2
		serfdom_freesubjects = 1
		random = {
			chance = 75
			random_owned = { noble_rebels = 1 }
     		}
		random = {
			chance = 50
			random_owned = { noble_rebels = 1 }
     		}
		random = {
			chance = 25
			random_owned = { noble_rebels = 1 }
     		}
		stability = -3
		define_advisor = { type = leader_of_bourgeoisie skill = 6 } 
		set_country_flag = deileader_of_bourgeoisie
	}
	option = {
		name = "EVTOPTB70708"
		ai_chance = { factor = 25 }
		serfdom_freesubjects = 2
		random_owned = { base_tax = 1 }
		random_owned = { base_tax = 1 }
		random = {
			chance = 75
			random_owned = { noble_rebels = 1 }
     		}
		random = {
			chance = 50
			random_owned = { noble_rebels = 1 }
     		}
		random = {
			chance = 25
			random_owned = { noble_rebels = 1 }
     		}
		stability = -2
		define_advisor = { type = leader_of_bourgeoisie skill = 6 } 
		set_country_flag = deileader_of_bourgeoisie
	}
	option = {
		name = "EVTOPTC70708"
		ai_chance = { factor = 40 }
		prestige = -0.03
		define_advisor = { type = leader_of_bourgeoisie skill = 6 } 
		set_country_flag = deileader_of_bourgeoisie
	}
	option = {
		name = "EVTOPTD70708"
		ai_chance = { factor = 10 }
		aristocracy_plutocracy = -1
		prestige = 0.01
		set_country_flag = deileader_of_bourgeoisie
	}

}

country_event = {

	id = 70709

	trigger = {
		culture_tech = 3
	}

	mean_time_to_happen = {
		months = 3100

		modifier = {
			factor = 0.8
			idea = patron_of_art
		}
		modifier = {
			factor = 0.9
			idea = liberty_egalite_fraternity
		}		
		modifier = {
			factor = 0.95
			serfdom_freesubjects = 2
		}
		modifier = {
			factor = 0.95
			NOT = { innovative_narrowminded = -1 }
		}
		modifier = {
			factor = 1.5
			OR = {
				technology_group = african
				technology_group = new_world
				technology_group = pacific
			}
		}
		modifier = {
			factor = 1.25
			OR = {
				technology_group = sahel
				technology_group = ordu
			}
		}
		modifier = {
			factor = 1.5
			NOT = { num_of_cities = 2 }
		}
		modifier = {
			factor = 1.2
			NOT = { num_of_cities = 4 }
		}
		modifier = {
			factor = 1.1
			NOT = { num_of_cities = 6 }
		}
		modifier = {
			factor = 1.25
			NOT = { prestige = 0 }
		}
		modifier = {
			factor = 1.2
			NOT = { year = 1400 }
		}
		modifier = {
			factor = 1.1
			NOT = { year = 1500 }
		}
		modifier = {
			factor = 0.9
			year = 1600
		}
		modifier = {
			factor = 0.9
			year = 1700
		}
		modifier = {
			factor = 1.25
			has_country_flag = deibureaucracy_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_army_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_naval_reformer
		}
		modifier = {
			factor = 1.25
			has_country_flag = deigreat_scientist
		}
		modifier = {
			factor = 1.25
			has_country_flag = deimaster_of_merchant_guild
		}
		modifier = {
			factor = 1.25
			has_country_flag = deisaint
		}
		modifier = {
			factor = 1.25
			has_country_flag = deiforeign_merchant
		}
		modifier = {
			factor = 1.25
			has_country_flag = deileader_of_bourgeoisie
		}
		modifier = {
			factor = 5
			has_country_flag = deirenaissance_man
		}

	}

	title = "EVTNAME70709"
	desc = "EVTDESC70709"

	option = {
		name = "EVTOPTA70709"
		ai_chance = { factor = 25 }
		years_of_income = -0.25
		prestige = 0.1
		cultural_tradition = 0.15
		stability = 1
		define_advisor = { type = renaissance_man skill = 6 } 
		set_country_flag = deirenaissance_man
	}
	option = {
		name = "EVTOPTB70709"
		ai_chance = { factor = 25 }
		years_of_income = -0.2
		officials = 2
		prestige = 0.05
		cultural_tradition = 0.05
		define_advisor = { type = renaissance_man skill = 6 } 
		set_country_flag = deirenaissance_man
	}
	option = {
		name = "EVTOPTC70709"
		ai_chance = { factor = 40 }
		prestige = -0.03
		define_advisor = { type = renaissance_man skill = 6 } 
		set_country_flag = deirenaissance_man
	}
	option = {
		name = "EVTOPTD70709"
		ai_chance = { factor = 10 }
		innovative_narrowminded = 1
		prestige = 0.01
		set_country_flag = deirenaissance_man
	}

}