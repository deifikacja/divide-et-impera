#Japan Discovered!
country_event = {
	id = 9000
	title = "EVTNAME9000"
	desc = "EVTDESC9000"
	
	major = yes
	
	trigger = {
		NOT = {
			has_global_flag = japan_has_encountered_the_west
		}
		technology_group = western
		OR = {
			religion = reformed
			religion = catholic
			religion = protestant
			religion = hussite
			religion = episcopalism
		}
		japanese_region = {
			has_discovered = THIS
		}
	}
	
	mean_time_to_happen = {
		months = 12
	}
	
	option = {
		name = "EVTOPTA9000"
		prestige = 0.1
		set_global_flag = japan_has_encountered_the_west
	}
}

country_event = {
	id = 9001
	title = "EVTNAME9001"
	desc = "EVTDESC9001"
	
	trigger = {
		NOT = { 1020 = { has_discovered = THIS } }
		NOT = { has_country_flag = japan_discovered }
		japanese_region = {
			has_discovered = THIS
		}
	}
	
	mean_time_to_happen = {
		months = 1
	}

	immediate = { set_country_flag = japan_discovered }

	option = {
		name = "EVTOPTA9001"
		1020 = { discover = yes }
	}
}

# Nanban Trade
country_event = {
	id = 9002
	title = "EVTNAME9002"
	desc = "EVTDESC9002"
	
	trigger = {
		is_daimyo = yes
		has_global_flag = japan_has_encountered_the_west
		NOT = {
			has_global_flag = japan_is_closed
		}
	}
	
	mean_time_to_happen = {
		months = 300
		modifier = {
			factor = 0.8
			has_country_modifier = western_embassy_mission
		}
		modifier = {
			factor = 0.8
			mercantilism_freetrade = 0
		}
		modifier = {
			factor = 0.8
			mercantilism_freetrade = 2
		}
	}
	
	option = {
		name = "EVTOPTA9002"
		random_list = {
			20 = {
				trade_tech = 200
			}
			20 = {
				land_tech = 200
			}
			20 = {
				naval_tech = 200
			}
			20 = {
				production_tech = 200
			}
			20 = {
				government_tech = 200
			}
		}
		treasury = -50
	}
	
	option = {
		name = "EVTOPTB9002"
		prestige = 0.05
		treasury = 50
	}
}

# Christian Pater
country_event = {
	id = 9004
	title = "EVTNAME9004"
	desc = "EVTDESC9004"
	
	trigger = {
		NOT = {
			has_global_flag = japan_is_closed
		}
		is_daimyo = yes
		has_global_flag = japan_has_encountered_the_west
		any_known_country = {
			technology_group = western
			religion_group = christian
		}
	}
	
	mean_time_to_happen = {
		months = 450
		modifier = {
			factor = 0.9
			NOT = { innovative_narrowminded = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { innovative_narrowminded = -2 }
		}
		modifier = {
			factor = 0.9
			NOT = { innovative_narrowminded = -3 }
		}
		modifier = {
			factor = 0.8
			has_country_modifier = western_embassy_mission
		}
	}
	
	option = {
		name = "EVTOPTA9004"
		random_owned = {
			add_province_modifier = {
				name = local_christian_mission
				duration = 3650
			}
		}
	}
	
	option = {
		name = "EVTOPTB9004"
		government_tech = -400
		prestige = 0.02
	}
}

#Christian Converts
province_event = {
	id = 9006
	title = "EVTNAME9006"
	desc = "EVTDESC9006"
	
	trigger = {
		NOT = {
			has_global_flag = japan_is_closed
		}
		has_province_modifier = local_christian_mission
		owner = {
			is_daimyo = yes
		}
		has_global_flag = japan_has_encountered_the_west
	}
	
	mean_time_to_happen = {
		months = 450
		modifier = {
			factor = 0.9
			owner = { NOT = { innovative_narrowminded = 0 } }
		}
		modifier = {
			factor = 0.9
			owner = { NOT = { innovative_narrowminded = -2 } }
		}
		modifier = {
			factor = 0.9
			owner = { NOT = { innovative_narrowminded = -3 } }
		}
	}
	
	option = {
		name = "EVTOPTA9006"
		religion = catholic
		remove_province_modifier = local_christian_mission
	}
	
	option = {
		name = "EVTOPTB9006"
		create_revolt = 3
		remove_province_modifier = local_christian_mission
	}
}

#Kirishitan Tensions
province_event = {
	id = 9008
	title = "EVTNAME9008"
	desc = "EVTDESC9008"
	
	trigger = {
		religion = catholic
		owner = {
			religion = shinto
		}
	}
	
	mean_time_to_happen = {
		months = 200
		modifier = {
			factor = 0.9
			revolt_risk = 2
		}
		modifier = {
			factor = 0.8
			revolt_risk = 4
		}
		modifier = {
			factor = 0.8
			revolt_risk = 8
		}
		modifier = {
			factor = 0.9
			idea = deus_vult
		}
		modifier = {
			factor = 1.4
			has_global_flag = japan_is_closed
		}
	}
	
	option = {
		name = "EVTOPTA9008"
		random_list = {
			95 = {
				create_revolt = 5
			}
			5 = {
				religion = shinto
				create_revolt = 3
			}
		}
	}
}

#Red Seal Ship Returns
country_event = {
	id = 9010
	title = "EVTNAME9010"
	desc = "EVTDESC9010"
	
	trigger = {
		is_daimyo = yes
		NOT = {
			has_global_flag = japan_is_closed
		}
	}
	
	mean_time_to_happen = {
		months = 700
		modifier = {
			factor = 0.8
			idea = merchant_adventures
		}
		modifier = {
			factor = 0.8
			mercantilism_freetrade = 0
		}
		modifier = {
			factor = 0.8
			mercantilism_freetrade = 2
		}
		modifier = {
			factor = 0.8
			mercantilism_freetrade = 4
		}
		modifier = {
			factor = 0.9
			idea = grand_navy
		}
		modifier = {
			factor = 0.9
			advisor = trader
		}
		modifier = {
			factor = 1.2
			NOT = {
				mercantilism_freetrade = 0
			}
		}
		modifier = {
			factor = 1.2
			NOT = {
				mercantilism_freetrade = -2
			}
		}
		modifier = {
			factor = 1.2
			NOT = {
				mercantilism_freetrade = -4
			}
		}
	}
	
	option = {
		name = "EVTOPTA9010"
		random_list = {
			20 = {
				trade_tech = 200
				naval_tech = 200
				army_tradition = 0.05
			}
			20 = {
				land_tech = 200
				government_tech = 200
				officials = 2
			}
			20 = {
				production_tech = 200
				government_tech = 200
				diplomats = 1
				spies = 1
			}
			20 = {
				years_of_income = 0.5
				merchants = 2
			}
		}
	}
}

#Western Embassy Returns Successful
country_event = {
	id = 9012
	title = "EVTNAME9012"
	desc = "EVTDESC9012"
	
	trigger = {
		has_country_modifier = western_embassy_mission
		any_owned_province = {
			religion = shinto
		}
		NOT = {
			religion = catholic
			has_global_flag = japan_is_closed
		}
	}
	
	mean_time_to_happen = {
		months = 500
		modifier = {
			factor = 0.9
			DIP = 4
		}
		modifier = {
			factor = 0.9
			DIP = 5
		}
		modifier = {
			factor = 0.9
			DIP = 6
		}
	}
	
	option = {
		name = "EVTOPTA9012"
		government_tech = 500
		trade_tech = 500
		random_owned = {
			limit = {
				religion = shinto
			}
			religion = catholic
		}
		remove_country_modifier = western_embassy_mission
	}
	
	option = {
		name = "EVTOPTB9012"
		any_country = {
			limit = {
				has_country_flag = japan_discovered
				technology_group = western
			}
			relation = {
				who = THIS
				value = 25
			}
		}
		prestige = 0.05
		diplomats = 2
		remove_country_modifier = western_embassy_mission
	}
}

#Local Kirishitans Harrassed
province_event = {
	id = 9014
	title = "EVTNAME9014"
	desc = "EVTDESC9014"
	
	trigger = {
		religion = catholic
		owner = {
			religion = shinto
		}
	}
	
	mean_time_to_happen = {
		months = 500
		modifier = {
			factor = 0.3
			has_global_flag = japan_is_closed
		}
		modifier = {
			factor = 0.8
			owner = { innovative_narrowminded = 0 }
		}
		modifier = {
			factor = 0.8
			owner = { innovative_narrowminded = 2 }
		}
		modifier = {
			factor = 0.8
			owner = { innovative_narrowminded = 4 }
		}
	}
	
	option = {
		name = "EVTOPTA9014"
		create_revolt = 5
		religion = shinto
	}
}

# Tempura Introduced
country_event = {
	id = 9016
	title = "EVTNAME9016"
	desc = "EVTDESC9016"
	
	trigger = {
		is_daimyo = yes
		has_global_flag = japan_has_encountered_the_west
		NOT = {
			has_global_flag = japan_is_closed
		}
	}
	
	mean_time_to_happen = {
		months = 400
		modifier = {
			factor = 0.8
			NOT = {
				innovative_narrowminded = 0
			}
		}
		modifier = {
			factor = 0.8
			NOT = {
				innovative_narrowminded = -2
			}
		}
		modifier = {
			factor = 0.8
			NOT = {
				innovative_narrowminded = -4
			}
		}
		modifier = {
			factor = 1.5
			innovative_narrowminded = 0
		}	
	}
	
	option = {
		name = "EVTOPTA9016"
		cultural_tradition = 0.05
	}
	
	option = {
		name = "EVTOPTB9016"
		years_of_income = 0.1
	}
}

#Western Arquebuses
country_event = {
	id = 9018
	title = "EVTNAME9018"
	desc = "EVTDESC9018"
	
	trigger = {
		is_daimyo = yes
		has_global_flag = japan_has_encountered_the_west
		NOT = {
			has_global_flag = japan_is_closed
		}
		any_known_country = {
			technology_group = western
		}
	}
	
	mean_time_to_happen = {
		months = 500
		modifier = {
			factor = 0.9
			NOT = {
				innovative_narrowminded = 0
			}
		}
		modifier = {
			factor = 0.9
			NOT = {
				innovative_narrowminded = -2
			}
		}
		modifier = {
			factor = 0.9
			NOT = {
				innovative_narrowminded = -4
			}
		}
	}
	
	option = {
		name = "EVTOPTA9018"
		add_country_modifier = {
			name = a_batch_of_western_muskets
			duration = 730
		}
		years_of_income = -0.5
	}
	
	option = {
		name = "EVTOPTB9018"
		prestige = 0.05
	}
}

#Kabuki Theatre 
country_event = {
	id = 9020
	title = "EVTNAME9020"
	desc = "EVTDESC9020"
	
	trigger = {
		year = 1600
		is_daimyo = yes
	}
	
	mean_time_to_happen = {
		months = 400
		modifier = {
			factor = 0.8
			cultural_tradition = 0.1
		}
		modifier = {
			factor = 0.8
			cultural_tradition = 0.2
		}
		modifier = {
			factor = 0.8
			cultural_tradition = 0.3
		}
		modifier = {
			factor = 0.6
			idea = patron_of_art
		}
	}
	
	option = {
		name = "EVTOPTA9020"
		years_of_income = 0.2
		cultural_tradition = 0.05
		prestige = 0.05
		legitimacy = 0.05
	}
	
	option = {
		name = "EVTOPTB9020"
		cultural_tradition = 0.05
		define_advisor = { 
			type = artist
			skill = 4
		}
	}
}

#Ronin offers his Service
country_event = {
	id = 9022
	title = "EVTNAME9022"
	desc = "EVTDESC9022"
	
	trigger = {
		is_daimyo = yes
	}
	
	mean_time_to_happen = {
		months = 450
		modifier = {
			factor = 0.9
			army_tradition = 0.1
		}
		modifier = {
			factor = 0.9
			army_tradition = 0.2
		}
		modifier = {
			factor = 0.9
			army_tradition = 0.3
		}
		modifier = {
			factor = 1.5
			war_exhaustion = 8
		}
	}
	
	option = {
		name = "EVTOPTA9022"
		years_of_income = -0.2
		random_list = {
			10 = {
				create_advisor = statesman
			}
			10 = {
				create_advisor = army_reformer
			}
			10 = {
				create_advisor = grand_captain
			}
		}
	}
	
	option = {
		name = "EVTOPTB9022"
		army_tradition = 0.05
	}
	
	option = {
		name = "EVTOPTC9022"
		years_of_income = 0.1
	}
}

#Nogaku Play
country_event = {
	id = 9024
	title = "EVTNAME9024"
	desc = "EVTDESC9024"
	
	trigger = {
		is_daimyo = yes
	}
	
	mean_time_to_happen = {
		months = 350
		modifier = {
			factor = 0.9
			stability = 1
		}
		modifier = {
			factor = 0.9
			cultural_tradition = 0.15
		}
		modifier = {
			factor = 1.5
			NOT = {
				stability = 0
			}
		}
	}
	
	option = {
		name = "EVTOPTA9024"
		cultural_tradition = 0.1
	}
	
	option = {
		name = "EVTOPTB9024"
		prestige = 0.05
		legitimacy = 0.05
	}
}

#Karesansui Garden
country_event = {
	id = 9026
	title = "EVTNAME9026"
	desc = "EVTDESC9026"
	
	trigger = {
		is_daimyo = yes
	}
	
	mean_time_to_happen = {
		months = 450
		modifier = {
			factor = 0.8
			advisor = artist
		}
		modifier = {
			factor = 0.8
			idea = patron_of_art
		}
		modifier = {
			factor = 0.8
			cultural_tradition = 0.05
		}
		modifier = {
			factor = 1.5
			badboy = 0.5
		}
	}
	
	option = {
		name = "EVTOPTA9026"
		random_owned = {
			add_province_modifier = {
				name = karesansui_garden
				duration = 18250
			}
		}
		years_of_income = -0.5
	}
	
	option = {
		name = "EVTOPTB9026"
		cultural_tradition = -0.04
	}
}

# Rewarding Loyal Samurai
country_event = {
	id = 9028
	title = "EVTNAME9028"
	desc = "EVTDESC9028"
	
	trigger = {
		is_daimyo = yes
		army_tradition = 0.1
	}
	
	mean_time_to_happen = {
		months = 450
		modifier = {
			factor = 0.9
			stability = 1
		}
		modifier = {
			factor = 0.9
			stability = 2
		}
		modifier = {
			factor = 0.9
			army_tradition = 0.2
		}
		modifier = {
			factor = 1.5
			NOT = {
				stability = 0
			}
		}
	}
	
	option = {
		name = "EVTOPTA9028"
		legitimacy = 0.05
		prestige = 0.05
		years_of_income = -0.5
	}
	
	option = {
		name = "EVTOPTB9028"
		stability = -1
	}
}

#Travelling Monk
country_event = {
	id = 9030
	title = "EVTNAME9030"
	desc = "EVTDESC9030"
	
	trigger = {
		religion = shinto
	}
	
	mean_time_to_happen = {
		months = 350
		modifier = {
			factor = 0.8
			innovative_narrowminded = 0
		}
		modifier = {
			factor = 0.8
			innovative_narrowminded = 3
		}
		modifier = {
			factor = 0.8
			innovative_narrowminded = 5
		}
	}
	
	option = {
		name = "EVTOPTA9030"
		years_of_income = -0.3
		prestige = 0.03
		missionaries = 1
	}
	
	option = {
		name = "EVTOPTB9030"
		stability = -1
	}
}

#Visiting A Shrine
country_event = {
	id = 9032
	title = "EVTNAME9032"
	desc = "EVTDESC9032"
	
	trigger = {
		religion = shinto
	}
	
	mean_time_to_happen = {
		months = 450
		modifier = {
			factor = 0.8
			innovative_narrowminded = 0
		}
		modifier = {
			factor = 0.8
			innovative_narrowminded = 3
		}
		modifier = {
			factor = 0.8
			innovative_narrowminded = 5
		}
		modifier = {
			factor = 1.5
			NOT = {
				stability = 0
			}
		}
	}
	
	option = {
		name = "EVTOPTA9032"
		stability = 1
		prestige = -0.1
		legitimacy = -0.1
	}
	
	option = {
		name = "EVTOPTB9032"
		years_of_income = -0.1
		cultural_tradition = 0.2
	}
}

#Cherry Blossom Festival
country_event = {
	id = 9034
	title = "EVTNAME9034"
	desc = "EVTDESC9034"
	
	trigger = {
		religion = shinto
		month = 0
		NOT = {
			month = 5
		}
	}
	
	mean_time_to_happen = {
		months = 450
		modifier = {
			factor = 0.8
			cultural_tradition = 0.1
		}
		modifier = {
			factor = 0.8
			cultural_tradition = 0.2
		}
		modifier = {
			factor = 1.5
			NOT = {
				stability = 0
			}
		}
	}
	
	option = {
		name = "EVTOPTA9034"
		stability = 1
		prestige = 0.05
	}
	
	option = {
		name = "EVTOPTB9034"
		years_of_income = 0.5
		cultural_tradition = 0.05
	}
}

# 1000 Koku Tax
country_event = {
	id = 9036
	title = "EVTNAME9036"
	desc = "EVTDESC9036"
	
	trigger = {
		is_daimyo = yes
	}
	
	mean_time_to_happen = {
		months = 450
		modifier = {
			factor = 0.8
			stability = 1
		}
		modifier = {
			factor = 0.9
			stability = 2
		}
		modifier = {
			factor = 0.9
			stability = 3
		}
		modifier = {
			factor = 1.5
			NOT = {
				stability = 0
			}
		}
	}
	
	option = {
		name = "EVTOPTA9036"
		years_of_income = 1
		stability = -1
		prestige = -0.05
	}
	
	option = {
		name = "EVTOPTB9036"
		years_of_income = 0.5
		stability = -1
		prestige = 0.05
	}
	
	option = {
		name = "EVTOPTC9036"
		prestige = -0.05
	}
}

#Death of a Geisha
country_event = {
	id = 9038
	title = "EVTNAME9038"
	desc = "EVTDESC9038"
	
	trigger = {
		is_daimyo = yes
	}
	
	mean_time_to_happen = {
		months = 450
		modifier = {
			factor = 0.9
			NOT = {
				stability = 0
			}
		}
		modifier = {
			factor = 0.9
			NOT = {
				stability = -1
			}
		}
		modifier = {
			factor = 0.9
			NOT = {
				stability = -2
			}
		}
		modifier = {
			factor = 1.5
			stability = 1
		}
		modifier = {
			factor = 1.5
			idea = patron_of_art
		}
	}
	
	option = {
		name = "EVTOPTA9038"
		stability = -1
		cultural_tradition = 0.05
	}
	
	option = {
		name = "EVTOPTB9038"
		prestige = -0.05
		cultural_tradition = -0.05
	}
}

#The Sharpness of the Tanto
country_event = {
	id = 9040
	title = "EVTNAME9040"
	desc = "EVTDESC9040"
	
	trigger = {
		is_daimyo = yes
		has_heir = yes
		NOT = {
			stability = -2
		}
		NOT = {
			prestige = 0
		}
	}
	
	mean_time_to_happen = {
		months = 450
		modifier = {
			factor = 0.8
			NOT = {
				prestige = -0.1
			}
		}
		modifier = {
			factor = 0.8
			NOT = {
				legitimacy = 0.7
			}
		}
		modifier = {
			factor = 0.8
			NOT = {
				legitimacy = 0.5
			}
		}
	}
	
	option = {
		name = "EVTOPTA9040"
		kill_ruler = yes
		stability = 2
		prestige = 0.05
		legitimacy = 0.05
	}
	
	option = {
		name = "EVTOPTB9040"
		legitimacy = -0.1
		prestige = -0.05
	}
}

#Unruly Daimyo
country_event = {
	id = 9042
	title = "EVTNAME9042"
	desc = "EVTDESC9042"
	
	trigger = {
		is_daimyo = yes
		NOT = {
			legitimacy = 0.6
		}
	}
	
	mean_time_to_happen = {
		months = 650
		modifier = {
			factor = 0.8
			NOT = {
				legitimacy = 0.5
			}
		}
		modifier = {
			factor = 0.8
			NOT = {
				legitimacy = 0.4
			}
		}
		modifier = {
			factor = 1.5
			stability = 1
		}
	}
	
	option = {
		name = "EVTOPTA9042"
		centralization_decentralization = 1
		prestige = -0.05
	}
	
	option = {
		name = "EVTOPTB9042"
		random_owned = {
			noble_rebels = 2
		}
		random_owned = {
			noble_rebels = 1
		}
		legitimacy = 0.05
		prestige = 0.05
	}
}

# Koku Reform
country_event = {
	id = 9044
	title = "EVTNAME9044"
	desc = "EVTDESC9044"
	
	trigger = {
		is_daimyo = yes
		inflation = 10
	}
	
	mean_time_to_happen = {
		months = 450
		modifier = {
			factor = 0.9
			inflation = 15
		}
		modifier = {
			factor = 0.9
			inflation = 20
		}
		modifier = {
			factor = 0.9
			inflation = 25
		}
		modifier = {
			factor = 1.5
			stability = 1
		}
		modifier = {
			factor = 1.5
			stability = 2
		}
	}
	
	option = {
		name = "EVTOPTA9044"
		create_advisor = banker
		years_of_income = 0.4
	}
	
	option = {
		name = "EVTOPTB9044"
		inflation = -4
		centralization_decentralization = 1
	}
}

#Rangaku
country_event = {
	id = 9046
	title = "EVTNAME9046"
	desc = "EVTDESC9046"
	
	trigger = {
		is_daimyo = yes
		has_global_flag = japan_has_encountered_the_west
		any_known_country = {
			technology_group = western
			religion_group = christian
		}
		NOT = {
			has_global_flag = japan_is_closed
		}
	}
	
	mean_time_to_happen = {
		months = 550
		modifier = {
			factor = 0.5
			year = 1700
		}
		modifier = {
			factor = 0.8
			NOT = { 
				innovative_narrowminded = 0 
			}
		}
		modifier = {
			factor = 0.8
			NOT = { 
				innovative_narrowminded = -2 
			}
		}
		modifier = {
			factor = 1.5
			NOT = {
				mercantilism_freetrade = 0
			}
		}
		modifier = {
			factor = 1.5
			NOT = {
				mercantilism_freetrade = -2
			}
		}
	}
	
	option = {
		name = "EVTOPTA9046"
		mercantilism_freetrade = 1
		stability = -1
		land_tech = 800
		government_tech = 400
		production_tech = 400
		years_of_income = -0.3
	}
	
	option = {
		name = "EVTOPTB9046"
		naval_tech = 400
		production_tech = 200
		merchants = 2
		prestige = 0.05
	}
}

#The Old Pond
country_event = {
	id = 9048
	title = "EVTNAME9048"
	desc = "EVTDESC9048"
	
	trigger = {
		is_daimyo = yes
		year = 1660
	}
	
	mean_time_to_happen = {
		months = 450
		modifier = {
			factor = 0.9
			stability = 3
		}
		modifier = {
			factor = 0.9
			stability = 2
		}
		modifier = {
			factor = 0.9
			stability = 1
		}
		modifier = {
			factor = 1.5
			NOT = {
				stability = 0
			}
		}
	}
	
	option = {
		name = "EVTOPTA9048"
		cultural_tradition = 0.05
	}
	
	option = {
		name = "EVTOPTB9048"
		naval_tech = 200
	}
}

#Rice Crops fail
country_event = {
	id = 9050
	title = "EVTNAME9050"
	desc = "EVTDESC9050"
	
	trigger = {
		is_daimyo = yes
	}
	
	mean_time_to_happen = {
		months = 450
		modifier = {
			factor = 0.8
			NOT = {
				stability = -2
			}
		}
		modifier = {
			factor = 0.8
			NOT = {
				stability = -1
			}
		}
		modifier = {
			factor = 1.5
			stability = 1
		}
	}
	
	option = {
		name = "EVTOPTA9050"
		stability = -1
	}
	
	option = {
		name = "EVTOPTB9050"
		years_of_income = -0.5
	}
}
