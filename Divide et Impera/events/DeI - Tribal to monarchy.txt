##############################
# changing tribal governments
#############################

# for asian & westernisation countries 

country_event = {

	id = 1900

		trigger = {
			year = 1450

			OR = {
				technology_group = western
				technology_group = eastern
				technology_group = muslim
				technology_group = chinese
				technology_group = indian
			     }

			OR = {
				government = tribal_despotism
				government = tribal_federation
				government = tribal_democracy
		     	     }

			any_neighbor_country = {
						NOT = { 
							government = tribal_despotism     		
							government = tribal_democracy
							government = tribal_federation
						}
			}
		}

		mean_time_to_happen = {
			months = 1000

			modifier = { 
				    factor = 0.7
				    ADM = 9
				   }
			modifier = { 
				    factor = 0.8
				    ADM = 7
				   }
			modifier = { 
				    factor = 0.8
				    DIP = 9
				   }		
			modifier = { 
				    factor = 0.9
				    DIP = 7
				   }
			modifier = { 
				    factor = 0.8
				    MIL = 9
				   }		
			modifier = { 
				    factor = 0.9
				    MIL = 7
				   }
			modifier = { 
				    factor = 1.4
				    NOT = { ADM = 4 }
				   }
			modifier = { 
				    factor = 1.3
				    NOT = { ADM = 6 }
				   }
			modifier = { 
				    factor = 1.3
				    NOT = { DIP = 4 }
				   }		
			modifier = { 
				    factor = 1.2
				    NOT = { DIP = 6 }
				   }
			modifier = { 
				    factor = 1.3
				    NOT = { MIL = 4 }
				   }		
			modifier = { 
				    factor = 1.2
				    NOT = { MIL = 6 }
				   }
			modifier = { 
				    factor = 0.8
				    any_neighbor_country = { technology_group = western }
				   }
			modifier = { 
				    factor = 0.8
				    any_neighbor_country = { technology_group = eastern }
				   }
			modifier = { 
				    factor = 0.7
				    NOT = { innovative_narrowminded = -4 }
				   }
			modifier = { 
				    factor = 0.8
				    NOT = { innovative_narrowminded = -1 }
				   }
			modifier = { 
				    factor = 1.2
				    innovative_narrowminded = 1
				   }
			modifier = { 
				    factor = 1.3
				    innovative_narrowminded = 5
				   }
			modifier = {
				    factor = 1.2
				    NOT = { year = 1590 } 
				   }
			modifier = {
				    factor = 0.9
				    year = 1690 
				   } 
				     }

		title = "EVTNAME1900"
		desc = "EVTDESC1900"

		option = {
				name = "EVTOPTA1900"
				ai_chance = { factor = 50 }
				government = eastern_despotism
				stability = -6
				treasury = -50
			 }
		
		option = {
				name = "EVTOPTB1900"
				ai_chance = { factor = 50 }
				stability = 1
			 }
		}

# for african countries

country_event = {

	id = 1901

		trigger = {
			year = 1500
			technology_group = african

			OR = {
				government = tribal_despotism
				government = tribal_federation
				government = tribal_democracy
		     	     }

			any_neighbor_country = {
						NOT = { 
							government = tribal_despotism     		
							government = tribal_democracy
							government = tribal_federation
						}
			}
		}

		mean_time_to_happen = {
					months = 1500

			modifier = { 
				    factor = 0.8
				    ADM = 9
				   }
			modifier = { 
				    factor = 0.9
				    ADM = 7
				   }
			modifier = { 
				    factor = 0.9
				    DIP = 9
				   }		
			modifier = { 
				    factor = 0.95
				    DIP = 7
				   }
			modifier = { 
				    factor = 0.9
				    MIL = 9
				   }		
			modifier = { 
				    factor = 0.95
				    MIL = 7
				   }
			modifier = { 
				    factor = 1.4
				    NOT = { ADM = 4 }
				   }
			modifier = { 
				    factor = 1.3
				    NOT = { ADM = 6 }
				   }
			modifier = { 
				    factor = 1.3
				    NOT = { DIP = 4 }
				   }		
			modifier = { 
				    factor = 1.2
				    NOT = { DIP = 6 }
				   }
			modifier = { 
				    factor = 1.3
				    NOT = { MIL = 4 }
				   }		
			modifier = { 
				    factor = 1.2
				    NOT = { MIL = 6 }
				   }
			modifier = { 
				    factor = 0.8
				    any_neighbor_country = { technology_group = western }
				   }
			modifier = { 
				    factor = 0.8
				    any_neighbor_country = { technology_group = eastern }
				   }
			modifier = { 
				    factor = 0.7
				    NOT = { innovative_narrowminded = -4 }
				   }
			modifier = { 
				    factor = 0.8
				    NOT = { innovative_narrowminded = -1 }
				   }
			modifier = { 
				    factor = 1.2
				    innovative_narrowminded = 1
				   }
			modifier = { 
				    factor = 1.3
				    innovative_narrowminded = 5
				   }
			modifier = {
				    factor = 1.2
				    NOT = { year = 1650 } 
				   }
			modifier = {
				    factor = 0.9
				    year = 1750  
				   }
				      }

		title = "EVTNAME1900"
		desc = "EVTDESC1900"

		option = {
				name = "EVTOPTA1900"
				ai_chance = { factor = 40 }
				government = eastern_despotism
				stability = -6
				treasury = -50
			 }
		
		option = {
				name = "EVTOPTB1900"
				ai_chance = { factor = 60 }
				stability = 2
			 }
		}

# for new world countries

country_event = {

	id = 1902

	trigger = {
		year = 1520
		OR = { 
			technology_group = new_world
			technology_group = pacific
		}
		OR = {
			government = tribal_despotism
			government = tribal_federation
			government = tribal_democracy
		}
		any_neighbor_country = {
			NOT = {
				government = tribal_despotism 
				government = tribal_democracy
				government = tribal_federation
			}
		}
	}

	mean_time_to_happen = {
		months = 1600

		modifier = {
			factor = 0.8
			ADM = 9
		}
		modifier = {
			factor = 0.9
			ADM = 7
		}
		modifier = {
			factor = 0.9
			DIP = 9
		}		
		modifier = {
			factor = 0.95
			DIP = 7
		}
		modifier = {
			factor = 0.9
			MIL = 9
		}		
		modifier = {
			factor = 0.95
			MIL = 7
		}
		modifier = { 
			factor = 1.4
			NOT = { ADM = 4 }
		}
		modifier = {
			factor = 1.3
			NOT = { ADM = 6 }
		}
		modifier = {
			factor = 1.3
			NOT = { DIP = 4 }
		}		
		modifier = {
			factor = 1.2
			NOT = { DIP = 6 }
		}
		modifier = {
			factor = 1.3
			NOT = { MIL = 4 }
		}		
		modifier = { 
			factor = 1.2
			NOT = { MIL = 6 }
		}
		modifier = { 
			factor = 0.8
			any_neighbor_country = { technology_group = western }
		}
		modifier = {
			factor = 0.8
			any_neighbor_country = { technology_group = eastern }
		}
		modifier = { 
			factor = 0.7
			NOT = { innovative_narrowminded = -4 }
		}
		modifier = {
			factor = 0.8
			NOT = { innovative_narrowminded = -1 }
		}
		modifier = {
			factor = 1.2
			innovative_narrowminded = 1
		}
		modifier = {
			factor = 1.3
			innovative_narrowminded = 5
		}
		modifier = {
			factor = 1.2
			NOT = { year = 1650 }
		}
		modifier = {
			factor = 0.9
			year = 1750
		}
	}

	title = "EVTNAME1900"
	desc = "EVTDESC1900"
	
	option = {
		name = "EVTOPTA1900"
		ai_chance = { factor = 40 }
		government = eastern_despotism
		stability = -6
		treasury = -50
	}

	option = {
		name = "EVTOPTB1900"
		ai_chance = { factor = 60 }
		stability = 2
	}
}