
###Quest for the New World AI events (83101-03)###

country_event = {
	id = 83101

	trigger = {	
		tag = POR
		ai = yes
		NOT = { idea = quest_for_the_new_world }
		year = 1450
	}
	
	mean_time_to_happen = {
		months = 12
	}

	title = "EVTNAME83101"
	desc = "EVTDESC83101"
	
	option = {
		name = "EVTOPTA83101"
		add_idea = quest_for_the_new_world
	}
}

country_event = {
	id = 83102

	trigger = {	
		OR = {
			tag = CAS
			tag = SPA
		}
		ai = yes
		NOT = { idea = quest_for_the_new_world }
		year = 1477
	}
	
	mean_time_to_happen = {
		months = 12
	}

	title = "EVTNAME83101"
	desc = "EVTDESC83101"
	
	option = {
		name = "EVTOPTA83101"
		add_idea = quest_for_the_new_world
	}
}

country_event = {
	id = 83103

	trigger = {	
		OR = {
			tag = ENG
			tag = GBR
			tag = FRA
			tag = NED
			tag = HOL
		}
		ai = yes
		NOT = { idea = quest_for_the_new_world }
		year = 1534
	}
	
	mean_time_to_happen = {
		months = 12
	}

	title = "EVTNAME83101"
	desc = "EVTDESC83101"
	
	option = {
		name = "EVTOPTA83101"
		add_idea = quest_for_the_new_world
	}
}

###Colonial governance (83201-02)###

province_event = {
	id = 83201

	trigger = {	
		OR = {
			has_province_modifier = colonization_focus_1
			has_province_modifier = colonization_focus_2
			has_province_modifier = colonization_focus_3
		}
		is_colony = no
	}
	
	mean_time_to_happen = {
		months = 1
	}

	title = "EVTNAME83201"
	desc = "EVTDESC83201"
	
	option = {
		name = "EVTOPTA83201"
		remove_province_modifier = colonization_focus_1
		remove_province_modifier = colonization_focus_2
		remove_province_modifier = colonization_focus_3
	}
}

province_event = {
	id = 83202

	trigger = {	
		has_province_modifier = colonization_trade_point
		citysize = 200
	}
	
	mean_time_to_happen = {
		months = 1
	}

	title = "EVTNAME83202"
	desc = "EVTDESC83202"
	
	option = {
		name = "EVTOPTA83202"
		ai_chance = { factor = 75 }
		remove_province_modifier = colonization_trade_point
	}
	
	option = {
		name = "EVTOPTB83202"
		ai_chance = { factor = 25 }
		multiply_citysize = 0.5
		prestige = -0.01
	}
}

###Colonial changes (83301-02)###
province_event = {
	id = 83301

	trigger = {
		is_overseas = yes
		NOT = { any_neighbor_province = { owned_by = THIS } }
		any_neighbor_province = { 
			owner = {
				OR = {
					technology_group = western
					technology_group = eastern
				}
				num_of_cities = 25
				prestige = 0.25
			}
		}
	}
	
	mean_time_to_happen = {
		months = 3000
		
		modifier = {
			factor = 2
			is_colony = no
		}
		
		modifier = {
			factor = 1.2
			any_neighbor_province = { 
			owner = {
				ai = no
				OR = {
					technology_group = western
					technology_group = eastern
				}
				num_of_cities = 25
				prestige = 0.25
			}
		}
		}
		
		modifier = {
			factor = 1.3
			has_province_modifier = colonization_trade_point 
		}
		
		modifier = {
			factor = 1.1
			base_tax = 3
		}
		
		modifier = {
			factor = 1.1
			base_tax = 4
		}
		
		modifier = {
			factor = 1.1
			base_tax = 5
		}
		
		modifier = {
			factor = 1.1
			base_tax = 6
		}
		
		modifier = {
			factor = 1.1
			base_tax = 7
		}
		
		modifier = {
			factor = 0.9
			NOT = { citysize = 800 }
		}
		
		modifier = {
			factor = 0.8
			NOT = { citysize = 500 }
		}
		
		modifier = {
			factor = 0.5
			owner = { NOT = { num_of_cities = 8 } }
		}
		
		modifier = {
			factor = 0.75
			owner = { 
				num_of_cities = 8
				NOT = { num_of_cities = 12 } 
			}
		}
		
		modifier = {
			factor = 0.9
			owner = { 
				num_of_cities = 12
				NOT = { num_of_cities = 20 } 
			}
		}
		
		modifier = {
			factor = 0.9
			owner = { 
				num_of_cities = 12
				NOT = { num_of_cities = 20 } 
			}
		}
	}

	title = "EVTNAME83301"
	desc = "EVTDESC83301"
	
	immediate = {
		set_province_flag = colony_out_of_control
		any_neighbor_province = {
			limit = { 
				owner = {
					OR = {
						technology_group = western
						technology_group = eastern
					}
					num_of_cities = 25
					prestige = 0.25
				}
			}
			owner = { set_country_flag = dei_take_colony }
		}
	}
	
	option = {
		name = "EVTOPTA83301" #Let them take it
		ai_chance = { factor = 85 }
		any_neighbor_country = {
			limit = { has_country_flag = dei_take_colony }
			country_event = 83302
			clr_country_flag = dei_take_colony
		}
		prestige = -0.01
		badboy = -0.1		
	}
	
	option = {
		name = "EVTOPTB83301" #No way, this colony is our!
		ai_chance = { factor = 15 }
		any_neighbor_country = {
			limit = { has_country_flag = dei_take_colony }
			add_casus_belli = {	
				target = THIS
				type = cb_insult
				months = 12
			}
			relation = { who = THIS value = -75 }
			clr_country_flag = dei_take_colony
			any_owned = {
				any_neighbor_province = {
					limit = { has_province_flag = colony_out_of_control }
					clr_province_flag = colony_out_of_control
				}
			}
		}
		prestige = 0.01
		badboy = 0.1		
	}
}

country_event = {
	id = 83302

	is_triggered_only = yes
	
	title = "EVTNAME83302"
	desc = "EVTDESC83302"
	
	option = {
		name = "EVTOPTA83302" # We claim a new land
		ai_chance = { factor = 85 }
		any_neighbor_country = {
			limit = { 
				any_owned_province = {
					has_province_flag = colony_out_of_control
				}
			}
			add_casus_belli = {	
				target = THIS
				type = cb_defection
				months = 12
			}
			relation = { who = THIS value = -75 }
		}
		any_owned = {
			any_neighbor_province = {
				limit = { has_province_flag = colony_out_of_control }
				secede_province = THIS
				clr_province_flag = colony_out_of_control
			}
		}
		badboy = 1		
	}
	
	option = {
		name = "EVTOPTA83302"
		ai_chance = { factor = 15 }
		any_neighbor_country = {
			limit = { 
				any_owned_province = {
					has_province_flag = colony_out_of_control
				}
			}
			relation = { who = THIS value = 50 }			
		}
		any_owned = {
			any_neighbor_province = {
				limit = { has_province_flag = colony_out_of_control }
				clr_province_flag = colony_out_of_control
			}
		}
		badboy = -0.5
	}
}

###Send people to colony###

province_event = {
	id = 83401

	trigger = {	
		owner = { has_country_modifier = dei_colonial_policy }
		is_colony = yes
		NOT = { has_province_modifier = colonization_trade_point }
	}
	
	mean_time_to_happen = {
		months = 12
		
		modifier = {
			factor = 0.75
			has_province_modifier = colonization_focus_1
		}
		
		modifier = {
			factor = 0.5
			has_province_modifier = colonization_focus_2
		}
		
		modifier = {
			factor = 0.25
			has_province_modifier = colonization_focus_3
		}
		
		modifier = {
			factor = 0.8
			citysize = 250
		}
		
		modifier = {
			factor = 0.8
			citysize = 500
		}
		
		modifier = {
			factor = 0.8
			citysize = 750
		}
	}

	title = "EVTNAME83401"
	desc = "EVTDESC83401"
	
	option = {
		name = "EVTOPTA83401"
		owner = { remove_country_modifier = dei_colonial_policy }
		citysize = 100
	}
}

###Coloial buildings###

province_event = {
	id = 83501

	trigger = {	
		is_overseas = yes
		has_building = plantation_system
		OR = {
			trade_goods = tea
			trade_goods = spices
			trade_goods = coffee
			trade_goods = cotton
			trade_goods = sugar
			trade_goods = tobacco
		}
		NOT = { has_province_modifier = growth_of_plantations }
	}
	
	mean_time_to_happen = {
		months = 6
	}

	title = "EVTNAME83501"
	desc = "EVTDESC83501"
	
	option = {
		name = "EVTOPTA83501"
		owner = {
			any_owned = {
				limit = { 
					is_overseas = yes
					has_building = plantation_system
					OR = {
						trade_goods = tea
						trade_goods = spices
						trade_goods = coffee
						trade_goods = cotton
						trade_goods = sugar
						trade_goods = tobacco
					}
					NOT = { has_province_modifier = growth_of_plantations }
				}
				add_province_modifier = {
					name = "growth_of_plantations"
					duration = -1
				}
			}
		}
	}
}

province_event = {
	id = 83502

	trigger = {	
		is_overseas = yes
		has_building = advanced_mine
		OR = {
			trade_goods = iron
			trade_goods = copper
		}
		NOT = { has_province_modifier = expanded_mines_1 }
	}
	
	mean_time_to_happen = {
		months = 6
	}

	title = "EVTNAME83502"
	desc = "EVTDESC83502"
	
	option = {
		name = "EVTOPTA83502"
		owner = {
			any_owned = {
				limit = {
					is_overseas = yes
					has_building = advanced_mine
					OR = {
						trade_goods = iron
						trade_goods = copper
					}
					NOT = { has_province_modifier = expanded_mines_1 }
				}		
				add_province_modifier = {
					name = "expanded_mines_1"
					duration = -1
				}
			}
		}
	}
}

province_event = {
	id = 83503

	trigger = {	
		is_overseas = yes
		has_building = advanced_mine
		trade_goods = gold
		NOT = { has_province_modifier = expanded_mines_2 }
	}
	
	mean_time_to_happen = {
		months = 6
	}

	title = "EVTNAME83503"
	desc = "EVTDESC83503"
	
	option = {
		name = "EVTOPTA83503"
		owner = {
			any_owned = {
				limit = {
					is_overseas = yes
					has_building = advanced_mine
					trade_goods = gold
					NOT = { has_province_modifier = expanded_mines_2 }
				}		
				add_province_modifier = {
					name = "expanded_mines_2"
					duration = -1
				}
			}
		}
	}
}
