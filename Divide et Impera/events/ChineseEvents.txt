######################################
#
# Events for China. Id 11331-
#
######################################


#Pirates Detected
province_event = {

	id = 11331

	trigger = {
		region = chinese_coast
		KOR = {
			has_country_flag = wokou_pirates_defeated
		}
		owner = {
			NOT = { tag = KOR }
			NOT = { has_country_flag = wokou_warning }
		}
		NOT = { has_province_modifier = wokou_raids }
		port = yes
	}

	mean_time_to_happen = {
		months = 12
	}

	title = "EVTNAME11331"
	desc = "EVTDESC11331"

	option = {
		name = "EVTOPTA11331"		#Curse them both!
		ai_chance = { factor = 100 }
		owner = {
			set_country_flag = wokou_warning
			relation = { who = KOR value = -20 }
		}
	}
}

#Pirate Raid
province_event = {

	id = 11332

	trigger = {
		region = chinese_coast
		KOR = {
			has_country_flag = wokou_pirates_defeated
		}
		owner = { 
			has_country_flag = wokou_warning
			NOT = { has_country_flag = prepare_wokou_expedition }
			NOT = { has_country_flag = wokou_expedition }
			NOT = { has_country_flag = wokou_pirates_defeated }
 		}
		NOT = { has_province_modifier = wokou_raids }
		port = yes
	}

	mean_time_to_happen = {
		months = 24

		modifier = {
			factor = 4.0
			has_province_modifier = coastal_defense
		}
		modifier = {
			factor = 1.2
			units_in_province = 1
		}
		modifier = {
			factor = 1.5
			units_in_province = 3
		}
	}

	title = "EVTNAME11332"
	desc = "EVTDESC11332"

	option = {
		name = "EVTOPTA11332"		#Damn!
		ai_chance = { factor = 100 }
		set_province_flag = wokou_raids
		add_province_modifier = {
			name = "wokou_raids"
			duration = 180
		}

		#If raids on a province that has coastal defense
		set_province_flag = hidden_tooltip
		any_province = {
			limit = {
				has_province_flag = hidden_tooltip
				has_province_modifier = coastal_defense
				owner = {
					NOT = { has_country_flag = prepare_wokou_expedition }
				}
			}
			owner = { country_event = 11334 }
		}
		clr_province_flag = hidden_tooltip
	}
}

#Prepare Wokou Expedition
country_event = {

	id = 11334

	is_triggered_only = yes

	title = "EVTNAME11334"
	desc = "EVTDESC11334"

	option = {
		name = "EVTOPTA11334"		#Fund the preparations
		ai_chance = { factor = 70 }
		prestige = 0.05
		treasury = -50
		set_country_flag = prepare_wokou_expedition
	}
	option = {
		name = "EVTOPTB11334"		#It is not worth the trouble
		ai_chance = { factor = 30 }
		prestige = -0.1
		FROM = {
			revolt_risk = 3
		}
	}
}

#Wokou Expedition
country_event = {

	id = 11336

	trigger = {
		has_country_flag = prepare_wokou_expedition
		manpower = 2
	}

	mean_time_to_happen = {
		months = 12

		modifier = {
			factor = 0.5
			MIL = 7
		}
		modifier = {
			factor = 0.8
			ADM = 7
		}
		modifier = {
			factor = 2.0
			NOT = { MIL = 4 }
		}
		modifier = {
			factor = 1.5
			NOT = { ADM = 4 }
		}
	}

	title = "EVTNAME11336"
	desc = "EVTDESC11336"

	option = {
		name = "EVTOPTA11336"		#The time has come
		ai_chance = { factor = 70 }
		prestige = 0.05
		manpower = -1
		clr_country_flag = prepare_wokou_expedition
		set_country_flag = wokou_expedition
	}
	option = {
		name = "EVTOPTB11336"		#Our soldiers are needed elsewhere
		ai_chance = { factor = 30 }
		prestige = -0.1
		any_owned = {
			limit ={
				has_province_flag = wokou_raids
				revolt_risk = 2
			}
		}
		random_owned = {
			limit = {
				has_province_flag = wokou_raids
			}
			spawn_rebels = {
				type = anti_tax_rebels
				size = 2
			}
			owner = { clr_country_flag = prepare_wokou_expedition }
		}
	}
}

#Wokou Expedition Returns
country_event = {

	id = 11338

	trigger = {
		has_country_flag = wokou_expedition
	}

	mean_time_to_happen = {
		months = 24

		modifier = {
			factor = 0.7
			OR = {
				advisor = navigator
				advisor = great_naval_reformer
				advisor = grand_admiral
			}
		}
		modifier = {
			factor = 0.95
			MIL = 6
		}
		modifier = {
			factor = 0.8
			ADM = 7
		}
		modifier = {
			factor = 0.7
			MIL = 8
		}
		modifier = {
			factor = 2.0
			NOT = { MIL = 7 }
		}
	}

	title = "EVTNAME11338"
	desc = "EVTDESC11338"

	immediate = {
		clr_country_flag = wokou_expedition
		set_country_flag = wokou_pirates_defeated
	}

	option = {
		name = "EVTOPTA11338"		#This is truly good news!
		ai_chance = { factor = 100 }
		prestige = 0.1
		stability = 1
		any_owned = {
			limit = {
				has_province_flag = wokou_raids
			}
			revolt_risk = -1
		}
		random_list = {
			50 = {
				treasury = 25
				random_owned = {
					limit = {
						has_province_flag = wokou_raids
					}
					add_province_modifier = {
						name = "wokou_slaves_return"
						duration = 365
					}					
				}
			}
			25 = {
				treasury = 50
				random_owned = {
					limit = {
						has_province_flag = wokou_raids
					}
					add_province_modifier = {
						name = "wokou_slaves_return"
						duration = 365
					}					
				}
			}
			25 = {
				treasury = 100
				any_owned = {
					limit = {
						has_province_flag = wokou_raids
					}
					add_province_modifier = {
						name = "wokou_slaves_return"
						duration = 365
					}					
				}				
			}
		}
	}
}

#Wokou Expedition Returns
country_event = {

	id = 11340

	trigger = {
		has_country_flag = wokou_expedition
	}

	mean_time_to_happen = {
		months = 48

		modifier = {
			factor = 0.95
			NOT = { MIL = 6 }
		}
		modifier = {
			factor = 0.8
			NOT = { MIL = 5 }
		}
		modifier = {
			factor = 0.7
			NOT = { MIL = 4 }
		}
		modifier = {
			factor = 2.0
			MIL = 7
		}
	}

	title = "EVTNAME11340"
	desc = "EVTDESC11340"

	immediate = {
		clr_country_flag = wokou_expedition
	}

	option = {
		name = "EVTOPTA11340"		#Black news, indeed!
		ai_chance = { factor = 100 }
		prestige = -0.05
	}
}

province_event = {

	id = 11341
	
	trigger = {
		has_province_flag = construct_the_forbidden_city
		NOT = { has_province_modifier = "construct_the_forbidden_city" }
		owner = {
			OR = {
				tag = MCH
				tag = MNG
			}
		}
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	immediate = {
		clr_province_flag = construct_the_forbidden_city
		set_province_flag = the_forbidden_city
	}
	
	title = "EVTNAME11341"
	desc = "EVTDESC11341"
	
	option = {
		name = "EVTOPTA11341"
		add_province_modifier = {
				name = "the_forbidden_city"
				duration = -1
			}
	}
}
