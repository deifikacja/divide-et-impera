country_event = {

	id = 22001
	
	major = yes
	
	trigger = {
		is_religion_enabled = protestant
		government = monarchy
		is_lesser_in_union = no
		is_subject = no
		religion_years = { protestant = 5 }
		NOT = { has_global_flag = dei_act_of_supremacy }
		NOT = { has_country_flag = reformation_money }
		NOT = { idea = divine_supremacy }
		NOT = { idea = deus_vult }
		defender_of_faith = no
		religion = catholic
		excommunicated = yes
		NOT = { government = papal_government }
		num_of_cities = 3
	}
	
	mean_time_to_happen = {
		months = 360
		
		modifier = {
			factor = 0.6
			religion_years = { protestant = 15 }
		}
		modifier = {
			factor = 0.6
			religion_years = { protestant = 30 }
		}
		modifier = {
			factor = 0.5			
			advisor = theologian
		}
		modifier = {
			factor = 0.95
			NOT = {	secularism_theocracy = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = {	secularism_theocracy = -1 }
		}
		modifier = {
			factor = 0.8
			NOT = {	secularism_theocracy = -2 }
		}
		modifier = {
			factor = 0.7
			NOT = {	secularism_theocracy = -3 }
		}
		modifier = {
			factor = 0.6
			NOT = {	secularism_theocracy = -3 }
		}
		modifier = {
			factor = 1.2
			secularism_theocracy = 1
		}
		modifier = {
			factor = 1.3
			secularism_theocracy = 3
		}
		modifier = {
			factor = 1.5
			secularism_theocracy = 5
		}
		modifier = {
			factor = 0.9
			NOT = { relation = { who = PAP value = -50 } }
		}
		modifier = {
			factor = 0.8
			NOT = { relation = { who = PAP value = -100 } }
		}
		modifier = {
			factor = 0.7
			NOT = { relation = { who = PAP value = -150 } }
		}
		modifier = {
			factor = 0.75
			NOT = { centralization_decentralization = 0 }
		}
		modifier = {
			factor = 1.25
			centralization_decentralization = 2
		}
		modifier = {
			factor = 1.5
			centralization_decentralization = 3
		}
		modifier = {
			factor = 2
			centralization_decentralization = 4
		}
		modifier = {
			factor = 2
			NOT = { num_of_cities = 5 }
		}
		modifier = {
			factor = 0.7
			num_of_cities = 10
		}
		modifier = {
			factor = 10
			has_country_flag = reject_episcopal_reformation
		}
		modifier = {
			factor = 2
			OR = {
				culture_group = latin
				culture_group = iberian
			}
		}
	}
	
	title = "EVTNAME22001"
	desc = "EVTDESC22001"
	
	option = {
		name = "EVTOPTA22001"
		ai_chance = { factor = 75 }
		enable_religion = episcopalism
		religion = episcopalism
		capital_scope = { religion = episcopalism }
		centralization_decentralization = -1
		add_country_modifier = {
			name = "head_of_the_church"
			duration = -1
		}
		stability = -3
		years_of_income = 2.0
		set_country_flag = episcopal_head_of_the_church
		set_country_flag = act_of_supremacy
		set_country_flag = episcopal_reformation 
		set_country_flag = reformation_money
		set_global_flag = dei_act_of_supremacy
	}
	
	option = {
		name = "EVTOPTB22001"
		ai_chance = { factor = 25 }
		random_owned = { 
			religion = protestant
			heretic_rebels = 1
		}
		stability = -1
		relation = { who = PAP value = 100 }
		add_papal_influence = 0.03
		set_country_flag = reject_episcopal_reformation
	}
}

province_event = {

	id = 22002

	trigger = {
		is_religion_enabled = episcopalism
		religion = catholic 
		owner = { religion = episcopalism }
		NOT = { religion_years = { episcopalism = 50 } }
		NOT = { year = 1700 }
	}
	mean_time_to_happen = {
		months = 900

		# Positive modifiers
		modifier = {
			factor = 0.8
			any_neighbor_province = {
				religion = episcopalism
			}
		}
		modifier = {
			factor = 0.7
			any_neighbor_province = {
				religion = episcopalism
				culture_group = THIS
			}
		}
		modifier = {
			factor = 0.7
			NOT = { religion_years = { episcopalism = 15 } }
		}
		
		modifier = {
			factor = 0.8
			owner = { idea = deus_vult }
		}
		modifier = {
			factor = 0.75
			owner = { idea = church_attendance_duty	}
		}
		modifier = {
			factor = 0.9
			owner = { defender_of_faith = yes }
		}
		modifier = {
			factor = 0.9
			owner = { secularism_theocracy = 2 }
		}
		modifier = {
			factor = 0.85
			owner = { secularism_theocracy = 4 }
		}
		
		# Negative modifiers
		modifier = {
			factor = 1.5
			religion_years = { episcopalism = 20 }
		}
		modifier = {
			factor = 2
			religion_years = { episcopalism = 30 }
		}
		modifier = {
			factor = 3
			religion_years = { episcopalism = 40 }
		}
		modifier = {
			factor = 3.0
			is_overseas = yes
		}
	}

	title = "EVTNAME22002"
	desc = "EVTDESC22002"

	option = {
		name = "EVTOPTA22002"
		religion = episcopalism
	}
}

country_event = {

	id = 22003
	
	is_triggered_only = yes
	
	trigger = {
		has_country_flag = episcopal_head_of_the_church
	}
	
	title = "EVTNAME22003"
	desc = "EVTDESC22003"
	
	immediate = { 
		remove_country_modifier = head_of_the_church 
		clr_country_flag = episcopal_head_of_the_church 
	}
	
	option = {
		name = "EVTOPTA22003"
		ai_chance = { factor = 20 }
		religion = protestant
		random_owned = { 
			limit = { religion = episcopalism }
			religion = protestant
		}
		random_owned = { 
			limit = { religion = episcopalism }
			heretic_rebels = 1
		}
		random_owned = { 
			limit = { religion = catholic }
			heretic_rebels = 1
		}
		any_owned = {
			limit = { religion = episcopalism }
			revolt_risk = 5
		}
		any_owned = {
			limit = { religion = catholic }
			revolt_risk = 10
		}
		stability = -3	
	}	
	option = {
		name = "EVTOPTB22003"
		ai_chance = { factor = 70 }
		random_owned = { 
			limit = { religion = catholic }
			heretic_rebels = 1
		}
		random_owned = { 
			limit = { religion = catholic }
			heretic_rebels = 1
		}
		any_owned = {
			limit = { religion = catholic }
			revolt_risk = 10
		}
		stability = -2	
	}
	option = {
		name = "EVTOPTC22003"
		ai_chance = { factor = 10 }
		religion = catholic
		random_owned = { 
			limit = { religion = episcopalism }
			religion = catholic
		}
		random_owned = { 
			limit = { religion = episcopalism }
			heretic_rebels = 1
		}
		random_owned = { 
			limit = { religion = protestant }
			heretic_rebels = 1
		}
		any_owned = {
			limit = { religion = protestant }
			revolt_risk = 10
		}
		any_owned = {
			limit = { religion = catholic }
			revolt_risk = 10
		}
		stability = -2
		relation = { who = PAP value = 100 }
		set_country_flag = "reject_episcopal_reformation" 
	}	
}

country_event = {

	id = 22004
	
	is_triggered_only = yes
	
	trigger = {
		has_country_flag = "episcopal_reformation"
		has_country_flag = "reject_episcopal_reformation"
	}
	
	title = "EVTNAME22004"
	desc = "EVTDESC22004"
	
	option = {
		name = "EVTOPTA22004"
		ai_chance = { factor = 70 }
		religion = episcopalism
		random_owned = { 
			limit = { religion = catholic }
			religion = episcopalism
		}
		random_owned = { 
			limit = { religion = catholic }
			heretic_rebels = 1
		}
		any_owned = {
			limit = { religion = catholic }
			revolt_risk = 10
		}
		clr_country_flag = "reject_episcopal_reformation"
	}	
	option = {
		name = "EVTOPTB22004"
		ai_chance = { factor = 20 }
		add_country_modifier = {
			name = "religious_tolerance"
			duration = 5475
		}
		stability = 1
		prestige = -0.05
		clr_country_flag = "episcopal_reformation"
	}
	option = {
		name = "EVTOPTC22004"
		ai_chance = { factor = 20 }
		add_country_modifier = {
			name = "religious_intolerance"
			duration = 5475
		}
		stability = -1
		prestige = 0.05
		clr_country_flag = "episcopal_reformation"
	}	
}

country_event = {

	id = 22005

	trigger = {
		religion = episcopalism 
		has_country_flag = act_of_supremacy
		NOT = { has_country_modifier = head_of_the_church }
		NOT = { has_country_flag = reform_in_episcopalism }
		ADM = 6
	}
	mean_time_to_happen = {
		months = 60
		
		modifier = {
			factor = 0.5
			ADM = 8
		}
	}

	title = "EVTNAME22005"
	desc = "EVTDESC22005"

	option = {
		name = "EVTOPTA22005"
		treasury = -50
		add_country_modifier = {
			name = "episcopalism_established"
			duration = -1
		}
		add_country_modifier = {
			name = "reform_tensions"
			duration = 5475
		}
		secularism_theocracy = 1
		prestige = 0.05
		set_country_flag = reform_in_episcopalism
	}
	option = {
		name = "EVTOPTB22005"
		stability = 1
		set_country_flag = reform_in_episcopalism
	}
	option = {
		name = "EVTOPTC22005"
		add_country_modifier = {
			name = "support_monarch"
			duration = 5475
		}
		centralization_decentralization = -1
		set_country_flag = reform_in_episcopalism
		set_country_flag = dialog_with_anabaptism
	}
}

country_event = {

	id = 22006

	trigger = {
		NOT = { year = 1580 }
		religion_years = { protestant = 5 }
		NOT = { has_country_flag = dei_anabaptism }
		NOT = { has_country_flag = dei_evangelicalism }
		OR = {
			AND = { 
				religion = episcopalism 
				has_country_flag = dialog_with_anabaptism
				num_of_revolts = 2
			}
			AND = { 
				religion = catholic
				OR = {
					any_owned_province = { religion = protestant }
					any_owned_province = { religion = reformed }
					any_neighbor_country = { religion = protestant }
					any_neighbor_country = { religion = reformed }
					any_neighbor_country = { religion = episcopalism }
				}
				num_of_revolts = 1
				NOT = { stability = 1 }
			}
			AND = { 
				OR = {
					religion = protestant
					religion = reformed
				}
				num_of_revolts = 2
				NOT = { stability = 1 }
			}
		}
	}
	mean_time_to_happen = {
		months = 6000
		
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.8
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 0.7
			NOT = { year = 1535 }
		}
		modifier = {
			factor = 0.8
			war = yes
		}
		modifier = {
			factor = 3
			any_neighbor_country = { has_country_flag = dei_anabaptism }
		}
		modifier = {
			factor = 2
			OR = {
				religion = catholic
				religion = protestant
				religion = reformed
			}
			num_of_cities = 5
		}
		modifier = {
			factor = 0.1
			has_country_flag = peasant_war
		}
	}

	title = "EVTNAME22006"
	desc = "EVTDESC22006"

	option = {
		name = "EVTOPTA22006"
		enable_religion = episcopalism
		stability = -5
		random_owned = {
			limit = { owner = { religion = catholic } }
			owner = {
				religion = episcopalism
				years_of_income = 2.0
				set_country_flag = "reformation_money"
			}
		}
		random_owned = { religion = episcopalism }
		aristocracy_plutocracy = 5
		serfdom_freesubjects = 5
		secularism_theocracy = 3
		centralization_decentralization = 2
		quality_quantity = 2
		innovative_narrowminded = -1
		add_country_modifier = {
			name = "anabaptism_movement"
			duration = -1
		}
		any_owned = { revolt_risk = 10 }
		random_owned = {
			limit = { religion = catholic }
			heretic_rebels = 1
		}
		random_owned = {
			limit = { religion = catholic }
			heretic_rebels = 1
		}
		infamy = 15
		set_country_flag = dei_anabaptism
		
	}
	option = {
		name = "EVTOPTB22006"
		enable_religion = episcopalism
		stability = -2
		random_owned = { 
			religion = episcopalism
			revolt_risk = 7
			heretic_rebels = 2
		}
		serfdom_freesubjects = -1
		centralization_decentralization = -1
		any_owned = { revolt_risk = 3 }
		set_country_flag = dei_anabaptism
	}
}

country_event = {

	id = 22007

	trigger = {
		religion_years = { episcopalism = 15 }
		NOT = { has_country_flag = dei_evangelicalism }
		religion = episcopalism 
		OR = {
			has_country_modifier = anabaptism_movement
			has_country_modifier = dei_pietism
			tag = USA
		}
	}
	
	mean_time_to_happen = {
		months = 480
		
		modifier = {
			factor = 0.2
			has_country_modifier = anabaptism_movement
		}
	}
	
	title = "EVTNAME22007"
	desc = "EVTDESC22007"
	
	immediate = { 
		random_owned = {
			limit = { owner = { has_country_modifier = anabaptism_movement  } }
			owner = {
				aristocracy_plutocracy = -3
				serfdom_freesubjects = -3
				secularism_theocracy = -2
			}
		}
		remove_country_modifier = anabaptism_movement
	}

	option = {
		name = "EVTOPTA22007"
		secularism_theocracy = 1
		add_country_modifier = {
			name = "dei_evangelicalism"
			duration = -1
		}
		remove_country_modifier = dei_pietism
		set_country_flag = dei_evangelicalism
		
	}
	option = {
		name = "EVTOPTB22007"
		centralization_decentralization = -1
		secularism_theocracy = -1
		any_owned = { revolt_risk = 5 }
		set_country_flag = dei_evangelicalism	
	}
}

country_event = {

	id = 22008

	trigger = {
		OR = {
			AND = { 
				year = 1670
				religion = protestant
			}
			AND = { 
				year = 1700
				religion = episcopalism
			}
		}
		NOT = { secularism_theocracy = 0 }
		NOT = { has_country_flag = dei_pietism }
		stability = 2
	}
	
	mean_time_to_happen = {
		months = 240
		
		modifier = {
			factor = 2
			NOT = { secularism_theocracy = 1 }
		}
		modifier = {
			factor = 1.5
			religion = episcopalism
		}
	}
	
	title = "EVTNAME22008"
	desc = "EVTDESC22008"

	option = {
		name = "EVTOPTA22008"
		add_country_modifier = {
			name = "dei_pietism"
			duration = -1
		}
		set_country_flag = dei_pietism
	}
	option = {
		name = "EVTOPTB22008"
		secularism_theocracy = -1
		set_country_flag = dei_pietism	
	}
}

country_event = {

	id = 22009

	trigger = {
		war = no
		has_country_modifier = evangelic_war
	}
	
	mean_time_to_happen = {
		months = 3
	}
	
	title = "EVTNAME22009"
	desc = "EVTDESC22009"

	option = {
		name = "EVTOPTA22009"
		remove_country_modifier = evangelic_war
	}
}