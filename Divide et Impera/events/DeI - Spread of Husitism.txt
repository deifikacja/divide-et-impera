province_event = {

	id = 55500

	trigger = {
		is_religion_enabled = hussite
		has_global_flag = husitte_won
		OR = { religion = catholic religion = episcopalism }
		NOT = { year = 1700 }
		NOT = { 
			province_id = 118 # Rome
			owner = { government = papal_government } 
		}
	}
	mean_time_to_happen = {
		months = 1000

		# Positive modifiers
		modifier = {
			factor = 0.6
			any_neighbor_province = {
				religion = hussite
			}
		}
		modifier = {
			factor = 0.6
			any_neighbor_province = {
				religion = hussite
				culture_group = THIS
			}
		}
	
		modifier = {
			factor = 0.75
			has_province_modifier = "heresy"
		}
		modifier = {
			factor = 0.75
			owner = { hussite = 0 } # Tolerance to hussites
		}
		
		modifier = {
			factor = 0.8
			owner = { 
				religion = hussite
				idea = deus_vult
			}
		}
		modifier = {
			factor = 0.8
			owner = { 
				religion = hussite
				idea = church_attendance_duty
			}
		}
		modifier = {
			factor = 0.8
			owner = { 
				religion = hussite
				defender_of_faith = yes
			}
		}
		modifier = {
			factor = 0.6
			NOT = { religion_years = { hussite = 20 } }
			owner = { 
				religion = hussite
			}
			any_neighbor_province = {
				religion = hussite
			}
		}
		
		modifier = {
			factor = 0.8
			NOT = { religion_years = { hussite = 40 } }
			owner = { 
				religion = hussite
			}
			any_neighbor_province = {
				religion = hussite
			}
		}
		
		modifier = {
			factor = 0.9
			owner = {
				NOT = {	innovative_narrowminded = -2 }
			}
		}
		modifier = {
			factor = 0.9
			owner = {
				NOT = {	innovative_narrowminded = -4 }
			}
		}
		
		modifier = {
			factor = 0.9
			owner = {
				serfdom_freesubjects = 2
			}
		}
		modifier = {
			factor = 0.9
			owner = {
				serfdom_freesubjects = 4
			}
		}
		modifier = {
			factor = 0.8
			owner = { religion = episcopalism }
			religion = catholic
		}
		
		# Negative modifiers
		modifier = {
			factor = 5
			religion = episcopalism
		}
		modifier = {
			factor = 1.1
			owner = {
				NOT = { serfdom_freesubjects = -2 }
			}
		}
		modifier = {
			factor = 1.2
			owner = {
				NOT = { serfdom_freesubjects = -4 }
			}
		}
		modifier = {
			factor = 1.2
			owner = {
				innovative_narrowminded = 2
			}
		}
		modifier = {
			factor = 1.4
			owner = {
				innovative_narrowminded = 4
			}
		}
		modifier = {
			factor = 2
			religion_years = { hussite = 100 }
		}
		
		modifier = {
			factor = 2
			owner = { 
				NOT = { religion = hussite }
				idea = divine_supremacy
			}
		}
		modifier = {
			factor = 2
			owner = { 
				NOT = { religion = hussite }
				idea = deus_vult
			}
		}
		modifier = {
			factor = 2
			owner = { 
				NOT = { religion = hussite }
				idea = church_attendance_duty
			}
		}
		modifier = {
			factor = 2
			owner = { 
				NOT = { religion = hussite }
				defender_of_faith = yes
			}
		}
		modifier = {
			factor = 2.0
			owner = { 
				NOT = { religion = hussite }
				government = theocratic_government 
			}
		}
		modifier = {
			factor = 2.0
			NOT = { owner = { religion_group = christian } }
		}
		modifier = {
			factor = 4.0
			owner = { government = papal_government }
		}
		modifier = {
			factor = 4.0
			owner = { religion = protestant }
		}
		modifier = {
			factor = 4.0
			owner = { religion = reformed }
		}
		modifier = {
			factor = 8.0
			is_overseas = yes
		}
		modifier = {
			factor = 1.5
			culture_group = germanic
		}
		modifier = {
			factor = 2
			culture_group = british
		}
		modifier = {
			factor = 1.75
			culture_group = scandinavian
		}
		modifier = {
			factor = 0.9
			culture_group = baltic
		}
		modifier = {
			factor = 0.75
			culture_group = west_slavic
		}
		modifier = {
			factor = 0.8
			culture_group = danubo_carpatian
		}
		modifier = {
			factor = 2
			culture_group = french
		}
		modifier = {
			factor = 1.5
			culture_group = netherlands
		}
		modifier = {
			factor = 2
			culture_group = latin
		}
		modifier = {
			factor = 2.5
			culture_group = iberian
		}
		modifier = {
			factor = 0.8
			culture_group = east_slavic
		}
		modifier = {
			factor = 0.9
			culture_group = byzantine
		}
		modifier = {
			factor = 6.0
			owner = { has_country_modifier = counter_reformation }
		}
		
	}

	title = "EVTNAME55500"
	desc = "EVTDESC55500"

	option = {
		name = "EVTOPTA55500"
		religion = hussite
		add_province_modifier = {
			name = "religious_zeal_at_conv"
			duration = 7500
		}
	}
}


province_event = {
	id = 55501

	trigger = {
		is_religion_enabled = hussite
		has_global_flag = husitte_won
		religion = orthodox
		NOT = { year = 1700 }
		any_neighbor_province = {
			religion = hussite
		}
	}
	mean_time_to_happen = {
		months = 1100

		# Positive modifiers
	
		modifier = {
			factor = 0.75
			has_province_modifier = "heresy"
		}
		modifier = {
			factor = 0.75
			owner = { hussite = 0 } # Tolerance to hussites
		}
		
		modifier = {
			factor = 0.8
			owner = { 
				religion = hussite
				idea = deus_vult
			}
		}
		modifier = {
			factor = 0.8
			owner = { 
				religion = hussite
				idea = church_attendance_duty
			}
		}
		modifier = {
			factor = 0.8
			owner = { 
				religion = hussite
				defender_of_faith = yes
			}
		}
		
		modifier = {
			factor = 0.9
			owner = {
				NOT = {	innovative_narrowminded = -2 }
			}
		}
		modifier = {
			factor = 0.9
			owner = {
				NOT = {	innovative_narrowminded = -4 }
			}
		}
		
		modifier = {
			factor = 0.9
			owner = {
				serfdom_freesubjects = 2
			}
		}
		modifier = {
			factor = 0.9
			owner = {
				serfdom_freesubjects = 4
			}
		}
		modifier = {
			factor = 0.85
			owner = {
				secularism_theocracy = 2
				religion = hussite
			}
		}
		modifier = {
			factor = 0.85
			owner = {
				secularism_theocracy = 4
				religion = hussite
			}
		}
		
		# Negative modifiers
		modifier = {
			factor = 1.1
			owner = {
				NOT = { serfdom_freesubjects = -2 }
			}
		}
		modifier = {
			factor = 1.2
			owner = {
				NOT = { serfdom_freesubjects = -4 }
			}
		}
		modifier = {
			factor = 1.2
			owner = {
				innovative_narrowminded = 2
			}
		}
		modifier = {
			factor = 1.4
			owner = {
				innovative_narrowminded = 4
			}
		}
		modifier = {
			factor = 1.2
			owner = {
				secularism_theocracy = 2
				NOT = { religion = hussite }
			}
		}
		modifier = {
			factor = 1.2
			owner = {
				secularism_theocracy = 4
				NOT = { religion = hussite }
			}
		}
		modifier = {
			factor = 2
			religion_years = { hussite = 100 }
		}
		modifier = {
			factor = 2
			owner = { 
				NOT = { religion = hussite }
				idea = divine_supremacy
			}
		}
		modifier = {
			factor = 2
			owner = { 
				NOT = { religion = hussite }
				idea = deus_vult
			}
		}
		modifier = {
			factor = 2
			owner = { 
				NOT = { religion = hussite }
				idea = church_attendance_duty
			}
		}
		modifier = {
			factor = 2
			owner = { 
				NOT = { religion = hussite }
				defender_of_faith = yes
			}
		}
		modifier = {
			factor = 2.0
			owner = { 
				NOT = { religion = hussite }
				government = theocratic_government 
			}
		}
		modifier = {
			factor = 4.0
			owner = { religion = protestant }
		}
		modifier = {
			factor = 4.0
			owner = { religion = reformed }
		}
		modifier = {
			factor = 6.0
			owner = { has_country_modifier = counter_reformation }
		}
	}

	title = "EVTNAME55500"
	desc = "EVTDESC55500"

	option = {
		name = "EVTOPTA55500"
		religion = hussite
		add_province_modifier = {
			name = "religious_zeal_at_conv"
			duration = 7500
		}
	}
}