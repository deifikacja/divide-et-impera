# Bosphorus toll � coastal black sea provinces
country_event = {
	id = 15000
	
	title = "EVTNAME15000"
	desc = "EVTDESC15000"
	
	trigger = {
		has_country_modifier = bosphorous_sound_toll
		or = {
			325	= { owned_by = this not = {	has_province_modifier = black_sea_trade } }			# Kastamon
			328	= { owned_by = this not = {	has_province_modifier = black_sea_trade } }			# Sinope
			330	= { owned_by = this not = {	has_province_modifier = black_sea_trade } }			# Trebizon
			422	= { owned_by = this not = {	has_province_modifier = black_sea_trade } }			# Imereti
			462	= { owned_by = this not = {	has_province_modifier = black_sea_trade } }			# Georgia
			1314 = { owned_by = this not = { has_province_modifier = black_sea_trade } }		# Abkhazia
			2171 = { owned_by = this not = { has_province_modifier = black_sea_trade } }		# Taman
			1918 = { owned_by = this not = { has_province_modifier = black_sea_trade } }		# Burgas
			159	= { owned_by = this not = {	has_province_modifier = black_sea_trade } }			# Silistria
			1263 = { owned_by = this not = { has_province_modifier = black_sea_trade } }		# Bujak
			282	= { owned_by = this not = {	has_province_modifier = black_sea_trade } }			# Cherson
			284	= { owned_by = this not = {	has_province_modifier = black_sea_trade } }			# Crimea
			285	= { owned_by = this not = {	has_province_modifier = black_sea_trade } }			# Mangup
			2170 = { owned_by = this not = { has_province_modifier = black_sea_trade } }		# Kaffa
			286	= { owned_by = this not = {	has_province_modifier = black_sea_trade } }			# Azow
			287	= { owned_by = this not = {	has_province_modifier = black_sea_trade } }			# Kouban
		}
	}
	
	mean_time_to_happen = {	months = 3 }
	
	option = {
		name = "EVTOPTA15000"
		set_country_flag = black_sea_trade
		# possible performance gains for the removal event
		# rather than using 'any_owned_province'
		any_owned = {
			limit = {
				not = { has_province_modifier = black_sea_trade }
				or = {
					province_id = 325				# Kastamon
					province_id = 328				# Sinope
					province_id = 330				# Trebizon
					province_id = 422				# Imereti
					province_id = 462				# Georgia
					province_id = 1314				# Abkhazia
					province_id = 2171				# Taman
					province_id = 1918				# Burgas
					province_id = 159				# Silistria
					province_id = 1263				# Bujak
					province_id = 282				# Cherson
					province_id = 283				# Zaporozhia
					province_id = 284				# Crimea
					province_id = 285				# Mangup
					province_id = 2170				# Kaffa
					province_id = 286				# Azow
					province_id = 287				# Kouban
				}
			}
			add_province_modifier = {
				name = "black_sea_trade"
				duration = -1
			}
		}
	}
}

# clears out the tolls when the Bosphorus isn't controlled anymore
country_event = {
	id = 15001
	
	title = "EVTNAME15001"
	desc = "EVTDESC15001"
	
	trigger = {
		has_country_flag = black_sea_trade
		not = { has_country_modifier = bosphorous_sound_toll }
	}
	
	mean_time_to_happen = { months = 3 }
	
	option = {
		name = "EVTOPTA15001"	
		remove_province_modifier = black_sea_trade				# every owned province is affected
		clr_country_flag = black_sea_trade
	}
}

# Sund toll � coastal and inner Baltic sea provinces
country_event = {
	id = 15002
	
	title = "EVTNAME15002"
	desc = "EVTDESC15002"
	
	trigger = {
		has_country_modifier = sound_toll
		or = {
			1 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Stockholm
			2 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Ostergotland
			3 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Smaland
			25 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Gotland
			9 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Halsingland
			11 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Vasterbotten
			19 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Osterbotten
			27 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Finland
			28 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Nyland
			29 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Tavastaland
			30 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Viborg
			31 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Savolax
			32 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Kexholm
			310 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Novgorod
			33 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Neva
			34 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Ingermanland
			35 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Osel
			36 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Estland
			37 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Livland
			38 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Riga
			39 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Kurland
			40 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Memel
			271 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Samogitia
			41 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# OstPreussen
			42 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Warmia
			43 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Danzig
			48 = { owned_by = this not = {	has_province_modifier = baltic_sea_trade } }				# Hinterpommern
		}
	}
	
	mean_time_to_happen = {	months = 3 }
	
	option = {
		name = "EVTOPTA15002"
		set_country_flag = baltic_sea_trade			
		# possible performance gains for the removal event
		# rather than using 'any_owned_province'
		any_owned = {
			limit = {
				not = { has_province_modifier = baltic_sea_trade }
				or = {
					province_id = 1 									# Stockholm
					province_id = 2                                     # Ostergotland
					province_id = 3                                     # Smaland
					province_id = 25                                    # Gotland
					province_id = 9                                     # Halsingland
					province_id = 11                                    # Vasterbotten
					province_id = 19                                    # Osterbotten
					province_id = 27                                    # Finland
					province_id = 28                                    # Nyland
					province_id = 29                                    # Tavastaland
					province_id = 30                                    # Viborg
					province_id = 310                                   # Novgorod
					province_id = 33                                    # Neva
					province_id = 34                                    # Ingermanland
					province_id = 35                                    # Osel
					province_id = 36                                    # Estland
					province_id = 37                                    # Livland
					province_id = 38                                    # Riga
					province_id = 273                                   # Wenden
					province_id = 39                                    # Kurland
					province_id = 40                                    # Memel
					province_id = 271                                   # Samogitia
					province_id = 41                                    # OstPreussen
					province_id = 42                                    # Warmia
					province_id = 43                                    # Danzig
					province_id = 1252                                  # WestPreussen
					province_id = 2156                                  # Stolp
					province_id = 48                                    # Hinterpommern
				}
			}				
			add_province_modifier = {
				name = "baltic_sea_trade"
				duration = -1
			}
		}
	}
}

# clears out the tolls when the Sund isn't controlled anymore
country_event = {
	id = 15003
	
	title = "EVTNAME15003"
	desc = "EVTDESC15003"
	
	trigger = {
		has_country_flag = baltic_sea_trade
		not = { has_country_modifier = sound_toll }
	}
	
	mean_time_to_happen = { months = 3 }
	
	option = {
		name = "EVTOPTA15003"	
		remove_province_modifier = baltic_sea_trade				# every owned province is affected
		clr_country_flag = baltic_sea_trade
	}
}

country_event = {
	id = 15004
	
	title = "EVTNAME15004"
	desc = "EVTDESC15004"
	
	trigger = {
		any_country = {
			has_country_modifier = sound_toll
		}
		45 = { not = { has_province_modifier = free_shipping_through_the_sound } }
		capital_scope = {										# these countries have direct access to Lubeck
			not = {
				region = northeastern_germany
				region = old_saxony
				region = sweden
				region = finland
				region = lithuania
				region = wielkopolska
				region = russian_region
			}
		}
		not = { has_country_modifier = sound_toll }
		any_center_of_trade = {
			or = {
				province_id = 45 							# Stockholm
				province_id = 1 							# Stockholm
				province_id = 2                             # Ostergotland
				province_id = 3                             # Smaland
				province_id = 25                            # Gotland
				province_id = 9                             # Halsingland
				province_id = 11                            # Vasterbotten
				province_id = 19                            # Osterbotten
				province_id = 27                            # Finland
				province_id = 28        	                # Nyland
				province_id = 29                            # Tavastaland
				province_id = 30                            # Viborg
				province_id = 310                           # Novgorod
				province_id = 33                            # Neva
				province_id = 34                            # Ingermanland
				province_id = 35                            # Osel
				province_id = 36                            # Estland
				province_id = 37                            # Livland
				province_id = 38                            # Riga
				province_id = 273                           # Wenden
				province_id = 39                            # Kurland
				province_id = 40                            # Memel
				province_id = 271                           # Samogitia
				province_id = 41                            # OstPreussen
				province_id = 42              	            # Warmia
				province_id = 43                	        # Danzig
				province_id = 1252                          # WestPreussen
				province_id = 2156                          # Stolp
				province_id = 48                            # Hinterpommern
			}
			placed_merchants = 2
			cot_value = 1000
		}
		not = {
			any_country = {
				has_country_modifier = sound_toll
				trade_agreement_with = this
			}
		}
	}
	
	mean_time_to_happen = { 
		months = 72
		modifier = {
			factor = 1.5
			any_country = {
				has_country_modifier = sound_toll
				relation = { who = this value = 125 }
			}
		}
		modifier = {
			factor = 0.9
			trade_income_percentage = 0.35
		}
		modifier = {
			factor = 0.8
			monthly_income = 100
			trade_income_percentage = 0.5
		}
		modifier = {
			factor = 0.8
			or = {
				45 = { placed_merchants = 3 }		# special cases for Lubeck and Novgorod
				310 = { placed_merchants = 3 }
			}
		}
		modifier = {
			factor = 0.8
			any_country = {
				has_country_modifier = sound_toll
				not = { mercantilism_freetrade = -3 }
			}
		}
		modifier = {
			factor = 0.7
			any_country = {
				has_country_modifier = sound_toll
				trade_embargoing = this								# tk check this
			}
		}
		modifier = {
			factor = 0.6
			any_country = {
				has_country_modifier = sound_toll
				war_with = this
			}
		}
	}

	option = {
		name = "EVTOPTA15004"
		ai_chance = { 
			factor = 35
			modifier = {
				factor = 2
				merchants = 3						# available merchants
			}
			modifier = {
				factor = 3
				aristocracy_plutocracy = 2			# cheap merchants
			}
		}
		random_center_of_trade = {
			limit = {
				or = {
					province_id = 45 							# Stockholm
					province_id = 1 							# Stockholm
					province_id = 2                             # Ostergotland
					province_id = 3                             # Smaland
					province_id = 25                            # Gotland
					province_id = 9                             # Halsingland
					province_id = 11                            # Vasterbotten
					province_id = 19                            # Osterbotten
					province_id = 27                            # Finland
					province_id = 28        	                # Nyland
					province_id = 29                            # Tavastaland
					province_id = 30                            # Viborg
					province_id = 310                           # Novgorod
					province_id = 33                            # Neva
					province_id = 34                            # Ingermanland
					province_id = 35                            # Osel
					province_id = 36                            # Estland
					province_id = 37                            # Livland
					province_id = 38                            # Riga
					province_id = 273                           # Wenden
					province_id = 39                            # Kurland
					province_id = 40                            # Memel
					province_id = 271                           # Samogitia
					province_id = 41                            # OstPreussen
					province_id = 42              	            # Warmia
					province_id = 43                	        # Danzig
					province_id = 1252                          # WestPreussen
					province_id = 2156                          # Stolp
					province_id = 48                            # Hinterpommern
				}
			}
			remove_merchant = this
			remove_merchant = this
		}
	}
	
	option = {
		name = "EVTOPTB15004"
		ai_chance = { 
			factor = 65
			modifier = {
				factor = 0
				not = { years_of_income = 0.08 }
			}
			modifier = {
				factor = 2
				not = { merchants = 1 }				# lack of merchants
			}
			modifier = {
				factor = 2
				trade_income_percentage = 0.35
			}
			modifier = {
				factor = 3
				or = {
					45 = { cot_value = 1350 placed_merchants = 2 }
					310 = { cot_value = 1350 placed_merchants = 2 }
				}
			}
		}
		years_of_income = -0.05
		any_country = {
			limit = { has_country_modifier = sound_toll }
			country_event = 15005
		}
	}
}
			
country_event = {
	id = 15005
	
	title = "EVTNAME15005"
	desc = "EVTDESC15005"
	
	is_triggered_only = yes
	
	option = {
		name = "EVTOPTA15005"
		any_country = {
			limit = {
				tag = this
				not = { trade_efficiency = 0.35 }
			}
			treasury = 25
		}
		any_country = {
			limit = {
				tag = this
				trade_efficiency = 0.35
				not = { trade_efficiency = 0.5 }
			}
			treasury = 35
		}
		any_country = {
			limit = {
				tag = this
				trade_efficiency = 0.5
				not = { trade_efficiency = 0.75 }
			}
			treasury = 50
		}
		any_country = {
			limit = {
				tag = this
				trade_efficiency = 0.75
			}
			treasury = 75
		}
	}
}