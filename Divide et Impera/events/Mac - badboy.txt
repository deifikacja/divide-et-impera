
country_event = {

id = 10400

	trigger = { 
		badboy = 1
		NOT = { badboy = 1.5 }
		NOT = { government = despotic_monarchy }
		NOT = { stability = 1 }
		}
	mean_time_to_happen = {
		months = 600
		modifier = {
			factor = 1.25
		 	stability = 0
		}
		modifier = {
			factor = 0.8
		 	NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.8
		 	NOT = { stability = -2 }
		}

	}

	title = "EVTNAME10400"
	desc = "EVTDESC10400"
	option = {
		name = "EVTOPTA10400"
		add_country_modifier = {
			name = "wojna_domowa"
			duration = 720
			}	
		random_owned = { 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 16 } } 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 20 } } 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 24 } } 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 28 } } 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 32 } } 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 36 } } 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 40 } } 		
			create_revolt = 3
			change_controller = REB
			}
		}

}


country_event = {

id = 10401

	trigger = { 
		badboy = 1.5
		NOT = { stability = 1 }
		}
	mean_time_to_happen = {
		months = 600
		modifier = {
			factor = 1.25
		 	stability = 0
		}
		modifier = {
			factor = 0.8
		 	NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.8
		 	NOT = { stability = -2 }
		}
		modifier = {
			factor = 0.8
		 	badboy = 2.0
		}
		modifier = {
			factor = 0.8
		 	badboy = 2.5
		}
		modifier = {
			factor = 0.8
		 	badboy = 3.0
		}
		modifier = {
			factor = 0.8
		 	badboy = 3.5
		}
		modifier = {
			factor = 0.8
		 	badboy = 4.0
		}
	}

	title = "EVTNAME10400"
	desc = "EVTDESC10400"
	option = {
		name = "EVTOPTA10400"
		add_country_modifier = {
			name = "wojna_domowa"
			duration = 720
			}	
		random_owned = { 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 8 } } 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 10 } } 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 12 } } 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 14 } } 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 16 } } 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 18 } } 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 20 } } 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 22 } } 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 24 } } 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 26 } } 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 28 } } 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 30 } } 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 32 } } 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 34 } } 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 36 } } 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 38 } } 		
			create_revolt = 3
			change_controller = REB
			}
		random_owned = { 
			limit = { owner = { num_of_cities = 40 } } 		
			create_revolt = 3
			change_controller = REB
			}
		}

}