country_event = {

	id = 600
	
	is_triggered_only = yes
	
	title = "EVTNAME600"
	desc = "EVTDESC600"
	
	option = {
		name = "EVTOPTB600A"
		random_owned = {
			pretender_rebels = 2
		}
		random_owned = {
			limit = {
				NOT = { is_core = THIS }
			}
			nationalist_rebels = 1
		}
		random_owned = {
			limit = {
				NOT = { is_core = THIS }
			}
			nationalist_rebels = 1
		}
		random_owned = {
			limit = {
				NOT = { is_core = THIS }
			}
			nationalist_rebels = 1
		}
		random_list = {
			25 = {	
				add_country_modifier = {
					name = "tribal_succession_crisis_0"
					duration = 730
				}
			}	
			60 = {	
				add_country_modifier = {
					name = "tribal_succession_crisis"
					duration = 730
				}
			}
			15 = {	
				add_country_modifier = {
					name = "tribal_succession_crisis_2"
					duration = 730
				}
			}
		}
	}
}