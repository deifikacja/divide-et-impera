#####Revolution Events######

country_event = {

	id = 6601
	
	major = yes
	
	trigger = {
		has_country_flag = revolution
		government_tech = 53
		revolution_target_exists = no 
		capital_scope = { continent = europe } 
		num_of_cities = 10
		OR = {
			NOT = { tag = FRA }
			exists = RFR
		}
	}

	mean_time_to_happen = {
		months = 24
		modifier = {
			factor = 0.9
			has_country_flag = religious_revolution
		}
		modifier = {
			factor = 0.9
			has_country_flag = serfdom
		}
		modifier = {
			factor = 0.9
			has_country_flag = social_reform
		}
		modifier = {
			factor = 0.9
			has_country_flag = royal_bureaucracy
		}
		modifier = {
			factor = 0.9
			has_country_flag = deplorable_reign
		}
		modifier = {
			factor = 0.9
			has_country_flag = road_to_bankruptcy
		}
		modifier = {
			factor = 0.9
			has_country_flag = financial_crisis
		}
	}

	title = "EVTNAME6601"
	desc = "EVTDESC6601"

	option = {
		name = "EVTOPTA6601"
		set_revolution_target = THIS
		serfdom_freesubjects = 10
		quality_quantity = -10
		land_naval = -10
		offensive_defensive = -10
		mercantilism_freetrade = -10
		innovative_narrowminded = -10
		centralization_decentralization	= -10
		secularism_theocracy = -10
		imperialism_isolationism = -10
		aristocracy_plutocracy = 10
		clr_country_flag = revolution
		clr_country_flag = religious_revolution
		clr_country_flag = serfdom
		clr_country_flag = social_reform
		clr_country_flag = royal_bureaucracy
		clr_country_flag = deplorable_reign
		clr_country_flag = road_to_bankruptcy
		clr_country_flag = financial_crisis
		stability = 6
		government = revolutionary_republic
		kill_ruler = THIS
		country_event = 8301
	}
}


country_event = {

	id = 6602
	
	trigger = {
		war = yes
		is_revolution_target = yes
		war_exhaustion = 1
	}
	
	mean_time_to_happen = {
		months = 24
		modifier = {
			factor = 0.95
			war_exhaustion = 2
		}
		modifier = {
			factor = 0.95
			war_exhaustion = 3
		}
		modifier = {
			factor = 0.95
			war_exhaustion = 4
		}
		modifier = {
			factor = 0.95
			war_exhaustion = 5
		}
		modifier = {
			factor = 0.95
			war_exhaustion = 6
		}
		modifier = {
			factor = 0.95
			war_exhaustion = 7
		}
		modifier = {
			factor = 0.95
			war_exhaustion = 8
		}
		modifier = {
			factor = 0.95
			war_exhaustion = 9
		}
		modifier = {
			factor = 0.95
			war_exhaustion = 10
		}
	}

	title = "EVTNAME6602"
	desc = "EVTDESC6602"

	option = {

		name = "EVTNAME6602"
		war_exhaustion = -1
	}
}


country_event = {

	id = 6603
	
	trigger = {
		capital_scope = { continent = europe }
		OR = {
			government = absolute_monarchy
			government = enlightened_despotism
			government = despotic_monarchy
			government = feudal_monarchy
			government = administrative_monarchy
			government = constitutional_monarchy
			government = bureaucratic_despotism
			government = imperial_government
			government = merchant_republic
		}
		revolution_target = {
			war_with = THIS
		}  
		war_exhaustion = 1
	}
	
	mean_time_to_happen = {
		months = 24
		modifier = {
			factor = 0.95
			war_exhaustion = 2
		}
		modifier = {
			factor = 0.95
			war_exhaustion = 3
		}
		modifier = {
			factor = 0.95
			war_exhaustion = 4
		}
		modifier = {
			factor = 0.95
			war_exhaustion = 5
		}
		modifier = {
			factor = 0.95
			war_exhaustion = 6
		}
		modifier = {
			factor = 0.95
			war_exhaustion = 7
		}
		modifier = {
			factor = 0.95
			war_exhaustion = 8
		}
		modifier = {
			factor = 0.95
			war_exhaustion = 9
		}
		modifier = {
			factor = 0.95
			war_exhaustion = 10
		}
	}

	title = "EVTNAME6603"
	desc = "EVTDESC6603"

	option = {
		name = "EVTNAME6603"
		war_exhaustion = -1
	}
}

country_event = {

	id = 6604

	trigger = {
		is_revolution_target = yes
		NOT = { tag = RFR }
		capital_scope = {
			NOT = {
				controlled_by = THIS
				controlled_by = REB
			}
		}
	}

	mean_time_to_happen = {
		months = 1
	}

	title = "EVTNAME6604"
	desc = "EVTDESC6604"

	

	option = {

		name = "EVTNAME6604"
		prestige = -0.50
		war_exhaustion = 3
		government = constitutional_monarchy
		kill_ruler = THIS
		country_event = 8301 
		set_revolution_target = xxx
		capital_scope = {	
			controller = {
				country_event = 6605
			}
		}
	}

}

country_event = {
		
	id = 6605

	is_triggered_only = yes

	title = "EVTNAME6605"
	desc = "EVTDESC6605"

	option = {
		
		name = "EVTNAME6605"
		stability = 6
		prestige = 2
	}
}

country_event = {

	id = 6606
	trigger = {
		tag = FRA
		NOT = { exists = RFR}
		has_country_flag = revolution
		government_tech = 53
		revolution_target_exists = no 
		capital_scope = { continent = europe } 
		num_of_cities = 10
	}

	mean_time_to_happen = {
		months = 24
		modifier = {
			factor = 0.9
			has_country_flag = religious_revolution
		}
		modifier = {
			factor = 0.9
			has_country_flag = serfdom
		}
		modifier = {
			factor = 0.9
			has_country_flag = social_reform
		}
		modifier = {
			factor = 0.9
			has_country_flag = royal_bureaucracy
		}
		modifier = {
			factor = 0.9
			has_country_flag = deplorable_reign
		}
		modifier = {
			factor = 0.9
			has_country_flag = road_to_bankruptcy
		}
		modifier = {
			factor = 0.9
			has_country_flag = financial_crisis
		}
	}

	title = "EVTNAME6606"
	desc = "EVTDESC6606"

	option = {
		name = "EVTOPTA6606"
		serfdom_freesubjects = 10
		quality_quantity = -10
		land_naval = -10
		offensive_defensive = -10
		mercantilism_freetrade = -10
		innovative_narrowminded = -10
		centralization_decentralization	= -10
		aristocracy_plutocracy = 10
		clr_country_flag = revolution
		clr_country_flag = religious_revolution
		clr_country_flag = serfdom
		clr_country_flag = social_reform
		clr_country_flag = royal_bureaucracy
		clr_country_flag = deplorable_reign
		clr_country_flag = road_to_bankruptcy
		clr_country_flag = financial_crisis	
		stability = 6
		government = revolutionary_republic
		change_tag = RFR
		set_revolution_target = RFR
		random_country = {
			limit = {
				tag = RFR
			} 
			country_event = 6608
		}	
	}
}

country_event = {

	id = 6607

	trigger = {
		tag = RFR
		NOT = { exists = FRA }
		capital_scope = {
			NOT = {
				controlled_by = THIS
				controlled_by = REB
			}
		}
	}

	mean_time_to_happen = {
		months = 1
	}

	title = "EVTNAME6607"
	desc = "EVTDESC6607"

	

	option = {

		name = "EVTOPTA6607"
		prestige = -0.50
		war_exhaustion = 3
		government = constitutional_monarchy
		set_revolution_target = xxx
		change_tag = FRA
		FRA = {
			country_event = 6609
		}	
		capital_scope = {	
			controller = {
				country_event = 6605
			}
		}
	}

}

country_event = {
		
	id = 6608

	is_triggered_only = yes

	title = "EVTNAME6608"
	desc = "EVTDESC6608"

	option = {
		name = "EVTOPTA6608"
		kill_ruler = THIS
		country_event = 8301
	}
}

country_event = {
		
	id = 6609

	is_triggered_only = yes

	title = "EVTNAME6609"
	desc = "EVTDESC6609"

	option = {
		name = "EVTOPTA6609"
		kill_ruler = THIS
		country_event = 8301
	}
}

country_event = {
	
	id = 6610

	trigger = {
		tag = RFR
		NOT = {
			OR = {
				government = revolutionary_republic
				government = revolutionary_empire
			}
		}
	}

	mean_time_to_happen = {
		months = 36
	}

	title = "EVTNAME6610"
	desc = "EVTDESC6610"

	option = {
		name = "EVTOPTA6610"
		government = revolutionary_republic
		kill_ruler = THIS
		country_event = 8301
		aristocracy_plutocracy = 10
		stability = 3
	}
}
				