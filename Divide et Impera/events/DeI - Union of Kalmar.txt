country_event = {

	id = 1100

	trigger = {
		tag = KAL
		primary_culture = norwegian
		NOT = { exists = DAN }
		NOT = { has_country_flag = after_union_1 }
	}

	mean_time_to_happen = {
		months = 3
	}

	title = "EVTNAME1100"
	desc = "EVTDESC1100"

	option = {
		name = "EVTOPTA1100"
		ai_chance = { factor = 60 }
		release_vassal = DAN
		stability = 1
		set_country_flag = after_union_1
	}

	option = {
		name = "EVTOPTB1100"
		ai_chance = { factor = 40 }
		12 = { nationalist_rebels = 4 }
		13 = { nationalist_rebels = 2 }
		14 = { nationalist_rebels = 2 }
		15 = { nationalist_rebels = 3 }
		6 = { nationalist_rebels = 1 }
		random = { 
			chance = 50 
			26 = { nationalist_rebels = 1 }
		}
		stability = -2
		treasury = -100
		set_country_flag = after_union_1
	}
}

country_event = {

	id = 1101

	trigger = {
		tag = KAL
		primary_culture = norwegian
		NOT = { exists = SWE }
		NOT = { has_country_flag = after_union_2 }
	}

	mean_time_to_happen = {
		months = 3
	}

	title = "EVTNAME1101"
	desc = "EVTDESC1101"

	option = {
		name = "EVTOPTA1116"
		ai_chance = { factor = 60 }
		release_vassal = SWE
		stability = 1
		set_country_flag = after_union_2
	}

	option = {
		name = "EVTOPTB1101"
		ai_chance = { factor = 40 }
		1 = { nationalist_rebels = 3 }
		2 = { nationalist_rebels = 2 }
		3 = { nationalist_rebels = 2 }
		4 = { nationalist_rebels = 2 }
		5 = { nationalist_rebels = 1 }
		7 = { nationalist_rebels = 1 }
		9 = { nationalist_rebels = 1 }
		random = { 
			chance = 50 
			27 = { nationalist_rebels = 2 }
		}
		stability = -2
		treasury = -100
		set_country_flag = after_union_2
	}
}

country_event = {

	id = 1116

	trigger = {
		tag = KAL
		primary_culture = danish
		NOT = { exists = SWE }
		NOT = { has_country_flag = after_union_1 }
	}

	mean_time_to_happen = {
		months = 3
	}

	title = "EVTNAME1116"
	desc = "EVTDESC1116"

	option = {
		name = "EVTOPTA1116"
		ai_chance = { factor = 60 }
		release_vassal = SWE
		stability = 1
		set_country_flag = after_union_1
	}

	option = {
		name = "EVTOPTB1116"
		ai_chance = { factor = 40 }
		1 = { nationalist_rebels = 3 }
		2 = { nationalist_rebels = 2 }
		3 = { nationalist_rebels = 2 }
		4 = { nationalist_rebels = 2 }
		5 = { nationalist_rebels = 1 }
		7 = { nationalist_rebels = 1 }
		9 = { nationalist_rebels = 1 }
		random = { 
			chance = 50 
			27 = { nationalist_rebels = 1 }
		}
		stability = -2
		treasury = -100
		set_country_flag = after_union_1
	}
}

country_event = {

	id = 1117

	trigger = {
		tag = KAL
		primary_culture = danish
		NOT = { exists = NOR }
		NOT = { has_country_flag = after_union_2 }
	}

	mean_time_to_happen = {
		months = 6
	}

	title = "EVTNAME1117"
	desc = "EVTDESC1117"

	option = {
		name = "EVTOPTA1117"
		ai_chance = { factor = 60 }
		release_vassal = NOR
		stability = 1
		set_country_flag = after_union_2
	}

	option = {
		name = "EVTOPTB1117"
		ai_chance = { factor = 40 }
		17 = { nationalist_rebels = 2 }
		20 = { nationalist_rebels = 2 }
		22 = { nationalist_rebels = 1 }
		23 = { nationalist_rebels = 1 }
		random = { 
			chance = 50 
			24 = { nationalist_rebels = 1 }
		}
		stability = -1
		treasury = -60
		set_country_flag = after_union_2
	}
}

country_event = {

	id = 1118

	trigger = {
		tag = KAL
		primary_culture = swedish
		NOT = { exists = DAN }
		NOT = { has_country_flag = after_union_1 }
	}

	mean_time_to_happen = {
		months = 3
	}

	title = "EVTNAME1118"
	desc = "EVTDESC1118"

	option = {
		name = "EVTOPTA1118"
		ai_chance = { factor = 60 }
		release_vassal = DAN
		stability = 1
		set_country_flag = after_union_1
	}

	option = {
		name = "EVTOPTB1118"
		ai_chance = { factor = 40 }
		12 = { nationalist_rebels = 4 }
		13 = { nationalist_rebels = 2 }
		14 = { nationalist_rebels = 2 }
		15 = { nationalist_rebels = 3 }
		6 = { nationalist_rebels = 1 }
		random = { 
			chance = 50 
			26 = { nationalist_rebels = 1 }
		}
		stability = -2
		treasury = -100
		set_country_flag = after_union_1
	}
}

country_event = {

	id = 1119

	trigger = {
		tag = KAL
		primary_culture = swedish
		NOT = { exists = NOR }
		NOT = { has_country_flag = after_union_2 }
	}

	mean_time_to_happen = {
		months = 6
	}

	title = "EVTNAME1119"
	desc = "EVTDESC1119"

	option = {
		name = "EVTOPTA1119"
		ai_chance = { factor = 60 }
		release_vassal = NOR
		stability = 1
		set_country_flag = after_union_2
	}

	option = {
		name = "EVTOPTB1119"
		ai_chance = { factor = 40 }
		17 = { nationalist_rebels = 2 }
		20 = { nationalist_rebels = 2 }
		22 = { nationalist_rebels = 1 }
		23 = { nationalist_rebels = 1 }
		random = { 
			chance = 50 
			24 = { nationalist_rebels = 1 }
		}
		stability = -1
		treasury = -60
		set_country_flag = after_union_2
	}
}