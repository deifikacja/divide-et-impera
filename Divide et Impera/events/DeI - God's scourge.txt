country_event = {
	id = 4200

	trigger = {
		has_country_modifier = gods_scourge
		any_neighbor_country = { 
			any_owned_province = { 
				controlled_by = THIS 
				any_neighbor_province = { owned_by = THIS }
			}
		}
	}
	
	mean_time_to_happen = {
		months = 6
		
		modifier = {
			factor = 2
			NOT = { MIL = 6 }
		}
		modifier = {
			factor = 2
			NOT = { prestige = 0 }
		}
		
	}

	title = "EVTNAME4200"
	desc = "EVTDESC4200"
	
	option = {
		name = "EVTOPTA4200"
		random_country = {
			limit = { any_owned_province = { controlled_by = THIS any_neighbor_province = { owned_by = THIS } }	}
			random_owned = {
				limit = { controlled_by = THIS any_neighbor_province = { owned_by = THIS } }
				multiply_citysize = 0.92
				secede_province = THIS
			}
		}
		badboy = 0.25
	}
	option = {
		name = "EVTOPTB4200"
		random_country = {
			limit = { any_owned_province = { controlled_by = THIS any_neighbor_province = { owned_by = THIS } }	}
			random_owned = {
				limit = { controlled_by = THIS any_neighbor_province = { owned_by = THIS } }
				multiply_citysize = 0.85
			}
		}
		badboy = 0.25
		treasury = 50
		capital_scope = { citysize = 250 }
	}
}

country_event = {
	id = 4201

	trigger = {
		has_country_flag = dei_gods_scourge
		NOT = { has_country_modifier = gods_scourge }
		NOT = { MIL = 7 }
	}
	
	mean_time_to_happen = {
		months = 6
		
		modifier = {
			factor = 0.5
			NOT = { MIL = 5 }
		}
		modifier = {
			factor = 0.5
			NOT = { prestige = 0 }
		}
		
	}

	title = "EVTNAME4201"
	desc = "EVTDESC4201"
	
	option = {
		name = "EVTOPTA4201"
		stability = -3
		centralization_decentralization = 1
		add_country_modifier = {
			name = "noble_revolt"
			duration = 1825
		}
		clr_global_flag = dei_gods_scourge_exists
		clr_country_flag = dei_gods_scourge
	}
}