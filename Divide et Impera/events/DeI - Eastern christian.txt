country_event = {

	id = 90001
	
	trigger = {
		religion = greek_orthodox
		owns = 147 #Salonica (with Athos)
		147 = { has_province_modifier = dei_renew_athos_privileges }
	}
	
	mean_time_to_happen = {
		months = 600
		
		modifier = {
			factor = 0.8
			defender_of_faith = yes
		}
		modifier = {
			factor = 2.0
			NOT = { secularism_theocracy = 0 }
		}
		modifier = {
			factor = 1.2
			NOT = { secularism_theocracy = 1 }
		}
		modifier = {
			factor = 0.9
			secularism_theocracy = 2
		}
		modifier = {
			factor = 0.85
			secularism_theocracy = 3
		}
		modifier = {
			factor = 0.8
			secularism_theocracy = 4
		}
		modifier = {
			factor = 0.75
			secularism_theocracy = 5
		}		
		modifier = {
			factor = 0.7
			government = theocratic_government
		}		
	}
	
	title = "EVTNAME90001"
	desc = "EVTDESC90001"
	
	option = {
		name = "EVTOPTA90001"
		ai_chance = { factor = 65 }
		treasury = -50
		prestige = 0.05
		stability = 1
		any_country = {
			limit = { 
				religion = greek_orthodox 
				neighbour = THIS 
			}
			relation = { who = THIS value = 30 }
		}
		any_country = {
			limit = { 
				religion_group = christian 
				neighbour = THIS 
			}
			relation = { who = THIS value = -25 }
		}
		random_country = {
				limit = { tag = PAP }
				relation = { who = THIS value = -100 }
		}
		147 = {
			remove_province_modifier = dei_renew_athos_privileges
			add_province_modifier = {
				name = "dei_great_athos_privileges"
				duration = 9125 
			}
		}		
	}
	option = {
		name = "EVTOPTB90001"
		ai_chance = { factor = 20 }
		prestige = -0.02
	}
	option = {
		name = "EVTOPTC90001"
		ai_chance = { factor = 15 }
		prestige = -0.05
		secularism_theocracy = -1
		any_country = {
			limit = { 
				religion = greek_orthodox 
				neighbour = THIS 
			}
			relation = { who = THIS value = -25 }
		}
		any_country = {
			limit = { 
				religion_group = christian 
				neighbour = THIS 
			}
			relation = { who = THIS value = 30 }
		}
		random_country = {
				limit = { tag = PAP }
				relation = { who = THIS value = 100 }
		}
		147 = {	
			remove_province_modifier = dei_renew_athos_privileges 
			revolt_risk = 5
		}
	}
}

country_event = {

	id = 90002
	
	trigger = {
		religion = greek_orthodox
		has_country_modifier = local_council_done
		has_country_modifier = dei_autocephaly_established
	}
	
	mean_time_to_happen = {
		months = 2400
		
		modifier = {
			factor = 0.6
			owns = 147
			OR = { 
				147 = { has_province_modifier = dei_renew_athos_privileges }
				147 = { has_province_modifier = dei_great_athos_privileges }
			}
		}
		modifier = {
			factor = 0.85
			defender_of_faith = yes
		}
		modifier = {
			factor = 0.95
			prestige = 0.25
		}
		modifier = {
			factor = 0.9
			prestige = 0.5
		}	
		modifier = {
			factor = 0.8
			government = theocratic_government
		}	
	}
	
	title = "EVTNAME90002"
	desc = "EVTDESC90002"
	
	option = {
		name = "EVTOPTA90002"
		badboy = -1
		prestige = 0.15
		any_country = {
			limit = { 
				religion_group = eastern_christian 
				neighbour = THIS 
			}
			relation = { who = THIS value = 15 }
		}
		remove_country_modifier = local_council_done
	}
}

country_event = {

	id = 90003
	
	trigger = {
		religion = greek_orthodox
		has_country_modifier = local_council_done
		has_country_modifier = dei_autocephaly_established
	}
	
	mean_time_to_happen = {
		months = 2400
		
		modifier = {
			factor = 0.6
			owns = 147
			OR = { 
				147 = { has_province_modifier = dei_renew_athos_privileges }
				147 = { has_province_modifier = dei_great_athos_privileges }
			}
		}
		modifier = {
			factor = 0.85
			defender_of_faith = yes
		}
		modifier = {
			factor = 0.95
			secularism_theocracy = 2
		}
		modifier = {
			factor = 0.9
			secularism_theocracy = 3
		}
		modifier = {
			factor = 0.9
			secularism_theocracy = 4
		}
		modifier = {
			factor = 0.9
			secularism_theocracy = 5
		}
		modifier = {
			factor = 1.25
			NOT = { secularism_theocracy = 1 }
		}
	}
	
	title = "EVTNAME90003"
	desc = "EVTDESC90003"
	
	option = {
		name = "EVTOPTA90003"
		add_country_modifier = {
			name = "conflict_with_patriarch"
			duration = 2555
		}
		remove_country_modifier = local_council_done
	}
}

country_event = {

	id = 90004
	
	trigger = {
		religion = greek_orthodox
		OR = {
			tag = BYZ
			AND = {
				owns = 151
				NOT = { has_country_modifier = dei_autocephaly_established }
			}
		}
		NOT = { has_country_modifier = dei_great_patriarch }
	}
	
	mean_time_to_happen = {
		months = 1800
		
		modifier = {
			factor = 0.9
			owns = 147
			OR = { 
				147 = { has_province_modifier = dei_renew_athos_privileges }
				147 = { has_province_modifier = dei_great_athos_privileges }
			}
		}
		modifier = {
			factor = 1.4
			has_country_flag = dei_great_patriarch
		}
		modifier = {
			factor = 0.95
			secularism_theocracy = 2
		}
		modifier = {
			factor = 0.95
			secularism_theocracy = 3
		}
		modifier = {
			factor = 0.95
			secularism_theocracy = 4
		}
		modifier = {
			factor = 0.95
			secularism_theocracy = 5
		}
		modifier = {
			factor = 1.25
			NOT = { secularism_theocracy = 1 }
		}
	}
	
	title = "EVTNAME90004"
	desc = "EVTDESC90004"
	
	option = {
		name = "EVTOPTA90004"
		stability = 1
		add_country_modifier = {
			name = "dei_great_patriarch"
			duration = 7300
		}
		set_country_flag = dei_great_patriarch
	}
}

country_event = {

	id = 90005
	
	trigger = {
		religion = greek_orthodox
		owns = 151
		has_country_modifier = dei_autocephaly_established
		NOT = { has_country_modifier = conflict_with_patriarch }
	}
	
	mean_time_to_happen = {
		months = 18
	}
	
	title = "EVTNAME90005"
	desc = "EVTDESC90005"
	
	option = {
		name = "EVTOPTA90005"
		stability = -1
		add_country_modifier = {
			name = "conflict_with_patriarch"
			duration = 1825
		}
		set_country_flag = two_patriarchs
	}
}

country_event = {

	id = 90006
	
	trigger = {
		religion = greek_orthodox
		OR = {
			tag = SER
			tag = BUL
		}
		NOT = { has_country_flag = autocephaly_in_Serbia_Bulgaria }
	}
	
	mean_time_to_happen = {
		months = 1
	}
	
	title = "EVTNAME90006"
	desc = "EVTDESC90006"
	
	option = {
		name = "EVTOPTA90006"
		add_country_modifier = {
				name = "dei_autocephaly_established"
				duration = -1
		}
		set_country_flag = autocephaly_in_Serbia_Bulgaria
	}
}