# Chile's quest for independence
country_event = {

	id = 1031
	
	trigger = {
		NOT = { exists = CHL }
		NOT = { has_country_flag = chilean_nationalism }
		CHL = {
			NOT = {
				OR = {
					is_core = 782		# Tierra del Fuego
					is_core = 783		# Patagonia Occidental
					is_core = 784		# Huilimapu
					is_core = 787		# Pincunmapu
					is_core = 792		# Copiapo
					is_core = 793		# Atacama
					is_core = 796		# Arica
				}
			}
		}		
		OR = {
			owns = 784		# Huilimapu
			owns = 787		# Pincunmapu
			owns = 792		# Copiapo
			owns = 793		# Atacama
		}
		year = 1750
	}
	
	mean_time_to_happen = {
		months = 480

		modifier = {
			factor = 0.9
			NOT = { centralization_decentralization = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { centralization_decentralization = -2 }
		}
		modifier = {
			factor = 0.9
			NOT = { centralization_decentralization = -4 }
		}
		modifier = {
			factor = 1.1
			centralization_decentralization = 1
		}
		modifier = {
			factor = 1.1
			centralization_decentralization = 3
		}
		modifier = {
			factor = 1.1
			centralization_decentralization = 5
		}
		modifier = {
			factor = 0.9
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -1 }
		}
		modifier = {
			factor = 0.9
			NOT = { stability = -2 }
		}
		modifier = {
			factor = 1.1
			stability = 1
		}
		modifier = {
			factor = 1.1
			stability = 2
		}
		modifier = {
			factor = 1.1
			stability = 3
		}		
		modifier = {
			factor = 0.9
			year = 1760
		}
		modifier = {
			factor = 0.8
			year = 1780
		}
	}
	
	title = "EVTNAME1031"
	desc = "EVTDESC1031"
	
	option = {
		name = "EVTOPTA1031"
		set_country_flag = chilean_nationalism
		CHL = {
			add_core = 782		# Tierra del Fuego
			add_core = 783		# Patagonia Occidental
			add_core = 784		# Huilimapu
			add_core = 787		# Pincunmapu
			add_core = 792		# Copiapo
			add_core = 793		# Atacama
			add_core = 796		# Arica
		}
		colonists = -1
		random_owned = {
			limit = { is_overseas = yes }
			colonial_rebels = 1
		}
	}
}


# Colonial assembly
province_event = {

	id = 1035
	
	trigger = {
		is_core = CHL
		revolt_risk = 3
		NOT = { exists = CHL }
		owner = {
			has_country_flag = chilean_nationalism
		}
		NOT = { has_province_modifier = loyalist_stronghold }
	}
	
	mean_time_to_happen = {
		months = 60

		modifier = {
			factor = 0.9
			owner = { NOT = { stability = -1 } }
		}
		modifier = {
			factor = 0.9
			owner = { NOT = { stability = -2 } }
		}
		modifier = {
			factor = 0.9
			owner = { NOT = { advisor = statesman } }
		}
		modifier = {
			factor = 0.9
			owner = { NOT = { ADM = 4 } }
		}
		modifier = {
			factor = 0.9
			owner = { NOT = { DIP = 4 } }
		}
		modifier = {
			factor = 1.1
			owner = { ADM = 8 }
		}
		modifier = {
			factor = 1.1
			owner = { DIP = 8 }
		}
		modifier = {
			factor = 1.1
			owner = { advisor = statesman }
		}
		#for the domino effect..
		modifier = {
			factor = 1.25
			NOT = { exists = VNZ }
		}
		modifier = {
			factor = 1.25
			NOT = { exists = CAM }
		}
		modifier = {
			factor = 1.25
			NOT = { exists = QUE }
		}
		modifier = {
			factor = 1.25
			NOT = { exists = PEU }
		}		
		modifier = {
			factor = 1.25
			NOT = { exists = PRG }
		}
		modifier = {
			factor = 1.25
			NOT = { exists = MEX }
		}		
		modifier = {
			factor = 1.25
			NOT = { exists = LOU }
		}		
		modifier = {
			factor = 1.25
			NOT = { exists = LAP }
		}		
		modifier = {
			factor = 1.25
			NOT = { exists = HAT }
		}		
		modifier = {
			factor = 1.25
			NOT = { exists = COL }
		}		
		modifier = {
			factor = 1.25
			NOT = { exists = CHL }
		}	
		modifier = {
			factor = 1.25
			NOT = { exists = CAN }
		}			
		modifier = {
			factor = 1.25
			NOT = { exists = BRZ }
		}		
		modifier = {
			factor = 2.0
			NOT = { exists = USA }
		}		
	}
	
	title = "EVTNAME1023"
	desc = "EVTDESC1023"
	
	option = {
		name = "EVTOPTA1023"		# Attempt to find a diplomatic solution
		ai_chance = { 
			factor = 1
			modifier = {
				factor = 0
				owner = { has_country_modifier = diplomatic_negotiation }
			}
		}
		owner = {
			clr_country_flag = chilean_nationalism
			add_country_modifier = {
				name = "diplomatic_negotiation"
				duration = -1
			}
		}
	}
		
	option = {
		name = "EVTOPTB1023"		# The days of diplomacy are over
		ai_chance = { factor = 100 }
		owner = { stability = -1 }
		add_province_modifier = {
			name = "colonial_tensions"
			duration = -1
		}
	}
}