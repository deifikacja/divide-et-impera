#Start
country_event = {
	id = 150001
	
	trigger = {
		idea = quest_for_the_new_world
		num_of_ports = 1 
		ai = yes
		NOT = { has_country_flag = discoveries_start }
	}

	#MTT
	mean_time_to_happen = {
		months = 120
		modifier = {
			factor = 0.9
			idea = merchant_adventures
		}
		modifier = {
			factor = 0.9
			land_naval = 0
		}
		modifier = {
			factor = 0.9
			land_naval = 2
		}
		modifier = {
			factor = 0.9
			land_naval = 4
		}
		modifier = {
			factor = 0.95
			idea = grand_navy
		}
		modifier = {
			factor = 1.1
			NOT = {
				land_naval = 0
			}
		}
		modifier = {
			factor = 1.1
			NOT = {
				land_naval = -2
			}
		}
		modifier = {
			factor = 1.1
			NOT = {
				land_naval = -4
			}
		}
		modifier = {
			factor = 1.5
			culture_group = british
		}
		modifier = {
			factor = 1.1
			culture_group = french
		}
		modifier = {
			factor = 0.8
			culture_group = iberian
		}
	}
	
	title = "EVTNAME150001"
	desc = "EVTDESC150001"
	
	immediate = { set_country_flag = discoveries_start }
	
	#Portuguese
	option = {
		name = "EVTOPTA150001"
		ai_chance = { 
			factor = 34
			modifier = {
				factor = 2
				primary_culture = portugese
			}
			modifier = {
				factor = 2.1
				culture_group = netherlands
			}
			modifier = {
				factor = 0.3
				culture_group = british
			}
			modifier = {
				factor = 0.3
				culture_group = french
			}
		}
		west_african_coast = { discover = yes }
		south_africa = { discover = yes }
		set_country_flag = discoveries_portugal
	}
	
	#Spanish
	option = {
		name = "EVTOPTB150001"
		ai_chance = { 
			factor = 33
			modifier = {
				factor = 3
				culture_group = iberian
			}
			modifier = {
				factor = 3
				culture_group = british
			}
			modifier = {
				factor = 0.5
				culture_group = netherlands
			}
			modifier = {
				factor = 0.3
				primary_culture = portugese
			}
			modifier = {
				factor = 0.3
				culture_group = french
			}
		}
		the_carribean = { discover = yes }
		gujana_region = { discover = yes }
		set_country_flag = discoveries_spain
	}
	#French
	option = {
		name = "EVTOPTC150001"
		ai_chance = { factor = 33 
		modifier = {
			factor = 0.3
			culture_group = iberian
		}
		modifier = {
			factor = 0.9
			culture_group = british
		}
		modifier = {
			factor = 0.3
			culture_group = netherlands
		}
		modifier = {
			factor = 0.1
			primary_culture = portugese
		}
		modifier = {
			factor = 3
			culture_group = french
		}
		}
		eastern_north_america = { discover = yes }
		hudson_bay_coast = { discover = yes }
		set_country_flag = discoveries_french
	}	
}
#Phase 1
country_event = {
	id = 150002
	
	trigger = {
		idea = quest_for_the_new_world
		num_of_ports = 1
		ai = yes
		NOT = { has_country_flag = discoveries_phase_1 }
	}

	#MTT
	mean_time_to_happen = {
		months = 120
		modifier = {
			factor = 0.9
			idea = merchant_adventures
		}
		modifier = {
			factor = 0.9
			land_naval = 0
		}
		modifier = {
			factor = 0.9
			land_naval = 2
		}
		modifier = {
			factor = 0.9
			land_naval = 4
		}
		modifier = {
			factor = 0.95
			idea = grand_navy
		}
		modifier = {
			factor = 1.1
			NOT = {
				land_naval = 0
			}
		}
		modifier = {
			factor = 1.1
			NOT = {
				land_naval = -2
			}
		}
		modifier = {
			factor = 1.1
			NOT = {
				land_naval = -4
			}
		}
		modifier = {
			factor = 1.5
			culture_group = british
		}
		modifier = {
			factor = 1.1
			culture_group = french
		}
		modifier = {
			factor = 0.9
			culture_group = iberian
		}
	}
	
	title = "EVTNAME150002"
	desc = "EVTDESC150002"
	
	immediate = { set_country_flag = discoveries_phase_1 }
	
	#Portugease
	option = {
		name = "EVTOPTA150002"
		ai_chance = { factor = 25
		modifier = {
			factor = 8
			culture_group = netherlands
		}
		modifier = {
			factor = 2
			has_country_flag = discoveries_portugal
		}
		modifier = {
			factor = 0.5
			has_country_flag = discoveries_spain
		}
		modifier = {
			factor = 0.5
			has_country_flag = discoveries_french
		}
		}
		indian_coast = { discover = yes }
		eastasian_trade_ports = { discover = yes }
		brazil_region = { discover = yes }
		set_country_flag = discoveries_portugal
	}
	
	#Spanish
	option = {
		name = "EVTOPTB150002"
		ai_chance = { factor = 25
		modifier = {
			factor = 0.25
			culture_group = netherlands
		}
		modifier = {
			factor = 0.5
			has_country_flag = discoveries_portugal
		}
		modifier = {
			factor = 2
			has_country_flag = discoveries_spain
		}
		modifier = {
			factor = 0.5
			has_country_flag = discoveries_french
		}
		}
		the_spanish_main= { discover = yes }
		florida = { discover = yes }
		central_american_region = { discover = yes }
		set_country_flag = discoveries_spain
	}
	
	#French
	option = {
		name = "EVTOPTC150002"
		ai_chance = { factor = 25
		modifier = {
			factor = 0.25
			culture_group = netherlands
		}
		modifier = {
			factor = 0.5
			has_country_flag = discoveries_portugal
		}
		modifier = {
			factor = 0.5
			has_country_flag = discoveries_spain
		}
		modifier = {
			factor = 4
			has_country_flag = discoveries_french
		}
		}
		gujana_region = { discover = yes }
		florida = { discover = yes }
		the_mississippi_region = { discover = yes }
		set_country_flag = discoveries_french
	}
	
	#British
	option = {
		name = "EVTOPTD150002"
		ai_chance = { factor = 25
		modifier = {
			factor = 8
			culture_group = british
		}
		modifier = {
			factor = 0.5
			has_country_flag = discoveries_portugal
		}
		modifier = {
			factor = 0.5
			has_country_flag = discoveries_spain
		}
		modifier = {
			factor = 0.5
			has_country_flag = discoveries_french
		}
		}
		the_thirteen_colonies = { discover = yes }
		set_country_flag = discoveries_britain
	}
	
}
#Phase 2
country_event = {
	id = 150003

	trigger = {
		idea = quest_for_the_new_world
		num_of_ports = 1
		ai = yes
		NOT = { has_country_flag = discoveries_phase_2 }
	}

	#MTT
	mean_time_to_happen = {
		months = 120
		modifier = {
			factor = 0.9
			idea = merchant_adventures
		}
		modifier = {
			factor = 0.9
			land_naval = 0
		}
		modifier = {
			factor = 0.9
			land_naval = 2
		}
		modifier = {
			factor = 0.9
			land_naval = 4
		}
		modifier = {
			factor = 0.95
			idea = grand_navy
		}
		modifier = {
			factor = 1.1
			NOT = {
				land_naval = 0
			}
		}
		modifier = {
			factor = 1.1
			NOT = {
				land_naval = -2
			}
		}
		modifier = {
			factor = 1.1
			NOT = {
				land_naval = -4
			}
		}
		modifier = {
			factor = 1.4
			culture_group = british
		}
		modifier = {
			factor = 1.1
			culture_group = french
		}
		modifier = {
			factor = 0.9
			culture_group = iberian
		}
	}
	
	title = "EVTNAME150003"
	desc = "EVTDESC150003"
	
	immediate = { set_country_flag = discoveries_phase_2 }
	
	#Portugease
	option = {
		name = "EVTOPTA150003"
		ai_chance = { factor = 25
		modifier = {
			factor = 8
			culture_group = netherlands
		}
		modifier = {
			factor = 2
			has_country_flag = discoveries_portugal
		}
		modifier = {
			factor = 0.5
			has_country_flag = discoveries_spain
		}
		modifier = {
			factor = 0.5
			has_country_flag = discoveries_french
		}
		}
		indonesian_region  = { discover = yes }
		pacific_ocean_islands = { discover = yes }
		set_country_flag = discoveries_portugal
	}
	
	#Spanish
	option = {
		name = "EVTOPTB150003"
		ai_chance = { factor = 25
		modifier = {
			factor = 0.25
			culture_group = netherlands
		}
		modifier = {
			factor = 0.5
			has_country_flag = discoveries_portugal
		}
		modifier = {
			factor = 2
			has_country_flag = discoveries_spain
		}
		modifier = {
			factor = 0.5
			has_country_flag = discoveries_french
		}
		}
		the_andes= { discover = yes }
		pacific_ocean_islands = { discover = yes }
		la_plata_region  = { discover = yes }
		set_country_flag = discoveries_spain
	}
	
	#French
	option = {
		name = "EVTOPTC150003"
		ai_chance = { factor = 25
		modifier = {
			factor = 0.5
			culture_group = british
		}
		modifier = {
			factor = 0.5
			has_country_flag = discoveries_portugal
		}
		modifier = {
			factor = 0.5
			has_country_flag = discoveries_spain
		}
		modifier = {
			factor = 4
			has_country_flag = discoveries_french
		}
		}
		ohio_basin = { discover = yes }
		florida = { discover = yes }
		great_lakes = { discover = yes }
		set_country_flag = discoveries_french
	}
	
	#British
	option = {
		name = "EVTOPTD150003"
		ai_chance = { 
		factor = 25		
		modifier = {
			factor = 4
			has_country_flag = discoveries_britain
		}
		modifier = {
			factor = 0.5
			has_country_flag = discoveries_portugal
		}
		modifier = {
			factor = 0.5
			has_country_flag = discoveries_spain
		}
		modifier = {
			factor = 0.5
			has_country_flag = discoveries_french
		}
		}
		great_lakes = { discover = yes }
		appalachian_mountains = { discover = yes }
		australian_coast = { discover = yes }
		new_zealand_region = { discover = yes }
		set_country_flag = discoveries_britain
	}
	
}