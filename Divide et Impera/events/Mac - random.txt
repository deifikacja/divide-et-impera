country_event = {
	id =  14000

	trigger = {
		war = no
		year = 1400
		NOT = { year = 1500 }
		check_variable = { 
				which = "dei_economic_situation"
				value = 9
		}
	}
		
	mean_time_to_happen = {

		months = 2800

		modifier = {
			factor = 0.7
			adm = 8
		}
		modifier = {
			factor = 0.8
			adm = 7
		}
		modifier = {
			factor = 0.9
			adm = 6
		}
		modifier = {
			factor = 0.8
			luck = yes
		}
		modifier = {
			factor = 1.5
			badboy = 0.5
		} 
		modifier = { 
			factor = 1.25
			NOT = { stability = 1 }
		}
		modifier = { 
			factor = 2
			NOT = { stability = -1 }
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 5
		}		
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 4
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 3
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 2
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 1
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 0
		}
		modifier = { 
			factor = 0.9
			check_variable = { 
				which = "dei_economic_situation"
				value = 19
			}
		}
	}

	title = "EVTNAME14000"
	desc = "EVTDESC14000"
		
	option = {
		name = "EVTOPTA14000"
		treasury = 100
		add_country_modifier = {
			name = "exceptional_year_mac" 
			duration = 365
			}
		}
}

country_event = {
	id =  14001

	trigger = {
		war = no
		year = 1500 
		NOT = { year = 1600 }
		check_variable = { 
				which = "dei_economic_situation"
				value = 9
		}
	}
		
	mean_time_to_happen = {

		months = 2600

		modifier = {
			factor = 0.7
			adm = 8
		}
		modifier = {
			factor = 0.8
			adm = 7
		}
		modifier = {
			factor = 0.9
			adm = 6
		}
		modifier = {
			factor = 0.8
			luck = yes
		}
		modifier = {
			factor = 1.5
			badboy = 0.5
		} 
		modifier = { 
			factor = 1.25
			NOT = { stability = 1 }
		}
		modifier = { 
			factor = 2
			NOT = { stability = -1 }
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 5
		}		
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 4
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 3
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 2
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 1
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 0
		}
		modifier = { 
			factor = 0.9
			check_variable = { 
				which = "dei_economic_situation"
				value = 19
			}
		}
	}
	title = "EVTNAME14001"
	desc = "EVTDESC14001"
		
	option = {
		name = "EVTOPTA14001"
		treasury = 200
		add_country_modifier = {
			name = "exceptional_year_mac" 
			duration = 365
			}
		}
}

country_event = {
	id =  14002

	trigger = {
		war = no
		year = 1600 
		NOT = { year = 1700 }
		check_variable = { 
				which = "dei_economic_situation"
				value = 9
		}
	}
		
	mean_time_to_happen = {

		months = 2400

		modifier = {
			factor = 0.7
			adm = 8
		}
		modifier = {
			factor = 0.8
			adm = 7
		}
		modifier = {
			factor = 0.9
			adm = 6
		}
		modifier = {
			factor = 0.8
			luck = yes
		}
		modifier = {
			factor = 1.5
			badboy = 0.5
		} 
		modifier = { 
			factor = 1.25
			NOT = { stability = 1 }
		}
		modifier = { 
			factor = 2
			NOT = { stability = -1 }
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 5
		}		
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 4
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 3
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 2
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 1
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 0
		}
		modifier = { 
			factor = 0.9
			check_variable = { 
				which = "dei_economic_situation"
				value = 19
			}
		}
	}
	title = "EVTNAME14002"
	desc = "EVTDESC14002"
		
	option = {
		name = "EVTOPTA14002"
		treasury = 300
		add_country_modifier = {
			name = "exceptional_year_mac" 
			duration = 365
			}
		}
}

country_event = {
	id =  14003

	trigger = {
		war = no
		year = 1700
		check_variable = { 
				which = "dei_economic_situation"
				value = 9
		}
	}
		
	mean_time_to_happen = {

		months = 2200

		modifier = {
			factor = 0.7
			adm = 8
		}
		modifier = {
			factor = 0.8
			adm = 7
		}
		modifier = {
			factor = 0.9
			adm = 6
		}
		modifier = {
			factor = 0.8
			luck = yes
		}
		modifier = {
			factor = 1.5
			badboy = 0.5
		} 
		modifier = { 
			factor = 1.25
			NOT = { stability = 1 }
		}
		modifier = { 
			factor = 2
			NOT = { stability = -1 }
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 5
		}		
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 4
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 3
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 2
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 1
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 0
		}
		modifier = { 
			factor = 0.9
			check_variable = { 
				which = "dei_economic_situation"
				value = 19
			}
		}
	}
	title = "EVTNAME14003"
	desc = "EVTDESC14003"
		
	option = {
		name = "EVTOPTA14003"
		treasury = 400
		add_country_modifier = {
			name = "exceptional_year_mac" 
			duration = 365
			}
		}
}

country_event = {
	id =  14020

	trigger = {
		num_of_cities = 30
	}
		
	mean_time_to_happen = {

		months = 2000

		modifier = {
			factor = 1.2
			adm = 7
		}
		modifier = {
			factor = 1.2
			adm = 8
		}
		modifier = {
			factor = 1.2
			adm = 6
		}
		modifier = {
			factor = 1.5
			luck = yes
		}
		modifier = {
			factor = 1.05
			NOT = { centralization_decentralization = -4 } 
		}
		modifier = {
			factor = 1.05
			NOT = { centralization_decentralization = -3 } 
		}
		modifier = {
			factor = 1.05
			NOT = { centralization_decentralization = -2 } 
		}
		modifier = {
			factor = 1.05
			NOT = { centralization_decentralization = -1 } 
		}
		modifier = {
			factor = 1.05
			NOT = { centralization_decentralization = 0 } 
		}
		modifier = {
			factor = 0.95
			centralization_decentralization = 1
		}
		modifier = {
			factor = 0.95
			centralization_decentralization = 2
		}
		modifier = {
			factor = 0.95
			centralization_decentralization = 3
		}
		modifier = {
			factor = 0.95
			centralization_decentralization = 4
		}

			
		}

	title = "EVTNAME14020"
	desc = "EVTDESC14020"
		
	option = {
		name = "EVTOPTA14020"
		add_country_modifier = {
			name = "Devastating_Fire" 
			duration = 365
			}
		treasury = -200
		}
}



country_event = {
	id =  14030

	trigger = {
		ADM = 5
		DIP = 5
		MIL = 5
	}
		
	mean_time_to_happen = {

		months = 2000

		modifier = {
			factor = 0.5
			luck = yes
		}
		modifier = {
			factor = 0.95
			NOT = { centralization_decentralization = -4 } 
		}
		modifier = {
			factor = 0.95
			NOT = { centralization_decentralization = -3 } 
		}
		modifier = {
			factor = 0.9
			NOT = { centralization_decentralization = -2 } 
		}
		modifier = {
			factor = 0.95
			NOT = { centralization_decentralization = -1 } 
		}
		modifier = {
			factor = 0.95
			NOT = { centralization_decentralization = 0 } 
		}
		modifier = {
			factor = 1.05
			centralization_decentralization = 1
		}
		modifier = {
			factor = 1.05
			centralization_decentralization = 2
		}
		modifier = {
			factor = 1.05
			centralization_decentralization = 3
		}
		modifier = {
			factor = 1.05
			centralization_decentralization = 4
		}

			
		}

	title = "EVTNAME14030"
	desc = "EVTDESC14030"
		
	option = {
		name = "EVTOPTA14030"
		stability = 1
		add_country_modifier = {
			name = "Good_Gov"
			duration = 720	
			}
		}
}

country_event = {
	id =  14031

	trigger = {
		NOT = { ADM = 6 }
		NOT = { DIP = 6 }
		NOT = { MIL = 6 }
	}
		
	mean_time_to_happen = {

		months = 2000

		modifier = {
			factor = 1.5
			luck = yes
		}


			
		}

	title = "EVTNAME14031"
	desc = "EVTDESC14031"
		
	option = {
		name = "EVTOPTA14031"
		stability = -1
		add_country_modifier = {
			name = "Bad_Gov"
			duration = 720	
			}
		}
}

country_event = {
	id =  14032

	trigger = {
		NOT = { has_country_modifier = narrowminded_army }
		NOT = { MIL = 7 }
		OR = {
			NOT = { army_tradition = 0.4 }
			NOT = { army_size_percentage = 0.4 }
		}
	}
		
	mean_time_to_happen = {

		months = 5000

		modifier = {
			factor = 1.5
			luck = yes
		}
		modifier = {
			factor = 0.9
			NOT = { MIL = 6 }
		}
		modifier = {
			factor = 0.9
			NOT = { MIL = 5 }
		}
		modifier = {
			factor = 0.9
			NOT = { MIL = 4 }
		}
		modifier = {
			factor = 0.9
			NOT = { army_size_percentage = 0.8 }
		}
		modifier = {
			factor = 0.9
			NOT = { army_size_percentage = 0.7 }
		}
		modifier = {
			factor = 0.9
			NOT = { army_size_percentage = 0.6 }
		}
		modifier = {
			factor = 0.9
			NOT = { army_size_percentage = 0.5 }
		}
		modifier = {
			factor = 0.9
			NOT = { army_size_percentage = 0.4 }
		}
		modifier = {
			factor = 0.9
			NOT = { army_size_percentage = 0.3 }
		}
		modifier = {
			factor = 0.9
			NOT = { army_size_percentage = 0.2 }
		}
		modifier = {
			factor = 0.9
			NOT = { army_size_percentage = 0.1 }
		}
		modifier = {
			factor = 0.9
			NOT = { army_tradition = 0.5 }
		}
		modifier = {
			factor = 0.9
			NOT = { army_tradition = 0.45 }
		}
		modifier = {
			factor = 0.9
			NOT = { army_tradition = 0.4 }
		}
		modifier = {
			factor = 0.9
			NOT = { army_tradition = 0.35 }
		}
		modifier = {
			factor = 0.9
			NOT = { army_tradition = 0.3 }
		}
		modifier = {
			factor = 0.9
			NOT = { army_tradition = 0.25 }
		}
		modifier = {
			factor = 0.9
			NOT = { army_tradition = 0.2 }
		}
		modifier = {
			factor = 0.9
			NOT = { army_tradition = 0.15 }
		}
		modifier = {
			factor = 0.9
			NOT = { army_tradition = 0.1 }
		}

			
		}

	title = "EVTNAME14032"
	desc = "EVTDESC14032"
		
	option = {
		name = "EVTOPTA14032"
		add_country_modifier = {
			name = "narrowminded_army"
			duration = 1095	
			}
		}
}
country_event = {
	id =  14040

	trigger = {
		NOT = { innovative_narrowminded = 3 }

	}
		
	mean_time_to_happen = {

		months = 3000

		modifier = {
			factor = 1.1
			NOT = { innovative_narrowminded = 2 }
		}

		modifier = {
			factor = 1.1
			NOT = { innovative_narrowminded = 1 }
		}

		modifier = {
			factor = 1.1
			NOT = { innovative_narrowminded = 0 }
		}

		modifier = {
			factor = 1.1
			NOT = { innovative_narrowminded = -1 }
		}

		modifier = {
			factor = 1.1
			NOT = { innovative_narrowminded = -2 }
		}

		modifier = {
			factor = 1.1
			NOT = { innovative_narrowminded = -3 }
		}

		modifier = {
			factor = 1.1
			NOT = { innovative_narrowminded = -4 }
		}


			
		}

	title = "EVTNAME14040"
	desc = "EVTDESC14040"
		
	option = {
		name = "EVTOPTB14040"
		innovative_narrowminded = 1
		ai_chance = {
			factor = 70

			}
		}
	option = {
		name = "EVTOPTA14040"
		stability = -1
		ai_chance = {
			factor = 30
			modifier = {
				factor = 2
				stability = 2
				} 
			modifier = {
				factor = 2
				stability = 3
				} 
			}
		}
}


country_event = {
	id =  14050

	trigger = {
		NOT = { innovative_narrowminded = 0 }
		ADM = 5
	}
		
	mean_time_to_happen = {

		months = 5000

		modifier = {
			factor = 1.4
			NOT = {
				OR = {
					advisor = natural_scientist
					advisor = treasurer
					advisor = architect
					advisor = patrician
				}
			}
		}

		modifier = {
			factor = 0.9
			natural_scientist = 5
		}
		modifier = {
			factor = 0.9
			treasurer = 5
		}
		modifier = {
			factor = 0.9
			architect = 5
		}
		modifier = {
			factor = 0.9
			patrician = 5
		}
		modifier = {
			factor = 0.85
			NOT = { innovative_narrowminded = -1 }
		}
		modifier = {
			factor = 0.85
			NOT = { innovative_narrowminded = -2 }
		}
		modifier = {
			factor = 0.85
			NOT = { innovative_narrowminded = -3 }
		}
		modifier = {
			factor = 0.85
			NOT = { innovative_narrowminded = -4 }
		}
		modifier = {
			factor = 0.75
			any_owned_province = { trade_goods = wine }
		}
		modifier = {
			factor = 0.75
			any_owned_province = { trade_goods = sugar }
		}
		modifier = {
			factor = 1.1
			NOT = { stability = 2 }
		}
		modifier = {
			factor = 1.5
			NOT = { stability = 1 }
		}
		modifier = {
			factor = 2
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 1.2
			NOT = { 
				check_variable = { 
					which = "dei_economic_situation"
					value = 10
				}
			}
		}	
		modifier = {
			factor = 1.5
			NOT = { 
				check_variable = { 
					which = "dei_economic_situation"
					value = 6
				}
			}
		}
		modifier = {
			factor = 1.8
			NOT = { 
				check_variable = { 
					which = "dei_economic_situation"
					value = 3
				}
			}
		}
	}

	title = "EVTNAME14050"
	desc = "EVTDESC14050"
		
	option = {
		name = "EVTOPTA14050"
		random_owned = {
			limit = {
			NOT = { has_building = refinery }
			NOT = { trade_goods = naval_supplies }
			NOT = { trade_goods = copper }
			NOT = { trade_goods = iron }
			NOT = { trade_goods = wool }
			NOT = { trade_goods = cloth }
			}
			add_building = refinery 
		}
	}

}
country_event = {
	id =  14051

	trigger = {
		NOT = { innovative_narrowminded = 0 }
		ADM = 5

	}
		
	mean_time_to_happen = {

		months = 5000

		modifier = {
			factor = 1.4
			NOT = {
				OR = {
					advisor = naval_organiser
					advisor = strategist
					advisor = architect
					advisor = naval_reformer
				}
			}
		}
		modifier = {
			factor = 0.9
			naval_organiser = 5
		}
		modifier = {
			factor = 0.9
			naval_reformer = 5
		}
		modifier = {
			factor = 0.9
			architect = 5
		}
		modifier = {
			factor = 0.9
			strategist = 5
		}
		modifier = {
			factor = 0.85
			NOT = { innovative_narrowminded = -1 }
		}
		modifier = {
			factor = 0.85
			NOT = { innovative_narrowminded = -2 }
		}
		modifier = {
			factor = 0.85
			NOT = { innovative_narrowminded = -3 }
		}
		modifier = {
			factor = 0.85
			NOT = { innovative_narrowminded = -4 }
		}
		modifier = {
			factor = 0.75
			any_owned_province = { trade_goods = naval_supplies }
		}
		modifier = {
			factor = 1.1
			NOT = { stability = 2 }
		}
		modifier = {
			factor = 1.2
			NOT = { stability = 1 }
		}
		modifier = {
			factor = 1.5
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 1.1
			NOT = { 
				check_variable = { 
					which = "dei_economic_situation"
					value = 10
				}
			}
		}	
		modifier = {
			factor = 1.3
			NOT = { 
				check_variable = { 
					which = "dei_economic_situation"
					value = 6
				}
			}
		}
		modifier = {
			factor = 1.5
			NOT = { 
				check_variable = { 
					which = "dei_economic_situation"
					value = 3
				}
			}
		}
	}
		
	title = "EVTNAME14051"
	desc = "EVTDESC14051"
		
	option = {
		name = "EVTOPTA14051"
		random_owned = {
			limit = {
			NOT = { has_building = wharf }
			NOT = { trade_goods = wine }
			NOT = { trade_goods = sugar }
			NOT = { trade_goods = copper }
			NOT = { trade_goods = iron }
			NOT = { trade_goods = wool }
			NOT = { trade_goods = cloth }
			}
			add_building = wharf 
		}
	}

}
country_event = {
	id =  14052

	trigger = {
		NOT = { innovative_narrowminded = 0 }
		ADM = 5
	}
		
	mean_time_to_happen = {

		months = 5000

		modifier = {
			factor = 1.4
			NOT = {
				OR = {
					advisor = army_organiser
					advisor = strategist
					advisor = architect
					advisor = army_reformer
				}
			}
		}
		modifier = {
			factor = 0.9
			army_organiser = 5
		}
		modifier = {
			factor = 0.9
			army_reformer = 5
		}
		modifier = {
			factor = 0.9
			architect = 5
		}
		modifier = {
			factor = 0.9
			strategist = 5
		}
		modifier = {
			factor = 0.85
			NOT = { innovative_narrowminded = -1 }
		}
		modifier = {
			factor = 0.85
			NOT = { innovative_narrowminded = -2 }
		}
		modifier = {
			factor = 0.85
			NOT = { innovative_narrowminded = -3 }
		}
		modifier = {
			factor = 0.85
			NOT = { innovative_narrowminded = -4 }
		}	
		modifier = {
			factor = 0.75
			any_owned_province = { trade_goods = copper }
		}
		modifier = {
			factor = 0.75
			any_owned_province = { trade_goods = iron }
		}
		modifier = {
			factor = 1.1
			NOT = { stability = 2 }
		}
		modifier = {
			factor = 1.2
			NOT = { stability = 1 }
		}
		modifier = {
			factor = 1.5
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 1.1
			NOT = { 
				check_variable = { 
					which = "dei_economic_situation"
					value = 10
				}
			}
		}	
		modifier = {
			factor = 1.3
			NOT = { 
				check_variable = { 
					which = "dei_economic_situation"
					value = 6
				}
			}
		}
		modifier = {
			factor = 1.5
			NOT = { 
				check_variable = { 
					which = "dei_economic_situation"
					value = 3
				}
			}
		}
	}

	title = "EVTNAME14052"
	desc = "EVTDESC14052"
		
	option = {
		name = "EVTOPTA14052"
		random_owned = {
			limit = {
			NOT = { has_building = weapons }
			NOT = { trade_goods = naval_supplies }
			NOT = { trade_goods = wine }
			NOT = { trade_goods = sugar }
			NOT = { trade_goods = wool }
			NOT = { trade_goods = cloth }
			}
			add_building = weapons
		}
	}

}
country_event = {
	id =  14053

	trigger = {
		NOT = { innovative_narrowminded = 0 }
		ADM = 5
	}
		
	mean_time_to_happen = {

		months = 5000

		modifier = {
			factor = 1.4
			NOT = {
				OR = {
					advisor = natural_scientist
					advisor = trader
					advisor = architect
					advisor = sheriff
				}
			}
		}
		modifier = {
			factor = 0.9
			natural_scientist = 5
		}
		modifier = {
			factor = 0.9
			trader = 5
		}
		modifier = {
			factor = 0.9
			architect = 5
		}
		modifier = {
			factor = 0.9
			sheriff = 5
		}
		modifier = {
			factor = 0.85
			NOT = { innovative_narrowminded = -1 }
		}
		modifier = {
			factor = 0.85
			NOT = { innovative_narrowminded = -2 }
		}
		modifier = {
			factor = 0.85
			NOT = { innovative_narrowminded = -3 }
		}
		modifier = {
			factor = 0.85
			NOT = { innovative_narrowminded = -4 }
		}
		modifier = {
			factor = 0.75
			any_owned_province = { trade_goods = wool }
		}
		modifier = {
			factor = 0.75
			any_owned_province = { trade_goods = cloth }
		}
		modifier = {
			factor = 1.1
			NOT = { stability = 2 }
		}
		modifier = {
			factor = 1.2
			NOT = { stability = 1 }
		}
		modifier = {
			factor = 1.5
			NOT = { stability = 0 }
		}
		modifier = {
			factor = 1.1
			NOT = { 
				check_variable = { 
					which = "dei_economic_situation"
					value = 10
				}
			}
		}	
		modifier = {
			factor = 1.3
			NOT = { 
				check_variable = { 
					which = "dei_economic_situation"
					value = 6
				}
			}
		}
		modifier = {
			factor = 1.5
			NOT = { 
				check_variable = { 
					which = "dei_economic_situation"
					value = 3
				}
			}
		}
	}

	title = "EVTNAME14053"
	desc = "EVTDESC14053"
		
	option = {
		name = "EVTOPTA14053"
		random_owned = {
			limit = {
			NOT = { has_building = textile }
			NOT = { trade_goods = naval_supplies }
			NOT = { trade_goods = wine }
			NOT = { trade_goods = sugar }
			NOT = { trade_goods = iron }
			NOT = { trade_goods = copper }
			}
			add_building = textile
		}
	}

}

country_event = {
	id =  14060

	trigger = {
		NOT = { has_country_modifier = obscure_production }
		serfdom_freesubjects = -3 
	}
		
	mean_time_to_happen = {

		months = 2000

		modifier = { 
			factor = 0.9
			serfdom_freesubjects = -2 	
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = -1 	
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 0	
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 1 	
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 2 	
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 3	
		}
		modifier = { 
			factor = 0.9
			serfdom_freesubjects = 4 	
		}
		
	}
	title = "EVTNAME14060"
	desc = "EVTDESC14060"
		
	option = {
		name = "EVTOPTA14060"
		ai_chance = { factor = 55 }
		serfdom_freesubjects = -1
	}

	option = {
		name = "EVTOPTB14060"
		ai_chance = { factor = 45 }
		add_country_modifier = { 
			name = "obscure_production"
			duration = 1800
		}
	}
}


country_event = {
	id =  14061

	trigger = {
		NOT = { serfdom_freesubjects = 3 }

	}
		
	mean_time_to_happen = {

		months = 2000

		modifier = { 
			factor = 0.9
			NOT = { serfdom_freesubjects = 2 }	
		}
		modifier = { 
			factor = 0.9
			NOT = { serfdom_freesubjects = 1 }	
		}
		modifier = { 
			factor = 0.9
			NOT = { serfdom_freesubjects = 0 }	
		}
		modifier = { 
			factor = 0.9
			NOT = { serfdom_freesubjects = -1 }	
		}
		modifier = { 
			factor = 0.9
			NOT = { serfdom_freesubjects = -2 }	
		}
		modifier = { 
			factor = 0.9
			NOT = { serfdom_freesubjects = -3 }	
		}
		modifier = { 
			factor = 0.9
			NOT = { serfdom_freesubjects = -4 }	
		}


			
		}
	title = "EVTNAME14061"
	desc = "EVTDESC14061"
		
	option = {
		name = "EVTOPTA14061"
		ai_chance = { factor = 80 }
		serfdom_freesubjects = 1
		stability = -2
		random_owned = { noble_rebels = 1 }
	}

	option = {
		name = "EVTOPTB14061"
		ai_chance = {
			factor = 20
			modifier = {
				factor = 2
				stability = 2
				} 
			modifier = {
				factor = 2
				stability = 3
				} 
			}
		stability = -1
		random_owned = { anti_tax_rebels = 1 }
		random_owned = { anti_tax_rebels = 1 }
		random_owned = { anti_tax_rebels = 1 }
	}
}


country_event = {
	id =  14062

	trigger = {
		NOT = { has_country_modifier = obscure_trade }
		NOT = { mercantilism_freetrade = 3 }

	}
		
	mean_time_to_happen = {

		months = 2000

		modifier = { 
			factor = 0.9
			NOT = { mercantilism_freetrade = 2 }	
		}
		modifier = { 
			factor = 0.9
			NOT = { mercantilism_freetrade = 1 }	
		}
		modifier = { 
			factor = 0.9
			NOT = { mercantilism_freetrade = 0 }	
		}
		modifier = { 
			factor = 0.9
			NOT = { mercantilism_freetrade = -1 }	
		}
		modifier = { 
			factor = 0.9
			NOT = { mercantilism_freetrade = -2 }	
		}
		modifier = { 
			factor = 0.9
			NOT = { mercantilism_freetrade = -3 }	
		}
		modifier = { 
			factor = 0.9
			NOT = { mercantilism_freetrade = -4 }	
		}
		modifier = { 
			factor = 1.1
			total_merchants = 5	
		}
		modifier = { 
			factor = 1.1
			total_merchants = 10	
		}
		modifier = { 
			factor = 1.1
			total_merchants = 15	
		}
		modifier = { 
			factor = 1.1
			total_merchants = 20	
		}
		modifier = { 
			factor = 1.1
			total_merchants = 25	
		}
		modifier = { 
			factor = 1.1
			total_merchants = 30	
		}
		modifier = { 
			factor = 1.1
			total_merchants = 35	
		}
		modifier = { 
			factor = 1.1
			total_merchants = 40	
		}


			
		}
	title = "EVTNAME14062"
	desc = "EVTDESC14062"
		
	option = {
		name = "EVTOPTA14062"
		ai_chance = {
			factor = 90
		}
		mercantilism_freetrade = 1
		add_country_modifier = { 
			name = "obscure_production"
			duration = 1800
		}
	}

	option = {
		name = "EVTOPTB14062"
		ai_chance = {
			factor = 10
		}
		add_country_modifier = { 
			name = "obscure_trade"
			duration = 1800
		}
		prestige = 0.02
	}
}


country_event = {
	id =  14070

	trigger = {
		NOT = { has_country_modifier = political_crisis }
		NOT = { DIP = 7 }
	}
		
	mean_time_to_happen = {

		months = 4000

		modifier = {
			factor = 0.9
			NOT = { DIP = 5 }
		}
		modifier = {
			factor = 0.9
			NOT = { DIP = 6 }
		}
		modifier = {
			factor = 0.9
			NOT = { DIP = 4 }
		}
		modifier = {
			factor = 0.8
			any_neighbor_country = { NOT = {  relation = { who = THIS value = 0 } } }
		}
		modifier = {
			factor = 0.8
			any_neighbor_country = { NOT = {  relation = { who = THIS value = -100 } } }
		}
		modifier = {
			factor = 0.9
			badboy = 0.5	
		}
		modifier = {
			factor = 0.9
			badboy = 0.6	
		}
		modifier = {
			factor = 0.9
			badboy = 0.7	
		}
		modifier = {
			factor = 0.9
			badboy = 0.8	
		}
		modifier = {
			factor = 0.9
			badboy = 0.9	
		}
		modifier = {
			factor = 0.9
			badboy = 1	
		}

	}
	title = "EVTNAME14070"
	desc = "EVTDESC14070"
		
	option = {
		name = "EVTOPTA14070"
		stability = -1
		any_neighbor_country = { relation = { who = THIS value = -20 } } 
		add_country_modifier = { 
			name = "political_crisis"
			duration = 1800
		}
	}


}

country_event = {
	id =  14090

	trigger = {
		aristocracy_plutocracy = -4
	}
		
	mean_time_to_happen = {

		months = 3000

		modifier = {
			factor = 0.8
			aristocracy_plutocracy = -3 
		}
		modifier = {
			factor = 0.8
			aristocracy_plutocracy = -2
		}
		modifier = {
			factor = 0.8
			aristocracy_plutocracy = -1
		}
		modifier = {
			factor = 0.8
			aristocracy_plutocracy = 0
		}
		modifier = {
			factor = 0.8
			aristocracy_plutocracy = 1
		}
		modifier = {
			factor = 0.8
			aristocracy_plutocracy = 2
		}
		modifier = {
			factor = 0.8
			aristocracy_plutocracy = 3
		}
		modifier = {
			factor = 0.8
			aristocracy_plutocracy = 4
		}
	}

	title = "EVTNAME14090"
	desc = "EVTDESC14090"
		
	option = {
		name = "EVTOPTA14090"
		aristocracy_plutocracy = -1
		ai_chance = {
			factor = 10
			modifier = {
				factor = 2
				NOT = { stability = 1 }
				} 
			modifier = {
				factor = 2
				NOT = { stability = 0 }
				} 
			modifier = {
				factor = 2
				NOT = { stability = -1 }
				} 
			modifier = {
				factor = 2
				NOT = { stability = -2 }
				} 
		}
	}
	option = {
		name = "EVTOPTB14090"
		add_country_modifier = {
			name = "restless_nobles" 
			duration = 720
			}
		ai_chance = {
			factor = 40
				modifier = {
				factor = 0.5			
				war_exhaustion = 4
				}
				modifier = {
				factor = 0.6			
				war_exhaustion = 6
				}
				modifier = {
				factor = 0.6			
				war_exhaustion = 8
				}
				modifier = {
				factor = 0.6			
				war_exhaustion = 10
				}
				modifier = {
				factor = 0.6			
				war_exhaustion = 12
				}

			}
		}
	option = {
		name = "EVTOPTC14090"
		aristocracy_plutocracy = 1
		stability = -3
		ai_chance = {
			factor = 20
			modifier = {
				factor = 2
				stability = 1 
			} 
			modifier = {
				factor = 2
				stability = 2 
			} 
			modifier = {
				factor = 4
				stability = 3
			} 
		}
	}
}

country_event = {

	id =  22061

	trigger = {
		num_of_unions = 1
		ai = yes
		NOT = { advisor = philosopher }
	}

	mean_time_to_happen = {
		months = 1
	}

	title = "EVTNAME22061"
	desc = "EVTDESC22061"

	option = {
		name = "EVTOPTA22061"		
		define_advisor = { type = philosopher skill = 6 }
		remove_advisor = random
	}	
}
