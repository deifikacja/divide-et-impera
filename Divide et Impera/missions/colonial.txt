discover_mission = {
	type = empty_provinces
	allow = {
		idea = quest_for_the_new_world
		range = yes
		port = yes
		this = { num_of_ports = 1  }
		NOT = { has_discovered = THIS }
		this = { war = no }
	}
	abort = {
		or = {
			NOT = {	idea = quest_for_the_new_world }
			this = { not = { num_of_ports = 1 } }
		}
	}
	success = {
		has_discovered = THIS
	}
	chance = {
		factor = 1
		#tweak for some historical preferences.
		modifier = {
			factor = 0
			THIS = { tag = POR }
			OR = {
				region = north_american_region
				region = central_american_region
				region = the_carribean
			}
		}
		modifier = {
			factor = 5
			THIS = { tag = POR }
			OR = {
				region = brazil_region
				region = west_african_coast
			}
		}		
		modifier = {
			factor = 0
			THIS = { 
				OR = {
					tag = SPA
					tag = CAS
				}
			}
			OR = {
				region = north_american_region
				region = brazil_region
				region = west_african_coast
			}
		}
		modifier = {
			factor = 5
			THIS = { 
				OR = {
					tag = SPA
					tag = CAS
				}
			}
			OR = {
				region = the_spanish_main
				region = central_american_region
				region = the_carribean
				region = la_plata_region
			}
		}	
		modifier = {
			factor = 5
			THIS = {
				OR = {
					tag = ENG
					tag = GBR
					tag = FRA
				}
			}
			OR = {
				region = north_american_region
				region = the_carribean
			}
		}
	}
	effect = {
		colonists = 5
	}
	
}


build_colony_to_city = {
	type = our_provinces
	allow = {
		this = { war = no }
		is_colony = yes
		owner = {
			colonists = 1
			monthly_income = 10
		}
	}
	abort = {
		NOT = { owned_by = THIS }
	}
	success = {
		is_colony = false
		owned_by = THIS
	}
	chance = {
		factor = 1
	}
	effect = {
		base_tax = 1
		change_manpower = 0.2
	}
	
}

convert_the_pagans = {
	type = our_provinces
	allow = {
		this = { war = no }
		owner = { religion_group = christian }
		religion_group = pagan
		is_overseas = yes
		owner = { missionaries = 1 }
	}
	abort = {
		NOT = { owned_by = THIS }
	}
	success = {
		religion_group = christian
	}
	chance = {
		factor = 1
	}
	effect = {
		change_manpower = 0.3
	}
}

establish_carribean_colony = {
	type = country
	allow = {
		war = no

		num_of_ports = 1
		idea = quest_for_the_new_world 
		religion_group = christian
		
		the_carribean = {
			empty = yes
			range = yes
			has_discovered = THIS
		}
		NOT = {
			the_carribean = {
				owned_by =  THIS
			}
		}
	}
	abort = {
		or = {
			NOT = {
				the_carribean = {
					empty = yes
					has_discovered = THIS
				}
			}
			not = { num_of_ports = 1 }
		}
	}
	success = {
		the_carribean = {
			owned_by =  THIS
		}
	}
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
			num_of_ports = 5
		}

	}
	effect = {
		prestige = 0.1
		colonists = 5
	}
}

establish_usa_colony = {
	type = country
	allow = {
		war = no
		num_of_ports = 1
		idea = quest_for_the_new_world 
		religion_group = christian
		
		the_thirteen_colonies = {
			empty = yes
			port = yes
			range = yes
			has_discovered = THIS
		}
		NOT = {
			the_thirteen_colonies = {
				owned_by =  THIS
			}
		}
	}
	abort = {
		or = {
			NOT = {
				the_thirteen_colonies = {
					empty = yes
					port = yes
					has_discovered = THIS
				}
			}
			not = { num_of_ports = 1 }
		}
	}
	success = {
		the_thirteen_colonies = {
			owned_by =  THIS
		}
	}
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
			num_of_ports = 5
		}

	}
	effect = {
		prestige = 0.1
		colonists = 5
	}
}

establish_canada_colony = {
	type = country
	allow = {
		war = no
		num_of_ports = 1
		religion_group = christian
		
		idea = quest_for_the_new_world 
		northern_america = {
			empty = yes
			port = yes
			range = yes
			has_discovered = THIS
		}
		NOT = {
			northern_america = {
				owned_by =  THIS
			}
		}
	}
	abort = {
		or = {
			NOT = {
				northern_america = {
					empty = yes
					port = yes
					has_discovered = THIS
				}
			}
			not = { num_of_ports = 1 }
		}
	}
	success = {
		northern_america = {
			owned_by =  THIS
		}
	}
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
			num_of_ports = 5
		}

	}
	effect = {
		prestige = 0.1
		colonists = 5
	}
}


# todo, colonize x..
# chinese trade station


make_base_on_spice_islands = {
	type = country
	allow = {
		war = no
		num_of_ports = 3
		religion_group = christian
		indonesian_region = {
			port = yes
			range = yes
			has_discovered = THIS
		}
		NOT = {
			indonesian_region = {
				owned_by =  THIS
			}
		}
	}
	abort = {
		not = { num_of_ports = 1 }
	}
	success = {
		indonesian_region = {
			owned_by =  THIS
		}
	}
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
			num_of_ports = 5
		}
		modifier = {
			factor = 1.5
			idea = vice_roys
		}
		modifier = {
			factor = 1.5
			idea = quest_for_the_new_world
		}
	}
	effect = {
		prestige = 0.1
	}
}

get_presence_in_india = {
	type = country
	allow = {
		war = no
		num_of_ports = 3
		monthly_income = 70
		religion_group = christian
		indian_region = {
			port = yes
			range = yes
			has_discovered = THIS
		}
		NOT = {
			indian_region = {
				owned_by =  THIS
			}
		}
	}
	abort = {
		not = { num_of_ports = 1 }
	}
	success = {
		indian_region = {
			owned_by =  THIS
		}
	}
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
			num_of_ports = 5
		}
		modifier = {
			factor = 1.5
			idea = vice_roys
		}
		modifier = {
			factor = 1.5
			idea = shrewd_commerce_practise
		}
	}
	immediate = {
		indian_region = {
			limit = { port = yes range = yes has_discovered = THIS }
			add_temp_claim = THIS
		}
	}
	abort_effect = {
		indian_region = {
			remove_temp_claim = THIS
		}
	}
	
	effect = {
		prestige = 0.1
		indian_region = {
			remove_temp_claim = THIS
		}
	}
}

get_foothold_in_china = {
	type = country
	allow = {
		war = no
		num_of_ports = 5
		monthly_income = 100
		religion_group = christian
		chinese_coast = {
			port = yes
			range = yes
			has_discovered = THIS
		}
		NOT = {
			chinese_coast = {
				owned_by =  THIS
			}
		}
	}
	abort = {
		not = { num_of_ports = 1 }
	}
	success = {
		chinese_coast = {
			owned_by =  THIS
		}
	}
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
			num_of_ports = 10
		}
		modifier = {
			factor = 1.5
			idea = vice_roys
		}
		modifier = {
			factor = 1.5
			idea = shrewd_commerce_practise
		}
	}
	
	immediate = {
		chinese_coast = {
			limit = { port = yes range = yes has_discovered = THIS }
			add_temp_claim = THIS
		}
	}
	abort_effect = {
		chinese_coast = {
			remove_temp_claim = THIS
		}
	}
	effect = {
		prestige = 0.1
		chinese_coast = {
			remove_temp_claim = THIS
		}
	}
}

establish_colony_mission = {
	type = empty_provinces
	allow = {
		this = { 
			num_of_ports = 1 
			idea = quest_for_the_new_world
		}
		range = yes
		port = yes
		has_discovered = THIS
	}
	abort = {
		or = {
			empty = no
			this = { not = { num_of_ports = 1 } }
		}
	}
	success = {
		owned_by = THIS
	}
	chance = {
		factor = 1
		#tweak for some historical preferences.
		modifier = {
			factor = 0
			THIS = { tag = POR }
			OR = {
				region = north_american_region
				region = central_american_region
				region = the_carribean
			}
		}
		modifier = {
			factor = 5
			THIS = { tag = POR }
			OR = {
				region = brazil_region
				region = west_african_coast
			}
		}		
		modifier = {
			factor = 0
			THIS = { 
				OR = {
					tag = SPA
					tag = CAS
				}
			}
			OR = {
				region = north_american_region
				region = brazil_region
				region = west_african_coast
			}
		}
		modifier = {
			factor = 5
			THIS = { 
				OR = {
					tag = SPA
					tag = CAS
				}
			}			OR = {
				region = the_spanish_main
				region = central_american_region
				region = the_carribean
				region = la_plata_region
			}
		}	
		modifier = {
			factor = 5
			THIS = {
				OR = {
					tag = ENG
					tag = GBR
					tag = FRA
				}
			}
			OR = {
				region = north_american_region
				region = the_carribean
			}
		}
	}
	effect = {
		colonists = 1
	}
}
