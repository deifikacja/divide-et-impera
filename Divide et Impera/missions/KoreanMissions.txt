haixi_expedition = {

	type = country

	allow = {
		tag = KOR
		OR = {
			AND = { 
				exists = MCH
				MCH = {	owns = 730 }
			}
			AND = { 
				exists = JZJ
				JZJ = {	owns = 730 }
			}
		}
		730 = {
			any_neighbor_province = {
				owned_by = KOR
			}
			OR = {
				has_province_flag = allows_nomad_raids
				has_province_flag = support_nomad_raids
			}
		}
	}
	abort = {
		JZJ = { NOT = { owns = 730 } }
		MCH = { NOT = { owns = 730 } }		# Conquered by someone else
		NOT = { owns = 730 }
	}
	success = {
		owns = 730
	}
	chance = {
		factor = 1000
	}
	immediate = {
		add_temp_claim = 730
	}
	abort_effect = {
		remove_temp_claim = 730
	}
	effect = {
		add_core = 730
	}
}

ninguta_expedition = {

	type = country

	allow = {
		tag = KOR
		OR = {
			AND = { 
				exists = MCH
				MCH = {	owns = 731 }
			}
			AND = { 
				exists = JZJ
				JZJ = {	owns = 731 }
			}
		}
		731 = {
			any_neighbor_province = {
				owned_by = KOR
			}
			OR = {
				has_province_flag = allows_nomad_raids
				has_province_flag = support_nomad_raids
			}
		}
	}
	abort = {
		MCH = { NOT = { owns = 731 } }		# Conquered by someone else
		JZJ = { NOT = { owns = 731 } }
		NOT = { owns = 731 }
	}
	success = {
		owns = 731
	}
	chance = {
		factor = 1000
	}
	immediate = {
		add_temp_claim = 731
	}
	abort_effect = {
		remove_temp_claim = 731
	}
	effect = {
		add_core = 731
	}
}