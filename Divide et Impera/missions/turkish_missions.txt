defeat_the_byzantine_empire = {
	
	type = country

	allow = {
		tag = TUR
		exists = BYZ
		BYZ = {
			owns = 151
			is_lesser_in_union = no
			is_subject = no			
		}
		is_lesser_in_union = no
		is_subject = no
	}
	abort = {
		or = {
			is_lesser_in_union = yes
			is_subject = yes
			BYZ = {
				or = {
					is_lesser_in_union = yes
					is_subject = yes
				}
			}
			and = {
				not = { exists = BYZ }		# Conquered by someone else
				not = { owns = 151 }
			}
		}
	}
	success = {
		owns = 151
		not = { exists = BYZ }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			is_monarch_leader = yes
		}
		modifier = {
			factor = 5
			year = 1500
		}
	}
	immediate = {
		add_temp_claim = 151
	}
	abort_effect = {
		remove_temp_claim = 151
	}
	effect = {
		prestige = 0.2
		add_core = 151
		badboy = -5
	}
}

conquer_southern_greece = {
	
	type = country

	allow = {
		tag = TUR
		or = {
			not = { owns = 145 }
			not = { owns = 146 }
			not = { owns = 1935 }
			not = { owns = 2179 }
		}
		is_lesser_in_union = no
		is_subject = no
	}
	abort = {
		or = {
			is_lesser_in_union = yes
			is_subject = yes
		}
	}
	success = {
		owns = 145
		owns = 146
		owns = 1935
		owns = 2179
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}
	}
	immediate = {
		add_temp_claim = 145
		add_temp_claim = 146
		add_temp_claim = 1935
		add_temp_claim = 2179
	}
	abort_effect = {
		remove_temp_claim = 145
		remove_temp_claim = 146
		remove_temp_claim = 1935
		remove_temp_claim = 2179
	}
	effect = {
		add_core = 145
		add_core = 146
		add_core = 1935
		add_core = 2179
		badboy = -5
	}
}

conquer_serbia = {
	
	type = country

	allow = {
		tag = TUR
		exists = SER
		greece = { owned_by = this }
		is_lesser_in_union = no
		is_subject = no
		SER = {
			owns = 141
			owns = 2266
			neighbour = this
			not = { vassal_of = TUR }
		}
	}
	abort = {
		or = {
			and = {
				not = { exists = SER }
				or = {
					not = { owns = 141 }
					not = { owns = 2266 }
				}
			}
			is_lesser_in_union = yes
			is_subject = yes
		}
	}
	success = {
		owns = 141
		owns = 2266
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = SER value = 0 } }
		}
	}
	immediate = {
		add_temp_claim = 141
		add_temp_claim = 2266
	}
	abort_effect = {
		remove_temp_claim = 141
		remove_temp_claim = 2266
	}
	effect = {
		add_core = 141
		add_core = 2266
		badboy = -5
	}
}

conquer_bosnia = {
	
	type = country

	allow = {
		tag = TUR
		exists = BOS
		is_lesser_in_union = no
		is_subject = no
		BOS = {
			owns = 139
			owns = 140
			neighbour = this
			not = { vassal_of = TUR }
		}
	}
	abort = {
		or = {
			and = {
				not = { exists = BOS }
				or = {
					not = { owns = 139 }
					not = { owns = 140 }	
				}
			}
			is_lesser_in_union = yes
			is_subject = yes
		}
	}
	success = {
		owns = 139
		owns = 140
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = BOS value = 0 } }
		}
	}
	immediate = {
		add_temp_claim = 139
		add_temp_claim = 140
	}
	abort_effect = {
		remove_temp_claim = 139
		remove_temp_claim = 140
	}
	effect = {
		add_core = 139
		add_core = 140
	}
}

control_medina = {
	
	type = country

	allow = {
		tag = TUR
		is_lesser_in_union = no
		is_subject = no
		384 = {
			not = { owned_by = this }
			any_neighbor_province = { owned_by = this }
		}
	}
	abort = {
		or = {
			is_lesser_in_union = yes
			is_subject = yes		
		}
	}
	success = {
		owns = 384
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}
		modifier = {
			factor = 2
			384 = {
				owner = { not = { religion_group = muslim } }
			}
		}
	}
	immediate = {
		add_temp_claim = 384
	}
	abort_effect = {
		remove_temp_claim = 384
	}
	effect = {
		add_core = 384
	}
}

control_mecca = {
	
	type = country

	allow = {
		tag = TUR
		is_lesser_in_union = no
		is_subject = no
		385 = {
			not = { owned_by = this }
			any_neighbor_province = { owned_by = this }
		}
	}
	abort = {
		or = {
			is_lesser_in_union = yes
			is_subject = yes		
		}
	}
	success = {
		owns = 385
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}
		modifier = {
			factor = 2
			385 = {
				owner = { not = { religion_group = muslim } }
			}
		}
	}
	immediate = {
		add_temp_claim = 385
	}
	abort_effect = {
		remove_temp_claim = 385
	}
	effect = {
		prestige = 0.05
		missionaries = 3
	}
}

vassalize_candar = {

	type = country

	allow = {
		tag = TUR
		exists = CND
		is_lesser_in_union = no
		is_subject = no
		CND = {
			is_lesser_in_union = no
			neighbour = this
			not = { vassal_of = TUR }
			not = { num_of_cities = this }
			is_subject = no
		}
	}
	abort = {
		or = {
			not = { exists = CND }
			CND = { is_lesser_in_union = yes }
			is_lesser_in_union = yes
			is_subject = yes
		}
	}
	success = {
		CND = { vassal_of = TUR }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = CND value = 0 } }
		}
	}
	immediate = {
		casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = CND
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = CND
		}
	}
	effect = {
		add_core = 325
		army_tradition = 0.1
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = CND
		}
	}
}

annex_candar = {
	
	type = country

	allow = {
		tag = TUR
		is_lesser_in_union = no
		is_subject = no
		exists = CND
		CND = {
			vassal_of = TUR
			neighbour = this
			not = { num_of_cities = this }
			owns = 325		# Kastamon
			religion_group = this
		}
	}
	abort = {
		or = {
			not = { exists = CND }
			CND = { 
			not = { religion_group = this } 
			}
			is_lesser_in_union = yes
			is_subject = yes
		}
	}
	success = {
			not = { exists = CND }
			owns = 325						
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			relation = { who = CND value = 100 }
		}
		modifier = {
			factor = 2
			relation = { who = CND value = 200 }
		}
	}
	immediate = {
		add_temp_claim = 325
	}
	abort_effect = {
		remove_temp_claim = 325
	}
	effect = {
		prestige = 0.1
	}
}

vassalize_karaman = {

	type = country

	allow = {
		tag = TUR
		exists = KAR
		is_lesser_in_union = no
		is_subject = no
		KAR = {
			is_lesser_in_union = no
			neighbour = this
			is_subject = no
			not = { vassal_of = TUR }
			not = { num_of_cities = this }
		}
	}
	abort = {
		or = {
			not = { exists = KAR }
			KAR = { is_lesser_in_union = yes }
			is_lesser_in_union = yes
			is_subject = yes
		}
	}
	success = {
		KAR = { vassal_of = TUR }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = KAR value = 0 } }
		}
	}
	immediate = {
		casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = KAR
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = KAR
		}
	}
	effect = {
		add_core = 324
		army_tradition = 0.1
		
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = KAR
		}
	}
}

annex_karaman = {
	
	type = country

	allow = {
		tag = TUR
		is_lesser_in_union = no
		is_subject = no
		exists = KAR
		KAR = {
			vassal_of = TUR
			neighbour = this
			not = { num_of_cities = this }
			owns = 324		# Karaman
			religion_group = this
		}
	}
	abort = {
		or = {
			not = { exists = KAR }
			is_lesser_in_union = yes
			is_subject = yes
			KAR = { not = { religion_group = this } }			
		}
	}
	success = {
		not = { exists = KAR }
		owns = 324
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			relation = { who = KAR value = 100 }
		}
		modifier = {
			factor = 2
			relation = { who = KAR value = 200 }
		}
	}
	immediate = {
		add_temp_claim = 324
	}
	abort_effect = {
		remove_temp_claim = 324
	}
	effect = {
		prestige = 0.1
	}
}

vassalize_dulkadir = {

	type = country

	allow = {
		tag = TUR
		exists = DUL
		is_lesser_in_union = no
		is_subject = no
		DUL = {
			is_lesser_in_union = no
			neighbour = this
			is_subject = no
			not = { vassal_of = TUR }
			not = { num_of_cities = this }
		}
	}
	abort = {
		or = {
			not = { exists = DUL }
			DUL = { is_lesser_in_union = yes }
			is_lesser_in_union = yes
			is_subject = yes
		}
	}
	success = {
		DUL = { vassal_of = TUR }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = DUL value = 0 } }
		}
	}
	immediate = {
		casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = DUL
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = DUL
		}
	}
	effect = {
		infamy = -5
		army_tradition = 0.1
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = DUL
		}
	}
}

retake_smyrna = {
	
	type = country

	allow = {
		tag = TUR
		318 = { is_core = this }
		exists = AYD
		AYD = { owns = 318 }
		is_lesser_in_union = no
		is_subject = no
	}
	abort = {
		or = {
			and = {
				AYD = { not = { owns = 318 } }
				not = { owns = 318 }
			}
			is_lesser_in_union = yes
			is_subject = yes			
		}
	}
	success = {
		owns = 318
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = AYD value = 0 } }
		}
	}
	effect = {
		infamy = -5
	}
}

vassalize_wallachia = {

	type = country

	allow = {
		tag = TUR
		exists = WAL
		is_lesser_in_union = no
		is_subject = no
		WAL = {
			is_lesser_in_union = no
			neighbour = this
			is_subject = no
			not = { vassal_of = TUR }
			not = { num_of_cities = this }
			owns = 161
		}
	}
	abort = {
		or = {
			not = { exists = WAL }
			WAL = { is_lesser_in_union = yes }
			is_lesser_in_union = yes
			is_subject = yes		
		}
	}
	success = {
		WAL = { vassal_of = TUR }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = WAL value = 0 } }
		}
	}
	immediate = {
		casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = WAL
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = WAL
		}
	}
	effect = {
		add_core = 161
		army_tradition = 0.1
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = WAL
		}
	}
}

vassalize_transylvania = {

	type = country

	allow = {
		tag = TUR
		exists = TRA
		is_lesser_in_union = no
		is_subject = no
		TRA = {
			is_lesser_in_union = no
			neighbour = this
			is_subject = no
			not = { vassal_of = TUR }
			not = { num_of_cities = this }
			owns = 158
		}
	}
	abort = {
		or = {
			not = { exists = TRA }
			TRA = { is_lesser_in_union = yes }
			is_lesser_in_union = yes
			is_subject = yes
		}
	}
	success = {
		TRA = { vassal_of = TUR }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = TRA value = 0 } }
		}
	}
	immediate = {
		casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = TRA
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = TRA
		}
	}
	effect = {
		add_core = 158
		army_tradition = 0.1
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = TRA
		}
	}
}

defeat_the_knights = {

	type = country

	allow = {
		tag = TUR
		exists = KNI
		KNI = { owns = 320 }		# Rhodes
		is_lesser_in_union = no
		is_subject = no
	}
	abort = {
		or = {
			and = {
				KNI = { not = { owns = 320 } }
				not = { owns = 320 }
			}
			is_lesser_in_union = yes
			is_subject = yes
		}
	}
	success = {
		owns = 320
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			war_with = KNI
		}
	}
	immediate = {
		add_temp_claim = 320
	}
	abort_effect = {
		remove_temp_claim = 320
	}
	effect = {
		prestige = 0.08
	}
}

take_crete_from_venice = {

	type = country

	allow = {
		tag = TUR
		exists = VEN
		war_with = VEN
		VEN = { owns = 163 }
		is_lesser_in_union = no
		is_subject = no
	}
	abort = {
		or = {
			and = {
				VEN = { not = { owns = 163 } }
				not = { owns = 163 }
			}
			is_lesser_in_union = yes
			is_subject = yes
		}
	}
	success = {
		owns = 163
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}
	}
	immediate = {
		add_temp_claim = 163
	}
	abort_effect = {
		remove_temp_claim = 163
	}
	effect = {
		prestige = 0.08
	}
}

muslim_asia_minor = {
	
	type = country

	allow = {
		tag = TUR
		war = no
		not = { exists = BYZ }
		asia_minor = { 
			owned_by = THIS
			religion_group = christian 
		}
		owns = 151
	}
	abort = {
		not = { religion_group = muslim  }
	}
	success = {
		not = { 
			asia_minor = { 
				owned_by =  THIS
				not = { religion_group = muslim  }
			} 
		}
		151 = { religion_group = muslim }
	}

	chance = {
		factor = 1000
		modifier = {
			factor = 2
			exists = TRE
		}
		modifier = {
			factor = 2
			idea = deus_vult
		}
	}
	effect = {
		army_tradition = 0.3
		infamy = -5
	}
}

vassalize_crimea = {

	type = country

	allow = {
		tag = TUR
		exists = CRI
		is_lesser_in_union = no
		is_subject = no
		CRI = {
			neighbour = this
			is_subject = no
			not = { vassal_of = TUR }
			not = { num_of_cities = 8 }
		}
	}
	abort = {
		or = {
			not = { exists = CRI }
			CRI = { is_lesser_in_union = yes }
			CRI = { num_of_cities = 8 }
			is_lesser_in_union = yes
			is_subject = yes
		}
	}
	success = {
		CRI = { vassal_of = TUR }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = CRI value = 0 } }
		}
	}
	immediate = {
		casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = CRI
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = CRI
		}
	}
	effect = {
		prestige = 0.1
		army_tradition = 0.1
	}
}

annex_egypt = {

type = country

allow = {
	tag = TUR
	owns = 151
	num_of_cities = 15
	is_lesser_in_union = no
	is_subject = no
	any_neighbor_country = { tag = MAM }
}
abort = {
	OR = {
		NOT = { any_neighbor_country = { tag = MAM } }
		is_lesser_in_union = yes
		is_subject = yes
	}
	MAM = { vassal_of = TUR }
}
success = {
	war_with = MAM
	358 = { NOT = { controlled_by = MAM } }
	361 = { NOT = { controlled_by = MAM } }
	379 = { NOT = { controlled_by = MAM } }
	382 = { NOT = { controlled_by = MAM } }
	MAM = { capital_scope = { controlled_by = TUR } }
}
chance = {
	factor = 1000
	modifier = {
		factor = 2
		is_monarch_leader = yes
	}
	modifier = {
		factor = 5
		year = 1500
	}
}
immediate = {
	any_province = {
		limit = { is_core = MAM }
		add_temp_claim = THIS
	}
}
abort_effect = {
	any_province = {
		limit = { is_core = MAM }
		remove_temp_claim = THIS
	}
}
effect = {
	prestige = 0.1
	inherit = MAM
	badboy = -10
	358 = { add_core = THIS }
	361 = { add_core = THIS }
	379 = { add_core = THIS }
	382 = { add_core = THIS }
	}
}

take_kaffa = {
	
	type = country

	allow = {
		tag = TUR
		not = { owns = 2170 }		# Kaffa
		is_lesser_in_union = no
		is_subject = no
	}
	abort = {
		or = {
			is_lesser_in_union = yes
			is_subject = yes		
		}
	}
	success = {
		owns = 2170
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}
	}
	immediate = {
		add_temp_claim = 361
	}
	abort_effect = {
		remove_temp_claim = 361
	}
	effect = {
		prestige = 0.05
	}
}

vassalize_moldavia = {

	type = country

	allow = {
		tag = TUR
		exists = MOL
		is_lesser_in_union = no
		is_subject = no
		MOL = {
			is_lesser_in_union = no
			neighbour = this
			is_subject = no
			not = { vassal_of = TUR }
			not = { num_of_cities = this }
			owns = 162
		}
	}
	abort = {
		or = {
			not = { exists = MOL }
			WAL = { is_lesser_in_union = yes }
			is_lesser_in_union = yes
			is_subject = yes		
		}
	}
	success = {
		MOL = { vassal_of = TUR }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = MOL value = 0 } }
		}
	}
	effect = {
		add_core = 1263
		prestige = 0.1
		army_tradition = 0.1
	}
}
