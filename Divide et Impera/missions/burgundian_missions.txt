conquer_hainaut = {
	
	type = country

	allow = {
		tag = BUR
		is_lesser_in_union = no
		is_subject = no
		exists = HAI
		HAI = { owns = 91 }		# Hainaut
	}
	abort = {
		or = {
			not = { exists = HAI }
			is_subject = yes
			is_lesser_in_union = yes
		}
	}
	success = {
		owns = 91
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = HAI value = 0 } }
		}	
	}
	immediate = {
		add_temp_claim = 91
	}
	abort_effect = {
		remove_temp_claim = 91
	}
	effect = {
		prestige = 0.05
		add_core = 91
	}
}

conquer_brabant = {
	
	type = country

	allow = {
		tag = BUR
		is_lesser_in_union = no
		is_subject = no
		exists = BRB
		BRB = { owns = 92 }		# Brabant
	}
	abort = {
		or = {
			not = { exists = BRB }
			is_subject = yes
			is_lesser_in_union = yes
		}
	}
	success = {
		owns = 92
		not = { exists = BRB }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = BRB value = 0 } }
		}	
	}
	immediate = {
		add_temp_claim = 92
		add_temp_claim = 95
	}
	abort_effect = {
		remove_temp_claim = 92
		remove_temp_claim = 95
	}
	effect = {
		any_owned = {
			limit = {
				previous_owner = BRB
				NOT = { is_core = THIS }
			}
			add_core = THIS
			add_province_modifier = {
				name = integrating_new_core
				duration = 3650
			}
		}
	}
}

conquer_luxemburg = {
	
	type = country

	allow = {
		tag = BUR
		is_lesser_in_union = no
		is_subject = no
		exists = LUX
		LUX = { owns = 94 }		# Luxemburg	
	}
	abort = {
		or = {
			not = { exists = LUX }
			is_subject = yes
			is_lesser_in_union = yes
		}
	}
	success = {
		owns = 94
		not = { exists = LUX }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = LUX value = 0 } }
		}	
	}
	immediate = {
		add_temp_claim = 94
		add_temp_claim = 1915
	}
	abort_effect = {
		remove_temp_claim = 94
		remove_temp_claim = 1915
	}
	effect = {
		treasury = 40
		add_core = 94
		add_core = 1915
	}
}

conquer_liege = {
	
	type = country

	allow = {
		tag = BUR
		is_lesser_in_union = no
		is_subject = no
		exists = LIE
		LIE = { owns = 93 }		# Li�ge
	}
	abort = {
		or = {
			not = { exists = LIE }
			is_subject = yes
			is_lesser_in_union = yes
		}
	}
	success = {
		owns = 93
		not = { exists = LIE }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = LIE value = 0 } }
		}	
	}
	immediate = {
		add_temp_claim = 93
	}
	abort_effect = {
		remove_temp_claim = 93
	}
	effect = {
		any_owned = {
			limit = {
				previous_owner = LIE
				NOT = { is_core = THIS }
			}
			add_core = THIS
			add_province_modifier = {
				name = integrating_new_core
				duration = 3650
			}
		}
	}
}

vassalize_lorraine = {
	
	type = country

	allow = {
		tag = BUR
		is_lesser_in_union = no
		exists = LOR
		is_subject = no
		LOR = {
			is_subject = no
			neighbour = this
			not = { vassal_of = BUR }
			not = { num_of_cities = this }
			is_lesser_in_union = no
		}
	}
	abort = {
		or = {
			not = { exists = LOR }
			LOR = { is_lesser_in_union = yes }
			is_lesser_in_union = yes
			is_subject = yes
		}
	}
	success = {
		LOR = { vassal_of = BUR }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = LOR value = 0 } }
		}
	}
	immediate = {
		casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = LOR
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = LOR
		}
	}
	effect = {
		army_tradition = 0.1
		stability = 1
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = LOR
		}
	}
}
