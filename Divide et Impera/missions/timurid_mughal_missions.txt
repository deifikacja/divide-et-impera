# Timurid
conquer_delhi = {
	
	type = country

	allow = {
		tag = TIM
		exists = DLH
		is_subject = no
		is_lesser_in_union = no
		DLH = {
			neighbour = this
			owns = 522		# Delhi
		}
	}
	abort = {
		or = {
			is_subject = yes
			is_lesser_in_union = yes
			and = {
				not = { exists = DLH }
				not = { owns = 506 }
				not = { owns = 507 }
				not = { owns = 510 }
				not = { owns = 511 }
				not = { owns = 521 }
				not = { owns = 522 }
				not = { owns = 523 }
				not = { owns = 524 }
				not = { owns = 578 }
				not = { owns = 740 }
			}
		}
	}
	success = {
		owns = 506 # Multan
		owns = 507 # Lahore
		owns = 510 # Chandigarh
		owns = 511 # Sutlej
		owns = 521 # Panipat
		owns = 522 # Delhi
		owns = 523 # Lucknow
		owns = 524 # Agra
		owns = 578 # Kohistan
		owns = 740 # Ladakh
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			year = 1526
		}
		modifier = {
			factor = 2
			TIM = { infantry = this cavalry = this }
		}
	}
	immediate = {
		add_temp_claim = 506
		add_temp_claim = 507
		add_temp_claim = 510
		add_temp_claim = 511
		add_temp_claim = 521
		add_temp_claim = 522
		add_temp_claim = 523
		add_temp_claim = 524
		add_temp_claim = 578
		add_temp_claim = 740
	}
	abort_effect = {
		remove_temp_claim = 506
		remove_temp_claim = 507
		remove_temp_claim = 510
		remove_temp_claim = 511
		remove_temp_claim = 521
		remove_temp_claim = 522
		remove_temp_claim = 523
		remove_temp_claim = 524
		remove_temp_claim = 578
		remove_temp_claim = 740
	}
	effect = {
		prestige = 0.25
		add_core = 522 # Delhi
		add_core = 578 # Kohistan
		add_core = 507 # Lahore
		add_core = 508 # Multan
		add_core = 510 # Chandigarh
		add_core = 511 # Sutlej
		add_core = 521 # Panipat
		add_core = 523 # Lucknow
		add_core = 524 # Agra
		add_core = 740 # Ladakh
	}
}

fund_the_taj_mahal = {
	
	type = country

	allow = {
		tag = MUG
		not = { year = 1648 }
		treasury = 50
		not = { has_country_flag = taj_mahal_exists }
	}
	abort = {
		number_of_loans = 1
	}
	success = {
		treasury = 200
		advisor = artist
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			adm = 7
		}
		modifier = {
			factor = 2
			adm = 9
		}
	}
	effect = {
		prestige = 0.1
		set_country_flag = taj_mahal_exists
	}
}

vassalize_golcanda = {
	
	type = country

	allow = {
		tag = MUG
		exists = GOC
		is_subject = no
		is_lesser_in_union = no
		not = { has_country_flag = vassalize_golcanda_done }
		GOC = {
			neighbour = this
			not = { vassal_of = MUG }
			not = { num_of_cities = this }
			owns = 543
			is_subject = no
		}
	}
	abort = {
		or = {
			not = { exists = GOC }
			GOC = { is_lesser_in_union = yes }
			is_subject = yes
			is_lesser_in_union = yes
		}
	}
	success = {
		GOC = { vassal_of = MUG }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = GOC value = 0 } }
		}
	}
	immediate = {
		casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = GOC
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = GOC
		}
	}
	effect = {
		set_country_flag = vassalize_golcanda_done
		add_core = 543
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = GOC
		}
	}
}

vassalize_gondwana = {
	
	type = country

	allow = {
		tag = MUG
		exists = GDW
		is_subject = no
		is_lesser_in_union = no
		not = { has_country_flag = vassalize_gondwana_done }
		GDW = {
			neighbour = this
			not = { vassal_of = MUG }
			not = { num_of_cities = this }
			is_subject = no
		}
	}
	abort = {
		or = {
			not = { exists = GDW }
			GDW = { is_lesser_in_union = yes }
			is_subject = yes
			is_lesser_in_union = yes
		}
	}
	success = {
		GDW = { vassal_of = MUG }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = GDW value = 0 } }
		}
	}
	immediate = {
		casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = GDW
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = GDW
		}
	}
	effect = {
		set_country_flag = vassalize_gondwana_done
		stability = 3
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = GDW
		}
	}
}

defend_the_mughal_borders = {
	
	type = country

	allow = {
		year = 1649
		tag = MUG
		exists = PER
		war_with = PER
		PER = { neighbour = this }
		owns = 575
		owns = 576
		owns = 577
		owns = 448
		owns = 451
		owns = 450
	}
	abort = {
		or = {
			not = { exists = PER }
			not = { war_with = PER }
		}
	}
	success = {
		not = { war_with = PER }
		owns = 575
		owns = 576
		owns = 577
		owns = 448
		owns = 451
		owns = 450
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			is_monarch_leader = yes
		}
	}
	effect = {
		infamy = -5
		stability = 1
	}
}

mughal_break_the_persian_border = {
	
	type = country

	allow = {
		tag = MUG
		exists = PER
		war_with = PER
		PER = { neighbour = this }
		not = { persian_region = { owned_by = this } }
		is_subject = no
		is_lesser_in_union = no
	}
	abort = {
		or = {
			not = { exists = PER }
			not = { war_with = PER }
			is_subject = yes
			is_lesser_in_union = yes
		}
	}
	success = {
		not = { war_with = PER }
		persian_region = { owned_by = this }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			is_monarch_leader = yes
		}
	}
	immediate = {
		persian_region = {
			add_temp_claim = THIS
		}
	}
	abort_effect = {
		persian_region = {
			remove_temp_claim = THIS
		}
	}
	effect = {
		army_tradition = 0.5
		war_exhaustion = -5
		persian_region = {
			remove_temp_claim = THIS
		}
	}
}

vassalize_ahmadnagar = {
	
	type = country

	allow = {
		tag = MUG
		exists = BAS
		is_subject = no
		is_lesser_in_union = no
		not = { has_country_flag = vassalize_ahmadnagar_done }
		BAS = {
			neighbour = this
			not = { vassal_of = MUG }
			not = { num_of_cities = this }
			is_subject = no
		}
	}
	abort = {
		or = {
			not = { exists = BAS }
			BAS = { is_lesser_in_union = yes }
			is_subject = yes
			is_lesser_in_union = yes
		}
	}
	success = {
		BAS = { vassal_of = MUG }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = BAS value = 0 } }
		}
	}
	immediate = {
		casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = BAS
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = BAS
		}
	}
	effect = {
		set_country_flag = vassalize_ahmadnagar_done
		prestige = 0.05
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = BAS
		}
	}
}

conquer_khandesh = {

	type = country

	allow = {
		tag = MUG
		not = { owns = 527 }		# Burhanpur
		is_subject = no
		is_lesser_in_union = no
	}
	abort = {
		or = {
			is_subject = yes
			is_lesser_in_union = yes		
		}
	}
	success = {
		owns = 527
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}
	}
	immediate = {
		add_temp_claim = 527
	}
	abort_effect = {
		remove_temp_claim = 527
	}
	effect = {
		stability = 1
	}
}

annex_berar = {
	
	type = country

	allow = {
		tag = MUG
		exists = BRR
		is_subject = no
		is_lesser_in_union = no
		BRR = {
			vassal_of = MUG
			neighbour = this
			not = { num_of_cities = this }
			owns = 546		# Nagpur
			religion_group = this
		}
	}
	abort = {
		or = {
			not = { exists = BRR }
			is_subject = yes
			is_lesser_in_union = yes
			BRR = { not = { religion_group = this } }
		}
	}
	success = {
		not = { exists = BRR }
		owns = 546
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			relation = { who = BRR value = 100 }
		}
		modifier = {
			factor = 2
			relation = { who = BRR value = 200 }
		}
	}
	immediate = {
		add_temp_claim = 546
	}
	abort_effect = {
		remove_temp_claim = 546
	}
	effect = {
		add_core = 546
	}
}

vassalize_gujarat = {
	
	type = country

	allow = {
		tag = MUG
		exists = GUJ
		is_subject = no
		is_lesser_in_union = no
		not = { has_country_flag = vassalize_gujarat_done }
		GUJ = {
			neighbour = this
			not = { vassal_of = MUG }
			not = { num_of_cities = this }
			is_subject = no
		}
	}
	abort = {
		or = {
			not = { exists = GUJ }
			GUJ = { is_lesser_in_union = yes }
			is_subject = yes
			is_lesser_in_union = yes
		}
	}
	success = {
		GUJ = { vassal_of = MUG }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = GUJ value = 0 } }
		}
	}
	immediate = {
		casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = GUJ
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = GUJ
		}
	}
	effect = {
		set_country_flag = vassalize_gujarat_done
		prestige = 0.05
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = GUJ
		}
	}
}

vassalize_orissa = {
	
	type = country

	allow = {
		tag = MUG
		exists = ORI
		is_subject = no
		is_lesser_in_union = no
		not = { has_country_flag = vassalize_orissa_done }
		ORI = {
			neighbour = this
			not = { vassal_of = MUG }
			not = { num_of_cities = this }
			is_subject = no
		}
	}
	abort = {
		or = {
			not = { exists = ORI }
			ORI = { is_lesser_in_union = yes }
			is_subject = yes
			is_lesser_in_union = yes
		}
	}
	success = {
		ORI = { vassal_of = MUG }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = ORI value = 0 } }
		}
	}
	immediate = {
		casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = ORI
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = ORI
		}
	}
	effect = {
		set_country_flag = vassalize_orissa_done
		prestige = 0.05
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = ORI
		}
	}
}

defeat_england = {

	type = country
	
	allow = {
		tag = MUG
		exists = ENG
		indian_region = { owned_by = ENG }
		war_with = ENG
		is_subject = no
		is_lesser_in_union = no
		ENG = {
			neighbour = this
			is_subject = no
			is_lesser_in_union = no
		}

	}
	abort = {
		or = {
			is_subject = yes
			is_lesser_in_union = yes
			not = { neighbour = ENG }
			not = { war_with = ENG }
			ENG = {
				or = {
					is_subject = yes
					is_lesser_in_union = yes
				}
			}
		}
	}
	success = {
		not = { war_with = ENG }
		not = { indian_region = { owned_by = ENG } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { dip = 4 }
		}
		modifier = {
			factor = 2
			not = { dip = 2 }
		}
	}
	immediate = {
		indian_region = {
			limit = { owned_by = ENG }
			add_temp_claim = THIS
		}
	}
	abort_effect = {
		indian_region = {
			limit = { owned_by = ENG }
			remove_temp_claim = THIS
		}
	}
	effect = {
		army_tradition = 0.5
		stability = 2
		indian_region = {
			limit = { owned_by = ENG }
			remove_temp_claim = THIS
		}
	}
}

defend_timurid_lands = {
	
	type = country

	allow = {
		tag = TIM
		exists = SHY
		war_with = SHY
		owns = 437
		owns = 438
		owns = 440
		owns = 441
		owns = 442
		owns = 444
		owns = 445
		owns = 450
		owns = 451
		owns = 452
		owns = 453
	}
	abort = {
		or = {
			not = { exists = SHY }
			not = { war_with = SHY }
		}
	}
	success = {
		not = { war_with = SHY }
		owns = 437
		owns = 438
		owns = 440
		owns = 441
		owns = 442
		owns = 444
		owns = 445
		owns = 450
		owns = 451
		owns = 452
		owns = 453
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}
		modifier = {
			factor = 2
			PER = { not = { mil = 5 } }
		}
	}
	effect = {
		army_tradition = 0.5
		stability = 1
	}
}

defend_timurid_lands_against_persia = {
	
	type = country

	allow = {
		or = {
			tag = KHO
			tag = TIM
		}
		exists = PER
		war_with = PER
		owns = 431
		owns = 432
		owns = 434
		owns = 435
		owns = 436
		owns = 437
		owns = 445
		owns = 446
	}
	abort = {
		or = {
			not = { exists = PER }
			not = { war_with = PER }
		}
	}
	success = {
		not = { war_with = PER }
		owns = 431
		owns = 432
		owns = 434
		owns = 435
		owns = 436
		owns = 437
		owns = 445
		owns = 446
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}
		modifier = {
			factor = 2
			PER = { not = { mil = 5 } }
		}
	}
	effect = {
		army_tradition = 0.5
		stability = 1
	}
}

timurid_break_the_persian_border = {
	
	type = country

	allow = {
		or = {
			tag = KHO
			tag = TIM
		}
		exists = PER
		war_with = PER
		PER = { neighbour = this }
		is_subject = no
		is_lesser_in_union = no
		not = { persian_region = { owned_by = this } }
	}
	abort = {
		or = {
			not = { exists = PER }
			not = { war_with = PER }
			is_subject = yes
			is_lesser_in_union = yes
		}
	}
	success = {
		not = { war_with = PER }
		persian_region = { owned_by = this }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			is_monarch_leader = yes
		}
	}
	immediate = {
		persian_region = {
			add_temp_claim = THIS
		}
	}
	abort_effect = {
		persian_region = {
			remove_temp_claim = THIS
		}
	}
	effect = {
		army_tradition = 0.5
		war_exhaustion = -5
		persian_region = {
			remove_temp_claim = THIS
		}
	}
}

conquer_sivas = {
	
	type = country

	allow = {
		tag = TIM
		not = { owns = 329 }		# Sivas
		not = { relation = { who = TUR value = 0 } }	
	}
	abort = {
		not = { exists = TUR }
	}
	success = {
		owns = 329
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			TUR = { not = { mil = 4 } }
		}
		modifier = {
			factor = 2
			mil = 7
		}
	}
	immediate = {
		add_temp_claim = 329
	}
	abort_effect = {
		remove_temp_claim = 329
	}
	effect = {
		army_tradition = 0.1
	}
}

annex_the_jalayrids = {
	
	type = country

	allow = {
		tag = TIM
		exists = JAI
		is_subject = no
		is_lesser_in_union = no
		JAI = {
			vassal_of = TIM
			neighbour = this
			not = { num_of_cities = this }
			owns = 410		# Baghdad
			religion_group = this
		}
	}
	abort = {
		or = {
			not = { exists = JAI }
			is_subject = yes
			is_lesser_in_union = yes
			JAI = { not = { religion_group = this } }
		}
	}
	success = {
		not = { exists = JAI }
		owns = 410
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			relation = { who = JAI value = 100 }
		}
		modifier = {
			factor = 2
			relation = { who = JAI value = 200 }
		}
	}
	immediate = {
		add_temp_claim = 410
	}
	abort_effect = {
		remove_temp_claim = 410
	}
	effect = {
		prestige = 0.1
	}
}

vassalize_the_akkoyunlu = {
	
	type = country

	allow = {
		tag = TIM
		exists = AKK
		is_subject = no
		is_lesser_in_union = no
		not = { has_country_flag = vassalize_the_akkoyunlu_done }
		AKK = {
			neighbour = this
			not = { vassal_of = TIM }
			not = { num_of_cities = this }
			is_subject = no
		}
	}
	abort = {
		or = {
			not = { exists = AKK }
			AKK	= { is_lesser_in_union = yes }
			is_subject = yes
			is_lesser_in_union = yes
		}
	}
	success = {
		AKK = { vassal_of = TIM }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = AKK value = 0 } }
		}
	}
	immediate = {
		casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = AKK
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = AKK
		}
	}
	effect = {
		set_country_flag = vassalize_the_akkoyunlu_done
		prestige = 0.05
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = AKK
		}
	}
}

vassalize_the_qara_qoyunlu = {
	
	type = country

	allow = {
		tag = TIM
		exists = QAR
		is_subject = no
		is_lesser_in_union = no
		not = { has_country_flag = vassalize_the_qara_qoyunlu_done }
		
		QAR = {
			neighbour = this
			not = { vassal_of = QAR }
			not = { num_of_cities = this }
			is_subject = no
		}
	}
	abort = {
		or = {
			not = { exists = QAR }
			QAR = { is_lesser_in_union = yes }
			is_subject = yes
			is_lesser_in_union = yes
		}
	}
	success = {
		QAR = { vassal_of = TIM }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = QAR value = 0 } }
		}
	}
	immediate = {
		casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = QAR
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = QAR
		}
	}
	effect = {
		set_country_flag = vassalize_the_qara_qoyunlu_done
		prestige = 0.05
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = QAR
		}
	}
}

vassalize_georgia = {
	
	type = country

	allow = {
		tag = TIM
		exists = GEO
		is_subject = no
		is_lesser_in_union = no
		not = { has_country_flag = vassalize_georgia_done }
		GEO = {
			neighbour = this
			is_subject = no
			not = { vassal_of = TIM }
			not = { num_of_cities = this }
		}
	}
	abort = {
		or = {
			not = { exists = GEO }
			GEO = { is_lesser_in_union = yes }
			is_subject = yes
			is_lesser_in_union = yes
		}
	}
	success = {
		GEO = { vassal_of = TIM }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = GEO value = 0 } }
		}
	}
	immediate = {
		casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = GEO
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = GEO
		}
	}
	effect = {
		set_country_flag = vassalize_georgia_done
		prestige = 0.05
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = GEO
		}
	}
}
