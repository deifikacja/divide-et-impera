# take certain province
# build up naval tradition.
# build up army tradition.
# destroy neigboring colonies
# capture neighboring colonies.


blockade_enemy_coast = {
	type = enemy_provinces
	allow = {
		THIS = {
			naval_tech = 9
			num_of_ports = 1
			big_ship = 1
			NOT = { last_mission = blockade_enemy_coast }
		}
		controlled_by = owner
		has_siege = no
		port = yes
		is_blockaded = no
	}
	abort = {
		OR = {
			NOT = { controlled_by = owner }
			NOT = { has_siege = no }
			owner = { NOT = { war_with = THIS } }
			THIS = {
				NOT = {
					num_of_ports = 1
					big_ship = 1
				}
			}
		}
	}
	success = {
		is_blockaded = yes
	}
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { continent = THIS }
		}		
		modifier = {
			factor = 0.1
			THIS = { NOT = { big_ship = 30 } }
		}
	}
	effect = {
		treasury = 20
	}
}



break_blockade = {
	type = our_provinces
	allow = {
		port = yes
		is_blockaded = yes
		controlled_by = owner
		THIS = { big_ship = 1 }
	}
	abort = {
		OR = {
			NOT = { controlled_by = owner }
			NOT = {
				THIS = { big_ship = 1 }
			}
		}
	}
	success = {
		NOT = { is_blockaded = yes }
	}
	chance = {
		factor = 1
	}
	effect = {
		navy_tradition = 0.02
	}
}

retake_occupied_mission = {
	type = our_provinces
	allow = {
		NOT = { controlled_by = THIS }
		has_siege = yes
	}
	abort = {
	}
	success = {
		controlled_by = THIS
		has_siege = no
	}
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
			THIS = { MIL = 6 }
		}	
		modifier = {
			factor = 1.5
			is_core = THIS
		}	
	}
	effect = {
		army_tradition = 0.02
	}
}

not_lose_capital_in_war = {
	type = war_countries
	allow = {
		war_with = THIS
		THIS = { capital_scope = { controlled_by = THIS } }
		infantry = THIS
		neighbour = THIS
	}
	abort = {
		NOT = { THIS = { capital_scope = { controlled_by = THIS }} }
	}
	success = {
		NOT = { war_with = THIS }
	}
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
			MIL = 6
		}		
	}
	effect = {
		THIS = { war_exhaustion = -3 }
	}
}

lift_siege_mission = {
	type = our_provinces
	allow = {
		controlled_by = THIS
		has_siege = yes
		owner = {
			NOT = { last_mission = lift_siege_mission }
		}
	}
	abort = {
		NOT = {	controlled_by = THIS }
	}
	success = {
		controlled_by = THIS
		has_siege = no
	}
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
			THIS = { MIL = 6 }
		}	
		modifier = {
			factor = 1.5
			is_core = THIS
		}			
	}
	effect = {
		war_exhaustion = -0.25
	}
}
