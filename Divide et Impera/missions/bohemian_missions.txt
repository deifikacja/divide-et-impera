bohemia_silesia_relations = {
	
	type = country

	allow = {
		tag = BOH
		exists = OPP
		not = { war_with = OPP }
		not = { relation = { who = OPP value = 150 } }
	}
	abort = {
		or = {
			not = { exists = OPP }
			war_with = OPP
		}
	}
	success = {
		relation = { who = OPP value = 150 }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = OPP value = 0 } }
		}
		modifier = {
			factor = 2
			not = { relation = { who = OPP value = -100 } }
		}	
	}
	effect = {
		infamy = -5
	}
}

diplo_annex_silesia = {
	
	type = country

	allow = {
		tag = BOH
		is_subject = no
		is_lesser_in_union = no
		exists = OPP
		OPP = {
			vassal_of = BOH
			neighbour = this
			not = { num_of_cities = this }
			owns = 263		# Ratibor
		}
	}
	abort = {
		not = { exists = OPP }
	}
	success = {
		not = { exists = OPP }
		owns = 263		# Ratibor
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			relation = { who = OPP value = 100 }
		}
		modifier = {
			factor = 2
			relation = { who = OPP value = 200 }
		}
	}
	effect = {
		prestige = 0.1
	}
}

bohemia_hungary_relations = {
	
	type = country

	allow = {
		tag = BOH
		exists = HUN
		not = { war_with = HUN }
		not = { relation = { who = HUN value = 150 } }
	}
	abort = {
		or = {
			not = { exists = HUN }
			war_with = HUN
		}
	}
	success = {
		relation = { who = HUN value = 150 }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = HUN value = 0 } }
		}
		modifier = {
			factor = 2
			not = { relation = { who = HUN value = -100 } }
		}	
	}
	effect = {
		stability = 1
	}
}

defend_bohemia_against_hungary = {
	
	type = country

	allow = {
		tag = BOH
		exists = HUN
		war_with = HUN
	}
	abort = {
		or = {
			not = { war_with = HUN }
			not = { exists = HUN }
		}
	}
	success = {
		not = { war_with = HUN }
		not = { bohemia = { owned_by = HUN } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}
		modifier = {
			factor = 2
			HUN = { not = { mil = 5 } }
		}
	}
	effect = {
		army_tradition = 0.3
		stability = 1
	}
}

bohemia_austria_relations = {
	
	type = country

	allow = {
		tag = BOH
		exists = HAB
		not = { war_with = HAB }
		not = { relation = { who = HAB value = 150 } }
	}
	abort = {
		or = {
			not = { exists = HAB }
			war_with = HAB
		}
	}
	success = {
		relation = { who = HAB value = 150 }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = HAB value = 0 } }
		}
		modifier = {
			factor = 2
			not = { relation = { who = HAB value = -100 } }
		}	
	}
	effect = {
		infamy = -1
	}
}

