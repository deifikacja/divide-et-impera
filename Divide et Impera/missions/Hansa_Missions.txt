sweden_to_the_fold = {
	type = country
		
	allow = {
		OR = { tag = HSA tag = HAM tag = LBK }
		government = major_merchant_republic
		exists = SWE
		is_subject = no
		is_lesser_in_union = no
		
		NOT = {
			trade_agreement_with = SWE
		}
		
	}
	
	abort = {
		is_subject = yes
		is_lesser_in_union = yes
		NOT = {
			exists = SWE
		}
		NOT = {
			government = major_merchant_republic
		}
	}
	
	success = {
		trade_agreement_with = SWE
	}
	chance = {
		factor = 1000
	}
	effect = {
		merchants = 2
		treasury = 10
		prestige = 0.1
	}
}

england_to_the_fold = {
	type = country
		
	allow = {
		OR = { tag = HSA tag = HAM tag = LBK }
		government = major_merchant_republic
		exists = ENG
		is_subject = no
		is_lesser_in_union = no
		NOT = {
			trade_agreement_with = ENG
		}
	}
	
	abort = {
		is_subject = yes
		is_lesser_in_union = yes
		NOT = {
			exists = ENG
		}
		NOT = {
			government = major_merchant_republic
		}
	}
	
	success = {
		trade_agreement_with = ENG
	}
	chance = {
		factor = 1000
	}
	effect = {
		treasury = 20
		merchants = 3
		prestige = 0.1
	}
}

save_the_league = {
	type = country
	
	allow = {
		OR = { tag = HSA tag = HAM tag = LBK }
		government = major_merchant_republic
		NOT = {
			is_subject = yes
			is_lesser_in_union = yes
		}
		num_of_league_members = 2
		NOT = {
			num_of_league_members = 3
		}
	}
	abort = {
		is_subject = yes
		is_lesser_in_union = yes
		NOT = {
			government = major_merchant_republic
		}
	}
	success = {
		num_of_league_members = 6
	}
	chance = {
		factor = 5000
	}
	effect = {
		treasury = 50
		merchants = 3
		prestige = 0.1
	}
}

end_sound_toll = {
	type = country
	
	allow = {
		OR = { tag = HSA tag = HAM tag = LBK }
		government = major_merchant_republic
		not = {
			45 = { has_province_modifier = free_shipping_through_the_sound }
			is_subject = yes
			is_lesser_in_union = yes
		}
		any_known_country = { has_country_modifier = sound_toll }
	}
	abort = {
		NOT = {	government = major_merchant_republic }
		is_subject = yes
		is_lesser_in_union = yes
	}
	success = {
		OR = {
			AND = {
				controls = 6			# Skane
				controls = 12			# Sjaelland
				controls = 14			# Fyn
			}
			any_known_country = {
				has_country_modifier = sound_toll
				relation = { who = THIS value = 190 }
				trade_agreement_with = this
			}	
		}
	}
	chance = {
		factor = 5000
	}
	effect = {
		45 = {							# Lubeck
			add_province_modifier = {
				name = "free_shipping_through_the_sound"
				duration = 13000
			}
		}
	}
}

gain_swedish_iron = {
	type = country
	
	allow = {
		OR = { tag = HSA tag = HAM tag = LBK }
		owns = 45
		45 = {
			cot = yes
		}
		government = major_merchant_republic
		trade_agreement_with = SWE  # Sweden is in your trade league
		NOT = { trade_rights = { tag = SWE } }
		exists = SWE
		SWE = {
			iron = 1
		}
	}
	abort = {
		OR = {
			NOT = { government = major_merchant_republic }
			NOT = { exists = SWE }
			NOT = { trade_agreement_with = SWE }
			AND = {
				NOT = {
					trade_rights = {
						tag = SWE
						trade_goods = iron
					}
				}
				trade_rights = { tag = SWE }
			}
			NOT = { SWE = { iron = 1 } }
		}
	}
	success = {
		trade_rights = {
			tag = SWE
			trade_goods = iron
		}
	}
	chance = {
		factor = 2000
	}
	effect = {
		treasury = 50
		merchants = 2
	}
}

gain_polish_grain_trade = {
	type = country
	
	allow = {
		OR = { tag = HSA tag = HAM tag = LBK }
		owns = 45
		exists = POL
		POL = {
			grain = 1
		}
		45 = {
			cot = yes
		}
		government = major_merchant_republic
		trade_agreement_with = POL
		NOT = {
			trade_rights = {
				tag = POL
				#trade_goods = grain
			}
			has_country_modifier = polish_grain_trade
		}
	}
	abort = {
		OR = {
			NOT = { government = major_merchant_republic }
			NOT = { exists = POL }
			NOT = { POL = {	grain = 1 } }
			NOT = { trade_agreement_with = POL }
			AND = {
				NOT = {
					trade_rights = {
						tag = POL
						trade_goods = grain
					}
				}
				trade_rights = {
					tag = POL
				}
			}
		}
		
	}
	success = {
		trade_rights = {
			tag = POL
			trade_goods = grain
		}
	}
	chance = {
		factor = 2600
	}
	effect = {
		add_country_modifier = {
			name = "polish_grain_trade"
			duration = -1
		}
	}
}