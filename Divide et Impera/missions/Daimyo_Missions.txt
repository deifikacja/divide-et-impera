# Restore Stability
restore_stability = {
	type = country
	
	allow = {
		is_daimyo = yes
		NOT = {
			stability = 1
		}
	}
	
	success = {
		stability = 1
	}
	
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = {
				stability = -2
			}
		}
		modifier = {
			factor = 2
			NOT = {
				stability = -1
			}
		}
		modifier = {
			factor = 2
			NOT = {
				stability = 0
			}
		}
	}
	
	effect = {
		prestige = 0.05
		years_of_income = 0.1
	}
}

view_the_hanami = {
	type = country
	
	allow = {
		is_daimyo = yes
		NOT = {
			cultural_tradition = 0.25
		}
	}
	
	success = {
		cultural_tradition = 0.25
	}
	
	chance = {
		factor = 1000
		modifier = {
			factor = 0.5
			NOT = {
				stability = -2
			}
		}
		modifier = {
			factor = 0.5
			war_exhaustion = 5
		}
		
		modifier = {
			factor = 0.5
			NOT = {
				stability = -1
			}
		}
	}
	
	effect = {
		prestige = 0.05
		cultural_tradition = 0.05
	}
}

improve_the_han = {
	type = country
	
	allow = {
		is_daimyo = yes
		inflation = 10
		NOT = { inflation = 15 }
	}
	
	success = {
		NOT = {
			inflation = 9
		}
	}
	
	abort = {
		inflation = 15
	}
	
	chance = {
		factor = 1000
	}
	
	effect = {
		inflation = -2
		years_of_income = 0.05
	}
}

restless_kami = {
	type = country
	
	allow = {
		is_daimyo = yes
		capital_scope = {
			has_national_focus = no
		}
		NOT = {
			stability = 1
		}
	}
	
	abort = {
		stability = 1
	}
	
	success = {
		capital_scope = {
			has_national_focus = yes
		}
	}
	
	chance = {
		factor = 1000
	}
	
	effect = {
		stability = 1
	}
}

sankin_kotai = {
	type = country
	
	allow = {
		is_daimyo = yes
		any_known_country = {
			is_shogun = yes
			relation = {
				who = THIS
				value = -150
			}
			NOT = {
				relation = {
					who = THIS
					value = 50
				}
			}
		}
	}
	
	abort = {
		any_known_country = {
			is_shogun = yes
			NOT = {
				relation = {
					who = THIS
					value = -150
				}
			}
		}
	}
	
	success = {
		treasury = 250
		officials = 3
	}
	
	chance = {
		factor = 1000
	}
	
	effect = {
		any_country = {
			limit = {
				is_shogun = yes
			}
			relation = {
				who = THIS
				value = 100
			}
		}
		treasury = -50
		officials = -3
	}
}

strengthen_the_bushido = {
	type = country
	
	allow = {
		is_daimyo = yes
		NOT = {
			army_tradition = 0.25
		}
	}
	
	success = {
		army_tradition = 0.25
	}
	
	chance = {
		factor = 1000
		modifier = {
			factor = 0.5
			army_tradition = 0.15
		}
		modifier = {
			factor = 0.5
			infamy = 6
		}
		modifier = {
			factor = 2
			NOT = {
				legitimacy = 0.5
			}
		}
	}
	
	effect = {
		cultural_tradition = 0.05
		prestige = 0.05
		legitimacy = 0.05
		infamy = -2
	}
}

support_the_kabuki = {
	type = country
	allow = {
		is_daimyo = yes
		shogun_exists = yes
		year = 1600
	}
	
	success = {
		advisor = artist
	}
	
	chance = {
		factor = 1000
		modifier = {
			factor = 0.8
			NOT = {
				cultural_tradition = 0.1
			}
		}
		modifier = {
			factor = 2
			cultural_tradition = 0.15
		}
	}
	
	effect = {
		cultural_tradition = 0.15
		legitimacy = 0.05
		prestige = 0.05
	}
}

the_red_seal_ships = {
	type = country
	
	allow = {
		is_daimyo = yes
		shogun_exists = yes
		NOT = {
			placed_merchants = 15
		}
		NOT = {
			idea = merchant_adventures
		}
	}
	
	success = {
		OR = {
			AND = {
				placed_merchants = 15
				idea = merchant_adventures
			}
			AND = {
				placed_merchants = 15
				advisor = trader
			}
		}
	}
	
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			num_of_cots = 2
		}
		modifier = {
			factor = 2
			placed_merchants = 10
		}
		modifier = {
			factor = 0.5
			infamy = 5
		}
	}
	
	effect = {
		years_of_income = 0.3
		merchants = 5
		navy_tradition = 0.1
	}
}

strengthen_the_shinobi = {
	type = country
	
	allow = {
		is_daimyo = yes
		NOT = {
			advisor = spymaster
		}
		any_owned_province = {
			NOT = {
				is_core = THIS
			}
		}
		NOT = {
			idea = espionage
		}
	}
	
	success = {
		advisor = spymaster
		idea = espionage
	}
	
	chance = {
		factor = 1000
		modifier = {
			factor = 0.5
			badboy = 0.25
		}
		modifier = {
			factor = 0.5
			badboy = 0.5
		}
	}
	
	effect = {
		random_owned = {
			limit = {
				NOT = {
					is_core = THIS
				}
			}
			add_core = THIS
		}
		spies = 5
		legitimacy = 0.05
		prestige = 0.05
		infamy = 1
	}
}

the_divine_wind = {
	type = country
	
	allow = {
		naval_forcelimit = 20
		is_daimyo = yes
		NOT = {
			big_ship = 10
			idea = grand_navy
		}
	}
	
	success = {
		OR = {
			big_ship = 15
			idea = grand_navy
		}
	}
	
	chance = {
		factor = 1000
		modifier = {
			factor = 0.8
			num_of_ports = 10
		}
		modifier = {
			factor = 0.8
			num_of_ports = 15
		}
	}
	
	effect = {
		navy_tradition = 0.2
		prestige = 0.1
		officials = 3
	}
}