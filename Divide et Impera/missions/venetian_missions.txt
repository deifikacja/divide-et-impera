conquer_friuli = {
	
	type = country

	allow = {
		tag = VEN
		is_subject = no
		is_lesser_in_union = no
		not = { owns = 111 }	# Friuli
		111 = {
			OR = {
				is_capital = no
				owner = {
					not = { num_of_cities = 2 }
				}
			}
		}
	}
	abort = {
		or = {
			is_subject = yes
			is_lesser_in_union = yes
		}
	}
	success = {
		owns = 111
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}	
	}
	immediate = {
		add_temp_claim = 111
	}
	abort_effect = {
		remove_temp_claim = 111
	}
	effect = {
		prestige = 0.05
		add_core = 111
	}
}

subjugate_aquileia = {
	
	type = country

	allow = {
		tag = VEN
		is_lesser_in_union = no
		is_subject = no
		exists = AQU
		AQU = {
			neighbour = this
			is_subject = no
			not = { vassal_of = VEN }
			not = { num_of_cities = this }
			is_lesser_in_union = no
		}
	}
	abort = {
		or = {
			not = { exists = AQU }
			AQU = { is_lesser_in_union = yes }
			is_lesser_in_union = yes
			is_subject = yes
		}
	}
	success = {
		AQU = { vassal_of = VEN }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = AQU value = 0 } }
		}
	}
	immediate = {
		casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = AQU
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = AQU
		}
	}
	effect = {
		prestige = 0.1
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = AQU
		}
	}
}

conquer_brescia = {
	
	type = country

	allow = {
		tag = VEN
		is_subject = no
		is_lesser_in_union = no
		107 = {
			not = { owned_by = this }
			any_neighbor_province = { owned_by = this }
		}
	}
	abort = {
		or = {
			is_subject = yes
			is_lesser_in_union = yes
		}
	}
	success = {
		owns = 107
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}	
	}
	immediate = {
		add_temp_claim = 107
	}
	abort_effect = {
		remove_temp_claim = 107
	}
	effect = {
		stability = 2
		add_core = 107
	}
}

conquer_verona = {
	
	type = country

	allow = {
		tag = VEN
		is_subject = no
		is_lesser_in_union = no
		not = { owns = 108 }	# Verona
	}
	abort = {
		or = {
			is_subject = yes
			is_lesser_in_union = yes
		}
	}
	success = {
		owns = 108
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}	
	}
	immediate = {
		add_temp_claim = 108
	}
	abort_effect = {
		remove_temp_claim = 108
	}
	effect = {
		prestige = 0.05
		add_core = 108
	}
}

conquer_istria = {
	
	type = country

	allow = {
		tag = VEN
		is_subject = no
		is_lesser_in_union = no
		owns = 108
		not = { owns = 130 }	# Istria
	}
	abort = {
		or = {
			is_subject = yes
			is_lesser_in_union = yes
		}
	}
	success = {
		owns = 130
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}	
	}
	immediate = {
		add_temp_claim = 130
	}
	abort_effect = {
		remove_temp_claim = 130
	}
	effect = {
		treasury = 50
		add_core = 130
	}
}

defend_crete = {
	
	type = country

	allow = {
		tag = VEN
		owns = 163		# Crete
		exists = TUR
		war_with = TUR
	}
	abort = {
		or = {
			not = { war_with = TUR }
			not = { exists = TUR }
		}
	}
	success = {
		not = { war_with = TUR }
		owns = 163
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}	
	}
	effect = {
		stability = 1
		infamy = -1
	}
}

defend_cyprus = {
	
	type = country

	allow = {
		tag = VEN
		owns = 321		# Cyprus
		exists = TUR
		war_with = TUR
	}
	abort = {
		or = {
			not = { war_with = TUR }
			not = { exists = TUR }
		}
	}
	success = {
		not = { war_with = TUR }
		owns = 321
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}	
	}
	effect = {
		stability = 1
	}
}

retake_crete = {
	
	type = country

	allow = {
		tag = VEN
		exists = TUR
		war_with = TUR
		TUR = { owns = 163 }		# Crete
		is_subject = no
		is_lesser_in_union = no
	}
	abort = {
		or = {
			not = { war_with = TUR }
			not = { exists = TUR }
			is_subject = yes
			is_lesser_in_union = yes
		}
	}
	success = {
		not = { war_with = TUR }
		owns = 163
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}	
	}
	immediate = {
		add_temp_claim = 163
	}
	abort_effect = {
		remove_temp_claim = 163
	}
	effect = {
		war_exhaustion = -4
		infamy = -4
		add_core = 163
	}
}

monopolize_italian_cot = {
	
	type = country

	allow = {
		tag = VEN
		trade_tech = 15
		merchants = 4
		italian_region = {
			cot = yes
			not = { placed_merchants = 6 }
		}
		NOT = {
			italian_region = {
				cot = yes
				placed_merchants = 6
			}
		}
		not = { last_mission = monopolize_italian_cot }
	}
	abort = {
		not = { italian_region = { cot = yes } }
	}
	success = {
		italian_region = {
			cot = yes
			placed_merchants = 6
		}		
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			idea = national_trade_policy
		}	
	}
	effect = {
		treasury = 50
	}
}

venetian_cot = {
	
	type = country

	allow = {
		tag = VEN
		not = { num_of_cots = 1 }
		treasury = 50
	}
	abort = {}
	success = {
		num_of_cots = 1
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			idea = national_trade_policy
		}	
	}
	effect = {
		merchants = 5
		treasury = 50
	}
}

disrupt_genoese_trade = {
	
	type = country
	
	allow = {
		tag = VEN
		exists = GEN
		GEN = {	
			has_country_modifier = black_sea_free_trade
			OR = {
				owns = 286
				owns = 2170
			}
			owns = 101
		}
		101 = {
			cot = yes
		}
	}
	
	abort = {
		GEN = {
			NOT ={
				has_country_modifier = black_sea_free_trade
			}
		}
	}
	
	success = {
		OR = {
			OR = {
				controls = 286
				controls = 2170
			}			
			101 = {
				placed_merchants = 6
			}
		}
	}
	chance = {
		factor = 2500
		modifier = {
			factor = 2
			idea = national_trade_policy
		}
	}
	effect = {
		treasury = 300
	}
}