get_an_alliance_mission = {

	type = neighbor_countries

	allow = {
		this = { NOT = { num_of_allies = 2 } }
		NOT = { num_of_allies = 2 }
		not = { is_threat = this }
		war = no
		relation = { who = THIS value = 50 }
		NOT = { alliance_with = THIS }
		NOT = { is_subject = yes }
		this = { NOT = { is_subject = yes } }
		OR = {
			NOT = { tag = JAP }
			AND = {
				tag = JAP
				NOT = { num_of_daimyos = 1 }
			}
		}
	}
	abort = {
		OR = {
			war_with = THIS
			this = { num_of_allies = 3 }
			not = { num_of_cities = 1 }
		}
	}

	success = {
		alliance_with = THIS
	}
	
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
			culture = THIS
		}
		modifier = {
			factor = 1.5
			religion = THIS
		}
		modifier = {
			factor = 1.5
			this = { DIP = 6 }
		}
		modifier = {
			factor = 0.5
		this = { NOT = { DIP = 5 } }
		}
		modifier = {
			factor = 2.0
			num_of_cities = 10
		}		
	}
	effect = {
		THIS = { prestige = 0.03 }
	}	
}

improve_relations_mission = {

	type = neighbor_countries

	allow = {
		war = no
		religion_group = THIS
		NOT = { war_with = THIS }
		NOT = { relation = { who = THIS value = 100 } }
		relation = {
			who = THIS
			value = 0 
		}
		OR = {
			NOT = { tag = JAP }
			AND = {
				tag = JAP
				NOT = { num_of_daimyos = 1 }
			}
		}		
	}
	abort = {
		or = {
			war_with = THIS
			not = { num_of_cities = 1 }
		}
	}

	success = {
		relation = { who = THIS value = 100 }
	}
	
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
			culture = THIS
		}
		modifier = {
			factor = 1.5
			religion = THIS
		}
		modifier = {
			factor = 1.5
			this = { DIP = 6 }
		}
		modifier = {
			factor = 0.5
			this = { NOT = { DIP = 5 } }
		}
	}
	effect = {
		THIS = { prestige = 0.03 }
	}	
}


generic_annex_vassal_mission = {
	
	type = neighbor_provinces

	allow = {
		this = { war = no }
		owner = {
			nomad = no
			is_subject = yes
			vassal_of = THIS
			not = { num_of_cities = this }
			religion_group = this
		}
		not = { is_core = THIS }
	}
	abort = {
		owner = {
			or = {
				not = { religion_group = this }
				not = { num_of_cities = 1 }
				not = { vassal_of = this }
			}
		}
	}
	success = {
		owned_by = this
	}
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
		this = { DIP = 6 }
		}
		modifier = {
			factor = 1.5
			this = { DIP = 8 }
		}
	}
	immediate = {
		set_province_flag = annex_vassal_mission_target
		owner = {
			random_owned = {
				limit = {
					NOT = { has_province_flag = annex_vassal_mission_target }
					any_neighbor_province = { owned_by = THIS }
				}
				set_province_flag = annex_vassal_mission_target
			}
		}
	}
	abort_effect = {
		owner = { any_owned = { clr_province_flag = annex_vassal_mission_target } }
	}
	effect = {
		THIS = {
			any_owned = {
				limit = {
					has_province_flag = annex_vassal_mission_target
					NOT = { is_core = THIS }
				}
				add_core = THIS
				add_province_modifier = {
					name = integrating_new_core
					duration = 3650
				}
				clr_province_flag = annex_vassal_mission_target
			}
		}
	}
}


improve_reputation_mission = {
	type = country
	allow = {
		infamy = 0.7
		ai = no
		NOT = { infamy = 0.9 }
	}
	abort = {
		infamy = 1.0
	}
	success = {
		NOT = { infamy = 0.5 }
	}
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
			DIP = 6
		}
		modifier = {
			factor = 1.5
			DIP = 8
		}
	}
	effect = {
		define_advisor = { type = diplomat skill = 4 }
	}
			
}

vassalize_mission = {
	type = neighbor_countries

	allow = {
		is_daimyo = no
		is_kampaku = no
		is_shogun = no
		NOT = { government = steppe_horde }
		regency = no
		this = { 
			is_subject = no 
			NOT = { government = steppe_horde }
			num_of_cities = 5
		}
		NOT = { num_of_cities = 4 }
		religion_group = THIS
		is_subject = no 
		NOT = {  
			war_with = THIS
		}
		OR = {
			NOT = { tag = JAP }
			AND = {
				tag = JAP
				NOT = { num_of_daimyos = 1 }
			}
		}
	}
	abort = {
		OR = {
			NOT = { num_of_cities = 1 }	#exists wont work, as we dont know the tag.
			and = {
				is_subject = yes
				not = { vassal_of = this }
			}
		}
	}

	success = {
		vassal_of = THIS
	}
	
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
			culture = THIS
		}
		modifier = {
			factor = 1.5
			religion = THIS
		}
		modifier = {
			factor = 1.5
			NOT = { num_of_cities = 3 }
		}
		modifier = {
			factor = 1.5
			THIS = { num_of_cities = 10 }
		}
		modifier = {
			factor = 1.5
			THIS = { DIP = 6 }
		}
		modifier = {
			factor = 1.5
			THIS = { DIP = 8 }
		}
	}
	
	immediate = {
		add_casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = THIS
		}
	}
	abort_effect = {
		this_remove_casus_belli = {
			type = cb_vassalize_mission
			target = THIS
		}
	}

	effect = {
		THIS = { infamy = -2 }
		this_remove_casus_belli = {
			type = cb_vassalize_mission
			target = THIS
		}
	}	
}

royal_marriage_mission = {

	type = neighbor_countries

	allow = {
		war = no
		nomad = no
		this = { 
			government = monarchy
			NOT = { government = tribal_federation } 
			NOT = { government = tribal_democracy }
			nomad = no
			is_subject = no 
		}
		government = monarchy
		NOT = { government = tribal_federation } 
		NOT = { government = tribal_democracy }
		religion_group = THIS
		is_subject = no 
		NOT = {  
			marriage_with = THIS 
			war_with = THIS
		}
		relation = { who = THIS value = 50 }
		OR = {
			NOT = { tag = JAP }
			AND = {
				tag = JAP
				NOT = { num_of_daimyos = 1 }
			}
		}		
	}
	abort = {
		OR = {
			NOT = { num_of_cities = 1 }	#exists won't work, as we don't know the tag.
			war_with =  THIS
			this = {
				or = {
					not = { government = monarchy }
					is_subject = yes
				}
			}
			not = { government = monarchy }
			is_subject = yes
		}
	}

	success = {
		marriage_with = THIS
	}
	
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
			culture = THIS
		}
		modifier = {
			factor = 1.5
			religion = THIS
		}
		modifier = {
			factor = 1.5
			this = { DIP = 6 }
		}
		modifier = {
			factor = 0.5
			this = { NOT = { DIP = 5 } }
		}
	}

	effect = {
		THIS = { infamy = -1 }
	}	
}

improve_relations_with_rival = {

	type = rival_countries 

	allow = {
		war = no
		this = { infamy = 10 }
		NOT = {  relation = { who = THIS value = 0 }  }
	}
	
	abort = {
		not = { num_of_cities = 1 }
	}
	success = {
		relation = { who = THIS value = 0 }
	}
	
	chance = {
		factor = 1
		modifier = {
			factor = 1.5
			culture = THIS
		}
		modifier = {
			factor = 1.5
			religion = THIS
		}
		modifier = {
			factor = 1.5
			this = { DIP = 6 }
		}
		modifier = {
			factor = 0.5
		this = { NOT = { DIP = 5 } }
		}
	}

	effect = {
		THIS = { infamy = -1 }
	}	
}
