# - increase inflation
# - misc other spy related.
# - declare war.
# - get cb.
# - support rebels in x
# try to co-ally against Europe's prime power and beat it
# seek alliances against an enemy
# use diplomacy to weaken an enemy (trade embargo against trading nation...)
# embargo rival

destabilize_rival = {
	type = rival_countries
	allow = {
		this = { spies = 3 }
		stability = 3
		this = { government_tech = 6 }
		this = { war = no }
		this = { ai = no }
		war = no
	}
	abort = {
		or = {
			this = { war = yes }
			this = { ai = yes }
		}
	}
	success = {
		 num_of_revolts = 2
	}
	chance = {
		factor = 1
		
		modifier = {
			factor = 2
			infamy = 0.5
		}
	}
	effect = {
		this = {
			treasury = 1000
			prestige = 0.1
		}
	}	
}