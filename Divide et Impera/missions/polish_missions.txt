conquer_danzig = {
	
	type = country

	allow = {
		tag = POL
		is_lesser_in_union = no
		is_subject = no
		not = { owns = 43 }	# Danzig
		exists = TEU
		not = { war_with = TEU }
		TEU = { owns = 43 }
	}
	abort = {
		or = {
			is_lesser_in_union = yes
			is_subject = yes
		}
	}
	success = {
		not = { war_with = TEU }
		owns = 43
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { num_of_cots = 1 }
		}	
	}
	immediate = {
		add_temp_claim = 43
	}
	abort_effect = {
		remove_temp_claim = 43
	}
	effect = {
		43 = { cot = yes }
	}
}

conquer_warmia = {
	
	type = country

	allow = {
		tag = POL
		is_lesser_in_union = no
		is_subject = no
		not = { owns = 42 }	# Warmia
		exists = TEU
		not = { war_with = TEU }
		TEU = { owns = 42 }
	}
	abort = {
		or = {
			is_lesser_in_union = yes
			is_subject = yes	
		}
	}
	success = {
		not = { war_with = TEU }
		owns = 42
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}	
	}
	immediate = {
		add_temp_claim = 42
	}
	abort_effect = {
		remove_temp_claim = 42
	}
	effect = {
		add_core = 42
	}
}


poland_mazovia_relations = {
	
	type = country

	allow = {
		OR = {
			tag = POL
			tag = RZP
			tag = RTN
		}
		exists = MAZ
		not = { war_with = MAZ }
		not = { marriage_with = MAZ }
		is_lesser_in_union = no
		is_subject = no
		government = monarchy
		MAZ = {
			government = monarchy
			is_lesser_in_union = no
			is_subject = no
		}
	}
	abort = {
		or = {
			not = { exists = MAZ }
			war_with = MAZ
			is_lesser_in_union = yes
			is_subject = yes
			not = { government = monarchy }
			MAZ = {
				or = {
					not = { government = monarchy }
					is_lesser_in_union = yes
					is_subject = yes
				}
			}
		}
	}
	success = {
		marriage_with = MAZ
		relation = { who = MAZ value = 100 }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = MAZ value = 0 } }
		}
		modifier = {
			factor = 2
			not = { relation = { who = MAZ value = -100 } }
		}
	}
	effect = {
		stability = 1
	}
}

vassalize_mazovia = {
	
	type = country

	allow = {
		OR = {
			tag = POL
			tag = RZP
			tag = RTN
		}
		exists = MAZ
		MAZ = {
			neighbour = this
			not = { vassal_of = POL }
			not = { num_of_cities = this }
			is_lesser_in_union = no
			is_subject = no
		}
	}
	abort = {
		or = {
			not = { exists = MAZ }
			MAZ = { is_lesser_in_union = yes }
		}
	}
	success = {
		OR = { 
			MAZ = { vassal_of = POL }
			MAZ = { vassal_of = RZP }
			MAZ = { vassal_of = RTN }
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			marriage_with = MAZ
		}
		modifier = {
			factor = 2
			relation = { who = MAZ value = 100 }
		}
	}
	immediate = {
		casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = MAZ
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = MAZ
		}
	}
	effect = {
		prestige = 0.1
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = MAZ
		}
	}
}

annex_mazovia = {
	
	type = country

	allow = {
		OR = {
			tag = POL
			tag = RZP
			tag = RTN
		}
		is_subject = no
		exists = MAZ
		MAZ = {
			vassal_of = POL
			neighbour = this
			not = { num_of_cities = this }
			religion_group = this
			owns = 257
		}
	}
	abort = {
		or = {
			not = { exists = MAZ }
			MAZ = { not = { religion_group = this } }
		}
	}
	success = {
		not = { exists = MAZ }
		owns = 257
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			relation = { who = MAZ value = 100 }
		}
		modifier = {
			factor = 2
			relation = { who = MAZ value = 200 }
		}
	}
	immediate = {
		add_temp_claim = 257
	}
	abort_effect = {
		remove_temp_claim = 257
	}
	effect = {
		prestige = 0.05
		add_core = 256
		add_core = 257
		add_core = 2172
		
	}
}

annex_plock = {
	
	type = country

	allow = {
		OR = {
			tag = POL
			tag = RZP
			tag = RTN
		}
		is_subject = no
		exists = PLO
		MAZ = {
			vassal_of = POL
			neighbour = this
			not = { num_of_cities = this }
			religion_group = this
			owns = 257
		}
	}
	abort = {
		or = {
			not = { exists = PLO }
			PLO = { not = { religion_group = this } }
		}
	}
	success = {
		not = { exists = PLO }
		owns = 256
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			relation = { who = PLO value = 100 }
		}
		modifier = {
			factor = 2
			relation = { who = PLO value = 200 }
		}
	}
	effect = {
		prestige = 0.05
		add_core = 256
	}
}

conquer_smolensk = {
	
	type = country

	allow = {
		OR = {
			tag = POL
			tag = RZP
			tag = RTN
		}
		exists = RUS
		war_with = RUS
		RUS = { owns = 293 } # Smolensk
		is_lesser_in_union = no
		is_subject = no
		is_core = 293
	}
	abort = {
		or = {
			not = { war_with = RUS }
			is_lesser_in_union = yes
			is_subject = yes	
		}
	}
	success = {
		not = { war_with = RUS }
		owns = 293
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}	
	}
	effect = {
		prestige = 0.05
	}
}

retake_podolia = {
	
	type = country

	allow = {
		OR = {
			tag = POL
			tag = RZP
			tag = RTN
		}
		is_lesser_in_union = no
		is_subject = no
		exists = TUR
		TUR = {
			or = {
				owns = 281		# Podolia
				controls = 281
			}
			neighbour = this
		}
		is_core = 281
		
	}
	abort = {
		or = {
			TUR = { not = { neighbour = this } }
			is_lesser_in_union = yes
			is_subject = yes
		}
	}
	success = {
		owns = 281
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = TUR value = 0 } }
		}	
	}
	effect = {
		infamy = -5
	}
}

poland_lithuania_relations = {
	
	type = country

	allow = {
		tag = POL
		exists = LIT
		not = { war_with = LIT }
		is_lesser_in_union = no
		is_subject = no
		government = monarchy
		LIT = {
			government = monarchy
			is_lesser_in_union = no
			is_subject = no
		}
		NOT = {
			OR = {
				alliance_with = LIT
				marriage_with = LIT
			}
		}
		not = { last_mission = poland_lithuania_relations }
	}
	abort = {
		or = {
			war_with = LIT
			is_lesser_in_union = yes
			is_subject = yes
			not = { government = monarchy }
			LIT = {
				or = {
					not = { government = monarchy }
					is_lesser_in_union = yes
					is_subject = yes
				}
			}
		}
	}
	success = {
		relation = { who = LIT value = 100 }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = LIT value = 0 } }
		}
		modifier = {
			factor = 2
			not = { relation = { who = LIT value = -100 } }
		}
		modifier = {
			factor = 0.5
			relation = { who = LIT value = 100 }
		}
		modifier = {
			factor = 0.5
			relation = { who = LIT value = 150 }
		}
	}
	effect = {
		prestige = 0.02
		create_marriage = LIT
		create_alliance = LIT
		relation = { who = LIT value = 50 }
	}
}

vassalize_lithuania = {
	
	type = country

	allow = {
		tag = POL
		exists = LIT
		LIT = {
			neighbour = this
			not = { vassal_of = POL }
			not = { num_of_cities = this }
			is_lesser_in_union = no
			is_subject = no
		}
	}
	abort = {
		or = {
			not = { exists = LIT }
			LIT = { is_lesser_in_union = yes }
		}
	}
	success = {
		LIT = { vassal_of = POL }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			relation = { who = LIT value = 100 }
		}
		modifier = {
			factor = 2
			marriage_with = LIT
		}
	}
	immediate = {
		casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = LIT
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = LIT
		}
	}
	effect = {
		prestige = 0.1
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = LIT
		}
	}
}

annex_lithuania = {
	
	type = country

	allow = {
		tag = POL
		is_subject = no
		exists = LIT
		LIT = {
			vassal_of = POL
			neighbour = this
			not = { num_of_cities = this }
			religion_group = this
			owns = 272			# Vilnius
		}
	}
	abort = {
		or = {
			not = { exists = LIT }
			LIT = { not = { religion_group = this } }
		}
	}
	success = {
		not = { exists = LIT }
		owns = 272
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			relation = { who = LIT value = 100 }
		}
		modifier = {
			factor = 2
			relation = { who = LIT value = 200 }
		}
	}
	effect = {
		stability = 2
		infamy = -5
		prestige = 0.1
	}
}

vassalize_the_teutonic_order = {
	
	type = country

	allow = {
		OR = {
			tag = POL
			tag = RZP
			tag = RTN
		}
		exists = TEU
		TEU = {
			neighbour = this
			not = { vassal_of = POL }
			not = { num_of_cities = this }
			is_lesser_in_union = no
			is_subject = no
		}
		not = { last_mission = vassalize_the_teutonic_order }
	}
	abort = {
		or = {
			not = { exists = TEU }
			TEU = { is_lesser_in_union = yes }
		}
	}
	success = {
		OR = {
			TEU = { vassal_of = POL }
			TEU = { vassal_of = RZP }
			TEU = { vassal_of = RTN }
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = TEU value = 0 } }
		}
	}
	immediate = {
		casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = TEU
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = TEU
		}
	}
	effect = {
		prestige = 0.1
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = TEU
		}
	}
}

retake_minsk = {
	
	type = country

	allow = {
		OR = {
			tag = POL
			tag = RZP
			tag = RTN
		}
		exists = RUS
		war_with = RUS
		RUS = {
			or = {
				owns = 276			# Minsk
				controls = 276
			}
		}
		is_core = 276
	}
	abort = {}
	success = {
		owns = 276
		controls = 276
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}	
	}
	effect = {
		prestige = 0.05
	}
}

fortify_the_southern_border = {
	
	type = country

	allow = {
		OR = {
			tag = POL
			tag = RZP
			tag = RTN
		}
		land_tech = 25
		year = 1750
		owns = 261
		owns = 262
		or = {
			261 = { not = { has_building = fort3 } }
			262 = { not = { has_building = fort3 } }
		}
		exists = HAB
		HAB = { neighbour = this }
	}
	abort = {
		HAB = { not = { neighbour = this } }
	}
	success = {
		261 = { has_building = fort3 }
		262 = { has_building = fort3 }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			not = { relation = { who = HAB value = 0 } }
		}
	}
	effect = {
		army_tradition = 0.4
		stability = 1
	}
}

defend_polands_western_border = {
	
	type = country

	allow = {
		OR = {
			tag = POL
			tag = RZP
			tag = RTN
		}
		owns = 254
		owns = 255
		owns = 258
		owns = 259
		year = 1750
		exists = PRU
		war_with = PRU
	}
	abort = {
		not = { war_with = PRU }
	}
	success = {
		not = { war_with = PRU }
		owns = 254
		owns = 255
		owns = 258
		owns = 259
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}	
	}
	effect = {
		war_exhaustion = -5
		stability = 1
	}
}

defend_polands_northern_border = {
	
	type = country

	allow = {
		OR = {
			tag = POL
			tag = RZP
			tag = RTN
		}
		owns = 272
		owns = 275
		owns = 292
		year = 1750
		exists = RUS
		war_with = RUS
	}
	abort = {
		not = { war_with = RUS }
	}
	success = {
		not = { war_with = RUS }
		owns = 272
		owns = 275
		owns = 292
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 7
		}	
	}
	effect = {
		stability = 3
	}
}
